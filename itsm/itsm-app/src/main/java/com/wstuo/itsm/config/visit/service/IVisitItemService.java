package com.wstuo.itsm.config.visit.service;

import com.wstuo.itsm.config.visit.dto.VisitItemDTO;
import com.wstuo.common.dto.PageDTO;
/**
 * 请求回访事项服务层接口
 * @author QXY
 *
 */
public interface IVisitItemService {
	/**
	 * 分页查询回访事项
	 * @param visitNo
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findPagerVisitItems(Long visitNo,int start, int limit,String sord, String sidx);
	/**
	 * 回访事项保存
	 * @param visitItemDto
	 */
	void visitItemSave(VisitItemDTO visitItemDto);
	/**
	 * 回访事项修改
	 * @param visitItemDto
	 */
	void visitItemUpdate(VisitItemDTO visitItemDto);
	/**
	 * 回访事项删除
	 * @param visitNo
	 * @param visitItemNo
	 */
	void visitItemDelete(Long visitNo,Long[] visitItemNo);
	
	/**
	 * 回访事项修改
	 * @param visitItemNo
	 */
	VisitItemDTO findById(Long visitItemNo);
}
