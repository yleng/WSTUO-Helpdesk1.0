package com.wstuo.itsm.domain.util;

public class ModuleUtils {
	public final static String ITSM_REQUEST="itsm.request";
	public final static String ITSM_PROBLEM="itsm.problem";
	public final static String ITSM_CHANGE="itsm.change";
	public final static String ITSM_RELEASE="itsm.release";
	public final static String ITSM_CIM="itsm.configureItem";
}
