package com.wstuo.itsm.request.action;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.itsm.request.dto.RequestDTO;
import com.wstuo.itsm.request.dto.RequestDraftQueryDTO;
import com.wstuo.itsm.request.entity.RequestDraft;
import com.wstuo.itsm.request.service.IRequestDraftService;
import com.wstuo.common.dto.PageDTO;
/**
 * 请求Action类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class RequestDraftAction extends ActionSupport {
	
	@Autowired
    private IRequestDraftService requestDraftService;
	private RequestDraft requestDraft;
	private PageDTO pageDTO;
	private String sord;
    private String sidx;
    private int page = 1;
    private int rows = 10;
    private RequestDraftQueryDTO queryDTO;
	private Long ids;
	private RequestDTO requestDTO;
	private boolean result;
	

	public RequestDraft getRequestDraft() {
		return requestDraft;
	}


	public void setRequestDraft(RequestDraft requestDraft) {
		this.requestDraft = requestDraft;
	}


	public PageDTO getPageDTO() {
		return pageDTO;
	}


	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}


	public String getSord() {
		return sord;
	}


	public void setSord(String sord) {
		this.sord = sord;
	}


	public String getSidx() {
		return sidx;
	}


	public void setSidx(String sidx) {
		this.sidx = sidx;
	}


	public int getPage() {
		return page;
	}


	public void setPage(int page) {
		this.page = page;
	}


	public int getRows() {
		return rows;
	}


	public void setRows(int rows) {
		this.rows = rows;
	}


	public RequestDraftQueryDTO getQueryDTO() {
		return queryDTO;
	}


	public void setQueryDTO(RequestDraftQueryDTO queryDTO) {
		this.queryDTO = queryDTO;
	}


	public Long getIds() {
		return ids;
	}


	public void setIds(Long ids) {
		this.ids = ids;
	}


	public RequestDTO getRequestDTO() {
		return requestDTO;
	}


	public void setRequestDTO(RequestDTO requestDTO) {
		this.requestDTO = requestDTO;
	}


	public boolean isResult() {
		return result;
	}


	public void setResult(boolean result) {
		this.result = result;
	}


	/**
	 * 分页查询请求草稿
	 */

	public String findPageRequestDraft(){
		int start = ( page - 1 ) * rows;
		queryDTO.setSidx(sidx);
		queryDTO.setSord(sord);
		queryDTO.setStart(start);
		queryDTO.setLimit(rows);
		pageDTO=requestDraftService.findPageRequestDraft(queryDTO);
		pageDTO.setPage(page);
		pageDTO.setRows(rows);
		return "pageDTO";
	}


	/**
	 * 添加请求草稿
	 * @return
	 */
	public String saveRequestDraft(){
		requestDraftService.saveRequestDraft(requestDraft,requestDTO);
		return SUCCESS;
	} 
	
	/**
	 * 根据id查询
	 */
	public String findRequestDraftById(){
		requestDTO = requestDraftService.findRequestDraftById(ids);
		return "requestDTO";
	}
	/**
	 * 删除请求草稿
	 */
	public String deleteRequestDraftById(){
		result=requestDraftService.deleteRequestDraftById(ids);
		return "result";
	}
	/**
	 * 修改草稿
	 */
	public String editRequestDraft(){
		result=requestDraftService.editRequestDraft(requestDraft);
		return "result"; 
	}
   
}