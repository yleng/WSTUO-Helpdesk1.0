package com.wstuo.itsm.request.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.wstuo.common.tools.sms.ISMSReceive;
import com.wstuo.common.bpm.api.IBPI;
import com.wstuo.common.bpm.dto.FlowTaskDTO;
import com.wstuo.common.bpm.dto.ProcessDetailDTO;
import com.wstuo.common.bpm.dto.ProcessHandleDTO;
import com.wstuo.itsm.request.dto.RequestDetailDTO;
import com.wstuo.itsm.request.dto.RequestDTO;
import com.wstuo.common.security.dto.UserDTO;
import com.wstuo.common.security.service.IUserInfoService;
import com.wstuo.common.security.utils.LanguageContent;



/**
 * 接收用户发送短信创建请求实现类
 * 江苏宁宿徐定制需求实现(江苏电信企信通2.0系统)
 * @author QXY
 * 
 */
public class SMSReceiveToRequestService implements ISMSReceive {

	@Autowired
	private IRequestService requestService;
	@Autowired
	private IUserInfoService userInfoService;
	@Autowired
	private IBPI bpi;
	private LanguageContent lc=LanguageContent.getInstance();
	/**
	 * 接收用户发送短信创建请求处理 接收用户发送短信审批处理
	 */
	@Transactional
	public void receiveSMS(String callNo, String smsContent, Date sendDate) {
		// 新增请求规则:以"请求:"(可能是中文的:英文)
		// 审批规则:请求id,以A+ID
		String appors = smsContent.substring(0, 1);
		// 审批通过的方法
		if (appors.equals("A") || appors.equals("a")) {
			UserDTO usdto = new UserDTO();
			// 根据电话去获取用户信息
			usdto.setMoblie(callNo);
			UserDTO reqUserDTO = userInfoService.updateUserByPhone(usdto);
			String requestenos = smsContent.substring(1, smsContent.length());
			ProcessHandleDTO handleDTO = new ProcessHandleDTO();
			handleDTO.setEno(Long.parseLong(requestenos));
			handleDTO.setFlowActivityId(11L);
			handleDTO.setLeaderNum(0L);
			handleDTO.setOperator(reqUserDTO.getLoginName());
			RequestDetailDTO reqdetDTO = requestService.findRequestById(Long.parseLong(requestenos));
			if (reqdetDTO != null) {
				handleDTO.setPid(reqdetDTO.getPid());
				FlowTaskDTO flowTaskDto = new FlowTaskDTO();
				List<ProcessDetailDTO> processDetailDTO = bpi.findProcessInstanceDetail(reqdetDTO.getPid());
				for (ProcessDetailDTO pdd : processDetailDTO) {
					for (FlowTaskDTO fd : pdd.getFlowTaskDTO()) {
						flowTaskDto = fd;
					}
				}
				handleDTO.setModule(IRequestService.FILTERCATEGORY);
				handleDTO.setOutcome(flowTaskDto.getOutcomes().get(1));
				handleDTO.setTaskId(flowTaskDto.getTaskId());
				handleDTO.setVariablesAssigneeGroupNo(0L);
				requestService.processHandle(handleDTO);
			}
			// 通过的话直接获取编号
			// 审批不通过为B
		}else if (appors.equals("B") || appors.equals("b")) {
			UserDTO usdto = new UserDTO();
			usdto.setMoblie(callNo);
			UserDTO reqUserDTO = userInfoService.updateUserByPhone(usdto);
			String requestenos = smsContent.substring(1, smsContent.length());
			ProcessHandleDTO handleDTO = new ProcessHandleDTO();
			handleDTO.setAssignType("autoAssignee");
			handleDTO.setEno(Long.parseLong(requestenos));
			handleDTO.setFlowActivityId(11L);
			handleDTO.setMailHandlingProcess(false);
			handleDTO.setModule(IRequestService.FILTERCATEGORY);
			handleDTO.setNextActivityType("task");
			handleDTO.setOperator(reqUserDTO.getLoginName());
			RequestDetailDTO reqdetDTO = requestService.findRequestById(Long.parseLong(requestenos));
			if (reqdetDTO != null) {
				handleDTO.setPid(reqdetDTO.getPid());
				FlowTaskDTO flowTaskDto = new FlowTaskDTO();
				List<ProcessDetailDTO> processDetailDTO = bpi.findProcessInstanceDetail(reqdetDTO.getPid());
				for (ProcessDetailDTO pdd : processDetailDTO) {
					for (FlowTaskDTO fd : pdd.getFlowTaskDTO()) {
						flowTaskDto = fd;
					}
				}
				handleDTO.setOutcome(flowTaskDto.getOutcomes().get(0));
				handleDTO.setTaskId(flowTaskDto.getTaskId());
				handleDTO.setPid(reqdetDTO.getPid());
			}
			handleDTO.setVariablesAssigneeGroupNo(0L);
			requestService.processHandle(handleDTO);
		}else {
			// 否则就是新增请求
			String AddRequests = smsContent.substring(0, 3);
			if (AddRequests.equals(lc.getContent("title.mainTab.request")+"：") || AddRequests.equals(lc.getContent("title.mainTab.request")+":")) {
				UserDTO userdto = new UserDTO();
				RequestDTO requestdto = new RequestDTO();
				userdto.setMoblie(callNo);
				userdto = userInfoService.updateUserByPhone(userdto);
				if (userdto != null) {
					requestdto.setCreatedByName(userdto.getLoginName());
					requestdto.setCreator(userdto.getLoginName());
				} else {
					requestdto.setCreatedByName("sms_send");
					requestdto.setCreator("sms_send");
				}
				requestdto.setActionName(lc.getContent("common.add"));
				requestdto.setEdesc(smsContent);
				requestdto.setRequestCategoryNo(28L);
				requestdto.setRequestCategoryName(lc.getContent("setting.requestCategory"));
				requestdto.setEtitle(lc.getContent("label.operator")+":" + requestdto.getCreator() + lc.getContent("lable.request.creating.sms"));
				requestdto.setTechnicianName(requestdto.getCreator());
				requestService.saveRequest(requestdto);
			}
		}
	}
}
