package com.wstuo.itsm.request.dao;

import com.wstuo.itsm.request.dto.EventCountDTO;
import com.wstuo.itsm.request.entity.EventCount;
import com.wstuo.common.dao.IEntityDAO;

/**
 * 事件统计DAO接口类
 * @author WSTUO_QXY
 *
 */
public interface IEventCountDAO extends IEntityDAO<EventCount> {
	/**
	 * 统计当天创建的事件
	 * @param dto
	 * @return Integer
	 */
	Integer eventCount(EventCountDTO dto);
}
