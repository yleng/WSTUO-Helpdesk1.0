package com.wstuo.itsm.request.dto;

import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.itsm.cim.dto.CIGridDTO;
import com.wstuo.common.dto.BaseDTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 请求详细信息DTO.
 * @author QXY
 *
 */
@SuppressWarnings( "serial" )
public class RequestDetailDTO
    extends BaseDTO
{
    private Long eno;
    private Long ecategoryNo;
    private String ecategoryName;
    private String etitle;
    private String edesc;
    private Long effectRangeNo;
    private String effectRangeName;
    private String effectRemark;
    private Long seriousnessNo;
    private String seriousnessName;
    private Long priorityNo;
    private String priorityName;
    private Long statusNo;
    private String statusName;
    private String statusCode;
    private String address;
    private Date createdOn;
    private Long createdByNo;
    private String createdByName;
    private String createdByOrgName;
    private String createdByOrgPhone;
    private Long technicianNo;
    private String technicianName;
    private Long imodeNo;
    private String imodeName;
    private Long levelNo;
    private String levelName;
    private Long assigneeNo;
    private Long assigneeGroupNo;
    private String assigneeGroupName;
    private String assigneeName;
    private String assigneeLoginName;
    private String pid;
    private String solutions;
    private Date closeTime;
    private Date responsesTime;
    private String requestCode;
    private String remarkStatusName;
    private Date maxResponsesTime;
    private Date maxCompletesTime;
    private Long upgradeApplySign;
    private Date requestResolvedTime;
    private Long requestCategoryNo;
    private String requestCategoryName;
    private Long categoryEavId;
    private String keyWord;
    private String approvalState;
    private String approvalName;
    private String visitRecord;
    private String ownerName;
    private Byte dataFlag;
    private String companyName;//客户名称
    private Long companyNo;//客户No
    private Boolean hang=false;//挂起(20110831-QXY)
    private String slaState;
    private String slaStateColor;//SLA状态背景色
    private String slaLevelName;
    private String fullName;
    /**已经转换为问题*/
    private Boolean isConvertdToProblem=false;
    /**已经转换为变更*/
    private Boolean isConvertdToChange=false;
    private Map attrVals;
    /**是否优化后的流程处理*/
    @Deprecated
    private String newFlowHandle;
    private Long assginTechnicalGroupId;
    private String assginTechnicalGroupName;
    private Boolean slaExpired=false;//SLA是否已过期
    private Long requestServiceDirNo;
    private String requestServiceDirName;
    private Long slaRuleNo;
    private Long offmodeNo;//请求的关闭方式编号
    private String offmodeName;//请求的关闭方式名称
    private List<EventCategory> serviceDirectory = new ArrayList<EventCategory>();//关联服务
    private Boolean agentActionShow =false;//代理动作显示判断
    private List<CIGridDTO> cigDTO;//关联配置项
    private Long approvalNo = 0L;
    private Boolean flowStart = false;
    private Boolean attendance = false;
    private String processKey;
    private Long formId;//表单Id
    private String isShowBorder;
    private String createdByLoginName;
    private Boolean isNewForm;
    
    private Long   locationNos;
    private String locationName;//地点
    
	public Long getLocationNos() {
		return locationNos;
	}
	public void setLocationNos(Long locationNos) {
		this.locationNos = locationNos;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Boolean getIsNewForm() {
		return isNewForm;
	}
	public void setIsNewForm(Boolean isNewForm) {
		this.isNewForm = isNewForm;
	}
	public String getCreatedByLoginName() {
		return createdByLoginName;
	}
	public void setCreatedByLoginName(String createdByLoginName) {
		this.createdByLoginName = createdByLoginName;
	}
	public String getIsShowBorder() {
		return isShowBorder;
	}
	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public String getProcessKey() {
		return processKey;
	}
	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}
	public Boolean getFlowStart() {
		return flowStart;
	}
	public void setFlowStart(Boolean flowStart) {
		this.flowStart = flowStart;
	}
	public List<CIGridDTO> getCigDTO() {
		return cigDTO;
	}
	public void setCigDTO(List<CIGridDTO> cigDTO) {
		this.cigDTO = cigDTO;
	}
	public List<EventCategory> getServiceDirectory() {
		return serviceDirectory;
	}
	public void setServiceDirectory(List<EventCategory> serviceDirectory) {
		this.serviceDirectory = serviceDirectory;
	}
	public String getAssigneeLoginName() {
		return assigneeLoginName;
	}
	public void setAssigneeLoginName(String assigneeLoginName) {
		this.assigneeLoginName = assigneeLoginName;
	}
	public Long getOffmodeNo() {
		return offmodeNo;
	}
	public void setOffmodeNo(Long offmodeNo) {
		this.offmodeNo = offmodeNo;
	}
	public String getOffmodeName() {
		return offmodeName;
	}
	public void setOffmodeName(String offmodeName) {
		this.offmodeName = offmodeName;
	}
	public Long getSlaRuleNo() {
		return slaRuleNo;
	}
	public void setSlaRuleNo(Long slaRuleNo) {
		this.slaRuleNo = slaRuleNo;
	}
	public Long getRequestServiceDirNo() {
		return requestServiceDirNo;
	}
	public void setRequestServiceDirNo(Long requestServiceDirNo) {
		this.requestServiceDirNo = requestServiceDirNo;
	}
	public String getRequestServiceDirName() {
		return requestServiceDirName;
	}
	public void setRequestServiceDirName(String requestServiceDirName) {
		this.requestServiceDirName = requestServiceDirName;
	}
	public Long getAssginTechnicalGroupId() {
		return assginTechnicalGroupId;
	}
	public void setAssginTechnicalGroupId(Long assginTechnicalGroupId) {
		this.assginTechnicalGroupId = assginTechnicalGroupId;
	}
	public String getAssginTechnicalGroupName() {
		return assginTechnicalGroupName;
	}
	public void setAssginTechnicalGroupName(String assginTechnicalGroupName) {
		this.assginTechnicalGroupName = assginTechnicalGroupName;
	}
	public Map getAttrVals() {
		return attrVals;
	}
	public void setAttrVals(Map attrVals) {
		this.attrVals = attrVals;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getSolutions() {
		return solutions;
	}

	public void setSolutions(String solutions) {
		this.solutions = solutions;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	//View
    private String createdByPhone;
    private String createdByEmail;
    private String createByMoblie;
    private String createdByOfficeAddress;
    private String createByPosition;
 
	public String getCreatedByOfficeAddress() {
		return createdByOfficeAddress;
	}
	public void setCreatedByOfficeAddress(String createdByOfficeAddress) {
		this.createdByOfficeAddress = createdByOfficeAddress;
	}
	public String getCreateByPosition() {
		return createByPosition;
	}
	public void setCreateByPosition(String createByPosition) {
		this.createByPosition = createByPosition;
	}
	public String getCreatedByEmail() {
		return createdByEmail;
	}
	public void setCreatedByEmail(String createdByEmail) {
		this.createdByEmail = createdByEmail;
	}
	public RequestDetailDTO(  )
    {
        super(  );
    }

    public Long getEno(  )
    {
        return eno;
    }

    public void setEno( Long eno )
    {
        this.eno = eno;
    }

    public Long getEcategoryNo(  )
    {
        return ecategoryNo;
    }

    public void setEcategoryNo( Long ecategoryNo )
    {
        this.ecategoryNo = ecategoryNo;
    }

    public String getEcategoryName(  )
    {
        return ecategoryName;
    }

    public void setEcategoryName( String ecategoryName )
    {
        this.ecategoryName = ecategoryName;
    }

    public String getEtitle(  )
    {
        return etitle;
    }

    public void setEtitle( String etitle )
    {
        this.etitle = etitle;
    }

    public String getEdesc(  )
    {
        return edesc;
    }

    public void setEdesc( String edesc )
    {
        this.edesc = edesc;
    }

    public Long getEffectRangeNo(  )
    {
        return effectRangeNo;
    }

    public void setEffectRangeNo( Long effectRangeNo )
    {
        this.effectRangeNo = effectRangeNo;
    }

    public String getEffectRangeName(  )
    {
        return effectRangeName;
    }

    public void setEffectRangeName( String effectRangeName )
    {
        this.effectRangeName = effectRangeName;
    }

    public String getEffectRemark(  )
    {
        return effectRemark;
    }

    public void setEffectRemark( String effectRemark )
    {
        this.effectRemark = effectRemark;
    }

    public Long getSeriousnessNo(  )
    {
        return seriousnessNo;
    }

    public void setSeriousnessNo( Long seriousnessNo )
    {
        this.seriousnessNo = seriousnessNo;
    }

    public String getSeriousnessName(  )
    {
        return seriousnessName;
    }

    public void setSeriousnessName( String seriousnessName )
    {
        this.seriousnessName = seriousnessName;
    }

    public Long getPriorityNo(  )
    {
        return priorityNo;
    }

    public void setPriorityNo( Long priorityNo )
    {
        this.priorityNo = priorityNo;
    }

    public String getPriorityName(  )
    {
        return priorityName;
    }

    public void setPriorityName( String priorityName )
    {
        this.priorityName = priorityName;
    }

    public Long getStatusNo(  )
    {
        return statusNo;
    }

    public void setStatusNo( Long statusNo )
    {
        this.statusNo = statusNo;
    }

    public String getStatusName(  )
    {
        return statusName;
    }

    public void setStatusName( String statusName )
    {
        this.statusName = statusName;
    }

    public String getAddress(  )
    {
        return address;
    }

    public void setAddress( String address )
    {
        this.address = address;
    }

    public Date getCreatedOn(  )
    {
        return createdOn;
    }

    public void setCreatedOn( Date createdOn )
    {
        this.createdOn = createdOn;
    }

    public Long getCreatedByNo(  )
    {
        return createdByNo;
    }

    public void setCreatedByNo( Long createdByNo )
    {
        this.createdByNo = createdByNo;
    }

    public String getCreatedByName(  )
    {
        return createdByName;
    }

    public void setCreatedByName( String createdByName )
    {
        this.createdByName = createdByName;
    }

    public Long getTechnicianNo(  )
    {
        return technicianNo;
    }

    public void setTechnicianNo( Long technicianNo )
    {
        this.technicianNo = technicianNo;
    }

    public String getTechnicianName(  )
    {
        return technicianName;
    }

    public void setTechnicianName( String technicianName )
    {
        this.technicianName = technicianName;
    }

    public Long getImodeNo(  )
    {
        return imodeNo;
    }

    public void setImodeNo( Long imodeNo )
    {
        this.imodeNo = imodeNo;
    }

    public Long getLevelNo(  )
    {
        return levelNo;
    }

    public void setLevelNo( Long levelNo )
    {
        this.levelNo = levelNo;
    }

    public Long getAssigneeNo(  )
    {
        return assigneeNo;
    }

    public void setAssigneeNo( Long assigneeNo )
    {
        this.assigneeNo = assigneeNo;
    }

    public Long getAssigneeGroupNo(  )
    {
        return assigneeGroupNo;
    }

    public void setAssigneeGroupNo( Long assigneeGroupNo )
    {
        this.assigneeGroupNo = assigneeGroupNo;
    }

    public String getCreatedByPhone(  )
    {
        return createdByPhone;
    }

    public void setCreatedByPhone( String createdByPhone )
    {
        this.createdByPhone = createdByPhone;
    }

    public String getAssigneeGroupName(  )
    {
        return assigneeGroupName;
    }

    public void setAssigneeGroupName( String assigneeGroupName )
    {
        this.assigneeGroupName = assigneeGroupName;
    }

    public String getAssigneeName(  )
    {
        return assigneeName;
    }

    public void setAssigneeName( String assigneeName )
    {
        this.assigneeName = assigneeName;
    }

	public String getImodeName() {
		return imodeName;
	}

	public void setImodeName(String imodeName) {
		this.imodeName = imodeName;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public Date getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	public Date getResponsesTime() {
		return responsesTime;
	}

	public void setResponsesTime(Date responsesTime) {
		this.responsesTime = responsesTime;
	}

	public String getRequestCode() {
		return requestCode;
	}

	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	public String getRemarkStatusName() {
		return remarkStatusName;
	}

	public void setRemarkStatusName(String remarkStatusName) {
		this.remarkStatusName = remarkStatusName;
	}

	public Date getMaxResponsesTime() {
		return maxResponsesTime;
	}

	public void setMaxResponsesTime(Date maxResponsesTime) {
		this.maxResponsesTime = maxResponsesTime;
	}

	public Date getMaxCompletesTime() {
		return maxCompletesTime;
	}

	public void setMaxCompletesTime(Date maxCompletesTime) {
		this.maxCompletesTime = maxCompletesTime;
	}

	public Long getUpgradeApplySign() {
		return upgradeApplySign;
	}

	public void setUpgradeApplySign(Long upgradeApplySign) {
		this.upgradeApplySign = upgradeApplySign;
	}

	public Date getRequestResolvedTime() {
		return requestResolvedTime;
	}

	public void setRequestResolvedTime(Date requestResolvedTime) {
		this.requestResolvedTime = requestResolvedTime;
	}

	public String getRequestCategoryName() {
		return requestCategoryName;
	}

	public void setRequestCategoryName(String requestCategoryName) {
		this.requestCategoryName = requestCategoryName;
	}

	public Long getRequestCategoryNo() {
		return requestCategoryNo;
	}

	public void setRequestCategoryNo(Long requestCategoryNo) {
		this.requestCategoryNo = requestCategoryNo;
	}

	public String getApprovalState() {
		return approvalState;
	}

	public void setApprovalState(String approvalState) {
		this.approvalState = approvalState;
	}

	public String getVisitRecord() {
		return visitRecord;
	}

	public void setVisitRecord(String visitRecord) {
		this.visitRecord = visitRecord;
	}
	public String getApprovalName() {
		return approvalName;
	}
	public void setApprovalName(String approvalName) {
		this.approvalName = approvalName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public Byte getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	public String getSlaState() {
		return slaState;
	}
	public void setSlaState(String slaState) {
		this.slaState = slaState;
	}
	public String getSlaStateColor() {
		return slaStateColor;
	}
	public void setSlaStateColor(String slaStateColor) {
		this.slaStateColor = slaStateColor;
	}
	public String getSlaLevelName() {
		return slaLevelName;
	}
	public void setSlaLevelName(String slaLevelName) {
		this.slaLevelName = slaLevelName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Boolean getIsConvertdToProblem() {
		return isConvertdToProblem;
	}
	public void setIsConvertdToProblem(Boolean isConvertdToProblem) {
		this.isConvertdToProblem = isConvertdToProblem;
	}
	public Boolean getIsConvertdToChange() {
		return isConvertdToChange;
	}
	public void setIsConvertdToChange(Boolean isConvertdToChange) {
		this.isConvertdToChange = isConvertdToChange;
	}
	public Long getCategoryEavId() {
		return categoryEavId;
	}
	public void setCategoryEavId(Long categoryEavId) {
		this.categoryEavId = categoryEavId;
	}
	public String getNewFlowHandle() {
		return newFlowHandle;
	}
	public void setNewFlowHandle(String newFlowHandle) {
		this.newFlowHandle = newFlowHandle;
	}
	public String getCreateByMoblie() {
		return createByMoblie;
	}
	public void setCreateByMoblie(String createByMoblie) {
		this.createByMoblie = createByMoblie;
	}
	public String getCreatedByOrgName() {
		return createdByOrgName;
	}
	public void setCreatedByOrgName(String createdByOrgName) {
		this.createdByOrgName = createdByOrgName;
	}
	public String getCreatedByOrgPhone() {
		return createdByOrgPhone;
	}
	public void setCreatedByOrgPhone(String createdByOrgPhone) {
		this.createdByOrgPhone = createdByOrgPhone;
	}
	public Long getApprovalNo() {
		return approvalNo;
	}
	public void setApprovalNo(Long approvalNo) {
		this.approvalNo = approvalNo;
	}
	public Boolean getHang() {
		return hang;
	}
	public void setHang(Boolean hang) {
		this.hang = hang;
	}
	public Boolean getSlaExpired() {
		return slaExpired;
	}
	public void setSlaExpired(Boolean slaExpired) {
		this.slaExpired = slaExpired;
	}
	public Boolean getAgentActionShow() {
		return agentActionShow;
	}
	public void setAgentActionShow(Boolean agentActionShow) {
		this.agentActionShow = agentActionShow;
	}
	public Boolean getAttendance() {
		return attendance;
	}
	public void setAttendance(Boolean attendance) {
		this.attendance = attendance;
	}
}
