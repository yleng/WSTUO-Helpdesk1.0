package com.wstuo.itsm.request.service;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import com.wstuo.common.util.AppliactionBaseListener;

/**
 * 邮件自动转请求Job类
 * @author QXY
 *
 */
public class AutoConvertEmailJob implements Job{
	
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		ApplicationContext ctx =AppliactionBaseListener.ctx;
		IMailToRequestService mailToRequestService=(IMailToRequestService)ctx.getBean("mailToRequestService");
		mailToRequestService.convertMail();
	}

}
