package com.wstuo.itsm.request.service;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import com.wstuo.common.scanmq.ScanEmailMessageProducer;
import com.wstuo.common.tools.dto.ExportPageDTO;
import com.wstuo.common.util.AppliactionBaseListener;

/**
 * 邮箱自动扫描
 * @author WSTUO_QXY
 *
 */
public class ScanEmailMessagesJob implements Job {
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		ApplicationContext ctx =AppliactionBaseListener.ctx;
		ScanEmailMessageProducer scanEmailMessageProducer=(ScanEmailMessageProducer)ctx.getBean("scanEmailMessageProducer");
		ExportPageDTO exportPageDTO=new ExportPageDTO();
		exportPageDTO.setMqTitle("Scanning the email....");
		exportPageDTO.setMqContent("System Scanning the email....");
		exportPageDTO.setMqCreator("System");
		scanEmailMessageProducer.send(exportPageDTO);

	}
}
