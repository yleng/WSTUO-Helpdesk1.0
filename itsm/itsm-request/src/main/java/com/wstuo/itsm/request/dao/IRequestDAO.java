package com.wstuo.itsm.request.dao;

import java.util.List;
import java.util.Map;

import com.wstuo.common.tools.dto.StatResultDTO;
import com.wstuo.itsm.request.dto.RequestQueryDTO;
import com.wstuo.itsm.request.entity.Request;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 
 * 请求DAO接口类
 * 
 * @author QXY
 * 
 */
public interface IRequestDAO extends IEntityDAO<Request> {
	/**
	 * 分页查询请求信息
	 * 
	 * @param qdto
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	PageDTO findPager(RequestQueryDTO qdto, String sidx, String sord);

	/**
	 * 分页查询请求信息
	 * 
	 * @param qdto
	 * @param sidx
	 * @param sord
	 * @return
	 */
	List findRequest(RequestQueryDTO qdto, String sidx, String sord);
	/**
	 * 保存请求
	 * 
	 * @param request
	 */
	void saveRequest(Request request);

	/**
	 * 查询长期未解决的请求.
	 * 
	 * @param dateLong
	 *            超出时间
	 * @return List<Request>
	 */
	List<Request> findUnresolved(Long dateLong);

	/**
	 * 查询没未关闭或SLA状态为空的请求
	 * 
	 * @return List<Request>
	 */
	PageDTO findSLAStatusRequest();

	/**
	 * 根据用户和状态查询请求
	 * 
	 * @param loginName
	 * @return List<Request>
	 */
	List<Request> findReqByUserAndState(String loginName);

	/**
	 * 根据公司分组统计
	 * 
	 * @param qdto
	 * @return
	 */
	List<StatResultDTO> groupStatRequest(RequestQueryDTO qdto);
	/**
	 * 查询符合条件的请求
	 * 
	 * @return List<Request>
	 */
	List<Request> findEligibleRequests(Long contractNo, Long responseTime, Long completeTime);

	/**
	 * 繁忙程度:用户请求统计
	 * 
	 * @param categoryId
	 *            请求分类ID:根据请求分类权限查询
	 * @return List
	 */
	//List findUserRequestStats(Long categoryId,String belongsGroupIds);
	List findUserRequestStats(Long categoryId,String belongsGroupIds,List<String> users);
	/**
	 * 分页并排序查询繁忙程度统计
	 * 
	 * @param sidx
	 * @param sord
	 * @param start
	 * @param limit
	 */
	PageDTO findUserRequestStatsBySort(Long[] companyNos,String sidx, String sord, int start, int limit);
	/**
	 * 请求统计
	 */
	Integer requestCountStat(RequestQueryDTO qdto, String type);
	

	/**
	 * 根据条件统计
	 * 
	 * @param qdto
	 * @return
	 */
	Integer countRequestByType(RequestQueryDTO qdto);
	
	/**
	 * 查询关联表单的请求数量
	 * @param formId
	 * @return
	 */
	Integer findRquestNumByFormId(Long[] formIds);
	
	int insertAttr(String hql);

	Map findByCustom(Long eno,Boolean export);
	/**
	 * 统计请求分类报表
	 * @return
	 */
	List requestCatagoryReport();
	/**
	 * 统计请求状态报表
	 * @return
	 */
	List requestStatusReport();
	/**
	 * 统计SLA响应率报表
	 * @return
	 */
	List requestSLAStatusReport();
	/**
	 * 统计SLA完成率报表
	 * @return
	 */
	List requestSLACompleteReport();
	/**
	 * 技术员工作量统计
	 * @return
	 */
	List engineerWorkloadReport();
	/**
	 * 技术员忙碌程度统计
	 * @return
	 */
	List workloadStatusStatistics();
	/**
	 * 技术员处理请求耗时
	 * @return
	 */
	List tctakeTimeperRequest();
	/**
	 * 技术员工时统计报表
	 * @return
	 */
	List technicianCostTime();
}
