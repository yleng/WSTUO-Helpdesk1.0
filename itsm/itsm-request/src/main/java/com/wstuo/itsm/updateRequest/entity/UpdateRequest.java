package com.wstuo.itsm.updateRequest.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.wstuo.common.entity.BaseEntity;
/**
 * 升级时间entity class
 * @author Will
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
public class UpdateRequest extends BaseEntity{
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;
	private Long timeLong;
	private Boolean enable = false;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTimeLong() {
		return timeLong;
	}
	public void setTimeLong(Long timeLong) {
		this.timeLong = timeLong;
	}
	public Boolean getEnable() {
		return enable;
	}
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	
}
