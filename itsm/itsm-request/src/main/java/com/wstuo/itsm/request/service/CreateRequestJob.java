package com.wstuo.itsm.request.service;


import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import com.wstuo.common.config.systeminfo.dao.ISystemInfoDAO;
import com.wstuo.common.config.systeminfo.entity.SystemInfo;
import com.wstuo.common.scheduledTask.dto.ScheduledTaskRequestDTO;
import com.wstuo.itsm.request.dto.RequestDTO;
import com.wstuo.common.util.AppliactionBaseListener;
import com.wstuo.common.util.StringUtils;


/**
 * 创建请求的JOB
 * @author WSTUO_QXY
 *
 */
public class CreateRequestJob implements Job {
	final static Logger LOGGER = Logger.getLogger(CreateRequestJob.class);
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		ApplicationContext ctx =AppliactionBaseListener.ctx;
		IRequestService requestService=(IRequestService)ctx.getBean("requestService");
		ISystemInfoDAO systemInfoDAO = (ISystemInfoDAO)ctx.getBean("systemInfoDAO");
		//获取JOBName参数MAP
		JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();
		//获取key为parameter的对象名
		Object strData =dataMap.get("parameter");
		
		ScheduledTaskRequestDTO scheduledTaskRequestDTO=(ScheduledTaskRequestDTO)strData;
		RequestDTO requestDTO=new RequestDTO();
		try {
			BeanUtils.copyProperties(requestDTO, scheduledTaskRequestDTO);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			LOGGER.error("ScheduledTaskRequestDTO properties copy to RequestDTO error "+e.getMessage());
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			LOGGER.error("ScheduledTaskRequestDTO properties copy to RequestDTO error "+e.getMessage());
		}
		requestDTO.setRequestCategoryNo(scheduledTaskRequestDTO.getEcategoryNo()); 
		String ciIds=scheduledTaskRequestDTO.getCiids();
		if(StringUtils.hasText(ciIds)){
			String[] ciid=ciIds.split(",");
			Long[] ids=new Long[ciid.length];
			for (int i = 0; i < ciid.length; i++) {
				ids[i]=Long.parseLong(ciid[i]);
			}
			requestDTO.setRelatedConfigureItemNos(ids);
		}
		requestDTO.setAttachmentStr(scheduledTaskRequestDTO.getAttachmentStr());
		requestDTO.setAttrVals(scheduledTaskRequestDTO.getAttrVals());
		requestDTO.setCreator("SYSTEM");
		String serviceDirNos=scheduledTaskRequestDTO.getRequestServiceDirNos();
		if(StringUtils.hasText(serviceDirNos)){
			String[] snos=serviceDirNos.split(",");
			Long[] ids=new Long[snos.length];
			for (int i = 0; i < snos.length; i++) {
				if(StringUtils.hasText(snos[i]) && !"null".equals(snos[i])){
					ids[i]=Long.parseLong(snos[i]);
				}
			}
			requestDTO.setServiceDirIds(ids);
		}
		List<SystemInfo> systemInfos = systemInfoDAO.findAll();
		String  languageVersion = systemInfos.get(0).getSystemDataVer();
		String result = "";
		Locale lc = new Locale("zh", "CN");
		if("en".equals(languageVersion)){
			lc=new Locale("en", "US");
		}else if("jp".equals(languageVersion)){
			lc=new Locale("ja", "JP");
		}else if("tw".equals(languageVersion)){
			lc=new Locale("zh", "TW");
		}else{
			lc=new Locale("zh", "CN");
		}
		result = ResourceBundle.getBundle("i18n.itsmbest",lc).getString("common.add");
		requestDTO.setActionName(result);
		requestService.saveRequest(requestDTO);
	}
}
