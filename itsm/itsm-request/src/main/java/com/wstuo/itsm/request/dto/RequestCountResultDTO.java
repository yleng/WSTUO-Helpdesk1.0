package com.wstuo.itsm.request.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * 请求统计信息DTO
 * @author QXY
 *
 */
@SuppressWarnings( "serial" )
public class RequestCountResultDTO extends BaseDTO {
	
	// 全部请求统计
	private Integer countAllRquest=0;
	// 我提出的请求
	private Integer countMyRquest=0;
	// 我们组提出的请求
	private Integer countMyGroupProposedRequest=0;
	// 指派我组的请求
	private Integer countMyGrRquest=0;
	// 指派给我的请求
	private Integer countMyPeRquest=0;
	// 我负责的请求
	private Integer countMyOwRquest=0;
	// 我待的请求任务
	private Integer countMyRequestTask=0;
	// 我组的请求任务
	private Integer countMyGroupRequestTask=0;
	//我已处理的任务
	private Integer countMyProcessedTask=0;
	//我挂起的任务
	private Integer countMyGetPendingTask=0;
	//我未开始处理的请求
	private Integer countMyNoHandleRequest=0;
	//我正在处理的请求
	private Integer countMyIngHandleRequest=0;
	//我已完成的请求
	private Integer countMyCompleteHandleRequest=0;
	//我已关闭的请求
	private Integer countMyCloseHandleRequest=0;
	//我描述不全、未提交的请求
	private Integer myNotComprehensiveNotSubmittedRequest=0;
	//代理给我的请求
	private Integer countactingToMyRequest=0;
	//代理给我的任务
	private Integer actingmyOutstandingTasks=0;
	
	
	public Integer getActingmyOutstandingTasks() {
		return actingmyOutstandingTasks;
	}
	public void setActingmyOutstandingTasks(Integer actingmyOutstandingTasks) {
		this.actingmyOutstandingTasks = actingmyOutstandingTasks;
	}
	public Integer getCountactingToMyRequest() {
		return countactingToMyRequest;
	}
	public void setCountactingToMyRequest(Integer countactingToMyRequest) {
		this.countactingToMyRequest = countactingToMyRequest;
	}
	public Integer getCountAllRquest() {
		return countAllRquest;
	}
	public void setCountAllRquest(Integer countAllRquest) {
		this.countAllRquest = countAllRquest;
	}
	public Integer getCountMyRquest() {
		return countMyRquest;
	}
	public void setCountMyRquest(Integer countMyRquest) {
		this.countMyRquest = countMyRquest;
	}
	public Integer getCountMyGrRquest() {
		return countMyGrRquest;
	}
	public void setCountMyGrRquest(Integer countMyGrRquest) {
		this.countMyGrRquest = countMyGrRquest;
	}
	public Integer getCountMyPeRquest() {
		return countMyPeRquest;
	}
	public void setCountMyPeRquest(Integer countMyPeRquest) {
		this.countMyPeRquest = countMyPeRquest;
	}
	public Integer getCountMyOwRquest() {
		return countMyOwRquest;
	}
	public void setCountMyOwRquest(Integer countMyOwRquest) {
		this.countMyOwRquest = countMyOwRquest;
	}
	public Integer getCountMyRequestTask() {
		return countMyRequestTask;
	}
	public void setCountMyRequestTask(Integer countMyRequestTask) {
		this.countMyRequestTask = countMyRequestTask;
	}
	public Integer getCountMyGroupRequestTask() {
		return countMyGroupRequestTask;
	}
	public void setCountMyGroupRequestTask(Integer countMyGroupRequestTask) {
		this.countMyGroupRequestTask = countMyGroupRequestTask;
	}
	public Integer getCountMyGroupProposedRequest() {
		return countMyGroupProposedRequest;
	}
	public void setCountMyGroupProposedRequest(Integer countMyGroupProposedRequest) {
		this.countMyGroupProposedRequest = countMyGroupProposedRequest;
	}
	public Integer getCountMyNoHandleRequest() {
		return countMyNoHandleRequest;
	}
	public void setCountMyNoHandleRequest(Integer countMyNoHandleRequest) {
		this.countMyNoHandleRequest = countMyNoHandleRequest;
	}
	public Integer getCountMyIngHandleRequest() {
		return countMyIngHandleRequest;
	}
	public void setCountMyIngHandleRequest(Integer countMyIngHandleRequest) {
		this.countMyIngHandleRequest = countMyIngHandleRequest;
	}
	public Integer getCountMyCompleteHandleRequest() {
		return countMyCompleteHandleRequest;
	}
	public void setCountMyCompleteHandleRequest(Integer countMyCompleteHandleRequest) {
		this.countMyCompleteHandleRequest = countMyCompleteHandleRequest;
	}
	public Integer getCountMyCloseHandleRequest() {
		return countMyCloseHandleRequest;
	}
	public void setCountMyCloseHandleRequest(Integer countMyCloseHandleRequest) {
		this.countMyCloseHandleRequest = countMyCloseHandleRequest;
	}
	public Integer getMyNotComprehensiveNotSubmittedRequest() {
		return myNotComprehensiveNotSubmittedRequest;
	}
	public void setMyNotComprehensiveNotSubmittedRequest(
			Integer myNotComprehensiveNotSubmittedRequest) {
		this.myNotComprehensiveNotSubmittedRequest = myNotComprehensiveNotSubmittedRequest;
	}
	public Integer getCountMyProcessedTask() {
		return countMyProcessedTask;
	}
	public void setCountMyProcessedTask(Integer countMyProcessedTask) {
		this.countMyProcessedTask = countMyProcessedTask;
	}
	public Integer getCountMyGetPendingTask() {
		return countMyGetPendingTask;
	}
	public void setCountMyGetPendingTask(Integer countMyGetPendingTask) {
		this.countMyGetPendingTask = countMyGetPendingTask;
	}


	
	
}
