package com.wstuo.itsm.cim.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.json.annotations.JSON;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.config.category.dto.EventCategoryDTO;
import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.util.TimeUtils;

/**
 * 配置项详细信息DTO
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings("serial")
public class CIDetailDTO extends BaseDTO {

    private Long ciId;
    private String cino;
    private String ciname;
    private String model;
    private String serialNumber;
    private String barcode;
    private Date buyDate;
    private Date arrivalDate;
    private String poNo;
    private Date warningDate;
    private Long providerId;
    private String providerName;
    private Integer lifeCycle;
    private Integer warranty;
    private String userName;
    private String owner;
    private Long statusId;
    private String status;
    private Long locId;
    private String loc;
    private Long brandId;
    private String brandName;
    private Long categoryNo;
    private String categoryName;
    private Long eavNo;
    private Map<String,String> attrVals =new HashMap<String, String>(); 
    private List<Attachment> attachments;//附件list
    private String attachmentStr;//附件字符串
    //20110709-QXY
    private String department;//部门
    private String CDI;//CDI
    private String workNumber;//Work Number
    private String project;//Project
    private String sourceUnits;//来源单位
    private Boolean financeCorrespond=false;//与财务对应
    private Double assetsOriginalValue; //资产原值
    private Date wasteTime;//报废时间
    private Date borrowedTime;//借出时间
    private String originalUser;//原使用者
    private Date recoverTime;//回收时间
    private Date expectedRecoverTime;//预计回收时间
    private String usePermissions;//使用权限
    private Byte dataFlag;
    private String companyName;//客户名称
    private Long companyNo;//客户No
    private Long ciServiceDirNo;
    private String ciServiceDirName;
    private Long systemPlatformId;
    private String systemPlatform;
    private List<String> serviceDirectoryNames;
    private List<EventCategoryDTO> serviceDirDtos;
    private Long softSetId;
   	private String softSetingParam;
   	private String softConfigureAm;
   	private String softRemark1;
   	private String softRemark2;
   	private String softRemark3;
   	private List<Attachment> softAttachments;//附件list
    //20130706 ciel
    private Integer depreciationIsZeroYears; //多少年折旧率为0
    private Double ciDepreciation ; //折旧率
    private Date createTime;
    private Date lastUpdateTime;
    private Long originalUserId;
    private Long userNameId;
    private Long ownerId;
    private Long formId;
    private String isShowBorder;
    private Boolean isNewForm;
    
    
	
	public Boolean getIsNewForm() {
		return isNewForm;
	}
	public void setIsNewForm(Boolean isNewForm) {
		this.isNewForm = isNewForm;
	}
	public String getIsShowBorder() {
		return isShowBorder;
	}
	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Long getOriginalUserId() {
		return originalUserId;
	}
	public void setOriginalUserId(Long originalUserId) {
		this.originalUserId = originalUserId;
	}
	public Long getUserNameId() {
		return userNameId;
	}
	public void setUserNameId(Long userNameId) {
		this.userNameId = userNameId;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public Long getSoftSetId() {
		return softSetId;
	}
	public void setSoftSetId(Long softSetId) {
		this.softSetId = softSetId;
	}
	public String getSoftSetingParam() {
		return softSetingParam;
	}
	public void setSoftSetingParam(String softSetingParam) {
		this.softSetingParam = softSetingParam;
	}
	public String getSoftConfigureAm() {
		return softConfigureAm;
	}
	public void setSoftConfigureAm(String softConfigureAm) {
		this.softConfigureAm = softConfigureAm;
	}
	public String getSoftRemark1() {
		return softRemark1;
	}
	public void setSoftRemark1(String softRemark1) {
		this.softRemark1 = softRemark1;
	}
	public String getSoftRemark2() {
		return softRemark2;
	}
	public void setSoftRemark2(String softRemark2) {
		this.softRemark2 = softRemark2;
	}
	public String getSoftRemark3() {
		return softRemark3;
	}
	public void setSoftRemark3(String softRemark3) {
		this.softRemark3 = softRemark3;
	}
	public List<Attachment> getSoftAttachments() {
		return softAttachments;
	}
	public void setSoftAttachments(List<Attachment> softAttachments) {
		this.softAttachments = softAttachments;
	}
	public List<EventCategoryDTO> getServiceDirDtos() {
		return serviceDirDtos;
	}
	public void setServiceDirDtos(List<EventCategoryDTO> serviceDirDtos) {
		this.serviceDirDtos = serviceDirDtos;
	}
	public List<String> getServiceDirectoryNames() {
		return serviceDirectoryNames;
	}
	public void setServiceDirectoryNames(List<String> serviceDirectoryNames) {
		this.serviceDirectoryNames = serviceDirectoryNames;
	}
	public Long getCiServiceDirNo() {
		return ciServiceDirNo;
	}
	public void setCiServiceDirNo(Long ciServiceDirNo) {
		this.ciServiceDirNo = ciServiceDirNo;
	}
	public String getCiServiceDirName() {
		return ciServiceDirName;
	}
	public void setCiServiceDirName(String ciServiceDirName) {
		this.ciServiceDirName = ciServiceDirName;
	}
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}
	public String getCino() {
		return cino;
	}
	public void setCino(String cino) {
		this.cino = cino;
	}
	public String getCiname() {
		return ciname;
	}
	public void setCiname(String ciname) {
		this.ciname = ciname;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	@JSON(format=TimeUtils.DATE_PATTERN)
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	@JSON(format=TimeUtils.DATE_PATTERN)
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	@JSON(format=TimeUtils.DATE_PATTERN)
	public Date getWarningDate() {
		return warningDate;
	}
	public void setWarningDate(Date warningDate) {
		this.warningDate = warningDate;
	}
	public Long getProviderId() {
		return providerId;
	}
	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}
	public String getProviderName() {
		return providerName;
	}
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	public Integer getLifeCycle() {
		return lifeCycle;
	}
	public void setLifeCycle(Integer lifeCycle) {
		this.lifeCycle = lifeCycle;
	}
	public Integer getWarranty() {
		return warranty;
	}
	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getLocId() {
		return locId;
	}
	public void setLocId(Long locId) {
		this.locId = locId;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public Long getBrandId() {
		return brandId;
	}
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public Long getCategoryNo() {
		return categoryNo;
	}
	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Long getEavNo() {
		return eavNo;
	}
	public void setEavNo(Long eavNo) {
		this.eavNo = eavNo;
	}
	public Map<String, String> getAttrVals() {
		return attrVals;
	}
	public void setAttrVals(Map<String, String> attrVals) {
		this.attrVals = attrVals;
	}
	public List<Attachment> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}
	public String getAttachmentStr() {
		return attachmentStr;
	}
	public void setAttachmentStr(String attachmentStr) {
		this.attachmentStr = attachmentStr;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCDI() {
		return CDI;
	}
	public void setCDI(String cDI) {
		CDI = cDI;
	}
	public String getWorkNumber() {
		return workNumber;
	}
	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getSourceUnits() {
		return sourceUnits;
	}
	public void setSourceUnits(String sourceUnits) {
		this.sourceUnits = sourceUnits;
	}
	public Boolean getFinanceCorrespond() {
		return financeCorrespond;
	}
	public void setFinanceCorrespond(Boolean financeCorrespond) {
		this.financeCorrespond = financeCorrespond;
	}
	public Double getAssetsOriginalValue() {
		return assetsOriginalValue;
	}
	public void setAssetsOriginalValue(Double assetsOriginalValue) {
		this.assetsOriginalValue = assetsOriginalValue;
	}
	public Date getWasteTime() {
		return wasteTime;
	}
	public void setWasteTime(Date wasteTime) {
		this.wasteTime = wasteTime;
	}
	public Date getBorrowedTime() {
		return borrowedTime;
	}
	public void setBorrowedTime(Date borrowedTime) {
		this.borrowedTime = borrowedTime;
	}
	public String getOriginalUser() {
		return originalUser;
	}
	public void setOriginalUser(String originalUser) {
		this.originalUser = originalUser;
	}
	public Date getRecoverTime() {
		return recoverTime;
	}
	public void setRecoverTime(Date recoverTime) {
		this.recoverTime = recoverTime;
	}
	public Date getExpectedRecoverTime() {
		return expectedRecoverTime;
	}
	public void setExpectedRecoverTime(Date expectedRecoverTime) {
		this.expectedRecoverTime = expectedRecoverTime;
	}
	public String getUsePermissions() {
		return usePermissions;
	}
	public void setUsePermissions(String usePermissions) {
		this.usePermissions = usePermissions;
	}
	public Byte getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	public Long getSystemPlatformId() {
		return systemPlatformId;
	}
	public void setSystemPlatformId(Long systemPlatformId) {
		this.systemPlatformId = systemPlatformId;
	}
	public String getSystemPlatform() {
		return systemPlatform;
	}
	public void setSystemPlatform(String systemPlatform) {
		this.systemPlatform = systemPlatform;
	}
	public Integer getDepreciationIsZeroYears() {
		return depreciationIsZeroYears;
	}
	public void setDepreciationIsZeroYears(Integer depreciationIsZeroYears) {
		this.depreciationIsZeroYears = depreciationIsZeroYears;
	}
	public Double getCiDepreciation() {
		return ciDepreciation;
	}
	public void setCiDepreciation(Double ciDepreciation) {
		this.ciDepreciation = ciDepreciation;
	}
    
}
