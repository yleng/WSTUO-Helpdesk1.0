package com.wstuo.itsm.cim.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.entity.BaseEntity;


/**
 * 关联配置项 实体类
 * @author QXY
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
public class CIRelevance
    extends BaseEntity
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long relevanceId;
    @Column(nullable=true)
    private String relevanceDesc;
    @ManyToOne(  )
    @JoinColumn( name = "ciRelevance" )
    private CI ciRelevanceId;
    @ManyToOne(  )
    @JoinColumn( name = "unCiRelevance" )
    private CI unCiRelevanceId;
    @ManyToOne
    private DataDictionaryItems ciRelationType;//配置项CI的关系类型
    
    public Long getRelevanceId(  )
    {
        return relevanceId;
    }

    public void setRelevanceId( Long relevanceId )
    {
        this.relevanceId = relevanceId;
    }

    public String getRelevanceDesc(  )
    {
        return relevanceDesc;
    }

    public void setRelevanceDesc( String relevanceDesc )
    {
        this.relevanceDesc = relevanceDesc;
    }

    public CI getCiRelevanceId(  )
    {
        return ciRelevanceId;
    }

    public void setCiRelevanceId( CI ciRelevanceId )
    {
        this.ciRelevanceId = ciRelevanceId;
    }

    public CI getUnCiRelevanceId(  )
    {
        return unCiRelevanceId;
    }

    public void setUnCiRelevanceId( CI unCiRelevanceId )
    {
        this.unCiRelevanceId = unCiRelevanceId;
    }

	public DataDictionaryItems getCiRelationType() {
		return ciRelationType;
	}

	public void setCiRelationType(DataDictionaryItems ciRelationType) {
		this.ciRelationType = ciRelationType;
	}
}
