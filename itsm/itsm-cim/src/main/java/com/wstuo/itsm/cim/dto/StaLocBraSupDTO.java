package com.wstuo.itsm.cim.dto;

import com.wstuo.common.config.dictionary.dto.DataDictionaryItemsDTO;

import java.util.List;

/**
 * 供应商、使用状态、所以位置、品牌基础信息DTO类
 * @author QXY
 */
public class StaLocBraSupDTO
{
    private List<DataDictionaryItemsDTO> dictionaryItemsStatusList;
    private List<DataDictionaryItemsDTO> dictionaryItemsBrandList;
    private List<DataDictionaryItemsDTO> dictionaryItemsLocList;
    private List<DataDictionaryItemsDTO> dictionaryItemsProviderList;
    private List<DataDictionaryItemsDTO> dictionaryItemsSystemPlatformList;

    public StaLocBraSupDTO(  )
    {
        super(  );
    }

    public List<DataDictionaryItemsDTO> getDictionaryItemsStatusList(  )
    {
        return dictionaryItemsStatusList;
    }

    public void setDictionaryItemsStatusList( List<DataDictionaryItemsDTO> dictionaryItemsStatusList )
    {
        this.dictionaryItemsStatusList = dictionaryItemsStatusList;
    }

    public List<DataDictionaryItemsDTO> getDictionaryItemsBrandList(  )
    {
        return dictionaryItemsBrandList;
    }

    public void setDictionaryItemsBrandList( List<DataDictionaryItemsDTO> dictionaryItemsBrandList )
    {
        this.dictionaryItemsBrandList = dictionaryItemsBrandList;
    }

    public List<DataDictionaryItemsDTO> getDictionaryItemsLocList(  )
    {
        return dictionaryItemsLocList;
    }

    public void setDictionaryItemsLocList( List<DataDictionaryItemsDTO> dictionaryItemsLocList )
    {
        this.dictionaryItemsLocList = dictionaryItemsLocList;
    }

    public List<DataDictionaryItemsDTO> getDictionaryItemsProviderList(  )
    {
        return dictionaryItemsProviderList;
    }

    public void setDictionaryItemsProviderList( List<DataDictionaryItemsDTO> dictionaryItemsProviderList )
    {
        this.dictionaryItemsProviderList = dictionaryItemsProviderList;
    }

	public List<DataDictionaryItemsDTO> getDictionaryItemsSystemPlatformList() {
		return dictionaryItemsSystemPlatformList;
	}

	public void setDictionaryItemsSystemPlatformList(
			List<DataDictionaryItemsDTO> dictionaryItemsSystemPlatformList) {
		this.dictionaryItemsSystemPlatformList = dictionaryItemsSystemPlatformList;
	}
    
}
