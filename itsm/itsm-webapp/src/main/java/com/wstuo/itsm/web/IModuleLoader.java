package com.wstuo.itsm.web;

import java.util.Map;
/**
 * 模块加载接口
 * @author QXY
 *
 */
public interface IModuleLoader {
	/**
	 * 加载全部
	 */
	void load(String Language,String module,Map<String,String> components);
	/**
	 * 加载流程
	 */
	void loadBpm(String fileName);
	/**
	 * 加载规则
	 */
	void loadRule(String fileName);
	/**
	 * 加载报表
	 */
	void loadReport(String fileName);
	/**
	 *  加载数据字典
	 */
	void loadDataDicts(String fileName);
	/**
	 * 加载数据分组
	 * @param fileName
	 */
	void loadDataDictGroups(String fileName);
	/**
	 * 加载分类
	 */
	void loadCategories(String fileName);
	/**
	 * 加载其他
	 */
	void loadOthers(String fileName);
}
