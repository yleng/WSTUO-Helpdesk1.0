package com.wstuo.schema.action;

import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.schema.ISchemaTool;
import com.wstuo.common.security.dto.UserDTO;

@SuppressWarnings("serial")
public class SchemaAction  extends ActionSupport{
	private static final Logger logger = Logger.getLogger(SchemaAction.class);

	@Autowired
	private ISchemaTool schemaTool;
	
	private String tenantIdentifier;
	private String result;
	private String language;
	private UserDTO userDTO;

	public UserDTO getUserDTO() {
		return userDTO;
	}
	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getTenantIdentifier() {
		return tenantIdentifier;
	}
	public void setTenantIdentifier(String tenantIdentifier) {
		this.tenantIdentifier = tenantIdentifier;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String createSchema(){
		try {
			schemaTool.createSchema(tenantIdentifier);
		} catch (SQLException e) {
			logger.error("createSchema()", e);
		}
		return SUCCESS;
	}
	
	public String updateSchema(){
		try {
			schemaTool.updateSchema(tenantIdentifier);
		} catch (SQLException e) {
			logger.error("updateSchema()", e);
		}
		return SUCCESS;
	}

	public String createDatabase(){
		try {
			schemaTool.createDatabase(tenantIdentifier);
		} catch (SQLException e) {
			logger.error("createDatabase()", e); //$NON-NLS-1$
		}
		return SUCCESS;
	}
	
	public String configDataLoader(){
		try {
			schemaTool.configDataLoader(tenantIdentifier,language,userDTO);
		} catch (SQLException e) {
			logger.error("configDataLoader()", e); //$NON-NLS-1$
		}
		return SUCCESS;
	}
	
	public String tenantDataInit(){
		try {
			schemaTool.tenantDataInit(tenantIdentifier, language, userDTO);
		} catch (SQLException e) {
			logger.error("schemaInit()", e); //$NON-NLS-1$
		}
		return SUCCESS;
	}
	
	public String configureFileInit(){
		schemaTool.configureFileInit(tenantIdentifier);
		return SUCCESS;
	}
}
