/**
 * Created by William on 2014/11/4.
 */
$package('leftMenu');

leftMenu = function(){
	this.loadUrl="";
    return {
        menuProp : {
                'skinUrl': '../skin/default/leftMenu/',  // 菜单路径
                'leftMenuWidth': 66, // 左菜单宽度
                'firstMenuInterval': null,  // 二級菜單计时器
                'isFirstMenuHover': false,
                'isSecondMenuHover': false,
                'secondMenuInterval': null,
                testInterval: null,
                firstMenuHoverId: '',    // 一级菜单悬停的id
                secondMenuId: ''
        },
        
        
        /**
         * @description 初始化菜单高度
         */
        initMenuHeight : function() {
            var panelHeight = $('#index_body').layout('panel', 'west').panel('options').height;
            $('#leftMenu_first').height(panelHeight);
            $('#leftMenu_second').height(panelHeight);
        },
        /**
         * 菜单图标点击样式
         */
        iconVisited:function(selector){
            
        	$(selector).each(function(){
        		$(this).click(function(){
        		    
                	var target = $(selector+"[class*='-visited']");
                	var id = $(this).attr("class").split(' ')[0];
            		if(target.attr("class")){
            			var oldIds = target.attr("class").split(' ');
            			var oldId = oldIds[oldIds.length - 1];
            			target.removeClass(oldId);
            		}
        			$(this).addClass(id+"-visited");
            		
        		});
        	});
        },
        
        /**
         * @description 加载一级菜单
         */
        loadMenu : function() {
        	/**
        	 * 一级菜单点击样式
        	 */
        	leftMenu.iconVisited('.leftMenu_first_items li');
            
            $('#firstMenu_new').mouseenter(function(){
                var holdTime = 0;
                clearTimeout(leftMenu.menuProp.secondMenuInterval);
                leftMenu.menuProp.testInterval = setInterval(function(){
                    holdTime ++;
                    if (holdTime >= 3) {
                        leftMenu.menuProp.firstMenuHoverId = 'firstMenu_new';
                        leftMenu.showSecondMenu('portal');
                    }
                }, 100);
                
            });
            $('#firstMenu_new').mouseleave(function(){
                clearTimeout(leftMenu.menuProp.testInterval);
                leftMenu.secondeMenuHover();
            });
            
            $('#toolsTopMenu').mouseenter(function(){
                var holdTime = 0;
                clearTimeout(leftMenu.menuProp.secondMenuInterval);
                leftMenu.menuProp.testInterval = setInterval(function(){
                    holdTime ++;
                    if (holdTime >= 3) {
                        leftMenu.menuProp.firstMenuHoverId = 'toolsTopMenu';
                        leftMenu.showSecondMenu('tools',function(){  
                            common.security.includes.includes.loadSelectCustomerIncludesFile();
                            common.config.includes.includes.loadCategoryIncludesFile();
                         });
                    }
                }, 100);
                
            });
            $('#toolsTopMenu').mouseleave(function(){
                clearTimeout(leftMenu.menuProp.testInterval);
                leftMenu.secondeMenuHover();
            });
            
            $('#reportTopMenu').mouseenter(function(){
            	common.report.includes.includes.loadReportViewIncludesFile();
            	common.config.includes.includes.loadCustomFilterIncludesFile();
                var holdTime = 0;
                clearTimeout(leftMenu.menuProp.secondMenuInterval);
                leftMenu.menuProp.testInterval = setInterval(function(){
                    holdTime ++;
                    if (holdTime >= 3) {
                        leftMenu.menuProp.firstMenuHoverId = 'reportTopMenu';
                        leftMenu.showSecondMenu('report',function(){  
                            common.security.includes.includes.loadSelectCustomerIncludesFile();
                            common.config.includes.includes.loadCategoryIncludesFile();
                            common.config.includes.includes.loadCustomFilterIncludesFile();
                        });
                    }
                }, 100);
                
            });
            $('#reportTopMenu').mouseleave(function(){
                clearTimeout(leftMenu.menuProp.testInterval);
                leftMenu.secondeMenuHover();
            });
            
        },
        /**
         * @description 一级菜单点击事件
         * @param menuName 菜单的名字
         */
        firstMenuClick : function(menuName) {
            leftMenu.hideSecondMenu(); // 隐藏二级菜单
        },
        /**
         * @description 二级菜单悬浮
         */
        secondeMenuHover : function() {
            $('#leftMenu_second').mouseenter(function(){
                leftMenu.menuProp.isSecondMenuHover = true;
            });
            $('#leftMenu_second').mouseleave(function(){
                leftMenu.menuProp.isSecondMenuHover = false;
            });
            leftMenu.menuProp.secondMenuInterval = setInterval(function(){
                leftMenu.menuProp.isFirstMenuHover = leftMenu.menuProp.isSecondMenuHover;
                if (!(leftMenu.menuProp.isFirstMenuHover || leftMenu.menuProp.isSecondMenuHover)) {
                    leftMenu.menuProp.isFirstMenuHover = false;
                    leftMenu.menuProp.isSecondMenuHover = false;
                    leftMenu.hideSecondMenu();
                    $('#leftMenu_second').unbind('mouseleave');
                }
            }, 300);
        },
        
        /**
         * @description 显示二级菜单
         * @param url 二级菜单路径 
         */
        showSecondMenu : function (url,method) {
        	var $secondMenu = $('#leftMenu_second');
        	var $secondMenuContainer = $('#leftMenu_second_container'); // 显示的二级菜单
        	
        	$('#portal').hide();
        	$('#tools').hide();
        	$('#report').hide();
        	
        	$secondMenu.show();
        	
        	leftMenu.menuProp.secondMenuId = url;
        	$('#' + url).show();
        	leftMenu.isSecondMenuScroll();
            
            leftMenu.secondMenuAction(); // 初始化二级菜单特效
            leftMenu.thirdMenuClickAction();
        },
        
        
        /**
         * @description 初始化一级菜单上下的滚动
         */
        initFirstMenuMove : function() {
            $('#leftMenu_menuUp').hover(function(){
                leftMenu.firstMenuInterval = window.clearInterval(leftMenu.firstMenuInterval);
                leftMenu.firstMenuInterval = setInterval('leftMenu.menuUp()', '50');
            }, function() {
                leftMenu.firstMenuInterval = window.clearInterval(leftMenu.firstMenuInterval);
            });
            
            $('#leftMenu_menuDown').hover(function(){
                leftMenu.firstMenuInterval = window.clearInterval(leftMenu.firstMenuInterval);
                leftMenu.firstMenuInterval = setInterval('leftMenu.menuDown()', '50');
            }, function() {
                leftMenu.firstMenuInterval = window.clearInterval(leftMenu.firstMenuInterval);
            });
        },
        
        /**
         * @description 二级菜单特效
         */
        secondMenuAction : function() {
           /**
            * 二级菜单点击效果样式
            */
           leftMenu.iconVisited('.leftMenu_second_items > li[class!=subItems]');
        },
        
        /**
         * @description 隐藏二级菜单
         */
        hideSecondMenu : function(){
            leftMenu.menuProp.firstMenuHoverId = '';
            clearTimeout(leftMenu.menuProp.secondMenuInterval);
            $('#leftMenu_second').hide();   // 隐藏二级菜单
        },
        
        /**
         * @description 二级菜单点击事件
         * @param url 要打开的地址
         */
        secondMenuClick : function(menuName) {
            if (leftMenu.menuProp.firstMenuHoverId) {
                $('#' + leftMenu.menuProp.firstMenuHoverId).click();
            }
            var $secondMenu = $('#secondMenu_' + menuName);
            $('.subItems').hide(); // 先清空所有展开的子菜单
            leftMenu.hideSecondMenu();
        },
        
        
        /**
         * @description 显示三级菜单
         * @param name 二级菜单的名字
         */
        showThirdMenu : function (name) {
            
            var $subItem = $('.subItems'); // 三级菜单项
            var $thirdItems = $('#thirdMenu_' + name); // 三级菜单
            var $secondMenu = $('#secondMenu_' + name);
            
            $('.leftMenu_second_items').css('top', '0px'); // 重置二级菜单位置
            
             // 先清空所有展开的子菜单
            if (!$thirdItems.is(':hidden')) {
                $subItem.hide();
                leftMenu.isSecondMenuScroll();
                return;
            }
            $subItem.hide();
            $thirdItems.show();
            leftMenu.isSecondMenuScroll();
        },
        
        
        /**
         * @description 三级菜单点击特效
         */
        thirdMenuClickAction : function() {
            // 渲染三级菜单
            $('.leftMenu_second .subItems li').each(function(){
                $(this).click(function(){
                    if (leftMenu.menuProp.firstMenuHoverId) {
                        $('#' + leftMenu.menuProp.firstMenuHoverId).click();
                    }
                    leftMenu.hideSecondMenu();
                    $('.subItems li').removeClass('thirdMenu_click');
                    $(this).addClass('thirdMenu_click');
                });
            });
            
            $('.leftMenu_systemSetting .subItems li').each(function(){
                $(this).click(function(){
                    $('.leftMenu_systemSetting .subItems li').removeClass('thirdMenu_click');
                    $(this).addClass('thirdMenu_click');
                });
            });
        },
        
        
        /**
         * 判断一级菜单是否需要上下移动
         */
        isFirstMenuMove : function() {
            var $firstItems = $('#leftMenu_first .leftMenu_first_items');
            if ($firstItems.height() > $('#index_leftMenu').height()) {
                $('#leftMenu_menuDown').show();
            } else {
                $firstItems.css('top' , '0px');
                $('#leftMenu_menuDown').hide();
                $('#leftMenu_menuUp').hide();
            }
        },
        
        
        /**
         * @description 判断是否需要折叠
         */
        isSecondMenuScroll : function() {
            if (!!!leftMenu.menuProp.secondMenuId){
                return;
            }
            
            var $secondItems = $('#leftMenu_second_' + leftMenu.menuProp.secondMenuId + ' .leftMenu_second_items'); // 二级菜单项
            
            var panelHeight = $('#index_body').layout('panel', 'west').panel('options').height;
            
            if ($secondItems.height() > panelHeight) {
                $('#leftMenu_second').css({"overflow-y":"scroll"});
            } else {
                $('#leftMenu_second').css({"overflow-y":"hidden"});
            }
        },
        
        
        /**
         * @description 向上滚动
         */
        menuUp : function() {
            var $firstItems = $('#leftMenu_first .leftMenu_first_items');
            
            var oldTop = parseInt($firstItems.css('top'));
            if (oldTop < 0) {
                var moveTop = 5;
                var newTop = oldTop + moveTop;
                if (newTop > 0) {
                    newTop = 0;
                }
                $firstItems.css({'top': newTop + 'px'});
                $('#leftMenu_menuDown').show();
            } else {
                $('#leftMenu_menuUp').hide();
            }
        },
        
        
        /**
         * @description 向下滚动
         */
        menuDown : function () {
            var $firstItems = $('#leftMenu_first .leftMenu_first_items');
            var oldTop = parseInt($firstItems.css('top'));
            if(($firstItems.height() + oldTop) > $('#index_leftMenu').height()) {
                var moveTop = 5;
                var newTop = oldTop - moveTop;
                $firstItems.css({'top': newTop + 'px'});
                $('#leftMenu_menuUp').show();
            } else {
                $('#leftMenu_menuDown').hide();
            }
        },
        
        
        /**
         * @description 动态改变左菜单大小
         * @param width 改变的宽度
         */
        resizeRegWest : function(width) {
            leftMenu.menuProp.leftMenuWidth = width;
            $('#index_body').layout('panel', 'west').panel('resize',{
                width: width
            });
            
            $('#index_body').layout('resize');
        }
        
        
    };
}();

$(function(){
    leftMenu.loadMenu();
    leftMenu.initMenuHeight();
    leftMenu.initFirstMenuMove();
    leftMenu.isFirstMenuMove();
});
window.onresize = function(){
    leftMenu.initMenuHeight();
    leftMenu.isFirstMenuMove();
    leftMenu.isSecondMenuScroll();
};