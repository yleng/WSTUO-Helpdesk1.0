$package("common.config.userCustom");
/**  
 * @author QXY  
 * @constructor users
 * @description 门户面板管理
 * @date 2011-10-31
 * @since version 1.0 
 */
common.config.userCustom.dashboardManage=function(){
	this.optType='';
	return {
		/**
		 * 面板列表
		 */
		dashboardGrid:function(){
			var params = $.extend({},jqGridParams, {	
				url:'dashboard!findPageDashboard.action',
				colNames:['ID',i18n['label_dashboard_name'],i18n['label_dashboard_divId'],i18n['sort'],i18n['label_dashboard_defautl_show'],''],
			 	colModel:[
			 	          {name:'dashboardId',align:'center',width:30},
			 	          {name:'dashboardName',align:'left',width:80},
			 	          {name:'dashboardDivId',align:'left',width:80},
			 	          {name:'sortNo',align:'center',width:80},
			 	          {name:'defaultShow',align:'center',width:50,formatter:common.config.userCustom.dashboardManage.defaultShowFormt},
			 	          {name:'defaultShow',align:'center',hidden:true}
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id:"dashboardId"}),
				sortname:'sortNo',
				sortorder:'asc',
				pager:'#dashboardPager'
				});
				$("#dashboardGrid").jqGrid(params);
				$("#dashboardGrid").navGrid('#dashboardPager',navGridParams);
				//列表操作项
				$("#t_dashboardGrid").css(jqGridTopStyles);
				$("#t_dashboardGrid").append($('#dashbardOptItem').html());
				
				//自适应宽度
				setGridWidth("#dashboardGrid","regCenter",20);
		},
		/**
		 * 默认显示格式化
		 * @param cell 当前列值
		 */
		defaultShowFormt:function(cellvalue){
			if(cellvalue==1)
				return '<span style="color:red">'+i18n['label_basicConfig_deafultCurrencyYes']+'</span>'
			else
				return i18n['label_basicConfig_deafultCurrencyNo']
		},
		
		/**
		 * 新增面板
		 */
		addDashboard:function(){
			optType="save";
			$('#dashboard_dashboardId').val('');
			resetForm('#dashbardAddEditForm');
			windows('dashbardAddEditDiv',{width:450});
		},
		/**
		 * 编辑面板
		 */
		editDashboard:function(){
			optType="edit";
			checkBeforeEditGrid('#dashboardGrid',common.config.userCustom.dashboardManage.dashboardEditValue);
		},
		/**
		 * 打开编辑面板
		 * @param  rowData 行数据
		 */
		dashboardEditValue:function(rowData){
			$('#dashboard_dashboardId').val(rowData.dashboardId);
			$('#dashboard_dashboardName').val(rowData.dashboardName);
			$('#dashboard_dashboardDivId').val(rowData.dashboardDivId);
			$('#dashboard_dashboardDataLoadUrl').val(rowData.dashboardDataLoadUrl);
			$('#dashboard_sortNo').val(rowData.sortNo);
			if(rowData.defaultShow==1 || rowData.defaultShow=='1'){
				$('#dashboard_defaultShow1').attr('checked',true)
			}else{
				$('#dashboard_defaultShow2').attr('checked',true)
			}
			windows('dashbardAddEditDiv',{width:450});
		},
		/**
		 * 保存面板
		 */
		saveDashboard:function(){
			if($('#dashbardAddEditDiv form').form('validate')){
				startProcess();
				var _frm=$('#dashbardAddEditDiv form').serialize();
				$.post('dashboard!'+optType+'Dashboard.action',_frm,function(){
					endProcess();
					$('#dashboardGrid').trigger('reloadGrid');
					msgShow(i18n['saveSuccess'],'show');
					$('#dashbardAddEditDiv').dialog('close');
				})
			}
		},
		/**
		 * 删除面板是否选中
		 */
		deleteDashboard:function(){
			checkBeforeDeleteGrid('#dashboardGrid',common.config.userCustom.dashboardManage.deleteDashboardMethod);
		},
		/**
		 * @description  进行删除面板
		 * @param  rowIds 行编号
		 */
		deleteDashboardMethod:function(rowsId){
			var url='dashboard!deteleDashboard.action';
			var param = $.param({'ids':rowsId},true);
			$.post(url, param, function(){
				//重新统计				
				$('#dashboardGrid').trigger('reloadGrid');
				msgShow(i18n['msg_deleteSuccessful'],'show');
			});	
		},
		/**
		 * 打开搜索面板框
		 */
		dashboardSearch:function(){
			windows('dashbardSearchDiv',{width:400});
		},
		/**
		 * 搜索面板
		 */
		dashboarDoSearch:function(){
			var sdata=$('#dashbardSearchDiv form').getForm();
			var postData = $("#dashboardGrid").jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);  
			var _url = 'dashboard!findPageDashboard.action';		
			$('#dashboardGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid',[{"page":"1"}]);
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			$("#dashboardManage_loading").hide();
			$("#dashboardManage_content").show();
			common.config.userCustom.dashboardManage.dashboardGrid();
			$('#link_dashboard_add').click(common.config.userCustom.dashboardManage.addDashboard);
			$('#link_dashboard_edit').click(common.config.userCustom.dashboardManage.editDashboard);
			$('#dashboard_save').click(common.config.userCustom.dashboardManage.saveDashboard);
			$('#link_dashboard_delete').click(common.config.userCustom.dashboardManage.deleteDashboard);
			$('#link_dashboard_search').click(common.config.userCustom.dashboardManage.dashboardSearch);
			$('#dashboard_dosearch').click(common.config.userCustom.dashboardManage.dashboarDoSearch);
			
		}
	}
}();
$(document).ready(common.config.userCustom.dashboardManage.init);