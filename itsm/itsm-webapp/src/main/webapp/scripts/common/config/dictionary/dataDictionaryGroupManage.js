 /**  
 * @author QXY  
 * @constructor dictionaryGroup
 * @description 数据字典分组管理
 * @date 2010-11-17
 * @since version 1.0 
 */ 
var dictionaryGroup = function(){
	this.operator;
	/**
	 * @description 数据字典分组列表
	 * 
	 */
	this.show_grid=function(){		
		$('#dataDictionaryGroupsGrid').jqGrid({				
			url:'dataDictionaryGroup!find.action',	//返回PageDTO的URL	
			mtype:'post',
			datatype:'json',			
			autowidth:true,
			height:'auto',
			colNames:['ID',i18n['name'],i18n['code'],''],
			colModel:[
					{name:'groupNo',width:80,sortable:true,align:'center'},					
					{name:'groupName',width:100,sortable:true},			
					{name:'groupCode',width:100,sortable:true},
					{name:'dataFlag',hidden:true}	
			    ],
			    viewrecords:true,
			    multiselect: true,
			    jsonReader: {
					root: "data",
					records: "totalSize",
					page: "page",
					total: "total",
					repeatitems : false,
					id: "groupNo"			
		     },		    	     
		     rowNum:15,
		     rowList:[5,10,15,20],
		     toolbar: [true,"top"],
		     pager:'#dataDictionaryGroupsPager',
		     hidegrid:false
		})
		.navGrid('#dataDictionaryGroupsPager',{"edit":false,"add":false,"del":false,"search":false,"view":false,"refresh":true});
		$('#t_dataDictionaryGroupsGrid')
		.css("padding-top","5px")
		.css("padding-bottom","10px")
		.css("border-top","none")
		.css("background","#eee")
		.css("text-align","left");
		$('#t_dataDictionaryGroupsGrid').append($('#dataDictionaryGroupsToolbar').html());
	}	
	/**
	 * 添加数据字典分组
	 */
	this.addDictionaryItem=function(){
		windows('addDataDictionaryGroupDiv',{title:i18n['newAdd'],width:400,height:180});
		operator = 'saveGroup';
	}
	/**
	 * 添加数据字典分组
	 */
	this.searchDictionaryItem=function(){
		windows('searchDataDictionaryGroupDiv',{width:400,height:150,modal: false});
	}
	 /**
	  * @description 新增数据字典组
	  * */
	this.saveDictionaryGroups = function(){		
		if($('#addDataDictionaryGroupDiv').form('validate')){
			var bb = $('#addDataDictionaryGroupDiv form').serialize();
			var url = "dataDictionaryGroup!"+operator+".action";
			var _groupNo=$('#add_groupNo').val();
			$.post(url, bb, function(){
				$('#addDataDictionaryGroupDiv').dialog('close');
				$('#dataDictionaryGroupsGrid').trigger('reloadGrid');
				//清空框内内容
				$('#addDataDictionaryGroupDiv [name="ddgDto.groupCode"]').val("");
				$('#addDataDictionaryGroupDiv [name="ddgDto.groupName"]').val("");
				$('#addDataDictionaryGroupDiv [name="ddgDto.groupNo"]').val("");
				msgShow(i18n['common_operation_success'],'info');
			});	
		}
	}
   /**	

    * @description 删除数据字典组
    * */
	this.delDictionaryGroups = function(){		
		var rowIds = $('#dataDictionaryGroupsGrid').jqGrid('getGridParam','selarrrow');
		if(rowIds==""){
			msgAlert(i18n['selectDeleteDataTip'],'info');
		}else{
			var _param = $.param({'groupNo':rowIds},true);
			//条件的判断是否删除
			   if(_param != null && _param != '') {
				   for ( var _int = 0; _int < rowIds.length; _int++) {
					   var data = $('#dataDictionaryGroupsGrid').getRowData(rowIds[_int]);
					   if(data.dataFlag==1){
						   msgAlert(i18n['ERROR_SYSTEM_DATA_CAN_NOT_DELETE'],'info');
						   return;
					   }
				   }
			    	msgConfirm(i18n['tip'], '<br/>'+i18n['deleteComfirm'], function(){
			    		$.post("dataDictionaryGroup!removeGroup.action", _param, function(){
							$('#dataDictionaryGroupsGrid').trigger('reloadGrid');
							msgShow(i18n['deleteSuccess'],'info');
						}, "json");
			    	});
			   }
		}
	}
	 /**
	  * @description 编辑数据字典组
	  * */
	this.editDictionaryGroups = function() {		
		checkBeforeEditGrid('#dataDictionaryGroupsGrid',function(data){
			if(data.dataFlag==1){
				msgAlert(i18n['ERROR_SYSTEM_DATA_CAN_NOT_EDIT'],'info');
				return;
			}
			operator = 'mergeGroup';	
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupCode"]').val(data.groupCode);
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupName"]').val(data.groupName);
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupNo"]').val(data.groupNo);
			windows('addDataDictionaryGroupDiv',{title:i18n['edit'],width:400,height:180});
	   });
	}
	 /**
	  * @description 搜索数据字典组
	  * */
	this.searchDictionaryGroups = function() {	
		var _url = 'dataDictionaryGroup!find.action';
		var sdata = $('#searchDataDictionaryGroupDiv form').getForm();
		var postData = $("#dataDictionaryGroupsGrid").jqGrid("getGridParam", "postData");     
		$.extend(postData, sdata);
		$('#dataDictionaryGroupsGrid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		return false;
	}
	/**
	 * 导出数据字典组
	 */
	this.exportDataDictionaryGroup=function(){
		$("#searchDataDictionaryGroupDiv form").submit();
	}
	return {
		/**
		 * 初始化
		 * @private
		 */
		init: function(){
			$('.loading').hide();
			$('.content').show();
			//加载数据列表
			show_grid();
			//创建事件
			$('#link_dataDictionary_add_ok').click(saveDictionaryGroups);
			$('#link_dataDictionary_search_ok').click(searchDictionaryGroups);
			$('#dataDictionaryGroupManage_export').click(exportDataDictionaryGroup);
		}
	}
}();
$(document).ready(dictionaryGroup.init);