$package('common.config.onlinelog');
$import("common.security.userUtil");
$import('common.security.defaultCompany');
$import('itsm.itsop.selectCompany');
$import('common.report.defaultTimeAndBind');
/**  
 * @author Van  
 * @constructor WSTO
 * @description 用户操作日志.
 * @date 2010-11-17
 * @since version 1.0 
 */
common.config.onlinelog.userOptLog=function(){
	 return {
		 /**
		  * 执行时间格式化
		  * @param cellvalue 当前列值
		  */
		 execTimeFormatter:function(cellvalue){
			 return cellvalue+"ms"
		 },
		 /**
		  * 资源信息格式化
		  * @param resourceName 名称
		  */
		 i18nResourceName:function(resourceName){
			 if('delete'==resourceName){
				 resourceName="deletes";
			 }
			 var i18nName=i18n[resourceName];
			 if(i18nName==undefined)
				 i18nName=resourceName
			 return i18nName;
		 },
		 /**
		  * 操作项格式化
		  * @param methodName 方法名
		  */
		 i18nMethodName:function(methodName){
			 if('delete'==methodName){
				 methodName="deletes";
			 }
			 var i18nName=i18n[methodName];
			 if(i18nName==undefined)
				 i18nName=methodName
			 return i18nName;
		 },
		 downloadOptFile:function(fileName){
			 location.href="log!downloadOptLog.action?fileName="+fileName;
		 }
		 ,
		 lnkName:function(cellvalue,options,rowOjbect){
			 return '<a href="javascript:common.config.onlinelog.userOptLog.downloadOptFile(\''+cellvalue+'\')"><img src="../images/icons/arrow_down_blue.gif"></a>';
		 }
		 ,
		 /**
		  * 查看用户操作日志列表
		  */
		 showUserOptLogGrid:function(){
				var params = $.extend({},jqGridParams, {	
					url:'log!userOptPage.action',
					colNames:['ID',i18n['title_backup_fileName'],i18n['title_backup_fileSize'],i18n['common_createTime'],i18n['common_download']],
					colModel:[
					          {name:'id',width:30,align:'center',sortable:false},
					          {name:'fileName',width:80,align:'center',sortable:false,formatter:function(cellvalue,options,rowOjbect){
									 return '<a href="javascript:common.config.onlinelog.userOptLog.downloadOptFile(\''+cellvalue+'\')">'+cellvalue+'</a>';
								 }},
							  {name:'fileSize',width:80,align:'center',sortable:false},
							  {name:'time',width:100,align:'center',sortable:false},
							  {name:'fileName',width:80,align:'center',sortable:false,formatter:common.config.onlinelog.userOptLog.lnkName}
							  ],
					jsonReader: $.extend(jqGridJsonReader, {id: "id"}),
					sortname:'id',
					multiselect:false,
					pager:'#userOptGridPager'
				});
				$("#userOptGrid").jqGrid(params);
				$("#userOptGrid").navGrid('#userOptGridPager',navGridParams);
				//列表操作项
				$("#t_userOptGrid").css(jqGridTopStyles);
				$("#t_userOptGrid").append($('#userOptGridToolbar').html());
				//自适应宽度
				setGridWidth("#userOptGrid","regCenter",20);
		},
		 /**
		  * @description 搜索操作日志
		  */
		searchUserOptLog_do:function(){				
			if($('#searchUserOptLog form').form('validate')){
				//获得表单对象
				var sdata = $('#searchUserOptLog form').getForm();		
				var postData = $('#userOptGrid').jqGrid('getGridParam', 'postData');       
				$.extend(postData, sdata);
				var _url ="useroptlog!findPager.action";	
				$('#userOptGrid').jqGrid('setGridParam',{url:_url,page:1}).trigger('reloadGrid');
			}
	
		},
		/**
		 * 刷新用户操作日志
		 */
		refreshUserOptLogjqGrid:function(){
			$('#user_Name').val("");
			$('#method_Name').val("");
			$('#startTime').val("");
			$('#endTime').val("");
			//获得表单对象
			var sdata = $('#searchUserOptLog form').getForm();		
			var postData = $('#userOptGrid').jqGrid('getGridParam', 'postData');       
			$.extend(postData, sdata); 
			var _url ="useroptlog!findPager.action";	
			$('#userOptGrid').jqGrid('setGridParam',{url:_url,page:1}).trigger('reloadGrid');
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			 $("#UserOptLog_loading").hide();
			 $("#UserOptLog_content").show();
			 common.config.onlinelog.userOptLog.showUserOptLogGrid();//加载列表
			 $("#userOptGrid_search").click(function(){
				 bindControl('#userOptLog_search_startTime,#userOptLog_search_endTime');
				 windows('searchUserOptLog',{width:450,close:function(){
					 $('#searchUserOptLog').click();
				 }});
			 });
			 $("#userOptGrid_download").click(function(){
					//bindControl('#optZipStartTime,#optZipEndTime');
					windows('zipUserOptLog',{width:450});
				});
				$("#userOptGrid_downloadzip").click(function(){
					startProcess();
					var startTime = $("#optZipStartTime").val();
					var endTime = $("#optZipEndTime").val();
					$.post("log!multiZip.action","zipType=optLog&startTime="+startTime+"&endTime="+endTime,function(data){
						if(data){
							location.href="log!downloadMultiZip.action?fileName="+data;
							endProcess();
						}
					});
				});
			 
			 //选择用户
			 $('#user_Name_select').click(function(){
				 common.security.userUtil.selectUser('#user_Name','','','loginName','-1');
			 });
			 $("#userOptGrid_doSearch").click(common.config.onlinelog.userOptLog.searchUserOptLog_do);
			//选择公司
			$('#userOptLog_search_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#userOptLog_search_companyNo','#userOptLog_search_companyName','','all');
			});
			//加载默认公司
			common.security.defaultCompany.loadDefaultCompany('#userOptLog_search_companyNo','#userOptLog_search_companyName');
		}
	 }
 }();
//载入
 $(document).ready(common.config.onlinelog.userOptLog.init);