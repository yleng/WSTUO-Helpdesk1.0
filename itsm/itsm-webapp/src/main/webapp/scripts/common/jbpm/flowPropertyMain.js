$package('common.jbpm');
$import("common.config.dictionary.dataDictionaryUtil");
$import("common.tools.noticeRule.noticeGrid");
$import("common.config.category.serviceOrgUtils");
 /**  
 * @author QXY  
 * @constructor flowPropertyMain
 * @description 流程属性设置
 * @date 2012-11-15
 * @since version 1.0 
 **/
common.jbpm.flowPropertyMain = function(){
	this.loadshowRulesGrid=false;
	return {
		/**
		 * @description 格式化节点名称
		 * @param value 流程节点名称
		 **/
		noteTypeFormat:function(value){
			if(value=='state')
				return 'State';
			if(value=='task')
				return 'Task';
			if(value=='foreach')
				return 'Foreach';
			if(value=='join')
				return 'Join';
			if(value=='decision')
				return 'Decision';
			if(value=='end')
				return 'End';
			else
				return 'Other';
		},
		/**
		 * @description 加载流程
		 **/
		loadFlowPropertyMainPage:function(){
			$('#flowNodeShow table tbody').html('');
			
			$.post('flowProperty!findFlowPropertyByProcessDefinitionId.action','processDefinitionId='+processDefinitionId,function(data){
				$('#flowPropertyMain_flow_key').val(data.processDefinitionId);//设置流程Key
				$('.flowProperty_approverTask_tr').hide();
				if(data!=null && data.flowActivityDTO!=null){
					var executionId = data.processDefinitionId;
					//判断当前流程是属性那个模块的
					if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
						$('#processType').text(i18n['change']);
						common.jbpm.flowPropertyMain.loadProcessAction('change');
						common.jbpm.flowPropertyMain.loadModuleValidateMethod('change');
						common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('changeStatus','#flowProperty_status');
						common.jbpm.flowPropertyMain.noticeRuleInputClick('change');
						$('.flowProperty_approverTask_tr').show();
						$('.showMatchRule_tr1').show();
						$('.showMatchRule_tr2').show();
						$('#flowProperty_matchRule').click(function(){
							windows('flowPropertyMatchRuleSelectWin',{width:530,maxHeight:480,padding:3});
							if(loadshowRulesGrid==false){
								loadshowRulesGrid=true;
								common.jbpm.flowPropertyMain.showRulesGrid("changeProce");//加载规则列表
							}else{
								$('#matchRuleSelect_grid').trigger('reloadGrid',[{"page":"1"}]);
							}
						});
						$('#matchRuleSelect_grid_select').click(function(){
							common.jbpm.flowPropertyMain.selectRulesGrid();
						});
					}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
						$('#processType').text(i18n['problem']);
						common.jbpm.flowPropertyMain.loadProcessAction('problem');
						common.jbpm.flowPropertyMain.loadModuleValidateMethod('problem');
						common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('problemStatus','#flowProperty_status');
						common.jbpm.flowPropertyMain.noticeRuleInputClick('problem');
					}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
						$('#processType').text(i18n['release']);
						common.jbpm.flowPropertyMain.loadModuleValidateMethod('release');
						common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('releaseStatus','#flowProperty_status');
						common.jbpm.flowPropertyMain.noticeRuleInputClick('release');
					}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
						$('#processType').text(i18n['request']);
						common.jbpm.flowPropertyMain.loadProcessAction('request');
						common.jbpm.flowPropertyMain.loadModuleValidateMethod('request');
						common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('requestStatus','#flowProperty_status');
						common.jbpm.flowPropertyMain.noticeRuleInputClick('request');
						$('.showMatchRule_tr1').show();
						$('.showMatchRule_tr2').show();
						$('#flowProperty_matchRule').click(function(){
							windows('flowPropertyMatchRuleSelectWin',{width:530,maxHeight:480,padding:3});
							if(loadshowRulesGrid==false){
								loadshowRulesGrid=true;
								common.jbpm.flowPropertyMain.showRulesGrid("requestProce");//加载规则列表
							}else{
								$('#matchRuleSelect_grid').trigger('reloadGrid',[{"page":"1"}]);
							}
						});
						$('#matchRuleSelect_grid_select').click(function(){
							common.jbpm.flowPropertyMain.selectRulesGrid();
						});
					}
					var flowActivityDTO = data.flowActivityDTO;
					var k = 1;
					for(var i=0 ; i<flowActivityDTO.length; i++){
						//只设置任务类型的节点
						if(flowActivityDTO[i].activityType=='task' || flowActivityDTO[i].activityType=='foreach'){
							//节点Start
							var _flowActivity = "<tr class='flowSelect' id='activity_tr_"+flowActivityDTO[i].id+"' style='cursor:pointer' onclick=javascript:common.jbpm.flowPropertyMain.loadPropertyInfo('"+flowActivityDTO[i].id+"')>" +
									"<td style='text-align:left'><a href=javascript:common.jbpm.flowPropertyMain.loadPropertyInfo('"+flowActivityDTO[i].id+"')>" +
									"<img src='../images/icons/navigate_right_10.gif' />"+k+".{activityName}:["+common.jbpm.flowPropertyMain.noteTypeFormat(flowActivityDTO[i].activityType)+"]</a></td>" +
									"<td style='text-align:left'>{transitionName}</td>" +
									"</tr>";
							_flowActivity = _flowActivity.replace('{activityName}',flowActivityDTO[i].activityName)
							//节点End
							
							//出口Start
							var transition = "";
							var flowTransitionDto = data.flowActivityDTO[i].flowTransitionDto;
							if(flowTransitionDto!=null){
								
								for(var j=0 ; j<flowTransitionDto.length; j++){
									var _flowTransition = "@{transition}<br>";
									if(flowTransitionDto[j].transitionName==null || flowTransitionDto[j].transitionName == '')
										transition = transition+_flowTransition.replace('{transition}','')
									else
										transition = transition+_flowTransition.replace('{transition}',flowTransitionDto[j].transitionName)
								}
							}
							_flowActivity = _flowActivity.replace('{transitionName}',transition);
							//出口End
							
							k++;
							$('#flowNodeShow table tbody').append(_flowActivity);
						}
						
					}
				}
			});
		},
		/**
		 * @description 加载规则列表.
		 **/
		showRulesGrid:function(flagName){
			var _postData={};
			$.extend(_postData,{'rulePackageDto.flagName':flagName});
			var params = $.extend({},jqGridParams, {	
				url:'rulePackage!findRulePackageByPage.action',
				postData:_postData,
				colNames:['ID',
				          i18n['lable_created_drl_processing'],
				          i18n['lable_Rules_package_name']
				],
			 	colModel:[
			 	          {name:'rulePackageNo',align:'center',width:20},
			 	          {name:'packageName',align:'center',width:50,formatter:function(cellvalue, options, rowObject){
							 return cellvalue.replace("com.drools.drl.","");
						  }},
			 	          {name:'rulePackageRemarks',align:'center',width:80}
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id: "rulePackageNo"}),
				sortname:'rulePackageNo',
				toolbar:true,
				pager:'#matchRuleSelect_gridPager'
			});
			$("#matchRuleSelect_grid").jqGrid(params);
			$("#matchRuleSelect_grid").navGrid('#matchRuleSelect_gridPager',navGridParams);
		},
		/**
		 * @description 选择规则列表.
		 **/
		selectRulesGrid:function(){
			var rowIds = $("#matchRuleSelect_grid").getGridParam('selarrrow');
			if(rowIds==''){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				if(rowIds.length>0){
					for ( var i = 0; i<rowIds.length; i++) {
						var RuleData=$("#matchRuleSelect_grid").getRowData(rowIds[i]);
						if($('#showMatchRule #matchRuleId_'+RuleData.packageName).length==0){
							$('#showMatchRule').append('<div id="matchRuleId_'+RuleData.packageName+'">'+RuleData.packageName+
									'<input name="matchRuleId" type="hidden" value="'+RuleData.packageName+'" />'+
									'<input type="hidden" value="'+RuleData.packageName+'" name="flowActivityDTO.matchRuleName"/>'+
									'&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:red;" onclick="common.jbpm.flowPropertyMain.matchRuleRemove(\''+RuleData.packageName+'\')">X</a>'+
									'<div>');
						}
					}
				}
				$('#flowPropertyMatchRuleSelectWin').dialog('close');
			}
		},
		/**
		 * 移除匹配规则
		 * @param id 规则Id
		 */
		matchRuleRemove:function(id){
			$('#matchRuleId_'+id).remove();
		},
		/**
		 * 规则名称格式化
		 */
		dataFlagFormatter:function(cell,event,data){
			
			if(data.dataFlag==1){
				return "<span style='color:#ff0000'>[System]</span>&nbsp;"+data.ruleName;
			}else{
				return data.ruleName;
			}
		},
		/**
		 * 弹出加载规则列表框
		 * @param type 对应模块
		 */
		noticeRuleInputClick:function(type){
			$('#flowProperty_noticeRule').click(function(){
				windows('flowPropertyNoticeRuleSelectWin',{width:530,maxHeight:480,padding:3});
				common.jbpm.flowPropertyMain.loadNoticeModelGridCheckbox(type);
			});
		},
		/**
		 * @description 加载流程属性信息
		 * @param id 流程id
		 **/
		loadPropertyInfo:function(id){
			$('.flowSelect td').css({"background":"#fff"});
			$('#activity_tr_'+id+' td').css({"background":"#ffef8f"});
			$(":checkbox[name='flowActivityDTO.taskActions']").attr('checked',false);
			$(":checkbox[name='flowActivityDTO.roleCodes']").attr('checked',false);
			$('#showNoticeRule').html('');
			$('#showMatchRule').html('');
			$.post('flowProperty!findFlowActivityById.action','flowActivityDTO.id='+id,function(data){
				$('#flowProperty_Id').val(data.id);
				$('#flowProperty_groupNo').val($vl(data.groupNo));
				$('#flowProperty_groupName').val($vl(data.groupName));
				$('#flowProperty_assigneeNo').val($vl(data.assigneeNo));
				$('#flowProperty_assigneeName').val($vl(data.assigneeName));
				$('#flowProperty_variablesValueType').val(data.variablesAssigneeType);
				$('#flowProperty_leaderNum').val(data.leaderNum);
				$('#flowProperty_variablesAssigneeGroupNo').val($vl(data.variablesAssigneeGroupNo));
				$('#flowProperty_variablesAssigneeGroupName').val($vl(data.variablesAssigneeGroupName));
				$('#flowProperty_candidate_GroupsNo').val($vl(data.candidateGroupsNo));
				$('#flowProperty_candidate_GroupsName').val($vl(data.candidateGroupsName));
				$('#flowProperty_candidate_UsersNo').val($vl(data.candidateUsersNo));
				$('#flowProperty_candidate_UsersName').val($vl(data.candidateUsersName));
				if(data.isUpdateEventAssign){
					$('#flowProperty_isUpdateEventAssign').attr('checked',true);
				}else{
					$('#flowProperty_isUpdateEventAssign').attr('checked',false);
				}
				if(data.allowUseDynamicAssignee){
					$('#flowProperty_allowUseDynamicAssignee').attr('checked',true);
				}else{
					$('#flowProperty_allowUseDynamicAssignee').attr('checked',false);
				}
				if(data.allowUseVariablesAssignee){
					$('#flowProperty_allowUseVariablesAssignee').attr('checked',true);
				}else{
					$('#flowProperty_allowUseVariablesAssignee').attr('checked',false);
				}
				if(data.taskActions!=null){
					var taskAction = data.taskActions;
					for(i = 0 ;i<taskAction.length;i++){
						$('#flowProperty_taskActions_'+taskAction[i]).attr('checked',true);
					}
				}
				if(data.dynamicAssignee){
					$('#flowProperty_dynamicAssignee').attr('checked',true);
				}else{
					$('#flowProperty_dynamicAssignee').attr('checked',false);
				}
				if(data.eventAssign){
					$('#flowProperty_eventAssign').attr('checked',true);
				}else{
					$('#flowProperty_eventAssign').attr('checked',false);
				}
				if(data.remarkRequired){
					$('#flowProperty_remarkRequired').attr('checked',true);
				}else{
					$('#flowProperty_remarkRequired').attr('checked',false);
				}
				//邮件处理流程
				if(data.mailHandlingProcess){
					$('#flowProperty_mailHandlingProcess').attr('checked',true);
				}else{
					$('#flowProperty_mailHandlingProcess').attr('checked',false);
				}
				//回退给上次任务指派的技术员
				if(data.isFallbackToTechnician){
					$('#flowProperty_isFallbackToTechnician').attr('checked',true);
				}else{
					$('#flowProperty_isFallbackToTechnician').attr('checked',false);
				}
				$('#flowProperty_validMethod').val($vl(data.validMethod));
				$('#flowProperty_formName').val($vl(data.formName));
				
				common.jbpm.flowPropertyMain.assignType(data.assignType);
				if(data.roleCodes!=null){
					var roleCode = data.roleCodes;
					for(i = 0 ;i<roleCode.length;i++){
						$('#flowActivityDTO_roleCodes_'+roleCode[i]).attr('checked',true);
					}
				}
				//状态
				$('#flowProperty_status').val($vl(data.statusNo));
				//任务完成后的通知规则
				if($vl(data.noticeRuleIds)!=''){
					var _noticeRuleIds = $vl(data.noticeRuleIds).split(',');
					var _noticeRule = $vl(data.noticeRule).split(',');
					$('#flowProperty_noticeRuleId').val($vl(data.noticeRuleIds));
					$('#flowProperty_noticeRule').val($vl(data.noticeRule));
					var noticeRuleId = '';
					var noticeRuleName = '';
					for ( var i = 0; i<_noticeRuleIds.length; i++) {
						noticeRuleId = trim(_noticeRuleIds[i]);
						noticeRuleName = trim(_noticeRule[i]);
						if(_noticeRuleIds[i]!='' && $('#showNoticeRule #noticeRuleId_'+noticeRuleId).length==0){
							$('#showNoticeRule').append('<div id="noticeRuleId_'+noticeRuleId+'">'+noticeRuleName+
									'<input name="noticeRuleId" type="hidden" value="'+noticeRuleId+'" />'+
									'<input name="flowActivityDTO.noticeRule" type="hidden" value="'+noticeRuleName+'" />'+
									'&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:red;" onclick="common.jbpm.flowPropertyMain.noticeRuleRemove('+noticeRuleId+')">X</a>'+
									'<div>');
						}
					}
				}
				if($vl(data.matchRuleIds)!=''){
					var _matchRuleIds = $vl(data.matchRuleIds).split(',');
					var _matchRuleName = $vl(data.matchRuleName).split(',');
					var matchRuleId = '';
					var matchRuleName = '';
					for ( var i = 0; i<_matchRuleIds.length; i++) {
						matchRuleId = trim(_matchRuleIds[i]);
						matchRuleName = trim(_matchRuleName[i]);
						if(_matchRuleIds[i]!='' && $('#showMatchRule #matchRuleId_'+matchRuleId).length==0){
							$('#showMatchRule').append('<div id="matchRuleId_'+matchRuleId+'">'+matchRuleName+
									'<input name="matchRuleId" type="hidden" value="'+matchRuleId+'" />'+
									'<input name="flowActivityDTO.matchRuleName" type="hidden" value="'+matchRuleName+'" />'+
									'&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:red;" onclick="common.jbpm.flowPropertyMain.matchRuleRemove(\''+matchRuleId+'\')">X</a>'+
									'<div>');
						}
					}
				}
				//审批任务
				if(data.approverTask){
					$('#flowProperty_approverTask').attr('checked',true);
				}else{
					$('#flowProperty_approverTask').attr('checked',false);
				}
				//允许修改默认指派
				if(data.allowUdateDefaultAssignee){
					$('#flowProperty_allowUdateDefaultAssignee').attr('checked',true);
				}else
					$('#flowProperty_allowUdateDefaultAssignee').attr('checked',false);
				//允许修改默认指派组
				if(data.allowUdateDefaultAssigneeGroup){
					$('#flowProperty_allowUdateDefaultAssigneeGroup').attr('checked',true);
				}else
					$('#flowProperty_allowUdateDefaultAssigneeGroup').attr('checked',false);
				//通知任务指派用户
				if(data.taskAssigneeUserNotice){
					$('#flowProperty_taskAssigneeUserNotice').attr('checked',true);
				}else
					$('#flowProperty_taskAssigneeUserNotice').attr('checked',false);
				//通知任务指派组组员
				if(data.taskAssigneeGroupNotice){
					$('#flowProperty_taskAssigneeGroupNotice').attr('checked',true);
				}else
					$('#flowProperty_taskAssigneeGroupNotice').attr('checked',false);
				//通知指定用户
				$('#flowProperty_noticSpecifiedUser').val($vl(data.noticSpecifiedUser));
				//通知指定邮箱
				$('#flowProperty_noticSpecifiedEmail').val($vl(data.noticSpecifiedEmail));
			});
			
		},
		savePropertyInfoByDynamicAssignee:function(_param){
			if(document.getElementById("flowProperty_allowUdateDefaultAssignee").checked || document.getElementById("flowProperty_allowUdateDefaultAssigneeGroup").checked ){
				startProcess();
				$.post('flowProperty!updateFlowActivity.action',_param,function(){
					msgShow(i18n['common_operation_success'],'show');
					endProcess();
				});
			}else{
				if($('#flowProperty_groupNo').val()!=''||$('#flowProperty_assigneeNo').val()!=''){
					startProcess();
					$.post('flowProperty!updateFlowActivity.action',_param,function(){
						msgShow(i18n['common_operation_success'],'show');
						endProcess();
					});
				}else{
					msgAlert(i18n['msg_request_assignCanNotNull'],'info');
				}
			}
		},
		savePropertyInfoByVariablesAssignee:function(_param){
			if($("#flowProperty_variablesValueType").val()!=""){
				if($("#flowProperty_variablesValueType").val()!="groupHead"){
					startProcess();
					$.post('flowProperty!updateFlowActivity.action',_param,function(){
						msgShow(i18n['common_operation_success'],'show');
						endProcess();
					});
				}else{
					if($("#flowProperty_variablesAssigneeGroupName").val()!=""){
						startProcess();
						$.post('flowProperty!updateFlowActivity.action',_param,function(){
							msgShow(i18n['common_operation_success'],'show');
							endProcess();
						});
					}else{
						msgAlert(i18n.lable_bpm_selectAssigneeGroup,'info');
					}
				}
			}else{
				msgAlert(i18n.lable_bpm_selectAssignee,'info');
			}
		},
		/**
		 * @description 保存属性信息
		 **/
		savePropertyInfo:function(){
			if($('#flowPropertyForm').form('validate')){
				if($('#flowProperty_Id').val()!=''){
					var _noticeRuleIds = '';
					$("#showNoticeRule input[name=noticeRuleId]").each(function(k,v){
						if(_noticeRuleIds==''){
							_noticeRuleIds = v.value;
						}else{
							_noticeRuleIds =_noticeRuleIds+','+v.value;
						}
					});
					var _param = $('#flowPropertyForm').serialize()+'&flowActivityDTO.noticeRuleIds='+_noticeRuleIds;
					var _matchRuleIds = '';
					$("#showMatchRule input[name=matchRuleId]").each(function(k,v){
						if(_matchRuleIds==''){
							_matchRuleIds = v.value;
						}else{
							_matchRuleIds =_matchRuleIds+','+v.value;
						}
					});
					_param = _param+'&flowActivityDTO.matchRuleIds='+_matchRuleIds;
					
					if(($("#flowProperty_candidate_GroupsNo").val()!="" && $("#flowProperty_candidate_GroupsName").val()!="")
							|| ($("#flowProperty_candidate_UsersNo").val()!="" && $("#flowProperty_candidate_UsersName").val()!="")){
						if($("#flowProperty_assignType").val()=="dynamicAssignee"){
							common.jbpm.flowPropertyMain.savePropertyInfoByDynamicAssignee(_param);
						}else if($("#flowProperty_assignType").val()=="variablesAssignee"){
							common.jbpm.flowPropertyMain.savePropertyInfoByVariablesAssignee(_param);
						}else{
							startProcess();
							$.post('flowProperty!updateFlowActivity.action',_param,function(){
								msgShow(i18n['common_operation_success'],'show');
								endProcess();
							});
						}
					}else{
						msgAlert(i18n.flow_candidateGroupOrUser_is_notNull,'info');
					}
				}else{
					msgAlert(i18n.plaseSelectOptInfo,'info');
				}
			}
			
		},
		/**
		 * @description 加载各模块验证方法
		 * @param module 模块标识
		 **/
		loadModuleValidateMethod:function(module){
			if(module=='request'){
				$('#flowProperty_validMethod').append('<option value="SolutionsNotEmptyValidate">'+i18n['title_bpm_solutions_not_empty_validate']+'</option>');
				$('#flowProperty_validMethod').append('<option value="RequestRequiredVerification">'+i18n['label_CES_NotNull']+'</option>');
			}else if(module=='change'){
				$('#flowProperty_validMethod').append('<option value="ChangeApproverNotNullValidate">'+i18n['err_changeApproverNotNull']+'</option>');
				$('#flowProperty_validMethod').append('<option value="ChangePlanNotNullValidate">'+i18n['err_changePlanNotNull']+'</option>');
				$('#flowProperty_validMethod').append('<option value="RelatedRequestAllCloseStatus">'+i18n['msg_relatedRequestAllCloseStatus']+'</option>');
				$('#flowProperty_validMethod').append('<option value="FlowAssignUnEqualChangeAssignValidate">'+i18n['tip_flowAssign_unEqual_changeAssign']+'</option>');
			}
			if(module=='problem'){
				$('#flowProperty_validMethod').append('<option value="ProblemSolutionsNotEmptyValidate">'+i18n['title_bpm_solutions_not_empty_validate']+'</option>');
			}else if(module=='release'){
				$('#flowProperty_validMethod').append('<option value="ReleaseRelatedChangeAllCloseValidate">'+i18n['title_releaseRelatedChangeAllCloseValidate']+'</option>');
			}
		},
		/**
		 * @description 加载各模块流程动作
		 * @param module 模块标识
		 **/
		loadProcessAction:function(module){
			if(module=='request'){
				//请求响应
				//$('#taskActions').append('<input type="checkbox" name="flowActivityDTO.taskActions" value="requestResponse" id="flowProperty_taskActions_requestResponse" />'+i18n['label_sla_updateBase_response']);
				//回访
				$('#taskActions').append('<input type="checkbox" name="flowActivityDTO.taskActions" value="requestVisit" id="flowProperty_taskActions_requestVisit" />'+i18n['requestVisit']);
				//申请升级
				$('#taskActions').append('<br><input type="checkbox" name="flowActivityDTO.taskActions" value="requestUpgradeApply" id="flowProperty_taskActions_requestUpgradeApply" />'+i18n['label_request_upgradeApplication']);
				//升级请求
				$('#taskActions').append('<br><input type="checkbox" name="flowActivityDTO.taskActions" value="requestUpgrade" id="flowProperty_taskActions_requestUpgrade" />'+i18n['label_request_requestUpdate']);
				//加签
				$('#taskActions').append('<br><input type="checkbox" name="flowActivityDTO.taskActions" value="requestApporUser" id="flowProperty_taskActions_requestApporUser" />'+i18n['lable_request_appro_user']);
			}
		},
		/**
		 * @description 加载指派类型
		 * @parma selectedId 下拉元素
		 **/
		assignType:function(selectedId){
			//$(":radio[name='flowActivityDTO.assignType']").attr('checked');
			//$('#flowProperty_'+selectedId).attr('checked','checked');
			$('.flowProperty_normalAssignee_tr,.flowProperty_dynamicAssignee_tr,.flowProperty_roleAssignee_tr,.flowProperty_variablesAssignee_tr').hide();
			$('.flowProperty_isUpdateEventAssign_tr').hide();
			$('.flowProperty_normalAssignee_tr').show();
			if(selectedId=="variablesAssignee"){
				$('.flowProperty_'+selectedId+'_tr').show();
			}
			$('.flowProperty_isUpdateEventAssign_tr').show();
			$("#flowProperty_assignType").val(selectedId);
			if(selectedId=='' || selectedId==null){
				//$('#flowProperty_autoAssignee').attr('checked','checked');
				$("#flowProperty_assignType").val("autoAssignee");
			}
		},
		/**
		 * @description 加载角色
		 **/
		loadRole:function(){
			$.post("role!findByState.action",function(data){
				var _html = '';
				if(data!=null){
					for(var i=0;i<data.length;i++){
						if(i % 2==0){
							_html = _html+'<tr>';
						}
						_html = _html+'<td style="text-align: left;"><input name="flowActivityDTO.roleCodes" id="flowActivityDTO_roleCodes_'+data[i].roleId+'" type="checkbox" value="'+data[i].roleId+'">'+data[i].roleName+'</td>';
						
						if(i % 2==1){
							_html = _html+'</tr>';
						}
					}
					$('#flowProperty_role_table').html(_html);
				}
			});
		},
		/**
		 * @description 加载规则列表
		 * @param type 对应模块
		 **/
		loadNoticeModelGridCheckbox:function(type){
			if(basics.ie6.htmlIsNull("#noticeRuleSelect_grid")){
				$("#noticeRuleSelect_grid").trigger("reloadGrid"); 
			}else{	
				var params = $.extend({},jqGridParams, {
					url:'noticeRule!findPager.action',
					caption:'',
					postData:{'noticeRuleDTO.noticeRuleNo':type},
					colNames:['ID',i18n.noticeRule_Name,'',i18n.notice_noticeObject,i18n.noticeMethod,i18n.status,i18n['check']], 
					colModel:[
								{name:'noticeRuleId',width:50,align:'center',sortable:true,hidden:false},
								{name:'noticeRuleName',width:150,align:'center',sortable:true},
								{name:'noticeRuleModule',index:'module',width:50,align:'center',hidden:true},
								{name:'noticeRuleType',width:100,align:'center',sortable:true,formatter:common.tools.noticeRule.noticeGrid.noticesTypeFormatter},
								{name:'noticeMethod',width:100,align:'center',sortable:false,formatter:common.tools.noticeRule.noticeGrid.noticeMethodFormatter},
								{name:'status',index:'useStatus',width:50,align:'center',sortable:true,formatter:common.tools.noticeRule.noticeGrid.statusFormatter},
								{name:'act',width:100,align:'center',sortable:false,formatter:common.jbpm.flowPropertyMain.confirmCheckFormatter},
							],
					jsonReader: $.extend(jqGridJsonReader,{id:"noticeRuleId"}),
					sortname:'noticeRuleId',
					multiselect:true,
					pager:'#noticeRuleSelect_gridPager',
					toolbar:true,
					ondblClickRow:function(rowId){
						var data=$('#noticeRuleSelect_grid').getRowData(rowId);
						common.jbpm.flowPropertyMain.confirmCheck(data.noticeRuleId,data.noticeRuleName);
					}
				});
				$("#noticeRuleSelect_grid").jqGrid(params);
				$("#noticeRuleSelect_grid").navGrid('#noticeRuleSelect_gridPager',navGridParams);
				$("#t_noticeRuleSelect_grid").css(jqGridTopStyles);
				$("#t_noticeRuleSelect_grid").append($('#noticeRuleSelectGridToolbar').html());
			}
		},
		/**
		 * 确认选择
		 * @param id  通知模板Id
		 * @param name 通知模板名称
		 */
		confirmCheck:function(id,name){
			if($('#showNoticeRule #noticeRuleId_'+id).length==0){
				$('#showNoticeRule').append('<div id="noticeRuleId_'+id+'">'+name+
					'<input name="noticeRuleId" type="hidden" value="'+id+'" />'+
					'<input name="flowActivityDTO.noticeRule" type="hidden" value="'+name+'" />'+
					'&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:red;" onclick="common.jbpm.flowPropertyMain.noticeRuleRemove('+id+')">X</a>'+	
					'<div>');
				$('#flowPropertyNoticeRuleSelectWin').dialog('close');
			}else{
				msgAlert(i18n['msg_ruleName_exist'],'info');
			};
		},
		/**
		 * 确认选择格式化
		 */
		confirmCheckFormatter:function(cell,event,data){
			return '<div style="padding:0px">'+
			'<a href="javascript:common.jbpm.flowPropertyMain.confirmCheck(\''+data.noticeRuleId+'\',\''+data.noticeRuleName+'\')" title="'+i18n.check+'">'+
			'<img src="../images/icons/ok.png"/></a>'+
			'</div>';
		},
		/**
		 * 添加选择
		 * @param id  通知模板Id
		 * @param name 通知模板名称
		 */
		confirmSelect:function(){
			var rowIds = $("#noticeRuleSelect_grid").getGridParam('selarrrow');
			if(rowIds==''){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				$("#flowProperty_noticeRuleId").val(rowIds);
				var rowValue="";
				if(rowIds.length>0){
					for ( var i = 0; i<rowIds.length; i++) {
						var rowData=$("#noticeRuleSelect_grid").getRowData(rowIds[i]);
						if($('#showNoticeRule #noticeRuleId_'+rowData.noticeRuleId).length==0){
							$('#showNoticeRule').append('<div id="noticeRuleId_'+rowData.noticeRuleId+'">'+rowData.noticeRuleName+
									'<input name="noticeRuleId" type="hidden" value="'+rowData.noticeRuleId+'" />'+
									'<input name="flowActivityDTO.noticeRule" type="hidden" value="'+rowData.noticeRuleName+'" />'+
									'&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:red;" onclick="common.jbpm.flowPropertyMain.noticeRuleRemove('+rowData.noticeRuleId+')">X</a>'+
									'<div>');
						}
					}
				}
				$('#flowPropertyNoticeRuleSelectWin').dialog('close');
			}
		},
		/**
		 * 通知模板移除
		 * @param id  通知模板Id
		 */
		noticeRuleRemove:function(id){
			$('#noticeRuleId_'+id).remove();
		},
		/**
		 * 加载匹配规则
		 * @param select 加载下拉元素
		 */
		loadOptionsByCode:function(select){
			$(select).empty();
			$('<option value="">-- '+i18n["common_pleaseChoose"]+' --</option>').appendTo(select);
			var url = 'callBusinessRuleSet!findlidto.action';
			$.post(url,function(res){
				if(res!=null && res.length>0){
					for(var i=0;i<res.length;i++){
						$('<option value="'+res[i].ruleNo+'">'+res[i].ruleName+'</option>').appendTo(select);
					}
				}
			});	
		},
		/**
		 * @description 初始化
		 * @private
		 */
		init:function(){
			$('.showMatchRule_tr1').hide();
			$('.showMatchRule_tr2').hide();
			$("#flowPropertyMain_loading").hide();
			$("#flowPropertyMainContent").show();
			common.jbpm.flowPropertyMain.loadFlowPropertyMainPage();
			$("#noticeRuleSelect_grid_select").click(function(){
				common.jbpm.flowPropertyMain.confirmSelect();
			});
			$('#flowPropertyMain_flow_view_link').click(function(){
			    var _url = '../pages/common/jbpm/traceInstance.jsp?processId='+$('#flowPropertyMain_flow_key').val();
			    basics.tab.tabUtils.refreshTab(i18n['flowChart'],_url);
			});
			
			$('#saveFlowProperty').click(function(){
				common.jbpm.flowPropertyMain.savePropertyInfo();
			});
			
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#flowProperty_assigneeName','com.wstuo.common.security.entity.User','loginName','fullName','userId','Long','#flowProperty_assigneeNo',cNo,'true');
			
			//绑定指派人
			$('#flowProperty_assigneeName').click(function(){
				common.security.userUtil.selectUser('#flowProperty_assigneeName','#flowProperty_assigneeNo','','fullName',companyNo,$('#flowProperty_groupNo').val());
			});
			$('#searchflowProperty_assigneeName').click(function(){
				common.security.userUtil.selectUser('#flowProperty_assigneeName','#flowProperty_assigneeNo','','fullName',companyNo,$('#flowProperty_groupNo').val());
			});
			//绑定指派组
			$('#flowProperty_groupName').click(function(){
				common.security.organizationTreeUtil.showAll_2('#index_selectORG_window','#index_selectORG_tree','#flowProperty_groupName','#flowProperty_groupNo',companyNo);
			});
			//绑定指派组
			$('#flowProperty_variablesAssigneeGroupName').click(function(){
				common.security.organizationTreeUtil.showAll_2('#index_selectORG_window','#index_selectORG_tree','#flowProperty_variablesAssigneeGroupName','#flowProperty_variablesAssigneeGroupNo',companyNo);
			});
			//绑定候選組
			$('#flowProperty_candidate_GroupsName').click(function(){
				common.config.category.serviceOrgUtils.selectORG("#flowProperty_candidate_GroupsName","#flowProperty_candidate_GroupsNo",i18n.label_userOwnerGroup)
			});
			//绑定候選人
			$('#flowProperty_candidate_UsersName').click(function(){
				common.security.userUtil.selectUserMultiRule('#flowProperty_candidate_UsersName','#flowProperty_candidate_UsersNo','candidate',companyNo);
				//common.security.organizationTreeUtil.showAll_2('#index_selectORG_window','#index_selectORG_tree','#flowProperty_candidate_UsersName','#flowProperty_candidate_UsersNo',companyNo);
			});
			common.jbpm.flowPropertyMain.loadRole();
			//common.jbpm.flowPropertyMain.loadOptionsByCode('#taskActions_priority');
		}
	}
	
}();

$(function(){
	common.jbpm.flowPropertyMain.init();	
});

