$package('common.jbpm')
/**  
 * @fileOverview for jbpm/ProcessInstances.jsp
 * @author amy  
 * @constructor processInstances
 * @description for 流程实例函数
 * @date 2010-11-10
 * @since version 1.0 
 **/
$import('common.jbpm.processCommon');
common.jbpm.processInstances=function() {
	
	return {
		/**
		 * Div组合
		 */
		$full:function(selector) {
			return $(parentDivPrefix + selector);
		},
		/** 
		 * 操作项格式化
		 * @param cellValue:单元格的值；
		 * @param options:字体样式等；
		 * @param row:一行的数据
		 * */
		operateFormatter:function(cellValue, options, row) {
			return '<a href="javascript:common.jbpm.processCommon.showFlowChart(\''+row.id+'\',\''+i18n['flowExample']+'\')" title="'+i18n['tracking']+'">'+i18n['tracking']+'</a>';
		},
		/**
		 * 流程实例列表
		 */
		load_processInstances_list:function(){
			var params = $.extend({},jqGridParams, {	
				url:'jbpm!findPageProcessInstance.action?processHistoriesQueryDTO.processDefinitionId='+_processDefinitionId,
				colNames:['ID',i18n['flow']+'ID','',i18n['status'],i18n['priority'],i18n['operateItems']],
				colModel:[
	  					  {name:'id',index:'id',align:'center',width:150,sortable:false},
						  {name:'processDefinitionId',index:'processDefinitionId',align:'center',width:100,sortable:false},
						  {name:'key',index:'key',width:100,hidden:true,sortable:false},
						  {name:'state',index:'state',width:50,align:'center',sortable:false},
						  {name:'priority',index:'priority',hidden:true,width:100,sortable:false},
						  {name:'operate',width:100,align:'center',formatter:common.jbpm.processInstances.operateFormatter,sortable:false}
					  ],
				toolbar:false,
				multiselect:false,
				jsonReader: $.extend(jqGridJsonReader, {id: "id"}),	  
				sortname:'id',
				pager:'#processInstancesPager'
				});
			
				$("#processInstancesGrids").jqGrid(params);
				$("#processInstancesGrids").navGrid('#processInstancesPager',navGridParams);
				//自适应宽度
				setGridWidth("#processDefinedGrid","regCenter",20);

		},
		/**
		 * @description 显示流程图
		 * @param _piId 流程Id
		 **/
		trace:function(_piId){
		    var _url = 'jbpm!traceInstance.action?instanceId='+_piId;
		    basics.tab.tabUtils.addTab(i18n['flowChart']+i18n['tracking'],_url);
		},
		/**
		 * 初始化
		 * @private
		 */
		init: function() {
			common.jbpm.processInstances.load_processInstances_list();
		}
  	}
}();
$(document).ready(common.jbpm.processInstances.init);