$package('common.sla');
/**  
 * @author Van  
 * @constructor WSTO
 * @description SLA详细信息主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.sla.slaDetail=function(){
	
	
	this.slaDetailGrids=[];
	return{
		/**
		 * @description 列表伸缩
		 */
		slaDetailPanelExpandOrCollapse:function(){
			 $(slaDetailGrids).each(function(i, g) {
			      setGridWidth(g, 'slaDetailMainTab', 10);
			 });
		},
		/**
		 * @description 获取事件
		 */
		fitSLADetailsGrids:function(){
			$('#slaDetailWest').panel({
				onCollapse:common.sla.slaDetail.slaDetailPanelExpandOrCollapse,
				onExpand:common.sla.slaDetail.slaDetailPanelExpandOrCollapse,
				onResize:function(width, height){
				setTimeout(function(){
					common.sla.slaDetail.slaDetailPanelExpandOrCollapse();
				},0);
			}
			});
		},
		/**
		 * @description 初始化
		 */
		init:function(){
/*			$("#SLAServiceManage_loading").hide();
			$("#SLAServiceManage_content").show();
*/
			setTimeout(function(){
				common.sla.slaDetail.fitSLADetailsGrids();
				setTimeout(function(){
					//实例化SLA规则列表
					common.sla.slaDetail_ruleGrid.init();
					//实例化自动升级列表
					common.sla.slaDetail_updateGrid.init();
				},100);
			},0);
			
		}
	};
 }();
$(document).ready(common.sla.slaDetail.init);
