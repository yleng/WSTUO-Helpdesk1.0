﻿$package('common.tools.includes');
/**  
 * @author Van  
 * @constructor includes
 * @description includes
 * @date 2010-11-17
 * @since version 1.0 
 */
common.tools.includes.includes=function(){
	//事件任务
	this._loadEventTaskIncludesFileFlag=false;
	//事件进展及成本
	this._loadEventTimeCostIncludesFileFlag=false;
	//导出
	this._loadExportInfoFileFlag = false;
	return {
		/**
		 * 加载添加编辑任务窗口includes文件
		 */
		loadEventTaskIncludesFile:function(){
			if(!_loadEventTaskIncludesFileFlag){
				$('#eventTask_html').load('common/tools/includes/includes_task.jsp',function(){
					$.parser.parse($('#eventTask_html'));
				});
			}
			_loadEventTaskIncludesFileFlag=true;
		},
		/**
		 * 加载时间及人工成本窗口includes文件
		 */
		loadEventTimeCostIncludesFile:function(){
			if(!_loadEventTimeCostIncludesFileFlag){
				$('#eventTimeCost_html').load('common/tools/includes/includes_timeCost.jsp',function(){
					$.parser.parse($('#eventTimeCost_html'));
				});
			}
			_loadEventTimeCostIncludesFileFlag=true;
		},
		/**
		 * 导出详细文件
		 */
		loadExportInfoIncludesFile:function(){
			if(!_loadExportInfoFileFlag){
				$('#exportInfo_html').load('common/tools/includes/includes_exportManage.jsp',function(){
					$.parser.parse($('#exportInfo_html'));
				});
			}
			_loadExportInfoFileFlag=true;
		},
		init:function(){
			
		}
	}
}();
$(function(){common.tools.includes.includes.init});
