
/**  
 * @fileOverview 进展及成本主函数
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 进展及成本主函数
 * @description 进展及成本主函数
 * @date 2011-05-19
 * @since version 1.0 
 */
function eventCost(eno,eventType){
	
	this.gridId='#'+eventType+'EventCostGrid';
	this.t_gridId='#t_'+eventType+'EventCostGrid';
	this.pagerId='#'+eventType+'EventCostGridPager';
	this.toolBarId='#'+eventType+'EventCostGridToolbar';
	this.actFormatter='#'+eventType+'EventCostActFormatterDiv';
	this.costFormId='#'+eventType+'AddOrEditEventCostForm';
	this.costWinId='#'+eventType+'AddOrEditEventCostDiv';
	this.eno=eno;
	this.eventType='itsm.'+eventType;
	this.eventCostEnoId='#'+eventType+'_cost_eno'
	var event = this;
	this.costActFormatter=function(cell,opt,data){
		return $('#'+eventType+'EventCostActFormatterDiv').html();
	};
	/**
	 * @description 状态国际化
	 */
	this.statusFormatter=function(cell,event,data){
		if(data.status==1){
			return i18n.task_label_pending;
		}else{
			return i18n.lable_complete;
		}
	}
	/**
	 * @description 分钟转为小时和分钟
	 */
	this.minute2HM=function(cell,event,data){
		return Math.floor(cell/60)+'h'+cell%60+'m';
	}
	/**
	 * @description 分钟转为小时和分钟 国际化
	 */
	this.minute2HM2=function(cell,event,data){
		return Math.floor(cell/60)+i18n.hours+cell%60+i18n.minutes;
	}
	
	this.hangFormat=function(cell,event,data){
		if(cell=='HANG')
			return i18n['label_dynamicReports_yes']
		else
			return i18n['label_dynamicReports_no']
	}
	/**
	 * @description 进展及成本列表
	 */
	this.eventCostGrid=function(tool_show_hide) {
		var gridId = this.actFormatter;
		var params;
		if(tool_show_hide){
			params = $.extend({},jqGridParams, {	
				url:'cost!findPagerCost.action?costDTO.eno='+eno+'&costDTO.eventType='+this.eventType,
				colNames:['ID','',i18n['label_request_hang'],i18n['title_technician'],i18n['title_startTime'],i18n['title_endTime'],i18n['label_startToEedTime'],i18n['label_actualTime'],'','',i18n['title_perHourFees'],i18n['title_technicianCost'],i18n['title_material_cost'],i18n['title_otherCost'],i18n['title_totalCost'],i18n['description'],i18n['title_belongsStage'],i18n['status'],'','','','','',i18n['operateItems']],
				colModel:[
				    {name:'progressId',align:'left',width:5},
				    {name:'userId',hidden:true},
				    {name:'type',width:5,formatter:this.hangFormat},
			   		{name:'fullName',index:'users',align:'left',width:10},
			   		{name:'startTime',index:'startTime',align:'left',width:15,formatter:timeFormatter},
			   		{name:'endTime',index:'endTime',align:'left',width:15,formatter:timeFormatter},
			   		{name:'startToEedTime',width:10,formatter:this.minute2HM},
			   		{name:'actualTime',width:10,formatter:this.minute2HM},
			   		{name:'startToEedTime',hidden:true},
			   		{name:'actualTime',hidden:true},
			   		{name:'perHourFees',width:10,hidden:true},
			   		{name:'skillFees',align:'left',width:10,hidden:false},
			   		{name:'materialCost',align:'left',width:10,hidden:false},
			   		{name:'othersFees',align:'left',width:10,hidden:false},
			   		{name:'totalFees',align:'left', width:10},
			   		{name:'description',align:'left',width:15,hidden:true},
			   		{name:'stage',align:'left',width:10},
			   		{name:'statusLabel',align:'center',width:10,sortable:false,formatter:this.statusFormatter},
			   		{name:'status',hidden:true},
			   		{name:'taskId',hidden:true},
			   		{name:'title',hidden:true},
			   		{name:'eno',hidden:true},
			   		{name:'eventType',hidden:true},
			   		{name:'act', width:10,align:'center',sortable:false,formatter:function(cell,event,data){
	             	   return $(gridId).html().replace(/'{progressId}'/g,event.rowId);
	                }}
				],
				jsonReader: $.extend(jqGridJsonReader,{id: "progressId"}),
				ondblClickRow:function(rowId){event.timeCostDetailWin(rowId)},
				sortname:'progressId',
				toolbar: [tool_show_hide,"top"],
				pager:this.pagerId
			});
		}else{
			params = $.extend({},jqGridParams, {	
				url:'cost!findPagerCost.action?costDTO.eno='+eno+'&costDTO.eventType='+this.eventType,
				colNames:['ID','',i18n['label_request_hang'],i18n['title_technician'],i18n['title_startTime'],i18n['title_endTime'],i18n['label_startToEedTime'],i18n['label_actualTime'],'','',i18n['title_perHourFees'],i18n['title_technicianCost'],i18n['title_material_cost'],i18n['title_otherCost'],i18n['title_totalCost'],i18n['description'],i18n['title_belongsStage'],i18n['status'],'','','','',''],
				colModel:[
				    {name:'progressId',align:'left',width:5},
				    {name:'userId',hidden:true},
				    {name:'type',width:5,formatter:this.hangFormat},
			   		{name:'fullName',index:'users',align:'left',width:10},
			   		{name:'startTime',index:'startTime',align:'left',width:15,formatter:timeFormatter},
			   		{name:'endTime',index:'endTime',align:'left',width:15,formatter:timeFormatter},
			   		{name:'startToEedTime',width:10,formatter:this.minute2HM},
			   		{name:'actualTime',width:10,formatter:this.minute2HM},
			   		{name:'startToEedTime',hidden:true},
			   		{name:'actualTime',hidden:true},
			   		{name:'perHourFees',width:10,hidden:true},
			   		{name:'skillFees',align:'left',width:10,hidden:false},
			   		{name:'materialCost',align:'left',width:10,hidden:false},
			   		{name:'othersFees',align:'left',width:10,hidden:false},
			   		{name:'totalFees',align:'left', width:10},
			   		{name:'description',align:'left',width:15,hidden:true},
			   		{name:'stage',align:'left',width:10},
			   		{name:'statusLabel',align:'center',width:10,sortable:false,formatter:this.statusFormatter},
			   		{name:'status',hidden:true},
			   		{name:'taskId',hidden:true},
			   		{name:'title',hidden:true},
			   		{name:'eno',hidden:true},
			   		{name:'eventType',hidden:true}
				],
				jsonReader: $.extend(jqGridJsonReader,{id: "progressId"}),
				ondblClickRow:function(rowId){event.timeCostDetailWin(rowId)},
				sortname:'progressId',
				toolbar: [tool_show_hide,"top"],
				pager:this.pagerId
			});
		}
			
		
		$(this.gridId).jqGrid(params);
		$(this.gridId).navGrid(this.pagerId,navGridParams);
		//列表操作项
		$(this.t_gridId).css(jqGridTopStyles);
		$(this.t_gridId).append($(this.toolBarId).html());
		if(tool_show_hide){
			$(this.gridId).jqGrid('showCol', 'cb');
		}else{
			$(this.gridId).jqGrid('hideCol', 'cb');
		}
		//自适应大小
//		setGridWidth(this.gridId,"regCenter",20);
	};
	
	this.timeCostDetailWin=function(id){
		var postUrl="cost!findCostById.action";
		var param="progressId="+id;
		$.post(postUrl,param,function(data){
			$('#index_timeCostDetail_fullName').text(data.fullName);
			$('#index_timeCostDetail_startTime').text(timeFormatter(data.startTime,'',''));
			$('#index_timeCostDetail_endTime').text(timeFormatter(data.endTime,'',''));
			$('#index_timeCostDetail_startToEedTime').text(event.minute2HM2(data.startToEedTime,'',''));
			$('#index_timeCostDetail_actualTime').text(event.minute2HM2(data.actualTime,'',''));
			$('#index_timeCostDetail_perHourFees').text(data.perHourFees);
			$('#index_timeCostDetail_skillFees').text(data.skillFees);
			$('#index_timeCostDetail_materialCost').text(data.materialCost);
			$('#index_timeCostDetail_othersFees').text(data.othersFees);
			$('#index_timeCostDetail_totalFees').text(data.totalFees);
			$('#index_timeCostDetail_description').text(data.description);
			$('#index_timeCostDetail_belongsStage').text(data.stage);
			if(data.status==1){
				$('#index_timeCostDetail_state').text(i18n.task_label_pending);
			}else{
				$('#index_timeCostDetail_state').text(i18n.lable_complete);
			}
			$('#index_timeCostDetail_task').text(data.title);
			windows('index_timeCostDetail_window',{width:600,modal: true});
		});
		
	}
	//操作
	this.saveEventCost=function(){
		var t1=$('#index_timeCost_startTime').val().split(" ");
		var t2=$('#index_timeCost_endTime').val().split(" ");
		
		var tt1=t1[0].split("-");
		var tt2=t2[0].split("-");
		t1=tt1[1]+"/"+tt1[2]+"/"+tt1[0]+" "+t1[1];//转化为标准时间格式(月/日/年 时:分:秒) 
		t2=tt2[1]+"/"+tt2[2]+"/"+tt2[0]+" "+t2[1];//转化为标准时间格式(月/日/年 时:分:秒) 
		var addm=Math.ceil((Date.parse(t2)-Date.parse(t1))/1000/60);//时间差(单位：分钟)不足1分钟按1分钟计 
		var d=Math.floor(addm/60/24);
		var hh=Math.floor(addm/60);
		var h=Math.floor((addm-d*60*24)/60);
		var m=addm-d*60*24-h*60;
		$('#index_timeCost_startToEedTime').val(addm);
		//开始到结束时间
		$('#timeCostHour').text(hh);
		$('#timeCostMinute').text(m);
		if($('#index_timeCost_form').form('validate')){
			var _param = $('#index_timeCost_form').serialize();
			var _url='cost!'+opt+'.action';
			$.post(_url,_param,function(){
				endProcess();
				$('#index_timeCost_window').dialog('close');
	 			$('#'+eventType+'EventCostGrid').trigger('reloadGrid');
	 			msgShow(i18n['saveSuccess'],'show');	
			})
		}else{
			endProcess();
		}	
	};

	//新增
	this.addEventCost_win=function()
	{
		opt='saveCost';
		resetForm('#index_timeCost_form');
		$('#index_timeCost_eno').val(eno);
		$('#index_timeCost_eventType').val(this.eventType);
		DateBox97('#index_timeCost_startTime,#index_timeCost_endTime');
		$('#index_timeCost_tcName').val(fullName);
		$('#index_timeCost_tcId').val(index_userId);
		$('#index_timeCost_save').show();
		$('#input_timeCostHour,#input_timeCostMinute').removeAttr('readonly');
		$('#timeCostHour,#timeCostMinute').text('0');
		windows('index_timeCost_window',{width:500,close:function(){
			$('#index_timeCost_window').click();
		}});
	};
	
	//编辑
	this.editEventCost_aff=function(){
		opt="editCost";
		checkBeforeEditGrid(this.gridId,this.editEventCost_win);
	};
	
	//编辑
	this.editEventCost_affs=function(rowId,ec){
		opt="editCost";
		var rowData= $(ec.gridId).jqGrid('getRowData',rowId);  // 行数据  
		ec.editEventCost_win(rowData);
	};
	
	this.editEventCost_win=function(rowData) 
	{
		
		$('#index_timeCost_eno').val(rowData.eno);
		$('#index_timeCost_progressId').val(rowData.progressId);
		$('#index_timeCost_eventType').val(rowData.eventType);
		$('#index_timeCost_tcId').val(rowData.userId);
		$('#index_timeCost_tcName').val(rowData.fullName);
		$('#index_timeCost_opertionTime').val(timeFormatterOnlyData(rowData.opertionTime));
		$('#index_timeCost_useTime').val(rowData.useTime);
		$('#index_timeCost_perHourFees').val(rowData.perHourFees);
		$('#index_timeCost_skillFees').val(rowData.skillFees);
		$('#index_timeCost_othersFees').val(rowData.othersFees);
		$('#index_timeCost_materialCost').val(rowData.materialCost);
		$('#index_timeCost_totalFees').val(rowData.totalFees);
		$('#index_timeCost_description').val(rowData.description);
		$('#index_timeCost_belongsStage').val(rowData.stage);
		
		
		$('#index_timeCost_startTime').val(rowData.startTime);
		$('#index_timeCost_endTime').val(rowData.endTime);
		$('#index_timeCost_startToEedTime').val(rowData.startToEedTime);
		$('#index_timeCost_actualTime').val(rowData.actualTime);
		
		
		var t1=rowData.startTime.split(" ");
		var t2=rowData.endTime.split(" ");
		var tt1=t1[0].split("-");
		var tt2=t2[0].split("-");
		t1=tt1[1]+"/"+tt1[2]+"/"+tt1[0]+" "+t1[1];//转化为标准时间格式(月/日/年 时:分:秒) 
		t2=tt2[1]+"/"+tt2[2]+"/"+tt2[0]+" "+t2[1];//转化为标准时间格式(月/日/年 时:分:秒) 
		var addm=Math.ceil((Date.parse(t2)-Date.parse(t1))/1000/60);//时间差(单位：分钟)不足1分钟按1分钟计 
		var d=Math.floor(addm/60/24);
		var hh=Math.floor(addm/60);
		var h=Math.floor((addm-d*60*24)/60);
		var m=addm-d*60*24-h*60;
		//开始到结束时间
		$('#timeCostHour').text(hh);
		$('#timeCostMinute').text(m);
		//开始到结束时间
	/*	$('#timeCostHour').text(Math.floor(rowData.startToEedTime*1/60));
		$('#timeCostMinute').text(rowData.startToEedTime*1%60);*/
		
		//实际处理时间
		$('#input_timeCostHour').val(Math.floor(rowData.actualTime*1/60));
		$('#input_timeCostMinute').val(rowData.actualTime*1%60);
		
		if(rowData.status=='1'){
			$('#index_timeCost_statusProcessing').attr("checked",'true');
		}else{
			$('#index_timeCost_statusComplete').attr("checked",'true');
		}
		$('#index_timeCost_costTaskId').val(rowData.taskId);
		$('#index_timeCost_costTaskTitle').val(rowData.title);
		DateBox97('#index_timeCost_startTime,#index_timeCost_endTime');
		if(rowData.type==i18n['label_dynamicReports_yes']){
			$('#input_timeCostHour,#input_timeCostMinute').attr('disabled','disabled');
			$('#index_timeCost_save').hide();
		}else{
			$('#input_timeCostHour,#input_timeCostMinute').removeAttr('readonly');
			$('#index_timeCost_save').show();
		}
		windows('index_timeCost_window',{width:500});
	};
	
	
	this.deleteEventCost_aff=function()
	{
		checkBeforeDeleteGrid(this.gridId,this.deleteEventCost);
	};
	
	this.deleteEventCost_affs=function(rowId,ec)
	{
		msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
			ec.deleteEventCost(rowId);
		});
	};
	
	//删除
	this.deleteEventCost=function(rowIds)
	{
		var _param = $.param({'ids':rowIds},true);
		$.post("cost!deleteCost.action", _param, function(){
			$('#'+eventType+'EventCostGrid').trigger('reloadGrid');
			msgShow(i18n['deleteSuccess'],'show');
		}, "json");
	};
	
}
