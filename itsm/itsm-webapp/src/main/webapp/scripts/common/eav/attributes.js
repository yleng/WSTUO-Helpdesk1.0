$package('common.eav');
/**  
 * @fileOverview 扩展属性主函数
 * @author QXY
 * @version 1.0  
 */

 /**  
 * @author QXY  
 * @constructor 扩展属性主函数
 * @description 扩展属性主函数
 * @date 2011-05-24
 * @since version 1.0 
 */ 
common.eav.attributes=function(){
	var attr;
	
	return {
		/**
		 * @description 新增页面,查询出该分类的扩展属性并组装html
		 * @param entityType 类型
		 * @param showAttrId 显示div的id
		 */
		showAttributes:function(entityType,showAttrId){
			$('#'+showAttrId+' table tbody').html('');
			
			$.post('attrs!findAttributesByEntityType.action','entityType='+entityType,function(data){
				if(data!==null && data.length>0 && data!==''){
					for(var i=0;i<data.length;i++){
			
						if(data[i].attrAsName.indexOf('num')!=-1){
							attr="<tr><td style='text-align:left'>"+data[i].attrAsName+"</td><td style='text-align:left'><input style=width:85% id=phoneNums name=requestDTO.attrVals['"+data[i].attrName+"'] class='easyui-datebox' ";
						}else{
							attr="<tr><td style='text-align:left'>"+data[i].attrAsName+"</td><td style='text-align:left'><input style=width:85% name=requestDTO.attrVals['"+data[i].attrName+"'] class='easyui-datebox' ";
						}		
						if(data[i].attrType=="Lob")
							$('#'+showAttrId+' table tbody').append("<tr><td style='text-align:left'>"+data[i].attrAsName+"</td><td style='text-align:left'><textarea  style=width:85% name=requestDTO.attrVals['"+data[i].attrName+"'] ></textarea></td></tr>");
						else{
							if(data[i].attrType=="String"){	
								attr+="";
							}else if(data[i].attrType=="Date"){	
								attr+="";
							}else if(data[i].attrType=="Integer"){
								attr+="min='0' maxlength='9'";
							}else if(data[i].attrType=="Double"){	
								attr+="precision='3'";
							}
							attr+=" /></td></tr>";
								
							$('#'+showAttrId+' table tbody').append(attr);
						}	
					}
				}else{
					$('#'+showAttrId+' table thead').remove();
					$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n.noData+"</td></tr>");
				}
			});
		},
		findAttributeByScheduledTask:function(eventEavVals,categoryNo,eventType,dtoName,method){
			$.post("event!findByIdCategorys.action",{"categoryId":categoryNo},function(res){
				if(res.eavId!="null" && res.eavId!==null && res.eavId!=""){
					$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+res.eavId,function(data){
						if(data!==null && data.length>0 && data!==''){
							var json=[];
							for(var i=0;i<data.length;i++){
								var val=data[i];
								var attr={
										fieldType:"customField",
										attrType:val.attrType,
										label:val.attrAsName,
										attrNo:val.attrNo,
										attrName:dtoName+".attrVals['"+val.attrName+"']",
										required:val.attrIsNull,
										attrdataDictionary:val.attrdataDictionary,
										attrItemName:val.attrItemName.join(","),
										attrColumn:"1"//一列显示
									};
								if(eventEavVals!==null){
									if(eval('eventEavVals.'+val.attrName)!==undefined && eval('eventEavVals.'+val.attrName)!==''){
										attr.value=eval('eventEavVals.'+val.attrName);
									}
								}
								json.push(attr);
								
							}
							method(json);
						}else
							method();
					});
				}else{
					method();
				}
			});
		},
		/**
		 * @description 自定义表单后旧数据处理
		 * @param eno 编号
		 * @param eventType 类型
		 * @param eavId 扩展属性id
		 * @param showAttrId 显示div的id
		 * @param dtoName 属性name
		 * @param type 标记生成编辑html还是详情html
		 */
		findAttributeByEno_New:function(eno,eventType,eavId,dtoName,type,method){
			//读取值
			$.post('eventEav!findEventEav.action','eventEavDTO.entityType='+eventType+'&eventEavDTO.eno='+eno,function(eventEavVals){
				if(eavId!="null" && eavId!==null && eavId!=0){
					$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+eavId,function(data){
						if(data!==null && data.length>0 && data!==''){
							var json=[];
							for(var i=0;i<data.length;i++){
								var val=data[i];
								var attr={
										fieldType:"customField",
										attrType:val.attrType,
										label:val.attrAsName,
										attrNo:val.attrNo,
										attrName:dtoName+".attrVals['"+val.attrName+"']",
										required:val.attrIsNull,
										attrdataDictionary:val.attrdataDictionary,
										attrItemName:val.attrItemName.join(","),
										attrColumn:"1"//一列显示
									};
								if(eventEavVals!==null){
									if(eval('eventEavVals.'+val.attrName)!==undefined && eval('eventEavVals.'+val.attrName)!==''){
										if("DataDictionaray"==val.attrType && type=="detail"){
											attr.value="{DataDictionaray"+eval('eventEavVals.'+val.attrName)+"}";
											attr.dcode=eval('eventEavVals.'+val.attrName);
										}else{
											attr.value=eval('eventEavVals.'+val.attrName);
										}
									}
								}
								json.push(attr);
								
							}
							method(json);
						}else
							method();
					});
				}else
					method();
			});
		},
		
		/**
		 * @description 配置项自定义表单后旧数据处理
		 * @param eno 编号
		 * @param eventType 类型
		 * @param eavId 扩展属性id
		 * @param showAttrId 显示div的id
		 * @param dtoName 属性name
		 * @param type 标记生成编辑html还是详情html
		 */
		findAttributeByCim_New:function(eno,eventType,eavId,dtoName,eventEavVals,method,type){
			//读取值
				if(eavId!="null" && eavId!==null && eavId!=0){
					$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+eavId,function(data){
						if(data!==null && data.length>0 && data!==''){
							var json=[];
							for(var i=0;i<data.length;i++){
								var val=data[i];
								var attr={
										fieldType:"customField",
										attrType:val.attrType,
										label:val.attrAsName,
										attrNo:val.attrNo,
										attrName:dtoName+".attrVals['"+val.attrName+"']",
										required:val.attrIsNull,
										attrdataDictionary:val.attrdataDictionary,
										attrItemName:val.attrItemName.join(","),
										attrColumn:"1"//一列显示
									};
								if(eventEavVals!==null){
									if(eval('eventEavVals.'+val.attrName)!==undefined && eval('eventEavVals.'+val.attrName)!==''){
										if("DataDictionaray"==val.attrType && type=="detail"){
											attr.value="{DataDictionaray"+eval('eventEavVals.'+val.attrName)+"}";
											attr.dcode=eval('eventEavVals.'+val.attrName);
										}else{
											attr.value=eval('eventEavVals.'+val.attrName);
										}
									}
								}
								json.push(attr);
								
							}
							method(json);
						}else
							method();
					});
				}else
					method();
		},
		/**
		 * @description 编辑页面的，拓展属性的详细
		 * @param eno 编号
		 * @param eventType 类型
		 * @param eavId 扩展属性id
		 * @param showAttrId 显示div的id
		 * @param dtoName 属性name
		 */
		findAttributeByEno:function(eno,eventType,eavId,showAttrId,dtoName){
			$('#'+showAttrId+' table tbody').html('');
			$.post('eventEav!findEventEav.action','eventEavDTO.entityType='+eventType+'&eventEavDTO.eno='+eno,function(eventEavVals){
				if(eavId!="null" && eavId!==null && eavId!=0){
					$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+eavId,function(data){
						if(data!==null && data.length>0 && data!==''){
							var oldGroupName="";
							for(var i=0;i<data.length;i++){
								var value='';
								if(eval('eventEavVals.'+data[i].attrName)!==undefined && eval('eventEavVals.'+data[i].attrName)!==''){
									value = eval('eventEavVals.'+data[i].attrName);
								}
								var requiredHtml ='';
								var required =false;
								if(!data[i].attrIsNull){
									required =true;
									requiredHtml ='<span style=color:red>&nbsp;&nbsp;*</span>';
								}
								var attr1="";
								var attrgroup = "";
								if(data[i].attrGroupName!=oldGroupName){
									attrgroup="<tr><th colspan=\"2\" style=\"text-align:left;\">"+data[i].attrGroupName+"</th></tr>";
								}
								oldGroupName=data[i].attrGroupName;
								var vals=data[i].attrName;
								if(data[i].attrType=="Lob"){
									attr1+=attrgroup+"<tr>"+
										"<td style='text-align:left'>"+data[i].attrAsName+requiredHtml+"</td>"+
										"<td style='text-align:left'>"+
											"<textarea "+(required==true?'validtype=nullValueValid':'')+" attrtype='String' style=width:85% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"'>{value}</textarea>"+
										"</td>"+
									"</tr>";
								}else if(data[i].attrType=="DataDictionaray" || data[i].attrType=="Radio" ||data[i].attrType=="Checkbox"){
									attr1+=attrgroup+"";
								}else{
									attr1+=attrgroup+"<tr>"+
									"<td style='text-align:left'>"+data[i].attrAsName+requiredHtml+"</td>"+
									"<td style='text-align:left'>"+
										"<input "+(required==true?'validtype=nullValueValid':'')+" attrtype='String' style=width:85% name="+dtoName+".attrVals['"+data[i].attrName+"'] value='{value}' id='"+data[i].attrName+showAttrId+"' />"+
									"</td>"+
                                    "</tr>";
								}
								attr1 = attr1.replace('{value}',value);
								
								attrdate=attrgroup+"<tr>"+
								"<td style='text-align:left'>"+data[i].attrAsName+requiredHtml+"</td>"+
								"<td style='text-align:left'>"+
									"<input style=width:85% name="+dtoName+".attrVals['"+data[i].attrName+"'] value='{value}' id='"+data[i].attrName+showAttrId+"' readonly/>"+
								"</td>"+
								"</tr>";
								attrdate = attrdate.replace('{value}',value);
								
								if(data[i].attrType=="Date"){
									$('#'+showAttrId+' table tbody').append(attrdate);
									DatePicker97(["#"+data[i].attrName+showAttrId]);
									$('#'+data[i].attrName+showAttrId).validatebox({
										required:required
									});

								}else if(data[i].attrType=="Integer"){
									$('#'+showAttrId+' table tbody').append(attr1);
									$('#'+data[i].attrName+showAttrId).numberbox({
										required:required,
										min:0,
										validType:length[1,9],
										max:999999999
									});
								}else if(data[i].attrType=="Double"){	
									$('#'+showAttrId+' table tbody').append(attr1)
									$('#'+data[i].attrName+showAttrId).numberbox({
										required:required,
										min:0,
										validType:length[1,13],
										max:999999999.999,
										precision:3
									});
								}else if(data[i].attrType=="DataDictionaray"){
									var data_name = data[i].attrName;
									var data_id = data[i].attrName+showAttrId;
									var attrValNo = Number(eval('eventEavVals.'+vals));
									if(attrValNo===0 || isNaN(attrValNo)){
										attr1+="<tr><td style='text-align:left;' >"+data[i].attrAsName+requiredHtml+"</td>";
										attr1+="<td style='text-align:left'><select style='text-align:left;width:75%' name="+dtoName+".attrVals['"+data_name+"'] id='"+data_id+"' >"; 
										attr1+="<option value=''>--"+i18n['pleaseSelect']+"--</option>";
										attr1+="</select></td></tr>";
										$('#'+showAttrId+' table tbody').append(attr1);
										var attrdata_url="dataDictionaryItems!findByCode.action?dataDictionaryQueryDto.groupCode="+data[i].attrdataDictionary;
										common.eav.attributes.loadDataDicEdit(attrdata_url,data_id);
										$('#'+data[i].attrName+showAttrId).validatebox({
											required:required
										});
									}else{
										attr1+="<tr><td style='text-align:left' >"+data[i].attrAsName+"</td>"+
										"<td style='text-align:left' id='attrvalue"+attrValNo+"'></td></tr>";
										$('#'+showAttrId+' table tbody').append(attr1);
										common.eav.attributes.showAttrDataDicEdit(data_name,data_id,attrValNo,dtoName);
										$('#'+data[i].attrName+showAttrId).validatebox({
											required:required
									});
									}
								}else if(data[i].attrType=="Radio"){
									var attrItemName = data[i].attrItemName;
									var field='';
									var data_name = data[i].attrName;
									for ( var _int = 0; _int < attrItemName.length; _int++) {
										var validType='';
										if(_int==attrItemName.length-1 && !data[i].attrIsNull){
											validType='validType="radioAndchekboxValid[\''+showAttrId+'\',\''+data_name+'\']" required="true" class="easyui-validatebox"';
										}
										field=field+'<label><input name="'+data_name+'" {checked} type="'+data[i].attrType+'" '+validType+' value="'+attrItemName[_int]+'" >'+attrItemName[_int]+'</label>&nbsp;&nbsp;';
										if(value===attrItemName[_int])
											field=field.replace(/{checked}/g,'checked="checked"');
										else
											field=field.replace(/{checked}/g,'');
									}
									var attr_html="<tr><td style=text-align:left;width:25%; >"+data[i].attrAsName+requiredHtml+"</td>"+
											"<td style=\"text-align:left;\">"+field+'<input name="'+dtoName+'.attrVals[\''+data_name+'\']" type="hidden" attrType="'+data[i].attrType+'" id="'+data_name+'"></td></tr>';
									$('#'+showAttrId+' table tbody').append(attrgroup+attr_html);
									
								}else if(data[i].attrType=="Checkbox"){
									var attrItemName = data[i].attrItemName;
									var field='';
									var data_name = data[i].attrName;
									for ( var _int = 0; _int < attrItemName.length; _int++) {
										var validType='';
										if(_int==attrItemName.length-1 && !data[i].attrIsNull){
											validType='validType="radioAndchekboxValid[\''+showAttrId+'\',\''+data_name+'\']" required="true" class="easyui-validatebox"';
										}
										field=field+'<label><input name="'+data_name+'" {checked} type="'+data[i].attrType+'" '+validType+' value="'+attrItemName[_int]+'" >'+attrItemName[_int]+'</label>&nbsp;&nbsp;';
										if(value.indexOf(attrItemName[_int])>-1)
											field=field.replace(/{checked}/g,'checked="checked"');
										else
											field=field.replace(/{checked}/g,'');
									}
									
									var attr_html="<tr><td style=text-align:left;width:25%; >"+data[i].attrAsName+requiredHtml+"</td>"
										+"<td style=\"text-align:left;\">"+field+'<input name="'+dtoName+'.attrVals[\''+data_name+'\']" type="hidden" attrType="'+data[i].attrType+'" id="'+data_name+'"></td></tr>';
									$('#'+showAttrId+' table tbody').append(attrgroup+attr_html);
								}
								else{
									$('#'+showAttrId+' table tbody').append(attr1);
									$('#'+data[i].attrName+showAttrId).validatebox({
										required:required
									});
								}
								$.parser.parse($('#'+showAttrId));
								
							}
						}else{
							$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n['noData']+"</td></tr>");
						}
					});
				}else{
					$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n['noData']+"</td></tr>");
				}
				
			});
		},
		/**
		 * @description 根据url查询数据字典数据
		 * @param attrdata_url  后台查询路径
		 * @param data_id 下拉框id
		 */
		loadDataDicEdit:function(attrdata_url,data_id){
			$.post(attrdata_url,function(res){
				$.each(res,function(key,value){
					$('#'+data_id).append("<option value='"+value.dcode+"'>"+value.dname+"</option>");
				});  
			});
		},
		/**
		 * @description 根据attrValNo查询数据字典数据
		 * @param data_name  
		 * @param data_id 下拉框id
		 * @param attrValNo 扩展属性id
		 * @param dtoName 扩展属性名
		 */
		showAttrDataDicEdit:function(data_name,data_id,attrValNo,dtoName){
			$.post('dataDictionaryItems!findByDcode.action?groupNo='+attrValNo,function(res){
				var attrdata_url="dataDictionaryItems!findByGroupNo.action?dataDictionaryQueryDto.groupNo="+res.groupNo;
				$("#attrvalue"+attrValNo).append("<select style=width:75%; name="+dtoName+".attrVals['"+data_name+"'] id='"+data_id+"'><option value=''>--"+i18n['pleaseSelect']+"--</option></select>");
				common.eav.attributes.showDataDicEdit(attrdata_url,data_id,res.dcode);
			});
		},
		/**
		 * @description 根据url查询数据字典数据
		 * @param attrdata_url  后台查询路径
		 * @param data_id 下拉框id
		 * @param dcode 数据字典dcode
		 */
		showDataDicEdit:function(attrdata_url,data_id,dcode){
			$.post(attrdata_url,function(resList){
				$.each(resList,function(key,value){
					if( value.dcode == dcode){
						$('#'+data_id).append("<option value='"+value.dcode+"' selected='selected'>"+value.dname+"</option>");
					}else{
						$('#'+data_id).append("<option value='"+value.dcode+"'>"+value.dname+"</option>");
					}
				}); 
			});
		},
		/**
		 * @description 详细页面，拓展属性详细
		 * @param eno
		 * @param entityType 类型
		 * @param showAttrId 显示div的id
		 */
		showAttributeInfoByEno:function(eno,entityType,showAttrId){
//			var _p = 'attrsQueryDTO.eavNo='+entityType.replace('itsm.','');
//			if(typte=='sss')
//				_p ='attrsQueryDTO.eavNo='+entityType.replace('itsm.','');
			$('#'+showAttrId+' table tbody').html('');
			$.post('eventEav!findEventEav.action','eventEavDTO.entityType='+entityType+'&eventEavDTO.eno='+eno,function(eventEavVals){
				if(entityType!="null" && entityType!=null && entityType!=''){
				$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+entityType.replace('itsm.',''),function(data){
					if(data!=null && data.length>0 && data!=''){
						var oldGroupName="";
						
						for(var i=0;i<data.length;i++){
							
							var eventEavHtml="";
							if(data[i].attrGroupName!=oldGroupName){
								eventEavHtml+="<tr><th colspan=\"2\" style=\"text-align:left;\">"+data[i].attrGroupName+"</th></tr>";
							}
							var vals=data[i].attrName
							
							if(data[i].attrType=="DataDictionaray"){
								var attrValNo = Number(eval('eventEavVals.'+vals));
								if(attrValNo==0 || isNaN(attrValNo)){
									eventEavHtml+="<tr>"+
									"<td width='30%' style='text-align:left'>"+data[i].attrAsName+"</td>"+
									"<td style='text-align:left;word-break:break-all;'></td>"+
									"</tr>";
									$('#'+showAttrId+' table tbody').append(eventEavHtml);;
								}else{
									var ids=showAttrId+data[i].attrName;//防止变更和问题都用一个扩展类型，赋值就会有问题，前面加上 showAttrId
									eventEavHtml+="<tr>"+
									"<td width='30%' style='text-align:left' >"+data[i].attrAsName+"</td>"+
									"<td style='text-align:left;word-break:break-all;' id='"+ids+"'></td>"+
									"</tr>";
									$('#'+showAttrId+' table tbody').append(eventEavHtml);
									common.eav.attributes.showDataDicInfo(attrValNo,ids);
								}
							}else{
								eventEavHtml+="<tr>"+
								"<td width='30%' style='text-align:left'>{0}</td>"+
								"<td style='text-align:left;word-break:break-all;'>{1}</td>"+
								"</tr>"
								if(eventEavVals!=null){
									eventEavHtml=eventEavHtml.replace('{0}',data[i].attrAsName)
									.replace('{1}',eval('eventEavVals.'+vals))
									.replace('undefined','')
								}else{
									eventEavHtml=eventEavHtml.replace('{0}',data[i].attrAsName)
									.replace('{1}','')
									.replace('undefined','')
								}
								$('#'+showAttrId+' table tbody').append(eventEavHtml);
							}
							oldGroupName=data[i].attrGroupName;
						
						}

					}else{
						$('#'+showAttrId+' table tbody tr').remove();
						$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n['noData']+"</td></tr>");
					}
				});
				}else{
					$('#'+showAttrId+' table tbody tr').remove();
					$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n['noData']+"</td></tr>");
				}
			});
		},
		/**
		 * @description 根据扩展属性id查询数据字典名称
		 * @param attrValNo
		 * @param showAttrId 显示控件id
		 */
		showDataDicInfo:function(attrValNo,ids){
			$.post('dataDictionaryItems!findByDcode.action?groupNo='+attrValNo,function(res){
				$("#"+ids).append(res.dname);
			});
		},
		/**
		 * @description 详细页面，拓展属性详细
		 * @param eno
		 * @param entityType 类型
		 * @param showAttrId 显示div的id
		 */
		showAttributesByEditScheduledTask:function(id,entityType,showAttrId){
			$('#'+showAttrId+' table tbody').html('');
			$.post('scheduledTask!findScheduledTaskById.action','queryDTO.scheduledTaskId='+id,function(res){
				$.post('attrs!findAttributesByEntityType.action','entityType='+entityType.replace('itsm.',''),function(data){
					if(data!=null && data.length>0 && data!=''){
						var oldGroupName="";
						var eventEavVals=res.requestDTO.attrVals;
						for(var i=0;i<data.length;i++){
							var vals=data[i].attrName
							if(data[i].attrGroupName!=oldGroupName){
								eventEavHtml+="<tr><th colspan=\"2\" style=\"text-align:left;\">"+data[i].attrGroupName+"</th></tr>";
							}
							attr="<tr><td style='text-align:left'>"+data[i].attrAsName+"</td><td style='text-align:left'><input style=width:85% name=requestDTO.attrVals['"+data[i].attrName+"'] class='easyui-datebox' "
							oldGroupName=data[i].attrGroupName;
							if(eventEavVals!=null){
								if(eval('eventEavVals.'+vals)!=undefined && eval('eventEavVals.'+vals)!=''){
									attr+=" value="+eval('eventEavVals.'+vals)
								}else{
									attr+=" value=''"
								}
							}
							if(data[i].attrType=="Lob"){
								if(eval('eventEavVals.'+vals)!=undefined){
									$('#'+showAttrId+' table tbody').append("<tr><td style='text-align:left'>"+data[i].attrAsName+"</td><td style='text-align:left'><textarea  style=width:85% name=requestDTO.attrVals['"+data[i].attrName+"'] >{eavVals}</textarea></td></tr>".replace('{eavVals}',eval('eventEavVals.'+vals)))
								}else{
									$('#'+showAttrId+' table tbody').append("<tr><td style='text-align:left'>"+data[i].attrAsName+"</td><td style='text-align:left'><textarea  style=width:85% name=requestDTO.attrVals['"+data[i].attrName+"'] ></textarea></td></tr>")
								}
							}else{
								if(data[i].attrType=="String")	
									attr+=""
								else if(data[i].attrType=="Date")	
									attr+=""
								else if(data[i].attrType=="Integer")
									attr+="min='0' maxlength='9'"
								else if(data[i].attrType=="Double")	
									attr+="precision='3'"
								else if(data[i].attrType=="DataDictionaray")	
									attr+=""
								attr+=" /></td></tr>"
									
								$('#'+showAttrId+' table tbody').append(attr);
							}
						}
					}else{
						$('#'+showAttrId+' table thead').remove();
						$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n['noData']+"</td></tr>");
					}
				});
			});
		}
	}
}();