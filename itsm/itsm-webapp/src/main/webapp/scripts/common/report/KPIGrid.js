/**  
 * @author Van  
 * @constructor WSTO
 * @description 自定义报表.
 * @date 2010-11-17
 * @since version 1.0 
 */ 
$package('common.report');



$import('common.config.customFilter.filterGrid_Operation');
$import('common.report.reportVariableGrid');
$import('common.report.fieldsProperties');

/**  
 * @fileOverview "KPI管理"
 * @author Van  
 * @constructor WSTO
 * @description KPI管理
 * @date 2011-06-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.KPIGrid=function(){
	this.loadShareGroupGrid=false;
	this.itsm_report_KPIGrid_postUrl='kpireport!save.action';
	this.loadVariableGridFlag=false;
	this.module=false;
	return {
		/**
		 * @description 显示报表
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		actionFormatter:function(cell,event,data){
		if(data.customFilterName!=null && data.customFilterNo!=null){
			//加载过滤器数据
			return "<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','kpireport!show.action?reportId="+data.reportId+"')\">"+i18n['label_dynamicReports_request_show']+"</a>"+"&nbsp;&nbsp;<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','kpireport!showdynamic.action?reportId="+data.reportId+"&width=600')\">"+i18n['View_Portal_effect']+"</a>";
		}else{//直接预览
			return "<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','kpireport!show.action?reportId="+data.reportId+"')\">"+i18n['label_dynamicReports_request_show']+"</a>"+"&nbsp;&nbsp;<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','kpireport!showdynamic.action?reportId="+data.reportId+"&width=600')\">"+i18n['View_Portal_effect']+"</a>";	
		}
		},
		/**
		 * 显示报表.
		 */
		calssNameFormatter:function(cell,event,data){
			return i18n[report_group_fields[cell]['className']];
		},
		
		
		/**
		 * 加载列表.
		 */
		showGrid:function(){
		
			var params = $.extend({},jqGridParams, {	
				url:'kpireport!findPager.action',
				colNames:[i18n['label_dynamicReports_reportName'],i18n['title'],i18n['label_dynamicReports_reportType'],
				          i18n['title_creator'],i18n['title_updator'],i18n['common_createTime'],i18n['common_updateTime'],i18n['custom_report_previewReport'],'','','','','','','','','','',''],
				colModel:[
			   		{name:'reportName',align:'center',width:30},
			   		{name:'title',align:'center',width:30,},
			   		{name:'entityClass',align:'center',width:20,formatter:common.report.KPIGrid.calssNameFormatter},
			   		{name:'creator',align:'center',width:30},
			   		{name:'lastUpdater',align:'center',width:30},
			   		{name:'createTime',align:'center',width:25},
			   		{name:'lastUpdateTime',align:'center',width:25},
			   		{name:'act', align:'center',width:30,sortable:false,formatter:common.report.KPIGrid.actionFormatter},
			   		{name:'reportId',hidden:true},
			   		{name:'entityClass',hidden:true},
			   		{name:'customFilterName',hidden:true},
			   		{name:'customFilterNo',hidden:true},
			   		{name:'KPIName',hidden:true},
			   		{name:'KPIExpression',hidden:true},
			   		{name:'groupField',hidden:true},
			   		{name:'formatter',hidden:true},
			   		{name:'reportModule',hidden:true},
			   		{name:'reportSharing',hidden:true},
			   		{name:'type',hidden:true}
			   	],		
				jsonReader: $.extend(jqGridJsonReader, {id: "reportId"}),
				sortname:'reportId',
				pager:'#KPIGridPager'
			});
			$("#KPIGrid").jqGrid(params);
			$("#KPIGrid").navGrid('#KPIGridPager',navGridParams);
			//列表操作项
			$("#t_KPIGrid").css(jqGridTopStyles);
			$("#t_KPIGrid").append($('#KPIGridToolbar').html());

		},
		
		/**
		 * 打开添加窗口.
		 */
		add_open_window:function(){
		    // 清空更新者
		    $('#add_edit_KPIReport_lastUpdater').val('');
			//清空
			$('#add_edit_KPIReport_customFilterNo').html('<option value="">-- '+i18n['label_dynamicReports_chooseFilter']+' --</option>');
			$('#add_edit_KPIReport_entityClass_groupField').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			//禁止选择其它
			$('#add_edit_KPIReport_entityClass').unbind('change');
			$('#add_edit_KPIReport_entityClass').change(common.report.KPIGrid.reportTypeChange);
			
			$('#kpiEntityClass').show();
			$('#add_edit_KPIReport_entityClass').attr("disabled","");
			
			resetForm('#KPIGrid_add_edit_window form');
			$('#add_edit_KPIReport_reportId').val('');
			//$('#add_edit_KPIReport_entityClass').attr("disabled","");
			windows('KPIGrid_add_edit_window',{modal: false});
			$("#KPI_shareGroupGrid_sign").val("");//报表共享组标记 will
			$('#selectShareGroupReportKPI').val('');
			itsm_report_KPIGrid_postUrl='kpireport!save.action';
			$('#add_edit_KPIReport_creator').val(loginName);	//添加创建者
			//$('#add_edit_KPIReport_lastUpdater').val(loginName);	//添加更新者
			$('#add_edit_reportVariable_isRelated').val('');
			$('#add_edit_reportVariable_variableId').val('');
			module=false;
			$('#kpiReport_module').show();
			if(loadVariableGridFlag==false){
				loadVariableGridFlag=true;
				//加载
				common.report.reportVariableGrid.loadGrid('-1','NO');
				common.report.reportVariableGrid.refreshGrid();
			}else{
				common.report.reportVariableGrid.refreshGrid();
			}
		},
		
		/**
		 * 添加自定义报表.
		 */
		add:function(){
			var variableIds = $("#reportVariableGrid").getDataIDs();
			if($('#KPIGrid_add_edit_window form').form('validate')){
				
				
				//标题
				var groupFieldName=$('#add_edit_KPIReport_entityClass_groupField').find("option:selected").text();
				$('#add_edit_KPIReport_groupFieldName').val(groupFieldName);
				var boo=$("#reportModule_checkbox_kpi input[type='checkbox']").is(':checked');
				$('#reportModule_checkbox_kpi div').html('');
				if(boo){	
					$('#reportModule_checkbox_kpi div').append('<input name="kpiReportDTO.reportType" type="hidden" value="kpiReport" />');
					$('#reportModule_checkbox_kpi div').append('<input name="kpiReportDTO.reportModule" type="hidden" value="true" />');
				}else{
					$('#reportModule_checkbox_kpi div').append('<input name="kpiReportDTO.reportType" type="hidden" value="false" />');
					$('#reportModule_checkbox_kpi div').append('<input name="kpiReportDTO.reportModule" type="hidden" value="" />');
				}
				
/*				var gropBoo=$('#kpiGroupReport_Sharing_group').attr("checked");
				var oBooleanFalse = new Boolean(gropBoo); 
				if(oBooleanFalse!=false){
					var grop=$('#selectShareGroupReportKPI').val();
					if(grop==" "){
						msgShow(i18n['report_not_group_msg'],'show');
						return false;
					}
				}*/
				if($("#kpiGroupReport_Sharing_group").attr("checked")){
					var ids = $("#KPI_shareGroupGrid").getDataIDs();
					if(ids==""){
						msgShow(i18n['report_not_group_msg'],'show');
						return false;
					}
					$('#selectShareGroupReportKPI').val(ids);
				}else{
					$('#selectShareGroupReportKPI').val('');
				}
				if(variableIds==""){
					msgShow(i18n['label_dynamicReports_variableGrid_is_notnull'],'show');
					return false;
				}
				var _param = $('#KPIGrid_add_edit_window form').serialize();
				var _paramVariableIds = $.param({'kpiReportDTO.variableIds':variableIds},true);
				var _KPIExpression = $.param({'kpiReportDTO.KPIExpression':$("#add_edit_KPIReport_KPIExpression").val()},true);
				startProcess();
				$.post(itsm_report_KPIGrid_postUrl,_param+'&'+_paramVariableIds+'&'+_KPIExpression,function(data){	
					endProcess();
					$('#KPIGrid').trigger('reloadGrid');
					$('#KPIGrid_add_edit_window').dialog('close');
					common.report.clickEvent.loadReportModuleLeftMenu();
					msgShow(i18n['label_dynamicReports_addEditSuccessful'],'show');
				});			
			}
		},
		
		/**
		 * 编辑.
		 */
		edit_open_window:function(){

			
			checkBeforeEditGrid('#KPIGrid', function(data){
				resetForm('#KPIGrid_add_edit_window form');
				//$('#add_edit_KPIReport_entityClass').attr("disabled","disabled");
				$('#kpiEntityClass').show();
				$('#add_edit_KPIReport_entityClass').removeClass('validatebox-invalid');
				$('#add_edit_KPIReport_entityClass').attr("disabled","true");
				$('#add_edit_KPIReport_entityClass').val($vl(data.entityClass));
				if($vl(data.entityClass)=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					$('#kpiReport_module').hide();
					module=true;
				}
				if(module&&$vl(data.entityClass)!="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					module=false;
					$('#kpiReport_module').show();
				}
				$('#add_edit_kpiReport_reportModule').val($vl(data.reportModule));
				$('#add_edit_KPIReport_reportName').val($vl(data.reportName));
				$('#add_edit_KPIReport_title').val($vl(data.title));
				$('#add_edit_KPIReport_reportId').val($vl(data.reportId));
				$('#add_edit_KPIReport_KPIName').val($vl(data.KPIName));
				$('#add_edit_KPIReport_KPIExpression').val($vl(data.KPIExpression));
				$('#add_edit_KPIReport_formatter').val($vl(data.formatter));
				$('#add_edit_KPIReport_lastUpdater').val(loginName);	//添加更新者
				
				var reportSharing=data.reportSharing;
				if(reportSharing=="share")
					$('#kpiGroupReport_Sharing').attr("checked",'true');
				else if(reportSharing.substr(0,5)=="name_")
					$('#kpiGroupReport_private').attr("checked",'true');
				else
					$('#kpiGroupReport_Sharing_group').attr("checked",'true');
				
				var bo=Boolean($vl(data.reportModule));
				$('#reportModule_add_kpi').attr('checked',bo);
				//禁止选择其它
				$('#add_edit_KPIReport_entityClass').change(function (){
					$('#add_edit_KPIReport_entityClass').val($vl(data.entityClass)); 
				});
				common.report.KPIGrid.reportTypeChange();
				
				//打开窗口
				itsm_report_KPIGrid_postUrl='kpireport!merge.action';
				windows('KPIGrid_add_edit_window',{modal: false});
				$("#KPI_shareGroupGrid_sign").val("");//报表共享组标记 will
				setTimeout(function(){//延迟加载
					if($vl(data.customFilterNo)!=0){
					$('#add_edit_KPIReport_customFilterNo').val($vl(data.customFilterNo));
					}
					$('#add_edit_KPIReport_entityClass_groupField').val($vl(data.groupField));
					
					
					common.report.reportVariableGrid.showGrid(data.entityClass);

				},1200);
			});
		},
		
		/**
		 * 删除报表.
		 */
		del:function(){
			
			checkBeforeDeleteGrid('#KPIGrid', function(ids){
				
				var _param = $.param({'reportIds':ids},true);
				$.post("kpireport!delete.action", _param, function(){
					common.report.clickEvent.loadReportModuleLeftMenu();
					$('#KPIGrid').trigger('reloadGrid');
					msgShow(i18n['msg_deleteSuccessful'],'show');
					
				}, "json");
			});
			
		},
		
		/**
		 * 选择过滤器
		 */
		reportTypeChange:function(){
			
			var entityClass=$('#add_edit_KPIReport_entityClass').val();
			if(entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
				$('#kpiReport_module').hide();
				module=true;
			}
			if(module&&entityClass!="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
				module=false;
				$('#kpiReport_module').show();
			}
			//清空
			$('#add_edit_KPIReport_customFilterNo').html('<option value="">-- '+i18n['label_dynamicReports_chooseFilter']+' --</option>');

			if(entityClass!=null && entityClass!=''){
				
				var filterType=report_group_fields[entityClass]['filterType'];
				
				//加载过滤器
				common.config.customFilter.filterGrid_Operation.loadFilterByModule("#add_edit_KPIReport_customFilterNo",filterType);
				//根据类型查询对应的表达式
				common.report.reportVariableGrid.queryByEntityClass(entityClass);
				common.report.reportVariableGrid.loadGroupField(entityClass);
				//加载分组字段
				$('#add_edit_KPIReport_entityClass_groupField').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
				$.each(report_group_fields[entityClass]['xgroupFields'],function(k,v){
					$('<option value="'+k+'">'+i18n[v]+'</option>').appendTo('#add_edit_KPIReport_entityClass_groupField');
				});
			}else{
				common.report.reportVariableGrid.queryByEntityClass('NO');
				//加载分组字段
				$('#add_edit_KPIReport_entityClass_groupField').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			}
			
		},
		
		/**
		 * 打开搜索窗口.
		 */
		search_open_window:function(){
			windows('KPIGrid_search_window',{width:320,modal: false});
		},
		
		/**
		 * 执行搜索.
		 */
		search:function(){

			var _search_data = $('#KPIGrid_search_window form').getForm();
			var _postData = $("#KPIGrid").jqGrid("getGridParam", "postData");       
			$.extend(_postData, _search_data);	
			$('#KPIGrid').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		
		/**
		 * 快速搜索.
		 */
		searchByEntityClass:function(){
			
			resetForm('#KPIGrid_search_window form');//重置搜索表单
			$('#KPIGrid_search_entityClass').val($('#KPIGrid_searchByEntityClass').val());
			common.report.KPIGrid.search();
		},
		
		/**
		 * 显示共享组列表
		 * */
		showShareGroup:function(){
			windows('KPI_shareGroupWindow',{width:500,height:450,close:function(){
				$("#kpiGroupReport_Sharing_group").attr("checked","checked");
			}});
			//$('#singleGroupReport_Sharing_group').attr('checked','');
			var groupId=$('#add_edit_KPIReport_reportId').val();
			if(!basics.ie6.htmlIsNull("#KPI_shareGroupGrid")){
				common.report.KPIGrid.showGridAction(groupId);
				$("#KPI_shareGroupGrid_sign").val("KPI_shareGroupGrid_sign");
			}else if($("#KPI_shareGroupGrid_sign").val()===""){
				$("#KPI_shareGroupGrid_sign").val("KPI_shareGroupGrid_sign");
				if (groupId != null && groupId != '') {
					var postData = $("#KPI_shareGroupGrid").jqGrid("getGridParam", "postData");       
					$.extend(postData, {"reportId":groupId});  //将postData中的查询参数覆盖为空值
					$('#KPI_shareGroupGrid').trigger('reloadGrid');
				} else {
				    $('#KPI_shareGroupGrid').jqGrid("clearGridData");
				}
			}
			
		},
		
		/**
		 * 查询共享组列表
		 * @param groupId 组编号
		 * */
		showGridAction:function(groupId){
			
			
			var _postData={};
			
			if(groupId!=null && groupId!=''){
				_postData={'reportId':groupId}
			}
			
			var params = $.extend({},jqGridParams, {	
				url:'kpireport!findShareGroups.action',
				//caption:'分享组',
				postData:_postData,
				colNames:['ID',i18n['name']],
				colModel:[{name:'orgNo',width:100,align:'center',sortable:false},
				          {name:'orgName',width:300,align:'center',sortable:false}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "orgNo"}),
				sortname:'orgNo',
				autowidth:true,
				rowNum:10000
			});
			
			$("#KPI_shareGroupGrid").jqGrid(params);
			$("#KPI_shareGroupGrid").navGrid('#KPI_shareGroupGridPager',navGridParams);
			//列表操作项
			$("#t_KPI_shareGroupGrid").css(jqGridTopStyles);
			$("#t_KPI_shareGroupGrid").html($('#KPI_shareGroupGridToolbar').html());
			
			$('#KPI_shareGroupGrid_save').unbind().click(common.config.customFilter.shareGroup.save);
			$('#KPI_shareGroupGrid_add').unbind().click(function(){common.config.customFilter.shareGroup.showSelectWindow("KPI_");});
			$('#KPI_shareGroupGrid_remove').unbind().click(function(){common.config.customFilter.shareGroup.removeShareGroups("KPI_");});
		},
		
		
		init:function(){
			$('.loading').hide();
			$('.content').show();
			
			setTimeout(function(){
						
				common.report.KPIGrid.showGrid('NO');
				//移除事件
				$('#KPIGrid_add,#KPIGrid_edit,#KPIGrid_delete,#KPIGrid_search,#KPIGrid_doSearch,#KPIGrid_searchByEntityClass,#add_edit_KPIReport_entityClass,#KPIGrid_save').unbind();
				$("#add_edit_KPIReport_KPIExpression").attr("disabled","true");
				$('#KPIGrid_add').click(common.report.KPIGrid.add_open_window);
				$('#KPIGrid_edit').click(common.report.KPIGrid.edit_open_window);
				$('#KPIGrid_delete').click(common.report.KPIGrid.del);
				$('#KPIGrid_search').click(common.report.KPIGrid.search_open_window);
				$('#KPIGrid_doSearch').click(common.report.KPIGrid.search);
				$('#KPIGrid_searchByEntityClass').change(common.report.KPIGrid.searchByEntityClass);
				$('#add_edit_KPIReport_entityClass').change(common.report.KPIGrid.reportTypeChange);
				$('#KPIGrid_save').click(common.report.KPIGrid.add);//执行保存
				$('#add_edit_KPIReport_addCustomFilter').click(function(){
					
					openReportFilter($("#add_edit_KPIReport_entityClass").val(),"add_edit_KPIReport_customFilterNo");
				});
						
			},0);	

			$('#kpiGroupReport_Sharing_group').click(common.report.KPIGrid.showShareGroup);
		}
		
	};
	
}();

$(document).ready(common.report.KPIGrid.init);