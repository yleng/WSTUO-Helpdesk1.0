/**  
 * @author Van  
 * @constructor WSTO
 * @description 自定义报表.
 * @date 2010-11-17
 * @since version 1.0 
 */ 
$package('common.report');

$import('common.config.customFilter.filterGrid_Operation');
$import('common.report.fieldsProperties');
/**  
 * @fileOverview "交叉报表管理"
 * @author Van  
 * @constructor WSTO
 * @description 交叉报表管理
 * @date 2011-09-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.crosstabGrid=function(){
	this.loadShareGroupGrid=false;
	this.crosstab_report_operation='save';
	this.module=false;
	return {
		/**
		 * @description 显示报表.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		actionFormatter:function(cell,event,data){
			if(data.customFilterName!=null && data.customFilterNo!=null){
				//加载过滤器数据
				return "<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','crossreport!show.action?reportId="+data.reportId+"')\">"+i18n['label_dynamicReports_request_show']+"</a>"+"&nbsp;&nbsp;<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','crossreport!showdreport.action?reportId="+data.reportId+"')\">"+i18n['View_Portal_effect']+"</a>";
			}else{//直接预览
			    return "<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','crossreport!show.action?reportId="+data.reportId+"')\">"+i18n['label_dynamicReports_request_show']+"</a>"+"&nbsp;&nbsp;<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','crossreport!showdreport.action?reportId="+data.reportId+"')\">"+i18n['View_Portal_effect']+"</a>";
			}
		},
		/**
		 * @description 显示报表.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		calssNameFormatter:function(cell,event,data){
			return i18n[report_group_fields[cell]['className']];
		},
		
		
		/**
		 * 加载列表.
		 */
		showGrid:function(){
		
			var params = $.extend({},jqGridParams, {	
				url:'crossreport!findPager.action',
				colNames:[i18n['label_dynamicReports_reportName'],i18n['title'],i18n['label_dynamicReports_reportType'],
				          i18n['title_creator'],i18n['title_updator'],i18n['common_createTime'],i18n['common_updateTime'],i18n['custom_report_previewReport'],'','','','','','','','','','',''],
				colModel:[
			   		{name:'reportName',align:'center',width:30},
			   		{name:'title',align:'center',width:30,},
			   		{name:'entityClass',align:'center',width:20,formatter:common.report.crosstabGrid.calssNameFormatter},
			   		{name:'creator',align:'center',width:30},
			   		{name:'lastUpdater',align:'center',width:30},
			   		{name:'createTime',align:'center',width:25},
			   		{name:'lastUpdateTime',align:'center',width:25},
			   		{name:'act', align:'center',width:30,sortable:false,formatter:common.report.crosstabGrid.actionFormatter},
			   		{name:'reportId',hidden:true},
			   		{name:'entityClass',hidden:true},
			   		{name:'customFilterName',hidden:true},
			   		{name:'customFilterNo',hidden:true},
			   		{name:'rowGroup',hidden:true},
			   		{name:'columnGroup',hidden:true},
			   		{name:'statisticField',hidden:true},
			   		{name:'statisticType',hidden:true},
			   		{name:'reportModule',hidden:true},
			   		{name:'reportSharing',hidden:true},
			   		{name:'type',hidden:true}


			   	],		
				jsonReader: $.extend(jqGridJsonReader, {id: "reportId"}),
				sortname:'reportId',
				pager:'#crosstabGridPager'
			});
			$("#crosstabGrid").jqGrid(params);
			$("#crosstabGrid").navGrid('#crosstabGridPager',navGridParams);
			//列表操作项
			$("#t_crosstabGrid").css(jqGridTopStyles);
			$("#t_crosstabGrid").append($('#crosstabGridToolbar').html());

		},
		
		/**
		 * 打开添加窗口.
		 */
		add_open_window:function(){
			//清空
			$('#add_edit_crosstab_customFilterNo').html('<option value="">-- '+i18n['label_dynamicReports_chooseFilter']+' --</option>');
			$('#add_edit_crosstab_rowGroup').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			$('#add_edit_crosstab_columnGroup').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			//取消禁止选择
			$('#add_edit_crosstab_entityClass').unbind('change');
			$('#add_edit_crosstab_entityClass').change(common.report.crosstabGrid.reportTypeChange);
			module=false;
			$('#crosstabReport_module').show();
			$('#crossEntityClass').show();
			$('#add_edit_crosstab_entityClass').attr("disabled","");
			
			/*$('#add_edit_crosstab_creator').val(loginName);	//添加创建者
			$('#add_edit_crosstab_lastUpdater').val(loginName);	//添加更新者
*/			//$('#add_edit_crosstab_entityClass').attr("disabled","");
			resetForm('#crosstabGrid_add_edit_window form');
			windows('crosstabGrid_add_edit_window',{width:450});
			$("#crosstab_shareGroupGrid_sign").val("");//报表共享组标记 will
			$('#add_edit_crosstab_reportId').val("");
			$('#selectShareGroupReportCrosstab').val("");
			crosstab_report_operation='save';
		},
		
		/**
		 * 添加自定义报表.
		 */
		add:function(){
		
			if($('#crosstabGrid_add_edit_window form').form('validate')){
				if(crosstab_report_operation=='save')
					$('#crosstablastUpdater').val('');
				else
					$('#crosstablastUpdater').val($('#crosstablastcreator').val());
				//标题
				var rowGroupText=$('#add_edit_crosstab_rowGroup').find("option:selected").text();
				var columnGroup=$('#add_edit_crosstab_columnGroup').find("option:selected").text();
				
				$('#add_edit_crosstab_headerText').val(rowGroupText+"  /  "+columnGroup);
				
				var boo=$("#reportModule_checkbox_crosstab input[type='checkbox']").is(':checked');
				$('#reportModule_checkbox_crosstab div').html('');
				if(boo){	
					$('#reportModule_checkbox_crosstab div').append('<input name="crosstabReportDTO.reportType" type="hidden" value="crosstab" />');
					$('#reportModule_checkbox_crosstab div').append('<input name="crosstabReportDTO.reportModule" type="hidden" value="true" />');
				}else{
					$('#reportModule_checkbox_crosstab div').append('<input name="crosstabReportDTO.reportType" type="hidden" value="false" />');
					$('#reportModule_checkbox_crosstab div').append('<input name="crosstabReportDTO.reportModule" type="hidden" value="" />');
				}
				if($("#crosstabGroupReport_Sharing_group").attr("checked")){
					var ids = $("#crosstab_shareGroupGrid").getDataIDs();
					if(ids==""){
						msgShow(i18n['report_not_group_msg'],'show');
						return false;
					}
					$('#selectShareGroupReportCrosstab').val(ids);
				}else{
					$('#selectShareGroupReportCrosstab').val('');
				}
				
				var _param = $('#crosstabGrid_add_edit_window form').serialize();
				startProcess();
				$.post('crossreport!'+crosstab_report_operation+'.action',_param,function(data){	
					endProcess();
					$('#crosstabGrid').trigger('reloadGrid');
					$('#crosstabGrid_add_edit_window').dialog('close');
					common.report.clickEvent.loadReportModuleLeftMenu();
					msgShow(i18n['label_dynamicReports_addEditSuccessful'],'show');
				});				
			}
		},
		
		/**
		 * 编辑.
		 */
		edit_open_window:function(){

			
			checkBeforeEditGrid('#crosstabGrid', function(data){
				
				
				resetForm('#crosstabGrid_add_edit_window form');
				//$('#add_edit_crosstab_entityClass').attr("disabled","disabled");
				
				$('#add_edit_crosstab_entityClass').val($vl(data.entityClass));
				$('#add_edit_crosstab_reportModule').val($vl(data.reportModule));
				
				var bo=Boolean($vl(data.reportModule));
				$('#reportModule_add_crosstab').attr('checked',bo);
				//禁止选择其它
				$('#add_edit_crosstab_entityClass').change(function (){
					$('#add_edit_crosstab_entityClass').val($vl(data.entityClass)); 
				});
				if($vl(data.entityClass)=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					$('#crosstabReport_module').hide();
					module=true;
				}
				if(module&&$vl(data.entityClass)!="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					module=false;
					$('#crosstabReport_module').show();
				}
				
				$('#crossEntityClass').show();
				$('#add_edit_crosstab_entityClass').removeClass('validatebox-invalid');
				$('#add_edit_crosstab_entityClass').attr("disabled","true");
				
				var reportSharing=data.reportSharing;
				if(reportSharing=="share")
					$('#crosstabGroupReport_Sharing').attr("checked",'true');
				else if(reportSharing.substr(0,5)=="name_")
					$('#crosstabGroupReport_private').attr("checked",'true');
				else
					$('#crosstabGroupReport_Sharing_group').attr("checked",'true');
				
				$('#add_edit_crosstab_reportName').val($vl(data.reportName));
				$('#add_edit_crosstab_title').val($vl(data.title));
				$('#add_edit_crosstab_reportId').val($vl(data.reportId));
				$('#add_edit_crosstab_statisticType').val($vl(data.statisticType));
				$('#add_edit_crosstab_lastUpdater').val(loginName);	//添加更新者
//				if(data.type=="dynamic"){
//					$('#type_crosstab').attr("checked",'false');
//					$('#type_dynamic').attr("checked",'true');
//				}

				common.report.crosstabGrid.reportTypeChange();
				
				//打开窗口
				crosstab_report_operation='merge';
				
				windows('crosstabGrid_add_edit_window',{width:450});
				$("#crosstab_shareGroupGrid_sign").val("");//报表共享组标记 will
				setTimeout(function(){//延迟加载
					if($vl(data.customFilterNo)!=0){
					   $('#add_edit_crosstab_customFilterNo').val($vl(data.customFilterNo));
					}
					$('#add_edit_crosstab_rowGroup').val($vl(data.rowGroup));
					$('#add_edit_crosstab_columnGroup').val($vl(data.columnGroup));
					$('#add_edit_crosstab_statisticField').val($vl(data.statisticField));
	

				},500);
				
				
				
			});
		},
		
		/**
		 * 删除报表.
		 */
		del:function(){
			
			checkBeforeDeleteGrid('#crosstabGrid', function(ids){
				
				var _param = $.param({'reportIds':ids},true);
				$.post("crossreport!delete.action", _param, function(){
					common.report.clickEvent.loadReportModuleLeftMenu();
					$('#crosstabGrid').trigger('reloadGrid');
					msgShow(i18n['msg_deleteSuccessful'],'show');
					
				}, "json");
				
				
			});
			
		},

		
		/**
		 * 选择过滤器
		 */
		reportTypeChange:function(){
			
			var entityClass=$('#add_edit_crosstab_entityClass').val();
			if(entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
				$('#crosstabReport_module').hide();
				module=true;
			}
			if(module&&entityClass!="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
				module=false;
				$('#crosstabReport_module').show();
			}
			//清空
			$('#add_edit_crosstab_customFilterNo').html('<option value="">-- '+i18n['label_dynamicReports_chooseFilter']+' --</option>');

			if(entityClass!=null && entityClass!=''){
				
				var filterType=report_group_fields[entityClass]['filterType'];
				
				//加载过滤器
				common.config.customFilter.filterGrid_Operation.loadFilterByModule("#add_edit_crosstab_customFilterNo",filterType);
			}
			
			//加载两个分组字段
			$('#add_edit_crosstab_rowGroup,#add_edit_crosstab_columnGroup').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			
			$.each(report_group_fields[entityClass]['xgroupFields'],function(k,v){
				$('<option value="'+k+'">'+i18n[v]+'</option>').appendTo('#add_edit_crosstab_rowGroup,#add_edit_crosstab_columnGroup');
			});
			
			//加载统计字段
			$('#add_edit_crosstab_statisticField').html('<option value="">-- '+i18n['label_dynamicReports_chooseCountField']+' --</option>');
			$.each(report_group_fields[entityClass]['xcountField'],function(k,v){
				$('<option value="'+k+'">'+i18n[v]+'</option>').appendTo('#add_edit_crosstab_statisticField');
			});
			
		},
		
		/**
		 * 打开搜索窗口.
		 */
		search_open_window:function(){
			windows('crosstabGrid_search_window',{width:320,modal: false});
		},
		
		/**
		 * 执行搜索.
		 */
		search:function(){

			var _search_data = $('#crosstabGrid_search_window form').getForm();
			var _postData = $("#crosstabGrid").jqGrid("getGridParam", "postData");       
			$.extend(_postData, _search_data);	
			$('#crosstabGrid').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		
		/**
		 * 快速搜索.
		 */
		searchByEntityClass:function(){
			
			resetForm('#crosstabGrid_search_window form');//重置搜索表单
			$('#crosstabGrid_search_entityClass').val($('#crosstabGrid_searchByEntityClass').val());
			common.report.crosstabGrid.search();
		},
		
		/**
		 * 显示共享组列表
		 * */
		showShareGroup:function(){
			
			windows('crosstab_shareGroupWindow',{width:500,height:450,close:function(){
				$("#crosstabGroupReport_Sharing_group").attr("checked","checked");
			}});
			var groupId=$('#add_edit_crosstab_reportId').val();
			if(!basics.ie6.htmlIsNull("#crosstab_shareGroupGrid")){
				common.report.crosstabGrid.showGridAction(groupId);
				$("#crosstab_shareGroupGrid_sign").val("crosstab_shareGroupGrid_sign");
			}else if($("#crosstab_shareGroupGrid_sign").val()===""){
				$("#crosstab_shareGroupGrid_sign").val("crosstab_shareGroupGrid_sign");
				if (groupId != null && groupId != '') {
					var postData = $("#crosstab_shareGroupGrid").jqGrid("getGridParam", "postData");       
					$.extend(postData, {"reportId":groupId});  //将postData中的查询参数覆盖为空值
					$('#crosstab_shareGroupGrid').trigger('reloadGrid');
				} else {
				    $('#crosstab_shareGroupGrid').jqGrid("clearGridData");
				}
			}
			
		},
		
		/**
		 * 查询共享组列表
		 * @param groupId 组Id
		 * */ 
		showGridAction:function(groupId){
			
			
			var _postData={};
			
			if(groupId!=null && groupId!=''){
				_postData={'reportId':groupId}
			}
			
			var params = $.extend({},jqGridParams, {	
				url:'crossreport!findShareGroups.action',
				//caption:'分享组',
				postData:_postData,
				colNames:['ID',i18n['name']],
				colModel:[{name:'orgNo',width:100,align:'center',sortable:false},
				          {name:'orgName',width:300,align:'center',sortable:false}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "orgNo"}),
				sortname:'orgNo',
				autowidth:true,
				rowNum:10000
			});
			
			$("#crosstab_shareGroupGrid").jqGrid(params);
			$("#crosstab_shareGroupGrid").navGrid('#crosstab_shareGroupGridPager',navGridParams);
			//列表操作项
			$("#t_crosstab_shareGroupGrid").css(jqGridTopStyles);
			$("#t_crosstab_shareGroupGrid").html($('#crosstab_shareGroupGridToolbar').html());
			
			$('#crosstab_shareGroupGrid_save').unbind().click(common.config.customFilter.shareGroup.save);
			$('#crosstab_shareGroupGrid_add').unbind().click(function(){common.config.customFilter.shareGroup.showSelectWindow("crosstab_");});
			$('#crosstab_shareGroupGrid_remove').unbind().click(function(){common.config.customFilter.shareGroup.removeShareGroups("crosstab_");});
		},
		
		
		
		
		init:function(){
			$('.loading').hide();
			$('.content').show();
			
			common.report.crosstabGrid.showGrid();
			
			//移除事件
			$('#crosstabGrid_add,#crosstabGrid_edit,#crosstabGrid_delete,#crosstabGrid_search,#crosstabGrid_doSearch,#crosstabGrid_searchByEntityClass,#add_edit_KPIReport_entityClass,#crosstabGrid_save').unbind();
		
			$('#crosstabGrid_add').click(common.report.crosstabGrid.add_open_window);
			$('#crosstabGrid_edit').click(common.report.crosstabGrid.edit_open_window);
			$('#crosstabGrid_delete').click(common.report.crosstabGrid.del);
			$('#crosstabGrid_search').click(common.report.crosstabGrid.search_open_window);
			$('#crosstabGrid_doSearch').click(common.report.crosstabGrid.search);
			$('#crosstabGrid_searchByEntityClass').change(common.report.crosstabGrid.searchByEntityClass);
			$('#add_edit_crosstab_entityClass').change(common.report.crosstabGrid.reportTypeChange);
			$('#crosstabGrid_save').click(common.report.crosstabGrid.add);//执行保存

			
			$('#add_edit_crosstab_addCustomFilter').click(function(){
				
				openReportFilter($("#add_edit_crosstab_entityClass").val(),"add_edit_crosstab_customFilterNo");
			});
			$('#crosstabGroupReport_Sharing_group').click(common.report.crosstabGrid.showShareGroup);
		}
		
	};
	
}();

$(document).ready(common.report.crosstabGrid.init);