$package('common.security');

$import('common.security.company');//公司信息

$import('common.security.organizationTree');//机构树
$import('common.security.organizationGrid');//机构列表
$import('common.security.organizationRole');//机构角色
$import('common.security.organizationHoliday');//机构节假日
$import('common.security.organizationServiceTime');//机构服务时间
$import('common.security.organizationMain');//机构设置

$import('common.security.customerMain');//客户管理
$import('common.security.supplierMain');//外包商管理
$import('common.security.user');//用户管理


$import("itsm.itsop.ITSOPCustomer_userGrid");//外包用户列表
$import('common.security.editUserPassword');//编辑用户密码
$import('itsm.itsop.ITSOPUserMain');//外包客户管理

$import('common.security.functionTree');//功能树
$import('common.security.functionGrid');//功能列表
$import('common.security.operationGrid');//操作列表
$import('common.security.resourceMain');//资源管理

$import('common.security.role');//角色

$import('common.security.ldap');//LDAP
$import('common.security.adUser');//AD USER