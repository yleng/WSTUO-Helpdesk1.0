$package('common.security');
$import('common.security.functionTree');
$import('common.security.functionGrid');
$import('common.security.operationGrid');

/**  
 * @author Van  
 * @constructor resourceMain
 * @description 资源管理主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.security.resourceMain = function() {
	//列表
	this.resourceGrids=[];
	//加载标识
	this.loadResourceGridViewFlag="0";
	this.loadResourceOperationGridFlag="0";
	
	return {
		
		/**
		 * @description 为tab点击绑定事件
		 */
		resourceTabClick:function(){
	
	            $('#resourceTabMain').tabs({
	                onSelect:function(title){
	                	
	                	//功能列表视图
	                	if(title==i18n['common_gridView']){
	        				if(loadResourceGridViewFlag=="0"){
	        					
	        					
	        					//工具栏事件
	        					common.security.functionGrid.init();
	        					
	        					loadResourceGridViewFlag="1";
	        				}
	        			}
	                	//操作列表视图
	        			if(title==i18n['caption_operationGrid']){
	        				if(loadResourceOperationGridFlag=="0"){
	        					
	        					common.security.operationGrid.init();
	        					
	        					//show_OperationList();
	        					loadResourceOperationGridFlag="1";
	        				}
	        			}
	                }  
	            });
		},
		
		/**
		 * 机构列表伸展
		 */
		resourcePanelCollapseAndExpand:function(){
			
			 $(resourceGrids).each(function(i, g) {
			      setGridWidth(g, 'resourceTabMain', 10);
			 });
		},
		/**
		 * 获取事件 
		 */
		fitResourceGrids:function(){
			
			$('#resourceWest,#resourceEast').panel({
				onCollapse:common.security.resourceMain.resourcePanelCollapseAndExpand,
				onExpand:common.security.resourceMain.resourcePanelCollapseAndExpand
			});
		},

		/**
		 * @description 验证节点ID不为空.
		 */
		validateFunctionId:function(){
				var id=$('#editCurrentFunction_resNo').val();
				if(id==null||id==""){
					msgAlert(i18n['msg_chooseFunction'],'info');
					return false;
				}else{
					return true;
				}
		},

		 /**
		  * @description 添加功能.
		  */
		 addFunction:function(){
			if($('#addChildrenFunctionForm').form('validate')){
				 //资源名称
				var resName=encodeURIComponent($('#resName').val());
				 var url_resName="function!findExist.action?functionQueryDTO.resName="+resName;
				 $.post(url_resName,function(res){
					 
					 
					if(Boolean(res)==true){
						msgAlert(i18n['msg_resource_functionNameExist'],'info');
						return;
					}else{
						
						//验证资源编码
						var url_resCode="function!findExist.action?functionQueryDTO.resCode="+$('#resCode').val();
						 //判断操作编码
						 $.post(url_resCode,function(res){ 
								if(Boolean(res)==true){
									msgAlert(i18n['msg_resource_functionCodeExist'],'info');
									return;
								}else{
									//判断资源地址 start
									var url_resUrl="function!findExist.action?functionQueryDTO.resUrl="+$('#resUrl').val();
									 //判断操作编码
									 $.post(url_resUrl,function(res){
											if(Boolean(res)==true){
												msgAlert(i18n['msg_resource_functionUrlExist'],'info');
												return;
											}else{
												common.security.resourceMain.addFunctionMethod();
											}
									 });
								}
						 });
					} 
				 });
			}
		 },

		 /**
		  * @description 执行添加功能.
		  */
		 addFunctionMethod:function(){
			 
			 var frm = $('#addFunctionDiv form').serialize();
			 var url = 'function!addFunction.action';
			 
			 $.post(url,frm, function(){
				 
				 	$('#functionGrid').trigger('reloadGrid');
				 	common.security.functionTree.showFunctionTree('#functionTreeDIV');//刷新功能树结构			 	
				 	msgShow(i18n['addSuccess'],'show');
				 	
				 	$("#show_icon").html("");//清空图标		 	
				 	resetForm('#addChildrenFunctionForm');//清空表单
			 });
			 
		 },
		 /**
		  * 编辑功能树
		  */
		 editFunction:function(){
			 if(common.security.resourceMain.validateFunctionId()&& $('#editCurrentFunction').form('validate')){
				 
				 var frm = $('#editFunctionDiv form').serialize();
				 var url = 'function!updateFunction.action';
				 $.post(url,frm, function(){
					 
					 	$('#functionGrid').trigger('reloadGrid');	
					 	$("#show_edit_icon").html("");
					 	resetForm('#editCurrentFunction');
					 	common.security.functionTree.showFunctionTree('#functionTreeDIV');//刷新功能树结构		 	
					 	msgShow(i18n['saveSuccess'],'show');
				 });
			 }
			 
		 },
		
		 
		 /**
		  * 导入导出.
		  */
		 
		init:function(){
			//载入
			$("#resourceMain_loading").hide();
			$("#resourceMain_content").show();
			
			
			//编辑当前功能
			$('#editFunctionBtn').click(common.security.resourceMain.editFunction);
			//添加子功能
			$('#addFunctionBtn').click(common.security.resourceMain.addFunction);			
			//加载机构树
			common.security.functionTree.showFunctionTree('#functionTreeDIV');
			//面板点击
			common.security.resourceMain.resourceTabClick();
			//隐显面板
			setTimeout(common.security.resourceMain.fitResourceGrids,0);
			
		}
	};
  
}();
//载入
$(document).ready(common.security.resourceMain.init);



