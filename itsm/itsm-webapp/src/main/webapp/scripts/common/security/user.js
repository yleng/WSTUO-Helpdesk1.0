$package("common.security");
$import('common.security.organizationTreeUtil');
$import('common.security.editUserPassword');
$import('common.security.includes.includes');
$import('common.config.systemGuide.systemGuide');
$import('common.security.organizationTree');
/**  
 * @author QXY  
 * @constructor users
 * @description 用户管理主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.security.user=function(){
	var operator;
	this.userRoleIds = new Array();
	this.mydata = new Array();
	this.userGrids=[];
	this.loadUserRole=0;
	this.ProxyType=0;
	this.Proxyshow=0;
	this.usedTechnicianNumber=10000;
	this.user_search_selectData_flag=true;
	
	return{

		countTc:function(){
			$.post('user!countTechnician.action',function(data){
				$('#TechnicianNumber_toolbar').text(i18n.toolbar_echnician_License.replace('{0}',technicianNumber).replace('{1}',data).replace('{0}',data).replace('{2}',technicianNumber-data));
				usedTechnicianNumber=data;
			});
		
		},
		
		/**
		 * @description 状态格式化.
		 * @param cellvalue 当前列
		 * @param options 下拉选项
		 */
		userStateFormat:function(cellvalue, options){
			if(cellvalue){
				return i18n.common_enable;
			}
			else{
				return i18n.common_disable;
			}
		},
		/**
		 * @description 动作格式化.
		 */
		userGridFormatter:function(cell,opt,data){
			return $('#userToolbarAct').html().replace(/{userId}/g,opt.rowId);
		},
		
		/**
		 * @description 性别格式化
		 * @param cell 当前列值
		 * @param opt 下拉选项
		 * @param data 行数据
		 * @private
		 */
		sexForma:function(cell,opt,data){
          if(cell){
            return i18n.label_user_man;
          }else{
			return i18n.label_user_woman;
          }
		},
		
		/**
		 * @description 加载用户列表.
		 */
		showUserGrid:function(){
			
			var params = $.extend({},jqGridParams, {	
				url:'user!find.action?userQueryDto.companyNo='+companyNo,
				colNames:['ID',i18n.title_user_loginName,i18n.title_user_lastName,i18n.title_user_firstName,i18n.label_user_sex,i18n.title_user_org,
                        i18n.label_userOwnerGroup,
                        i18n.title_user_phone,i18n.title_user_mobile,i18n.title_customer_email,i18n.title_org_role,i18n.common_state,i18n.label_user_holidayStatus,
                          '‘’','','‘’','','','','','','','','','',i18n.common_action],
                colModel:[
                        {name:'userId',align:'center',width:40},
                        {name:'loginName',align:'left',width:80},
                        {name:'firstName',align:'center',width:80},
                        {name:'lastName',align:'center',width:80},
                        {name:'sex',align:'center',width:30,formatter:common.security.user.sexForma,hidden:true},
                        {name:'orgName',align:'left',index:'orgnization',width:120},
                        {name:'belongsGroupStr',index:'belongsGroup',align:'left',width:120,formatter:function(cellvalue, options, rowObject){
							 return rowObject.belongsGroupStr.substring(0,rowObject.belongsGroupStr.length-1);
						   }},
                        {name:'phone',align:'left',width:80},
                        {name:'moblie',align:'left',width:80},
                        {name:'email',align:'left',width:120},
                        {name:'roles',align:'left',width:120},
                        {name:'userState',align:'center',width:40,formatter:common.security.user.userStateFormat},
                        {name:'holidayStatus',align:'center',width:80,formatter:function(cell,opt,data){
                          if(cell=='true' || cell){
                            return i18n.label_user_holiday_ing;
                          }else{
                            return i18n.label_user_duty_ing;
                          }
                          }},
                        {name:'userState',hidden:true},
                        {name:'job',hidden:true},
                        {name:'password',hidden:true},
                        {name:'officePhone',hidden:true},
                        {name:'orgNo',hidden:true},
                        {name:'description',hidden:true},
                        {name:'fax',hidden:true},
                        {name:'msn',hidden:true},
                        {name:'userCost',hidden:true},
                        {name:'officeAddress',hidden:true},
                        {name:'remark',hidden:true},
                        {name:'tcGroupIds',hidden:true},
                        {name:'act',align:'center',sortable:false,formatter:common.security.user.userGridFormatter}
                ],
				jsonReader: $.extend(jqGridJsonReader, {id: "userId"}),
				sortname:'userId',
				pager:'#userPager',
				shrinkToFit:false,
				gridComplete:function(){
					$("#userGrid td[aria-describedby=userGrid_act]").attr("title","");
					common.security.user.countTc();//统计技术员
				}
				});
				$("#userGrid").jqGrid(params);
				$("#userGrid").navGrid('#userPager',navGridParams);
				//列表操作项
				$("#t_userGrid").css(jqGridTopStyles);
				$("#t_userGrid").append($('#userTopMenu').html());
				
				//自适应宽度
				//setGridWidth("#userGrid","regCenter",20);
				setGridWidth("#userGrid", "regCenter",223);
		},
		
		/** 
		 * @description 保存新用户
		 * */
		addUser:function() {
			var lastName= $('#lastName').val();
			$('#lastName').val(lastName.replace(/[ ]/g,""));
			
			var firstName= $('#firstName').val();
			$('#firstName').val(firstName.replace(/[ ]/g,""));
			
			if($('#userForm').form('validate')){
				if(operator=='save'){
					startProcess();
					$.post('user!userExist.action','userDto.userId='+$("#userId").val()+'&userDto.loginName='+$("#loginName").val(),function(data){
						endProcess();
						if(data){
							common.security.user.addUserOpt();
						}else{
							msgAlert(i18n.error_userExist,'info');
						}
						/**保存向导状态*/
						common.config.systemGuide.systemGuide.saveSystemGuide("userGuide");
					});
				}else{
					common.security.user.addUserOpt();
				}
			}
		},
		
		/** 
		 * @description 编辑保存用户
		 * */
		addUserOpt:function() {
			if($('#userForm').form('validate')){
				startProcess();
				var roleIds = $('#userForm form').serialize();
				if(roleResourceAuthorize){
					if(roleIds.indexOf('userDto.roleIds')>-1){
						var user_form = $('#userForm form,#roleSet-win form').serialize();
						var url = 'user!'+operator+'.action';
						$.post(url, user_form, function(){
							msgShow(i18n.saveSuccess,'show');
							$('#addUser').dialog('close');
							$('#userGrid').trigger('reloadGrid');
							common.security.user.countTc();//统计技术员
							endProcess();
							common.config.systemGuide.systemGuide.saveSystemGuide("userGuide");
							common.security.organizationTree.loadOrganizationTreeView('');
						});
					}else{
						endProcess();
						$('#userOtherInfo').tabs('select',i18n.userRole);
						msgAlert(i18n.msg_plase_set_role,'info');
					}
				}else{
					var user_form = $('#userForm form,#roleSet-win form').serialize();
					var url = 'user!'+operator+'.action';
					$.post(url, user_form, function(){
						msgShow(i18n.saveSuccess,'show');
						$('#addUser').dialog('close');
						$('#userGrid').trigger('reloadGrid');
						common.security.user.countTc();//统计技术员
						endProcess();
						common.config.systemGuide.systemGuide.saveSystemGuide("userGuide");
						common.security.organizationTree.loadOrganizationTreeView('');
					});
				}
			}
		},
		
		/**
		 * @description 用户添加窗口
		 */
		user_add:function(){
			
		    // 清除验证状态
		    common.security.user.removeInvalid();
			$('#loginName,#password,#rpassword').val('');
			$.post('currency!findDefaultCurrency.action',function(data){
				if(data!==null && data!=''){
					$('#userCost_currency').text(data);
				}else{
					$('#userCost_currency').text('RMB');
				}
			});
			$.post('currency!findDefaultCurrency.action',function(data){
              if(data!==null && data!=''){
					$('#userCost_currency').text(data);
              }else{
					$('#userCost_currency').text('RMB');
              }
			});
			$('#resetPassword_tr').hide();
			$('#password_tr,#rpassword_tr').show();
			$('#loginName').removeAttr("readonly");
			resetForm('#addUserForm');
			common.security.user.add_loadRole(0,"roleSet");
			$('#user_companyNo').val(companyNo);
			$('#belongsGroupShowId').empty();
			$('#addUser').dialog({autoOpen: true,width:710});
			operator = 'save';
			$('#userOtherInfo').tabs();
			tab_option = $('#userOtherInfo').tabs('getTab',i18n['lable_proxy_div']).panel('options').tab;  
			tab_option.hide();
		},

		/**
		 * @description 编辑用户
		 */
		editUser_aff:function(){
			checkBeforeEditGrid('#userGrid',common.security.user.editUser);
			tab_option = $('#userOtherInfo').tabs('getTab',i18n['lable_proxy_div']).panel('options').tab;  
			tab_option.show();
		},
		/**
		 * @description 编辑用户
		 * @param rowId 行的编号
		 */
		editUser_affs:function(rowId){
			var rowData= $("#userGrid").jqGrid('getRowData',rowId);  // 行数据  
			common.security.user.editUser(rowData);
		},
		/**
		 * 用户字段格式化
		 * @param value 字段值
		 */
		userSetInput:function(value){
			if(value!=null)
				return value;
			else
				return "";
		},
		/**
		 * @description 编辑用户窗口
		 * @param rowData  行数据
		 */
		editUser:function(rowData) {
			operator = 'merge';
			$("#user_technicalGroupId").val(rowData.tcGroupIds+",");
			if(rowData.tcGroupNames!=null && rowData.tcGroupNames!=""){
				$("#user_groupName").val(rowData.tcGroupNames+";");
			}else{
				$("#user_groupName").val("");
			}
			$.post('user!findUserDetail.action','userDto.userId='+rowData.userId,function(data){
				$('#userId').val(data.userId);
				if(Proxyshow==0){
					Proxyshow=1;
					common.security.user.userProxy(data.userId);//代理人
					$('#link_proxyMain_add').click(common.security.user.proxyadd);
					$('#saveproxyBtn').click(common.security.user.saveprox);
					$('#link_proxyMain_edit').click(function(){common.security.user.proxyedit($("#selectUserByProxy_grid").getGridParam("selrow"))});
					$('#link_proxyMain_delete').click(common.security.user.deleteprox_aff);
					$('#link_proxyMain_search').click(common.security.user.search_aff);
					$('#Search_proxyBtn').click(common.security.user.SearchOpt);
				}else{
					var _postData = $("#selectUserByProxy_grid").jqGrid("getGridParam", "postData");       
					$.extend(_postData, {'proxyDTO.proxieduserId':data.userId});	
					$('#selectUserByProxy_grid').trigger('reloadGrid',[{"page":"1"}]);
				}
				$('#loginName').val(data.loginName);
				if(data.firstName!=null && data.firstName!='null')
					$('#firstName').val(data.firstName);
				else
					$('#firstName').val('');
				if(data.lastName!=null && data.lastName!='null')	
					$('#lastName').val(data.lastName);
				else
					$('#lastName').val('');
				if(data.email!=null && data.email!='null')
					$('#email').val(data.email);
				else
					$('#email').val('');
				
				
				$('#password,#rpassword').val(data.password);
				$('#resetPassword_tr').show();
				$('#password_tr,#rpassword_tr').hide();
				

				$('#moblie').val(common.security.user.userSetInput(data.moblie));
				$('#phone').val(common.security.user.userSetInput(data.phone));
				$('#fax').val(common.security.user.userSetInput(data.fax));
				$('#msn').val(common.security.user.userSetInput(data.msn));
				$('#officeAddress').val(common.security.user.userSetInput(data.officeAddress));
				$('#officePhone').val(common.security.user.userSetInput(data.officePhone));
				$('#description').val(common.security.user.userSetInput(data.description));
				$('#remark').val(common.security.user.userSetInput(data.remark));
				$('#userCost').val(data.userCost);
				$('#job').val(common.security.user.userSetInput(data.job));
				$('#user_orgName').val(common.security.user.userSetInput(data.orgName));
				$('#user_orgNo').val(data.orgNo);
				$('#extension').val(common.security.user.userSetInput(data.extension));
				$('#user_position').val(common.security.user.userSetInput(data.position));
				if(data.sex==false){
					$('#user_sex_woman').attr("checked",'false');
				}else
				{
					$('#user_sex_man').attr("checked",'true');
				}
				
				$('#user_icCard').val(common.security.user.userSetInput(data.icCard));
				$('#user_pinyin').val(common.security.user.userSetInput(data.pinyin));
				$('#user_birthday').val(timeFormatterOnlyData(data.birthday));
				/**
				 * 单选按钮选择项
				 */
				if(data.userState==false){
					$('#userState_true').attr("checked",'false');
					$('#userState_flase').attr("checked",'true');
				}else
				{
					$('#userState_flase').attr("checked",'false');
					$('#userState_true').attr("checked",'true');
				}
				$("#userId").attr('name','userDto.userId')
				$('#loginName').attr("readonly",'readonly');
				$('#user_companyNo').val(companyNo);
				$('#lcallCodeOper').val(common.security.user.userSetInput(data.lcallCodeOper));
				$('#remoteHost').val(common.security.user.userSetInput(data.remoteHost));
				$('#remotePort').val(common.security.user.userSetInput(data.remotePort));
				if(data.holidayStatus==true || data.holidayStatus=='true'){
					$('#user_holidayStatus_true').attr("checked",'true') 
				}else{
					$('#user_holidayStatus_false').attr("checked",'false');
				}
				//所属组
				if(data.belongsGroups!=null){
					var _belongsGroups = data.belongsGroups;
					var _content = '';
					for(var i=0;i<_belongsGroups.length;i++){
						_content = '<div id="belongsGroup_[id]">[name]&nbsp;&nbsp;'+
						'<input type="hidden" name="userDto.belongsGroupIds" value="[id1]" />'+
						'<a style="color:red;" onclick="common.security.organizationTreeUtil.belongsGroupRemove([id2])">X</a><div>';
						$('#belongsGroupShowId').append(
								_content.replace('[id]',_belongsGroups[i].orgNo)
								.replace('[id1]',_belongsGroups[i].orgNo)
								.replace('[id2]',_belongsGroups[i].orgNo)
								.replace('[name]',_belongsGroups[i].orgName));
					}
					
				}
			});
			common.security.user.add_loadRole(rowData.userId,"roleSet");
			$('#belongsGroupShowId').empty();
			$('#addUser').dialog({autoOpen: true,width:710});
			
			common.security.user.removeInvalid();
			
			$('#userOtherInfo').tabs();
		},
		/**
		 * 移除验证
		 */
		removeInvalid : function() {
		    $('#loginName').removeClass('validatebox-invalid'); 
            $('#password').removeClass('validatebox-invalid'); 
            $('#rpassword').removeClass('validatebox-invalid'); 
            $('#lastName').removeClass('validatebox-invalid'); 
            $('#user_orgName').removeClass('validatebox-invalid');   
		},
		
		/**
		 * @description 删除.
		 */
		opt_delete:function(){
			checkBeforeDeleteGrid('#userGrid',common.security.user.opt_deleteInLineMethod);
		},
		
		/**
		 * @description 删除.
		 * @param rowid 行的编号
		 */
		opt_delete_aff:function(rowId){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				common.security.user.opt_deleteInLineMethod(rowId);
			});
		},
		/**
		 * @description 删除Method.
		 * @param rowid 行的编号
		 */
		opt_deleteInLineMethod:function(rowId){
			var param = $.param({'ids':rowId},true);
			$.post("user!delUser.action", param, function(data){
					$('#userGrid').trigger('reloadGrid');
					msgShow(i18n['msg_user_deleteSuccessful'],'show');
					common.security.user.countTc();//统计技术员
			}, "json");
		},
		
		/**
		 * @description 搜索用户
		 */
		searchUser:function(){
			
			var userOrg=$('#user_search_orgNo').val();
			var sdata=$('#searchUser form').getForm();
			var postData = $("#userGrid").jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);
			var _url = 'user!find.action';		
			$('#userGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid',[{"page":"1"}]);
		},
		
		/**
		 * @description 打开
		 */
		opt_setRole_aff:function(){
			checkBeforeEditGrid('#userGrid',common.security.user.opt_setRole);
		},
		
		/**
		 * @description 操作项-用户角色确定
		 */
		opt_setRole_affs:function(rowId){
			var rowData= $("#userGrid").jqGrid('getRowData',rowId);  // 行数据  
			common.security.user.opt_setRole(rowData);
		},
		/**
		 * @description 操作项-用户角色设置
		 */
		opt_setRole:function(rowData){
			$('#roleIds').val(rowData.userId);
			common.security.user.add_loadRole(rowData.userId,"roleSetInfo");
			windows('roleSet-win',{width:400});
		},
		/**
		 * @description 操作项-保存(根据用户ID修改角色)
		 */
		updateRoleByUserId:function(){
			var rowIds = $('#roleIds').val();
			if(rowIds=='')
			{
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}
			else
			{	
				var _rowIds = rowIds.split(",");
				var param = $.param({'ids':_rowIds},true);
				var user_form = $('#roleSet-win form').serialize();
				if(user_form.indexOf('userDto.roleIds')>-1){
					$.post("user!updateRoleByUserId.action",user_form+'&'+param, function(){
						$('#rolesTb').html("");
						$('#roleSet-win').dialog('close');
						$('#userGrid').trigger('reloadGrid');
						
						msgShow(i18n['msg_user_roleSetSuccessful'],'show');
						common.security.user.countTc();//统计技术员
	
						$('#roleIds').val('')
						
						
					}, "json");
				}else{
					msgAlert(i18n['msg_plase_set_role'],'info');
				}		
			}
		},

		/**
		 * @description 加载角色
		 * @param id  编号
		 * @param tab 表格
		 * @param isitsop 是否是外包客户
		 * 
		 */
		add_loadRole:function(id,tab,isitsop){
					
			loadUserRole=isitsop;
			$("#"+tab+" table").html("");
			$("#"+tab+" table").empty();
			
			if(id!=null){
				if(id==-1){
					id=0;
				}
				common.security.user.getUserRole(id);
				setTimeout(function(){
					common.security.user.getUserRoleAll(tab);
				},500);
			}else{
				setTimeout(function(){
					common.security.user.getUserRoleAll(tab);
				},0);
			}
			
		},
		
		/**
		 * @description 新增页面加载角色复选框
		 * @param id  编号
		 */
		getUserRole:function(id){
				if(id!=0){
					$.post("user!getUserRole.action","id="+id,function(data){
						userRoleIds=data;
					},"json")
				}
		},
		
		
		/**
		 * 得到用户端饿所以角色
		 * @param Tab  表格
		 */
		getUserRoleAll:function(tab){
			$("#"+tab+" table").html('');
			$.post("role!findByState.action",function(data){
				mydata=data;
				this.roleHtml;
				roleHtml="";
				var r = 0;
				var u = 0;
				for(var i=0;i<mydata.length;i++)
				{
					
					if(topCompanyNo!=companyNo || loadUserRole){
						
						if(mydata[i].roleCode=="ROLE_ENDUSER" || mydata[i].roleCode=="ROLE_ITSOP_MANAGEMENT"){
							
							if(r % 2==0){
								roleHtml=roleHtml+"<tr>"
							}
							roleHtml=roleHtml+"<td><input type='checkbox' class='roleBox' name='userDto.roleIds' class='userRoleId'"+
							" value='"+mydata[i].roleId+"'";
							if(userRoleIds!=null){
								for(var j=0; j<userRoleIds.length;j++)
								{	
									if(userRoleIds[j].roleId==mydata[i].roleId)
									{
										roleHtml=roleHtml+" checked='checked' ";
									}
								}
							}
							roleHtml=roleHtml+" />"+mydata[i].roleName+"</td>"
							r++;
						}
						
					}else{
						if(mydata[i].roleCode!="ROLE_ITSOP_MANAGEMENT"){
							if(u % 2==0){
								roleHtml=roleHtml+"<tr>"
							}
							roleHtml=roleHtml+"<td><input type='checkbox' class='roleBox' name='userDto.roleIds' class='userRoleId'"+
							" value='"+mydata[i].roleId+"'";
							if(userRoleIds!=null){
								for(var j=0; j<userRoleIds.length;j++)
								{	
									if(userRoleIds[j].roleId==mydata[i].roleId)
									{
										roleHtml=roleHtml+" checked='checked' ";
									}
								}
							}
							roleHtml=roleHtml+" />"+mydata[i].roleName+"</td>";
							u++;
						}
					}
					
				}
				if(topCompanyNo!=companyNo || loadUserRole){
					if(r % 2==1){
						roleHtml+="</tr>"
					}
				}else{
					if(u % 2==1){
						roleHtml+="<td></td>";
					}
				}
				$("#"+tab+" table").html(roleHtml);
				userRoleIds=null;
				
				
				
				
				//选定验证
				$('.roleBox').click(function(){
					if(tab!='itsop_user_roles'){
						var roleId=$(this).val();
						if(roleId!=11 && roleId!=12 && ((technicianNumber+1)<=usedTechnicianNumber)){	
							$(this).attr({'checked':false});
							msgAlert(i18n['TechnicianLicense_Not_Enough'],'info');
						}							
					}
				});
				
				
			},"json");
			
			
			loadRoleId=0;
		},
		

		/**
		 * 机构设置保存
		 */
		orgSet_save:function(){
			if($('#ortSet_orgNo').val()==null || $('#ortSet_orgNo').val()==""){
				msgAlert(i18n['title_user_selectOrg'],'info');
			}else{	
				var data='id='+$('#ortSet_orgNo').val()+'&str='+$('#userId1').val();
				$.post("user!mergeByOrg.action", data, function(){
					
					msgShow(i18n['msg_operationSuccessful'],'show');
					$('#orgSet-win').dialog('close');
					$('#userGrid').trigger('reloadGrid');
					
					$('#orgSet_orgName').val('');
					$('#ortSet_orgNo').val('');
					$('#userId1').val('');
				}, "json");
			}
		},
		

		/**
		 * 用户角色批量设置窗口
		 * 
		 * */
		userReleSet_win:function(){
			var rowIds = $("#userGrid").getGridParam("selarrrow");

			if(rowIds==""){
				
				msgAlert(i18n.msg_atLeastChooseOneData,'info');
				
			}else{	
				
				
				var userId=null;
				
				if(rowIds.length==1){			
					userId=rowIds[0];
				}
				
				
				$('#roleIds').val(rowIds);
				$('#orgName1').val('');
				$('#orgNo1').val('');
				common.security.user.add_loadRole(userId,"roleSetInfo");
				
				windows('roleSet-win',{width:400});
			}
			
		},
		/**
		 * 用户所属机构批量设置
		 * */
		userOrgSet_win:function(){
			$('#orgSet_orgName,#ortSet_orgNo').val('');
			var rowIds = $("#userGrid").getGridParam("selarrrow");
			if(rowIds==""){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{

				if(rowIds.length==1){
					$.post('user!findUserDetail.action','userDto.userId='+rowIds,function(data){
						$('#orgSet_orgName').val(data.orgName);
						$('#ortSet_orgNo').val(data.orgNo);
					});
				};
				$('#userId1').val(rowIds);
				windows('orgSet-win',{width:400});
			}
		},

		
		/**
		 * 导出
		 */
		exportUserView:function(){

			
			$("#searchUser form").submit();
			
		},
		
		/**
		 * 导入数据.
		 */
		importUserData:function(){
			windows('importUserDataWindow',{width:400});
		},
		/**
		 * @descriptionadd 客户端导入数据
		 * */
		importUserExcel:function()
		{
				startProcess();
				$.ajaxFileUpload({
		            url:'user!importUserData.action',
		            secureuri:false,
		            fileElementId:'importUserFile', 
		            dataType:'json',
		            success: function(data,status){
		             	$('#importUserDataWindow').dialog('close');
		            	if(data=="FileNotFound"){
							msgAlert(i18n['msg_dc_fileNotExists'],'info');
							endProcess();
						}else if(data=="IOError"){
							msgAlert(i18n['msg_dc_importFailure'],'info');
							endProcess();
						}else if(data=="TCNotEnough"){
							msgAlert(i18n['TechnicianLicense_Not_Enough'],'info');
							endProcess();
						}else if(data=="ERROR_COMPANY_NAME"){//公司名称输入错误will(20130715)
							msgAlert("<br/>"+i18n['ERROR_COMPANY_NAME'],'info');
							endProcess();
						}else{
							msgAlert(i18n['msg_dc_dataImportSuccessful']
									+'<br>['+
									data
									.replace('Total',i18n['opertionTotal'])
									.replace('Insert',i18n['newAdd'])
									.replace('Update',i18n['update'])
									.replace('Failure',i18n['failure'])+']'
									,'show');
							endProcess();
						}
		            	$('#userGrid').trigger('reloadGrid');
		            }
		        });
		},
		/**
		 * 加载用户角色
		 */
		loadSearchUserRoles:function(){
			$("#search_user_roles").empty(); 
			$("<option value='0'> -- "+i18n['label_user_selectRole']+" -- </option>").appendTo("#search_user_roles");
			
			$.post("role!find.action?rows=500", function(data){
				if(data!=null && data.data!=null && data.data.length!=0){
					var optionsHTML="";
					
					for(var i=0;i<data.data.length;i++){					
						optionsHTML+="<option value='{0}'>{1}</option>".replace("{0}",data.data[i].roleId).replace("{1}",data.data[i].roleName);
					}					
					$("#search_user_roles").append(optionsHTML);
				}
					
			}, "json");
		},
		/**
		 * 修改密码
		 */
		edit_user_pwd:function(){
			var rowData=$('#userGrid').getRowData($('#userId').val());
			$.post('user!findUserDetail.action','userDto.userId='+$('#userId').val(),function(data){
				$('#editUserPwd_loginName').val(data.loginName);
				$('#editUserPwd_loginName_span').text(data.loginName);
				$('#editUserPwd_oldPassword').val(data.password);
				$('#editUserPwd_newPassword,#editUserPwd_repeatPassword').val('');
				windows('editUserPwd_win',{width:380,height:190});
			})
			
		},
		/**
		 * 登录名去空格
		 * @param  字段值
		 */
		trimLoginName:function(value){
			var reg = /\s/g; 
			$("#user_loginName").val($("#user_loginName").val().replace(reg,""));
		},
		/**
		 * 当前用户角色
		 */
		currentUserRoleCode:function(){
			$.post("user!getCurrentLoginUser.action",function(data){
				for ( var i = 0; i < data.userRoles.length; i++) {
					if( "ROLE_SYSADMIN" == data.userRoles[i].roleCode){
						$("#search_user_state_tr").removeAttr("style");
					}
				}
			});
		},
		/**
		 * 展开或收缩面板.
		 */
		panelCollapseExpand:function(){
			 $(userGrids).each(function(i, g) {
			      setGridWidth(g, 'userDetailInfo_center', 10);
			 });
		},
		
		/**
		 * 数据列表自动伸展.
		 */
		fitProblemGrids:function(){
			
			$('#user_west').panel({
				onCollapse:common.security.user.panelCollapseExpand,
				onExpand:common.security.user.panelCollapseExpand,
				onResize:function(width, height){
				
					setTimeout(function(){
						
						common.security.user.panelCollapseExpand();
						
					},0);
					
				}
			});
		},
		userProxy:function(userId){
			userGrids.push('#selectUserByProxy_grid');
			var params = $.extend({},jqGridParams, {
				url:'proxyAction!findProxyByPage.action',
				postData:{'proxyDTO.proxieduserId':userId},
				colNames:['ID','userId',
				          i18n['lable_proxy_user'],
				          i18n['title_startTime'],
				          i18n['title_endTime'],
				          i18n['title_snmp_state']
				],
			 	colModel:[
			 	          {name:'proxyId',align:'center',width:45,hidden:true},
			 	          {name:'userId',align:'center',width:45,hidden:true},
			 	          {name:'userName',index:'userAgent',align:'center',width:100},
			 	          {name:'startTime',align:'center',width:80},
			 	          {name:'endTime',align:'center',width:80},
			 	         {name:'proxyState',align:'center',width:50,sortable:false,formatter:common.security.user.isToproxyState}
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id: "proxyId"}),
				sortname:'proxyId',
				pager:'#selectUserByProxy_pager'
				});
				$("#selectUserByProxy_grid").jqGrid(params);
				$("#selectUserByProxy_grid").navGrid('#selectUserByProxy_pager',navGridParams);
				//列表操作项
				$("#t_selectUserByProxy_grid").css(jqGridTopStyles);
				$("#t_selectUserByProxy_grid").append($('#proxyMainToolbar').html());
				//自适应宽度
				setGridWidth("#selectUserByProxy_grid","userDetailInfo_center",8);
		},
		isToproxyState:function(cell,event,data){
		  if(cell){
			  return i18n['disable'];
		  }else{
			  return i18n['enable'];
		  }
		},
		proxyadd:function(){
			ProxyType=0;
			$('#proxy_add_proxyTypeId').val('');
			$('#proxyUserId').val('');
			$('#proxy_add_proxyName').val('');
			$('#proxy_add_startTime').val('');
			$('#proxy_add_endTime').val('');
			windows('proxyMainAddWin',{width:380,height:190});
		},
		proxyedit:function(rowId){
			ProxyType=1;
			if(rowId==null){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				var data = $("#selectUserByProxy_grid").getRowData(rowId);
				$('#proxy_add_proxyTypeId').val(data.proxyId);
				$('#proxyUserId').val(data.userId);
				$('#proxy_add_proxyName').val(data.userName);
				$('#proxy_add_startTime').val(data.startTime);
				$('#proxy_add_endTime').val(data.endTime);
				windows('proxyMainAddWin',{width:380,height:190});
			}
		},
		saveprox:function(){
			$('#proxy_add_proxieduserId').val($("#userGrid").getGridParam("selrow"));
			if($('#proxyMainAddWin form').form('validate')){
				var _frm = $('#proxyMainAddWin form').serialize();
				var url = 'proxyAction!saveProxy.action';
				if(ProxyType==1){
					url = 'proxyAction!editProxy.action';
				}
				//调用
				startProcess();
				$.post(url,_frm, function(){
					msgShow(i18n['saveSuccess'],'show');
					$('#proxyMainAddWin').dialog('close');
					$('#selectUserByProxy_grid').trigger('reloadGrid');
					endProcess();
				});
			}
		},
		/**
		 * @description 判断是否选择要删除
		 */
		deleteprox_aff:function(){
			checkBeforeDeleteGrid('#selectUserByProxy_grid', common.security.user.deleteproxOpt);
		},
		/**
		 * @description 删除操作
		 * @param rowids  行编号
		 */
		deleteproxOpt:function(rowsId){
			var url="proxyAction!deleteProxy.action";
			var param = $.param({'ids':rowsId},true);
			$.post(url, param, function()
			{
				msgShow(i18n['msg_deleteSuccessful'],'show');
				$('#selectUserByProxy_grid').trigger('reloadGrid');
			}, "json");	
		},
		/**
		 * 查询
		 */
		search_aff:function(){
			$('#Search_proxyUserId').val('');
			$('#proxy_Search_proxyName').val('');
			$('#proxy_Search_proxieduserId').val($("#userGrid").getGridParam("selrow"));
			windows('Search_proxyMainWin',{width:380,height:160});
		},
		/**
		 * 查询
		 */
		SearchOpt:function(){
			var sdata=$('#Search_proxyMainWin form').getForm();
			var postData = $("#selectUserByProxy_grid").jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);
			var _url = 'proxyAction!findProxyByPage.action';
			$('#selectUserByProxy_grid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid',[{"page":"1"}]);
		},
		/**
		 * 点击右边的机构进行查询机构下面的用户
		 */
		search_UserGrid : function(treeId,companyNo){
			$(treeId).jstree({
				json_data:{
					ajax: {
						url : "organization!findAll.action?companyNo="+companyNo,
						data:function(n){
					    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
						},
						cache: false
					}
				},
				plugins : ["themes", "json_data", "ui"]

			})
			.bind('select_node.jstree',function(e,data){
				var orgNo = data.rslt.obj.attr("orgNo");
				var orgType = data.rslt.obj.attr("orgType");
				var postData = $("#userGrid").jqGrid("getGridParam", "postData");
				if(orgType!="innerPanel"&&orgType!="servicePanel"){
					var _postData={'userQueryDto.orgNo':orgNo,'userQueryDto.companyNo':companyNo};
					var _url = 'user!find.action';
					if(orgType=="company")
						_postData={'userQueryDto.orgNo':"",'userQueryDto.companyNo':companyNo};
					$.extend(postData,_postData);
					$('#userGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid',[{"page":"1"}]);
				}
			});
		},
		/**
    	 * 获取事件
    	 */
    	fitUser_organizationEasteGrids:function(){
    		
    		$('#user_organizationEast').panel({
    			onCollapse:function(){
    				setGridWidth('#userGrid', 'regCenter', 50);
    			},
    			onExpand:function(){
    				setGridWidth('#userGrid', 'regCenter', 223);
    			}
    		});
    	},
    	
		/**
		 * 初始化加载
		 */
		init:function(){
			setTimeout(common.security.user.fitProblemGrids,0);
			//绑定日期控件
			DatePicker97(['#user_birthday']);
			bindControl('#proxy_add_startTime,#proxy_add_endTime');
			$("#userMain_loading").hide();
			$("#userMain_content").show();
			common.security.user.currentUserRoleCode();
			common.security.user.showUserGrid();
			$('#link_user_add').click(common.security.user.user_add);
			$('#link_user_edit').click(common.security.user.editUser_aff);
			$('#link_user_delete').click(common.security.user.opt_delete);
			$('#link_user_search').click(function(){
				if(user_search_selectData_flag){
					common.security.user.loadSearchUserRoles();
					user_search_selectData_flag=false;
				}
				windows('searchUser',{width:400});
			});
			$('#user_link_search').click(common.security.user.searchUser);
			$('#link_user_role').click(common.security.user.userReleSet_win);
			$('#link_roleSet_save').click(common.security.user.updateRoleByUserId);
			$('#link_user_org').click(common.security.user.userOrgSet_win);
			$('#link_orgSet_save').click(common.security.user.orgSet_save);
			$('#user_orgName').click(function(){
			    $('#user_orgName').blur();
				common.security.organizationTreeUtil.selectOrg('#userGroup_win','#userGroupTree','#user_orgName','#user_orgNo',companyNo,function(orgNo,orgName,orgType){
					var _content = '<div id="belongsGroup_[id]">[name]&nbsp;&nbsp;'+
					'<input type="hidden" name="userDto.belongsGroupIds" value="[id1]" />'+
					'<a style="color:red;" onclick="common.security.organizationTreeUtil.belongsGroupRemove([id2])">X</a><div>';
					if(orgType != 'innerPanel' && orgType != 'servicePanel'){
						if($('#belongsGroup_'+orgNo).html()==null){
							$('#belongsGroupShowId').append(
									_content.replace('[id]',orgNo)
									.replace('[id1]',orgNo)
									.replace('[id2]',orgNo)
									.replace('[name]',orgName));
						}
					}
				});
			});
			$('#orgSet_orgName').click(function(){
				common.security.organizationTreeUtil.showAll_2('#userGroup_win','#userGroupTree','#orgSet_orgName','#ortSet_orgNo',companyNo);
			});
			$('#user_search_orgName').click(function(){
				common.security.organizationTreeUtil.showAll_2('#userGroup_win','#userGroupTree','#user_search_orgName','#user_search_orgNo',companyNo);
			});
			$('#search_user_belongsGroup').click(function(){
				common.security.organizationTreeUtil.showAll_3('#userGroup_win','#userGroupTree','#search_user_belongsGroup','#user_search_orgNo',companyNo);
			});
			$('#link_user_password_reset').click(function(){common.security.editUserPassword.userPasswordReset($('#loginName').val())});
			$('#importUserData').click(common.security.user.importUserData);
			$('#exportUserData').click(common.security.user.exportUserView);
			$('#link_edit_user_pwd').click(common.security.user.edit_user_pwd);//修改密码
			$('#edit_user_pwd_but').click(common.security.editUserPassword.editUserPassword_manage);//确定修改密码
			$('#user_groupName').click(function(){
				common.security.selectTechnicalGroup.selectTechmentGroup('user_groupName','user_technicalGroupId');
			});
			//选择所属组
			$('#selectBelongsGroupBut').click(function(){
				$('#index_selectORG_window').attr('title',i18n.label_userOwnerGroup);
				
				common.security.organizationTreeUtil.selectBelongsGroup('belongsGroupShowId',companyNo);
			});
			common.security.user.search_UserGrid("#user_orgTreeDIV",companyNo);
			setTimeout(function () {
				common.security.user.fitUser_organizationEasteGrids();
			}, 0);
		}
	}
}();
/**载入**/
$(document).ready(common.security.user.init);
