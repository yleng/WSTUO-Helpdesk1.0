
$package('common.security');

$import('common.security.organizationTree');
$import('common.security.organizationGrid');
$import('common.security.organizationRole');
$import('common.security.organizationHoliday');
$import('common.security.organizationServiceTime');
$import("common.security.userUtil");
$import('common.config.systemGuide.systemGuide');
$import('itsm.app.autocomplete.autocomplete');

/**
 * @author Van  
 * @constructor WSTO
 * @description 机构管理主函数.
 * @date 2011-02-25
 * @since version 1.0 
 **/
common.security.organizationMain = function() {
	//机构中的数据列表集合
	this.organizationGrids=[];
	//加载标识
	this.holidayTabClick="0";
	this.gridTabClick="0";
	this.roleTabClick="0";
	
	
	return {
		/**
		 * @description 编辑当前机构
		 **/
		saveCurrentOrg:function (){
			var fullName=$('#organizationDto_personInChargeName').val();
			validateUserByFullName(fullName,common.security.organizationMain.checkUserEdit);
		},
		
		
		/**
		 * @description 编辑机构方法
		 **/
		checkUserEdit:function(){
			var editorgName=trim($('#edit_orgName').val());
			$('#edit_orgName').val(editorgName);
			if($('#editCurrentOrgForm').form('validate')){
				var frm = $('#editOrgDiv form,#orgRoleDiv form').serialize();
				var edit_orgType=$('#edit_orgType').val();
				if(edit_orgType=="servicePanel" || edit_orgType=="innerPanel"){
					msgAlert(i18n.ERROR_SYSTEM_DATA_CAN_NOT_EDIT,'info');
				}else{
					var url = 'organization!updateOrg.action';
					// 判断分类是否存在    
	                $.post('organization!isCategoryExistdOnEdit.action', frm, function(data){
	                   if (data) {
	                       msgAlert(i18n.err_orgNameIsExisted,'info');
	                   }  else {
	                       $.post(url,frm, function(){
	                           $('#orgGrid').trigger('reloadGrid');
	                           common.security.organizationTree.loadOrganizationTreeView('#orgTreeDIV');//重新载入树结构
	                           msgShow(i18n['msg_org_orgEditSuccessful'],'show');
	                           common.config.systemGuide.systemGuide.saveSystemGuide("orgGuide");
	                       });
	                   }
	                });
				}
			}
		},
		
		/**
		 * @description 保存机构信息
		 **/
		addCurrentOrg:function (){
			var parentNo=$('#add_parentOrgNo').val();
			
			if(parentNo!=null &&  parentNo!=""){
				 startProcess();
				var orgName=trim($('#addChild_orgName').val());
				$('#addChild_orgName').val(orgName);
				if($('#addChildForm').form('validate')){
					var fullName=$('#organizationDto_personInChargeName_add').val();
					validateUserByFullName(fullName,common.security.organizationMain.addCurrentOrgMethod);
				}
				endProcess();
			}else{
				
				msgAlert(i18n["msg_chooseOrg"],"info");
			}

		},
		
		/**
		 * @description 保存机构信息
		 **/
		addCurrentOrgMethod:function (){
				var frm = $('#addOrgDiv form').serialize();
				var url = 'organization!save.action';
				$.post('organization!isCategoryExisted.action', frm, function(data){
	                   if (data) {
	                	   endProcess();
	                       msgAlert(i18n.err_orgNameIsExisted,'info');
	                   }  else {
	                       $.post(url,frm, function(){
	                           
	                           //重新载入树结构
	                           common.security.organizationTree.loadOrganizationTreeView('#orgTreeDIV');
	                       
	                           $('#orgGrid').trigger('reloadGrid');
	                           $('#add_actionType').val('inner');
	                           $('#addChild_orgName').val('');
	                           $('#addChild_officePhone').val('');
	                           $('#addChild_officeFax').val('');
	                           $('#addChild_email').val('');
	                           $('#addChild_address').val('');
	                           $('#organizationDto_personInChargeName_add').val('');
	                           //$('#organizationDto_personInChargeNo_add').val('');
	                           msgShow(i18n['msg_org_orgAddSuccessful'],'show');
	                       
	                           common.config.systemGuide.systemGuide.saveSystemGuide("orgGuide");
	                           endProcess();
	                       });
	                   }
	                   
				});
		},

		
		/**
		 * @description 默认显示公司信息
		 **/
		showCompanyInfo:function(){
			var url = 'organization!findCompany.action?companyNo='+companyNo;
			$.post(url,function(res){

				$("#edit_actionType>option[value='services']").remove();
				$("#edit_actionType>option[value='inner']").remove();
				$("<option value='company'>"+i18n['label_org_companyInfo']+"</option>").appendTo("#edit_actionType");
				
				if(res.officeFax!=null && res.officeFax!="")
					$('#edit_officeFax').val(res.officeFax);
				else
					$('#edit_officeFax').val("");
				if(res.officePhone!=null && res.officePhone!="")
					$('#edit_officePhone').val(res.officePhone);
				else
					$('#edit_officePhone').val("");
				if(res.email!=null && res.email!="")
					$('#edit_email').val(res.email);
				else
					$('#edit_email').val("");
				if(res.address!=null && res.address!="")
					$('#edit_address').val(res.address);
				else
					$('#edit_address').val("");
				if(res.orgNo!=null && res.orgNo!="")
					$('#edit_orgNo').val(res.orgNo);
				else
					$('#edit_orgNo').val("");
				if(res.orgName!=null && res.orgName!="")
					$('#edit_orgName').val(res.orgName);
				else
					$('#edit_orgName').val("");
				if(res.personInChargeName!=null && res.personInChargeName!='null'){
					$('#organizationDto_personInChargeName').val(res.personInChargeName);
					//$('#organizationDto_personInChargeNo').val(res.personInChargeNo);
				}else{
					$('#organizationDto_personInChargeName').val('');
					//$('#organizationDto_personInChargeNo').val('');
				}
				
			});
		},
		
		
		/**
		 * @description 为tab点击绑定事件
		 **/
		innerTabClick:function(){

		        $('#organizationMainTab').tabs({
		            onSelect:function(title){
		            	
		        		if(title==i18n['title_org_holiday'] && holidayTabClick=="0"){	
		        			common.security.organizationHoliday.init();
	        				holidayTabClick="1";
		        		}

		        		if(title==i18n['common_gridView'] && gridTabClick=="0"){     
	        				common.security.organizationGrid.init();
	        				gridTabClick="1";
		        		}
		        		if(title==i18n['title_org_role'] && roleTabClick=="0"){
	        			
	        				common.security.organizationRole.showRoleView('','#orgRoleDiv');	        				
	        				roleTabClick="1";
		        		}
		            }  
		        });
		},
		
		/**
		 * 展开或收缩面板.
		 **/
		panelCollapseExpand:function(){
			
			 $(organizationGrids).each(function(i, g) {
			      setGridWidth(g, 'organizationMainTab', 10);
			 });
		},
		
		/**
		 * 数据列表自动伸展.
		 **/
		fitOrganizationGrids:function(){
			
			$('#organzationWest,#organizationEast').panel({
				onCollapse:common.security.organizationMain.panelCollapseExpand,
				onExpand:common.security.organizationMain.panelCollapseExpand
			});
		},
		
		
		/**
		 * @description 选择机构负责人.
		 * @param 用户编号
		 * @param 用户名
		 **/
		selectPersonIncharge:function(userId,userName){
			common.security.userUtil.selectUser(userName,userId,'','fullName',companyNo);
		},
		
		/**
		 * 数据列表伸展
		 */
		OrganizationPanelCollapseAndExpand:function(){
			 $(organizationGrids).each(function(i, g) {
			     msgAlert(g);
			      setGridWidth(g, 'organizationMainTab', 9);
			 });
		},
		/**
		 * 数据列表自动伸展
		 */
		fitOrganizationMain:function(){
			$('#organzationWest,#organizationCenter,#organizationEast').panel({
				onCollapse:common.security.organizationMain.OrganizationPanelCollapseAndExpand,
				onExpand:common.security.organizationMain.OrganizationPanelCollapseAndExpand,
				onResize:function(width, height){
					setTimeout(function(){
						common.security.organizationMain.OrganizationPanelCollapseAndExpand();
					},0);
				}
			});
		},
		
		/**
		 * 载入.
		 **/
		init:function(){	
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#organizationDto_personInChargeName_add','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#organizationDto_personInChargeNo_add','','false');
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#organizationDto_personInChargeName','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#organizationDto_personInChargeNo','','false');
			organizationGrids.push('#orgGrid');
			//绑定日期控件
			DatePicker97(['#hoilday_hdate','#hoilday_hedate']);
			
			//隐显面板
			$("#organizationMain_loading").hide();
			$("#organizationMain_content").show();
			
			//面板点击事件
			common.security.organizationMain.innerTabClick();
			//默认公司信息
			common.security.organizationMain.showCompanyInfo();
			//显示机构树
			common.security.organizationTree.loadOrganizationTreeView('');
			//面板伸展
			setTimeout(common.security.organizationMain.fitOrganizationGrids,0);
			//点击事件
			$('#mergeCurrentOrgBtn,#mergeCurrentOrgBtn_role').click(common.security.organizationMain.saveCurrentOrg);
			$('#saveCurrentOrgBtn').click(common.security.organizationMain.addCurrentOrg);
			
			//服务时间
			common.security.organizationServiceTime.init();
			
			
			$('#organizationDto_personInChargeName_choose').click(function(){//选择机构负责人
				common.security.organizationMain.selectPersonIncharge('#organizationDto_personInChargeNo','#organizationDto_personInChargeName');
				
			});
			
			$('#organizationDto_personInChargeName_add_choose').click(function(){//选择机构负责人
				common.security.organizationMain.selectPersonIncharge('#organizationDto_personInChargeNo_add','#organizationDto_personInChargeName_add');
				
			});
			$('#organizationDto_personInChargeName_search_choose').click(function(){//选择机构负责人
				common.security.organizationMain.selectPersonIncharge('','#organizationDto_personInChargeName_search');
				
			});
			
			$('#organizationDto_personInChargeName_gridAdd_choose').click(function(){
				common.security.organizationMain.selectPersonIncharge('#organizationDto_personInChargeNo_gridAdd','#organizationDto_personInChargeName_gridAdd');
				
			});
		}
		
	};
	

}();
//载入
$(document).ready(common.security.organizationMain.init);
