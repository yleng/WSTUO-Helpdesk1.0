$package("itsm.request");
$import("common.config.category.eventCategoryTree");
$import("common.config.dictionary.dataDictionaryUtil");
$import('common.security.organizationTreeUtil');
$import('itsm.itsop.selectCompany');
$import("common.security.userUtil");
$import('common.security.includes.includes');
$import('common.config.includes.includes');
$import('common.config.category.serviceCatalog');
$import('basics.bottomMain');
$import('basics.showChart');
$import("common.config.category.serviceCatalog");
$import('common.config.category.serviceDirectoryUtils');
$import('common.security.base64Util');
/**  
 * @fileOverview "请求主函数"
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor WSTO
 * @description 请求主函数
 * @date 2010-11-17
 * @since version 1.0 
 */
itsm.request.requestMain = function() {
	this.request_search_selectData_flag=true;
	var options={};
	if(isITSOPUser){		
		$.extend(options,{'companyNo_companyNo': 'label_belongs_client'});
	}
	this.requestBatchClose_enos;
	$.extend(options,{
		//变量名_类型:字段名
		'requestCode_String':'number',
		'etitle_String':'common_title',
		'requestCategory.eventId_ReventId' : 'category',
		'slaState.dcode_dcode':'title_request_SLAState',
		'requestStatus.dcode_dcode':'common_state',
		'imode.dcode_dcode':'label_sla_imode',
		'level.dcode_dcode':'label_sla_level',
		'priority.dcode_dcode' : 'priority',
		'seriousness.dcode_dcode':'label_sla_seriousness',
		'effectRange.dcode_dcode':'label_sla_effectRange',
		'createdBy.loginName_loginName':'requester',
		//请求人所在组
		'createdBy.orgnization.orgNo':'title_requester_group',
		'assigneeGroup.orgNo_orgNo' :  'title_request_assignToGroup',
		'technician.loginName_loginName' : 'title_request_assignToTC',
		'owner.loginName_loginName':'common_owner',
		'createdOn_Data' : 'common_createTime',
		'closeTime_Data':'title_sla_completeTime',
		'serviceDirectory.eventId_singleServiceId':'title_service_directory',
		'location.eventNames_eventNames' : 'location',
//		'serviceDirectoryItem.subServiceIds_subServiceId' : 'title_service_directory'
			
	});
	//载入
	return {
		/**
		 * @description 升级申请标识格式化
		 * @param cellValue 列显示值
		 * @param options 操作项
		 * @param rowObject 行对象
		 */
		upgradeApplySignFormat:function(cellValue,options,rowObject){
			var returnResult="";
			if(cellValue=='0'){
				returnResult="<img src=../images/icons/app_normor.png title='"+i18n.label_request_normor+"' />";
				if(rowObject.hang){
					returnResult="<img src='../images/icons/hold.jpg' title='"+i18n.label_request_hang+"'/>";
				}
				return returnResult;
			}
			if(cellValue=='1'){
				returnResult="<img src=../images/icons/app_toupdate.png title='"+i18n.label_request_appToUpdate+"' />";
				if(rowObject.hang){
					returnResult="<img src='../images/icons/hold.jpg' title='"+i18n.label_request_hang+"'/>";
				}
				return returnResult;
			}
			if(cellValue=='2'){
				returnResult="<img src='../images/icons/app_update.png' title='"+i18n.label_request_updateComplete+"' />";
				if(rowObject.hang){
					returnResult="<img src='../images/icons/hold.jpg' title='"+i18n.label_request_hang+"'/>";
				}
				return returnResult;
			}
		},
		/**
		 * @description 请求标题添加URL格式化
		 * @param cellValue 列显示值
		 * @param options 操作项
		 * @param rowObject 行对象
		 */
		titleUrlFormatter:function(cellvalue, options, rowObject){
			return '<a href="JavaScript:itsm.request.requestMain.requestDetails('+rowObject.eno+')">'+cellvalue+'</a>';
			
		},
		/**
		 * @description 操作项格式化
		 * @param cellValue 列显示值
		 * @param options 操作项
		 * @param rowObject 行对象
		 */
		requestGridFormatter:function(cellvalue, options, rowObject){
			if(rowObject.statusDno=='request_close'){
				return $('#requestGridFormatterDiv').html();
			}else{
				return $('#requestGridFormatterDiv').html();
			}
			
		},
		/**
		 * @description 是否是问题或变更标题格式化
		 * @param cellValue 列显示值
		 * @param options 操作项
		 * @param rowObject 行对象
		 */
		isToChangeOrProblem:function(cellvalue, options, rowObject){
			  if(cellvalue){
				  return '<span style="color:red">'+i18n.label_basicConfig_deafultCurrencyYes+'</sapn>';
			  }else{
				  return i18n.label_basicConfig_deafultCurrencyNo;
			  }
		},
		/**
		 * @description 请求列表
		 */
		showRequestList:function(){
			var _url='request!findRequests.action';
			if(filterId!=null && filterId!=='' && filterId!=-1){
				_url='request!findPagerRequestByCustomFilter.action?requestQueryDTO.filterId='+filterId;
			}
			if(requestMain_companyNo!=null && requestMain_companyNo!==''){//根据客户
				_url='request!findRequests.action?requestQueryDTO.companyNo='+requestMain_companyNo;
				$('#request_search_companyNo').val(requestMain_companyNo);
			}
			var _postData={};
			$.extend(_postData,{'requestQueryDTO.lastUpdater':userName});			
			if(request_fullSearchFlag=="yes"){//全文检索
				
				var fullSearchQueryString=$('#fullSearchKeyWord').val();
				_url="request!analogousRequest.action";
				$.extend(_postData,{'fullTextQueryDTO.alias':'RequestInfo','fullTextQueryDTO.queryString':fullSearchQueryString});
				
			}else{
				if(countQueryType!=='' && currentUser!==''){
					if(countQueryType=='all')
						_url='request!findRequests.action';
					else{
						$.extend(_postData,{'requestQueryDTO.countQueryType':countQueryType,'requestQueryDTO.currentUser':currentUser});
					}	
				}
			}
			if(countQueryType=='dynamicSearch'){//报表可链接数据传递
				_url='request!findRequestsByCrosstabCell.action';
				$.extend(_postData,{'ktd.rowValue':rowValue,'ktd.colValue':colValue,'ktd.rowKey':rowKey,'ktd.colKey':colKey,'ktd.customFilterNo':customFilterNo});
			}
			var params=$.extend({},jqGridParamsTen,{
				/*caption:i18n['title_request_requestGrid'],*/
				url:_url,
				postData:_postData,
				colNames:['ID','tag',i18n.number,i18n.label_belongs_client,'',i18n.label_ci_serviceDir,i18n.common_title,i18n.category,i18n.title_requester_group,i18n.requester,i18n.common_owner,i18n.title_request_assignToGroup,i18n.title_request_assignToTC,i18n.priority,i18n.label_sla_imode,i18n.label_sla_seriousness,i18n.label_sla_effectRange,i18n.label_sla_level,
				          i18n.common_state,i18n.request_close_Code,'',i18n.common_createTime,i18n.common_updateTime,i18n.title_sla_requestTime,i18n.title_sla_completeTime,i18n.label_sla_response_time,i18n.label_sla_complete_time,
				          i18n.label_request_isConvertdToProblem,i18n.label_request_isConvertdToChange,i18n.title_request_SLAState,i18n.common_action,''],
				colModel:[
				          {name:'eno',width:55,hidden:true},
				          {name:'upgradeApplySign',width:25,formatter:itsm.request.requestMain.upgradeApplySignFormat,sortable:false,align:'center',hidden:false},
				          {name:'requestCode',width:120,hidden:false},
				          {name:'companyName',index:'companyNo',width:100,hidden:hideCompany},
				          {name:'companyNo',hidden:true},
				          {name:'requestServiceDirName',index:'requestServiceDirectory',sortable:false,width:80,align:'center',hidden:false},
						  {name:'etitle',width:180,formatter:itsm.request.requestMain.titleUrlFormatter,hidden:false},
						  {name:'requestCategoryName',index:'requestCategory',width:80,align:'center',hidden:false},
						  {name:'requesterGroup',sortable:false,width:80,align:'center',hidden:false},
						  {name:'createdByName',index:'createdBy',sortable:false,width:80,align:'center',hidden:false},
						  {name:'ownerName',sortable:false,width:80,align:'center',hidden:true},
/*						  {name:'ciName',index:'relatedAsset',width:80,align:'center',hidden:true},*/
						  {name:'assigneeGroupName',index:'assigneeGroup',sortable:false,width:80,align:'center',hidden:false},
						  {name:'technicianName',index:'technician',width:80,sortable:false,align:'center',hidden:false},
						  {name:'priorityName',index:'priority',width:80,align:'center',hidden:false,formatter:function(cellvalue, options, rowObject){
							 return colorFormatter(rowObject.priorityColor,cellvalue);
						  }},
						  {name:'mode',index:'imode',width:80,align:'center',hidden:true,formatter:function(cellvalue, options, rowObject){
							 return colorFormatter(rowObject.modeColor,cellvalue);
						  }},
						  {name:'seriousnessName',index:'seriousness',width:80,align:'center',sortable:false,hidden:true,formatter:function(cellvalue, options, rowObject){
							 return colorFormatter(rowObject.seriousnessColor,cellvalue);
						  }},
						  {name:'effectRangeName',width:80,align:'center',sortable:false,hidden:true,formatter:function(cellvalue, options, rowObject){
							 return colorFormatter(rowObject.effectRangeColor,cellvalue);
						  }},
						  {name:'levelName',index:'level',width:80,align:'center',hidden:true,formatter:function(cellvalue, options, rowObject){
							 return colorFormatter(rowObject.levelColor,cellvalue);
						  }},
						  {name:'statusName',index:'status',width:80,align:'center',hidden:false,sortable:false,formatter:function(cellvalue, options, rowObject){
							 return colorFormatter(rowObject.statusColor,cellvalue);
						  }},
						  {name:'closeCode',width:80,hidden:true},
						  {name:'statusDno',sortable:false,hidden:true},
						  {name:'createdOn',width:120,formatter:timeFormatter,align:'center',hidden:false},
						  {name:'lastUpdateTime',width:80,formatter:timeFormatter,align:'center',hidden:true},
						  {name:'responsesTime',width:80,formatter:timeFormatter,align:'center',hidden:true,sortable:false},
						  {name:'closeTime',width:80,formatter:timeFormatter,align:'center',hidden:true,sortable:false},
						  {name:'maxResponsesTime',width:80,align:'center',hidden:true,sortable:false},
						  {name:'maxCompletesTime',width:80,align:'center',hidden:true,sortable:false},
						  {name:'isConvertdToProblem',width:40,align:'center',hidden:true,sortable:false,formatter:itsm.request.requestMain.isToChangeOrProblem},
						  {name:'isConvertdToChange',width:40,align:'center',hidden:true,sortable:false,formatter:itsm.request.requestMain.isToChangeOrProblem},
						  {name:'slaState',index:'slaState',width:110,align:'center',hidden:false,formatter:itsm.request.requestMain.formatSLAstatus },
						  {name:'act',width:85,sortable:false,align:'center',formatter:function(cell,event,data){
							  return $('#requestGridFormatterDiv').html().replace(/{eno}/g,data.eno).replace(/{statusDno}/g,data.statusDno);
						  }},
						  {name:'slaStateColor',hidden:true}
						  ],
				jsonReader: $.extend({},jqGridJsonReader, {id: "eno"}),
				ondblClickRow:function(rowId){itsm.request.requestMain.requestDetails(rowId);},
				sortname:'eno',
				height:'100%',
				shrinkToFit:false,
				pager:'#requestGridPager'
			});
			$("#requestGrid").jqGrid(params);
			
			$("#requestGrid").navGrid('#requestGridPager',navGridParams);
			//列表操作项
			$("#t_requestGrid").css(jqGridTopStyles);
			$("#t_requestGrid").append($('#requestGridToolbar').html());
			
			//列表显示隐藏
			defaultLoadColumn("#requestGrid");
			//自定义列
			$("#requestGrid").jqGrid('navButtonAdd','#requestGridPager',{
			    caption:"",
			    title:i18n.label_set_column,
			    onClickButton : function (){
			    	loadColumnChooserItem('requestGrid');
			    }
			});			
			//自适应大小
			setGridWidth("#requestGrid","regCenter",20);
			request_fullSearchFlag="no";
//			params=null;
			//windowResize.resizeItsmMainTab();
		},
		/**
		 * SLA状态图标
		 */
		formatSLAstatus:function(cellvalue, options, rowObject){
			var img = rowObject.slaStateDno || 'Other';//如果状态为空就不显示
			var slaImgHtml = '<div title="'+rowObject.slaState+'">  <img  src="../skin/default/slaStatus/'+img+'.png" />  </div>';
			return slaImgHtml;
		},
		
		/**
		 * @description 新增请求
		 */
		showAddRequest:function(){
			basics.tab.tabUtils.closeTab(i18n.request_add_templet_title);
			basics.tab.tabUtils.closeTab(i18n.request_edit_templet_title);
			basics.tab.tabUtils.refreshTab(i18n.title_request_addRequest,'../pages/itsm/request/addRequestFormCustom.jsp');
		},
		/**
		 * @description 请求详细
		 */
		requestDetails_aff:function(){
			checkBeforeEditGrid('#requestGrid',itsm.request.requestMain.requestDetailsOpt);
		},
		/**
		 * @description 打开请求详细
		 * @param eno 事件eno
		 */
		requestDetails:function(eno){
			startProcess();
			basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+eno,i18n.request_detail);
			
		},
		/**
		 * @description 打开请求详细
		 * @param rowData 选中行数据
		 */
		requestDetailsOpt:function(rowData){
			startProcess();
			basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+rowData.eno,i18n.request_detail);
		},
		
		/**
		 * @description 请求编辑操作
		 */
		editRequest_aff:function(){
			checkBeforeEditGrid('#requestGrid', itsm.request.requestMain.showEditRequest);
		},
		
		/**
		 * @description 请求编辑操作
		 * @param eno 事件eno
		 * @param statusDno 状态Code
		 */
		editRequest:function(eno,statusDno){
			if(statusDno=='request_close'){
				msgAlert(i18n.request_close_not_edit,'info');
			}else{
				basics.tab.tabUtils.reOpenTab("itsm/request/editRequestFormCustom.jsp?eno="+eno,i18n.title_request_editRequest);
			}
		},
		
		/**
		 * @description 请求编辑打开
		 * @param rowData 要编辑行的数据对象
		 */
		showEditRequest:function(rowData){
			if(rowData.statusDno=='request_close')
				msgAlert(i18n.request_close_not_edit,'info');
			else
				basics.tab.tabUtils.reOpenTab("itsm/request/editRequestFormCustom.jsp?eno="+rowData.eno+'&eavId='+_eavId,i18n.title_request_editRequest);
		},
		/**
		 * @description 判断是否选择要删除的请求
		 */
		deleteRequest_aff:function(){
			checkBeforeDeleteGrid('#requestGrid', itsm.request.requestMain.deleteRequestOpt);
		},
		
		/**
		 * @description 判断是否选择要删除的请求
		 * @param eno 要删除的请求ID
		 */
		deleteRequest:function(eno){
			msgConfirm(i18n.msg_msg,'<br/>'+i18n.msg_confirmDelete,function(){
				itsm.request.requestMain.deleteRequestOpt(eno);
			});
		},
		/**
		 * @description 删除请求操作
		 * @param rowsId 要删除行的ID
		 */
		deleteRequestOpt:function(rowsId){
			var url="request!deleteRequests.action";
			var param = $.param({'enos':rowsId},true);
			$.post(url, param, function()
			{
				//重新统计				
				$('#requestGrid').trigger('reloadGrid');
				msgShow(i18n.msg_deleteSuccessful,'show');
				itsm.request.requestStats.loadRequestCount();
			}, "json");	
		},
		
		
		/**
		 * @description 搜索
		 */
		searchRequestOpenWindow:function(){
			if(request_search_selectData_flag){
				itsm.request.requestMain.request_s_selectData();
				request_search_selectData_flag=false;
			}
			windows('searchRequestWindow',{width:600,close:function(){
				/*$('#searchRequestWindow input').val('');
				$('#searchRequest_etitle').autocomplete("destroy");
    			$('#searchRequest_requestCode').autocomplete("destroy");*/
			}});
		},
		
		/**
		 * @description 搜索页面数据字典及自定补全绑定
		 */
		request_s_selectData:function(){
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#main_request_companyName','com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long','#request_search_companyNo',userName,'ITSOPUser');
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#searchRequest_etitle','com.wstuo.itsm.request.entity.Request','etitle','etitle','eno','Long','','','false');//标题
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#searchRequest_requestCode','com.wstuo.itsm.request.entity.Request','requestCode','requestCode','eno','Long','','','false');//编号
			
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('requestStatus','#searchRequest_statusNo');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('imode','#searchRequest_imode');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('priority','#searchRequest_priority');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('level','#searchRequest_level');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('effectRange','#searchRequest_effectRange');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('seriousness','#searchRequest_seriousness');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('SLAStatus','#searchRequest_slaStateNo');
			$('#search_createdByName').click(function(){common.security.userUtil.selectUserHolidayTip('#search_createdByName','','','fullName',companyNo);});
			$('#search_ownerName').click(function(){common.security.userUtil.selectUserHolidayTip('#search_ownerName','','','fullName',companyNo);});
			$('#search_assigneeName').click(function(){common.security.userUtil.selectUserHolidayTip('#search_assigneeName','','','fullName',companyNo);});
			
			//选择部门
			$('#search_assigneGroupName').click(function(){
				common.security.organizationTreeUtil.showAll_2('#index_assignGroup_window','#index_assignGroup_tree','#search_assigneGroupName','#search_assigneGroupNo',companyNo);
			});
			
			//请求人所在组
			$('#search_requestorOrgName').click(function(){
				common.security.organizationTreeUtil.showAll_2('#index_assignGroup_window','#index_assignGroup_tree','#search_requestorOrgName','#search_requestorOrgNo',companyNo);
			});
		},
		/**
		 * @description 搜索树结构.
		 */
		search_select_category:function(){
			common.config.category.eventCategoryTree.showSelectTree('#request_category_select_window',
                                                                    '#request_category_select_tree',
                                                                    'Request',
                                                                    '#searchRequest_ecategoryName',
                                                                    '#searchRequest_ecategoryNo');

		},
		/**
		 * @description 提交请求搜索
		 */
		doSearchRequest:function(){
			if($("#main_request_companyName").val()===""){
    			$("#request_search_companyNo").val("");
    		}
			if($('#searchRequestWindow').form('validate')){
				var _url = 'request!findRequests.action';
				if(countQueryType!=='' && currentUser!==''){
					$('#request_countQueryType').val(countQueryType);
					$('#request_currentUser').val(currentUser);
				}else{
					$('#request_countQueryType,#request_currentUser').val('');
				}
				var sdata = $('#searchRequestWindow form').getForm();
				var postData = $("#requestGrid").jqGrid("getGridParam", "postData");
				postData['fullTextQueryDTO.queryString'] = '';
				$.extend(postData, sdata);
				$('#requestGrid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
			}
		},
		
		/**
		 * @description 提交变更
		 */
		requestMainToChange:function(){
			var rowIds = $("#requestGrid").getGridParam('selarrrow');
			if(rowIds=='')
			{
				msgAlert(i18n.msg_atLeastChooseOneData,'info');
			}
			else
			{	
				var compareRowData=$("#requestGrid").getRowData(rowIds[0]);
				var compareResule=true;
				var statusCode=true;
				for(var i=0;i<rowIds.length;i++){
					var rowData=$("#requestGrid").getRowData(rowIds[i]);
					if(compareRowData.companyName!=rowData.companyName){
						compareResule=false;
					}
					if(rowData.statusDno=='request_close'){
						statusCode=false;
					}
				}
				if(compareResule){
					if(statusCode){
						basics.tab.tabUtils.reOpenTab('../pages/itsm/change/addChange.jsp?enos='+rowIds+'&companyNo='+compareRowData.companyNo,i18n.titie_change_add);
					}else{
						msgAlert(i18n.lable_to_change_or_request,'info');
					}
				}else{
					msgAlert(i18n.msg_select_customer_data,'info');
				}
				
			}	
		},
		
		/**
		 * @description 提交问题.
		 */
		request2Problem:function(){
			
			var rowIds = $("#requestGrid").getGridParam('selarrrow');
			if(rowIds==''){
				msgAlert(i18n.msg_atLeastChooseOneData,'info');
			}
			else{
				
				var compareRowData=$("#requestGrid").getRowData(rowIds[0]);
				var compareResule=true;
				for(var i=0;i<rowIds.length;i++){
					var rowData=$("#requestGrid").getRowData(rowIds[i]);
					if(compareRowData.companyName!=rowData.companyName){
						compareResule=false;
					}
				}
				if(compareResule){
					basics.tab.tabUtils.reOpenTab('../pages/itsm/problem/addProblem.jsp?enos='+rowIds+'&companyNo='+compareRowData.companyNo,i18n.problem_add);
				}else{
					msgAlert(i18n.msg_select_customer_data,'info');
				}
				
				
			}	
		},
		
		/**
		 * @description 请求工单
		 */
		requestToPrint:function(){
			checkBeforeEditGrid('#requestGrid',function(rowData){
				window.open('../pages/itsm/request/printTemplates/callTable.jsp?eno='+rowData.eno);
			});
				
		},
		
		
		/**
    	 * @description 通过过滤器查询请求数据
    	 * @param filterId 过滤器ID
    	 */
		getDataByFilterSearch:function(filterId){
			
				var _url = 'request!findPagerRequestByCustomFilter.action';
				
				var _param={};
				
				if(filterId===0||filterId==-1){//空数据
					_url='request!findRequests.action?requestQueryDTO.lastUpdater='+userName;	
					$.extend(_param,{'requestQueryDTO.filterId':0});//will 131228
				}else{
					$.extend(_param,{'requestQueryDTO.filterId':filterId});
				}
				
				$('#requestGrid').jqGrid('setGridParam',{page:1,url:_url,postData:_param}).trigger('reloadGrid');
				
				
		},
		
		/**
    	 * @description 打开过滤器页面
    	 */
		openCustomFilterWin:function(){
			common.config.customFilter.filterGrid_Operation.openCustomFilterWin(options,"request","com.wstuo.itsm.request.entity.Request","request_userToSearch");
		},
		
	
	   /**
	    * @description 导出请求列表.
	    */
		exportRequestView:function(){
			
			
			var _postData = $("#requestGrid").jqGrid("getGridParam", "postData"); //列表参数
			$('#export_request_values').html('');//清空参数
			
			$.each(_postData,function(k,v){
				if(k!=='requestQueryDTO.filterId' || k==='requestQueryDTO.filterId'&& v!==0){//will 131228
					//加入参数
					$("<input type='hidden' name='"+k+"' value='"+v+"'/>").appendTo("#export_request_values");
				}
			});
			var _postUrl = $("#requestGrid").jqGrid("getGridParam", "url"); //列表参数
			
			var _params=$('#export_request_form').serialize();
			var params=(_postUrl.split('?')[1]);
			if(_params.indexOf(params)==-1)//will 140106 防止重复
				params=params+"&"+_params;
			$.post('request!exportRequest.action',params,function(data){
				$('#exportInfoGrid').trigger('reloadGrid');
				basics.tab.tabUtils.addTab(i18n.exportDown,'../pages/common/tools/includes/includes_exportManage.jsp');
			});
		},

		
		/**
		 * @description 全文检索
		 * @param queryString 查询条件字段串
		 */
		showFullSearchGrid:function(queryString){
			
				basics.tab.tabUtils.addTab(i18n.title_request_requestGrid,'../pages/itsm/request/requestMain.jsp');//如果没打开就打开显示
				
				setTimeout(function(){
					
					var _url="request!analogousRequest.action";
					var _fullSearchData = $.param({'fullTextQueryDTO.alias':'RequestInfo','fullTextQueryDTO.queryString':''});								
					$('#requestGrid').jqGrid('setGridParam',{page:1,url:_url,postData:_fullSearchData}).trigger('reloadGrid');
					
				},1000);
				
		},
		
		
		/**
		 * @description 导入数据.
		 */
		importRequestData:function(){
			windows('importRequestDataWindow',{width:400});
		},
		
		/**
		 * @description 请求导入
		 */
		importRequest:function(){
				$.ajaxFileUpload({
		            url:'request!importRequest.action',
		            secureuri:false,
		            fileElementId:'importRequestFile', 
		            dataType:'json',
		            success: function(data,status){
		             	$('#importUserDataWindow').dialog('close');
		            	if(data=="FileNotFound"){
							msgAlert(i18n.msg_dc_fileNotExists,'info');
						}else if(data=="IOError"){
							msgAlert(i18n.msg_dc_importFailure,'info');
						}else if(data=="TCNotEnough"){
							msgAlert(i18n.TechnicianLicense_Not_Enough,'info');
						}else{
							$('#userGrid').trigger('reloadGrid');
							msgAlert(i18n.msg_dc_dataImportSuccessful+'<br>['+
									data
									.replace('Total',i18n.opertionTotal)
									.replace('Insert',i18n.newAdd)
									.replace('Update',i18n.update)
									.replace('Failure',i18n.failure)+']','show');
							
						}
		            }
		        });
		},
		mianBottomShow:function(){
	        itsm.request.requestStats.countAllRquest();
			//basics.showChart.showChart("request");
		},
		requestBatchClose:function(){
			startProcess();
			var requestCodes=[];
			var enos=[];
			$.each(requestBatchClose_enos, function(i, value) {
				var rowData=$("#requestGrid").jqGrid("getRowData",value);
				if(!rowData.closeTime){
					requestCodes.push(rowData.requestCode);
					enos.push(rowData.eno);
				}
			});
			if(enos.length>0){
				var param = $.param({'requestDTO.enos':enos,'requestDTO.requestCodes':requestCodes,'requestDTO.remark':$("#requestDTO_remark").val()},true);
				$.post('request!requestBatchClose.action',param,function(data){
					$('#requestGrid').trigger('reloadGrid');
					$('#requestBatchCloseWindow').dialog('close');
					endProcess();
					//basics.tab.tabUtils.addTab(i18n.exportDown,'../pages/common/tools/includes/includes_exportManage.jsp');
				});
			}else{
				endProcess();
				$('#requestBatchCloseWindow').dialog('close');
				msgAlert(i18n.Request_for_selection_closed,'info');
			}
		},
		/**
		 * @description 初始化
		 */
		init: function(){
			//绑定日期控件
			DatePicker97(['#request_search_startTime','#request_search_endTime']);
			itsm.request.includes.includes.loadRequestActionIncludesFile();
			common.security.includes.includes.loadSelectCustomerIncludesFile();
			common.config.includes.includes.loadCategoryIncludesFile();
			common.config.includes.includes.loadCustomFilterIncludesFile();
			$("#requestMain_loading").hide();
			$("#requestMain_content").show();
			itsm.request.requestMain.showRequestList('');
			$('#addRequestBtn').click(itsm.request.requestMain.showAddRequest);
			$('#editRequestBtn').click(itsm.request.requestMain.editRequest_aff);
			$('#deleteRequestBtn').click(itsm.request.requestMain.deleteRequest_aff);
			$('#searchRequestBtn').click(itsm.request.requestMain.searchRequestOpenWindow);
			$('#doSearchRequestBtn').click(itsm.request.requestMain.doSearchRequest);
			$('#submitChangeBtn_list').click(itsm.request.requestMain.requestMainToChange);
			$('#request2problem').click(itsm.request.requestMain.request2Problem);
			$('#requestToPrint').click(itsm.request.requestMain.requestToPrint);
			$('#refresh_requestGrid').click(itsm.request.requestStats.requestStat);
			$('#request_customFilter').click(itsm.request.requestMain.openCustomFilterWin);
			//选择公司
			$('#icon_search_request_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#request_search_companyNo','#main_request_companyName');
			});
			//搜索分类
			$('#searchRequest_ecategoryName').click(itsm.request.requestMain.search_select_category);
			
			
			$('#request_mian_export').click(itsm.request.requestMain.exportRequestView);
			
			$('#request_reopen_opt_but').click(function(){//reopen
				windows('request_reopen_win',{width:500});
			});
			common.config.customFilter.filterGrid_Operation.loadFilterByModule("#request_userToSearch","request");//加载过滤器
			
			if(reqRenovate){
					var intervalId = window.setInterval(function(){
						tabValue=$("ul[class=tabs] li[class=tabs-selected] span[class=tabs-title tabs-closable]").text();
						if(tabValue == i18n.title_request_requestGrid){
							tabValue=$("ul[class=tabs] li[class=tabs-selected] span[class=tabs-title tabs-closable]").text();
							reqRenovate=false;$('#requestGrid').trigger('reloadGrid');
						}
						},180000); 
					reqRenovate=true;
			}
			//windowResize.resizeItsmMainTab();
			itsm.request.requestMain.mianBottomShow();
			
			$('#requestBatchClose_link').click(function(){
				checkBeforeMethod('#requestGrid', function(rowsId){
					$("#requestDTO_remark").val("");
					windows('requestBatchCloseWindow',{width:400});
					requestBatchClose_enos=rowsId;
				});
			});
		}
	};
}();
$(document).ready(itsm.request.requestMain.init);