﻿$package('itsm.request.includes'); 
/**  
 * @author QXY  
 * @constructor Includes
 * @description "请求用于加载Includes文件JS主函数"
 * @date 2012-7-31
 * @since version 1.0 
 */ 
itsm.request.includes.includes=function(){
	//是否已加载请求动作页面
	this._loadRequestActionIncludesFileFlag=false;
	//关联的请求
	this._loadRelatedRequestIncludesFileFlag=false;
	
	return {
		
		/**
		 * 加载请求动作includes文件
		 */
		loadRequestActionIncludesFile:function(){
			if(!_loadRequestActionIncludesFileFlag){
				$('#requestAction_html').load('itsm/request/includes/requestActionHtml.jsp',function(){
					$.parser.parse($('#requestAction_html'));
					
				});
			}
			_loadRequestActionIncludesFileFlag=true;
		},
		/**
		 * 加载选择关联请求窗口includes文件
		 */
		loadRelatedRequestIncludesFile:function(){
			if(!_loadRelatedRequestIncludesFileFlag){
				$('#relatedRequest_html').load('itsm/request/includes/includes_relatedRequest.jsp',function(){
					$.parser.parse($('#relatedRequest_html'));
				});
			}
			_loadRelatedRequestIncludesFileFlag=true;
		},
		init:function(){
			
		}
	};
}();
$(function(){itsm.request.includes.includes.init();});