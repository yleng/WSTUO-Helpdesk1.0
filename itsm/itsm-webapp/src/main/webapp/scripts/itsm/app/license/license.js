$package('itsm.app.license')

/**  
 * @author QXY  
 * @constructor license
 * @description "license"
 * @date 2010-11-17
 * @since version 1.0 
 */  
itsm.app.license.license=function(){
	var url;
	var param;
	return {
		/**
		 * 打开搜索面板
		 */
		openSearchWindow:function(){
			windows('search_dialog',{width: 400});
		},
		/**
		 * 客户类型国际化
		 */
		customerTypeForma:function(cell){
			if(cell=='ITSOP')
				return i18n['label_license_itsopt']
			else
				return i18n['label_license_intranet']
		},
		/**
		 * 版本类型
		 */
		versionType:function(cell){
			if(cell=='Official')
				return i18n['label_formal_version'];
			else
				return '<span style="color:red;">'+i18n['label_trail_version']+'<span>';
		},
		/**
		 * 加载列表
		 */
		showGird:function(){
			var params = $.extend({},jqGridParams,{
					url:'license!findAll.action',
					mtype:'post',
					datatype: "json",
//					caption:i18n['title_license_grid'],
					colNames:[i18n['title_license_id']/*,i18n['label_software_type']*/,i18n['label_license_category'],i18n['version'],i18n['label_license_thLicenseNum'],i18n['label_license_outCustomerNum'],i18n['label_license_alreadyUsed'],i18n['label_license_overDay'],i18n['label_license_startUseDate'],i18n['label_license_endUseDate'],i18n['label_license_registerDate']],
					colModel:[
					   {name:'id',width:25,align:'center',sortable:true},
					   /*{name:'customerType',width:60,align:'center',sortable:true,formatter:itsm.app.license.license.customerTypeForma},*/
					   {name:'versionType',width:60,align:'center',sortable:true,formatter:itsm.app.license.license.versionType},
					   {name:'version',width:60,align:'center',sortable:true},
					   {name:'thLicenseNum',width:60,align:'center',sortable:true},
					   {name:'outCustomerNum',width:60,align:'center',sortable:true,hidden:true},
					   {name:'used',width:60,align:'center',sortable:true,hidden:true},
					   {name:'surplus',width:60,align:'center',sortable:true,hidden:true},
					   {name:'startDate',width:60,align:'center',sortable:true,formatter:timeFormatter},
					   {name:'endDate',width:60,align:'center',sortable:true,formatter:timeFormatter},
					   {name:'activate',width:60,align:'center',sortable:true,formatter:timeFormatter}
					],	
					jsonReader:$.extend({},jqGridJsonReader,{id:'id'}),    //这里的ID，必须对应正确，否则，在取ID时，就会出错
			        height:'100%',
			    	multiselect:false,
			        sortname:'id',
			        sortorder:'desc',
				   	pager: '#license_pager',
					gridComplete:function(){
						$('#search_license_window').bind('click',itsm.app.license.license.openSearchWindow);
					}
				});
				$("#licenseTable").jqGrid(params);
				$("#licenseTable").navGrid('#license_pager',navGridParams);
				$("#t_licenseTable").css(jqGridTopStyles);
				$("#t_licenseTable").html($('#gridToolbar').html());//将工具栏写到表格里
				//自适应大小
				//setGridWidth("#licenseTable","regCenter",20);
		},
		/**
		 * 加载日期进度条
		 */
		showSurplus_progressbar:function(totalDay,surplus){
			var totalDayNum = (100/totalDay)*surplus;
			var warnSurplus = totalDay*(1-0.8);
			if(surplus<warnSurplus){
				$("#surplus_progressbar").css('background','red');
				$("#surplus_value_span").css('color','red');
			}
			$("#surplus_progressbar").progressbar({value: totalDayNum});
			$('#surplus_value_span').text("  "+surplus+"/"+totalDay);
		},
		/**
		 * 加载技术员进度条
		 */
		showInfo:function(){			
			url='license!readTxt.action';
			$.post(url,param,function(result){
				var licenseDTO=result["data"][0];
				$('#thLicense_value').text(licenseDTO.thLicenseNum);
				if(licenseDTO.concurrent!="0"){
					$('#concurrent_label').text(i18n.label_concurrent_license_number);
					$('#concurrent_value').text(licenseDTO.concurrent);
				}else{
					$('#concurrent_label').text('');
					$('#concurrent_value').text('');
				}
				if(licenseDTO.customerType=='ITSOP'){
					$('#customerType_value').text(i18n['label_license_itsopt']);
				}else{
					$('#customerType_value').text(i18n['label_license_intranet']);
				}
				$('#startDate_value').text(timeFormatter(licenseDTO.startDate));
				$('#endDate_value').text(timeFormatter(licenseDTO.endDate));
				$('#used_value').text(licenseDTO.used);
				$('#surplus_value').text(licenseDTO.surplus);
				
				if(licenseDTO.versionType=='Official')
					$('#license_category').text(i18n['label_formal_version']);
				else
					$('#license_category').text(i18n['label_trail_version']);
				$('#WSTUO_version').text(licenseDTO.version);
				//查询已用技术员, 并用进度条显示详细
				$.post('license!countTechnician.action',function(res){
					var barNum = (100/licenseDTO.thLicenseNum)*(licenseDTO.thLicenseNum-res);
					var warnNum = licenseDTO.thLicenseNum*(1-0.8);
					if(licenseDTO.thLicenseNum-res < warnNum){
						$("#thLicense_progressbar").css('background','red');
						$("#thLicense_value_span").css('color','red');
					}else{
						$("#thLicense_progressbar").css('background','');
						$("#thLicense_value_span").css('color','');
					}
					$("#thLicense_progressbar").progressbar({value: barNum});
					$('#thLicense_value_span').text("  "+(licenseDTO.thLicenseNum-res)+"/"+licenseDTO.thLicenseNum);
				});
				//查看总天数
				itsm.app.license.license.showSurplus_progressbar(licenseDTO.totalDay,licenseDTO.surplus);
			});
		},
		/**
		 * 打开
		 */
		openLicenseLoadWin:function(){
			windows('license_code',{width:600});
		},
		/**
		 * 许可码写入
		 */
		rereadTxt:function(){
			if($('#license_code_form').form('validate')){
				//将许可码写入，进行判断是否可用
				url = "license!checkLicenseCode.action";
				param = 'licenseCoed='+$('#licenseCode').val();
				$.post(url,param,function(result){
					if(result==true || result=='true'){
						msgAlert(i18n['label_license_rightCode_restartService'],'info');
						$('#licenseCode').val('');
						$('#license_code').dialog('close');
						itsm.app.license.license.showInfo();
						$('#licenseTable').trigger('reloadGrid');
					}else{
						msgAlert(i18n['label_license_errorCode'],'error');
					}
				});
			}
		},
		/**
		 * 打开导入许可面板
		 */
		openUploadWindow:function(){
			windows('license_file',{width:350});
		},
		/**
		 * 上传许可文件
		 */
		uploadLicense:function(){
			if($('#importLicenseFile').val()!=''){
				$.ajaxFileUpload({
		            url:'license!uploadLicense.action',
		            secureuri:false,
		            fileElementId:'importLicenseFile',
		            dataType:'json',
		            success: function(data){
		             	if(data){
		             		$('#importLicenseFile').val('');
		             		$('#license_file').dialog('close');
							itsm.app.license.license.showInfo();
							$('#licenseTable').trigger('reloadGrid');
							msgAlert(i18n['label_license_rightCode_restartService'],'info');
		             	}else{
		             		msgAlert(i18n['label_license_errorFile'],'error');
		             	}
		            }
		        });
			}else{
				msgAlert(i18n['label_license_nullFile'],'warning');
			}
		},
		/**
		 * 禁用许可
		 */
		disableLicenseKey:function(){
			var url = "licenseKey!disableLicenseKey.action";
			msgConfirm(i18n['common_disable']+'--'+i18n['label_ci_softwareLicenses'],'<BR/><font color="red">'+i18n['disable_licenseKey_tip']+'</font>',function(){
				$.post(url,function(result){
					if(result=="true"){
						setTimeout(function(){msgAlert(i18n['disbale_licenseKey_success'],'info')},1000);
					}
				});
			});
		},
		/**
		 * 初始化
		 */
		init:function(){
			//绑定日期控件
			DatePicker97(['#MMOA','#MMMW']);
			$("#license_loading").hide();
			$("#license_content").show();
			itsm.app.license.license.showInfo();
			itsm.app.license.license.showGird();
			$('#upload_license_window').click(itsm.app.license.license.openUploadWindow);
			$('#licenseCode_input').click(itsm.app.license.license.openLicenseLoadWin);
			$('#licenseCode_submit').click(itsm.app.license.license.rereadTxt);
			$('#uploadFile_license_window_submit').click(itsm.app.license.license.uploadLicense);
			$('#disable_license_window').click(itsm.app.license.license.disableLicenseKey);
		}
	};
}();
$(function(){itsm.app.license.license.init()});
