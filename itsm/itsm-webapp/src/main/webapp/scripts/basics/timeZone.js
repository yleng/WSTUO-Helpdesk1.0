
/**
 * 获取当前系统时区
 * @param format 返回值的前缀格式(xxx+0800)
 * @returns {String}
 */
function clientTimeZone(format){
	var munites = new Date().getTimezoneOffset();
	var hour = parseInt(munites / 60);
	var munite = munites % 60;
	var prefix = "-";
	if (hour < 0 || munite < 0) {
	     prefix = "+";
	     hour = -hour;
	     if (munite < 0) {
	    	 munite = -munite;
	     }
	}
	hour = hour + "";
	munite = munite + "";
	if (hour.length == 1) {
		hour = "0" + hour;
	}
	if (munite.length == 1) {
		munite = "0" + munite;
	}
	return format+prefix + hour + munite;
}