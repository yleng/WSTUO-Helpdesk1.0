/**  
 * @author QXY & Van
 * @constructor indexPage
 * @description "indexPage"
 * @date 2010-11-17
 * @since version 1.0 
 */
$package('itsm.common');
$import('basics.includes');
$import('itsm.cim.includes.includes');
$import('common.security.includes.includes');
$import('common.report.includes.includes');
$import('common.config.includes.includes');
$import('common.jbpm.includes.includes');
itsm.common.setting = function(){
	return {
		//判断值是否为空,如果为Null，则返回空字符串
		valueNullReturn:function(value){
			if(value==null || value=='null')
				return '';
			else
				return value;
		},
		
		init:function(){
			
		}
		
		
	}
}();
var settingPage=function(){	 
		this.showIndexLogo=function(){
			var url = 'organization!findCompany.action?companyNo='+companyNo;
			var src = '../images/GuoYu_Logo.png';
			$.post(url,function(res){
			    src = "logo.jsp?picName=" + res.logo;
				if($(res.logo)!=''){
					if(res.homePage != ''){
						$('#index_logo_href').attr({'href':res.homePage});
					}else{
						$('#index_logo_href').attr({'onClick':'javascript:basics.tab.tabUtils.addTab("'+i18n.companyInformation+'","../pages/common/security/company.jsp")'});
					}					
				}
				$('#index_logo_href').html('<img id="index_logo" height="36px" src="' + src + '">');
                $('.index_header_north_company').show();
			});		
		},
		 /**
		  * 注销
		  */
		 this.logOut=function(){
			msgConfirm(i18n['tip'], '<br/>'+i18n['systemExitConfirm'], function(){
				window.location='logout.jsp';
			});
		 }
		/**@description 基础设置左菜单*/
		this.showTab=function(id)
		{
			$("#menuContent").html($('#'+id).html());
			
			var outTime=500;
			
			if(id=="003"){
				startProcess();
				showDictionaryGroup();  
			}
			if(id=="006"){
				startProcess();
				showRulePackage();
			}
			if(id=="002"||id=="004"){
				startProcess();
				setTimeout(function(){
				$("#menuContent").html($('#'+id).html());
				endProcess();
				},outTime);  
			}
			for(var i=1;i<10;i++)
			{
				if(document.getElementById("M00"+i)!=null){
					document.getElementById("M00"+i).className="mianstyle outfuc";
				}
			}
			if(document.getElementById("M"+id)!=null){
				document.getElementById("M"+id).className="mianstyle onfuc";
			}
			
		}
		
	  /**@description 上传文件   */
		this.commonUploadFile=function(fileId,valueId,showId){
			startProcess();
			var fileUrl=$('#'+fileId).val();
			var tag=true;
			var fileType=fileUrl.lastIndexOf(".");
			
			if(fileType==-1){
				tag=false;
				
			}else{
				fileType=fileUrl.substring(fileType+1,fileUrl.length).toLowerCase();
/*				if(fileId=="uploadReportLogo"){//报表只能上传 *.png格式
					if(fileType=="png"){
						tag==true;
					}else{
						tag=false;
					}
				}else{*/
					if(fileType=="jpg"||fileType=="gif"||fileType=="png"||fileType=="bmp"){
						tag==true;
					}else{
						tag=false;
					}
				
			}
			
			
			if(tag==false){
/*				if(fileId=="uploadReportLogo"){
					$('#uploadReportLogo').val("");
					msgAlert(i18n['uploadFileReportError'],'info');
				}else*/
				msgAlert(i18n['uploadFileFormatError'],'info');
				$('#'+fileId).val("");
				$('#'+valueId).val("");
				endProcess();

			}else{
				var url='fileUpload.action';
				if(fileId=="uploadReportLogo"){
					url+="?imgSize=true";
				}
				$.ajaxFileUpload({
						url:url,
						secureuri:true,
						fileElementId:fileId,
						dataType:'text',
						success: function (data, status){
						   if(data==="\"Image is too big\""){
							   msgShow(i18n["ImageToBig"],'show');
						   }else{
							   $('#'+valueId).val(data);
							   $('#'+showId).html('<img id="img'+showId+'" src="logo.jsp?picName='+data+'" max-width="135px" height="30px" />');
						   }
						   endProcess();
						},
						error: function (data, status, e){
						    msgAlert(e);
						    endProcess();
			            }
					});
			}
				return false;
		}
		
		 /**@description 加载请求首页 */
		this.showRequestIndex=function(){
			
//			expandSubMenu();
			//showLeftMenu('../pages/itsm/request/leftMenu.jsp?random='+Math.random()*10+1,'leftMenu');				$('#itsmMainTab').tabs('select',i18n['title_request_requestGrid']);
			var _url='../pages/itsm/request/requestMain.jsp?'
			if(!isAllRequest_Res){//是否允许查看全部权限
				_url=_url+'&countQueryType=myProposedRequest&currentUser='+userName
			}
			//basics.tab.tabUtils.showByArray(basics.tab.modelTabs.request);
			basics.tab.tabUtils.addTab(i18n['title_request_requestGrid'],_url,function(){
				itsm.request.requestTop.includesLoading();
			});
		}
		  /**@description 动态加载数据字典 */
		this.showDictionaryGroup=function(){
		   	$.post("dataDictionaryGroup!findGroup.action", function(data) {
		   		$('#thirdMenu_systemSetting_dataDictionary ul').html(data);
		   		endProcess();
		     });
		}
		
	     /**@description 加载规则   */        
		this.showRulePackage=function(){    
			//var operationRuleNotifyRuleDIV = $('#operationRuleNotifyRuleDIV').html();
			$.get("rulePackage!findRulePackage.action", function(data) {
		   		$('#thirdMenu_systemSetting_businessRule ul').html(data);
		   		leftMenu.isSecondMenuScroll();
		   		leftMenu.thirdMenuClickAction();
		   		endProcess();
		       });
		}
	 	 /**@description 加载左菜单  */
		this.showLeftMenu=function(url,div)  
		{
			$('#'+div).empty();
			$('#'+div).load(url);
		}
	
      /**@description  读取未读信息 */
	this.showMessager=function() {
			$.post('immanage!findPager.action?manageQueryDTO.status=0&manageQueryDTO.receiveUsers='+userName,function(data){
				if(data!=null){
					$('#norederim').text(data.totalSize);
				}
			});	
	}
	this.look_Details=function(eventmodel,eno,id,winId){
		if(eventmodel=='itsm.problem' || eventmodel=='com.wstuo.itsm.problem.dto.ProblemDTO'){
			basics.tab.tabUtils.reOpenTab('problem!findProblemById.action?eno='+eno,i18n['probelm_detail']);
		}else if(eventmodel=='itsm.change' || eventmodel=='com.wstuo.itsm.change.dto.ChangeDTO'){
			basics.tab.tabUtils.reOpenTab('change!changeInfo.action?queryDTO.eno='+eno,i18n['change_detail']);
		}else if(eventmodel=="itsm.request" || eventmodel=='com.wstuo.itsm.request.dto.RequestDTO'){
			basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+eno,i18n["request_detail"]);
		}else if(eventmodel=="itsm.release" || eventmodel=='com.wstuo.itsm.release.dto.ReleaseDTO'){
			basics.tab.tabUtils.reOpenTab("release!releaseDetails.action?releaseDTO.eno="+eno,i18n["release_details"]);
		}else if(eventmodel=="itsm.cim" || eventmodel=='com.wstuo.itsm.cim.dto.CIDTO'){
			basics.tab.tabUtils.reOpenTab('ci!findByciId.action?ciEditId='+eno,i18n['ci_configureItemInfo']);
		}
		var postUrl="immanage!findInstantmessageById.action";
		var param="imDTO.imId="+id;
		$.post(postUrl,param,function(imDTO){
			if(imDTO.isReadRequest=='0'){
				$.post('immanage!update.action','imDTO.imId='+id+'&imDTO.status=1&imDTO.isReadRequest=1',function(){
					$('#IMjqGrid').trigger('reloadGrid');
				})
				if(messageSize==1){
					stopBlinkNewMsg();
				}
				messageSize-=1;
			}
		});
		$("#"+winId).dialog('close');
	}
	
	 /**@description  快速创建 */
	this.fastCreateRequest=function(){
		var title= $('#fastCreateEtitle').val();
		$('#fastCreateEtitle').val(trim(title));
		 if($('#fastCreateDiv form').form('validate')){
				var frm = $('#fastCreateDiv form').serialize();
				var url = 'request!saveRequest.action';
				startProcess();
				$.post(url,frm, function(eno){
					endProcess();
					//重新统计请求
					msgShow(i18n['createSuccess'],'show');
					$('#fastCreateEtitle').val('');
					$('#fastCreateEdesc').val('');
					$('#fastRequest_priority').val('');
					$('#fastRequest_effectRange').val('');
					$('#fastRequest_seriousness').val('');
//					itsm.request.requestStats.requestCountSearch('myProposedRequest')
//					showWaitAssign();//刷新待指派的请求
//					itsm.request.requestStats.loadRequestCount();
//					itsm.request.requestStats.countConvertedProblem();
//					itsm.request.requestStats.countConvertedChange();
					$('#requestGrid').trigger('reloadGrid');
					basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+eno,i18n["request_detail"]);
					itsm.request.requestStats.requestStat();//刷新二级菜单的请求统计
					itsm.request.requestStats.companyStat();//刷新二级菜单的所属客户统计
				});
		 }

	}
	
	/**
	 * 重置表单.
	 * @param fromId
	 */
	this.resetForm=function(fromId){
		clearFormData(fromId); 
	}
	
	/**
	 * 根据用户名字符串判断用户是否存在.
	 * @param userNames
	 * @param method
	 */
	this.validateUsersByFullName=function(userNames,method){
		var userNamesArrs=userNames.split(";");
		//userNames=userNames.replace(/;/g,'').replace(/,/g,'');
		var url='user!vaildateUserByFullName.action';
		var _param = $.param({'userNames':userNamesArrs},true);

		$.post(url,_param, function(res){
			if(res==true || res=="true"){					
				method(userNamesArrs);
			}else{
				msgAlert(i18n['msg_msg_userNotExist'],'info');
			}
		});
	}
	/**
	 * 根据登录名符串判断用户是否存在.
	 * @param userNames
	 * @param method
	 */
	this.validateUsersByLoginName=function(userNames,method){
		startLoading();
		userNames=userNames.replace(/,/g,';');
		var userNamesArrs=userNames.split(";");
		var url='user!vaildateUserByLoginName.action';
		var _param = $.param({'userNames':userNamesArrs},true);
		var validName = trim(userNames.replace(/;/g,''));
		if(validName==''&& userNames.indexOf(';')>=0){
			endLoading();
			msgAlert(i18n['msg_msg_userNotExist'],'info');
		}else{
			$.post(url,_param, function(res){
				if(res || res=="true"){
					endLoading();
					method(userNamesArrs);
				}else{
					endLoading();
					msgAlert(i18n['msg_msg_userNotExist'],'info');
				}
			});
		}
	}

	//清空指定ID值
	this.cleanIdValue=function(id1,id2){
		$('#'+id1).val('');
		$('#'+id2).val('');
	}
	this.cleanValue=function(id){
		$('#'+id).val('');
	}

	//设置Cookie
	this.setCookie=function(c_name,value,exdays){
		
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}
	//获取Cookie并返回字符串
	this.getCookie=function(Name) 
	{ 
	    var search = Name + "=" 
	    if(document.cookie.length > 0) 
	    { 
	        offset = document.cookie.indexOf(search) 
	        if(offset != -1) 
	        { 
	            offset += search.length 
	            end = document.cookie.indexOf(";", offset) 
	            if(end == -1) end = document.cookie.length 
	            return unescape(document.cookie.substring(offset, end)) 
	        } 
	        else return "" 
	    }
	}
	//删除数组值班为""元素，str:字符串
	this.strToArray=function(str){
		if(str!=null || str!=''){
			var array=str.split(',')
			array = jQuery.grep(array, function (a) { return a != ''; });
			return array;
		}else{
			return '';
		}
		
	}
	//打开帮助
	this.openHelp=function(){
		var param=$('#itsmMainTab').tabs('getSelected').panel('options').tab.text(); 
		var _url='help/help_'+language+'.jsp?function='+param
		_url=encodeURI(_url);
		//_url=encodeURI(_url);
		window.open(_url);
	}
	
	//日期格式化
	this.dateFormat=function(date){
		Date.prototype.format = function(format){
			 /*
			  * eg:format="YYYY-MM-dd hh:mm:ss";
			  */
			var o = {
			  "M+" :  this.getMonth()+1,  //month
			  "d+" :  this.getDate(),     //day
			  "h+" :  this.getHours(),    //hour
			  "m+" :  this.getMinutes(),  //minute
			  "s+" :  this.getSeconds(), //second
			  "q+" :  Math.floor((this.getMonth()+3)/3),  //quarter
			  "S"  :  this.getMilliseconds() //millisecond
			}
				 
			if(/(y+)/.test(format)) {
				format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
			}
			for(var k in o) {
				 if(new RegExp("("+ k +")").test(format)) {
				      format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
				 }
			}
			return format;
		}
		//使用方法:
		var testDate = new Date(date);
		var testStr = testDate.format("yyyy-MM-dd hh:mm:ss");
		return testStr;
	}
	/**
	 * @description 全选
	 * */
    this.checkAll=function(id,divId,name) {
    	if($('#'+id).attr('checked'))
    		$('#'+divId+' input[name="'+name+'"]').attr("checked",true);
    	else
    		$('#'+divId+'  input[name="'+name+'"]').attr("checked",false);
    }
    //检测输入的邮件地址是否正确
	this.checkEmail=function(_address,allowNull){
		var result = true;
		var email='';
		var ck=/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
		if(_address!=null && _address!='' && _address!=undefined){
			email=_address.split(';');
		}else{
			if(allowNull==true)
				result=true;
			else
				result=false;
		}
		for(var i=0;i<email.length;i++){
			email[i]=trim(email[i]);
			if(email[i]!=null && email[i]!='' && !ck.test(email[i])){
				result=false;
			}
		}
		return result;
	}
	
	/**
	 * 刷新门户.
	 */
	this.refreshProtal=function(){
		common.config.userCustom.dashboardDataLoad.loadDashboardData();
		//$('#itsmMainTab').tabs('getTab','门户').panel('refresh');
	}
	this.resizePortalGrid=function(){
		for(var gid=0;gid<portalGridID.length;gid++){
			if(document.getElementById("protlet_"+portalGridID[gid])!=null){
				var wid=$("#protlet_"+portalGridID[gid]).width();//document.getElementById("protlet_"+portalGridID[gid]).style.width;
				if(wid!="" && wid!=undefined && wid!=0){
					$("#TAB_ID_"+portalGridID[gid]).setGridWidth(wid-12);
				}
			}
		}
	}
	this.hideresizePortalGrid=function(){ 
		for(var gid=0;gid<portalGridID.length;gid++){
			if(document.getElementById("protlet_"+portalGridID[gid])!=null){
				var wid=$("#protlet_"+portalGridID[gid]).width();
				if(wid!="" && wid!=undefined && wid!=0){
					$("#TAB_ID_"+portalGridID[gid]).setGridWidth(wid-20);
				}
			}
		}
	}
	this.portalScrollRefresh=function(){
		var offsetHeight=document.getElementById('helpdeskPortalTabid').offsetHeight;
		  var scrollHeight=document.getElementById('helpdeskPortalTabid').scrollHeight;
		  if(offsetHeight!==scrollHeight){
			  resizePortalGrid();
		  }
		  //出现滚动条时Grid重新自适应宽度
	}
	
	
	//加载欢迎页面
	this.welcomeIndex=function(){
		
		$('#main_menu_tabs li').removeClass("tabs-selected");
		$('#protal_link_li').addClass("tabs-selected");
		showLeftMenu('../pages/itsm/portal/leftMenu.jsp','leftMenu');
		
		$('#itsmMainTab').tabs('add',{
			title:i18n['label_quick_launch'],
			cache:"false",
			href:'../pages/includes/welcome_tab.jsp?random='+new Date().getTime(),
			closable:false
		});
		
	}
	//初始化文本编辑器
	this.initCkeditor=function(textarea,type,callback){
		var ckeditor;
		if(CKEDITOR.instances[textarea]){
			if(isckeditor == 'true'){
				CKEDITOR.instances[textarea].destroy(true);
			}
		}
		if(type=='Simple'){
			ckeditor=CKEDITOR.replace(textarea,{
				toolbar:[
				         ['Bold','Italic','Underline','Link','Unlink','Image','Table','Maximize','-']
				]
			});
		}else{
			ckeditor=CKEDITOR.replace(textarea);
		}
		callback();
		return ckeditor
	
	}
	
	
	//清空文本编辑器
	this.ckeditorDestroy=function(title){
		var textareId = '';
		if(title==i18n["title_request_addRequest"]){
			textareId='request_edesc';
		}
		if(title==i18n['request_close_not_edit']){
			textareId='requestEdit_edesc';
		}
		if(title==i18n['problem_add']){
			textareId='add_problem_edesc';
		}
		if(title==i18n['problem_edit']){
			textareId='problem_edit_edesc';
		}
		if(title==i18n['titie_change_add']){
			textareId='add_changeDesc';
		}
		if(title==i18n['titie_change_edit']){
			textareId='edit_changeDesc';
		}
		if(title==i18n['title_request_newKnowledge']){
			textareId='addKnowledgeCon';
		}
		if(title==i18n['knowledge_editKnowledge']){
			textareId='editKnowledgeCon';
		}
		
		if(title==i18n['title_add_scheuledTask']){
			textareId='add_scheduledTask_edesc';
		}
		if(title==i18n['title_edit_scheuledTask']){
			textareId='edit_scheduledTask_edesc';
		}
		if(CKEDITOR.instances[textareId]){
			CKEDITOR.instances[textareId].destroy(true);
		}
	}
	
	this.setHeaderHeight=function(height){
		$('#index_header_north').css({'height':height,'overflow-x':'auto'});
	}
	/**初始化主页面顶部菜单高度*/
	this.initHeaderHeight=function(){
		var minWidth = 1350;
		if(language=='en_US' || language=='ja_JP'){
			minWidth = 1630;
		}
		
		var dom1=document.getElementById("index_header_north").clientHeight;
		var dom2=document.getElementById("index_header_north").scrollHeight;
		var x=dom1-dom2;
		if(getWindowWidth()<minWidth){
			setHeaderHeight('92px');
			$('.layout-panel-west .panel-header,#regCenter').css({'margin-top':'0px'});
		}else{
			setHeaderHeight('92px');
			$('.layout-panel-west .panel-header,#regCenter').css({'margin-top':'0px'});
		}
	}
	return{
		init:function(){
		    
			//加载首页Logo
			showIndexLogo();
			//setInterval(refreshProtal,60000);//一分钟刷新一次门户
			//showMessager();//读取IM
			
			setTimeout(function() {
		        //helpdeskProtal();
				if("ROLE_ENDUSER,"===userRoleCode || "ROLE_ITSOP_MANAGEMENT,"===userRoleCode){
					$('#itsmMainTab').tabs('add',{
	                       title:i18n['companyInformation'],
	                       cache:"true",
	                       href:'../pages/common/security/company.jsp',
	                       closable:false
	                   });
	                   // 直接调用菜单的点击事件, 就不用重新写太多东西
	                   firstMenu && $('#secondMenu_systemSetting_' + firstMenu).click();
	                   secondMenu && $('#thirdMenu_' + secondMenu).click();
				}else{
				    var url = 'systemGuide!findGuideBoo.action';
		            $.post(url, function(res){
		               if(res)
		                   basics.tab.tabUtils.addTab(i18n.label_systemGuide_title,'../pages/common/config/systemGuide/SystemGuide.jsp');
		               else
		                   $('#itsmMainTab').tabs('add',{
		                       title:i18n['companyInformation'],
		                       cache:"true",
		                       href:'../pages/common/security/company.jsp',
		                       closable:false
		                   });
		                   // 直接调用菜单的点击事件, 就不用重新写太多东西
		                   firstMenu && $('#secondMenu_systemSetting_' + firstMenu).click();
		                   secondMenu && $('#thirdMenu_' + secondMenu).click();
		            });
				}
				windowResize.resizeItsmMainTab();
				itsm.cim.includes.includes.loadSelectCIIncludesFile();
				common.security.includes.includes.loadSelectCustomerIncludesFile();
                basics.includes.loadImportCsvIncludesFile();
                common.config.includes.includes.loadCategoryIncludesFile();
				common.security.includes.includes.loadSelectUserIncludesFile();
				common.jbpm.includes.includes.loadFlowCommonWinIncludesFile();
				common.security.includes.includes.loadAclWinIncludesFile();
				//刷新按钮加载标示
				load_shuaxin_flag=true;
				
			},0);
			
			$(document).ajaxError(function(e, xhr, settings, exception) {
				//隐藏进程
				endProcess();
				endLoading();
				if(xhr.status==606){
						msgAlert(i18n['common_systemTimeout'],'info');
						setTimeout(function(){
							location.reload();
						},2000);
				}
				
				if(xhr.status==403){
					msgAlert(i18n['error403']+'<a href="'+i18n['system_error_contact_url']+'"  target="_blank">'+i18n['contactUs']+'</a>','info');
				};
				if(xhr.status==404){
					msgAlert(i18n['error404']+'<a href="'+i18n['system_error_contact_url']+'"  target="_blank">'+i18n['contactUs']+'</a>','info');
					//location.reload();//404也刷新页面
				};
				if(xhr.status==500){
					$('#systemErrorInfo').html('');
					var msg=xhr.responseText;
					if(msg!=null && msg.indexOf("org.hibernate.exception.DataException")!=-1){
						msgAlert(i18n['error_data_dataToLong'],'info');
						return;
					}
					if(msg!=null && msg.indexOf('ApplicationException:')!=-1){
						msg=msg.substring(msg.indexOf('ApplicationException:')+22,msg.indexOf("\n"));
						var errorContent='<table width="100%">'+
						'<tr><td rowspan="2"><img src="../images/icons/messager_info.gif" /></td><td style="padding:5px"><br/>{errorMsg}</td></tr>'+
						'<tr><td  align="right"><a id="showHideErrorMsg" style="">[{showHiddBut}]</a></td></tr>'+
						'<tr><td colspan="2"><div style="text-align:left"><span id="errorMsg" style="width: autopx;height:autopx;display:none;">{errorInfo}<span></div></td></tr>'+
						'</table>'
						$('#systemErrorInfo').html(errorContent.replace('{errorMsg}',i18n[msg]).replace('{showHiddBut}',i18n['lable_showHide_errorMsg']).replace('{errorInfo}',xhr.responseText))
						$('#showHideErrorMsg').click(function(){$('#errorMsg').toggle()});
						windows('systemErrorWin',{width:400,height:200});
						if(msg=="ERROR_SMS_ACCOUNT"){
							basics.tab.tabUtils.addTab(junp_company,'../pages/common/security/company.jsp');
						}
						return;
					}else{
						var errorMsg=i18n['error500']+'<a href="'+i18n['system_error_contact_url']+'"  target="_blank">'+i18n['contactUs']+'</a>';
						if(msg.indexOf('UNIQUE')!=-1 ||msg.indexOf('Duplicate entry')){
							errorMsg=i18n.tip_Duplicate_data_error;
						}
						var error500Content='<table width="100%">'+
						'<tr><td rowspan="2"><img src="../images/icons/messager_info.gif" /></td><td style="padding:5px"><br/>{errorMsg}</td></tr>'+
						'<tr><td  align="right"><a id="showHideErrorMsg500" style="">[{showHiddBut}]</a></td></tr>'+
						'<tr><td colspan="2"><div style="text-align:left"><span id="errorMsg500" style="width: autopx;height:autopx;display:none;">{errorInfo}<span></div></td></tr>'+
						'</table>'
						$('#systemErrorInfo').html(error500Content.replace('{errorMsg}',errorMsg).replace('{showHiddBut}',i18n['lable_showHide_errorMsg']).replace('{errorInfo}',msg))
						$('#showHideErrorMsg500').click(function(){$('#errorMsg500').toggle()});
						windows('systemErrorWin',{width:400,height:200});
						return;
					}
				};
			});
			var ckLang=lang==="zh_CN"?"zh-cn":lang.toLowerCase();
			CKEDITOR.lang.load(ckLang, ckLang, function(){});//加载ckeditor的国际化
		}	
	}
 }();
 $(document).ready(settingPage.init);
