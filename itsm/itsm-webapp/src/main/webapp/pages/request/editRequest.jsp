<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../language.jsp" %>

<script>
startProcess();
var eno='${param.eno}';
$('#addRequestActivity_requestNo').val(eno);
var title_edit_request_attr='<fmt:message key="common.attachment" />';
var request_edit_eavAttributet='<fmt:message key="config.extendedInfo"/>';
var companyNoReqEdit=companyNo;
var editRequestUser="0";
var defaultFormId='edit';
var belongsClient="0";
</script>
<%
if(session.getAttribute("loginUserName")==null){
	out.print("<script>window.location='login.jsp';</script>");
}else{

    WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
	IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
	if(session.getAttribute("loginUserName")!=null){
		Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
		  "/pages/user!find.action"
		  });
		request.setAttribute("resAuth",resAuth);
	}
}
%>
<sec:authorize url="/pages/user!find.action">
	<script>editRequestUser="1";</script>
</sec:authorize>
<sec:authorize url="/pages/itsopUser!findITSOPUserPager.action">
	<script>belongsClient="1";</script>
</sec:authorize>
<form id ="editRequest_from" event="itsm.request.editRequest.saveRequestEdit">
 <div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> 编辑请求</h2>
            </div>
            <div class="box-content" id="">
            	<div class="row">
   				 	<div class="box col-md-12">
						<button class="btn btn-default btn-xs" ><i class="glyphicon glyphicon-floppy-disk"></i> <fmt:message key="common.submit" /></button>
						<a class="btn btn-default btn-xs" onclick="basics.index.back()"><i class=" glyphicon glyphicon-share-alt"/> <fmt:message key="common.returnToList" /></a>
					</div>
				</div>
			 <div class="row">
				<div class="box col-md-12">
	           		 <div class="box-content" id="edit_basicInfo_field">
						
						<input id="requestEdit_formId" type="hidden" name ="requestDTO.formId" />
						<input id="requestEdit_eno" type="hidden" name="requestDTO.eno" />
						<input id="requestEdit_pid" type="hidden" name="requestDTO.pid" />
						<input id="requestEdit_requestCode" type="hidden" name="requestDTO.requestCode" />
						<input id="requestEdit_isNewForm" type="hidden" name="requestDTO.isNewForm" />
						<input type="hidden" id="requestEdit_status" name="requestDTO.statusNo" />
						<input type="hidden" name="requestDTO.creator" id="attachmentCreator" value="${loginUserName}"  />
						<input type="hidden" name="requestDTO.actionName" value="<fmt:message key="common.edit" />"  />
						<input type="hidden" name="requestDTO.attachmentStr" id="edit_request_attachmentStr"/>
						<input type="hidden" name="requestDTO.processKey" id="editRequest_processKey" value="">
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.belongs.client"/><span class="required">*</span></label>
							<input type="hidden" value="${sessionScope.companyNo}" id="request_companyNo" name="requestDTO.companyNo" required='true'>
							<input name="requestDTO.companyName" id="request_companyName" value="${sessionScope.companyName}" class="form-control" type="text" required='true'>
							<sec:authorize url="/pages/itsopUser!findITSOPUserPager.action"><a title='<fmt:message key="common.select"/>' onclick="itsm.request.requestCommon.chooseCompany('#request_companyNo','#request_companyName',this)" ><span class="glyphicon glyphicon-user form-control-feedback choose" ></span></a></sec:authorize>
						</div>
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.request.requestUser"/><span class="required">*</span></label>
							<input type="hidden" value="${sessionScope.userId}" id="request_userId" name="requestDTO.createdByNo"  required='true'>
							<input name="requestDTO.createdByName" id="request_userName" value="${loginUserName}" class="form-control" type="text" required='true'>
							<sec:authorize url="/pages/user!find.action"><a title='<fmt:message key="common.select"/>' onclick="itsm.request.requestCommon.selectRequestCreator('#request_userName','#request_userId','#request_companyNo',this)" ><span class="glyphicon glyphicon-user form-control-feedback choose" ></span></a></sec:authorize>
						</div>
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="setting.requestCategory"/><span class="required">*</span></label>
							<input type="hidden" value="" id="request_categoryNo" name="requestDTO.requestCategoryNo" required='true'>
							<input name="requestDTO.requestCategoryName"  required='true' onclick="itsm.request.requestCommon.selectRequestCategory('#request_categoryName','#request_categoryNo','#request_etitle','#request_edesc',this)" id="request_categoryName" value="" class="form-control" type="text">
						</div>
						<div class="form-group col-md-6 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="common.title"/><span class="required">*</span></label>
							<input name="requestDTO.etitle" id="request_etitle" value=""  required='true' class="form-control" type="text">
						</div>
						<div class="form-group col-md-12 ui-sortable-handle">
							<label class="form-control-label"><fmt:message key="label.common.desc"/><span class="required">*</span></label>
							<textarea class="form-control" required="true" name="requestDTO.edesc" id="editrequest_edesc" title='<fmt:message key="label.common.desc" />' style="height: 53px;"></textarea>
						</div>
						<div id="edit_formField">
						
						</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div></form>
<div class="row">
   <div class="box col-md-12">
   	    <div class="box-inner" >	
			<div class="box-content" >
   		<ul class="nav nav-tabs" id="editreqest_tab">
            <%-- <sec:authorize url="request_manage_interfixRequestAndKnowledge"> <li class="active"><a href="#related_request_and_knowledge"><fmt:message key="related_request_and_knowledge" /></a></li></sec:authorize> --%>
             <li><a href="#attachment"><fmt:message key="common.attachment" /></a></li>
         </ul>
				<%-- <c:if test="${cimHave eq true}">
				<div title='<fmt:message key="label.request.withAssets" />'>
				<div class="hisdiv">
						<form id="edit_request_relatedCIShow">
						<table class="histable" width="100%" cellspacing="1">
						
							<thead>
							<tr>
								<td colspan="5" style="text-align:left">
									<a id="Request_edit_ref_ci_btn" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
								</td>
							</tr>
							<tr height="20px">
								<th align="center"><fmt:message key="lable.ci.assetNo" /></th>
								<th align="center"><fmt:message key="label.name"/></th>
								<th align="center"><fmt:message key="common.category" /></th>
								<th align="center"><fmt:message key="common.state" /></th>
								<th align="center"><fmt:message key="ci.operateItems" /></th>
							</tr>
							</thead>
							<tbody></tbody>
						</table>
						</form>
					</div>
				</div>
				</c:if> --%>
				<%-- <div title="<fmt:message key="config.extendedInfo"/>" style="overflow:hidden;">
				<form id="request_edit_eavAttributet_form">
					<div class="hisdiv" id="request_edit_eavAttributet">
						<table style="width:100%" class="histable" cellspacing="1">
							<tbody></tbody>
	             			</table>
	               	</div>
	               	</form>
				</div> --%>
				<!-- 扩展信息 end -->
		 <div id="myTabContent" class="tab-content">
				<div class="tab-pane" id="attachment">
					<table width="100%" cellspacing="1" class="lineTable">
							<tr>
								<td>
									<div style="padding-top:0px">
									  <!-- <div id="show_edit_request_attachment"></div> -->
									  <div class="hisdiv" id="show_edit_request_attachment">
										 <table style="width:100%" class="histable" cellspacing="1" id="show_edit_request_attachment_table">
											 <thead>
												 <tr>
													 <th><fmt:message key="common.id" /></th>
													 <th><fmt:message key="label.problem.attachmentName" /></th>
													 <th><fmt:message key="label.problem.attachmentUrl" /></th>
													 <th><fmt:message key="label.sla.operation" /></th>
												 </tr>
											 </thead>
											 <tbody></tbody>
					             		  </table>
					               	  </div>
					               	  
						              <div id="show_edit_request_attachment_success" style="line-height:25px;color:#555;display: none"></div>
						            </div>
								</td>
							</tr>
					</table>
					<div class="form-group" id="edit_request_attachment_div" >
	                    <input id="edit_request_file" type="file" name="filedata" multiple class="file" data-overwrite-initial="false" data-min-file-count="1">
	                </div>
					<div class="form-group">
                         <a href="javascript:wstuo.tools.chooseAttachment.showAttachmentGrid('request','edit')" class="btn btn-default btn-xs" icon="icon-selectOldFile" id="chooseAtt">
                             <fmt:message key="common.attachmentchoose" />
                         </a>
                     </div>
					<%-- <form id="uploadForm_RequestEdit" method="post" enctype="multipart/form-data">
					  <table border="0" cellspacing="1" class="fu_list">
			              <tr height="40px">
				        	<td colspan="2">
				        	<table border="0" cellspacing="0"><tr><td >
					       			<a href="javascript:void(0);" class="files" id="idFile_RequestEdit"></a>
					       		</td><td>
							        <a class="easyui-linkbutton" style="float:left" plain="true"  icon="icon-ok" id="idBtnupload_RequestEdit"><fmt:message key="label.startUpload" /></a>
							         &nbsp;&nbsp;&nbsp;
									<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_RequestEdit"><fmt:message key="label.allcancel" /></a>
									 &nbsp;&nbsp;&nbsp;
									<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('requestDetail','edit')" class="easyui-linkbutton" icon="icon-ok" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
								</td></tr></table>
							</td>
				      		</tr>
				          <tbody id="idFileList_RequestEdit">
				          </tbody>
						</table>
					</form> --%>
				</div>
				<%-- <div title='<fmt:message key="label.ci.ciServiceDir" />'>
							<form>

								<div class="hisdiv">
									<table class="histable" id="edit_request_serviceDirectory" style="width:100%" cellspacing="1">
									<thead>
									<tr>
										<td colspan="3" style="text-align:left">
											<a id="edit_request_service_edit" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
										</td>
									</tr>
									<tr height="20px">
										<th align="center"><fmt:message key="common.id"/></th>
										<th align="center"><fmt:message key="relevance.service.name"/> </th>
										<th align="center"><fmt:message key="label.service.scores"/> </th>
										<th align="center"><fmt:message key="label.rule.operation"/> </th>
									</tr>
									</thead>
									<!-- 服务目录显示 -->
									<tbody id="edit_request_serviceDirectory_tbody">
										
									</tbody>
									
									</table>
								</div>
							</form>--%>
				</div> 
			</div>
   		</div>
	</div>
</div>
<script src="../js/itsm/request/editRequest.js"></script>
<script>
	$(function(){
	    
	});
</script>