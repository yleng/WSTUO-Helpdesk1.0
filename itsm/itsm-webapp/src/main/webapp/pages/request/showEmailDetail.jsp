<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../language.jsp" %>

<div style="border:#99bbe8 1px solid; margin:3px;padding-top:10px">
	


<div style="border-top:#99bbe8 1px solid;border-bottom:#99bbe8 1px solid;background-color:#e4edfe;margin:0px;padding:10px;font-size:24px;text-align:center">

${emailMessageDto.subject}
<div style="color:#AAA;padding:8px;text-align:left"><fmt:message key="request.email.fromUser" />：<c:out value=" ${emailMessageDto.fromUser}"></c:out></div>

<c:if test="${not empty emailMessageDto.receiveDate}">
<div style="color:#AAA;padding:8px;text-align:left"><fmt:message key="request.email.receiveDate" />：<fmt:formatDate type="date" value="${emailMessageDto.receiveDate}"  pattern="yyyy-MM-dd HH:mm:ss" /></div>
</c:if>

<c:if test="${not empty emailMessageDto.sendDate}">
<div style="color:#AAA;padding:8px;text-align:left"><fmt:message key="request.email.sendDate" />：<fmt:formatDate type="date" value="${emailMessageDto.sendDate}"  pattern="yyyy-MM-dd HH:mm:ss" /></div>
</c:if>

<div style="color:#AAA;padding:8px;text-align:left"><fmt:message key="request.email.toUser" />：<c:out value=" ${emailMessageDto.toUser}"></c:out></div>

<c:if test="${not empty emailMessageDto.bcc}">
<div style="color:#AAA;padding:8px;text-align:left"><fmt:message key="request.email.bcc" />
：${emailMessageDto.bcc}</div>
</c:if>

<c:if test="${not empty emailMessageDto.cc}">
<div style="color:#AAA;padding:8px;text-align:left"><fmt:message key="request.email.cc" />
：${emailMessageDto.cc}</div>
</c:if>
</div>

<div style="line-height:20px;padding:20px">

${emailMessageDto.content}

</div>
<c:if test="${not empty emailMessageDto.affachment}">

<div style="margin:10px;border:#FFD2D2 1px dashed;padding:5px;line-height:22px">
<div style="font-size:14px;font-weight:bold;font-style:italic;color:#FF2D2D"><fmt:message key="common.attachment" />：</div>
<c:forEach items="${emailMessageDto.affachment}" varStatus="i" var="attachment">

${i.count}、<a target="_blank" href="attachment!download.action?downloadAttachmentId=${attachment.aid}">${attachment.attachmentName}</a><br/>

</c:forEach>

</div>

</c:if>

</div>
<style>
<!--
a{
	color:#333;	
	text-decoration:none;
}

a:hover{
	color:#ff0000;	
	text-decoration:none;
}

a:visited{
	color:#555;	
	/*text-decoration:none;*/
}

a:link{
	color:#333;
	text-decoration:none;
}
-->
</style>