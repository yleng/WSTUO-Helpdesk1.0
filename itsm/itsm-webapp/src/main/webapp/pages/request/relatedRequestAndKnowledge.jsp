<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../language.jsp" %>

<script src="../scripts/itsm/request/relatedRequestAndKnowledge.js"></script>


<div class="loading" id="relatedRequestAndKnowledge_loading"><img src="../images/icons/loading.gif" /></div>

<div class="content" id="relatedRequestAndKnowledge_content">
	<table id="findRelatedKnowledgeGrid"></table>
	<div id="findRelatedKnowledgePager"></div>
	
	<table id="findRelatedRequestGrid"></table>
	<div id="findRelatedRequestPager"></div>
</div>