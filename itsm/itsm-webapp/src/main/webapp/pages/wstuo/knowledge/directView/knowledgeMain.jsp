<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<fmt:setLocale value="${empty cookie['language'].value?defaultLanguage:cookie['language'].value}"/>
<fmt:setBundle basename="i18n.itsmbest"/>
<c:set var="lang" value="${empty cookie['language'].value?defaultLanguage:cookie['language'].value}" />
<c:set var="currentTime" value="<%=new java.util.Date().getTime()%>" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="label.knowledgeCore"/></title>

	<!-- 1.1.2 -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/scripts/jquery/easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/scripts/jquery/easyui/themes/default/easyui.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/css/cupertino/jquery-ui-1.8.2.custom.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jqgrid/css/jqgrid.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jm-calendar/css/jm-calendar.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/comm.css">
	<!-- 1.4.2 -->
	<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-1.4.2.min.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/i18n/i18n_${lang}.js"></script>
	<!-- 1.1.2 -->
	<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/locale/easyui-lang-${lang}.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/fullcalendar-1.5/fullcalendar/fullcalendar.css" />
	
	<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/jquery.easyui.min.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/jquery/jm-calendar/js/jm-calendar.js?random=${currentTime}"></script>
	
	<script src="${pageContext.request.contextPath}/scripts/jquery/jqgrid/js/jqGrid.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/jquery.jstree.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/rewritejstree.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.query-2.1.7.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-json.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/core/import.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/core/package.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/basics/jqgrid_common.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/basics/jquery_common.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/basics/easyui_common.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/lib/jquery.hotkeys.js"></script>
	<script type="text/javascript">var kn=1;</script>
	<script src="${pageContext.request.contextPath}/scripts/common/config/serviceDirectory/serviceDirectoryUtils.js"></script>
	
	<script src="${pageContext.request.contextPath}/scripts/common/knowledge/directKnowledges.js"></script>
<!--   <script src="resources/directKnowledge.js"></script> -->
</head>
<body class="easyui-layout">
<div region="north" split="true" class="header" style="overflow-y:hidden">
<table width="100%">
	<tr>
		<td>
			<img src="resources/itiad.png" border="0" style="width:120;height:30px;cursor:pointer" id="index_logo"/>
		</td>
		<td><span style="font-size: 20px;"><b><fmt:message key="label.knowledgeCore" /></b></span></td>
		<td align="right" valign="bottom">
		</td>
	</tr>
</table>
	
</div>

<!-- 知识库分类树 -->
<div region="west" id="leftMenu" split="true" title='知识类型' style="width:250px;padding:2px;">
	<div id="knowledgeCategoryTree" style="overflow: auto;height: 100%;"></div>
</div>
	
 
<!-- 内容 -->
<div region="center" id="regCenter">
   <div id="itsmMainTab" class="easyui-tabs" fit="true" border="false">
	   <div title="知识列表" style="padding:5px" id="knowledgeMain">
	  		<table id="knowledgeGrid" width="100%"></table>
			<div id="knowledgeGridPager"></div>
			<div id="knowledgeGridToolbar" style="display:none">
			&nbsp;&nbsp;&nbsp;&nbsp;
			<fmt:message key="knowledge.label.knowledgeTitle" />&nbsp;:&nbsp;
			<input id="search_knowTitle" name="knowledgeQueryDto.title" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			<fmt:message key="common.content" />&nbsp;:&nbsp;
			<input id="search_kncontent" name="knowledgeQueryDto.content" />
			<a class="easyui-linkbutton" plain="true" icon="icon-search" onClick="common.knowledge.directKnowledges.search_konwledge()"><fmt:message key="common.search" /></a>
			<a class="easyui-linkbutton" plain="true" onClick="common.knowledge.directKnowledges.search_konwledge_openwindow()"><fmt:message key="label.advancedSearch" /></a>
			</div>
	   </div>
   </div>
</div>
<!-- 搜索知识库 -->
<div id="knowledgeSearchDiv" class="WSTUO-dialog" title="<fmt:message key="label.searchKnowledge" />"
	style="width: 450px; height: auto">
<form>
<div class="lineTableBgDiv">
<table style="width: 100%" class="lineTable" cellspacing="1">
	<tr>
		<td><fmt:message key="knowledge.label.knowledgeSort" /></td>
		<td><input name="knowledgeQueryDto.category"
			id="search_knCategory" class="choose" style="width:80%" readonly />
			<a class="ka" onclick="javascript:$('#search_knCategory').val('');"><fmt:message key="label.request.clear" /></a>
			</td>
	</tr>
	<tr>
		<td><fmt:message key="knowledge.label.knowledgeTitle" /></td>
		<td><input id="search_knTitle" name="knowledgeQueryDto.title"
			style="width: 96%" /></td>
	</tr>
	<tr>
		<td><fmt:message key="tool.affiche.creator" /></td>
		<td><input id="search_creator" style="width: 96%" name="knowledgeQueryDto.creatorFullName"/>
		<%-- <a id="search_creator_select" class="ka"><fmt:message key="common.select" /></a> --%></td>
	</tr>
	
		<tr>
		<td><fmt:message key="label.knowledge.relatedService" /></td>
		<td>
			<input id="search_categoryNames" style="width: 96%" name="knowledgeQueryDto.categoryNames"/>
		</td>
	</tr>
	
	<tr>
		<td><fmt:message key="common.attachment" /></td>
		<td><input id="search_attachmentContent" name="knowledgeQueryDto.attachmentContent" style="width: 96%" /></td>
	</tr>
	<tr>
		<td><fmt:message key="common.createTime" /></td>
		<td><input id="know_search_startTime"
			name="knowledgeQueryDto.startTime" class="input"
			style="width: 80px" />&nbsp;<fmt:message key="setting.label.to" />
		&nbsp;<input id="know_search_endTime" name="knowledgeQueryDto.endTime"
		 style="width: 80px" /></td>
	</tr>

	<tr>
		<td colspan="2">
		<input type="hidden" id="knowledgeQueryDto_keyWord" name="knowledgeQueryDto.keyWord" /> 
		<a id="knowledge_search_ok" onclick="common.knowledge.directKnowledges.doSearchKnowledge()" class="easyui-linkbutton" icon="icon-search"><fmt:message key="common.search" /></a>
		</td>
	</tr>
</table>
</div>
</form>
</div>
<!-- 知识分类 -->
<div id="knowledge_category_select_window" class="WSTUO-dialog" title="<fmt:message key="knowledge.knowledgeCategory" />" style="max-height:400px;overflow:auto;width:240px;padding:3px;min-height:200px;max-height:400">
	<div id="knowledge_category_select_tree"></div>
</div>
<!-- 底部 -->
<div region="south" split="true" style="height:40px; padding:8px; text-align:center">
Powered by WSTUO of GuoYuSoft,Ltd.
</div>
<style>
.ka{
	color:#474747;
	text-decoration:none;
	cursor:pointer;
}
</style>

</body>
</html>