<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<script >
var _processDefinitionId = '${param.processDefinitionId}';
var parentDivPrefix = "#show_"+_processDefinitionId+" ";
</script>
<script src="../js/wstuo/jbpmMge/processInstances.js?random=<%=new java.util.Date().getTime()%>"></script>

<div align="left" id="show_${param.processDefinitionId}">
	<div style="overflow:hidden;padding:4px;padding-right:6px">
		<table id="processInstancesGrids" width="100%"></table>
		<div id="processInstancesPager"></div>
	</div>
</div>
