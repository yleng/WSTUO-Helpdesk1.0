<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ include file="../../language.jsp" %>   
<script type="text/javascript" src="${pageContext.request.contextPath}/js/basics/htmlformat.js"/>
<script type="text/javascript">
$(document).ready(function(){
	do_js_beautify("emailEdit_templateContent");
	wstuo.noticeRule.noticeGrid.loadVariable('#variableValue','${noticeRuleDTO.module}');
	$('#smsEdit_templateContent,#imEdit_templateContent,#pushEdit_templateContent,#titleEdit_templateContent,#emailEdit_templateContent').focus(function(obj){
		$("#eventObjEdit").val(obj.target.id);
	});
});
wstuo.noticeRule.noticeGrid.loadVariable("#variableValueEdit","${noticeRuleDTO.module}");
</script>
 <div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><fmt:message key="lable.notice.editNoticeRule"/></h2>
            </div>
            <div class="box-content" style="height: 1000px;">
            	<a class="btn btn-primary btn-sm" id="editNotice" href="javascript:wstuo.noticeRule.noticeGrid.editNotice()" ><fmt:message key="common.save" /></a>
				<br/><div id="editNotice_layout" >
				<!-- 编辑通知規則窗口 -->
					<div id="editNotice_center" >
					<div id="index_edit_notice_window" style="width:auto;height:auto">
						<div style="float: left;width: 75%;">
						<div id="noticesMethod" >
						<b style="margin-top: 8px;margin-left: 10px;"><fmt:message key="common.basicInfo" /></b>	
						<hr style="margin-top: 2px;margin-bottom: 5px;width: 95%;margin-left: 2px;">	
			
						<form id="editNoticesMethodForm">
							<table style="width:100%;" cellspacing="1" >
							<tr style="display:none">
								<td style="width: 15%"></td>
								<td style="width: 60%;">
									<input type="text" class="form-control" id="editMethodNoticeId" name="noticeRuleDTO.noticeRuleId" required="true" style="width: 90%" value="${noticeRuleDTO.noticeRuleId }"/>
								</td>						
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 50px;"><fmt:message key="lable.notice.name" /></td>
								<td style="width: 60%;">
									<input type="text" class="form-control" id="editMethodNoticeName" name="noticeRuleDTO.noticeRuleName" required="true" style="width: 90%" value="${noticeRuleDTO.noticeRuleName }"/>
								</td>						
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 50px;"><br/><fmt:message key="notice.enableThisRule" /></td>
								<td style="width: 60%;"><br/>
									<label class="radio-inline" >
										<input type="radio" name="noticeRuleDTO.useStatus" value="true" <c:if test="${noticeRuleDTO.useStatus}">checked="checked"</c:if> /><fmt:message key="common.enable" />
									</label>&nbsp;&nbsp;
									<label class="radio-inline" >
										<input type="radio" name="noticeRuleDTO.useStatus" value="false" <c:if test="${noticeRuleDTO.useStatus == false}">checked="checked"</c:if> /><fmt:message key="common.disable" />
									</label>							
								</td>
								
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 50px;"><br/><fmt:message key="notice.module" /></td>
								<td style="width: 60%;"><br/>
									<input type="hidden" name="noticeRuleDTO.module" value="${noticeRuleDTO.module }" >
									<c:if test="${noticeRuleDTO.module eq 'task'}"><input style="width: 70%" class="form-control" type="text" disabled="disabled" value="<fmt:message key="lable.task.manage" />"/></c:if>
									<c:if test="${noticeRuleDTO.module eq 'other'}"><input style="width: 70%" class="form-control" type="text" disabled="disabled" value="<fmt:message key="label.returnVisit.other" />"/></c:if>
									<c:if test="${noticeRuleDTO.module eq 'request'}"><input style="width: 70%" class="form-control" type="text" disabled="disabled" value="<fmt:message key="itsm.request" />"/></c:if>
									<c:if test="${noticeRuleDTO.module eq 'problem'}"><input style="width: 70%" class="form-control" type="text" disabled="disabled" value="<fmt:message key="title.mainTab.problem.manage" />"/></c:if>
									<c:if test="${noticeRuleDTO.module eq 'change'}"><input style="width: 70%" class="form-control" type="text" disabled="disabled" value="<fmt:message key="title.mainTab.change.manage" />"/></c:if>
									<c:if test="${noticeRuleDTO.module eq 'release'}"><input style="width: 70%" class="form-control" type="text" disabled="disabled" value="<fmt:message key="title.mainTab.release.manage" />"/></c:if>
									<c:if test="${noticeRuleDTO.module eq 'cim'}"><input style="width: 70%" class="form-control" type="text" disabled="disabled" value="<fmt:message key="title.mainTab.cim.manage" />"/></c:if>
								</td>
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 50px;"><br/><fmt:message key="notice.noticeTarget" /></td>
								<td style="width: 60%;"><br/>
									<c:if test="${noticeRuleDTO.module eq 'request' || noticeRuleDTO.module eq 'problem' || noticeRuleDTO.module eq 'change' || noticeRuleDTO.module eq 'release' }">							
									<label class="checkbox-inline">
										<input type="checkbox" name="noticeRuleTypeEdit" value="requester" <c:if test="${fn:contains(noticeRuleDTO.noticeRuleType,'requester')}">checked="checked"</c:if> /><fmt:message key="notice.requesterOrReporter" />
									</label>							
									</c:if>
									<c:if test="${noticeRuleDTO.module eq 'request' || noticeRuleDTO.module eq 'problem' || noticeRuleDTO.module eq 'change' || noticeRuleDTO.module eq 'release' || noticeRuleDTO.module eq 'other'}">
									<label class="checkbox-inline">
										<input type="checkbox" name="noticeRuleTypeEdit" value="technician" <c:if test="${fn:contains(noticeRuleDTO.noticeRuleType,'technician')}">checked="checked"</c:if> /><fmt:message key="notice.assignTechnician" />
									</label>
									</c:if>
									<c:if test="${noticeRuleDTO.module eq 'request' || noticeRuleDTO.module eq 'problem' || noticeRuleDTO.module eq 'change' }">
									<label class="checkbox-inline">	
										<input type="checkbox" name="noticeRuleTypeEdit" value="technicalGroup" <c:if test="${fn:contains(noticeRuleDTO.noticeRuleType,'technicalGroup')}">checked="checked"</c:if> /><fmt:message key="notice.assignTechnicalGroup" />
									</label>
									</c:if>
									<c:if test="${noticeRuleDTO.module eq 'request' || noticeRuleDTO.module eq 'task'}">
									<label class="checkbox-inline">	
										<input type="checkbox" name=noticeRuleTypeEdit value="owner" <c:if test="${fn:contains(noticeRuleDTO.noticeRuleType,'owner')}">checked="checked"</c:if> /><fmt:message key="task.ownerName" />
									</label>
									</c:if>
									<c:if test="${noticeRuleDTO.module eq 'request' || noticeRuleDTO.module eq 'problem' || noticeRuleDTO.module eq 'change'}">
									<label class="checkbox-inline">	
										<input type="checkbox" name="noticeRuleTypeEdit" value="taskGroupAndTechnician" <c:if test="${fn:contains(noticeRuleDTO.noticeRuleType,'taskGroupAndTechnician')}">checked="checked"</c:if> /><fmt:message key="notice.taskAssignObjectNotice" />
									</label>
									</c:if>
									<c:if test="${noticeRuleDTO.module eq 'request' || noticeRuleDTO.module eq 'problem' || noticeRuleDTO.module eq 'change'}">
									<label class="checkbox-inline">	
										<input type="checkbox" name="noticeRuleTypeEdit" value="taskTechnicalGroupLeader" <c:if test="${fn:contains(noticeRuleDTO.noticeRuleType,'taskTechnicalGroupLeader')}">checked="checked"</c:if> /><fmt:message key="technologyGroupLeader" />
									</label>
									</c:if>
									<c:if test="${noticeRuleDTO.module eq 'cim'}">
									<label class="checkbox-inline">	
										<input type="checkbox" name="noticeRuleTypeEdit" value="ciOwner" <c:if test="${fn:contains(noticeRuleDTO.noticeRuleType,'ciOwner')}">checked="checked"</c:if> /><fmt:message key="ci.owner" />
									</label>
									</c:if>
									<c:if test="${noticeRuleDTO.module eq 'cim'}">
									<label class="checkbox-inline">	
										<input type="checkbox" name="noticeRuleTypeEdit" value="ciUse" <c:if test="${fn:contains(noticeRuleDTO.noticeRuleType,'ciUse')}">checked="checked"</c:if> /><fmt:message key="ci.use" />
									</label>
									</c:if>
								</td>
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 50px;"><br/><fmt:message key="sla.request.NotificationWay" /></td>
								<td style="width: 60%;"><br/>
									<label class="checkbox-inline">	
										<input type="checkbox" name="noticeRuleDTO.mailNotice" value="true" <c:if test="${noticeRuleDTO.mailNotice}">checked="checked"</c:if> /><fmt:message key="lable.notice.emialNoticeMethod" />
									</label>
									<label class="checkbox-inline">		
										<input type="checkbox" name="noticeRuleDTO.smsNotice" value="true" <c:if test="${noticeRuleDTO.smsNotice}">checked="checked"</c:if> /><fmt:message key="lable.notice.smsNotice" />
									</label>
									<label class="checkbox-inline">		
										<input type="checkbox" name="noticeRuleDTO.imNotice" value="true" <c:if test="${noticeRuleDTO.imNotice}">checked="checked"</c:if> /><fmt:message key="notice.IMnotice" />
									</label>
									<%-- <label class="checkbox-inline">		
										<input type="checkbox" name="noticeRuleDTO.pushNotice" value="true" <c:if test="${noticeRuleDTO.pushNotice}">checked="checked"</c:if> /><fmt:message key="notice.pushNotice"/>
									</label> --%>
								</td>
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 50px;"><br/><fmt:message key="lable.notice.noticeTechnician" /></td>
								<td style="width: 60%;"><br/>
									<input type="text" id="technicianMethodInput" style="width: 90%;float: left;" value="${noticeRuleDTO.technician}" class="form-control"/>
									
									<h5><a class="glyphicon glyphicon-user" style="float: left;padding-left: 8px;" href="javascript:wstuo.noticeRule.noticeGrid.selectNoticeUser('#technicianMethodInput')">
									</a></h5>
								</td>
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 50px;"><br/><fmt:message key="lable.notice.noticeAssignMial" /></td>
								<td style="width: 60%;"><br/>
									<input type="text" id="emailMethodInput" style="width: 90%;float: left;" value="${noticeRuleDTO.emailAddress}" class="form-control"/>							
									<h5><a class="glyphicon glyphicon-user" style="float: left;padding-left: 8px;"  href="javascript:wstuo.noticeRule.noticeGrid.selectNoticeUser('#emailMethodInput','email')" >
									</a></h5>
								</td>
							</tr>
						</table>
						
			</form>
			</div>
			<div style="width:100%;">
				 <b style="margin-left: 10px;"><fmt:message key="notice.noticeContentModule" /> </b>		                              
	             <hr style="margin-top: 3px;margin-bottom: 5px;width: 95%;margin-left: 2px;">	
				<form id="editNoticeMailFrom">
					<table style="width:100%;" cellspacing="1">
						<tr>
							<td style="width: 15%;padding-left: 50px;"><fmt:message key="notice.smsModule" /></td> 
							<td style="width: 60%;">
								<input  id="smsEdit_templateContent" name="noticeRuleDTO.smsTemp" style=" width:90%;float: left;" class="form-control" validType="noticeContent" required="true" value="${noticeRuleDTO.smsTemp }" />
								<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true"  id="seeNotice_but" href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('sms','Edit')" title="<fmt:message key="lable.notice.see" />"></a>
								</h5>
							</td>
					
						</tr>
						<tr>
							<td style="width: 15%;padding-left: 50px;"><fmt:message key="notice.IMModule" /></td>
							<td style="width: 60%;"><br/>
								<input  id="imEdit_templateContent" name="noticeRuleDTO.imTemp" style=" width:90%;float: left;" class="form-control" validType="noticeContent" required="true" value="${noticeRuleDTO.imTemp }" />
								<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true"  href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('im','Edit')"  title="<fmt:message key="lable.notice.see" />"></a>
							</h5>
							</td>				
						</tr>
						<%-- <tr>
							<td style="width: 15%;padding-left: 50px;"><fmt:message key="notice.pushNoticeTemp" /></td> 
							<td style="width: 60%;"><br/>
								<input id="pushEdit_templateContent" name="noticeRuleDTO.pushTemp" style=" width:90%;float: left;" class="form-control" validType="noticeContent" required="true" value="${noticeRuleDTO.pushTemp }"/>
								<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true"  href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('push','Edit')" title="<fmt:message key="lable.notice.see" />"></a>
							</h5>
							</td>
						</tr> --%>
					<tr>
					
						<td colspan="2" class="dashboard-list">
							<br/>
							<b style="margin-left: 20px;margin-top: 10px;"><fmt:message key="notice.mailModule" /></b>
                        	<hr style="margin-left: 10px;margin-top: 3px;width: 95%;margin-left: 2px;">
						</td>
					</tr>
						<tr>
							<td style="width: 15%;padding-left: 50px;"><fmt:message key="common.title" /></td>
							<td style="width: 60%;">
								<input name="noticeRuleDTO.emailTitleTemp" id="titleEdit_templateContent" class="form-control" validType="noticeTitle" required="true"  style=" width:90%;float: left;" value="${noticeRuleDTO.emailTitleTemp }"/>
								<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true"  id="seeNotice_but" href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('title','Edit')" title="<fmt:message key="lable.notice.see" />"></a>
							</h5>
							</td>
						</tr>
						<tr>
							<td style="width: 15%;padding-left: 50px;"><fmt:message key="common.content" /></td> 
							<td style="width: 60%;">								
								<br/>
								<div style="width: 90%;float: left;">
								<textarea id="emailEdit_templateContent" name="noticeRuleDTO.emailTemp" style=" width:100%; height: 200px;" >${noticeRuleDTO.emailTemp }</textarea>																					
								</div>
								<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true" id="seeNotice_but" href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('email','Edit')" title="<fmt:message key="lable.notice.see" />">
								</a></h5>													
							</td>
							
						</tr>
					</table>
					<a class="btn btn-primary btn-sm" id="editNotice" href="javascript:wstuo.noticeRule.noticeGrid.editNotice()" ><fmt:message key="common.save" /></a>
				
				</form>
				</div>
			</div>
			<div style="width: 25%;float: left;margin-top: 20px;">
				<table>
					<tr>
						<td>
							<b><fmt:message key="lable.notice.variableValue" /></b>
							<input type="hidden" id="eventObjEdit">
						</td>
					</tr>
					<tr>
						<td>
						<select id="variableValueEdit" multiple="true" style="width:160px;;height:370px" onclick="wstuo.noticeRule.noticeGrid.getValue('Edit')" class="form-control">
						</select>
						<label id="labelValueEdit"></label>
			
					</td>
					</tr>						
				</table>						
			</div>
			</div>
		</div>
	<div id="historyRecord_win" class="WSTUO-dialog" title="<fmt:message key="label.noticeHistoryRecordHtml" />" style="width:auto;height:auto">
		<div style="margin-left: 10px;margin-top: 10px;">
			<span><fmt:message key="label.noticeHistoryRecord" /></span>
		</div>
		<div>
			<textarea id="historyRecord_html" style="width: 400px;height: 200px;"></textarea>
		</div>
	</div>
	<div id="templatePreviewDivEdit" class="WSTUO-dialog" title="<fmt:message key="notice.seeModule" />" style="width:auto;height:auto">
	
	</div>

	</div>
	</div>
        </div>
    </div>
</div>
