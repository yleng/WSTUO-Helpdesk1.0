<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../language.jsp" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>导出管理页面</title>
<script type="text/javascript">
var exportType="${param.exportType}";
var fileName="${param.fileName}";
</script>
<script src="${pageContext.request.contextPath}/js/wstuo/tools/exportManage.js?random=<%=new java.util.Date().getTime()%>"></script>
</head>
<body>
<div id="index_export_manage">
		<div class="row">
			<div class="box col-md-12">
				<div class="box-inner">
					<div class="box-header well" data-original-title="">
						<h2>
							<i class="glyphicon glyphicon-list-alt"></i>&nbsp;导出下载管理
						</h2>
						<div class="box-icon">
						</div>
					</div>

					<div class="box-content ">
					
						<table id="exportInfoGrid"></table>
						<div id="exportInfoePager"></div>
						<div id="exportInfoToolbar" style="display:none">
							&nbsp;&nbsp;<button class="btn btn-default btn-xs"  id="export_delete_but" ><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete"/></button>
							<button class="btn btn-default btn-xs"  onclick="basics.index.back()" ><i class=" glyphicon glyphicon-share-alt"></i><fmt:message key="common.returnToList" /></button>
						</div>
										
					</div>
				</div>
			</div>
		</div>
</div>


<div id="index_export_common_window" class="WSTUO-dialog" title="<fmt:message key="label.export.selectFormat"/>" style="width:400px;height:auto">
	<div class="lineTableBgDiv" >
	<form>
    <table  class="lineTable"  width="100%" cellspacing="1"> 
        <tr>
            <td>csv<input type="radio" name="knowledgeQueryDto.fileFormat" value="csv" value="csv" checked/></td>
            <td>xls<input type="radio" name="knowledgeQueryDto.fileFormat" value="xls" value="xls"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="padding-top:8px;">
                	<a id="index_export_commonClick" class="easyui-linkbutton" icon="icon-ok"><fmt:message key="label.dc.export"/></a>
                </div>
        </td>
        </tr>
    </table>
    </form>
    </div>
</div>
<form action="ci!exportData.action" method="post" id="export_common_form">
	<input type="hidden" id="export_exportFileName" name="categoryName">
</form>
</body>
</html>