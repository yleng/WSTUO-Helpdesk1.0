<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ACL Includes文件</title>
</head>
<body>

<!-- ACL Setting Window Start -->
<div id="index_acl_window" class="WSTUO-dialog" title="<fmt:message key="label.role.permissionAssign" />" style="width:450px;height:auto">

<div>
<fieldset>
    <legend><fmt:message key="label.acl.object.identity" />:</legend>
    <div id="objectIdentity_identifier"></div>
</fieldset>
</div>

<div>
<fieldset>
    <legend><fmt:message key="label.acl.role.group.user" />:</legend>
    <div style="max-height: 160px;overflow: auto;">
    	<table style="width:100%" id="entries_sid_table"><tbody></tbody></table>
    </div>
    <div style="padding-right: 20px;" align="right"><a class="btn btn-default btn-xs" plain="false" onclick="common.security.acl.openAddPermissionWin()"><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></a>
    &nbsp;&nbsp;<a class="btn btn-default btn-xs" plain="false" onclick="common.security.acl.deletePermission()"><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></a></div>
</fieldset>
</div>

<div>
<fieldset>
    <legend><fmt:message key="label.acl.role.group.user.permission" />:</legend>
    <div>
    <form id="permissionSetForm">
    <input type="hidden" id="recipients" name="recipients" />
    <input type="hidden" id="aclclass" name="aclclass" />
    <table>
    	<tr>
    		<th><fmt:message key="label.acl.permission" /></th>
    		<th><fmt:message key="label.acl.allow" /></th>
    		<th><fmt:message key="label.acl.reject" /></th>
    	</tr>
    	<tr>
    		<td><fmt:message key="label.acl.full.control" /></td>
    		<td><input class="allowFlag" onclick="common.security.acl.allControl('allow_a','allow')" name="allowMasks" id="allow_a" value="16" type="checkbox" /></td>
    		<td><input class="rejectFlag" onclick="common.security.acl.allControl('reject_a','reject')" name="rejectMasks" id="reject_a" value="16" type="checkbox" /></td>
    	</tr>
    	<tr>
    		<td><fmt:message key="label.acl.read" /></td>
    		<td><input class="allowFlag" onclick="common.security.acl.unAllControl('r','allow')" name="allowMasks" id="allow_r" value="1" type="checkbox" /></td>
    		<td><input class="rejectFlag" onclick="common.security.acl.unAllControl('r','reject')" name="rejectMasks" id="reject_r" value="1" type="checkbox" /></td>
    	</tr>
    	<tr>
    		<td><fmt:message key="common.update" /></td>
    		<td><input class="allowFlag" onclick="common.security.acl.unAllControl('w','allow')" name="allowMasks" id="allow_w" value="2"  type="checkbox" /></td>
    		<td><input class="rejectFlag" onclick="common.security.acl.unAllControl('w','reject')" name="rejectMasks" id="reject_w" value="2" type="checkbox" /></td>
    	</tr>
    	<tr>
    		<td><fmt:message key="common.delete" /></td>
    		<td><input class="allowFlag" onclick="common.security.acl.unAllControl('d','allow')" name="allowMasks" id="allow_d" value="8" type="checkbox" /></td>
    		<td><input class="rejectFlag" onclick="common.security.acl.unAllControl('d','reject')" name="rejectMasks" id="reject_d" value="8" type="checkbox" /></td>
    	</tr>
    	<tr><td colspan="3"><a class="btn btn-primary btn-sm" plain="false" onclick="common.security.acl.updatePermission()"><fmt:message key="common.save" /></a></td></tr>
    </table>
    </form>
    </div>
</fieldset>
</div>

</div>
<!-- ACL Setting Window End -->

<!-- ACL Setting Window Start -->
<div id="add_acl_window" class="WSTUO-dialog" title="<fmt:message key="label.request.add" />" style="width:450px;height:auto">
	<form id="add_acl_form">
		<input type="hidden" id="add_acl_aclclass" name="aclclass" />
		<div style="max-height: 190px;">
			<fieldset>
		    	<legend><fmt:message key="label.acl.role.group.user" />:</legend>
		    	<div style="max-height: 160px;overflow: auto;">
			    	<table style="width:100%" id="add_sid_table"><tbody></tbody></table>
			    </div>
		    	<div style="padding-right: 20px;" align="right">
		    	<select id="sid_type">
		    		<option value="role"><fmt:message key="title.orgSettings.role" /></option>
		    		<option value="group"><fmt:message key="label.request.group" /></option>
		    		<option value="user"><fmt:message key="label.user" /></option>
		    	</select>
		    	<a class="btn btn-primary btn-sm" plain="false" onclick="common.security.acl.selectSid()"><fmt:message key="common.add" /></a></div>
	    	</fieldset>
	    </div>
	    <div>
			<fieldset>
		    <legend><fmt:message key="label.acl.role.group.user.permission" />:</legend>
			<input type="checkbox" id="add_acl_allow_a" onclick="common.security.acl.addAllControl('add_acl_allow_a')" value="16" name="allowMasks" /><fmt:message key="label.acl.full.control" /><br>
			<input type="checkbox" id="add_acl_allow_r" onclick="common.security.acl.addUnControl('add_acl_allow_r')" value="1" name="allowMasks" /><fmt:message key="label.acl.read" /><br>
			<input type="checkbox" id="add_acl_allow_w" onclick="common.security.acl.addUnControl('add_acl_allow_w')" value="2" name="allowMasks" /><fmt:message key="common.update" /><br>
			<input type="checkbox" id="add_acl_allow_d" onclick="common.security.acl.addUnControl('add_acl_allow_d')" value="8" name="allowMasks" /><fmt:message key="common.delete" /><br>
			</fieldset>
		</div>
	</form>
	<div align="center"><a class="btn btn-primary btn-sm" plain="false" onclick="common.security.acl.addPermission()"><fmt:message key="common.save" /></a></div>
</div>


</body>
</html>