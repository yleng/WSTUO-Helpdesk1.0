<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script type="text/javascript">
$(function(){
	$.post("report!engineerWorkloadReport.action",function(data){
		var trhtml="";
		var html="";
		var requestcount=0;
		var statecount=0;
		$.each(data,function(i,o){
			requestcount=0;
			if(o[2]==null){
				o[2]="无状态";
			}
			if(o[1]==null){
				o[1]="无指派";
			}
			trhtml=trhtml+"<th>"+o[2]+"</th>";
			html=html+"<tr><td>"+o[1]+"</td>";
			
			var adminname=o[1];
			$.each(data,function(i,o){
				if(adminname==o[1]){
					requestcount=requestcount+o[0];
					html=html+"<td>"+o[0]+"</td>";
				}else{
					html=html+"<td>0</td>";
				}
				
			});
			html=html+"<td>"+requestcount+"</td></tr>";
		});
		trhtml=trhtml+"<th>请求统计</th>";
		$("#engineerWorkloadinfo tbody").append(html);
		var html1="";
		html1=html1+"<tr><td>状态统计</td>";
		$("#engineerWorkloadinfo thead tr").append(trhtml);
		var len = $('#engineerWorkloadinfo tbody tr:eq(0) td').length;
        for ( var i = 1; i < len; i++)
        {
            var sum = 0;
            $ ('#engineerWorkloadinfo  tr:gt(0) td:nth-child(' + (i + 1) + ')').each (function (j, dom)
            {
                sum += parseFloat ($ (this).text ());
            });
            html1=html1+"<td>"+sum+"</td>";
        }
		html1=html1+"</tr>";
		$("#engineerWorkloadinfo tbody").append(html1);
	});
});

</script>
<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-list-alt"></i>&nbsp;技术员工作量统计
					</h2>
					<div class="box-icon">
					</div>
				</div>
				<div class="box-content buttons" >
				 <table id="engineerWorkloadinfo" class="table table-striped table-bordered responsive">
                        <thead>
                        <tr>
                            <th>技术员\状态</th>
                        </tr>
                        </thead>
                        <tbody>
                       
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
 </div>