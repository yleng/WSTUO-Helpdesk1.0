<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script type="text/javascript">
$(function(){
	var html="";
	$.post("report!technicianCostTime.action",function(data){
		$.each(data,function(i,o){
			if(o[0]==null){
				o[0]="没指派技术人员";
			}
			if(o[1]==null){
				o[1]="";
			}
			if(o[2]==null){
				o[2]="";
			}
			if(o[3]==null){
				o[3]="";
			}
			if(o[4]==null){
				o[4]="";
			}
			html=html+"<tr><td>"+o[0]+"</td><td>"+o[5]+"</td><td>"+o[1]+"</td><td>"+o[2]+"</td><td>"+o[3]+"</td><td>"+o[4]+"</td></tr>";
		});
		$("#technicianCostTimeinfo tbody").append(html);
	});
	
});
</script>
<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-list-alt"></i>&nbsp;技术员工时统计报表
					</h2>
					<div class="box-icon">
					</div>
				</div>
				<div class="box-content buttons" >
				 <table id="technicianCostTimeinfo" class="table table-striped table-bordered responsive">
                        <thead>
                        <tr>
                            <th>技术员</th>
                            <th>请求数量</th>
                            <th>预计耗时/预计工作量(小时)</th>
                            <th>实际耗时/实际工作量(小时)</th>
                            <th colspan="2" width="30%">时间范围</th>
                        </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
 </div>