<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ include file="../../language.jsp" %>


<script src="${pageContext.request.contextPath}/js/wstuo/sms/smsRecord.js"></script>

<script>

$(document).ready(function(){
	//绑定日期控件
	DatePicker97(['#search_sendTime_start','#search_sendTime_end']);

	$('.loading').hide();
	$('.content').show();
	wstuo.sms.smsRecord.showGrid("#smsRecordGrid");

	$('#search_sender_link').click(function(){
		wstuo.user.userUtil.selectUser('#search_sender','','','loginName',companyNo);
	});
});

</script>

<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" >
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;短信历史记录</h2>
        </div>
		<div id="smsRecord_content" style="padding: 3px;">
			<table id="smsRecordGrid"></table>
			<div id="smsRecordGridPager"></div>
			
			<div id="smsRecordGridToolbar"  style="display: none">
				<div class="panelBar">	
			
					<sec:authorize url="/pages/smsRecord!delete.action">
						<a class="btn btn-default btn-xs" onClick="wstuo.sms.smsRecord.delRecord()" title="<fmt:message key="common.delete"/>"><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></a>
					</sec:authorize>
				
					<a class="btn btn-default btn-xs" onClick="wstuo.sms.smsRecord.search()" title="<fmt:message key="common.search"/>"><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></a>
					
				</div>
			</div>
		</div>
	</div>	

	<!-- 搜索框 -->
	<div id="search_sms_record_window" class="WSTUO-dialog" title='<fmt:message key="common.search"/>' style="width:450px;height:auto;">
		<form>
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td style="width:25%"><fmt:message key="label.sms.receivingMobile"/></td>
					<td style="width:75%"><input name="smsRecordDTO.mobile" class="form-control" style="width: 96%;"></td>
				</tr>			
				<tr>
					<td style="width:25%"><fmt:message key="tool.sms.smsContent"/></td>
					<td style="width:75%"><input name="smsRecordDTO.content" class="form-control" style="width: 96%;"></td>
				</tr>			
				<tr>
					<td style="width:25%"><fmt:message key="label.sms.sender"/></td>
					<td style="width:75%">
						<span style="float: left;">
							<input name="smsRecordDTO.sender" id="search_sender" class="form-control" style="width: 265px;">
						</span>			
						<h5 style="float: left;margin-left: 5px;">
							<a href="javascript:void(0)" id="search_sender_link"><fmt:message key="common.select"/></a>
						</h5>
					</td>
				</tr>
				
				<tr>
					<td style="width:25%"><fmt:message key="label.sms.sendTime"/></td>
					<td style="width:75%">
						<div style="margin-top: 5px;">
						<span style="float: left;">
						<input name="smsRecordDTO.sendTime_start" id="search_sendTime_start" style="width:128px" id="sendTime_start" class="form-control" readonly/>&nbsp;&nbsp;
						</span>	
						<h5 style="float: left;margin-left: 5px;margin-right: 5px;">
						<fmt:message key="tool.affiche.to"/>
						</h5>
						<span style="float: left;">
						<input name="smsRecordDTO.sendTime_end" id="search_sendTime_end" style="width:128px" id="sendTime_end"  class="form-control" validType="DateComparison['search_sendTime_start']" readonly/>
						</span>	
						<h5 style="float: left;margin-left: 3px;">
						<a href="#" onclick="cleanIdValue('search_sendTime_start','search_sendTime_end')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-trash"></i></a>
						</h5>
						</div>
					</td>
				</tr>
			</table>
			<div style="text-align:center;margin-top: 8px;margin-bottom: 8px;">	
				<span style="margin-right: 20px;"class="btn btn-primary btn-sm" onclick="wstuo.sms.smsRecord.doSearch()"><fmt:message key="common.search" /></span>
				<span style="margin-right: 10px;" onclick="$('#search_sms_record_window input').val('');" class="btn btn-primary btn-sm"><fmt:message key="i18n.reset"/></span>			
			</div>
		</form>
	</div>
	<!-- 短信详情 -->
	<div id="sms_record_detail_window" class="WSTUO-dialog" title='<fmt:message key="title.sms.detail"/>' style="width:450px;height:230px;">
		<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
				<td style="width:30%"><fmt:message key="label.sms.receivingMobile"/></td>
				<td style="width:70%"><div id="sms_detail_mobile"></div></td>
			</tr>
			<tr>
				<td><fmt:message key="tool.sms.smsContent"/></td>
				<td><div id="sms_detail_content"></div></td>
			</tr>
			<tr>
				<td><fmt:message key="label.sms.sender"/></td>
				<td><div id="sms_detail_sender"></div></td>
			</tr>
			<tr>
				<td><fmt:message key="label.sms.sendTime"/></td>
				<td><div id="sms_detail_sendTime"></div></td>
			</tr>	
	
		</table>
	</div>
</div>
