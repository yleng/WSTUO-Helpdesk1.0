<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- <fmt:setBundle basename="i18n.itsmbest"/> --%>
<%@ include file="../../language.jsp" %>
<div class="field_options" attrtype="String" label='<fmt:message key="label.belongs.client"/>' attrno="request_companyName" attrname="requestDTO.companyName"
	required="false" attrdatadictionary="" attrhiddenid="request_companyNo" attrhiddenname="requestDTO.companyNo" 
	attrhiddenval="" attreventtype="onclick" attrEventVal="itsm.request.requestCommon.chooseCompany('#request_companyNo','#request_companyName',this)"
	value="" attrImgSrc="../images/icons/userpicker_disabled.gif"  attrI18n="common_select">
	
	<div class="label">
		<div class="fieldName" title="<fmt:message key="label.belongs.client"/>"><fmt:message key="label.belongs.client"/></div><div class="required_div">*</div>
	</div>
	<div class="field">
		<input type="text" class="easyui-validatebox control-img" validType="nullValueValid" required="true" value="" attrtype="String" name="requestDTO.companyName" id="request_companyName" title='<fmt:message key="label.belongs.client"/>'>
		<input type="hidden" value="" id="request_companyNo" name="requestDTO.companyNo">
		<a title='<fmt:message key="common.select"/>' onclick="itsm.request.requestCommon.chooseCompany('#request_companyNo','#request_companyName',this)" id="add_search_request_companyName">
			<img src="../images/icons/userpicker_disabled.gif" style="vertical-align:middle;">
		</a>
	</div><div class="field_opt"><a class="opacity" title="<fmt:message key="formDesigner.basicFieldsNotRemove"/>"><img src="../skin/default/images/grid_delete.png"></a></div>
		
</div>
<div class="field_options" attrtype="String" label='<fmt:message key="label.request.requestUser"/>' attrno="request_userName" attrname="requestDTO.createdByName" required="false" attrdatadictionary="" attrhiddenid="request_userId" attrhiddenname="requestDTO.createdByNo" attrhiddenval="" attreventtype="onclick"
 attreventval="itsm.request.requestCommon.selectRequestCreator('#request_userName','#request_userId','#request_companyNo',this)" value=""
	attrImgSrc="../skin/default/images/user.png" attrI18n="common_select">
	<div class="label">
		<div class="fieldName" title="<fmt:message key="label.request.requestUser"/>"><fmt:message key="label.request.requestUser"/></div><div class="required_div">*</div>
	</div>
	<div class="field">
		<input type="text" class="easyui-validatebox control-img" validType="nullValueValid" required="true" value="" attrtype="String" name="requestDTO.createdByName" id="request_userName" title='<fmt:message key="label.request.requestUser"/>'>
		<input type="hidden" value="" id="request_userId" name="requestDTO.createdByNo">
		<a title='<fmt:message key="common.select"/>' onclick="itsm.request.requestCommon.selectRequestCreator('#request_userName','#request_userId','#request_companyNo',this)" id="searchCreator">
			<img src="../skin/default/images/user.png" style="vertical-align:middle;">
		</a>
	</div><div class="field_opt"><a class="opacity" title="<fmt:message key="formDesigner.basicFieldsNotRemove"/>"><img src="../skin/default/images/grid_delete.png"></a></div>
		
</div>
<div class="field_options" attrtype="String" label='<fmt:message key="common.title"/>' attrno="request_etitle" attrname="requestDTO.etitle" required="false" attrdatadictionary="">
	<div class="label">
		<div class="fieldName" title="<fmt:message key="common.title"/>"><fmt:message key="common.title"/></div><div class="required_div">*</div>
	</div>
	<div class="field">
		<input type="text" class="easyui-validatebox input" validType="nullValueValid" required="true" value="" attrtype="String" name="requestDTO.etitle" id="request_etitle" title='<fmt:message key="common.title"/>'>
	</div><div class="field_opt"><a class="opacity" title='<fmt:message key="formDesigner.basicFieldsNotRemove"/>'><img src="../skin/default/images/grid_delete.png"></a></div>
		
</div>
<div class="field_options_lob field_options" attrtype="Lob" label='<fmt:message key="label.common.desc" />' attrno="request_edesc" attrname="requestDTO.edesc" required="false" attrdatadictionary="">
	<div class="label">
		<div class="fieldName" title="<fmt:message key="label.common.desc" />"><fmt:message key="label.common.desc" /></div><div class="required_div">*</div>
	</div>
	<div class="field">
		<textarea class="easyui-validatebox input" validType="nullValueValid" required="true" attrtype="Lob" name="requestDTO.edesc" id="request_edesc" title='<fmt:message key="label.common.desc" />' style="width: 741px; height: 53px;"></textarea>
	</div><div class="field_opt"><a class="opacity" title='<fmt:message key="formDesigner.basicFieldsNotRemove"/>'><img src="../skin/default/images/grid_delete.png"></a></div>
		
</div>