<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>   
<script>
var formCustomId = '${param.formCustomId}';
</script>
<style>
<!--
.right_options{
	margin:5px;
	cursor: move;
}
.float_right{
	float: right;
	cursor: pointer;
	display: none;
	color:red;
}
#dashboard_column div{
	cursor: move;
	padding:3px;
}
#dashboard_column div:hover {
	background-color: #ccccc;
}
#dashboard_column div:hover .float_right{
	display: block;
}
-->
</style>
<script src="../js/wstuo/customForm/customFormDesign.js?random=${currentTime}"></script>
<div class="row">
    <div class="box col-md-9">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 表单设计</h2>
            </div>
            <div class="box-content buttons " id="formCustom_content" style="min-height:550px">
				<input type="hidden" value="${param.formCustomId}" id="formCustomId">
				<%-- <fmt:message key="formDesigner.similarFormCustom"/>&nbsp;:&nbsp;
				<select id="similarFormCustom" onchange="javascript:wstuo.customForm.customFormDesign.formCustomDesign.getTemplateValue(this.value)"></select> --%>
				 <div class="form-inline">
						<div class="form-group">
							 <label><fmt:message key="label.moduleManage.moduleName"/></label>
							<select id="formCustomType" class="form-control" onchange="wstuo.customForm.customFormDesign.changeModule(this.value)">
							</select>
						</div>
						<div class="form-group">
							 <label ><fmt:message key="formCustom.formName"/>:</label>
							 <input type="text" class="form-control"  id="update_formCustomName">
						</div>
						<span id="customFormDesign_default_span">
						<label>
						<input type="checkbox" id="customFormDesign_default_form"> <fmt:message key="set.as.the.default.form"/></label></span>
						<button class="btn btn-default btn-xs" style="float:right" onclick="wstuo.customForm.customFormDesign.previewFormCustom()"><i class="glyphicon glyphicon-zoom-in"/> <fmt:message key="lable.notice.see"/></button>
						<button class="btn btn-primary btn-xs" style="float:right" id="customFormDesign_save" title="Ctrl+S" onclick="wstuo.customForm.customFormDesign.saveFormCustomEdit('')"><i class="glyphicon glyphicon-floppy-disk"/> <fmt:message key="common.save" /></button>
						<button class="btn btn-default btn-xs" style="float:right" onclick="basics.index.back()"><i class=" glyphicon glyphicon-share-alt"/> 返回列表</button>
				</div>
				<div class="column" id="dashboard_column" style="float:left;min-height:400px;width:99%;margin-top:6px">
				</div>
		</div>
	</div>
	</div>
	<div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 字段属性</h2>
            </div>
            <div class="box-content buttons" id="customField_fieldset">
					
			</div>
		</div>
			
	</div>
</div>
</div>
	<div id="previewFormCustom_win" class="WSTUO-dialog" style="width:650px;height:550px;padding:5px;">
		<form id="previewFormCustom"><!-- 点击事件时要用到 -->
		</form>
	</div>
	
	<div id="tabsField_win" class="WSTUO-dialog" title="<fmt:message key="label.TabSetting"/>">
	    <form id="tabsField_form">
	    <input type="hidden" id="tabs_id" name="formCustomTabsDTO.tabId">
		<input id="tabsField_fieldselect_attrNos" type="hidden">
		<table style="width: 100%" class="lineTable" cellspacing="1">
			<tr>
				<td width="150px"><fmt:message key="label.TabName"/></td>
				<td><input id="tabs_name" name="formCustomTabsDTO.tabName" style="width:96%" /></td>
			</tr>
			<tr>
				<td width="150px"><fmt:message key="formDesigner.extendedAttributeField" /></td>
				<td>
				<a id="tabsAttr_customField_select"><fmt:message key="common.select"/></a>
				<input type="hidden" id="tabAttrsConentJson" name="formCustomTabsDTO.tabAttrsConent">
				<input type="hidden" id="tabAttrsDecode" name="formCustomTabsDTO.tabAttrsDecode">
				<input type="hidden" id="tabAttrsConentDecode" name="formCustomTabsDTO.tabAttrsConentDecode">
				<div id="selectedTabAttrs"></div>
				<div id="tabAttrsConent" style="display: none"></div> 
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top: 8px;">
					<a id="link_tabs_save_ok" class="easyui-linkbutton" icon="icon-save"><fmt:message key="common.save"/></a>
				</td>
			</tr>
		</table>
		</form>
	</div>
	<div id="tabsAttrDiv" title='<fmt:message key="formDesigner.extendedAttributeField" />' style="width:530px;height:380px;padding:3px;display: none;">
		<div id="tabsAttr_customFieldToolbar">
			<a class="easyui-linkbutton" plain="true" icon="icon-ok" id="tabsAttr_customFieldGrid_select"><fmt:message key="common.select"/></a>
		</div>
		<table id="tabsAttr_customFieldList"></table>
		<div id="tabsAttr_customFieldPager"></div>
	</div>
	
	<div>
		<form id="formCustom_tabsIdForm">
		
		</form>
	</div>