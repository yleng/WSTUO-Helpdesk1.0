<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="../js/wstuo/customForm/fieldMain.js"></script>
<sec:authorize url="/pages/field!findPageField.action">
<div class="row">
	<div class="box col-md-12">
		<div class="box-inner">
			<div class="box-header well" data-original-title="">
				<h2>
					<i class="glyphicon glyphicon-list-alt"></i> 自定义字段列表
				</h2>
			</div>
			
			<!-- box-content -->
			<div class="box-content ">
				<table id="fieldGrid" class="scroll"></table>
				<div id="fieldGridPager" class="scroll" style="text-align: center;"></div>
				<div id="fieldGridToolbar" style="display:none">
				    <sec:authorize url="/pages/field!save.action">
					 <button class="btn btn-default btn-xs"  onclick="wstuo.customForm.fieldMain.addOpenWindow();"><i class="glyphicon glyphicon-plus"></i> 添加</button>
					</sec:authorize>
					 <sec:authorize url="/pages/field!update.action">
					<button class="btn btn-default btn-xs" onclick="wstuo.customForm.fieldMain.editOpenWindow();"><i class="glyphicon glyphicon-edit"></i> 修改</button>
					</sec:authorize>
					<sec:authorize url="/pages/field!remove.action">
					<button class="btn btn-default btn-xs" onclick="wstuo.customForm.fieldMain.deleteField();"><i class="glyphicon glyphicon-trash"></i> 删除</button>
					</sec:authorize>
					<button class="btn btn-default btn-xs"  onclick="wstuo.customForm.fieldMain.searchOpenWindow();" >
					<i class="glyphicon glyphicon-search"></i><fmt:message key="common.search" />
					</button>
				</div> 
				<div style="display:none" id="fieldGridAction"><sec:authorize url="/pages/field!update.action"><a href="javascript:wstuo.customForm.fieldMain.editfield('{id}')" title="<fmt:message key="title.orgSettings.editOrg" />"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;&nbsp;&nbsp; <sec:authorize url="/pages/field!remove.action"><a href="javascript:wstuo.customForm.fieldMain.delete_aff('{id}')" title="<fmt:message key="common.delete" />"><i class="glyphicon glyphicon-trash"></i></a></sec:authorize></div>
			</div>
			<!-- box-content -->
		</div>
	</div>
</div>
</sec:authorize>
<div id="addFieldDiv"  class="WSTUO-dialog" title="<fmt:message key="common.add"/>" style="height:auto; padding:15px; line-height:20px;">
		<form class="form-horizontal" event="wstuo.customForm.fieldMain.saveField">
			<div class="form-group">
				<label class="col-sm-4 control-label"><fmt:message key="label.role.roleName"/></label>
				<div class="col-sm-5">
					 <input name="field.fieldName" class="form-control" validType="nullValueValid"  required="true" id="add_fieldName" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><fmt:message key="label.moduleManage.moduleName"/></label>
				<div class="col-sm-5">
					<select id="add_fieldModule" name="field.module" class="form-control">
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.type"/></label>
				<div class="col-sm-5">
					<select id="add_fieldType" name="field.type" class="form-control" onchange="wstuo.customForm.fieldMain.selectDictionary(this.value)">
							<option value="text">文本</option>
							<option value="textarea">多行文本</option>
							<option value="number">数字</option>
							<option value="double">小数</option>
							<option value="date">日期</option>
							<option value="select">下拉框</option>
							<option value="radio">单选框</option>
							<option value="checkbox">多选框</option>
							<option value="tree">树结构</option>
					</select>
				</div>
			</div>
			<div class="form-group" style="display: none" id="add_dictionary_div">
				<label class="col-sm-4 control-label" for="lastname"><fmt:message key="title.security.dataDictionary"/></label>
				<div class="col-sm-5">
					<select id="add_dictionary" name="field.dataDictionary" class="form-control">
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">属性</label>
				<div class="col-sm-5">
					<label class="checkbox-inline"><input type="checkbox" id="field_required">必填</label>
					<label class="checkbox-inline"><input type="checkbox" id="field_showList">列表显示</label>
					<label class="checkbox-inline"><input type="checkbox" id="field_export">可导出</label>
					<!-- <label class="checkbox-inline"><input type="checkbox" id="field_search">可搜索</label> -->
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><fmt:message key="common.sort"/></label>
				<div class="col-sm-5">
					 <input name="field.sort" class="form-control" type="number" value="1" required="true" id="add_fieldsort" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><fmt:message key="label.user.description"/></label>
				<div class="col-sm-5">
					<textarea id="add_fielddescription" name="field.description" class="form-control" rows="3" ></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-4">
					<input name="field.id" type="hidden" id="add_fieldId">
					<input name="field.name" type="hidden" id="add_name">
					<input name="field.required" type="hidden" id="add_required" value="false">
					<input name="field.showList" type="hidden" id="add_showList" value="false">
					<input name="field.export" type="hidden" id="add_export" value="false">
					<input name="field.search" type="hidden" id="add_search" value="false">
					
                     <button type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save"/></button>
				</div>
			</div>
		</form>
		
	</div>
	
	<!-- 搜索 数据字典目录DIV start -->
	<div id="searchFieldDiv"  class="WSTUO-dialog" title="<fmt:message key="common.search"/>" style="height:auto; padding:15px; line-height:20px;">
		<form  method="post" class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-4 control-label"><fmt:message key="label.role.roleName"/></label>
				<div class="col-sm-5">
					 <input name="field.fieldName" class="form-control" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><fmt:message key="label.moduleManage.moduleName"/></label>
				<div class="col-sm-5">
					<select name="field.module" class="form-control">
							<option value="request">请求管理</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.type"/></label>
				<div class="col-sm-5">
					<select name="field.type" class="form-control">
						<option value="">全部</option>
							<option value="text">文本</option>
							<option value="textarea">多行文本</option>
							<option value="number">数字</option>
							<option value="double">小数</option>
							<option value="date">日期</option>
							<option value="select">下拉框</option>
							<option value="radio">单选框</option>
							<option value="checkbox">多选框</option>
							<option value="tree">树结构</option>
					</select>
				</div>
			</div>
<%-- 			<div class="form-group">
				<label class="col-sm-4 control-label"><fmt:message key="label.user.description"/></label>
				<div class="col-sm-5">
					<textarea name="field.description" class="autogrow"  cols="3"></textarea>
				</div>
			</div> --%>
			
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-4">
					<button id="fieldGrid_doSearch"  type="button" class="btn btn-primary btn-sm"><fmt:message key="common.search"/></button>
				</div>
			</div>
		</form>
	</div>
