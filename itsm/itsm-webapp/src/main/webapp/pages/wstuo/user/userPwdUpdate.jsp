<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../../language.jsp" %>
<script src="../js/wstuo/user/userPwdUpdate.js"></script>

<div id="pwdUpdate" class="WSTUO-dialog" title="<fmt:message key="title.password_changed"/>" style="height:auto;padding: 0px;">
	<form id="edit_user_password_form">
			<table width="100%" class="lineTable" cellspacing="1">
				<tr>
					<td style="width:30%"><fmt:message key="label.user.loginName" /></td>
					<td style="width:70%">${loginUserName}
						<input type="hidden"  name="editUserPasswordDTO.loginName" value="${loginUserName}" />
					</td>
				</tr>
				<tr>
					<td ><fmt:message key="label.old.password" /></td>
					<td><input type="password" id="oldPassword" name="editUserPasswordDTO.oldPassword" class="easyui-validatebox" style="width:200px" required="true"/></td>
				</tr>
				<tr>
					<td ><fmt:message key="label.new.password" /></td>
					<td><input type="password" id="newPassword" name="editUserPasswordDTO.newPassword" class="easyui-validatebox" style="width:200px" validType="useEnCharPwd" <c:if test="${passwordType ne 'Simple'}">validType="passwordsecurity"</c:if>  required="true" /> </td>
				</tr>
				<tr>
					<td ><fmt:message key="label.user.comfirmLoginPassword" /></td>
					<td><input type="password" id="repeatPassword" class="easyui-validatebox" style="width:200px" validType="equalTo['newPassword']" required="true"/></td>
				</tr>
				
				<tr>
					<td colspan="2" ><a class="easyui-linkbutton" icon="icon-save" id="link_eidt_user_password" ><fmt:message key="common.update" /></a></td>
				</tr>
				
			</table>
		</form>
</div>
