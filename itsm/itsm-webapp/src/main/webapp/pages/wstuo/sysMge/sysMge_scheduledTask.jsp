<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="${pageContext.request.contextPath}/js/wstuo/scheduledTask/scheduledTasksManage.js?random=<%=new java.util.Date().getTime()%>"></script>

<div class="tab-pane active" id="list">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;<fmt:message key="title.scheduled.tasks.manage" /></h2>
	        <div class="box-icon"></div>
        </div>
		<div id="scheduledTasksManage_content" style="padding: 3px;">
			<!-- 定期任务数据列表 -->
			<table id="scheduledTasksGrid"></table>
			<div id="scheduledTasksPager"></div>	
			<div id="scheduledTaskOptItem"  style="display: none">
				<div class="panelBar">	
					<sec:authorize url="/pages/user!save.action">
						<a class="btn btn-default btn-xs" id="link_scheduledTask_add" ><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></a>			
					</sec:authorize>	
					<sec:authorize url="/pages/user!merge.action">
						<a class="btn btn-default btn-xs" id="link_scheduledTask_edit" ><i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit" /></a>
					</sec:authorize> 	
					<sec:authorize url="/pages/user!delete.action">
						<a class="btn btn-default btn-xs" id="link_scheduledTask_delete" ><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></a>
					</sec:authorize>	
					<sec:authorize url="/pages/user!find.action">
						<a class="btn btn-default btn-xs" id="scheduledTask_search"><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></a>				
					</sec:authorize>				
				</div>
			</div>
		
<!-- 搜索用户 -->
<div id="scheduledTaskSearchDiv" class="WSTUO-dialog" title="<fmt:message key="common.search" />" style="width:480px;height:auto">
	<form>
		<div >
			<table style="width:100%; cellspacing="1" class="table">
				<tr style="height: 30px;">
					<td><h5 style="color:black;padding-left: 15px; "><fmt:message key="label.type" /></h5></td>
					<td>
						<select name="queryDTO.scheduledTaskType" class="form-control">
							<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
							<c:if test="${requestHave eq true}">
							<option value="request"><fmt:message key="label.scheduled.request.task" /></option>
							</c:if>
							<option value=sla><fmt:message key="label.scheduled.sla.task" /></option>
							<option value="email"><fmt:message key="label.scheduled.email.task" /></option>
							<%-- 
							<c:if test="${requestHave eq true and problemHave eq true}">
							<option value="request2problem"><fmt:message key="label.ur.request_to_problem_rule" /></option>
							</c:if>
							<c:if test="${cimHave eq true}">
							<option value="configureItem"><fmt:message key="label.scheduled_configureItem.task" /></option>
							</c:if>
							<option value="adUpdate"><fmt:message key="label_ad_update_task" /></option>
							<option value="senReport"><fmt:message key="label.customReport.autoSendReport" /></option>
							 --%>
						</select>
					</td>
				</tr>
				<tr style="height: 30px;">
					<td><h5 style="color:black;padding-left: 15px; "><fmt:message key="label.scheduled.type" /></h5></td>
					<td>
						<select name="queryDTO.timeType" class="form-control">
							<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
							<option value="day"><fmt:message key="title.scheduled.day.plan" /></option>
							<option value="weekly"><fmt:message key="title.scheduled.week.plan" /></option>
							<option value="month"><fmt:message key="title.scheduled.month.plan" /></option>
							<option value="cycle"><fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.day" />)</option>
							<option value="cycleMinute"><fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.minute" />)</option>
							<option value="on_off"><fmt:message key="title.scheduled.on-off.plan" /></option>
						</select>
					</td>
				</tr>

				<tr style="height: 30px;">
					<td><h5 style="color:black;padding-left: 15px; "><fmt:message key="label.dateRange"/></h5></td>
					<td>
						<input id="scheduledTask_search_taskDate" class="form-control"  name="queryDTO.taskDate" style="width:120px;float: left;" readonly="readonly"/>
						<span style="float: left;">&nbsp;<fmt:message key="setting.label.to"/>&nbsp;</span>
						<input id="scheduledTask_search_taskEndDate" class="form-control"  name="queryDTO.taskEndDate" style="width:120px;float: left;" readonly="readonly"/>
						<h5 style="float: left;padding-left: 5px;">
						<a href="#" plain="true" onclick="cleanIdValue('scheduledTask_search_taskDate','scheduledTask_search_taskEndDate')" title="<fmt:message key="label.request.clear"/>"><i class=" glyphicon glyphicon-trash"></i></a>
						</h5>
					</td>
				</tr>
				<tr style="height: 30px;">
			    	<td><h5 style="color:black;padding-left: 15px; "><fmt:message key="common.createTime"/></h5></td>
			    	<td>
				    	<input name="queryDTO.startCreateTime" class="form-control" id="scheduledTask_search_startCreateTime" style="width:120px;float: left;" readonly="readonly"/>
				    	
				    	<span style="float: left;">&nbsp;<fmt:message key="setting.label.to"/>&nbsp;</span>
				    	<input name="queryDTO.endCreateTime" class="form-control" id="scheduledTask_search_endCreateTime" style="width:120px;float: left;" readonly="readonly"/>		    		
			    		<h5 style="float: left;padding-left: 5px;">
			    		<a href="#" plain="true"  onclick="cleanIdValue('scheduledTask_search_startCreateTime','scheduledTask_search_endCreateTime')" title="<fmt:message key="label.request.clear"/>"><i class=" glyphicon glyphicon-trash"></i></a>
			    	 	</h5>
			    	</td>
		    	</tr>
		    	<tr style="height: 30px;">
			    	<td><h5 style="color:black;padding-left: 15px; "><fmt:message key="common.updateTime"/></h5></td>
			    	<td>
			    		
			    		<input name="queryDTO.startUpdateTime" class="form-control" id="scheduledTask_search_startUpdateTimes" style="width:120px;float: left;" readonly="readonly"/>
			    		<span style="float: left;">&nbsp;<fmt:message key="setting.label.to"/>&nbsp;</span>
			    		<input name="queryDTO.endUpdateTime" class="form-control" id="scheduledTask_search_startUpdateTime" style="width:120px;float: left;" readonly="readonly"/>
			    		<h5 style="float: left;padding-left: 5px;">
			    		<a href="#" plain="true" onclick="cleanIdValue('scheduledTask_search_startUpdateTimes','scheduledTask_search_startUpdateTime')" title="<fmt:message key="label.request.clear"/>"><i class=" glyphicon glyphicon-trash"></i></a>
			    		</h5>
			    	</td>
		    	</tr>
		    	
		    	<tr>
		    		<td colspan="2" style="height:30px;text-align: center;">		    		
		    		 <input type="button" id="scheduledTask_search_doSearch" class="btn btn-primary btn-sm"  value="<fmt:message key="common.search" />"/>			        		
		        	</td>
		    	</tr>
			</table>
		</div>
	</form>		
</div>

<!-- 选择创建的类型 -->
<div id="scheduledTaskTypeDiv" class="WSTUO-dialog" title="<fmt:message key="label.type" />" style="width:300px;height:auto;">
	<div style="text-align: center; ">		
		<c:if test="${requestHave eq true}">
			<%-- 
			<label style="text-align: center;margin-top: 15px;">
				<input type="button" class="btn btn-primary btn-sm" name="scheduledTaskType" value="<fmt:message key="label.scheduled.request.task" />" onclick="wstuo.scheduledTask.scheduledTasksManage.selectScheduledTaskConfirm('request')"/><br>
			</label> 
			--%>
			<label style="text-align: center;margin-top: 15px;">
				<input type="button" class="btn btn-primary btn-sm" name="scheduledTaskType" value="<fmt:message key="label.scheduled.sla.task"/>" onclick="wstuo.scheduledTask.scheduledTasksManage.selectScheduledTaskConfirm('sla')"/><br>
			</label>	
			<label style="text-align: center;margin-top: 15px;">
				<input type="button" class="btn btn-primary btn-sm" name="scheduledTaskType" value="<fmt:message key="label.scheduled.email.task" />" onclick="wstuo.scheduledTask.scheduledTasksManage.selectScheduledTaskConfirm('email')"/><br>
			</label><br/>
		</c:if>			
	</div>
</div>

<!-- 创建定期任务 -->
<div id="scheduledTask_add_win" class="WSTUO-dialog" title="<fmt:message key="创建定期任务" />" style="width:520px;height:auto;">
	<form id="scheduledTask_add_comm_form">	
		<input type="hidden" id="base_scheduledTask_reportId" name="scheduledTaskDTO.reportId" value="0"  />
		<input type="hidden" id="base_scheduledTask_id" name="scheduledTaskDTO.scheduledTaskId"  />
		<input type="hidden" id="base_scheduledTaskType" name="scheduledTaskDTO.scheduledTaskType"  />
		<input type="hidden" id="base_scheduledTask_beanId" name="scheduledTaskDTO.beanId"  />
		<input type="hidden" id="base_scheduledTask_companNo" name="scheduledTaskDTO.requestDTO.companyNo"  />		
	</form>
	<form id="scheduledTask_add_form" >
	<div style="height: auto">
	<table style="width:100%" cellspacing="1" class="table table-condensed">
		<tr id="base_scheduledTask_title_tr">
			<td style="border-top: 0px;"><fmt:message key="common.title" /></td>
			<td style="border-top: 0px;"><input id="base_scheduledTask_etitle" name="scheduledTaskDTO.requestDTO.etitle" validType="length[1,200]" class="form-control"  required="true" /></td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="100%">
					<tr>
						<td>
							<label class="radio-inline">
								<input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_day" value="day" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')" checked="checked">
								<fmt:message key="title.scheduled.day.plan" />
							</label>
						</td>
						<td>
							<label class="radio-inline"><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_weekly" value="weekly" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
							<fmt:message key="title.scheduled.week.plan" /></label>
						</td>
						<td>
							<label class="radio-inline"><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_month" value="month" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
							<fmt:message key="title.scheduled.month.plan" /></label>
						</td>
					</tr>
					<tr>
						<td>
							<%-- 周期性计划(天) --%>
							<label class="radio-inline"><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_cycle" value="cycle" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
							<fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.day" />)</label>
						</td>
						<td>
							<%-- 周期性计划(分钟) --%>
							<label class="radio-inline"><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_cycleMinute" value="cycleMinute" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
							<fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.minute" />)</label>
						</td>
						<td>
							<label class="radio-inline"><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_on_off" value="on_off" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
							<fmt:message key="title.scheduled.on-off.plan" /></label>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<b>[<span id="base_everyWhat_show"><fmt:message key="label.scheduledTask.day" /></span>]</b><br>
				
				<div id="base_everyWhat_weekly" style="display: none;" >
					<!--每周定时维护： -->
					<table width="100%"  cellspacing="1">
						<tr><td colspan="4"><input type="checkbox" id="base_scheduledTask_weekWeeks_all" value="1" onclick="wstuo.scheduledTask.scheduledTask.checkAll('base_scheduledTask_weekWeeks_all','base_everyWhat_weekly','scheduledTaskDTO.weekWeeks')" />
						<fmt:message key="label.date.weekly" />：</td></tr>
						<tr>
							<td><input type="checkbox" id="checkbox_SUN" value="SUN" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.sunday" /></td>
							<td><input type="checkbox" id="checkbox_MON" value="MON" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.monday" /></td>
							<td><input type="checkbox" id="checkbox_TUE" value="TUE" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.tuesday" /></td>
							<td><input type="checkbox" id="checkbox_WED" value="WED" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.wednesday" /></td>
						</tr>
						<tr>
							<td><input type="checkbox" id="checkbox_THU" value="THU" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.thursday" /></td>
							<td><input type="checkbox" id="checkbox_FRI" value="FRI" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.friday" /></td>
							<td><input type="checkbox" id="checkbox_SAT" value="SAT" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.saturday" /></td>
							<td></td>
						</tr>
					</table>
				</div>
				
				<div id="base_everyWhat_monthly" style="display: none;"  >
					<!-- 每月定时维护：  -->
					
					<table width="100%"  cellspacing="1">
						<tr><td colspan="4">
							<input type="checkbox" value="1" id="base_scheduledTask_monthMonths_all" onclick="wstuo.scheduledTask.scheduledTask.checkAll('base_scheduledTask_monthMonths_all','base_everyWhat_monthly','scheduledTaskDTO.monthMonths')" />
						<fmt:message key="label.date.month" />：</td></tr>
						<tr>
							<td><input type="checkbox" id="checkbox_JAN" value="JAN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jan" /></td>
							<td><input type="checkbox" id="checkbox_FEB" value="FEB" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Feb" /></td>
							<td><input type="checkbox" id="checkbox_MAR" value="MAR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Mar" /></td>
							<td><input type="checkbox" id="checkbox_APR" value="APR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Apr" /></td>
						</tr>
						<tr>
							<td><input type="checkbox" id="checkbox_MAY" value="MAY" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.May" /></td>
							<td><input type="checkbox" id="checkbox_JUN" value="JUN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jun" /></td>
							<td><input type="checkbox" id="checkbox_JUL" value="JUL" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jul" /></td>
							<td><input type="checkbox" id="checkbox_AUG" value="AUG" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Aug" /></td>
						</tr>
						<tr>
							<td><input type="checkbox" id="checkbox_SEP" value="SEP" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Sep" /></td>
							<td><input type="checkbox" id="checkbox_OCT" value="OCT" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Oct" /></td>
							<td><input type="checkbox" id="checkbox_NOV" value="NOV" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Nov" /></td>
							<td><input type="checkbox" id="checkbox_DEC" value="DEC" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Dec" /></td>
						</tr>
						<tr>
						<td colspan="4">
							<h5 style="float: left;color: black;"><fmt:message key="common.date"></fmt:message>：</h5>
							<span style="float: left;margin-left: 8px;">
							<select name="scheduledTaskDTO.monthDay" id="base_scheduledTask_monthDay" class="form-control" style="width: 90px;">
							</select>
							</span>						
						</td>
						</tr>
					</table>

				</div>
				
				<div id="base_everyWhat_cycle" style="display: none;">
					<!-- 周期性地维护： ：  -->
					<hr>
					<h5 style="float: left;color: black;"><fmt:message key="label.scheduledTask.every"/></h5>
					<span style="float: left;margin-left: 8px;">
						<input type="text"  class="form-control" style="width: 50px;" value="1" id="base_scheduledTask_cyclicalDay" name="scheduledTaskDTO.cyclicalDay">
					</span>					
					<h5 style="float: left; margin-left: 5px;color: black;"><fmt:message key="label.scheduledTask.day.one"/></h5>
				</div>
				
				<%-- 周期性执行(分钟) --%>
				<div id="base_everyWhat_cycleMinute" style="display: none;">				
					<hr>
					<h5 style="float: left;color: black;"><fmt:message key="label.scheduledTask.every"/></h5>
					<span style="float: left;margin-left: 8px;">
					<input type="text" width="12px" class="form-control" style="width: 50px;" value="1" id="base_scheduledTask_cyclicalMinute" name="scheduledTaskDTO.cyclicalMinute">
					</span>
					<h5 style="float: left; margin-left: 5px;color: black;"><fmt:message key="label.sla.minute" /></h5>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="base_scheduledTask_taskDate" style="width: 100%">
				<table cellspacing="1" class="table table-bordered">
					<tr>
						<td><span id="base_scheduledTask_startTime" ><fmt:message key="label.sla.slaStartTime" /></span></td>
						<td><input type="text" id="base_scheduledTask_startDate_input" name="scheduledTaskDTO.taskDate" style="width:170px;" class="form-control" readonly="readonly"></td>
					</tr>	
					<tr id="scheduledTask_endTime_id">
						<td><span id="base_scheduledTask_endTime"><fmt:message key="label.sla.slaEndTime" /></span></td>
						<td><input type="text" name="scheduledTaskDTO.taskEndDate" id="base_scheduledTask_endDate_input" style="width:170px;" class="form-control" readonly="readonly"/></td>										
					</tr>
					<tr>
						<td><fmt:message key="label.scheduledTask.specific.time" /></td>
						<td>
							<span style="float: left;">
							<select name="scheduledTaskDTO.taskHour" id="base_scheduledTask_hours" class="form-control" style="width: 70px;">
								<c:forEach var="i" begin="0" end="23" step="1"> 
									<option value="${i}">${i}</option>
								</c:forEach>
							</select>
							</span>
							<h5 style="float: left; margin-left: 5px;">
							<fmt:message key="label.orgSettings.hour" />
							</h5>
							<!-- 分 -->
							<span style="float: left;margin-left: 5px;">
							<select name="scheduledTaskDTO.taskMinute" id="base_scheduledTask_minute" class="form-control" style="width: 70px;">
								<c:forEach var="i" begin="0" end="59" step="1"> 
									<option value="${i}">${i}</option>
								</c:forEach>
							</select>
							</span>
							<h5 style="float: left; margin-left: 5px;">
								<fmt:message key="label.orgSettings.minute" />
							</h5>	
						 </td>										
					</tr>
				</table>						
				</div>					
			</td>
		</tr>
		<tr>
			<td colspan="2">		
				<input type="button" class="btn btn-primary btn-sm" id="scheduledTask_save_but" style="margin-right: 15px;" value="<fmt:message key="common.save"/>">		
				<a class="btn btn-primary btn-sm" id="scheduledTask_ok_btn" onclick="javascript:$('#scheduledTask_add_win').window('close');"><fmt:message key="common.save"/></a>
			</td>
		</tr>
	</table>
	</div>	
	</form>
	</div>
	</div>
</div>

</div>