<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="../js/wstuo/sysMge/moduleManage.js?random=<%=new java.util.Date().getTime()%>"></script>

<!-- 树形主面板开始-->
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 模块管理</h2>
            </div>
            <div class="box-content buttons" id="moduleManage_content">
                <table id="moduleManageGrid"></table>
				<div id="moduleManageGridPager"></div>
            </div>
        </div>
    </div>
</div>
<div id="moduleManageGridToolbar" style="display: none">
	 <%-- <button class="btn btn-default btn-xs"  id="installModuleBtn" onclick="javascript:wstuo.sysMge.moduleManage.addopenWindow()"><i class="glyphicon glyphicon-plus"></i> <fmt:message key="common.add" /></button>
	--%><button class="btn btn-default btn-xs" id="editShowSortBtn" onclick="javascript:wstuo.sysMge.moduleManage.edit_openWindow()"><i class="glyphicon glyphicon-edit"></i> <fmt:message key="common.edit" /> </button>
	<%-- <button class="btn btn-default btn-xs" id="unInstallModuleBtn" onclick="javascript:wstuo.sysMge.moduleManage.unInstall_aff()"><i class="glyphicon glyphicon-ban-circle"></i> <fmt:message key="common.enable" />/<fmt:message key="common.disable" /> </button>
	 --%> <button class="btn btn-default btn-xs" id="searchModuleBtn" onclick="javascript:wstuo.sysMge.moduleManage.search_openWindow()"><i class="glyphicon glyphicon-search"></i> <fmt:message key="common.search" /></button>
</div>

<div id="searchModuleDiv" class="WSTUO-dialog" title="<fmt:message key="label.technical.searchGroup" />" style="width: 400px; height:auto">
<form class="form-horizontal" event="wstuo.sysMge.moduleManage.editModule">
	<div class="form-group">
		<label class="col-sm-4 control-label" for=""><fmt:message key="label.moduleManage.moduleName" /></label>
		<div class="col-sm-5">
			 <input type="text" class="form-control" id="searchTitle" name="dto.title">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-4 control-label" for=""><fmt:message key="common.code" /></label>
		<div class="col-sm-5">
			 <input type="text" class="form-control" name="dto.moduleName" id="searchmoduleName">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-4 control-label" for=""><fmt:message key="label.user.description" /></label>
		<div class="col-sm-5">
			 <textarea class="form-control" name="dto.description" id="searchdescription"></textarea>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-9 col-sm-offset-4">
			<button  type="button" class="btn btn-primary btn-sm" id="searchModuleBtn_OK"><fmt:message key="common.search"/></button>
			<button type="reset" class="btn btn-default btn-sm"><fmt:message key="common.reset"/></button>
		</div>
	</div>
</form>

</div>


<div id="editModuleDiv" class="WSTUO-dialog" title="<fmt:message key="common.add" />/<fmt:message key="common.edit" />" style="width: 400px; height:auto">
<form class="form-horizontal" event="wstuo.sysMge.moduleManage.editModule">
	<input type="hidden" name="dto.moduleId" id="editModuleId"/>
	<div class="form-group">
		<label class="col-sm-4 control-label" for=""><fmt:message key="label.moduleManage.moduleName" /></label>
		<div class="col-sm-5">
			 <input type="text" class="form-control" id="editTitle" name="dto.title" required="true">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-4 control-label" for=""><fmt:message key="common.code" /></label>
		<div class="col-sm-5">
			 <input type="text" class="form-control" name="dto.moduleName" id="editmoduleName" required="true" validType="chars">
			<span>只能输入英文</span>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-4 control-label" for=""><fmt:message key="common.sort" /></label>
		<div class="col-sm-5">
			 <input type="number" class="form-control" name="dto.showSort" id="editShowSort" required="true" >
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-4 control-label" for="">默认流程</label>
		<div class="col-sm-5">
			 <select class="form-control" name="dto.pid" id="editPid"></select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-4 control-label" for=""><fmt:message key="label.user.description" /></label>
		<div class="col-sm-5">
			 <textarea class="form-control" name="dto.description" id="editdescription"></textarea>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-9 col-sm-offset-4">
			<button  type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save"/></button>
			<button type="reset" class="btn btn-default btn-sm"><fmt:message key="common.reset"/></button>
		</div>
	</div>
</form>
</div>
