<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="java.text.DecimalFormat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>   
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<%
    WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
	IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
	if(session.getAttribute("loginUserName")!=null){
	 	Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
	 		  "/pages/ci!cItemUpdate.action",
			  "CofnigureItem_AssetOriginalValue_Res",
			  "CofnigureItem_Barcode_Res",
			  "CofnigureItem_Supplier_Res",
			  "CofnigureItem_PurchaseDate_Res",
			  "CofnigureItem_ArrivalDate_Res",
			  "CofnigureItem_PurchaseNo_Res",
			  "CofnigureItem_Owner_Res",
			  "CofnigureItem_LifeCycle_Res",
			  "CofnigureItem_WarrantyDate_Res",
			  "CofnigureItem_WarningDate_Res",
			  "CofnigureItem_SerialNumber_Res",
			  "CofnigureItem_Department_Res",
			  "CofnigureItem_Fimapping_Res",
			  "CofnigureItem_ExpiryDate_Res",
			  "CofnigureItem_LendTime_Res",
			  "CofnigureItem_OriginalUser_Res",
			  "CofnigureItem_RecycleTime_Res",
			  "CofnigureItem_PlannedRecycleTime_Res",
			  "CofnigureItem_UsePermission_Res",
			  "/pages/ciRelevance!save.action",
			  "/pages/ciRelevance!update.action",
			  "/pages/ciRelevance!delete.action"
			  });
	 	request.setAttribute("resAuth",resAuth);
	}
%>
<script type="text/javascript">
if("${ciDetailDTO.ciId}" ==null || "${ciDetailDTO.ciId}" ==""){
	endProcess();
	msgAlert(i18n.requestIsDelete,'info');
	basics.tab.tabUtils.closeTab(i18n.ci_configureItemInfo);
}
</script>
<input type="hidden" id="configureCiInfoId" value="${ciDetailDTO.ciId}" />
<input type="hidden" id="configureItem_num" value="${ciDetailDTO.cino}" />
<script src="${pageContext.request.contextPath}/scripts/itsm/cim/ciTree.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="${pageContext.request.contextPath}/scripts/itsm/cim/configureItemInfoFormCustom.js?random=<%=new java.util.Date().getTime()%>"></script>

<div  class="loading" id="configureItemInfo_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>
	<!-- 新的面板 start-->
	<div id="ciDetails_layout" class="easyui-layout" fit="true" style="display: none;">
		<div region="north" border="false" class="panelTop">
			<a class="easyui-linkbutton" plain="true" icon="icon-undo" id="configureItemInfo_returnToList" title="<fmt:message key="common.returnToList" />"></a>
			<c:if test="${resAuth['/pages/ci!cItemUpdate.action'] }">
				<a class="easyui-linkbutton" plain="true" icon="icon-edit" id="configureItem_edit_but" title="<fmt:message key="common.edit"/>"></a> 
			</c:if>
		</div>
	
		<div region="center" class="easyui-panel" fit="true"  id="configureItemInfo_center" title=''>
			<div>
				<div style="background-color: #E0ECFF;float:left;width:99.5%;margin-top:5px;padding:5px;font-weight: bold;"><fmt:message key="common.basicInfo" /></div>
				<div id="cidetail_formField" style="float: left; width: 99%;padding-left:3% ">
				
				</div>
			</div>
			<div id="configureItemInfoTab" class="easyui-tabs" fit="true" style="width:95%;height: 500px;"> 
                
                <%-- 附件 --%>
				<div title='<fmt:message key="common.attachment" />'>
						<div class="lineTableBgDiv" id="configureItemInfo_effect_atta_div">
						<form>
							<input type="hidden" id="configureItemInfo_effect_attachmentStr"/>
							<table width="100%" cellspacing="1" class="lineTable">
								
								<tr>
									<td>
									<div id="configureItemInfo_effect_success_attachment" style="line-height:25px;color:#555;display: none"></div>
									<div class="hisdiv" id="show_configureItemInfo_effectAttachment">
										<table style="width:100%" class="histable" cellspacing="1">
											<thead>
												<tr>
													<th><fmt:message key="common.id" /></th>
													<th><fmt:message key="label.problem.attachmentName" /></th>
													<th><fmt:message key="label.problem.attachmentUrl" /></th>
													<th><fmt:message key="label.sla.operation" /></th>
												</tr>
											</thead>
											<tbody></tbody>
					             			</table>
					               	</div>
									</td>
								</tr>
								
								 <tr>
									<td>
                                        <div class="diyLinkbutton">
                                           <div style="float: left;cursor: pointer;">
										      <input type="file"  name="filedata" id="configureItemInfo_effect_file">
											</div>
										<div style="float: left;margin-left: 15px;">
											<a class="easyui-linkbutton" icon="icon-upload" href="javascript:itsm.cim.configureItemInfoFormCustom.configureitemInfo_uploadifyUpload()" ><fmt:message key="label.attachment.upload" /></a>
							            </div>
							            <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('ConfigureItemDetail','edit')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
						             	 </div>
                                        <div style="clear: both;"></div>
                                         <div id="configureItemInfo_effect_fileQueue"></div>
									</td>
								</tr>
							</table>
						</form>
						<%-- <form id="uploadForm_CiInfo" method="post" enctype="multipart/form-data">
							  <table border="0" cellspacing="1" class="fu_list">
					              <tr height="40px">
						        	<td colspan="2">
						        	<table border="0" cellspacing="0"><tr><td >
							       			<a href="javascript:void(0);" class="files" id="idFile_CiInfo"></a>
							       		</td><td>
									        <a class="easyui-linkbutton" style="float:left" plain="true"  icon="icon-ok" id="idBtnupload_CiInfo"><fmt:message key="label.startUpload" /></a>
									         &nbsp;&nbsp;&nbsp;
											<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_CiInfo"><fmt:message key="label.allcancel" /></a>
											 &nbsp;&nbsp;&nbsp;
											<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('ConfigureItemDetail','edit')" class="easyui-linkbutton" icon="icon-ok" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
										</td></tr></table>
									</td>
						      		</tr>
						          <tbody id="idFileList_CiInfo">
						          </tbody>
								</table>
							</form> --%>
					</div>
				</div>
				 <!-- 软件配置参数 start 
				<div title="<fmt:message key="label.ci.softSettingParam"/>">
					
					<form>
					<div class="hisdiv" id="ci_add_softSetting">
					<table style="width:100%" class="histable" cellspacing="1">
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSetParam"/></td>
							<td width="60%"><pre>${ciDetailDTO.softSetingParam}</pre></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingAm"/></td>
							<td width="60%"><pre>${ciDetailDTO.softConfigureAm}</pre></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark1"/></td>
							<td width="60%"><pre>${ciDetailDTO.softRemark1}</pre></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark2"/></td>
							<td width="60%"><pre>${ciDetailDTO.softRemark2}</pre></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark3"/></td>
							<td width="60%"><pre>${ciDetailDTO.softRemark3}</pre></td>
						</tr>
             		</table>
               		</div>
					<div class="hisdiv">
						<table width="100%" cellspacing="1" class="histable">
							<c:if test="${not empty ciDetailDTO.softAttachments}">
							<thead>
							<tr>
								<th><fmt:message key="common.id" /></th>
								<th><fmt:message key="label.problem.attachmentName" /></th>
								<th><fmt:message key="label.problem.attachmentUrl" /></th>
								<th><fmt:message key="ci.operateItems" /></th>
							</tr>
							</thead>
							
							<c:forEach items="${ciDetailDTO.softAttachments}" varStatus="i" var="attachment">
							<tr>
								<td>${i.count}</td>
								<td style="text-align: left;" ><a target="_blank" href="attachment!download.action?downloadAttachmentId=${attachment.aid }">${attachment.attachmentName}</a></td>
								<td style="text-align: left;" >&nbsp;&nbsp;${attachment.url}</td>
								<td><a target="_blank" href="attachment!download.action?downloadAttachmentId=${attachment.aid }">[<fmt:message key="label.problem.attachmentDownload" />]</a></td>
							</tr>
							</c:forEach>
							
							</c:if>

							<c:if test="${empty ciDetailDTO.softAttachments}">
								<tr><td style="color:red;font-size:16px"><fmt:message key="label.notAttachment" /></td></tr>
							</c:if>
						</table>	
                
                	</div>
				</form>	
				</div>-->
				<!-- 软件配置参数end -->
                <div title='<fmt:message key="label.ci.ciServiceDir" />'>
					
					<div class="hisdiv">
						<table width="100%" cellspacing="1" class="histable">
							<c:if test="${not empty ciDetailDTO.serviceDirDtos}">
							<thead>
								<tr>
									<th style="width:10%"><fmt:message key="lable.serial.number"/> </th>
									<th style="width:90%"><fmt:message key="lable.ci.ciServiceDirName" /></th>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${ciDetailDTO.serviceDirDtos}" var="sd" varStatus="i">
							<tr>
							<td>${i.count}</td>
							<td>${sd.eventName}</td>
							</tr>
							</c:forEach>
							</tbody>
							</c:if>
							<c:if test="${empty ciDetailDTO.serviceDirDtos}">
								<tr><td style="color:#FF0000;font-size:16px"><fmt:message key="commom.noData" /></td></tr>
							</c:if>
						</table>
					</div>
				</div>
				<sec:authorize url="configItem_detail_queryRelatingconfigItem">
                <div title="<fmt:message key="ci.relatedCI"/>" style="padding:2px; width: 100%;">
                	<div style="display: none;" id="ciRelevancaToolbar">
                	<c:if test="${resAuth['/pages/ciRelevance!save.action'] }">
                    	<a class="easyui-linkbutton" icon="icon-add" plain="true" onclick="javascript:itsm.cim.configureItemInfoFormCustom.open_ci_relevance_win()" title="<fmt:message key="common.add"/>"></a>
                    </c:if>
                    <c:if test="${resAuth['/pages/ciRelevance!update.action'] }"> 
                        <a class="easyui-linkbutton" icon="icon-edit"  plain="true" id="link_ci_relevance_edit" onclick="itsm.cim.configureItemInfoFormCustom.ciRelevanceEdit()" title="<fmt:message key="common.edit"/>"></a>
                    </c:if>
                    <c:if test="${resAuth['/pages/ciRelevance!delete.action'] }"> 
                        <a class="easyui-linkbutton" icon="icon-cancel"  plain="true" onclick="javascript:itsm.cim.configureItemInfoFormCustom.ciRelevancetool_bar_delete_aff()"  title="<fmt:message key="common.delete"/>"></a>
                    </c:if>
                    <sec:authorize url="configItem_detail_configItemRelatingTreePic">
                    	<a class="easyui-linkbutton"  icon="icon-treeView" plain="true" id="ciRelevanceTree_but" onclick="itsm.cim.configureItemUtil.ciRelevanceTree(${ciDetailDTO.ciId},'configureItemInfo_ci_relevance_win','configureItemInfo_ci_relevance_tree')" title="<fmt:message key="ci.ciRelevanceTreeView" />"></a>
                   	</sec:authorize>
                   	<sec:authorize url="configItem_detail_configItemRelatingPic">
                   		<a class="easyui-linkbutton" icon="icon-relation"  plain="true" id="prefuser" onclick="itsm.cim.configureItemInfoFormCustom.showPrefuse()" title="<fmt:message key="ci.Topological.Graph"/>"></a>
                   </sec:authorize>
                    </div>
                	<table id="configureItemInfoRelevanceCI" ></table>
					<div id="configureItemInfoRelevanceCIPager"></div>
                </div>
                </sec:authorize>
                
              	<%-- <sec:authorize url="/pages/ciSoftwareAction!findSoftwarePager.action">
                <div title="<fmt:message key="config.installSort"/>" style="padding:2px; width: 100%;">         
                	<table id="installSoftwareGrid"></table>
					<div id="installSoftwarePager" ></div>
                </div>
                </sec:authorize> --%>
                
                
                <c:if test="${requestHave eq true}">
                <sec:authorize url="configItem_detail_relatingRequest">
                <div title="<fmt:message key="title.historyRequest"/>" style="padding:2px; width: 100%;">         
                	<table id="historyCall"></table>
					<div id="historyCallPager" ></div>
                </div>
                </sec:authorize>
                </c:if>
                <c:if test="${changeHave eq true}">
                <sec:authorize url="configItem_detail_relatingChange">
                <div title="<fmt:message key="title.change.historyChange"/>" style="padding:2px; width: 100%;">         
                	<table id="historyChangeGrid"></table>
					<div id="historyChangePager" ></div>
                </div>
                </sec:authorize>
                </c:if>
                <c:if test="${problemHave eq true}">
                <sec:authorize url="configItem_detail_relatingProblem">
                <div title="<fmt:message key="title.problem.historyProblem" />" style="padding:2px; width: 100%;">         
                	<table id="historyProblemGrid"></table>
					<div id="historyProblemPager" ></div>
                </div>
                </sec:authorize>
                </c:if>
                
                <!-- 历史更新 -->
                <sec:authorize url="configItem_detail_historyUpdate">
				<div title='<fmt:message key="label.customReport.history"/>'>
					<form>
                	<div id="cust_hisdiv_id" class="hisdiv">
                	
						<table id="configureItemHistory" class="histable" style="width:100%" cellspacing="1">
							<thead>
							<tr>
								<th width="10%"><fmt:message key="common.id"/></th>
								<th width="20%"><fmt:message key="label.customReport.versions"/></th>
								<th width="15%"><fmt:message key="label.operator"/></th>
								<th width="20%"><fmt:message key="common.updateTime"/></th>
								<th width="20%"><fmt:message key="title.dashboard.noticeDetails"/></th>
								<th width="20%"><fmt:message key="label.updateVersion"/></th>
							</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
                	</div>
                	</form>
				</div>
			</sec:authorize>
			
			
			
			  <!-- 二维码  <a onclick="itsm.cim.configureItemInfoFormCustom.scanShow(${ciDetailDTO.ciId})" style="margin-left: 30px;" >显示</a> -->
                 <sec:authorize url="CONFIGITEM_DETAIL_TDCODE">
				<div title="<fmt:message key="label.ci.scan.tab"/>">
					
					<div style="background-color:#e4edfe;font-weight:bold;height: 26px;padding-top: 12px; margin-bottom:15px; padding-left: 10px;"><fmt:message key="label.ci.scan.title" /></div>
                	
                	 <a class="easyui-linkbutton l-btn" style="margin-left: 18px;" 
                	icon="icon-ok" onclick="itsm.cim.configureItemInfoFormCustom.scanCreate('${ciDetailDTO.cino}','${ciDetailDTO.ciId}')"><fmt:message key="label.ci.scan.create"/></a> 
                	
                	<a href="ciScanAction!findImage.action?ciScanDTO.ciId=${ciDetailDTO.ciId}" style="margin-left: 30px;"  target=_blank><fmt:message key="label.problem.attachmentDownload"/></a>
                	<br/>
                	<br/>
                	<div>
	                	<div style="float: left;border: solid 1px; ">
	                		<table width="328px;">
	                			<tr>
	                			<%-- 	<td>
	                					<table>
	                						<tr><td style="font-size: 15px;font-weight: 900"><fmt:message key="lable.ci.Use"/>: ${ciDetailDTO.ciname}</td></tr>
	                						<tr>
				                				<td style="font-size: 15px;font-weight: 900">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IP: <span id="scan_CIip"></span></td>
				                			</tr>
				                			<tr><td style="font-size: 15px;font-weight: 900"><fmt:message key="ci.use"/>: ${ciDetailDTO.userName}</td></tr>
	                					</table>
	                				
	                				<td> --%>
	                					<div id="cust_QRcode_id" style="height: 70px;width: 328px;overflow: hidden;"></div>
	                				
	                			</tr>
	                		</table>
	                	</div>
	                	
	                	
	                		<%-- <img id="input_QRcode_id" src="${pageContext.request.contextPath}/QRCodePath/img_${ciDetailDTO.cino}.png" onerror="itsm.cim.configureItemInfoFormCustom.imgClose()"/> --%>
	                		<!-- <img  src="./${pageContext.request.contextPath}/QRCodePath/img_hh.png"/> -->
	                	
                	</div>
				</div>
			</sec:authorize>
			<!-- 修改行为 -->
<!-- 			<div title='修改行为'> -->
<!-- 				<form> -->
<!--                	<div id="behavior_modification_id" class="hisdiv"> -->
               	
<!-- 					<table id="behaviormodification" class="histable" style="width:100%;" cellspacing="1"> -->
<!-- 						<thead> -->
<!-- 						<tr> -->
<%-- 							<th width="15%"><fmt:message key="common.id"/></th> --%>
<%-- 							<th width="15%"><fmt:message key="label.operator"/></th> --%>
<%-- 							<th width="20%"><fmt:message key="common.updateTime"/></th> --%>
<!-- 							<th width="20%">修改内容</th> -->
<!-- 						</tr> -->
<!-- 						</thead> -->
<!-- 						<tbody> -->
							
<!-- 						</tbody> -->
<!-- 					</table> -->
<!--                	</div> -->
<!--                	</form> -->
<!-- 			</div> -->
            </div>
		</div>	
	</div>
	<!-- 显示历史更新 -->
	 <div id="Cihistory" class="WSTUO-dialog" title="<fmt:message key="label.customReport.history.Detail"/>" style="width:580px;height:380px;">
		<div id="CihistoryInfo" class="histable_left" style="width:100%;" cellspacing="1">
		</div>
	</div>
	
	<!-- 显示修改行为更新 -->
	 <div id="behaviorModification" class="WSTUO-dialog" title="<fmt:message key="label.ci.updatecontent"/>" style="width:580px;height:380px;">
			<div class="hisdiv">
			<table id="behaviorModificationInfo" class="histable_left" style="width:100%;" cellspacing="1">
				<tbody >
					
				</tbody>
			</table>
			</div>
	</div>
	
	<!-- 添加配置项关联 -->
	<div id="add_ci_relevance_win" title="<fmt:message key="common.add"/>/<fmt:message key="common.edit"/>-<fmt:message key="ci.relatedCI"/>" class="WSTUO-dialog" style="width:420px;height:auto;">
	    <form>
	    <input type="hidden" name="ciRelevanceDTO.ciRelevanceId" id="ciRelevanceId" />
	    <input type="hidden" name="ciRelevanceDTO.relevanceId" id="relevanceId" />
	    <div class="lineTableBgDiv" style="width: 98%">
	    <table style="width:100%" class="lineTable" cellspacing="1">
	     	<tr>
	            <td><fmt:message key="ci.relatedCI"/></td>
	            <td>
	            <input type="hidden" name="ciRelevanceDTO.unCiRelevanceId" id="unCiRelevanceId" />	
	            <input name="ciRelevanceDTO.unCiRelevanceName" id="unCiRelevanceName" class="input choose" readonly /></td>
	        </tr>
	        <tr>	
				<td><fmt:message key="label.dc.ciRelationType" /></td>
				<td>
					<select id="relevance_ciRelationType" name="ciRelevanceDTO.ciRelationTypeNo" class="input"></select>
				</td>
			</tr>
	        <tr>
	            <td><fmt:message key="common.remark"/></td>
	            <td>
	            <input name="ciRelevanceDTO.relevanceDesc" id="relevanceDesc" class="input"  /></td>
	        </tr>
	      	<tr>
	            <td colspan="2" align="center"> 
		             <a  class="easyui-linkbutton" plain="true" icon="icon-save" id="link_add_ci_relevance_ok"><fmt:message key="common.save"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	            </td> 
	      	</tr>
	      	
	      	
	    </table>
	    </div>
	    </form>
	</div>
	
	
	<!-- 选择配置项 -->
	<div id="ci_select_win" class="WSTUO-dialog" title="<fmt:message key="common.select"/><fmt:message key="ci.ci"/>" style="width:650px;height:500px;padding:5px;">
	    	<div class="easyui-layout" style="width:630px;height:400px;">
	    		<div region="west" split="true" title="<fmt:message key="ci.ci"/><fmt:message key="common.category"/>" style="width:180px;padding:10px;">
	    			<div id="ci_select_tree"></div>
	    		</div>
	    		
	    		<div region="center" title="<fmt:message key="ci.ci"/>" style="width:400px;padding:4px;">
	    			<table id="ci_select_grid"></table>
	    			<div id="ci_select_pager"></div>
	    		</div>
	    	</div>
	</div>
	
	
	
	
	<div id="ci_select_Toolbar" style="display:none">
		<form>
		<input id="ci_select_toolbar_search" name="ciQueryDTO.ciname" />
		<a class="easyui-linkbutton" plain="true" icon="icon-search" onclick="selectQuery('ci_select_Toolbar','ci_select_grid')"><fmt:message key="common.search"/></a>
		</form>
	</div>
	
	
	<!-- 关联配置项树 -->
	<div id="configureItemInfo_ci_relevance_win" title="<fmt:message key="ci.ciRelevanceTreeView" />" class="WSTUO-dialog" style="width:200px;height:auto">
		<div id="configureItemInfo_ci_relevance_tree"></div>
	</div>
	
	<!-- 安装的软件工具栏 -->
	<div id="installSoftwareGridToolbar" style="display: none">
        <%-- <sec:authorize url="/pages/ciSoftwareAction!saveSoftware.action">
		<a class="easyui-linkbutton" plain="true" icon="icon-add" id="installSoftwareGrid_add_but" onclick="javascript:itsm.cim.configureItemInfoFormCustom.openAddConfigureItemSoftware()" title="<fmt:message key="common.add"/>"></a>
		</sec:authorize> --%>
        <sec:authorize url="/pages/ciSoftwareAction!deleteSoftware.action">
        <a class="easyui-linkbutton" plain="true" icon="icon-cancel" id="installSoftwareGrid_delete_but" onclick="javascript:itsm.cim.configureItemInfoFormCustom.deleteConfigureItemSoftware()" title="<fmt:message key="common.delete"/>"></a>
	    </sec:authorize>
    </div>
	<!-- 选择关联软件窗口 -->
	<div id="configureItemInfo_installSoftware_win" title="<fmt:message key="common.select" />-<fmt:message key="label_scan_software" />" class="WSTUO-dialog" style="width:650px;height:auto">
		<%-- 数据列表 --%>
		<table id="configureItemInfoSoftwareMainGrid" ></table>
		<div id="configureItemInfoSoftwareMainPager"></div>
		<div id="configureItemInfo_installSoftwareToolbar" style="display:none">
		&nbsp;&nbsp;<fmt:message key="label_software_name" />:<input id="search_software_name" type="text">
		&nbsp;&nbsp;<a class="easyui-linkbutton" plain="true" icon="icon-search" onclick="javascript:itsm.cim.configureItemInfoFormCustom.searchSoftware()"><fmt:message key="common.search" /></a>
	    &nbsp;&nbsp;<a class="easyui-linkbutton" plain="true" icon="icon-ok" onclick="javascript:itsm.cim.configureItemInfoFormCustom.addConfigureItemSoftware()"><fmt:message key="label.determine" /></a>
		</div>
	
	</div>
	<div id="ciPrefuse"  class="WSTUO-dialog" title='<fmt:message key="msg.tips"/>' style="width:260px;height:auto">
		
		<img alt="temp" src="${pageContext.request.contextPath}/images/icons/messager_info.gif">
			
	
		<br/>
		<p style="text-align: center;"> <a class="easyui-linkbutton" icon="icon-ok" onclick="javascript:itsm.cim.configureItemInfoFormCustom.ciPrefuseColse()"><fmt:message key="label.determine"/></a></p>
	</div>
	
	
		<%-- 自定义列设置窗口 --%>
	<div id="columnChooserWin_installSoftwareGrid"  class="WSTUO-dialog" title="<fmt:message key="label.set.column"/>" style="width:250px;height:300px;padding:1px;">
		<div id="columnChooserDiv_installSoftwareGrid" class="hisdiv">
			<table class="histable" style="width:100%" cellspacing="1">
				<thead>
				<tr>
					<td colspan="2">
						<a id="columnChooserTopBut_installSoftwareGrid" plain="true" class="easyui-linkbutton" icon="icon-ok"><fmt:message key="label.determine"/></a>
						<a id="columnChooserDefaultBut_installSoftwareGrid" plain="true" class="easyui-linkbutton" icon="icon-ok"><fmt:message key="label.dashboard.defautl.show"/></a>
						<span id="columnChooserStatus_installSoftwareGrid"></span>
					</td>
				</tr>
				
				<tr>
					<th style="padding-left:11px;"><input type="checkbox" id="colAllUnSelect_installSoftwareGrid"  onclick="colAllUnSelect('installSoftwareGrid')" /></th>
					<th><fmt:message key="label.colName" /></th>
				</tr>
				</thead>
				<tbody>
				</tbody>			
			</table>
		</div>
	</div>
	
<div id="ciCategory_div" style="display: none">
	<div class="field_options">
		<div class="label">
			<div class="fieldName" title="<fmt:message key="title.request.CICategory" />"><fmt:message key="title.request.CICategory" /></div>
		</div>
		<div class="field" id="ciCategory_value"></div>
	</div>
</div>