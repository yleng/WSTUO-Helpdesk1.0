<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/itsm/cim/leftMenu.js"></script>
<%--
<div class="musp"><fmt:message key="ci.equipmentManagement"/></div>

<div style="overflow:auto;padding:8px;padding-left:12px;line-height:22px">

<a id="deviceManage" onclick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="ci.equipmentManagement"/>','${pageContext.request.contextPath}/pages/snmp/networkManage.jsp?ip=192.168.75.192')"><fmt:message key="ci.equipmentManagement"/></a>
<br>

<!--DEMO  -->

<a id="deviceManage" onclick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="ci.equipmentManagement"/>-DEMOS','${pageContext.request.contextPath}/pages/snmp/networkManage_Demos.jsp')"><fmt:message key="ci.equipmentManagement"/>-DEMOS</a>
</div>
 --%>
<sec:authorize url="/pages/ciSoftwareAction!findSoftwarePager.action">
<div class="musp"><fmt:message key="label_scan_software"/></div>
<div style="padding:8px;padding-left:12px;line-height:22px">
<a onclick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label_scan_software" />','${pageContext.request.contextPath}/pages/itsm/cim/softwareMain.jsp')"><fmt:message key="label_scan_software" /></a>
</div>
</sec:authorize>

<sec:authorize url="/pages/itsopUser!findITSOPUserPager.action">
	<!-- 配置项统计 -->
	<c:if test="${versionType=='ITSOP'}">
	<div class="musp"><fmt:message key="title.customer.stat"/>
	<a href="javascript:itsm.cim.leftMenu.companyStat()">[<fmt:message key="common.refresh" />]</a>
	</div>
	<div id="companyConfigureItemDataCount" style="padding:5px; line-height:22px"></div>
	</c:if>
</sec:authorize>

<sec:authorize url="configItem_menu_configItemAssort">
<div class="musp"><fmt:message key="title.request.CICategory"/></div>
<div style="padding:8px;padding-left:12px;line-height:22px;overflow: auto;">
<div id="configureItemCategoryTree"></div>
</div>
</sec:authorize>
<script>
$(document).ready(function(){
	$('#companyConfigureItemDataCount').html('');
	endLoading();
});

</script>