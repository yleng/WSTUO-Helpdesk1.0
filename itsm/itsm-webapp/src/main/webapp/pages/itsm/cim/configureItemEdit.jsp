<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<%
    WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
	IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
	if(session.getAttribute("loginUserName")!=null){
	 	Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
			  "CofnigureItem_AssetOriginalValue_Res",
			  "CofnigureItem_Barcode_Res",
			  "CofnigureItem_Supplier_Res",
			  "CofnigureItem_PurchaseDate_Res",
			  "CofnigureItem_ArrivalDate_Res",
			  "CofnigureItem_PurchaseNo_Res",
			  "CofnigureItem_Owner_Res",
			  "CofnigureItem_LifeCycle_Res",
			  "CofnigureItem_WarrantyDate_Res",
			  "CofnigureItem_WarningDate_Res",
			  "CofnigureItem_SerialNumber_Res",
			  "CofnigureItem_Department_Res",
			  "CofnigureItem_Fimapping_Res",
			  "CofnigureItem_ExpiryDate_Res",
			  "CofnigureItem_LendTime_Res",
			  "CofnigureItem_OriginalUser_Res",
			  "CofnigureItem_RecycleTime_Res",
			  "CofnigureItem_PlannedRecycleTime_Res",
			  "CofnigureItem_UsePermission_Res"
			  });
	 	request.setAttribute("resAuth",resAuth);
	}
%>
<script type="text/javascript">
var kno="${ciDetailDTO.serviceDirDtos}";
var ci_edit_eavAttributet = '<fmt:message key="config.extendedInfo"/>';
var ci_edit_hardware = '<fmt:message key="label_hardware"/>';
</script>
<link href="${pageContext.request.contextPath}/scripts/jquery/uploadify/uploadify.css"  rel="stylesheet" type="text/css"/>  
<script src="${pageContext.request.contextPath}/scripts/itsm/cim/configureItemEdit.js?random=<%=new java.util.Date().getTime()%>"></script> 



<div class="loading" id="configureItemEdit_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>
	
<div id="configureItemEdit_panel" class="content" fit="true" border="none"  style="height: 100%">
	
	<div class="easyui-layout"  fit="true" id="configureItemEdit_layout">
		<!-- 新的面板 start-->
		
		<div region="north" border="false" class="northBar">
			<DIV align="left">
				<a id="itemEditSave" class="easyui-linkbutton" icon="icon-save" plain="true" title="<fmt:message key="common.save"/>"></a>
				<a class="easyui-linkbutton" plain="true" icon="icon-undo" style="margin-right:10px" onclick="itsm.cim.configureItemEdit.returnConfigureItemList()" title="<fmt:message key="common.returnToList" />"></a>
			</DIV>
		</div>
		
		<div region="west" split="true" title="<fmt:message key="common.basicInfo" />" style="width:450px">
		
		        <form id="configureItemEdit_form">
		        <input type="hidden" name="ciDto.ciId" id="editCiId" value="${ciDetailDTO.ciId}"/>
		         <div class="lineTableBgDiv">
		           
				<table style="width:100%" class="lineTable" cellspacing="1">
        		
        		<!-- 所属客户  -->
				<tr <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
					<td><fmt:message key="label.belongs.client" />&nbsp;<span style="color:red">*</span></td>
					<td>
						<input type="hidden" name="ciDto.companyNo" id="ci_edit_companyNo" value="${ciDetailDTO.companyNo}" />
						<input id="ci_edit_companyName" class="easyui-validatebox input" value="${ciDetailDTO.companyName}" required="true" readonly disabled />
					</td>
				</tr>
	            <tr>
	                <td style="width:40%"><fmt:message key="title.request.CICategory"/>&nbsp;<span style="color:red">*</span></td>
					<td style="width:60%">
					  <input type="hidden" name="ciDto.categoryNo" id="ci_categoryNo_edit" value="${ciDetailDTO.categoryNo}"/>
					  <input name="ciDto.categoryName" value="${ciDetailDTO.categoryName}" id="ci_categoryName_edit" class="easyui-validatebox input" required="true" 
					  onclick="itsm.cim.ciCategoryTree.autoUpdateEavAttrCiCategoryTree('configureItemEditCategory','configureItemEditCategoryTree','ci_categoryNo_edit','ci_categoryName_edit','ci_edit_eavAttributet','ci_edit_cino')" 
					  readonly style="cursor:pointer"/>
	                </td>
	             </tr><tr>
	                <td><fmt:message key="lable.ci.assetNo"/>&nbsp;<span style="color:red">*</span></td>
	                <td>
	                  <input type="hidden" id="ci_edit_cino_old" value="${ciDetailDTO.cino}" />
	                  <input name="ciDto.cino" id="ci_edit_cino" value="${ciDetailDTO.cino}" class="easyui-validatebox input"  validType="nullValueValid" required="true" validType="length[1,50]" />
	                </td>
	            </tr>
	            <tr>
	                <td><fmt:message key="title.asset.name"/>&nbsp;<span style="color:red">*</span></td>
	                <td>
	                <input name="ciDto.ciname" id="ciname_edit" value="${ciDetailDTO.ciname}" class="easyui-validatebox input"  validType="nullValueValid" required="true" validType="length[1,50]"/>
	                </td>
	           </tr>
	            <tr>    
	               <td><fmt:message key="label.dc.systemPlatform"/></td>
	                <td>
	                
	                 <select name="ciDto.systemPlatformId" id="ci_systemPlatform" value="${ciDetailDTO.systemPlatformId}"  class="easyui-validatebox input">
	                 	<option value="">-- <fmt:message key="common.pleaseSelect" /> --</option>
	                	<c:forEach items="${dictionaryItemsSystemPlatformList}" var="dc">
	                	<option value="${dc.dcode}" ${dc.dcode eq ciDetailDTO.systemPlatformId?'selected':''} >${dc.dname}</option>
	                	</c:forEach>
	                </select>
	                                           
	                </td>
	            </tr>
	           <tr>     
	                <td><fmt:message key="ci.productType"/></td>
	                <td>
	                <input name="ciDto.model" id="model"  class="input" value="${ciDetailDTO.model}"/>
	                </td>        
	            </tr>
	            <c:if test="${resAuth['CofnigureItem_SerialNumber_Res'] }">
	            <tr>
	                <td><fmt:message key="ci.serialNumber"/></td>
	                <td>
	                <input name="ciDto.serialNumber" id="serialNumber" class="input" value="${ciDetailDTO.serialNumber}"/>
	                </td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_Barcode_Res'] }">
	            <tr>    
	                <td><fmt:message key="ci.barcode"/></td>
	                <td>
	                <input name="ciDto.barcode" id="barcode" class="input" value="${ciDetailDTO.barcode}" />
	                </td>
	            </tr>
	            </c:if>
	            <tr>
	                <td><fmt:message key="common.status"/></td>
	                <td>
	 
	                <select name="ciDto.statusId" id="ci_statusAdd" class="input">
	                	<option value="0">-- <fmt:message key="common.pleaseSelect" /> --</option>
	                	<c:forEach items="${dictionaryItemsStatusList}" var="dc">
	                
	                	<option value="${dc.dcode}" ${dc.dcode eq ciDetailDTO.statusId?'selected':''}>${dc.dname}</option>
	                	</c:forEach>
	                </select>
	                
	                </td>
	                
	                
	            </tr>
	            
	            <tr>    
	                <td><fmt:message key="ci.brands"/></td>
	                <td>
	                
	                 <select name="ciDto.brandId" id="ci_brandNameAdd" value="${ciDetailDTO.brandId}" class="input">
	                 	<option value="0">-- <fmt:message key="common.pleaseSelect" /> --</option>
	                	<c:forEach items="${dictionaryItemsBrandList}" var="dc">
	                	<option value="${dc.dcode}" ${dc.dcode eq ciDetailDTO.brandId?'selected':''} >${dc.dname}</option>
	                	</c:forEach>
	                </select>
	                                           
	                </td>
	            </tr>
	            <c:if test="${resAuth['CofnigureItem_Supplier_Res'] }">
	            <tr>
	                <td><fmt:message key="label.supplier"/></td>
	                <td>

	                <select name="ciDto.providerId" id="ci_providerAdd" class="input">
	                	<option value="0">-- <fmt:message key="common.pleaseSelect" /> --</option>
	                	<c:forEach items="${dictionaryItemsProviderList}" var="dc">
	                	<option value="${dc.dcode}" ${dc.dcode eq ciDetailDTO.providerId?'selected':''}>${dc.dname}</option>
	                	</c:forEach>
	                </select>
	
	                </td>
	            </tr>
	            </c:if>
	            <tr>    
	                <td><fmt:message key="ci.location"/></td>
	                <td>
	            	<input type=hidden name="ciDto.locId" value="${ciDetailDTO.locId}" id="ci_locid_edit">
           			<input id="ci_loc_edit" value="${ciDetailDTO.loc}"  class="easyui-validatebox choose" readonly/>
	                </td>
	            </tr>
	            <c:if test="${resAuth['CofnigureItem_PurchaseDate_Res'] }">
	            <tr>
	                <td><fmt:message key="ci.purchaseDate"/></td>
	                <td>
		                <input name="ciDto.buyDate" id="configureItem_edit_buyDate" class="choose" value="<fmt:formatDate type="date" value="${ciDetailDTO.buyDate}"  pattern="yyyy-MM-dd" />" readonly />
	                </td>
	             </tr>
	             </c:if>
	             <c:if test="${resAuth['CofnigureItem_ArrivalDate_Res'] }">
	             <tr>   
	                <td><fmt:message key="ci.arrivalDate"/></td>
	                <td>
		                <input name="ciDto.arrivalDate" id="configureItem_edit_arrivalDate" class="easyui-validatebox choose" validType="DateComparisonCI['configureItem_edit_buyDate']" value="<fmt:formatDate type="date" value="${ciDetailDTO.arrivalDate}"  pattern="yyyy-MM-dd" />" readonly/>
	                </td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_WarningDate_Res'] }">
	            <tr>     
	                 <td><fmt:message key="ci.warningDate"/></td>
	                <td>
		                 <input name="ciDto.warningDate" id="configureItem_edit_warningDate" class=" choose" value="<fmt:formatDate type="date" value="${ciDetailDTO.warningDate}"  pattern="yyyy-MM-dd" />" readonly/>
	                </td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_PurchaseNo_Res'] }">
	            <tr>
	                <td><fmt:message key="ci.purchaseNo"/></td>
	                <td>
	                <input name="ciDto.poNo" id="poNo" class="input" value="${ciDetailDTO.poNo}" />
	                </td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_AssetOriginalValue_Res'] }">
	            <tr>
	            	<td><fmt:message key="ci.assetOriginalValue"/></td>
	            	<td>
	            	<input name="ciDto.assetsOriginalValue" value="<fmt:formatNumber  value="${ciDetailDTO.assetsOriginalValue}" pattern="##00.#"/>" class="easyui-numberbox input" validType="length[1,9]" min="0"  /></td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_Fimapping_Res'] }">
	            <tr>
	            	<td><fmt:message key="ci.FIMapping"/></td>
	            	<td>
	            	<input name="ciDto.financeCorrespond" type="radio" value="true" ${ciDetailDTO.financeCorrespond eq true?'checked':''} /><fmt:message key="tool.affiche.yes" />
	            	<input name="ciDto.financeCorrespond" type="radio" value="false"  ${ciDetailDTO.financeCorrespond eq false?'checked':''}/><fmt:message key="tool.affiche.no" />
	            	</td>
	            </tr>
				</c:if>
	            
	            <!-- 多少年折旧率为0 -->
	            <tr>
	            	<td><fmt:message key="ci.depreciationIsZeroYears"/></td>
	            	<td>
	            	<input name="ciDto.depreciationIsZeroYears" style="width: 80%" id="depreciationIsZeroYears" class="easyui-validatebox easyui-numberbox input" min="0" validType="length[1,13]" max="999999999" value="${ciDetailDTO.depreciationIsZeroYears > 0 ? ciDetailDTO.depreciationIsZeroYears:''}"/>
	            	<fmt:message key="title.report.year"/>
	            	</td>
	            </tr>
	            <tr><td colspan="2">&nbsp;<br><br><br><br></td></tr>
          </table>
          </div>
         </form>
		
		
		</div>
			
			
		<div region="center" id="configureItemEdit_tabs" title="<fmt:message key="common.detailInfo" />" style="padding-bottom: 15px;">
			
			<!-- 扩展信息 start -->
			<div class="easyui-tabs" fit="true" border="false" id="editCim_tabs">
			
				<!-- 其他信息  -->
				<div title="<fmt:message key="ci.ciOtherInfo" />" >
					<div class="lineTableBgDiv">
					<form>
						<table style="width:100%" class="lineTable" cellspacing="1">	
							<c:if test="${resAuth['CofnigureItem_Department_Res'] }">
							<tr>
				            	<td><fmt:message key="ci.department" /></td>
				            	<td><input name="ciDto.department" style="cursor:pointer;" class="input" id="configureItem_edit_department"  value="${ciDetailDTO.department}" readonly/>
				            	</td>
				            </tr>
				            </c:if>
				            
				            <tr>
				            	<td><fmt:message key="label.ci.businessEntity" /></td>
				            	<td><input name="ciDto.workNumber" value="${ciDetailDTO.workNumber}" class="input"/></td>
				            </tr>
				            <tr>
				            	<td><fmt:message key="label.ci.project" /></td>
				            	<td><input name="ciDto.project" value="${ciDetailDTO.project}" class="input"/></td>
				            </tr>
				            <tr>
				            	<td><fmt:message key="ci.productProvider" /></td>
				            	<td><input name="ciDto.sourceUnits" value="${ciDetailDTO.sourceUnits}" class="input"/></td>
				            </tr>
				            <c:if test="${resAuth['CofnigureItem_LifeCycle_Res'] }">
							<tr>
				                <td><fmt:message key="ci.lifeCycle"/></td>
				                <td>
				                	<input name="ciDto.lifeCycle" id="lifeCycle"  maxlength="9" class="easyui-validatebox easyui-numberbox input"  value="<c:if test="${ciDetailDTO.lifeCycle!='0'}">${ciDetailDTO.lifeCycle}</c:if>" min="0"/>
				                </td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_WarrantyDate_Res'] }">
				            <tr>    
				                <td><fmt:message key="ci.warrantyDate"/></td>
				                <td>
			
				                <input name="ciDto.warranty" id="warranty" maxlength="9" class="easyui-validatebox easyui-numberbox input"   value="<c:if test="${ciDetailDTO.warranty!='0'}">${ciDetailDTO.warranty}</c:if>" min="0"/>
			
				                </td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_ExpiryDate_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.expiryDate" /></td>
				            	<td><input id="wasteTime_edit" name="ciDto.wasteTime"  class=" choose"  value="<fmt:formatDate type="date" value="${ciDetailDTO.wasteTime}"  pattern="yyyy-MM-dd" />" readonly/></td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_LendTime_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.lendTime" /></td>
				            	<td><input name="ciDto.borrowedTime" id="borrowedTime_edit" value="<fmt:formatDate type="date" value="${ciDetailDTO.borrowedTime}"  pattern="yyyy-MM-dd" />"  class=" choose"  readonly/></td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_PlannedRecycleTime_Res'] }">
				             <tr>
				            	<td><fmt:message key="ci.plannedRecycleTime" /></td>
				            	<td><input name="ciDto.expectedRecoverTime" id="expectedRecoverTime_edit" value="<fmt:formatDate type="date" value="${ciDetailDTO.expectedRecoverTime}"  pattern="yyyy-MM-dd" />"  class=" choose"/></td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_RecycleTime_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.recycleTime" /></td>
				            	<td><input name="ciDto.recoverTime" id="recoverTime_edit" class="easyui-validatebox choose" value="<fmt:formatDate type="date" value="${ciDetailDTO.recoverTime}"  pattern="yyyy-MM-dd" />" readonly/></td>
				            </tr>			           
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_UsePermission_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.usePermission" /></td>
				            	<td><input name="ciDto.usePermissions" value="${ciDetailDTO.usePermissions}" class="input"/></td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_OriginalUser_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.originalUser" /></td>
				            	<td><input name="ciDto.originalUser" id="ci_edit_originalUser" style="width: 80%;"  class="input" value="${ciDetailDTO.originalUser}" readonly/>&nbsp;&nbsp;
				            	  &nbsp;&nbsp;
				            	  <input type="hidden" name="ciDto.originalUserId"  id="ci_edit_originalUserId" style="width: 80%;"  class="input" value="${ciDetailDTO.originalUserId}" />
				                <a id="ci_edit_originalUser_select"><img style="vertical-align:middle;" src="../skin/default/images/user.png"></a>
				            	<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('ci_edit_originalUser','ci_edit_originalUserId')" title="<fmt:message key="label.request.clear" />"></a>
				            	</td>
				            </tr>	
				            </c:if>
				            <tr>
				                <td><fmt:message key="ci.use"/></td>
				                <td>
				                <input name="ciDto.userName" id="ci_edit_useName" style="width: 80%;" class="input"  value="${ciDetailDTO.userName}" readonly/>&nbsp;&nbsp;
				                &nbsp;&nbsp;
				                <input type="hidden" name="ciDto.userNameId" id="ci_edit_useNameId" style="width: 80%;" class="input"  value="${ciDetailDTO.userNameId}"/>
				                <a id="ci_edit_useName_select"><img style="vertical-align:middle;" src="../skin/default/images/user.png"></a>
				            	<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('ci_edit_useName','ci_edit_useNameId')" title="<fmt:message key="label.request.clear" />"></a>
				            	</td>
				            </tr>
				            <c:if test="${resAuth['CofnigureItem_Owner_Res'] }">
				            <tr>    
				                <td><fmt:message key="ci.owner"/></td>
				                <td><input name="ciDto.owner" id="ci_edit_owner" style="width: 80%;" class="input"  value="${ciDetailDTO.owner}" readonly/>&nbsp;&nbsp;
				                 &nbsp;&nbsp;
				                 <input type="hidden" name="ciDto.ownerId" id="ci_edit_ownerId" style="width: 80%;" class="input"  value="${ciDetailDTO.ownerId}" />
				                <a id="ci_edit_owner_select"><img style="vertical-align:middle;" src="../skin/default/images/user.png"></a>
				            	<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('ci_edit_owner','ci_edit_ownerId')" title="<fmt:message key="label.request.clear" />"></a>
				            	</td> 
				            </tr>
				            </c:if>
				            <tr>
				            	<td><fmt:message key="label.role.roleMark" /></td>
				            	<td><input name="ciDto.CDI" value="${ciDetailDTO.CDI}"  class="input"/></td>
				            </tr>
						</table>		
					</form>
				</div>	
				</div>
				<div title="<fmt:message key="config.extendedInfo"/>">
					<form id="">
						<div class="hisdiv" id="ci_edit_eavAttributet" >
						
						<table style="width:100%" class="histable" cellspacing="1">
						 <c:set var="oldCiEditGroupName"></c:set>
							<s:if test="attributeList!=null">
							<s:iterator value="attributeList" status="attrs" >
							<c:if test="${attrGroupName != oldCiEditGroupName}">
							<c:if test="${ attrs.index eq 0}">
							<thead>
							</c:if>
								<tr>
									<th colspan="2" style="text-align:left;"><s:property value="attrGroupName"/></th>
									<c:set var="oldCiEditGroupName" value="${attrGroupName}"></c:set>
								</tr>
							<c:if test="${ attrs.index eq 0}">	
							</thead>
							</c:if>
							</c:if>
	              			<tr>
							    <td style="text-align: left;width: 25%">
							    	<s:property value="attrAsName"/><c:if test="${!attrIsNull}"><span style=color:red>&nbsp;&nbsp;*</span></c:if>
							    </td>
							    <td style="text-align: left;">
							    	<s:if test="%{attrType=='String'}" >
							    		<input style="width:98%" name="ciDto.attrVals['${attrName}']" value="${ciDetailDTO.attrVals[attrName]}" <c:if test="${!attrIsNull}">class="easyui-validatebox input" required="<s:property value='!attrIsNull'/>"</c:if>  />
							    	</s:if>
							    	<s:if test="%{attrType=='Date'}" >
							    		<input style="width:98%" id="ci_edit_eavAttributet_Date" name="ciDto.attrVals['${attrName}']" value="${ciDetailDTO.attrVals[attrName]}" class="choose" readonly <c:if test="${!attrIsNull}">class="easyui-validatebox input" required="<s:property value="!attrIsNull"/>" </c:if> />
							    	</s:if>
							    	
							    	<s:if test="%{attrType=='Integer'}" >
							    		<input style="width:98%" name="ciDto.attrVals['${attrName}']" value="${ciDetailDTO.attrVals[attrName]}" class="easyui-numberbox" min="0" maxlength="9" <c:if test="${!attrIsNull}">class="easyui-validatebox input" required="<s:property value="!attrIsNull"/>" </c:if>/>
							    	</s:if>
							    	
							    	<s:if test="%{attrType=='Double'}" >
							    		<input style="width:98%" name="ciDto.attrVals['${attrName}']" value="${ciDetailDTO.attrVals[attrName]}" class="easyui-numberbox" precision="3" <c:if test="${!attrIsNull}">class="easyui-validatebox" required="<s:property value="!attrIsNull"/> </c:if>"/>
							    	</s:if>
							    	<s:if test="%{attrType=='Lob'}" >
							    		<textarea style="width:98%" name="ciDto.attrVals['${attrName}']" <c:if test="${!attrIsNull}">class="easyui-validatebox" required="<s:property value="!attrIsNull"/>" </c:if> >${ciDetailDTO.attrVals[attrName]}</textarea>
							    	</s:if>
							    	<s:if test="%{attrType=='DataDictionaray'}" >
								    	<select name="ciDto.attrVals['${attrName}']"  id="edit_dataDictionaray_select" style='text-align:left;width:75%' <c:if test="${!attrIsNull}">class="easyui-validatebox"  required="<s:property value="!attrIsNull"/>" </c:if>>
									    	<option value="">---<fmt:message key="common.pleaseSelect"/>----</option>
								    		<s:iterator value="listMap[attrName]" id="subm">
									    		<option value="<s:property value="#subm.key"/>" <c:if test="${ciDetailDTO.attrVals[attrName] eq subm.key}">selected="selected"</c:if>><s:property value="#subm.value"/></option>
									    		</s:iterator>
								    	</select>
							    	</s:if>
							    	<s:if test="%{attrType=='Radio'}" >
							    		<input name="ciDto.attrVals['${attrName}']" type="hidden" attrType="${attrType }" id="${attrName}">
							    		<s:iterator value="attrItemName" id="item" status="ind">
											<label><input name="${attrName}" type="Radio" value="${item}"  
												<c:if test="${ciDetailDTO.attrVals[attrName] eq item}">checked="checked"</c:if>
												<c:if test="${!attrIsNull && ind.last  }">validType="radioAndchekboxValid['ci_edit_eavAttributet','${attrName}']" class="easyui-validatebox"  required="<s:property value="!attrIsNull"/>" </c:if>>${item}</label>
								    	</s:iterator>
							    	</s:if>
							    	<s:if test="%{attrType=='Checkbox'}" >
							    		<input name="ciDto.attrVals['${attrName}']" type="hidden" attrType="${attrType }" id="${attrName}">
							    		<s:iterator value="attrItemName" id="item" status="ind">
											<label><input name="${attrName}" type="Checkbox" value="${item}"  
												<c:if test="${fn:contains(ciDetailDTO.attrVals[attrName] , item)}">checked="checked"</c:if>
												<c:if test="${!attrIsNull && ind.last  }">validType="radioAndchekboxValid['ci_edit_eavAttributet','${attrName}']" class="easyui-validatebox"  required="<s:property value="!attrIsNull"/>" </c:if>>${item}</label>
								    	</s:iterator>
							    	</s:if>
								</td>
						       </tr>
							</s:iterator>
							</s:if>
							<s:if test="attributeList==null">
								<tr><td colspan="2" style="color:red"><b><fmt:message key="label.ciNotExtendedInfo" /></b></td></tr>
							</s:if>
							
                		 </table>
                		</div>
                	</form>
                	
				</div>
				<!-- 软件配置参数 start -->
				<div title="<fmt:message key="label.ci.softSettingParam"/>">
					
					<form>
					<div class="hisdiv" id="ci_add_softSetting">
					<input type="hidden" name="ciDto.softSetId" value="${ciDetailDTO.softSetId}"/>
					<table style="width:100%" class="histable" cellspacing="1">
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSetParam"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softSetingParam">${ciDetailDTO.softSetingParam}</textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingAm"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softConfigureAm">${ciDetailDTO.softConfigureAm}</textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark1"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark1">${ciDetailDTO.softRemark1}</textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark2"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark2">${ciDetailDTO.softRemark2}</textarea></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark3"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark3">${ciDetailDTO.softRemark3}</textarea> </td>
						</tr>
             		</table>
               		</div>
					<div class="lineTableBgDiv">
					<table style="width:100%" class="lineTable" cellspacing="1">
                		<tr>
			              <td>
			             <div style="color:#ff0000;line-height:28px"><fmt:message key="msg.attachment.maxsize" /></div>
			               <div class="diyLinkbutton">
                            
                            <div style="float: left;cursor: pointer;">
                                <input type="file"  name="filedata" id="edit_uploadSoftAttachments">
    			               </div>
                            <div style="float: left;margin-left: 15px;">
				 		    <a class="easyui-linkbutton" icon="icon-upload" href="javascript:if($('#editConfigureItem_SoftfileQueue').html()!='')$('#edit_uploadSoftAttachments').uploadifyUpload();else msgShow(i18n['msg_add_attachments'],'show');" ><fmt:message key="label.attachment.upload" /></a>
	             		   </div>
    	             		   <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('edit_uploadedSoftAttachments','ciDto.softAids')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
			               <br>
				 		   
	             		   <input style=" width: 100%" type="hidden" name="ciDto.softAttachmentStr" id="editConfigureItem_Softattachments"/>
			                 </div>
                             <div style="clear: both;"></div>
                             <div id="edit_uploadedSoftAttachments" style="line-height:25px;color:#555"></div>
                            <div id="editConfigureItem_SoftfileQueue"></div>
                          </td>
			        	</tr>
                	</table>
                	</div>
				</form>	
				<%-- <form id="uploadForm_CiEditSoft" method="post" enctype="multipart/form-data">
					  <table border="0" cellspacing="1" class="fu_list">
			              <tr height="40px">
				        	<td colspan="2">
				        	<table border="0" cellspacing="0"><tr><td >
					       			<a href="javascript:void(0);" class="files" id="idFile_CiEditSoft"></a>
					       		</td><td>
							        <a class="easyui-linkbutton" style="float:left" plain="true"  icon="icon-ok" id="idBtnupload_CiEditSoft"><fmt:message key="label.startUpload" /></a>
							         &nbsp;&nbsp;&nbsp;
									<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_CiEditSoft"><fmt:message key="label.allcancel" /></a>
									 &nbsp;&nbsp;&nbsp;
									<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('edit_uploadedSoftAttachments','ciDto.softAids')" class="easyui-linkbutton" icon="icon-ok" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
								</td></tr></table>
							</td>
				      		</tr>
				          <tbody id="idFileList_CiEditSoft">
				          </tbody>
						</table>
					</form> --%>
				</div>
				<%--硬件 --%>
                <div id="configureItemEdit_hardware" title="<fmt:message key="label_hardware" />" >
                <form>
                	<input type="hidden" name="hardwareDTOs.hardwareId" value="${hardwareDTOs.hardwareId}" />
                	<s:if test="hardwareDTOs.bios_manufacturer=='H3C' || hardwareDTOs.bios_manufacturer=='Cisco' || hardwareDTOs.bios_manufacturer=='Huawei'">
                	<fieldset>
		 				<legend>${hardwareDTOs.bios_manufacturer}</legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="label.role.roleName" />:
		 						<input class="input" name="hardwareDTOs.computerSystem_name" value="${hardwareDTOs.computerSystem_name}" /></td>
		 						<td><fmt:message key="ci.productType" />:
		 						<input class="input" name="hardwareDTOs.computerSystem_model" value="${hardwareDTOs.computerSystem_model}" /></td>
		 					</tr>
		 					<tr>
		 						<td>IP:
		 						<input class="input" name="hardwareDTOs.netWork_ip" value="${hardwareDTOs.netWork_ip}" /></td>
		 						<td><fmt:message key="ci.brands" />:
		 						<input class="input" name="hardwareDTOs.bios_manufacturer" value="${hardwareDTOs.bios_manufacturer}" /></td>
		 					</tr>
		 					<tr>
		 					<td><fmt:message key="label.snmp.systemOID" />:
		 						<input class="input" name="hardwareDTOs.operatingSystem_serialNumber"  value="${hardwareDTOs.operatingSystem_serialNumber}" /></td>
			 					<td><fmt:message key="config_operatingSystem_lastBootUpTime" />:
			 					<input class="input" id="operatingSystem_lastBootUpTime_edit" readonly name="hardwareDTOs.operatingSystem_lastBootUpTime"  value="${hardwareDTOs.operatingSystem_lastBootUpTime}" /></td>
		 					</tr>
		 					<tr>
		 						<%-- <td><fmt:message key="config_operatingSystem_totalVisibleMemorySize" />:
		 						<input class="input" name="hardwareDTOs.operatingSystem_totalVisibleMemorySize" value="${hardwareDTOs.operatingSystem_totalVisibleMemorySize}" /></td>
		 						 --%>
		 						 <td colspan="2"><fmt:message key="config.systemVersion" />:
			 					<input class="input" name="hardwareDTOs.operatingSystem_version" value="${hardwareDTOs.operatingSystem_version}"/>
		 						<input type="hidden" name="hardwareDTOs.other"  value="${hardwareDTOs.other}" />
		 						<%-- <input type="hidden" name="hardwareDTOs.bios_manufacturer"  value="${hardwareDTOs.bios_manufacturer}" /> --%></td>
			 				</tr>
			 			</table>
		 			</fieldset>
		 			</s:if>
		 			<s:else>
                	<fieldset> 
		 				<legend><fmt:message key="label_computer_system" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="config.computerName" />:<br/>
		 						<input class="input" name="hardwareDTOs.computerSystem_name" value="${hardwareDTOs.computerSystem_name}" /></td>
		 						<td><fmt:message key="config_computerSystem_model" />:<br/>
		 						<input class="input" name="hardwareDTOs.computerSystem_model" value="${hardwareDTOs.computerSystem_model}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_computerSystem_domain" />:<br/>
		 						<input class="input" name="hardwareDTOs.computerSystem_domain" value="${hardwareDTOs.computerSystem_domain}" /></td>
		 						<td><fmt:message key="common.Username" />:<br/>
		 						<input class="input" name="hardwareDTOs.computerSystem_userName" value="${hardwareDTOs.computerSystem_userName}" /></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config.systemName" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_serialNumber" value="${hardwareDTOs.operatingSystem_serialNumber}" /></td>
		 						<td><fmt:message key="label.snmp.systemName" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_caption" value="${hardwareDTOs.operatingSystem_caption}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config.systemVersion" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_version" value="${hardwareDTOs.operatingSystem_version}"/></td>
		 						<td><fmt:message key="config_operatingSystem_osArchitecture" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_osArchitecture"  value="${hardwareDTOs.operatingSystem_osArchitecture}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_operatingSystem_csdVersion" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_csdVersion"  value="${hardwareDTOs.operatingSystem_csdVersion}" /></td>
		 						<td><fmt:message key="config_operatingSystem_installDate" />:<br/>
		 						<input class="input" id="operatingSystem_installDate_edit" readonly name="hardwareDTOs.operatingSystem_installDate"  value="${hardwareDTOs.operatingSystem_installDate}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_operatingSystem_lastBootUpTime" />:<br/>
		 						<input class="input" id="operatingSystem_lastBootUpTime_edit" readonly name="hardwareDTOs.operatingSystem_lastBootUpTime"  value="${hardwareDTOs.operatingSystem_lastBootUpTime}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config_memory" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="config_operatingSystem_totalVisibleMemorySize" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_totalVisibleMemorySize" value="${hardwareDTOs.operatingSystem_totalVisibleMemorySize}" /></td>
		 						<td><fmt:message key="config_operatingSystem_totalVirtualMemorySize" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_totalVirtualMemorySize"  value="${hardwareDTOs.operatingSystem_totalVirtualMemorySize}" /></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			<fieldset>
		 				<legend>CPU</legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="config_processor_name" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_name" value="${hardwareDTOs.processor_name}" /></td>
		 						<td><fmt:message key="config_processor_maxClockSpeed" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_maxClockSpeed"  value="${hardwareDTOs.processor_maxClockSpeed}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_processor_l2CacheSize" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_l2CacheSize"  value="${hardwareDTOs.processor_l2CacheSize}"  /></td>
		 						<td><fmt:message key="config_processor_l3CacheSize" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_l3CacheSize" value="${hardwareDTOs.processor_l3CacheSize}"  /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_processor_level" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_level" value="${hardwareDTOs.processor_level}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			<fieldset>
		 				<legend>BIOS</legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.bios_serialNumber" value="${hardwareDTOs.bios_serialNumber}" /></td>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.bios_name" value="${hardwareDTOs.bios_name}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_manufacturer" />:<br/>
		 						<input class="input" name="hardwareDTOs.bios_manufacturer" value="${hardwareDTOs.bios_manufacturer}" /></td>
		 						<td><fmt:message key="config_bios_version" />:<br/>
		 						<input class="input" name="hardwareDTOs.bios_version" value="${hardwareDTOs.bios_version}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_bios_releaseDate" />:<br/>
		 						<input class="input" id="bios_releaseDate_edit" readonly name="hardwareDTOs.bios_releaseDate" value="${hardwareDTOs.bios_releaseDate}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config_baseBoard_name" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.baseBoard_name" value="${hardwareDTOs.baseBoard_name}" /></td>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.baseBoard_serialNumber" value="${hardwareDTOs.baseBoard_serialNumber}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config.mainboard" />:<br/>
		 						<input class="input" name="hardwareDTOs.baseBoard_manufacturer" value="${hardwareDTOs.baseBoard_manufacturer}" /></td>
		 						<td></td>
		 					</tr>
		 				
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config_netWork_info" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td>IP:<br/>
		 						<input class="input easyui-validatebox" validType="ip" name="hardwareDTOs.netWork_ip" value="${hardwareDTOs.netWork_ip}" /></td>
		 						<td>MAC:<br/>
		 						<input class="input" name="hardwareDTOs.netWork_mac" value="${hardwareDTOs.netWork_mac}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.netWork_name" value="${hardwareDTOs.netWork_name}" /></td>
		 						<td>DHCP:<br/>
		 						<input class="input" name="hardwareDTOs.netWork_dhcp" value="${hardwareDTOs.netWork_dhcp}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_dhcp_server" />:<br/>
		 						<input class="input" name="hardwareDTOs.netWork_dhcpServer" value="${hardwareDTOs.netWork_dhcpServer}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			<fieldset>
		 				<legend><fmt:message key="config_desktopMonitor_name" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_name" value="${hardwareDTOs.desktopMonitor_name}" /></td>
		 						<td><fmt:message key="config_desktopMonitor_height" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_screenHeight" value="${hardwareDTOs.desktopMonitor_screenHeight}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_desktopMonitor_width" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_screenWidth" value="${hardwareDTOs.desktopMonitor_screenWidth}" /></td>
		 						<td><fmt:message key="config_manufacturer" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_monitorManufacturer" value="${hardwareDTOs.desktopMonitor_monitorManufacturer}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_serialNumber" value="${hardwareDTOs.desktopMonitor_serialNumber}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config_pointingDevice" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td width="120px"><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.pointingDevice_Name" value="${hardwareDTOs.pointingDevice_Name}" /></td>
		 						<td width="120px"><fmt:message key="label.user.description" />:<br/>
		 						<input class="input" name="hardwareDTOs.pointingDevice_description" value="${hardwareDTOs.pointingDevice_description}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.pointingDevice_serialNumber" value="${hardwareDTOs.pointingDevice_serialNumber}" /></td>
		 						<td><fmt:message key="config_manufacturer" />:<br/>
		 						<input class="input" name="hardwareDTOs.pointingDevice_manufacturer" value="${hardwareDTOs.pointingDevice_manufacturer}" /></td>
		 					</tr>
		 				
		 				</table>
		 			</fieldset>
		 			<fieldset>
		 				<legend><fmt:message key="config_keyboard" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_Name" value="${hardwareDTOs.keyboard_Name}" /></td>
		 						<td><fmt:message key="label.user.description" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_description" value="${hardwareDTOs.keyboard_description}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_keyboard_numberOfFunctionKeys" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_numberOfFunctionKeys" value="${hardwareDTOs.keyboard_numberOfFunctionKeys}" /></td>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_serialNumber" value="${hardwareDTOs.keyboard_serialNumber}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_manufacturer" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_manufacturer" value="${hardwareDTOs.keyboard_manufacturer}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			</s:else>
     	  			</form> 
                </div>
                <%---硬件END --%>
				
				<div title='<fmt:message key="common.attachment" />' style="padding: 3px;">
							<div class="lineTableBgDiv" id="configureItemEdit_effect_atta_div">
							<form>
								<input type="hidden" id="configureItemEdit_effect_attachmentStr"/>
								<table width="100%" cellspacing="1" class="lineTable">
									
									<tr>
										<td>
										<div id="configureItemEdit_effect_success_attachment" style="line-height:25px;color:#555;display: none"></div>
										<div class="hisdiv" id="show_configureItemEdit_effectAttachment">
											<table style="width:100%" class="histable" cellspacing="1">
												<thead>
													<tr>
														<th><fmt:message key="common.id" /></th>
														<th><fmt:message key="label.problem.attachmentName" /></th>
														<th><fmt:message key="label.problem.attachmentUrl" /></th>
														<th><fmt:message key="label.sla.operation" /></th>
													</tr>
												</thead>
												<tbody></tbody>
						             			</table>
						               	</div>
										</td>
									</tr>
									<tr>
										<td>
                                            <div class="diyLinkbutton">
                                            <div style="float: left;cursor: pointer;">
											     <input type="file"  name="filedata" id="configureItemEdit_effect_file">
											</div>
                                            <div style="float: left;margin-left: 15px;">
												<a class="easyui-linkbutton" icon="icon-upload" href="javascript:itsm.cim.configureItemEdit.configureitemEdit_uploadifyUpload()" ><fmt:message key="label.attachment.upload" /></a>
								            </div>
								            <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('ConfigureItem','edit')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
                                             </div>
                                            <div style="clear: both;"></div>
                                            <div id="configureItemEdit_effect_fileQueue"></div>
										</td>
									</tr> 
								</table>
							</form>
							<%-- <form id="uploadForm_CiEdit" method="post" enctype="multipart/form-data">
							  <table border="0" cellspacing="1" class="fu_list">
					              <tr height="40px">
						        	<td colspan="2">
						        	<table border="0" cellspacing="0"><tr><td >
							       			<a href="javascript:void(0);" class="files" id="idFile_CiEdit"></a>
							       		</td><td>
									        <a class="easyui-linkbutton" style="float:left" plain="true"  icon="icon-ok" id="idBtnupload_CiEdit"><fmt:message key="label.startUpload" /></a>
									         &nbsp;&nbsp;&nbsp;
											<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_CiEdit"><fmt:message key="label.allcancel" /></a>
											 &nbsp;&nbsp;&nbsp;
											<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('ConfigureItem','edit')" class="easyui-linkbutton" icon="icon-ok" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
										</td></tr></table>
									</td>
						      		</tr>
						          <tbody id="idFileList_CiEdit">
						          </tbody>
								</table>
							</form> --%>
						</div>
				</div>
				<div id="ciEditEffectServiceShow" title='<fmt:message key="label.ci.ciServiceDir" />'>
				
							<form id="edit_ci_relatedService">

								<div class="hisdiv">
									<table class="histable" id="edit_ci_serviceDirectory" style="width:100%" cellspacing="1">
									<thead>
									<tr>
										<td colspan="3" style="text-align:left">
											<a id="edit_ci_service_add" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
										</td>
									</tr>
									<tr height="20px">
										<%-- <th align="center"><fmt:message key="common.id"/></th> --%>
										<th align="center"><fmt:message key="lable.ci.ciServiceDirName"/> </th>
										<th align="center"><fmt:message key="label.rule.operation"/> </th>
									</tr>
									</thead>
									<!-- 服务目录显示 -->
									<tbody id="edit_ci_serviceDirectory_tbody">
										<c:forEach var="data" items="${ciDetailDTO.serviceDirDtos}">
											<tr id="edit_ci_${data.eventId}"><td>${data.eventName}</td><td><a onclick="common.knowledge.knowledgeTree.serviceRm(${data.eventId})">删除</a><input type=hidden name=ciDto.serviceDirectoryNos value="${data.eventId}"></td></tr>
										</c:forEach>
									</tbody>
									
									</table>
								</div>
							</form>
				</div>
	    	</div>
			<!-- 扩展信息 end -->
		</div>
	
	</div>
<!-- 新的面板 end-->
</div>

    <!-- 选择配置项分类 -->
    <div id="configureItemEditCategory" class="WSTUO-dialog" title="<fmt:message key="title.request.CICategory"/>" style="width:400px;height:400px;padding:3px">
		<div id="configureItemEditCategoryTree"></div>
	</div> 

<div id="CIselectServiceDirDivEdit" title="<fmt:message key="label.sla.selectServiceDir"/>" class="WSTUO-dialog" style="width:250px;height:400px; padding:10px; line-height:20px;" >
	
	<div id="CISelectServiceDirTreeDivEdit"></div>
	<br/>
	<div style="border:#99bbe8 1px solid;padding:5px;text-align:center">
	<a class="easyui-linkbutton" icon="icon-ok" plain="true" id="CISelectServiceDir_getSelectedNodesEdit"><fmt:message key="label.sla.confirmChoose"/></a>
	</div>
</div>

