<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ include file="../../language.jsp" %>

<%--坐席--%>
<sec:authorize url="/pages/portal!incomingCallsNumber.action">
<div id="call_seat_div">
		<div class="hisdiv">
			<fmt:message key="label.snmp.connectionState" />:<span id="messageTd" style="background-color:#FFCCFF;height:20" ><fmt:message key="label.conn.failure" /></span>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<fmt:message key="label.incomingCallsNumber" /><input id="callNumber" />
			<a onclick="itsm.portal.voiceCard.callShowTest()">[<fmt:message key="label.call.show.test" />]</a>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<a onclick="itsm.portal.voiceCard.chStatus('','vo_id=getChStatus&type=2&_secs_=lcall')">[<fmt:message key="label.all.channel" />]</a>
		</div>
		<div class="hisdiv">
			<table id="chunnelStatus" class="histable" style="width:100%" cellspacing="1">
				<thead>	
					<tr>
						<th><fmt:message key="label.chanel" /></th>
						<th><fmt:message key="label.type" /></th>
						<th><fmt:message key="common.state" /></th>
						<th><fmt:message key="label.steps" /></th>
						<th><fmt:message key="label.user.phone" /></th>
						<th><fmt:message key="label.callCenterWorkerNo" /></th>
						<th><fmt:message key="label.orgSettings.startTime" /></th>
						<th>IP</th>
					</tr>
				</thead>
				<tbody></tbody>	
			</table>
		</div>	
</div>
</sec:authorize>
<script>
var messageTd = document.getElementById("messageTd");
if(messageTd_msg!=''){
 	messageTd.innerHTML=messageTd_msg;
 	messageTd.style.color="#0000CC";
 }
	basics.includes.loadVoiceCardIncludesFile();//加载弹屏
</script>
