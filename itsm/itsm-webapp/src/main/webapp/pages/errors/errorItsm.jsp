<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../language.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><fmt:message key="i18n.mainTitle"/></title>
<style>
.lineTableBgDiv{
	background:#eee;
	margin:3px;
}
</style>
</head>

<body>

<div class="lineTableBgDiv" >
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="line-height:28px;font-size:12px;color:#FF0000">
				<fmt:message key="lable.errors.license"/><br/>
				
				<fmt:message key="label.request.phone" />：4008318185<br/>
				<fmt:message key="label.user.qqOrMsn" />：<a href="http://wpa.qq.com/msgrd?v=1&Uin=1477274212&Exe=QQ&Site=56135&menu=no" target="_bank">1477274212</a><br/>
				<fmt:message key="label.user.email" />：<a href="mailto:support@WSTUO.com" target="_bank">support@WSTUO.com</a><br/>
				<%-- <fmt:message key="Trial.Website" />：<a href="http://www.WSTUO.com" target="_bank">http://www.WSTUO.com</a><br/>
				<fmt:message key="label.company.website" />：<a href="http://www.bangzhutai.com" target="_bank">http://www.bangzhutai.com</a> --%>
			</td>
		</tr>
	</table>
</div>

</body>

</html>