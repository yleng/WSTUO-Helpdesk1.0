<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.config.basicConfig.service.ICurrencyService"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.common.tools.service.IPagesSetSerice"%>
<%@page import="com.wstuo.common.tools.entity.PagesSet"%>
<%@page import="com.wstuo.common.config.moduleManage.dao.IModuleManageDAO"%>
<%@page import="com.wstuo.common.config.moduleManage.entity.ModuleManage"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setTimeZone value="${clientTimeZone}" scope="session"/>

<%
String url="login.jsp";

 if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
} 
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
//获取当前浏览器语言做为默认的语言版本start
String browserLanguage = (String)session.getAttribute("browserLanguage");
if(browserLanguage==null){
	request.setAttribute("defaultLanguage","zh_CN");
}else{
	request.setAttribute("defaultLanguage",browserLanguage);
}
//获取当前浏览器语言做为默认的语言版本end

ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
IPagesSetSerice systemPage = (IPagesSetSerice)ac.getBean("pagesSetSerice");
PagesSet pagesSet=systemPage.showPagesSet();
request.setAttribute("pagesSet",pagesSet);


//默认货币单位
ICurrencyService currencyService = (ICurrencyService)ac.getBean("currencyService");
request.setAttribute("currency_sign",currencyService.findDefaultCurrency().getSign());
String version="1.0";
request.setAttribute("version",version);

IModuleManageDAO moduleManageDAO=(IModuleManageDAO)ac.getBean("moduleManageDAO");
ModuleManage m=moduleManageDAO.findUniqueBy("moduleName", "request");
request.setAttribute("moduleManage",m);
%>

<c:set var="currency_sign" scope="session" value="${currency_sign}" />

<fmt:setLocale value="${empty cookie['language'].value?defaultLanguage:cookie['language'].value}"/>
<fmt:setBundle basename="i18n.itsmbest"/>
<c:set var="lang" value="${empty cookie['language'].value?defaultLanguage:cookie['language'].value}" />
<!--<c:set var="versionType" value="${empty cookie['versionType'].value?'':cookie['versionType'].value}" />-->
<c:set var="versionType" scope="session" value="ITSOP"/>
<c:set var="currentTime" value="<%=new java.util.Date().getTime()%>" />
<c:if test="${param.callNumber ne ''}">
<c:set var="callNumber" value="${empty cookie['callNumber'].value?'':cookie['callNumber'].value}" />
<c:set var="seatNo" value="${empty cookie['seatNo'].value?'':cookie['seatNo'].value}" />
<c:set var="keyLoggers" value="${empty cookie['keyLoggers'].value?'':cookie['keyLoggers'].value}" />
<c:set var="other1" value="${empty cookie['other1'].value?'':cookie['other1'].value}" />
<c:set var="other2" value="${empty cookie['other2'].value?'':cookie['other2'].value}" />
<%-- systemVersion：OEM版本控制,值非WSTUO为OEM版本(提供给我们合作伙伴的); --%>
<c:set var="systemVersion" scope="session" value="WSTUO" />
<%-- passwordType：密码安全级别；值为Simple,密码安全级别为简单,其他为高(大小写，8位以上...) --%>
<c:set var="passwordType" scope="session" value="Simple" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<META http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" >
<c:if test="${empty pagesSet.paName}">
	<title>企业一体化解决方案</title>
</c:if>
<c:if test="${!empty pagesSet.paName}">
<title>${pagesSet.paName}</title>
</c:if>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="wstuo,企业一体化解决方案">
<meta name="author" content="WSTUO">
<%@ include file="wstuo/includes/includes_categorys.jsp" %>      
<link rel="stylesheet" href="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
<link rel="stylesheet" href="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.structure.min.css" />
<link rel="stylesheet" href="../css/easyui.css">
<!-- The styles -->
<link id="bs-css" href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<link href="../css/charisma-app.css" rel="stylesheet">
<link href='../bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
<link href='../bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
<link href='../bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='../bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
<link href='../css/jquery.noty.css' rel='stylesheet'>
<link href='../css/noty_theme_default.css' rel='stylesheet'>
<link href='../css/elfinder.min.css' rel='stylesheet'>
<link href='../css/elfinder.theme.css' rel='stylesheet'>
<link href='../css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='../css/animate.min.css' rel='stylesheet'>
<link href='../js/jquery/jqgrid/css/ui.jqgrid-bootstrap.css' rel='stylesheet'>
<link href='../js/jquery/jqgrid/css/ui.jqgrid.css' rel='stylesheet'>
<link href='../js/jquery/jqgrid/css/ui.jqgrid-bootstrap-ui.css' rel='stylesheet'>
<link href='../css/css.css' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="../css/upload/default.css">
<link href="../css/upload/fileinput.css" media="all" rel="stylesheet" type="text/css" />	
<!-- jQuery -->
<script src="../bower_components/jquery/jquery.min.js"></script>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="../images/logo2.png">

<!-- external javascript -->

<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="../js/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='../bower_components/moment/min/moment.min.js'></script><!-- 
<script src='../bower_components/fullcalendar/dist/fullcalendar.min.js'></script> -->
<!-- data table plugin -->
<script src='../js/js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="../bower_components/chosen/chosen.jquery.min.js"></script>
<!-- notification plugin -->
<script src="../js/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="../bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin --><!-- 
<script src="../bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script> -->
<!-- star rating plugin -->
<script src="../js/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="../js/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="../js/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin --><!-- 
<script src="../js/js/jquery.uploadify-3.1.min.js"></script> -->
<!-- history.js for cross-browser state change on ajax -->
<script src="../js/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="../js/js/charisma.js"></script>


<script src="../js/My97DatePicker/WdatePicker.js"></script>
<script src="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>

<script src="../js/ckeditor/ckeditor.js"></script>
<!-- <script src="../scripts/jquery/jquery.floatingmessage.js"></script> -->

<script src='../dwr/engine.js'></script>  
<script src='../dwr/util.js'></script>
<script src='../dwr/interface/getuserpagedwr.js'></script>
<script src='../js/itsm/dwr/userAssgin.js'></script>
<script type="text/javascript" src="../js/basics/package.js"></script>
<script type="text/javascript" src="../js/basics/import.js"></script>

<script src="../js/i18n/i18n_${lang}.js"></script>
<script src="../js/jquery/jqgrid/js/i18n/grid.locale-cn.js"></script>
<script src="../js/jquery/jqgrid/js/jquery.jqGrid.min.js"></script>
<script src="../js/jquery/jstree/js/lib/jquery.cookie.js"></script>
<script src="../js/jquery/jstree/js/lib/jquery.hotkeys.js"></script>
<script src="../js/jquery/jstree/js/jquery.jstree.js"></script>
<script src="../js/jquery/jstree/js/rewritejstree.js"></script>

<script src="../js/jquery/jquery.validate.min.js"></script>
<script src="../js/jquery/messages_zh.js"></script>

<script src="../js/jquery/jquery-json.js"></script> 
<script type="text/javascript" src="../js/basics/easyui_common.js"></script>
<script type="text/javascript" src="../js/basics/jquery_common.js"></script>
<script type="text/javascript" src="../js/basics/jqgrid_common.js"></script>
<script type="text/javascript" src="../js/basics/browserProp.js"></script>
<script type="text/javascript" src="../js/jquery/ajaxfileupload.js"></script>
<script type="text/javascript" src="../js/basics/ie6.js"></script>
<script type="text/javascript" src="../js/basics/autocomplete.js"></script>
<script type="text/javascript" src="../js//basics/columnChooser.js"></script>
<script type="text/javascript" src="../js/wstuo/user/userPwdUpdate.js"></script>
<script type="text/javascript" src="../js/basics/index.js"></script>
<script type="text/javascript">

var loginID="loginID";
var isProblemHave='${applicationScope.problemHave}';//问题模块是否存在
var isChangeHave='${applicationScope.changeHave}';//变更模块是否存在
var isReleaseHave='${applicationScope.releaseHave}';//发布模块是否存在
var isRequestHave='${applicationScope.requestHave}';//请求模块是否存在
var isCIHave='${applicationScope.cimHave}';//配置项 模块是否存在

var category_num=50;//分类条数
var langBrowser = '${lang}';
</script>

<script>var lang="${lang}";var isAllRequest_Res=false;var allRequest_Res;</script>
<sec:authorize url="AllRequest_Res">
<script>isAllRequest_Res=true</script>
</sec:authorize>
<script><!--
$.ajaxSetup({
	contentType: "application/x-www-form-urlencoded; charset=utf-8"
});
//刷新按钮加载标示
var load_shuaxin_flag=false;
var messageSize=0;//控制标题信息条数
var eventType = "${sessionScope.eventType}";
var pageType = "${sessionScope.pageType}";
var eventID = "${sessionScope.eventId}";
var language='${lang}';//当前语言版本
var fullName="${sessionScope.fullName}";
var userId = "${sessionScope.userId}";
var userName="${sessionScope.loginUserName}";//当前登录用户
var index_userId="${sessionScope.userId}";//当前登录用户id
var companyNo='${sessionScope.companyNo}';//当前用户所在公司No
var orgNo='${sessionScope.orgNo}';//当前用户所在机构No
var companyName='${sessionScope.companyName}';//当前用户所在公司名称
var topCompanyNo='${sessionScope.topCompanyNo}';//最上层公司
var versionType='${sessionScope.versionType}';//系统版本类型
var userRoleCode='${sessionScope.roleCode }';//系统版本类型
var systemVersion = '${sessionScope.systemVersion }';//系统版本
var currency_sign = '${sessionScope.currency_sign}';
var endUserStyle='${sessionScope.endUserStyle}';
var tableModule = "";
var messageTd_msg="";
var msg="";
var hideCompany=true;//是否要隐藏所属公司字段
var knownErrors;
var onhelp=false;
var knowledge_fullSearchFlag;
var isITSOPUser=false;
if(versionType=='ITSOP')
	hideCompany=false;
if(versionType=='ITSOP' && companyNo==topCompanyNo)
	isITSOPUser=true;

var guideShare=""; 
var guideShareId=""; 

var reqRenovate=true;
var common_url=$.cookie('common_url') == null ? '' : $.cookie('common_url');
//公司信息国际化
var junp_company='<fmt:message key="title.security.companyInfo"/>';
//默认页面
/* var default_page_resources='<fmt:message key="label.tools.workloadStatusStatistics"/>';
var default_page_tools='<fmt:message key="common.myTask"/>';
var default_page_report='<fmt:message key="title.report.strtusCount" />';
var default_page_basicset='<fmt:message key="title.security.companyInfo"/>';
var default_page_basicset='<fmt:message key="title.security.companyInfo"/>';
var default_page_release='<fmt:message key="title.mainTab.release.manage"/>';
var default_page_availability='<fmt:message key="title.mainTab.availabilityBase"/>';

var label_visit_rate='<fmt:message key="visit_rate_month_x_year" />';
 */
/**浏览器窗口大小改变事件*/
$(window).resize(function(){
	$('#ajax-content .ui-jqgrid').each(function(i,n){
        var id = $(n).attr('id').replace(/gbox_/g,'');
		setGridWidth('#'+id,15);
    });
});

//弹屏函数,函数名不能改
function popCustWin(req){
	itsm.portal.voiceCard.openCallWindow(req);
}
//实时显示连接小灵呼后台的状态,函数名不能改
function connectStatus(status){
//以下内容可以修改掉,以你自己的方式去显示
if(status.indexOf('sendComplete7')!=-1)
	messageTd_msg=i18n.label_conn_server_success;
else if(status.indexOf('reconnect')!=-1)
	messageTd_msg=i18n.label_re_conn_server;
else if(status=='6')
	messageTd_msg=i18n.label_conn_server;
else if(status=='9')
	messageTd_msg=i18n.label_conn_server_failure;
else 
	messageTd_msg=i18n.label_conn_server_status+"="+status+"";
 
}
/////////DWR JS Start//////
//页面初始化  
function init() { 
 	//dwr.engine.setActiveReverseAjax(true); // 激活反转 重要   
 	getuserpagedwr.setScriptSessionFlag(userId);
  	register();
}   
function onPageLoad(){
	getuserpagedwr.onPageLoad(userId);
}
//DWR 服务器异常处理
dwr.engine._errorHandler = function(message, ex) {
	//dwr.engine._debug("Error: " + ex.name + ", " + ex.message, true);
}; 
/////////DWR JS End//////

--></script>
<c:set var="groupId" scope="session" value="Group_${orgNo}"/>
<style type="text/css">
	#header{height:85px}
	#leftside, #container, #splitBar, #splitBarProxy{top:90px}
</style>
</head>
<body id="index_body"  onload="dwr.engine.setActiveReverseAjax(true);dwr.engine.setNotifyServerOnPageUnload(true);onPageLoad();">
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">导航切换 </span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.jsp"> <img id="index_logo" alt="WSTUO" src="../images/logo2.png" class="hidden-xs"/>
                <span id="index_Name">维思拓导航</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs">${sessionScope.fullName}</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="javascript:basics.index.refreshContent();">刷新</a></li>
                    <li><a href="#" onclick="wstuo.user.userPwdUpdate.Open_PwdUpdate()">密码修改</a></li>
    
                    <li><a href="logout.jsp">退出</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> 皮肤</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> 经典 </a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> 天蓝</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> 简洁</a></li>
                    <!-- <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li> -->
                </ul>
            </div>
            <!-- theme selector ends -->

            <ul class="collapse navbar-collapse nav navbar-nav top-menu" id="top-menu">
            	<li class="active"><a class="ajax-link" href='view!findAllViewAndUserView.action?queryDTO.customType=1&queryDTO.userName=${sessionScope.loginUserName }' menuUrl='sidebar/sidebar_index.jsp'><i class="glyphicon glyphicon-home"></i> 我的面板</a></li>
                <li><a class="ajax-link" href="request/requestMain.jsp" menuUrl="sidebar/sidebar_request.jsp"><i class="glyphicon glyphicon-tags"></i> ${moduleManage.title }</a></li>
                <!-- <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-th-large"></i> 配置项</a></li> -->
                <li><a class="ajax-link" href="wstuo/knowledge/knowledgeMain.jsp" menuUrl='sidebar/sidebar_knowledge.jsp'><i class="glyphicon glyphicon-book"></i> 知识库</a></li>
                <sec:authorize url="/pages/slaContractManage!find.action"><li><a class="ajax-link" href="wstuo/slaMge/slaMain.jsp"><i class="glyphicon glyphicon-flag"></i> 服务协议</a></li></sec:authorize>
                <sec:authorize url="REPORT_MAIN"><li><a class="ajax-link" href="wstuo/report/requestCatagory.jsp" menuUrl='sidebar/sidebar_report.jsp'><i class="glyphicon glyphicon-globe"></i> 报表</a></li></sec:authorize>
                <li><a class="ajax-link" href='wstuo/baseData/soft_setting.jsp' menuUrl='sidebar/sidebar_setting.jsp'><i class="glyphicon glyphicon-cog"></i> 系统设置</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-plus"></i> 添加 <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a class="ajax-link" href="request/addRequest.jsp">请求工单</a></li>
                        <li><a class="ajax-link" href="wstuo/knowledge/addKnowledge.jsp">知识库</a></li>
                       <!-- <li><a class="ajax-link" href="#">配置项</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li> -->
                    </ul>
                </li>
                <li>
                    <form class="navbar-search pull-left" id="fullsearch_form" onsubmit="return basics.index.fullsearch()">
                       <input placeholder="Search" data-toggle="dropdown" class="search-query form-control col-md-10" id="fullsearch"  type="text">
	                    <ul class="dropdown-menu" id="fullsearch_down" style="left:2">
	                       <li><a data-value="request" href="#"><i class="whitespace"></i> 请求 </a></li>
	                       <li><a data-value="knowledge" href="#"><i class="whitespace"></i> 知识库</a></li>
	                    </ul></div>
                    </form>
                </li>
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked top-menu">
                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu" id="main-menu">
				        <!-- left menu starts -->
				         <%@ include file="sidebar/sidebar_index.jsp" %>  
				        <!-- left menu ends -->
					</ul>
                    <!-- <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label> -->
                </div>
            </div>
        </div>
<!--         <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript> -->

        <div id="ajax-content" class="col-lg-10 col-sm-10">
          
		    <!-- content ends -->
	    </div>
	  
	</div><!--/fluid-row-->

    <hr>


    <footer class="row">
        <p class="col-md-9 col-xs-12 copyright">&copy; <a href="http://wstuo.com" target="_blank" >维思拓</a> 2015 - 2016</p>

        <p class="col-md-3 col-sm-3 col-xs-12 powered-by"><a
                href="http://wstuo.com" title="永久用于个人学习或企业免费使用！请勿用于商业用途，合作请联系我们"  target="_blank" data-placement="left" data-toggle="tooltip" class="btn btn-success">WSTUO<sup>TM</sup></a></p>
    </footer>

</div><!--/.fluid-container-->

<div style="height: 0px;overflow: hidden;width: 0px;">
<%--加载提示 --%>
<%@ include file="includes/includes_loading.jsp"%>
<%--显示操作进程 --%>
<%@ include file="includes/includes_process.jsp"%>
<%--系统错误提示框--%>
<div id="systemErrorWin" title="<fmt:message key="msg.tips" />" style="width:300px;height:155px;">
	<div style="padding:3px;" id="systemErrorInfo" ></div>
</div>
<%--信息提示框--%>
<div class="modal fade" id="alert_modal" tabindex="-1" role="dialog" aria-labelledby="alert_modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3 id="modal_title">Settings</h3>
                </div>
                <div class="modal-body" id="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal" id="modal_cancel">取消</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal" id="modal_confirm">确定</a>
                </div>
            </div>
        </div>
    </div>
    
<%--用户密码修改--%> 
<div id="pwdUpdate" class="WSTUO-dialog" title="<fmt:message key="title.password_changed"/>" style="height:auto;padding: 0px;">
	<form id="edit_user_password_form" >
		<table width="100%" class="lineTable" cellspacing="1">
			<tr>
				<td style="width:30%">&nbsp;&nbsp;<fmt:message key="label.user.loginName" /></td>
				<td style="width:70%">${loginUserName}
					<input type="hidden"  name="editUserPasswordDTO.loginName" value="${loginUserName}" />
				</td>
			</tr>
			<tr>
				<td >&nbsp;&nbsp;<fmt:message key="label.old.password" /></td>
				<td><input type="password" id="oldPassword" name="editUserPasswordDTO.oldPassword" class="form-control" style="width:220px" required="true"/></td>
			</tr>
			<tr>
				<td >&nbsp;&nbsp;<fmt:message key="label.new.password" /></td>
				<td><input type="password" id="newPassword" name="editUserPasswordDTO.newPassword"class="form-control" style="width:220px" <c:if test="${passwordType ne 'Simple'}">validType="passwordsecurity"</c:if>  required="true" /> </td>
			</tr>
			<tr>
				<td >&nbsp;&nbsp;<fmt:message key="label.user.comfirmLoginPassword" /></td>
				<td><input type="password" id="repeatPassword" class="form-control" style="width:220px" required="true"/></td>
			</tr>
			
			<tr>
				<td colspan="2" >
				<span id="link_eidt_user_password"  class="btn btn-primary btn-sm" style="margin-right: 15px;margin-top: 8px;margin-bottom: 10px;"><fmt:message key="common.update"/></span>
				</td>
			</tr>
			
		</table>
	</form>
</div>
    
    
<%-- 临时 --%>
<div id="temp_html"></div>
<%-- 知识库 --%>
<div id="knowledge_html"></div>
<%-- 面板数据详细信息 --%>
<div id="dashboardDataInfo_html"></div>
<%-- 请求动作页面 --%>
<div id="requestAction_html"></div>
<%-- 添加编辑任务 --%>
<div id="eventTask_html"></div>
<%-- 人工成本 --%>
<div id="eventTimeCost_html"></div>
<%-- 外包客户选择 --%>
<div id="selectCustomer_html"></div>
<%-- 选择关联配置项窗口（多选）--%>
<div id="selectCI_html"></div>
<%-- 选择用户窗口（单选）--%>
<div id="selectUser_html"></div>
<%-- 导入数据窗口--%>
<div id="importCsv_html"></div>
<%-- TCode窗口--%>
<div id="tCode_html"></div>
<%-- 常用分类 窗口--%>
<div id="category_html"></div>
<%-- 报表 窗口--%>
<div id="reportView_html"></div>
<%-- 自定义查询过滤器 窗口--%>
<div id="customFilter_html"></div>
<%-- 选择关联请求窗口 --%>
<div id="relatedRequest_html"></div>
<%-- 选择关联问题窗口 --%>
<div id="relatedProblem_html"></div>
<%-- 选择附件窗口 --%>
<div id="chooseAttachment_html"></div>
<%-- 邮件模板编辑窗口 --%>
<div id="editnotice_html"></div>
<%-- 快捷页面窗口 --%>
<div id="welcome_html"></div>
<%-- 弹屏页面窗口 --%>
<div id="voiceCard_html"></div>
<%-- 导出格式窗口 --%>
<div id="exportInfo_html"></div>
<%-- 扩展属性分钟窗口 --%>
<div id="attributeGroup_html"></div>
<%-- 选择关联变更窗口 --%>
<div id="relatedChange_html"></div>
<%-- 选择技术组窗口 --%>
<div id="selectTechnicalGroup_html"></div>
<%-- 流程公用窗口 --%>
<div id="flowCommonWin_html"></div>
<%-- 邮件新增窗口 --%>
<div id="addnotice_html"></div>
<%-- ACL --%>
<div id="aclWin_html"></div>
<div id="popover"></div>
</div>
<script type="text/javascript">
$(function() {

});
</script>
  <script src="../js/upload/fileinput.js" type="text/javascript"></script>
  <script src="../js/upload/fileinput_locale_zh.js" type="text/javascript"></script>
  <script src="../js/basics/initUploader.js" type="text/javascript"></script>
</body>

</html>