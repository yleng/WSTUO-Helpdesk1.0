<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- 加载数据 -->
<div id="loading_window" class="WSTUO-dialog" style="width:200px;height:60px;overflow:hidden;">
	<div style="height:60px;padding-top:5px;padding-left:10px">
		<img src="../images/img/ajax-loaders/ajax-loader-4.gif"  style="vertical-align:middle"/>&nbsp;&nbsp;&nbsp;<fmt:message key="label.loading"/>
	</div>
</div>
