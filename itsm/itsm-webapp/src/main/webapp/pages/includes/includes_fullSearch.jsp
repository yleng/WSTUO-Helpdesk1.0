<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="index_fullSearch_window" class="WSTUO-dialog" title="<fmt:message key="label.fullsearch.fullsearch" />" style="width:360px;height:auto;">

	<div style="height:250px" id="index_fullSearch_height"></div>
	
	<div class="lineTableBgDiv" style="display:none" id="index_fullSearch_table">
			<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
			<td>
			<c:if test="${requestHave eq true}">
			<sec:authorize url="/pages/request!findRequestPager.action">
			<input type="radio" value="request" name="fullSearch_type" checked><fmt:message key="title.mainTab.request" />
			</sec:authorize>
			</c:if>
			<c:if test="${problemHave eq true}">
			<sec:authorize url="/pages/problem!findProblemPager.action">
				<sec:authorize url="/pages/problem!findProblemPager.action">
					<input type="radio" value="problem" name="fullSearch_type"><fmt:message key="title.mainTab.problem" />
				</sec:authorize>
			</sec:authorize>
			</c:if>
			<c:if test="${changeHave eq true}">
			<sec:authorize url="/pages/change!findChangePager.action">
				<input type="radio" value="change" name="fullSearch_type"><fmt:message key="title.mainTab.change" />
			</sec:authorize>
			</c:if>
			<sec:authorize url="KNOWLEGEINFO_VIEWKNOWLEGE">
			<input type="radio" value="knowledge" name="fullSearch_type"><fmt:message key="knowledge.knowledge" />
			</sec:authorize>
			</td>
			</tr>
			
			<tr>
			<td><input class="input" id="fullSearchKeyWord"></td>
			</tr>
			
			<tr>
			<td><a class="easyui-linkbutton" id="fullSearchBtn" icon="icon-search"><fmt:message key="common.search" /></a></td>
			</tr>
			</table>
	</div>
	
</div>
