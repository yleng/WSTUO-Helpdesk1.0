<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String url="login.jsp";

 if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
} 
 %>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>WSTUO 企业一体化解决方案</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detlogin.htmlection" content="telephone=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<meta name="description" content="wstuo,企业一体化解决方案">
	<meta name="author" content="WSTUO">
	<link rel="icon" type="image/png" href="../images/logo2.png">
	<link rel="stylesheet" href="assets/css/amazeui.min.css"/>
	<link rel="stylesheet" href="assets/css/app.css">
</head>
<body>
	<header data-am-widget="header" class="am-header am-header-default">
	    <h1 class="am-header-title">密码修改 </h1>  
	</header>
	<input type="hidden" value="${knowledgeDto.kid}" id="knowledge_kid" >
	<!-- Menu -->
	<nav data-am-widget="menu" class="am-menu am-menu-offcanvas1" data-am-menu-offcanvas>
		<a href="javascript:history.go(-1);" class="am-menu-left" >
	    	<i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-chevron-left"></i>
	  	</a>
  		<a href="javascript: void(0)" class="am-menu-toggle">
	    	<i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-bars"></i>
	  	</a>
	  	<div class="am-offcanvas">
		    <div class="am-offcanvas-bar">
		    	<ul class="am-menu-nav sm-block-grid-1">      
			        <li>
			          <a href="user!findUserDetail_wap.action?userName=${sessionScope.loginUserName}">个人信息</a>        
			        </li>
			        <li >
			          <a href="pwdUpdate.jsp">密码修改</a>       
			        </li>  
			        <li>
			          <a href="index.jsp">返回首页</a>       
			        </li>   
			        <li>
			          <a href="logout.jsp">注&nbsp;&nbsp;销</a>       
			        </li>
			      </ul>
		    </div>
	  	</div>
	</nav>
	
<%--用户密码修改--%> 
<div class="am-u-sm-11 am-u-sm-centered" style="width: 98%;">
	<form id="edit_user_password_form" class="am-form">
		<div class="am-g am-margin-top">
        	<div class="am-text-right" style="width: 25%;float: left;">登录账号&nbsp;&nbsp;&nbsp;&nbsp;</div>
            <div class="am-u-end col-end" style="width: 75%;float: left;">
              ${loginUserName}
              <input type="hidden"  name="editUserPasswordDTO.loginName" value="${loginUserName}" />
            </div>
		</div>
		<div class="am-g am-margin-top">
        	<div class="am-text-right" style="width: 25%;float: left;">原始密码&nbsp;&nbsp;&nbsp;&nbsp;</div>
            <div class="am-u-end col-end" style="width: 75%;float: left;">
       			<input type="password" id="oldPassword" name="editUserPasswordDTO.oldPassword" class="am-input-sm" style="width:98%;" required="true"/>
            </div>
		</div>
		<div class="am-g am-margin-top">
        	<div class="am-text-right" style="width: 25%;float: left;">新&nbsp;&nbsp;密&nbsp;&nbsp;码&nbsp;&nbsp;&nbsp;&nbsp;</div>
            <div class="am-u-end col-end" style="width: 75%;float: left;">
       			<input type="password" id="newPassword" name="editUserPasswordDTO.newPassword"class="am-input-sm" style="width:98%;" required="true" placeholder="输入6以上的密码"/>
            </div>
		</div>
		<div class="am-g am-margin-top">
        	<div class="am-text-right" style="width: 25%;float: left;">确认密码&nbsp;&nbsp;&nbsp;&nbsp;</div>
            <div class="am-u-end col-end" style="width: 75%;float: left;">          
              <input type="password" id="repeatPassword" class="am-input-sm" style="width:98%;" required="true" placeholder="输入6位以上的密码"/>
            </div>
		</div>
		<div class="am-g am-margin-top" style="text-align: center;">
        	<button type="button" class="am-btn am-btn-primary am-btn-sm" id="link_eidt_user_password" style="width: 100px;">修改</button>
		</div>
		
	</form>
</div>	
	
<%@ include file="include_tip.jsp"%>                   	
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>
<script src="../js/i18n/i18n_zh_CN.js"></script>
<script src="js/common.js"></script>

<script type="text/javascript">

$("#link_eidt_user_password").click(function(){
	
	var oldPwd=$('#oldPassword').val();
	var newPwd=$('#newPassword').val();
	var repeatPwd=$('#repeatPassword').val();
	
	var parent=/^[A-Za-z]+$/;
	
	var content=$("#reply_content").val();
	
	if(!oldPwd){
		msgAlert(i18n.oldPasswordError);
		return;
	}
	if(newPwd =="" || newPwd.length < 6){
		msgAlert(i18n.validate_password);
		return;
	}
	if(repeatPwd =="" || repeatPwd !=newPwd ){
		msgAlert("两次密码输入不一致");
		return;
	}
	var _param = $('#edit_user_password_form').serialize();
	$.post('user!resetPassword.action',_param,function(data){
		if(data){
			$('#oldPassword,#newPassword,#repeatPassword').val('');
	
			msgAlert(i18n['editSuccess']);

			window.location.href="logout.jsp"; 
			
		}else{
			msgAlert(i18n['oldPasswordError'],'info');
		}
	});	
});
</script>

</body>
</html>
