<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.common.tools.service.IPagesSetSerice"%>
<%@page import="com.wstuo.common.tools.entity.PagesSet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
<%
String url="login.jsp";

if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
}
 ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
 IPagesSetSerice systemPage = (IPagesSetSerice)ac.getBean("pagesSetSerice");
 PagesSet pagesSet=systemPage.showPagesSet();
 request.setAttribute("pagesSet",pagesSet);
 %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="description" content="wstuo,企业一体化解决方案">
<meta name="author" content="WSTUO">
<c:if test="${empty pagesSet.paName}">
	<title>企业一体化解决方案</title>
</c:if>
<c:if test="${!empty pagesSet.paName}">
<title>${pagesSet.paName}</title>
</c:if>

<link rel="icon" type="image/png" href="assets/img/logo2.png">
<link rel="stylesheet" href="assets/css/amazeui.min.css">
<link rel="stylesheet" href="assets/css/app.css">
<script type="text/javascript">
var fullName="${sessionScope.fullName}";
var userId = "${sessionScope.userId}";

var userName="${sessionScope.loginUserName}";//当前登录用户
var companyNo='${sessionScope.companyNo}';//当前用户所在公司No
</script>
</head>
<body>
<header data-am-widget="header" class="am-header am-header-default">
   <h1 class="am-header-title" id="header-title">
      <img src="assets/img/logo2.png" width="25px"/> 企业一体化解决方案
    </h1>
  </header>

<!-- Menu -->
<nav data-am-widget="menu" class="am-menu  am-menu-offcanvas1" data-am-menu-offcanvas>
<a href="javascript:void(0)" class="am-menu-left" >
    <i style="text-align: right;padding: 16px;" class="am-menu-toggle-icon am-icon-home"></i>
  </a>
  <a href="javascript: void(0)" class="am-menu-toggle">
    <i style="text-align: right;padding: 16px;" class="am-menu-toggle-icon am-icon-bars"></i>
  </a>
  <div class="am-offcanvas">
    <div class="am-offcanvas-bar">

      <ul class="am-menu-nav sm-block-grid-1">      
        <li>
          <a href="user!findUserDetail_wap.action?userName=${sessionScope.loginUserName}">个人信息</a>        
        </li>
        <li >
          <a href="pwdUpdate.jsp">密码修改</a>       
        </li>    
        <li>
          <a href="logout.jsp">注&nbsp;&nbsp;销</a>       
        </li> 
      </ul>
    </div>
  </div>
</nav>
<!-- List -->
<div  class="am-list-news am-list-news-default">
	<table style="width: 100%;text-align: center;" >
		<tr style="height: 120px;">
			<td style="width: 50%;">
				<a href="requestList.jsp?countQueryType=myProposedRequest" class="am-text-secondary">
					<span class="am-icon-btn am-icon-user-plus" style="padding: 10px;"></span><br/>我提交的请求<br/><span style="color: #F00" id="myProposedRequest"></span>
				</a>
			</td>
			<td style="width: 50%;">
				<a href="requestList.jsp?countQueryType=assigneeToMyRequest" class="am-text-secondary">
					<span class="am-icon-btn am-icon-street-view" style="padding: 10px;"></span><br/>指派给我的请求<br/><span style="color: #F00" id="assigneeToMyRequest"></span>
				</a>
			</td>
		</tr>
		<tr style="height: 120px;">
			<td style="width: 50%;">
				<a href="knowledgeList.jsp?opt=my" class="am-text-secondary">
					<span class="am-icon-btn am-icon-recycle" style="padding: 10px;"></span><br/>我发布的知识<br/><span style="color: #F00" id="share_my"></span>
				</a>
			</td>
			<td style="width: 50%;">
				<a href="knowledgeList.jsp?opt=myap" class="am-text-secondary">
					<span class="am-icon-btn am-icon-fire" style="padding: 10px;"></span><br/>我待审的知识<br/><span style="color: #F00" id="share_myapp"></span>
				</a>
			</td>
		</tr>
	</table>
</div>
<!-- List -->
<div class="am-list-news am-list-news-default">

<!-- Navbar -->
<div  class="am-navbar am-cf am-navbar-default "
     id="">
  <ul class="am-navbar-nav am-cf am-avg-sm-4">
    <li>
      <a href="index.jsp">
        <span ><i class="am-icon-home"></i></span>
        <span class="am-navbar-label">主页</span>
      </a>
    </li>
    <li>
      <a href="requestList.jsp">
        <span class="am-icon-list"></span>
        <span class="am-navbar-label">请求管理</span>
      </a>
    </li>
    <li >
      <a href="knowledgeList.jsp">
        <span class="am-icon-book"></span>
        <span class="am-navbar-label">知识库</span>
      </a>
    </li>
    <li>
      <a href="addRequest.jsp">
        <span class="am-icon-plus-square "></span>
        <span class="am-navbar-label">添加请求</span>
      </a>
    </li>
    <li>
      <a href="addKnowledge.jsp">
        <span class="am-icon-plus-square"></span>
        <span class="am-navbar-label">添加知识库</span>
      </a>
    </li>

  </ul>
</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>
<script src="js/index.js"></script>
</body>
</html>