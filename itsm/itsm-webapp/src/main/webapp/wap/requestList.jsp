<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String url="login.jsp";

 if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
} 
 %>
<!doctype html>
<html class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<title>WSTUO-企业一体化解决方案</title>

<link rel="icon" type="image/png" href="../images/logo2.png">
<link rel="stylesheet" href="assets/css/amazeui.min.css"/>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/handlebars.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>

 <style>
    html,
    body,
    .page {
      height: 100%;
    }

    #wrapper {
      position: absolute;
      top: 49px;
      bottom: 0;
      overflow: hidden;
      margin: 0;
      width: 100%;
      padding: 0 8px;
      background-color: #f8f8f8;
    }

    .am-list {
      margin: 0;
    }

    .am-list > li {
      background: none;
      box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.8);
    }

    .pull-action {
      text-align: center;
      height: 45px;
      line-height: 45px;
      color: #999;
    }

    .pull-action .am-icon-spin {
      display: none;
    }

    .pull-action.loading .am-icon-spin {
      display: block;
    }

    .pull-action.loading .pull-label {
      display: none;
    }
  </style>
</head>
<body>
<div class="page">
  <header data-am-widget="header" class="am-header am-header-default">
    <h1 class="am-header-title">请求列表</h1>
  </header>
<!-- Menu -->
<nav data-am-widget="menu" class="am-menu am-menu-offcanvas1" data-am-menu-offcanvas>
    <a href="javascript:history.go(-1);" class="am-menu-left" >
    <i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-chevron-left"></i>
  </a>
  <a href="javascript: void(0)" class="am-menu-toggle">
    <i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-bars"></i>
  </a>
  <div class="am-offcanvas">
    <div class="am-offcanvas-bar">

      <ul class="am-menu-nav sm-block-grid-1">      
        <li>
          <a href="user!findUserDetail_wap.action?userName=${sessionScope.loginUserName}">个人信息</a>        
        </li>
        <li >
          <a href="pwdUpdate.jsp">密码修改</a>       
        </li>  
        <li>
          <a href="index.jsp">返回首页</a>       
        </li>   
        <li>
          <a href="logout.jsp">注&nbsp;&nbsp;销</a>       
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div id="wrapper" data-am-widget="list_news"
       class="am-list-news am-list-news-default">
    <div class="am-list-news-bd">
      <div class="pull-action loading" id="pull-down">
        <span class="am-icon-arrow-down pull-label"
              id="pull-down-label"> 下拉刷新</span>
        <span class="am-icon-spinner am-icon-spin"></span>
      </div>
      <ul class="am-list" id="events-list">
        <li class="am-list-item-desced">
          <div class="am-list-item-text">
            正在加载内容...
          </div>
        </li>
      </ul>
      <div class="pull-action" id="pull-up">
        <span class="am-icon-arrow-up pull-label"
              id="pull-up-label">上拉加载更多</span>
        <span class="am-icon-spinner am-icon-spin"></span>
      </div>
    </div>
  </div>
</div>

<script type="text/x-handlebars-template" id="tpi-list-item">
  {{#each this}}
  <li class="am-list-item-desced" data-id="{{eno}}">
	<div>
    <a href="request!requestDetailsWap.action?eno={{eno}}" style="float: left;" class="am-list-item-hd">{{etitle}}</a>
	<span style="margin-left: 15px;float: left;font-size: 12px;padding-top:5px;">编号：{{requestCode}} </span></div><br/>
	<div class="am-list-item-text">
		<span style="margin-left: 1px">请求分类：{{requestCategoryName}}</span>
		<span style="margin-left: 15px">创建人：{{createdByName}}</span>
		
	</div> 
   
  </li>
  {{/each}}

</script>

<script>
  (function($) {
    var EventsList = function(element, options) {
      var $main = $('#wrapper');
      var $list = $main.find('#events-list');
      var $pullDown = $main.find('#pull-down');
      var $pullDownLabel = $main.find('#pull-down-label');
      var $pullUp = $main.find('#pull-up');
      var topOffset = -$pullDown.outerHeight();

      this.compiler = Handlebars.compile($('#tpi-list-item').html());
      this.prev = this.next = this.start = options.params.start;
      this.total = null;

      this.getURL = function(params) {
        var queries = ['callback=?'];
        for (var key in  params) {
          if (key !== 'start') {
            queries.push(key + '=' + params[key]);
          }
        }
        var countQueryType="${param.countQueryType}";
       	if(countQueryType)queries.push("requestQueryDTO.countQueryType="+countQueryType);
        queries.push('start=');
        return options.api + '?' + queries.join('&');
      };

      this.renderList = function(start, type) {
        var _this = this;
        var $el = $pullDown;

        if (type === 'load') {
          $el = $pullUp;
        }
        $.post(this.URL+start,function(data){
          _this.total = data.total;
          var html = _this.compiler(data.data);
          if (type === 'refresh') {
            $list.children('li').first().before(html);
          } else if (type === 'load') {
            $list.append(html);
          } else {
            $list.html(html);
          }

          // refresh iScroll
          setTimeout(function() {
            _this.iScroll.refresh();
          }, 100);
        }).always(function() {
          _this.resetLoading($el);
          if (type !== 'load') {
            _this.iScroll.scrollTo(0, topOffset, 800, $.AMUI.iScroll.utils.circular);
          }
        });
      };

      this.setLoading = function($el) {
        $el.addClass('loading');
      };

      this.resetLoading = function($el) {
        $el.removeClass('loading');
      };

      this.init = function() {
        var myScroll = this.iScroll = new $.AMUI.iScroll('#wrapper', {
          click: true
        });
        // myScroll.scrollTo(0, topOffset);
        var _this = this;
        var pullFormTop = false;
        var pullStart;

        this.URL = this.getURL(options.params);
        this.renderList(options.params.start);

        myScroll.on('scrollStart', function() {
          if (this.y >= topOffset) {
            pullFormTop = true;
          }

          pullStart = this.y;
          // console.log(this);
        });

        myScroll.on('scrollEnd', function() {
          if (pullFormTop && this.directionY === -1) {
            _this.handlePullDown();
          }
          pullFormTop = false;

          // pull up to load more
          if (pullStart === this.y && (this.directionY === 1)) {
            _this.handlePullUp();
          }
        });
      };

      this.handlePullDown = function() {
        console.log('handle pull down');
        if (this.prev > 0) {
          this.setLoading($pullDown);
          this.prev -= options.params.count;
          this.renderList(this.prev, 'refresh');
        } else {
          console.log('别刷了，没有了');
        }
      };

      this.handlePullUp = function() {
        console.log('handle pull up');
        if (this.next < this.total) {
          this.setLoading($pullUp);
          this.next += options.params.count;
          this.renderList(this.next, 'load');
        } else {
          console.log(this.next);
          // this.iScroll.scrollTo(0, topOffset);
        }
      }
    };

    $(function() {
     	var _url='request!findRequests.action';
     	var app = new EventsList(null, {
	        api:_url,
	        params: {
	        	start: 0,
	        	sidx:'eno',
	        	sord:'desc'
	        }
	      });
      	app.init();
    });

    document.addEventListener('touchmove', function(e) {
      e.preventDefault();
    }, false);
  })(window.jQuery);
</script>
</body>
</html>