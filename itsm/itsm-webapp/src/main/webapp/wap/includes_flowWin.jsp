<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<%-- 任务指派窗口 start --%>
<div class="am-modal am-modal-no-btn" tabindex="-1" id="task_assignee_window">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><span id="title"><fmt:message key="title.bpm.task.assign" /></span>
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
    <form id="task_assignee_form">
			<input type="hidden" name="processHandleDTO.pid" id="task_assignee_pid" />
			<input type="hidden" name="processHandleDTO.taskId" id="task_assignee_taskId" />
			<input type="hidden" name="processHandleDTO.eno" id="task_assignee_eno" />
			<input type="hidden" name="processHandleDTO.module" id="task_assignee_module" />
			<input type="hidden" name="processHandleDTO.operator" value="${loginUserName}">
			<input type="hidden" name="processHandleDTO.handleType" id="task_assignee_handleType" />
			<input type="hidden" name="processHandleDTO.assignType" id="task_assignee_assignType">
			<input type="hidden" name="processHandleDTO.allowUseDynamicAssignee" id="task_assignee_allowUseDynamicAssignee" >
			<input type="hidden" name="processHandleDTO.allowUseVariablesAssignee" id="task_assignee_allowUseVariablesAssignee" >
			
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr id="task_assignee_tr">
					<td>技术员&nbsp;<span style="color:red">*</span></td>
					<td>
						<input type="hidden"  name="processHandleDTO.assigneeNo" id="task_assignee_assigneeNo" value="${sessionScope.userId}" />
						<input style="width:98%;" id="task_assignee_assigneeName" 
						required="true" class="form-control" value="${sessionScope.fullName}"
						onclick="requestDetail.selectRequestUser_byOrgNo2('#task_assignee_assigneeName','#task_assignee_assigneeNo')"/>
					</td>
				</tr>
				<tr>
					<td>备注</td>
					<td>
						<textarea style="margin-top: 10px;width: 98%;height: 85px" name="processHandleDTO.remark" class="form-control" maxLength="255"></textarea>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<a class="btn btn-primary btn-sm" id="task_assignee_submit_button" title="">提交</a>
					</td>
				</tr>
			</table>
		</form>
	</div></div></div>
	<%-- 任务指派窗口 end --%>
	
	
	<%-- 流程处理窗口 start --%>
<div class="am-modal am-modal-no-btn" tabindex="-1" id="flow_handle_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><span id="title"></span>
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
    <form id="flow_handle_from">
			<input type="hidden" id="flow_handle_validMethod" />
			<input type="hidden" name="processHandleDTO.pid" id="flow_handle_pid" />
			<input type="hidden" name="processHandleDTO.taskId" id="flow_handle_taskId" />
			<input type="hidden" name="processHandleDTO.eno" id="flow_handle_eno" />
			<input type="hidden" name="processHandleDTO.operator" value="${loginUserName}">
			<input type="hidden" name="processHandleDTO.assignType" id="flow_handle_assignType">
			<input type="hidden" name="processHandleDTO.outcome" id="flow_handle_nextStep" >
			<input type="hidden" name="processHandleDTO.nextActivityType" id="flow_handle_activityType" >
			<input type="hidden" name="processHandleDTO.allowUseDynamicAssignee" id="flow_handle_allowUseDynamicAssignee" >
			<input type="hidden" name="processHandleDTO.flowActivityId" id="flow_handle_activity_id" >
			<input type="hidden" name="processHandleDTO.approverTask" id="flow_handle_approverTask">
			<input type="hidden" name="processHandleDTO.module" id="flow_handle_module" />
			<input type="hidden" name="processHandleDTO.variablesAssigneeType" id="flow_handle_variablesAssigneeType" />
			<input type="hidden" name="processHandleDTO.leaderNum" id="flow_handle_leaderNum" />
			<input type="hidden" name="processHandleDTO.allowUseVariablesAssignee" id="flow_handle_allowUseVariablesAssignee" >
			<input type="hidden" name="processHandleDTO.variablesAssigneeGroupNo" id="flow_handle_variablesAssigneeGroupNo" >
			<input type="hidden" name="processHandleDTO.variablesAssigneeGroupName" id="flow_handle_variablesAssigneeGroupName" >
			<input type="hidden" name="processHandleDTO.mailHandlingProcess" id="flow_handle_mailHandlingProcess" >
			<input type="hidden" name="processHandleDTO.activityName" id="task_assignee_activityName" >
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td>备注</td>
					<td>
						<textarea style="width: 98%;height: 85px" id="flow_handle_remark" name="processHandleDTO.remark" class="form-control"  required="true" maxLength="255"></textarea>
					</td>
				</tr>
				<tr id="flow_handle_group_tr" style="display: none">
					<td>工作组&nbsp;</td>
					<td>
					<input type="hidden" name="processHandleDTO.groupNo" id="flow_handle_assigneeGroupNo"/>
					<input style="width:98%;margin-top: 10px;"  name="assigneeGroupName" id="flow_handle_assigneeGroupName" class="form-control"
					onclick="requestDetail.selectAssginGroup1('#flow_handle_assigneeGroupNo','#flow_handle_assigneeGroupName')"  />
					</td>
				</tr>
				<tr id=flow_handle_assignee_tr style="display: none">
					<td>技术员&nbsp;</td>
					<td>
					<input type="hidden" name="processHandleDTO.assigneeNo" id="flow_handle_assigneeNo" />
					<input style="width:98%;margin-top: 10px;" name="assigneeName" id="flow_handle_assigneeName" class="form-control"
					onclick="requestDetail.selectRequestUser_byOrgNo1('#flow_handle_assigneeName','#flow_handle_assigneeNo')" />
					</td>
				</tr>
				<%-- <tr id="flow_handle_approversMemberSet_tr" style="display: none">
					<td><fmt:message key="title.change.memberApprover" /></td>
					<td>
					<input style="width:80%"  name="processHandleDTO.approversMemberSet" id="flow_handle_approversMemberSet_show"  readonly="readonly"/>
					</td>
				</tr>
				<tr id=flow_handle_approverTask_tr style="display: none">
					<td><fmt:message key="title.bpm.approverTask" /></td>
					<td>
						<input type="radio" name="processHandleDTO.approverResult" value="true" checked="checked" /><fmt:message key="label.request.requestApprovalAgree" />
						<input type="radio" name="processHandleDTO.approverResult" value="false" /><fmt:message key="label.request.requestApprovalDisagree" />
					</td>
				</tr>
				<tr id=flow_handle_closeNoticeEmailAddress_tr style="display: none">
					<td class="lineTableTd"><fmt:message key="lable.notice.noticeAssignMial" /></td>
					<td><textarea id="closeNoticeEmailAddress" name="processHandleDTO.emailAddress" style="width: 88%;height: 15px"></textarea> <a href="javascript:common.security.userUtil.selectUserMulti('#closeNoticeEmailAddress','','email',-1)"><fmt:message key="common.select" /></a>
					</td>
				</tr> --%>
				<tr>
					<td colspan="2">
						<a class="btn btn-primary btn-sm" id="processHandleButton" style="display: none">提交</a>
						<a class="btn btn-primary btn-sm" id="openProcessButton"  style="display: none">提交</a>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<%-- 流程处理窗口 end --%>
</div></div>