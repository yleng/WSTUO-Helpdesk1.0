<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String url="login.jsp";

 if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
} 
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp,no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />

<title>WSTUO</title>

<link rel="icon" type="image/png" href="assets/img/logo2.png">
<link rel="stylesheet" href="assets/css/amazeui.min.css">
<link href="../css/upload/fileinput.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="editor/css/wangEditor-mobile.min.css">
<link rel="stylesheet" href="assets/css/app.css">	
<style type="text/css">
.container {
	width:100%; 
	height:220px; 
	border:1px solid #ccc;
	background-color: #fff;
	text-align: left;
	box-shadow: 0 0 10px #ccc;
	text-shadow: none;
}
</style>
</head>
<script type="text/javascript">
var eno="${param.eno}";
</script>
<body>
<!-- Header -->
<header data-am-widget="header" class="am-header am-header-default">
    <h1 class="am-header-title">
     添加请求
    </h1>
  </header>
<!-- Menu -->
<nav data-am-widget="menu" class="am-menu am-menu-offcanvas1" data-am-menu-offcanvas>
    <a href="javascript:history.go(-1);" class="am-menu-left">
    <i style="text-align: right;padding: 16px;" class="am-menu-toggle-icon am-icon-chevron-left"></i>
  </a>
  <a href="javascript: void(0)" class="am-menu-toggle">
    <i style="text-align: right;padding: 16px;" class="am-menu-toggle-icon am-icon-bars"></i>
  </a>
  <div class="am-offcanvas">
    <div class="am-offcanvas-bar">
      <ul class="am-menu-nav sm-block-grid-1">      
        <li>
          <a href="user!findUserDetail_wap.action?userName=${sessionScope.loginUserName}">个人信息</a>        
        </li>
        <li >
          <a href="pwdUpdate.jsp">密码修改</a>       
        </li>  
        <li>
          <a href="index.jsp">返回首页</a>       
        </li>   
        <li>
          <a href="logout.jsp">注&nbsp;&nbsp;销</a>       
        </li>
      </ul>
    </div>
  </div>
</nav>
<hr />
<!-- List -->
<div  class="am-list-news am-list-news-default">
	 <form class="am-form" id="addRequestForm" event="saveRequest">
		<input id="requestEdit_eno" type="hidden" name="requestDTO.eno" />
		<input id="requestEdit_pid" type="hidden" name="requestDTO.pid" />
		<input type="hidden" id="requestEdit_status" name="requestDTO.statusNo" />
	 	<input type="hidden" name="requestDTO.technicianName" value="${loginUserName}"  />
		<input type="hidden" name="requestDTO.creator" value="${loginUserName}"  />
		<input type="hidden" name="requestDTO.actionName" id="requestDTO_actionName" value="添加"  />
		<input type="hidden" name="requestDTO.requestCode" id="requestEdit_requestCode" value="" />
		<input type="hidden" name="requestDTO.processKey" id="addRequest_processKey" value="">
		<input type="hidden" name ="requestDTO.formId"  id="addRequest_formId" value="" />
		<input type="hidden" value="${sessionScope.companyNo}" id="request_companyNo" name="requestDTO.companyNo">
		<input type="hidden" value="${sessionScope.userId}" id="request_userId" name="requestDTO.createdByNo" >
          <div class="form-group col-md-6 ui-sortable-handle">
			<label class="form-control-label">分类<span class="required">*</span></label>
			<input type="hidden" value="" id="request_categoryNo" name="requestDTO.requestCategoryNo" required='true'>
			<input name="requestDTO.requestCategoryName"  required='true' id="request_categoryName" value="" class="form-control" readonly="readonly" type="text">
		</div>
		<div class="form-group col-md-6 ui-sortable-handle">
			<label class="form-control-label">标题<span class="required">*</span></label>
			<input name="requestDTO.etitle" id="request_etitle" value=""  required='true' class="form-control" type="text">
		</div>
		<div class="form-group col-md-12 ui-sortable-handle">
			<label class="form-control-label">描述<span class="required">*</span></label>
			<div class="container"><textarea class="form-control" rows="6" required="true" name="requestDTO.edesc" id="request_edesc" ></textarea></div>
		</div>
		<div id="addRequest_formField"></div>
   		<c:if test="${empty param.eno }">
      	<div class="form-group col-md-12 ui-sortable-handle" id="add_request_attachment_div" style="margin-top:10px">
     	 <input type="hidden" name="requestDTO.attachmentStr" id="add_request_attachmentStr" value="">
         <input id="add_request_file" type="file" name="filedata" multiple>
     	</div></c:if>
        <div style="height:50px"></div>
     </form>
</div>

<div class="am-modal am-modal-no-btn" tabindex="-1" id="request_category_select_window">
  <div class="am-modal-dialog">
    <div class="am-modal-hd">
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="request_category_select_tree">
    </div>
  </div>
</div>
<%@ include file="include_tip.jsp"%>
<!-- List -->
<div class="am-list-news am-list-news-default">

<!-- Navbar -->
<div  class="am-navbar am-cf am-navbar-default "
     id="">
  <ul class="am-navbar-nav am-cf am-avg-sm-4">
    <li>
      <a href="#" id="reset_btn">
        <span ><i class="am-icon-repeat"></i></span>
        <span class="am-navbar-label">重置</span>
      </a>
    </li>
    <li>
      <a href="#" id="save_btn">
        <span class="am-icon-save"></span>
        <span class="am-navbar-label">保存</span>
      </a>
    </li>
</ul>
</div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>
<script src="../js/i18n/i18n_zh_CN.js"></script>
<script type="text/javascript" src="../js/basics/package.js"></script>
<script type="text/javascript" src="../js/basics/import.js"></script>
<script src="../js/jquery/jstree/js/lib/jquery.cookie.js"></script>
<script src="../js/jquery/jstree/js/lib/jquery.hotkeys.js"></script>
<script src="../js/jquery/jstree/js/jquery.jstree.js"></script>
<script src="../js/jquery/jstree/js/rewritejstree.js"></script>
<script src="../js/My97DatePicker/WdatePicker.js"></script>
<script src="../js/jquery/jquery.validate.min.js"></script>
<script src="../js/jquery/messages_zh.js"></script>

<script src="js/fileinput.js" type="text/javascript"></script>
<script src="../js/basics/initUploader.js" type="text/javascript"></script>
<script type="text/javascript" src="editor/js/lib/zepto.js"></script>
<script type="text/javascript" src="editor/js/lib/zepto.touch.js"></script>
<script type="text/javascript" src="editor/js/wangEditor-mobile.min.js"></script>
<script type="text/javascript">
// 生成编辑器
var editor = new ___E('request_edesc');
// 初始化
editor.init();
</script>
<script src="js/common.js"></script>
<script src="js/addRequest.js?random=<%=new java.util.Date().getTime()%>"></script>
</body>
</html>