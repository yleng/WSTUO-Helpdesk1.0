<style>
    .table_border{width:100%}
	.table_border{ border:solid black; border-width:1px 0 0 1px;}
	.table_border caption {font-size:14px;font-weight:bolder;}
	.table_border th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
    .table_border a{text-decoration:none}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                您好:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
  &nbsp;&nbsp;这是编号为：${variables.ecode}的请求工单。
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="732px">
                    <thead>
                        <tr style="font-size:12px;" align=left>
                            <th width="40%" align="left">
                                <th align="left">
                                    <br/>
                                    <font size="6px">
                                        请求工单
                                    </font>
                                </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr height=30px>
                            <td>
                                工单号:
                                <span id="request_printCode">
				${variables.ecode}
                                </span>
                            </td>
                            <td align=right>
                                请求创建时间:
                                <span id="request_printCreatedOn">
                                     ${variables.createdOn?string("yyyy-MM-dd HH:MM:ss")}
                                </span>
                                 
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table_border" border="1" cellspacing="0px" style="border-collapse:collapse;width:732px;height:1026px;border:1px solid black;">
                    <tbody>
                        <tr style="font-size:12px;background:#ccc" align="left">
                            <th style="padding:5px; text-align: left" colspan="4">
                                请求人基本信息
                            </th>
                        </tr>
                        <tr height=30>
                            <td width="108px" style="border:1px solid black;">
                                请求用户
                            </td>
                            <td width="208px">
                                <span>
                                    ${variables.createdByName}
                                </span>
                            </td>
                            <td width="109px">
                                联系电话
                            </td>
                            <td width="208px">
                                <span>
                                    ${variables.createdByPhone}
                                </span>
                            </td>
                        </tr>
                        <tr style="font-size:12px;background:#ccc;" align="left">
                            <th style="padding:5px;text-align: left" colspan="4">
                                请求信息
                            </th>
                        </tr>
                        <tbody>
                            <tr>
                                <td>
                                    标题
                                </td>
                                <td colspan="3">
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.etitle}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    内容
                                </td>
                                <td height="200px" colspan="3">
                                    ${variables.edesc}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    影响
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.effectRangeName}
                                    </span>
                                </td>
                                <td>
                                    状态
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.statusName}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    分类
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.ecategoryName}
                                    </span>
                                </td>
                                <td>
                                    复杂程度
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.complexity}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    优先级
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.priorityName}
                                    </span>
                                </td>
                                <td>
                                    紧急度
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.seriousnessName}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    来源
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.imodeName}
                                    </span>
                                </td>
                                <td>
                                    关联配置项
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.ciname}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    SLA响应时间
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.maxResponsesTime?string("yyyy-MM-dd HH:MM:ss")}
                                    </span>
                                </td>
                                <td>
                                    SLA完成时间
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.maxCompletesTime?string("yyyy-MM-dd HH:MM:ss")}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    实际响应时间
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.responsesTime?string("yyyy-MM-dd HH:MM:ss")}
                                    </span>
                                </td>
                                <td>
                                    实际完成时间
                                </td>
                                <td>
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.closeTime?string("yyyy-MM-dd HH:MM:ss")}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    历史记录
                                </td>
                                <td height="130px" colspan="3">
                                    <span style="text-align:left;font-size:12px;">
                                        <#if (historyRecordDTO?exists && historyRecordDTO?size>
                                            0) >
                                            <#list historyRecordDTO as historyRecord>
                                                <span class=wspan>
                                                    <span>
                                                    </span>
                                                    ${historyRecord.logTitle}: ${historyRecord.logDetails}&nbsp;&nbsp;(${historyRecord.operator})${historyRecord.createdTime?string("yyyy-MM-dd
                                                    HH:mm:ss")}
                                                    <br/>
                                                </span>
                                            </#list>
                                        </#if>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    解决方案
                                </td>
                                <td height="269px" colspan="3">
                                    <span style="text-align:left;font-size:12px;">
                                        ${variables.solutions}
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
                谢谢！
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
这是一封系统的邮件，请勿直接回复！
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~