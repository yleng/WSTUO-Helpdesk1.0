<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                您好:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                您有一个新任务，请及时查看。
                <br/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                任务标题：${variables.etitle}
                <br/>
                任务描述：${variables.edesc}
                <br/>
                任务计划时间：${variables.planTime}
                <br/>
                任务创建人：${variables.createdByName}
		 <br/>
                创建时间: ${variables.createdOn?string("yyyy-MM-dd HH:MM:ss")}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
                谢谢！
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
这是一封系统的邮件，请勿直接回复！
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~