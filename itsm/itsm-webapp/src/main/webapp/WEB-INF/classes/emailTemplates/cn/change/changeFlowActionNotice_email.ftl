<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
		<#if (userName??)>
                    ${userName}，
                </#if>
                您好:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                内容：变更流程任务指派通知；
                <#if variables.activityName?exists && variables.outcome?exists>
		${variables.createdByName} 对[变更][${variables.etitle}]执行动作：
                    （ ${variables.activityName} ->${variables.outcome}）
                </#if>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                编号：${variables.ecode}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                标题：${variables.etitle}
            </td>
        </tr>
	<tr>
            <td colspan="2">
		<br>变更 [${variables.ecode}] 关联配置项
		<#if (variables.ciInfos?exists && variables.ciInfos?size>0) >
                    <br>
                    <table class="table_border">
                        <tr>
                            <td>
                                固定资产编号
                            </td>
                            <td>
                                资产名称
                            </td>
                            <td>
                                配置项分类
                            </td>
                            <td>
                                状态
                            </td>
                        </tr>
                        <#list variables.ciInfos as ciDTO>
                            <tr>
                                <td>
                                    ${ciDTO.cino}
                                </td>
                                <td>
                                    ${ciDTO.ciname}
                                </td>
                                <td>
                                    ${ciDTO.categoryName}
                                </td>
                                <td>
                                    ${ciDTO.status}
                                </td>
                            </tr>
                        </#list>
                    </table>
                </#if>
	    </td>
        </tr>
        <tr>
            <td colspan="2">
		<br>变更 [${variables.ecode}] 历史记录 
                <#if (variables.historyRecordDTO?exists && variables.historyRecordDTO?size>0) >
		<br><table class="table_border">
                        <tr>
                            <td>
                                动作
                            </td>
                            <td>
                                详细
                            </td>
                            <td>
                                操作者
                            </td>
                            <td>
                                操作时间
                            </td>
                        </tr>
                        <#list variables.historyRecordDTO as historyRecord>
                            <tr>
                                <td width="20%">
                                    ${historyRecord.logTitle}
                                </td>
                                <td style="word-wrap:break-word;word-break:break-all;" width="40%">
                                    ${historyRecord.logDetails}
                                </td>
                                <td width="20%">
                                    ${historyRecord.operator}
                                </td>
                                <td width="20%">
                                    ${historyRecord.createdTime?string("yyyy-MM-dd HH:mm:ss")}
                                </td>
                            </tr>
                        </#list>
                    </table><br>
		</#if>
	     </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
                谢谢！
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
这是一封系统的邮件，请勿直接回复！
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~