<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                <#if (userName??)>
                    ${userName}，
                </#if>
                您好:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
		內容：變更流程任務指派通知；
                <#if variables.activityName?exists && variables.outcome?exists>
		${variables.createdByName} 對[變更][${variables.etitle}]執行動作：
                    （ ${variables.activityName} ->${variables.outcome}）
                </#if>
            </td>
        </tr>
	<tr>
            <td colspan="2">
                編號：${variables.ecode}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                標題：${variables.etitle}
            </td>
        </tr>
	<tr>
            <td colspan="2">
		<br>變更 [${variables.ecode}] 關聯配置項
		<#if (variables.ciInfos?exists && variables.ciInfos?size>0) >
                    <br>
                    <table class="table_border">
                        <tr>
                            <td>
                                固定資產編號
                            </td>
                            <td>
                                資產名稱
                            </td>
                            <td>
                                配置項分類
                            </td>
                            <td>
                                狀態
                            </td>
                        </tr>
                        <#list variables.ciInfos as ciDTO>
                            <tr>
                                <td>
                                    ${ciDTO.cino}
                                </td>
                                <td>
                                    ${ciDTO.ciname}
                                </td>
                                <td>
                                    ${ciDTO.categoryName}
                                </td>
                                <td>
                                    ${ciDTO.status}
                                </td>
                            </tr>
                        </#list>
                    </table>
                </#if>
	    </td>
        </tr>
        <tr>
            <td colspan="2">
                變更[${variables.ecode}]歷史記录:
		<#if (variables.historyRecordDTO?exists && variables.historyRecordDTO?size>0) >
                <br><table class="table_border">
                        <tr>
                            <td>
                                動作
                            </td>
                            <td>
                                詳細
                            </td>
                            <td>
                                操作者
                            </td>
                            <td>
                                操作時間
                            </td>
                        </tr>
                        <#list variables.historyRecordDTO as historyRecord>
                            <tr>
                                <td width="20%">
                                    ${historyRecord.logTitle}
                                </td>
                                <td style="word-wrap:break-word;word-break:break-all;" width="40%">
                                    ${historyRecord.logDetails}
                                </td>
                                <td width="20%">
                                    ${historyRecord.operator}
                                </td>
                                <td width="20%">
                                    ${historyRecord.createdTime?string("yyyy-MM-dd HH:mm:ss")}
                                </td>
                            </tr>
                        </#list>
		</table>
		</#if>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
             謝謝！
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
這是一封係統的郵件，請勿直接迴複！
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~