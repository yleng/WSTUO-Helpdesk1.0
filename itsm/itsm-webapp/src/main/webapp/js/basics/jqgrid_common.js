var jqGridJsonReader = {
	root: "data",
	records: "totalSize",
	page: "page",
	total: "total",
    repeatitems: false
};
var jqGridParams = {
	mtype:'post',
	datatype:'json',
	autowidth:true,
	height:'100%',
	viewrecords: true, 
	multiselect:true,
	multiboxonly:false,
	sortorder:'desc',
    rowNum:20,
	rowList:[5,10,15,20],
	toolbar:[true,"top"],
	hidegrid:false,
	gridComplete:function(){
		$('.ui-pg-input').focus(function(){
			tcode_foucs=false;
		});
	}
};
//grid数据显示10条
var jqGridParamsTen = jQuery.extend(true, {}, jqGridParams); 
jqGridParamsTen.rowNum=10;

var navGridParams = {
	edit:false,add:false,del:false,search:false
};
var jqGridTopStyles = {
	"padding-top": "5px",
	"padding-bottom": "10px",
	"border-top": "none",
	"background": "#eee",
	"text-align": "left",
	"overflow":"auto"
};
function afficheActFormatter(cell,opt,data){
	return $('#afficheActFormatterDiv').html();
}

function checkBeforeMethod(grid, method){
	
	var rowIds = $(grid).getGridParam('selarrrow');
	if(rowIds==''){
		msgAlert(i18n['msg_atLeastChooseOneData'],'info');
	}else{
		method(rowIds);
	}
}

function checkBeforeDeleteGrid(grid, deleteMethod) {
	var rowIds = $(grid).getGridParam('selarrrow');
	
	if(rowIds==''){
		msgAlert(i18n['msg_atLeastChooseOneData'],'info');
	}
	else{
		msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
			deleteMethod(rowIds);
		});
	}
}
/**
 * 确认操作.
 */

function checkBeforeEditGrid(grid, editMethod) {
	var rowId = $(grid).getGridParam('selrow');
	if(rowId==null){
		msgAlert(i18n['msg_atLeastChooseOneData'],'info');
	}else{
		var rowData=$(grid).getRowData(rowId);
//		if(rowData.dataFlag!=null && rowData.dataFlag==1){
//			msgAlert(i18n['msg_system_data_can_not_edit'],'warning');
//		}else{
			editMethod(rowData);
//		}
	}
}

/**
 * @description  自适应大小
 */
function setGridWidth(grid,fitchSize,parentId){
	var cs='.box-content';
	if(parentId)cs=parentId;
	var wid=$(grid).closest(cs).width();

	if(!wid){
		wid=$(grid).closest(cs).get(0).style.width;
		wid=wid.replace(/px/g,'');
	}
	$(grid).setGridWidth(wid-2);	
			//以下代码是处理自动调整宽度时标题与内容错位的问题
//			var column=$(grid).jqGrid('getGridParam','colModel');
//			if(column[0].hidden){
//				$(grid).showCol(column[0].name);
//				setTimeout(function(){
//					$(grid).hideCol(column[0].name);
//				},0);
//			}else{
//				$(grid).hideCol(column[0].name);
//				setTimeout(function(){
//					$(grid).showCol(column[0].name);
//				},0);
//			}
}

/**
 * @description  行内删除
 */
function checkBeforeDeleteGridInLine(grid,deleteMethod){
	
	var rowId = $(grid).getGridParam('selrow');
	

	msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
			deleteMethod(rowId);
	});

}

function confirmBeforeDelete(method){
	
	msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
			method();
	});
}



/**
 * @description  搜索方法
 */
function searchGridWithForm(grid,form,submitUrl){
	
	var sdata = $(form+' form').getForm();
	var postData = $(grid).jqGrid("getGridParam", "postData");       
	$.extend(postData, sdata);
	$(grid).jqGrid('setGridParam',{url:submitUrl,page:1}).trigger('reloadGrid');
	
	
}


/**
 * @description  提交form方法
 */
function submitFormCommon(divId,url,method){
	
	 var frm = $(divId+' form').serialize();
	 $.post(url,frm, function(r){
		 method(r);
	 });
}


this.timeFormatter=function(cell,opt,data){
	if(cell!=null){
		return cell.replace("T"," ").replace('.0','');
	}else{
		return '';
	}
};


this.timeFormatter2=function(cell,opt,data){
	if(cell!=null){
		return cell.split("T")[0];
	}else{
		return '';
	}
};
this.timeFormatter3=function(cell,opt,data){
	if(cell!=null){
		return (cell.split("T")[0]+"").replace(/ 00:00:00/g,"");
	}else{
		return '';
	}
};

this.timeFormatterOnlyData=function(cell,opt,data){
	if(cell!=null){
		return cell.substring(0,cell.lastIndexOf("-")+3);
	}
	else{
		return '';
	}
};

/**
 * @description  验证用户是否存在
 */
this.checkUserByName=function(userName,func){
	
	var url='user!findByUserName.action';
	var param='userName='+userName;
	
	$.post(url,param, function(res){

		if(res!=null){
			func(res.userId);
		}else{
			msgAlert(i18n['msg_msg_userNotExist'],'info');
		}
	});
};

/**
 * @description  验证表单
 */
this.vaildateForm=function(formId,method){

	
	 if($(formId).form('validate')){ 
		 method();
	 }
};



/**
 * @description 选择请求分类.
 */
this.commonSelectRequestCategory=function(func){
	windows('commonSelectRequestCategory');
	selectRequestCategoryTreeView(func);
};

/**
 * @description 请求分类树加载.
 */
this.selectRequestCategoryTreeView=function(func){
	
	$("#commonSelectRequestCategoryTree").jstree({
		json_data: {
			
			ajax: {
				type:"post",
				url : "event!getCategoryTree.action",
				dataType: 'json',
				data:function(n){
			  	  return {'types': 'Request','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
			    }
			}
		},
		plugins : ["themes", "json_data", "ui", "crrm"]
	})
	.bind('select_node.jstree',func);
};

this.importCSV=function(p_url,callback){
	
	$.ajaxFileUpload({
        url:p_url,
        secureuri:false,
        fileElementId:'importFile', 
        dataType:'json',
        success: function(data,status){
			callback(data);
        }
  });
	
};

this.readyImportCSV=function(callback){
	window('index_import_excel_window',{width:400,height:'auto'});
	$("#index_import_confirm").unbind(); //清空事件				
	$('#index_import_confirm').click(callback);
};

//String函数之LTrim(),用于清除字符串开头的空格\换行符\回车等
this.ltrim=function(str) {
	var pattern = new RegExp("^[\\s]+","gi");
	return str.replace(pattern,"");
};
//End

//String函数之RTrim(),用于清除字符串结尾的空格\换行符\回车等
this.rtrim=function(str) {
	var pattern = new RegExp("[\\s]+$","gi");
	return str.replace(pattern,"");
};
//End

//清除两边空格
this.trim=function(str) {
	return rtrim(ltrim(str));
};

this.colorFormatter=function(color,cell){
	if(cell==null || cell == 'null' || cell == undefined)
		cell = '';
	var _str='<div>'+cell+'</div>';
	if(color!=null && color!='' ){
		if(color=='#ffffff'||color.indexOf('#fff')!=-1){
			_str="<span style='background-color:"+color+";font-weight: normal;font-size:99%;color: #000;' class='label'>"+cell+"</span>";
		}else{
			_str="<span style='background-color:"+color+";font-weight: normal;font-size:99%;color: #ffffff;' class='label'>"+cell+"</span>";
		}
	}
	return _str;
};
this.formatDescContent= function(str){
	if( str ){
		str = str.replace(/\s*<\/p>\s*|\s*<\/p>\s*/, '')
			.replace(/\s*<p>\s*|\s*<\/p>\s*/, '')
			.replace(/&nbsp;/gi, '');
	}
	return str
}