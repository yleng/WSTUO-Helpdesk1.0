 $package('itsm.cim');
/**  
 * @fileOverview 配置项分类树
 * @author QXY
 * @version 1.0  
 */  

 /**  
 * @author QXY  
 * @constructor CategoryTree
 * @description 配置项分类树
 * @since version 1.0 
 */  
itsm.cim.ciCategoryTree=function(){
	this._categoryCodeRule='';
	return {
		/**
		 * @description 配置项分类树结构
		 * @param treeWin1 显示分类树的窗口
		 * @param treeDiv1 显示分类树的div
		 * @param sid1 选中树时保存分类id
		 * @param sname1 选中树时保存分类名称
		 * @param findAll 是否查询全部
		 * @param method 回调函数
		 */
		configureItemTree:function(treeWin1,treeDiv1,sid1,sname1,findAll,method){
			itsm.cim.ciCategoryTree.configureItemTreeNew(treeWin1,treeDiv1,sid1,sname1,findAll,method,null);
			
		},
		configureItemTreeNew:function(treeWin1,treeDiv1,sid1,sname1,findAll,method,anyMethod){
			treeWin=treeWin1;
			treeDiv=treeDiv1;
			sid=sid1;
			sname=sname1;
			var _url='';
			if(findAll=='false'){
				_url="ciCategory!getConfigurationCategoryTree.action?findAll=false&userName="+userName;
			}else{
				_url="ciCategory!getConfigurationCategoryTree.action?findAll=true&userName="+userName;
			}
			windows(treeWin1,{width:'auto',height:'auto'});
			$("#"+treeDiv1).jstree({
				"json_data":{
				    ajax: {
				    	url :_url,
				    	data:function(n){
					    	  return {'parentEventId':n.attr ? n.attr("cno").replace("node_",""):0};//types is in action
					    },
				    	cache: false}
				},
				"plugins" : ["themes", "json_data", "ui", "crrm", "cookies", "types", "hotkeys"]
				})
				//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
				.bind('select_node.jstree',function(e, data){
					var cno=data.rslt.obj.attr('cno');
					if(cno=='1'){
					}else{
						$("#"+sid1).val(data.rslt.obj.attr('cno'));
						$("#"+sname1).val(data.rslt.obj.attr('cname')).focus();
						$('#'+treeWin1).dialog('close');
						if(method!=undefined)
							method();
					}
					if(anyMethod!=undefined)
						anyMethod(e,data);
			});
		},
		/**
		 * 选择配置项分类更新扩展属性
		 * @param treeWin 显示分类树的窗口
		 * @param treeDiv 显示分类树的div
		 * @param categoryNoId 选中树时保存分类id
		 * @param categoryNameId 选中树时保存分类名称
		 * @param showAttrId 显示扩展属性div id
		 * @param ciNoId 配置项id
		 */
		autoUpdateEavAttrCiCategoryTree:function(treeWin,treeDiv,categoryNoId,categoryNameId,showAttrId,ciNoId){
			$("#"+treeDiv).jstree({
				"json_data":{
				    ajax: {
				    	url : "ciCategory!getConfigurationCategoryTree.action?findAll=false&userName="+userName+"&simpleDto=true",
				    	data:function(n){
					    	  return {'parentEventId':n.attr ? n.attr("cno").replace("node_",""):0};//types is in action
					    },
				    	cache: false}
				},
				"plugins" : ["themes", "json_data", "ui", "crrm", "cookies", "types", "hotkeys"]
			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind('select_node.jstree',function(e, data){
				if(data.rslt.obj.attr('categoryRoot')=='CICategory'){
				}else if(data.rslt.obj.attr('cno')==1){    //不能选择父节点
				}else{
					var cno=data.rslt.obj.attr('cno');
					$("#"+categoryNoId).val(data.rslt.obj.attr('cno'));
					$("#"+categoryNameId).val(data.rslt.obj.attr('cname')).focus();
					var categoryCodeRule=data.rslt.obj.attr('categoryCodeRule');
					var ciNo=$('#'+ciNoId).val();//当前的配置项编号
					if(categoryCodeRule!=null && categoryCodeRule!='null'){
						if(ciNo.indexOf(categoryCodeRule)>-1){//如果当前配置项编号存在同样的规则，则替换
							$('#'+ciNoId).val($('#'+ciNoId).val().replace(_categoryCodeRule,categoryCodeRule))
						}else{
							$('#'+ciNoId).val(categoryCodeRule+$('#'+ciNoId).val().replace(_categoryCodeRule,''))
						}
						
					}
					var dtoName="ciDto";
					itsm.cim.ciCategoryTree.showAttributet(data.rslt.obj.attr('categoryType'),showAttrId,dtoName);
					
					_categoryCodeRule=categoryCodeRule;
					
					$('#'+treeWin).dialog('close');
				}
				
			});
			windows(treeWin,{width:300,height:300,close:function(){
				$("#"+categoryNameId).blur();
			}});
		},
		
		/**
		 * 显示扩展属性
		 * @param eavId 扩展属性id
		 * @param showAttrId 显示div的id
		 * @param dtoName 扩展属性name
		 */
		showAttributet:function(eavId,showAttrId,dtoName){
			$('#'+showAttrId+' table tbody').html('');
			if(!isNaN(eavId)){
				$('#'+showAttrId+' table thead').html("");
				$.post('ci!attributeList.action','eavNo='+eavId,function(data){
					if(data!=null && data.length>0 && eavId!=null){
						var oldName='';
						for(var i=0;i<data.length;i++){
							var requiredHtml ='';
							var required =false;
							if(!data[i].attrIsNull){
								required =true;
								requiredHtml ='<span style=color:red>&nbsp;&nbsp;*</span>';
							}
							var attr_html="";
							if(data[i].attrGroupName!=oldName){
								attr_html="<tr><th colspan=\"2\" style=\"text-align:left;\">"+data[i].attrGroupName+"</th></tr>";
							}
							oldName=data[i].attrGroupName;
							
							if(data[i].attrType=="String"){
								attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><input "+(required==true?'validtype=nullValueValid':'')+" attrtype='String' style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"'/></td></tr>";
								$('#'+showAttrId+' table tbody').append(attr_html);
								$('#'+data[i].attrName+showAttrId).validatebox({
									required:required,
									validType:length[1,200]
								});
							}
							else if(data[i].attrType=="Date"){	
								attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><input style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"' readonly/></td></tr>";
								$('#'+showAttrId+' table tbody').append(attr_html);
								DatePicker97(['#'+data[i].attrName+showAttrId]);
								$('#'+data[i].attrName+showAttrId).validatebox({
										required:required
								});
							}
							else if(data[i].attrType=="Integer"){
								attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><input style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"'/></td></tr>";
								$('#'+showAttrId+' table tbody').append(attr_html);
								$('#'+data[i].attrName+showAttrId).numberbox({
										required:required,
										min:0,
										validType:length[1,9],
										max:999999999
									});
							}	
							else if(data[i].attrType=="Double"){	
								attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><input style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"'/></td></tr>";
								$('#'+showAttrId+' table tbody').append(attr_html);
								$('#'+data[i].attrName+showAttrId).numberbox({
										required:required,
										min:0,
										validType:length[1,13],
										max:999999999.999,
										precision:3
									});
							}
							else if(data[i].attrType=="Lob"){
								attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><textarea "+(required==true?'validtype=nullValueValid':'')+" attrtype='String'  style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"' ></textarea></td></tr>";
								$('#'+showAttrId+' table tbody').append(attr_html);
								$('#'+data[i].attrName+showAttrId).validatebox({
									required:required
								});
							}else if(data[i].attrType=="DataDictionaray"){
								var attrdata_url="dataDictionaryItems!findByCode.action?dataDictionaryQueryDto.groupCode="+data[i].attrdataDictionary;
								var data_name = data[i].attrName;
								var data_id = data[i].attrName+showAttrId;
								attr_html+="<tr><td style=text-align:left;width:25%; >"+data[i].attrAsName+requiredHtml+"</td>";
								attr_html+="<td style='text-align:left;'><select style=width:75%; name="+dtoName+".attrVals['"+data_name+"'] id='"+data_id+"' >"; 
								attr_html+="<option value=''>--"+i18n['pleaseSelect']+"--</option>";
								attr_html+="</select></td></tr>";
								$('#'+showAttrId+' table tbody').append(attr_html);
								if(!data[i].attrIsNull)
									$('#'+data[i].attrName+showAttrId).validatebox({
										required:required
									});
								itsm.cim.ciCategoryTree.loadDataDic(attrdata_url,data_id);
							}else if(data[i].attrType=="Radio" || data[i].attrType=="Checkbox"){
								var attrItemName = data[i].attrItemName;
								var field='';
								var data_name = data[i].attrName;
								for ( var _int = 0; _int < attrItemName.length; _int++) {
									var validType='';
									if(_int==attrItemName.length-1 && !data[i].attrIsNull){
										validType='validType="radioAndchekboxValid[\''+showAttrId+'\',\''+data_name+'\']" required="true" class="easyui-validatebox"';
									}
									
									field=field+'<label><input name="'+data_name+'" type="'+data[i].attrType+'" '+validType+' value="'+attrItemName[_int]+'" >'+attrItemName[_int]+'</label>&nbsp;&nbsp;';
								}
								attr_html+="<tr><td style=text-align:left;width:25%; >"+data[i].attrAsName+requiredHtml+"</td>";
								attr_html+="<td style='text-align:left;'>"+field+'<input name="'+dtoName+'.attrVals[\''+data_name+'\']" type="hidden" attrType="'+data[i].attrType+'" id="'+data_name+'"></td></tr>';
								$('#'+showAttrId+' table tbody').append(attr_html);
							}
							$.parser.parse($('#'+showAttrId));
						}
					}else{
						$('#'+showAttrId+' table thead').hide();
						$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n['noData']+"</td></tr>");
					}
				});
			}else{
				$('#'+showAttrId+' table thead').hide();
				$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n['noData']+"</td></tr>");
			}
		},
		/**
		 * 显示扩展属性
		 * @param eavId 扩展属性id
		 * @param showAttrId 显示div的id
		 * @param dtoName 扩展属性name
		 */
		showAttributetTovalue:function(eavId,showAttrId,dtoName,templateId){
			$('#'+showAttrId+' table tbody').html('');
			if(!isNaN(eavId)){
				$('#'+showAttrId+' table thead').html("");
				$.post("template!findByTemplateId.action",{"templateDTO.templateId":templateId},function(eventEavVals){
					$.post('ci!attributeList.action','eavNo='+eavId,function(data){
						if(data!=null && data.length>0 && eavId!=null){
							var oldName='';
							for(var i=0;i<data.length;i++){
								var requiredHtml ='';
								var required =false;
								if(!data[i].attrIsNull){
									required =true;
									requiredHtml ='<span style=color:red>&nbsp;&nbsp;*</span>';
								}
								var attr_html="";
								if(data[i].attrGroupName!=oldName){
									attr_html="<tr><th colspan=\"2\" style=\"text-align:left;\">"+data[i].attrGroupName+"</th></tr>";
								}
								oldName=data[i].attrGroupName;
								var value='';
								value = eventEavVals.dto.attrVals[data[i].attrName];
								if(data[i].attrType=="String"){
									attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><input  attrtype='String' style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"'/></td></tr>";
									$('#'+showAttrId+' table tbody').append(attr_html);
									$('#'+data[i].attrName+showAttrId).validatebox({
										required:required,
										validType:length[1,200]
									});
								}
								else if(data[i].attrType=="Date"){	
									attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><input style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"' readonly/></td></tr>";
									$('#'+showAttrId+' table tbody').append(attr_html);
									DatePicker97(['#'+data[i].attrName+showAttrId]);
									$('#'+data[i].attrName+showAttrId).validatebox({
											required:required
									});
								}
								else if(data[i].attrType=="Integer"){
									attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><input style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"'/></td></tr>";
									$('#'+showAttrId+' table tbody').append(attr_html);
									$('#'+data[i].attrName+showAttrId).numberbox({
											required:required,
											min:0,
											validType:length[1,9],
											max:999999999
										});
								}	
								else if(data[i].attrType=="Double"){	
									attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><input style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"'/></td></tr>";
									$('#'+showAttrId+' table tbody').append(attr_html);
									$('#'+data[i].attrName+showAttrId).numberbox({
											required:required,
											min:0,
											validType:length[1,13],
											max:999999999.999,
											precision:3
										});
								}
								else if(data[i].attrType=="Lob"){
									attr_html+="<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+requiredHtml+"</td><td style='text-align:left;'><textarea attrtype='String'  style=width:75% name="+dtoName+".attrVals['"+data[i].attrName+"'] id='"+data[i].attrName+showAttrId+"' ></textarea></td></tr>";
									$('#'+showAttrId+' table tbody').append(attr_html);
									$('#'+data[i].attrName+showAttrId).validatebox({
										required:required
									});
								}else if(data[i].attrType=="DataDictionaray"){
									var attrdata_url="dataDictionaryItems!findByCode.action?dataDictionaryQueryDto.groupCode="+data[i].attrdataDictionary;
									var data_name = data[i].attrName;
									var data_id = data[i].attrName+showAttrId;
									attr_html+="<tr><td style=text-align:left;width:25%; >"+data[i].attrAsName+requiredHtml+"</td>";
									attr_html+="<td style='text-align:left;'><select style=width:75%; name="+dtoName+".attrVals['"+data_name+"'] id='"+data_id+"' >"; 
									attr_html+="<option value=''>--"+i18n['pleaseSelect']+"--</option>";
									attr_html+="</select></td></tr>";
									$('#'+showAttrId+' table tbody').append(attr_html);
									if(!data[i].attrIsNull)
										$('#'+data[i].attrName+showAttrId).validatebox({
											required:required
										});
									itsm.cim.ciCategoryTree.loadDataDic(attrdata_url,data_id);
								}else if(data[i].attrType=="Radio"){
									var attrItemName = data[i].attrItemName;
									var field='';
									var data_name = data[i].attrName;
									for ( var _int = 0; _int < attrItemName.length; _int++) {
										var validType='';
										if(_int==attrItemName.length-1 && !data[i].attrIsNull){
											validType='validType="radioAndchekboxValid[\''+showAttrId+'\',\''+data_name+'\']" required="true" class="easyui-validatebox"';
										}
										field=field+'<label><input name="'+data_name+'" {checked} type="'+data[i].attrType+'" '+validType+' value="'+attrItemName[_int]+'" >'+attrItemName[_int]+'</label>&nbsp;&nbsp;';
										if(value===attrItemName[_int])
											field=field.replace(/{checked}/g,'checked="checked"');
										else
											field=field.replace(/{checked}/g,'');
									}
									attr_html+="<tr><td style=text-align:left;width:25%; >"+data[i].attrAsName+requiredHtml+"</td>";
									attr_html+="<td style='text-align:left;'>"+field+'<input name="'+dtoName+'.attrVals[\''+data_name+'\']" type="hidden" attrType="'+data[i].attrType+'" id="'+data_name+'"></td></tr>';
									$('#'+showAttrId+' table tbody').append(attr_html);
								}else if(data[i].attrType=="Checkbox"){
									var attrItemName = data[i].attrItemName;
									var field='';
									var data_name = data[i].attrName;
									for ( var _int = 0; _int < attrItemName.length; _int++) {
										var validType='';
										if(_int==attrItemName.length-1 && !data[i].attrIsNull){
											validType='validType="radioAndchekboxValid[\''+showAttrId+'\',\''+data_name+'\']" required="true" class="easyui-validatebox"';
										}
										field=field+'<label><input name="'+data_name+'" {checked} type="'+data[i].attrType+'" '+validType+' value="'+attrItemName[_int]+'" >'+attrItemName[_int]+'</label>&nbsp;&nbsp;';
										if(value.indexOf(attrItemName[_int]) !=-1)
											field=field.replace(/{checked}/g,'checked="checked"');
										else
											field=field.replace(/{checked}/g,'');
									}
									attr_html+="<tr><td style=text-align:left;width:25%; >"+data[i].attrAsName+requiredHtml+"</td>";
									attr_html+="<td style='text-align:left;'>"+field+'<input name="'+dtoName+'.attrVals[\''+data_name+'\']" type="hidden" attrType="'+data[i].attrType+'" id="'+data_name+'"></td></tr>';
									$('#'+showAttrId+' table tbody').append(attr_html);
								}
							}
							$.parser.parse($('#'+showAttrId+' table tbody'));//加上这句，必填才有用
						}else{
							$('#'+showAttrId+' table thead').hide();
							$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n['noData']+"</td></tr>");
						}
					});
				});
			}else{
				$('#'+showAttrId+' table thead').hide();
				$('#'+showAttrId+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan=2>"+i18n['noData']+"</td></tr>");
			}
		},
		/**
		 * 加载数据字典
		 * @param attrdata_url 查询数据字典路径
		 * @param data_id 显示下拉框的id
		 */
		loadDataDic:function(attrdata_url,data_id){
			$.post(attrdata_url,function(res){
				$.each(res,function(key,value){
					$('#'+data_id).append("<option value='"+value.dcode+"'>"+value.dname+"</option>");
				});  
				
			});
		},
		/**
		 * 默认显示扩展属性
		 * @param ciCategoryId 配置项id
		 * @param showAttrId 显示div id
		 */
		defaultShowAttributet:function(ciCategoryId,showAttrId){
			var Name="ciDto";
			if(ciCategoryId!=null && ciCategoryId!=''){
				$('#'+showAttrId+' table tbody').html('');
				$.post('ciCategory!findCICategoryById.action','categoryNo='+ciCategoryId,function(data){
					if(data!=null && data.categoryType!=null){
						if(!isNaN(data.categoryType)){
							$.post('ci!attributeList.action','eavNo='+data.categoryType,function(data){
								if(data!=null && data.length>0){
									//$('#'+showAttrId+' table thead').show();
									for(var i=0;i<data.length;i++){
										if(data[i].attrType=="String"){
												$('#'+showAttrId+' table tbody').append("<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+"</td><td><input style=width:85% name="+Name+".attrVals['"+data[i].attrName+"'] id=ciShowAttVals'"+data[i].attrName+"'/></td></tr>")
												$('#ciShowAttVals'+data[i].attrName).validatebox({
													//required:true,
												});
										}else if(data[i].attrType=="Date"){	
												$('#'+showAttrId+' table tbody').append("<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+"</td><td><input style=width:85% name="+Name+".attrVals['"+data[i].attrName+"'] id=ciShowAttVals'"+data[i].attrName+"' readonly/></td></tr>")
												DatePicker97(['#ciShowAttVals'+data[i].attrName]);
										}else if(data[i].attrType=="Integer"){
												$('#'+showAttrId+' table tbody').append("<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+"</td><td><input style=width:85% name="+Name+".attrVals['"+data[i].attrName+"'] id=ciShowAttVals'"+data[i].attrName+"'/></td></tr>")
												$('#ciShowAttVals'+data[i].attrName).numberbox({
													//required:true,
													min:0,
													validType:length[1,9],
													max:999999999
												});
										}else if(data[i].attrType=="Double"){	
												$('#'+showAttrId+' table tbody').append("<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+"</td><td><input style=width:85% name="+Name+".attrVals['"+data[i].attrName+"'] id=ciShowAttVals'"+data[i].attrName+"'/></td></tr>")
												$('#ciShowAttVals'+data[i].attrName).numberbox({
													//required:true,
													min:0,
													validType:length[1,13],
													max:999999999.999,
													precision:3
												});
										}else if(data[i].attrType=="Lob"){
												$('#'+showAttrId+' table tbody').append("<tr><td style=text-align:left;width:25%>"+data[i].attrAsName+"</td><td><textarea  style=width:85% name="+Name+".attrVals['"+data[i].attrName+"'] id=ciShowAttVals'"+data[i].attrName+"' ></textarea></td></tr>")
												$('#ciShowAttVals'+data[i].attrName).validatebox({
													//required:true,
												});
										}
									}
								}else{
									$('#'+showAttrId+' table thead').hide();
									$('#'+showAttrId+' table tbody').append("<tr><td colspan=2 style='color:red'>"+i18n['label_notExtendedInfo']+"</td></tr>")
								}
							});
						} 
					}
				});
			}
			
			
		},
		/**
		 * @description 清空选择
		 * @param treeWin1 显示树的窗口id
		 * @param sid1 选中树时保存分类id
		 * @param sname1 选中树时保存分类名称
		 */
		cleanSelect:function(treeWin1,sid1,sname1){
			$('#'+sid1).val('');
			$('#'+sname1).val('');
			$('#'+treeWin1).dialog('close');
		}
	}
}();