$package("itsm.cim"); 
$import("common.config.dictionary.dataDictionaryUtil");
$import("common.config.category.eventCategoryTree");
$import("itsm.cim.getConfig");
/**  
 * @author QXY  
 * @constructor 
 * @description 软件
 * @date 2010-4-28
 * @since version 1.0 
 * @returns
 */
itsm.cim.softwareMain=function(){
	var options={};
	$.extend(options,{
		
	});
	this._opt="";
	this.loadDataAddFlag=true;
	this.loadDataSearchFlag=true;
	return{
		/**
		 * 软件列表
		 */
		softwareMainGrid:function(){
			var params = $.extend({},jqGridParams, {
				url:'ciSoftwareAction!findSoftwarePager.action',
				colNames:['ID',i18n['title_softName'],i18n['label_software_version'],i18n['label_software_type'],i18n['label_software_category'],i18n['supplier'],i18n['title_installDate'],
				          i18n['title_snmp_installState']
				          ,i18n['title']
						,i18n['label_software_identifyingNumber']
						,i18n['title_snmp_productID']
						,i18n['label_software_regOwner']
						,i18n['label_software_packageCache']
						,i18n['label_software_packageCode']
						,i18n['label_software_assignmentType']
						,i18n['label_software_LocalPackage']
						,i18n['label_software_InstallSource']
						,i18n['label_software_InstallLocation']
						,i18n['title_installDate']
						,i18n['label_software_Vendor']
						,i18n['label_software_PackageName']
						,i18n['label_software_Language']
						,i18n['label_software_regCompany']
						,i18n['label_software_SKUNumber']
				          ],
			 	colModel:[
			 	          {name:'softwareId',align:'center',width:45},
			 	          {name:'softwareName',align:'left',width:250,formatter:itsm.cim.softwareMain.softwareNameForma},
			 	          {name:'softwareVersion',align:'center',width:80,formatter:itsm.cim.softwareMain.softwareVersionForma},
			 	          {name:'softwareTypeName',index:'softwareType',align:'center',width:80,formatter:function(cellvalue, options, rowObject){
	  							 return colorFormatter(rowObject.softwareTypeColor,cellvalue);
						   }},
			 	          {name:'softwareCategoryName',index:'softwareCategory',align:'center',width:80},
			 	          {name:'softwareProviderName',index:'softwareProvider',align:'center',width:80,formatter:function(cellvalue, options, rowObject){
	  							 return colorFormatter(rowObject.softwareProviderColor,cellvalue);
						   }},
			 	          {name:'installDate',align:'center',width:80,formatter:itsm.cim.getConfig.dataForma},
			 	          {name:'installState',width:80,align:'center',sortable:false,hidden:true},
						  {name:'caption',width:80,align:'center',sortable:false,hidden:true},
						  {name:'identifyingNumber',width:80,align:'center',sortable:false,hidden:true},
						  {name:'productID',width:80,align:'center',sortable:false,hidden:true},
						  {name:'regOwner',width:80,align:'center',sortable:false,hidden:true},
						  {name:'packageCache',width:80,align:'center',sortable:false,hidden:true},
						  {name:'packageCode',width:80,align:'center',sortable:false,hidden:true},
						  {name:'assignmentType',width:80,align:'center',sortable:false,hidden:true},
						  {name:'localPackage',width:80,align:'center',sortable:false,hidden:true},
						  {name:'installSource',width:80,align:'center',sortable:false,hidden:true},
						  {name:'installLocation',width:80,align:'center',sortable:false,hidden:true},
						  {name:'installDate2',width:80,align:'center',sortable:false,hidden:true},
						  {name:'vendor',width:80,align:'center',sortable:false,hidden:true},
						  {name:'packageName',width:80,align:'center',sortable:false,hidden:true},
						  {name:'softwareLanguage',width:80,align:'center',sortable:false,hidden:true},
						  {name:'regCompany',width:80,align:'center',sortable:false,hidden:true},
						  {name:'skuNumber',width:80,align:'center',sortable:false,hidden:true}
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id: "softwareId"}),
				sortname:'softwareId',
				pager:'#softwareMainPager'
				});
				$("#softwareMainGrid").jqGrid(params);
				$("#softwareMainGrid").navGrid('#softwareMainPager',navGridParams);
				//列表操作项
				$("#t_softwareMainGrid").css(jqGridTopStyles);
				$("#t_softwareMainGrid").append($('#softwareMainToolbar').html());
				defaultLoadColumn("#softwareMainGrid");
				//自定义列
				$("#softwareMainGrid").jqGrid('navButtonAdd','#softwareMainPager',{
				    caption:"",
				    title:i18n['label_set_column'],
				    onClickButton : function (){
				    	loadColumnChooserItem('softwareMainGrid');
				    }
				});	
				//自适应宽度
				setGridWidth("#softwareMainGrid","regCenter",20);
		},
		softwareNameForma:function(cell,event,data){
			return data.softwareName;
		},
		softwareVersionForma:function(cell,event,data){
			return data.softwareVersion;
		},
		/**
		 * 软件新增
		 */
		softwareAdd:function(){
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('softwareType','#software_add_softwareType');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('supplier','#software_add_softwareProvider');
			$('#software_add_softwareCategoryName').unbind().click(function(){itsm.cim.softwareMain.selectSoftwareCategory('software_add_softwareCategoryName','software_add_softwareCategoryId')});
	
			_opt="saveSoftware";
			resetForm('#softwareMainAddWin form');
			windows('softwareMainAddWin',{width:600});
		},
		/**
		 * 软件编辑
		 */
		softwareEdit:function(){
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('softwareType','#software_add_softwareType');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('supplier','#software_add_softwareProvider');
			$('#software_add_softwareCategoryName').unbind().click(function(){itsm.cim.softwareMain.selectSoftwareCategory('software_add_softwareCategoryName','software_add_softwareCategoryId')});
		
			_opt="updateSoftware";
			
			setTimeout(function(){
				checkBeforeEditGrid('#softwareMainGrid',itsm.cim.softwareMain.softwareEditWin);
			},1000);
		},
		/**
		 * 软件编辑赋值
		 */
		softwareEditWin:function(rowData){
			$.post('ciSoftwareAction!findSoftwareInfo.action', 'softwareDTO.softwareId='+rowData.softwareId, function(data){
				var softwareName =data.softwareName;
				var softwareVersion = data.softwareVersion;
				if(softwareName == null){
					softwareName = 'null';
				}
				if(typeof softwareVersion == typeof data && softwareVersion == null){
					softwareVersion='null';
				}
				$('#software_add_softwareTypeId').val(data.softwareId);
				$('#software_add_softwareName').val(softwareName);
				$('#software_add_softwareVersion').val(softwareVersion);
				if(data.softwareTypeId!=null && data.softwareTypeId!='' && data.softwareProviderId!='null')
					$('#software_add_softwareType').val(data.softwareTypeId);
				
				if(data.softwareCategoryName!=null && data.softwareCategoryName!='' && data.softwareProviderId!='null')
					$('#software_add_softwareCategoryName').val(data.softwareCategoryName);
				else
					$('#software_add_softwareCategoryName').val('');
				
				if(data.softwareCategoryId!=null && data.softwareCategoryId!='' && data.softwareProviderId!='null')
					$('#software_add_softwareCategoryId').val(data.softwareCategoryId);
				else
					$('#software_add_softwareCategoryId').val('');
				
				if(data.softwareProviderId!=null && data.softwareProviderId!='' && data.softwareProviderId!='null')
					$('#software_add_softwareProvider').val(data.softwareProviderId);
				if(data.description!=null && data.description!='' && data.description!='null')
					$('#software_add_description').val(data.description);
				else
					$('#software_add_description').val('');
				if(data.installDate!=null && data.installDate!='' && data.installDate!='null')
					$('#software_add_installDate').val(itsm.cim.getConfig.dataForma(data.installDate));
				else
					$('#software_add_installDate').val('');
				windows('softwareMainAddWin',{width:600});
			});
			
			
		},
		/**
		 * 软件保存
		 */
		softwareSave:function(){
			var name = $('#software_add_softwareName').val();
			$('#software_add_softwareName').val(trim(name));
			if($('#softwareMainAddWin form').form('validate')){
				var _frm = $('#softwareMainAddWin form').serialize();
				var url = 'ciSoftwareAction!'+_opt+'.action';
				$.post(url, _frm, function(){
					msgShow(i18n['saveSuccess'],'show');
					$('#softwareMainAddWin').dialog('close');
					$('#softwareMainGrid').trigger('reloadGrid');
				});
			}
		},
		/**
		 * 软件删除
		 */
		softwareDelete:function(){
			checkBeforeDeleteGrid('#softwareMainGrid',itsm.cim.softwareMain.softwareDeleteOpt);
		},
		/**
		 * 软件删除执行
		 */
		softwareDeleteOpt:function(rowId){
			var param = $.param({'ids':rowId},true);
			$.post("ciSoftwareAction!deleteSoftware.action", param, function(){
					$('#softwareMainGrid').trigger('reloadGrid');
					msgShow(i18n['msg_deleteSuccessful'],'show');
			});
		},
		/**
		 * 软件搜索
		 */
		softwareSearch:function(){
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('softwareType','#software_search_softwareType');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('supplier','#software_search_softwareProvider');
			$('#software_search_softwareCategoryName').unbind().click(function(){itsm.cim.softwareMain.selectSoftwareCategory('software_search_softwareCategoryName','software_search_softwareCategoryId')});
		
			windows('softwareMainSearchWin',{width:600});
		},
		/**
		 * 软件搜索执行
		 */
		softwareSearchOpt:function(){
			var sdata=$('#softwareMainSearchWin form').getForm();
			var postData = $("#softwareMainGrid").jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);
			var _url = 'ciSoftwareAction!findSoftwarePager.action';
			$('#softwareMainGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid',[{"page":"1"}]);
		},
		
		/**
		 * 选择软件分类
		 */
		selectSoftwareCategory:function(name,id){
			common.config.category.eventCategoryTree.showSelectTree('#software_category_select_window'
					,'#software_category_select_tree'
					,'Software'
					,'#'+name
					,'#'+id);
		},
		/**
		 * 初始化
		 */
		init:function(){
			//绑定日期控件
			DatePicker97(['#software_add_installDate']);
			
			$("#softwareMain_loading").hide();
			$("#softwareMain_content").show();
			itsm.cim.softwareMain.softwareMainGrid();
			$('#link_softwareMain_add').click(itsm.cim.softwareMain.softwareAdd);
			$('#link_softwareMain_edit').click(itsm.cim.softwareMain.softwareEdit);
			$('#link_softwareMain_delete').click(itsm.cim.softwareMain.softwareDelete);
			$('#link_softwareMain_search').click(itsm.cim.softwareMain.softwareSearch);
			$('#link_softwareMain_search_opt').click(itsm.cim.softwareMain.softwareSearchOpt);
			$('#link_softwareMain_save').click(itsm.cim.softwareMain.softwareSave);
			
				
		}
	};
}();

$(document).ready(itsm.cim.softwareMain.init);