$package('itsm.cim');
$import("itsm.cim.configureItemUtil");
$import("itsm.cim.getConfig");
if(isProblemHave){
$import("itsm.problem.relatedProblem");
}
$import("common.config.dictionary.dataDictionaryUtil");
if(isRequestHave){
	$import("itsm.request.requestUtil");
}
$import("common.tools.event.eventAttachment");
$import('common.config.attachment.chooseAttachment');
$import('common.security.includes.includes');
$import("common.eav.attributes");
$import('common.config.formCustom.formControlImpl');
$import('itsm.cim.cimCommon');
$import('common.security.base64Util');
$import('common.config.formCustom.formControl');
/**  
 * @fileOverview this script is for "webapp/pages/configureItem/configureItemInfo.jsp"
 * @author jack
 * @version 1.0  
 **/  
 /**  
 * @author jack  
 * @constructor configureItemInfo
 * @description this script is for "webapp/pages/configureItem/configureItemInfo.jsp"
 * @date 2010-09-29
 * @since version 1.0 
 **/
itsm.cim.configureItemInfoFormCustom=function(){
	//加载标识
	this.loadRelevanceCIFlag="0";
	this.loadHistoryCallFlag="0";
	this.loadHistoryProblemFlag="0";
	this.loadHistoryChangeFlag="0";
	this.loadInstallSoftFlag="0";
	this.loadHardwareFlag="0";
	//变更详细页面数据列表集合
	this.configureItemInfoGrids=[];
	this.opt='';
	return{
		detailTabsHtml:function(attrNos,eventEavVals,htmlDivId){
			$.post('attrs!findAttributesByAttrNos.action','attrNos='+attrNos,function(data){
				if(data!==null && data.length>0 && data!==''){
					for(var i=0;i<data.length;i++){
						var value='';
						var attrName=data[i].attrName;
						if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
							value = eval('eventEavVals.'+attrName);
						}
						$(htmlDivId+" :input[name*='"+attrName+"']").closest('.field,.field_lob').html(value);
					}
					itsm.cim.configureItemInfoFormCustom.initBorder(htmlDivId);
					$(htmlDivId+" .label .required_div").remove();
				}
				$(htmlDivId).show();
			});
		},
		initDetailTabs:function(res,eventEavVals){
			var data = res.formCustomTabsDtos;
			for ( var i = 0; i < data.length; i++) {
				var tabId =data[i].tabId;
				var name =data[i].tabName;
				var attrNos = data[i].tabAttrNos;
				var htmlDivId = 'detailCim_tabs_formField_'+tabId;
				var formCustomContents = common.config.formCustom.formControlImpl.editHtml(data[i].tabAttrsConent,htmlDivId);
				$('#configureItemInfoTab').tabs('add',{
					title:name,
					cache:"true",
					content:"<div id='"+htmlDivId+"' style='display: none;float: left; width: 98%;padding-left:3% '>"+formCustomContents+"</div>",
					active: 1
				});
				$.parser.parse($('#'+htmlDivId));//加上这句，必填才有用
				itsm.cim.configureItemInfoFormCustom.detailTabsHtml(attrNos,eventEavVals,'#'+htmlDivId);
			}
		},
		loadCimFormHtmlByFormId:function(data,htmlDivId){
			$("#"+htmlDivId).prepend($("#ciCategory_div").html());
			if(!data.isNewForm){
				 $.post("ciCategory!findByCategoryId.action","categoryNo="+data.categoryNo,function(res){
					 common.eav.attributes.findAttributeByCim_New($('#configureCiInfoId').val(),'itsm.request',res,'ciDto',data.attrVals,function(json){
						 if(json){
							var jsonStr=common.security.base64Util.encode(JSON.stringify(json));
							$("#"+htmlDivId).append(//'<div class="field_options"><div class="label"></div><div class="field"></div></div>'+
									common.config.formCustom.formControlImpl.detailHtml(jsonStr));
						 }
						 itsm.cim.cimCommon.setfieldOptionsLobCss("#"+htmlDivId);
						 itsm.cim.configureItemInfoFormCustom.initBorder("#"+htmlDivId);
					},"detail");
				 }); 
			 }else{
				itsm.cim.configureItemInfoFormCustom.initBorder("#"+htmlDivId);
			}
			itsm.cim.cimCommon.setCimParamValueToOldFormByDetail("#"+htmlDivId,data);
			itsm.cim.cimCommon.setCimDataDictionarayValueByDetail("#"+htmlDivId,data);
			itsm.cim.cimCommon.initDefaultFormByAttr(htmlDivId);
			$("#"+htmlDivId+" .label .required_div").remove();
			$("#"+htmlDivId).show();
		},
		initDetailForm:function(){
			var htmlDivId = "cidetail_formField";
			var ciId=$('#configureCiInfoId').val();
			$.post('ci!findDetailDTOByCiId.action?ciEditId='+ciId,function(data){
				$("#ciCategory_value").text(data.categoryName);
				$("#"+htmlDivId).hide();
				$.post("formCustom!findFormCustomByCiCategoryNo.action","formCustomDTO.ciCategoryNo="+data.categoryNo,function(res){
					if(res!=null&&res.formCustomContents!=""){
						itsm.cim.configureItemInfoFormCustom.initDetailTabs(res,data.attrVals);
						var formCustomContents = common.config.formCustom.formControlImpl.editHtml(res.formCustomContents,htmlDivId);
						$("#"+htmlDivId).html($("#ciCategory_div").html()+formCustomContents);
						itsm.cim.configureItemInfoFormCustom.loadDetailHtml(data,data.eavNo,"#"+htmlDivId,data.attrVals);
						$("#"+htmlDivId+" .label .required_div").remove();
					}else{
						var durl='common/config/formCustom/ciDefaultField_Old.jsp';
						if(data.isNewForm){
							durl='common/config/formCustom/ciDefaultField.jsp';
						}
						$("#"+htmlDivId).load(durl,function(){
							itsm.cim.configureItemInfoFormCustom.loadCimFormHtmlByFormId(data,htmlDivId);
						});
					}
				});
			});
		},
		loadDetailHtml:function(data,eavNo,htmlDivId,eventEavVals){
			itsm.cim.cimCommon.setCimFormValuesByDetail(data,htmlDivId,'itsm.configureItem',eavNo,eventEavVals,function(){
				itsm.cim.configureItemInfoFormCustom.initBorder(htmlDivId);
			});
		},
		/**
		 * 初始化边框
		 */
		initBorder:function(htmlDivId){
			setTimeout(function(){//延迟加载
				$(htmlDivId+' .label span').remove();
				$(htmlDivId).show();
				//$("#detail_formField > div[class=field_options] > div[class=label]:odd").css("border-left","none");
				var arrys = new Array();
				if($(htmlDivId+" > div[class='field_options']").size()==0){//判断是否是一列显示
					$(htmlDivId+" > div").css("border-left","1px solid #EEE");
					$(htmlDivId+" > div[class='field_options']").attr("class","field_options_2Column field_options").css("border-left","1px solid #EEE");
					mark=false;
				}else{
					$(htmlDivId).prepend($('#detail_codeAndServices').html());
//					$(htmlDivId).append($("#assignInfo").html());
					var count_element = $(htmlDivId+" > div").size();
					$.each($(htmlDivId+" > div"),function(ind,val){
						if($(this).attr("class")==="field_options_lob field_options" || count_element === (ind+1)){
							if(arrys.length % 2 == 0)
								$(this).css("border-left","1px solid #EEE");
							for(var i = 0 ; i < arrys.length ; i ++){
								if((i+1) % 2 != 0)
									$(arrys[i]).css("border-left","1px solid #EEE");
							}
							arrys.splice(0);
						}else{
							arrys.push(this);
						}
			        });
				}
			},0);
		},
		/**
		 * @description 配置信息tab面板 为tab点击绑定事件
		 */
		configureInfoTab:function(){
	        $('#configureItemInfoTab').tabs({
	            onSelect:itsm.cim.configureItemInfoFormCustom.configureInfoTabClickEvents  
	        });
		},
		/**
		 * @description 点击tab后，根据tab标题加载不同的数据
		 */
		configureInfoTabClickEvents:function(title){
			if(title==i18n['relatedCI']){
				if(loadRelevanceCIFlag=="0"){
					itsm.cim.configureItemInfoFormCustom.relevanceCI($('#configureCiInfoId').val());
					loadRelevanceCIFlag="1";
				}else{
				}
				common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('ciRelationType','#relevance_ciRelationType');
			}
			if(title==i18n['historyRequest']){
				if(loadHistoryCallFlag=="0"){
					itsm.cim.configureItemInfoFormCustom.historyCall();
					loadHistoryCallFlag="1"
				}else{
				}
			}
	
			if(title==i18n['historyProblem']){
				itsm.problem.relatedProblem.showRelatedProblem('ci',$('#configureCiInfoId').val(),'#historyProblemGrid','#historyProblemPager');
			}
			if(title==i18n['title_historyChange']){
				if(loadHistoryChangeFlag=="0"){
					itsm.cim.configureItemInfoFormCustom.historyChangeList();
					loadHistoryChangeFlag="1"
				}else{
					
				}
			}
			if(title==i18n['title_configInfo']){
				
				itsm.cim.getConfig.showConfigInfo();
			}
			if(title==i18n['title_installSoft']){
				if(loadInstallSoftFlag=="0"){
					itsm.cim.getConfig.showInstallSoftware();
					loadInstallSoftFlag="1"
				}else{
				}
			}
			//硬件
			if(title==i18n['label_hardware']){
				itsm.cim.configureItemInfoFormCustom.findHardware();
			}
			//二维码
			if(title==i18n['label_ci_scan_tab']){
			    var ciId = $('#configureCiInfoId').val();
			   $('#cust_QRcode_id').html('<img id="input_QRcode_id" src="ciScanAction!findImage.action?ciScanDTO.ciId='+ciId+'&random='+new Date()+'" onerror="itsm.cim.configureItemInfoFormCustom.imgClose()"/>');
				$.post('ci!findConfigureItenHardware.action', {'ciQueryDTO.ciId':ciId}, function(data){
					$('#scan_CIip').text(data.netWork_ip);
				});
				
				/*$('#cust_QRcode_id').html('<img id="input_QRcode_id" src="../QRCodePath/img_'+$('#configureCiInfoId').val()+'.png?random='+new Date()+'" onerror="itsm.cim.configureItemInfoFormCustom.imgClose()"/>');
				$.post('ci!findConfigureItenHardware.action', {'ciQueryDTO.ciId':$('#configureCiInfoId').val()}, function(data){
					$('#scan_CIip').text(data.netWork_ip);
				});*/
			}
			//历史记录
			if(title==i18n['label_customReport_history']){
				itsm.cim.configureItemInfoFormCustom.historyUpdatelist();
			}
			if(title=='修改行为'){
				itsm.cim.configureItemInfoFormCustom.behaviorModificationlist();
			}
		},
		
		
		/**
		 * 查询硬件详细
		 */
		findHardware:function(){
			
			$.post('ci!findConfigureItenHardware.action', {'ciQueryDTO.ciId':$('#configureCiInfoId').val()}, function(data){

				if(data.bios_manufacturer==='H3C' || data.bios_manufacturer==='Cisco' || data.bios_manufacturer==='Huawei'){
					$("#ciInfoTwo").css("display","");
					$('#operatingSystem_lastBootUpTime1').text(data.operatingSystem_lastBootUpTime);
					//$('#operatingSystem_totalVisibleMemorySize1').text(data.operatingSystem_totalVisibleMemorySize);
					$('#computerSystem_name1').text(data.computerSystem_name);
					$('#computerSystem_model1').text(data.computerSystem_model);
					$('#operatingSystem_version1').text(data.operatingSystem_version);
					$('#computerSystem_userName1').text(data.bios_manufacturer);
					$('#operatingSystem_serialNumber1').text(data.operatingSystem_serialNumber);
					$('#netWork_ip1').text(data.netWork_ip);
					//接口
					if(data.other!=null && data.other!='' && data.other!='null'){
						$('#interfacesCi table').html('');
						var interfaces=data.other;
						var interfacesJson = $.parseJSON(interfaces);
						$('#interfacesCi table').append("<tr><th>"+i18n['title_snmp_Index']+"</th><th>"+i18n['title_snmp_description']+"</th><th>"+i18n['title_snmp_type']+"</th><th>MTU</th><th>"+i18n['title_snmp_speed']+"</th>"+
						"<th>MAC</th><th>"+i18n['title_snmp_manageState']+"</th><th>"+i18n['title_snmp_operationState']+"</th><th>"+i18n['title_snmp_lastChangeTime']+"</th></tr>");
						$.each(interfacesJson,function(i,item){
							var rem="";
							switch(item.type){
							    case "1":
							    	rem="<td>other("+item.type+")</td>";
							    	break;
							    case "2":
							    	rem="<td>regular1822("+item.type+")</td>";
							    	break;
							    case "3":
							    	rem="<td>hdh1822("+item.type+")</td>";
							    	break;
							    case "4":
							    	rem="<td>ddn-x25("+item.type+")</td>";
							    	break;
							    case "5":
							    	rem="<td>rfc877-x25("+item.type+")</td>";
							    	break;
							    case "6":
							    	rem="<td>ethernet-csmacd("+item.type+")</td>";
							    	break;
							    case "7":
							    	rem="<td>iso88023-csmacd("+item.type+")</td>";
							    	break;
							    case "8":
							    	rem="<td>iso88024-tokenBus("+item.type+")</td>";
							    	break;
							    case "9":
							    	rem="<td>iso88025-tokenRing("+item.type+")</td>";
							    	break;
							    case "10":
							    	rem="<td>iso88026-man("+item.type+")</td>";
							    	break;
							    case "11":
							    	rem="<td>starLan("+item.type+")</td>";
							    	break;
							    case "12":
							    	rem="<td>proteon-10Mbit("+item.type+")</td>";
							    	break;
							    case "13":
							    	rem="<td>proteon-80Mbit("+item.type+")</td>";
							    	break;
							    case "14":
							    	rem="<td>hyperchannel("+item.type+")</td>";
							    	break;
							    case "15":
							    	rem="<td>fddi("+item.type+")</td>";
							    	break;
							    case "16":
							    	rem="<td>lapb("+item.type+")</td>";
							    	break;
							    case "17":
							    	rem="<td>sdlc("+item.type+")</td>";
							    	break;
							    case "18":
							    	rem="<td>ds1("+item.type+")</td>";
							    	break;
							    case "19":
							    	rem="<td>e1("+item.type+")</td>";
							    	break;
							    case "20":
							    	rem="<td>basicISDN("+item.type+")</td>";
							    	break;
							    case "21":
							    	rem="<td>primaryISDN("+item.type+")</td>";
							    	break;
							    case "22":
							    	rem="<td>propPointToPointSerial("+item.type+")</td>";
							    	break;
							    case "23":
							    	rem="<td>ppp("+item.type+")</td>";
							    	break;
							    case "24":
							    	rem="<td>softwareLoopback("+item.type+")</td>";
							    	break;
							    case "25":
							    	rem="<td>eon("+item.type+")</td>";
							    	break;
							    case "26":
							    	rem="<td>ethernet-3Mbit("+item.type+")</td>";
							    	break;
							    case "27":
							    	rem="<td>nsip("+item.type+")</td>";
							    	break;
							    case "28":
							    	rem="<td>slip("+item.type+")</td>";
							    	break;
							    case "29":
							    	rem="<td>ultra("+item.type+")</td>";
							    	break;
							    case "30":
							    	rem="<td>ds3("+item.type+")</td>";
							    	break;
							    case "31":
							    	rem="<td>sip("+item.type+")</td>";
							    	break;
							    case "32":
							    	rem="<td>frame-relay("+item.type+")</td>";
							    	break;
							    case "33":
							    	rem="<td>rs232(33)</td>";
						    		break;                   
							    case "36":
							    	rem="<td>cnetPlus(36)</td>";
						    		break; 
							    case "37":
							    	rem="<td> atm(37)</td>";
						    		break; 
							    case "38":
							    	rem="<td> miox25(38)</td>";
						    		break; 
							    case "39":
							    	rem="<td> sonet(39)</td>";
						    		break;  
							    case "40":
							    	rem="<td>  x25ple(40)</td>";
						    		break; 
							    case "41":
							    	rem="<td>iso88022llc(41)</td>";
						    		break; 
							    case "42":
							    	rem="<td>localTalk(42)</td>";
						    		break; 
							    case "43":
							    	rem="<td>smdsDxi(43)</td>";
						    		break; 
							    case "44":
							    	rem="<td>frameRelayService(44)</td>";
						    		break; 
							    case "45":
							    	rem="<td>v35(45)</td>";
						    		break; 
							    case "46":
							    	rem="<td>hssi(46)</td>";
						    		break; 
							    case "47":
							    	rem="<td> hippi(47)</td>";
						    		break; 
							    case "48":
							    	rem="<td>modem(48)</td>";
						    		break; 
							    case "49":
							    	rem="<td>aal5(49)</td>";
						    		break; 
							    case "50":
							    	rem="<td>sonetPath(50)</td>";
						    		break; 
							    case "51":
							    	rem="<td> sonetVT(51)</td>";
						    		break; 
							    case "52":
							    	rem="<td>smdsIcip(52)</td>";
						    		break; 
							    case "53":
							    	rem="<td>propVirtual(53)</td>";
						    		break; 
							    case "54":
							    	rem="<td>propMultiplexor(54)</td>";
						    		break; 
							    case "55":
							    	rem="<td>ieee80212(55)</td>";
						    		break; 
							    case "56":
							    	rem="<td>fibreChannel(56)</td>";
						    		break; 
							    case "57":
							    	rem="<td>hippiInterface(57)</td>";
						    		break;          
							    case "58":
							    	rem="<td>frameRelayInterconnect(58)</td>";
						    		break; 
							    case "59":
							    	rem="<td>aflane8023(59)</td>";
						    		break; 
							    case "60":
							    	rem="<td>aflane8025(60)</td>";
						    		break; 
							    case "61":
							    	rem="<td>cctEmul(61)</td>";
						    		break;                    
							    case "62":
							    	rem="<td>fastEther(62)</td>";
						    		break; 
							    case "63":
							    	rem="<td>isdn(63)</td>";
						    		break;                      
							    case "64":
							    	rem="<td>v11(64)</td>";
						    		break;                          
							    case "65":
							    	rem="<td>v36(65)</td>";
						    		break;                                    
							    case "66":
							    	rem="<td>g703at64k(66)</td>";
						    		break; 
							    case "67":
							    	rem="<td>g703at2mb(67)</td>";
						    		break; 
							    case "68":
							    	rem="<td>qllc(68)</td>";
						    		break;                                        
							    case "69":
							    	rem="<td>fastEtherFX(69)</td>";
						    		break; 
							    case "70":
							    	rem="<td>channel(70)</td>";
						    		break;                                          
							    case "71":
							    	rem="<td>ieee80211(71)</td>";
						    		break;              
							    case "72":
							    	rem="<td>ibm370parChan(72)</td>";
						    		break; 
							    case "73":
							    	rem="<td>escon(73)</td>";
						    		break; 
							    case "74":
							    	rem="<td>dlsw(74)</td>";
						    		break; 
							    case "75":
							    	rem="<td>isdns(75)</td>";
						    		break; 
							    case "76":
							    	rem="<td>isdnu(76)</td>";
						    		break; 
							    case "77":
							    	rem="<td>lapd(77)</td>";
						    		break; 
							    case "78":
							    	rem="<td>ipSwitch(78)</td>";
						    		break; 
							    case "79":
							    	rem="<td>rsrb(79)</td>";
						    		break; 
							    case "80":
							    	rem="<td>atmLogical(80)</td>";
						    		break; 
							    case "81":
							    	rem="<td> ds0(81)</td>";
						    		break; 
							    case "82":
							    	rem="<td>ds0Bundle(82)</td>";
						    		break; 
							    case "83":
							    	rem="<td>bsc(83)</td>";
						    		break; 
							    case "84":
							    	rem="<td>async(84)</td>";
						    		break; 
							    case "85":
							    	rem="<td>cnr(85)</td>";
						    		break; 
							    case "86":
							    	rem="<td>iso88025Dtr(86)</td>";
						    		break; 
							    case "87":
							    	rem="<td>eplrs(87)</td>";
						    		break; 
							    case "88":
							    	rem="<td>arap(88)</td>";
						    		break; 
							    case "89":
							    	rem="<td>propCnls(89)</td>";
						    		break; 
							    case "90":
							    	rem="<td>hostPad(90)</td>";
						    		break; 
							    case "91":
							    	rem="<td>termPad(91)</td>";
						    		break; 
							    case "92":
							    	rem="<td>frameRelayMPI(92)</td>";
						    		break; 
							    case "93":
							    	rem="<td>x213(93)</td>";
						    		break; 
							    case "94":
							    	rem="<td>adsl(94)</td>";
						    		break; 
							    case "95":
							    	rem="<td>radsl(95)</td>";
						    		break; 
							    case "96":
							    	rem="<td> sdsl(96)</td>";
						    		break; 
							    case "97":
							    	rem="<td> vdsl(97)</td>";
						    		break; 
							    case "98":
							    	rem="<td>iso88025CRFPInt(98)</td>";
						    		break; 
							    case "99":
							    	rem="<td>myrinet(99)</td>";
						    		break; 
							    case "100":
							    	rem="<td>voiceEM(100)</td>";
						    		break; 
							    case "101":
							    	rem="<td>voiceFXO(101)</td>";
						    		break; 
							    case "102":
							    	rem="<td>voiceFXS(102)</td>";
						    		break; 
							    case "103":
							    	rem="<td>voiceEncap(103)</td>";
						    		break; 
							    case "104":
							    	rem="<td>voiceOverIp(104)</td>";
						    		break; 
							    case "105":
							    	rem="<td>atmDxi(105)</td>";
						    		break; 
							    case "106":
							    	rem="<td>atmFuni(106)</td>";
						    		break; 
							    case "107":
							    	rem="<td>atmIma(107)</td>";
						    		break;
							    case "108":
							    	rem="<td>pppMultilinkBundle(108)</td>";
						    		break;
							    case "109":
							    	rem="<td>ipOverCdlc(109)</td>";
						    		break;
							    case "110":
							    	rem="<td>ipOverClaw(110)</td>";
						    		break;
							    case "111":
							    	rem="<td>stackToStack(111)</td>";
						    		break;
							    case "112":
							    	rem="<td>virtualIpAddress(112)</td>";
						    		break;
							    case "117":
							    	rem="<td>gabitEthernet (117)</td>";
						    		break; 
							    case "118":
							    	rem="<td>hdlc(118)</td>";
						    		break; 
							    case "119":
							    	rem="<td>lapf(119)</td>";
						    		break; 
							    case "120":
							    	rem="<td>v37 (120)</td>";
						    		break; 
							    case "121":
							    	rem="<td>x25mlp(121)</td>";
						    		break; 
							    case "122":
							    	rem="<td>x25huntGroup(122)</td>";
						    		break; 
							    case "123":
							    	rem="<td>trasnpHdlc(123)</td>";
						    		break; 
							    case "124":
							    	rem="<td>interleave(124)</td>";
						    		break; 
							    case "125":
							    	rem="<td>fast(125)</td>";
						    		break; 
							    case "126":
							    	rem="<td>ip(126)</td>";
						    		break; 
							    case "127":
							    	rem="<td>docsCableMaclayer(127)</td>";
						    		break; 
							    case "128":
							    	rem="<td>docsCableDownstream(128)</td>";
						    		break; 
							    case "129":
							    	rem="<td>docsCableUpstream(129)</td>";
						    		break; 
							    case "130":
							    	rem="<td>a12MppSwitch(130)</td>";
						    		break; 
							    case "131":
							    	rem="<td>tunnel(131)</td>";
						    		break; 
							    case "132":
							    	rem="<td>coffee(132)</td>";
						    		break; 
							    case "133":
							    	rem="<td>ces(133)</td>";
						    		break; 
							    case "134":
							    	rem="<td>atmSubInterface(134)</td>";
						    		break; 
							    case "135":
							    	rem="<td>l2vlan(135)</td>";
						    		break; 
							    case "136":
							    	rem="<td>l3ipvlan(136)</td>";
						    		break; 
							    case "137":
							    	rem="<td>l3ipxvlan(137)</td>";
						    		break; 
							    case "138":
							    	rem="<td>digitalPowerline(138)</td>";
						    		break; 
							    case "139":
							    	rem="<td>mediaMailOverIp(139)</td>";
						    		break; 
							    case "140":
							    	rem="<td>dtm(140)</td>";
						    		break; 
							    case "141":
							    	rem="<td>dcn(141)</td>";
						    		break; 
							    case "142":
							    	rem="<td>ipForward(142)</td>";
						    		break; 
							    case "143":
							    	rem="<td>msdsl(143)</td>";
						    		break; 
							    case "144":
							    	rem="<td>ieee1394(144)</td>";
						    		break; 
							    case "145":
							    	rem="<td>if-gsn(145)</td>";
						    		break;  
							    case "146":
							    	rem="<td>dvbRccMacLayer(146)</td>";
						    		break; 
							    case "147":
							    	rem="<td>dvbRccDownstream(147)</td>";
						    		break; 
							    case "148":
							    	rem="<td>dvbRccUpstream(148)</td>";
						    		break;
							    case "149":
							    	rem="<td>atmVirtual(149)</td>";
						    		break;
							    case "150":
							    	rem="<td>mplsTunnel(150)</td>";
						    		break;
							    case "151":
							    	rem="<td>srp(151)</td>";
						    		break;
							    default:
							    	rem="<td>"+item.type+"</td>";
						    		break;
						    
							}
							var manageState="";
							switch(item.manageState){
						    case "1":
						    	manageState="up("+item.manageState+")";
						    	break;
						    case "2":
						    	manageState="down("+item.manageState+")";
						    	break;
						    case "3":
						    	manageState="testing("+item.manageState+")";
						    	break;
						    default :
						    	manageState=item.manageState;
						    	break;
							}
							var operationState="";
							switch(item.operationState){
							    case "1":
							    	operationState="up("+item.operationState+")";
							    	break;
							    case "2":
							    	operationState="down("+item.operationState+")";
							    	break;
							    case "3":
							    	operationState="testing("+item.operationState+")";
							    	break;
							    case "4":
							    	operationState="unknown("+item.operationState+")";
							    	break;
							    case "5":
							    	operationState="dormant("+item.operationState+")";
							    	break;
							    case "6":
							    	operationState="notPresent("+item.operationState+")";
							    	break;
							    case "7":
							    	operationState="lowerLayerDown("+item.operationState+")";
							    	break;
							    default :
							    	operationState=item.operationState;
							    	break;
							}
							$('#interfacesCi table').append("<tr><td>"+(i+1)+"</td><td>"+item.description+"</td>"+rem+"<td>"+item.MTU+"</td>" +
									"<td>"+item.speed+"</td><td>"+item.MAC+"</td><td>"+manageState+"</td>" +
											"<td>"+operationState+"</td><td>"+item.lastChangeTime+"</td></tr>");
						});
					}
				}else{
					$("#ciInfoOne").css("display","");
					$('#operatingSystem_lastBootUpTime').text(data.operatingSystem_lastBootUpTime);
					$('#operatingSystem_totalVisibleMemorySize').text(data.operatingSystem_totalVisibleMemorySize);
					$('#operatingSystem_totalVirtualMemorySize').text(data.operatingSystem_totalVirtualMemorySize);
					$('#computerSystem_name').text(data.computerSystem_name);
					$('#computerSystem_model').text(data.computerSystem_model);
					$('#operatingSystem_version').text(data.operatingSystem_version);
					$('#computerSystem_userName').text(data.computerSystem_userName);
					$('#netWork_ip').text(data.netWork_ip);
					$('#computerSystem_domain').text(data.computerSystem_domain);
					$('#operatingSystem_serialNumber').text(data.operatingSystem_serialNumber);
					$('#operatingSystem_caption').text(data.operatingSystem_caption);
					$('#operatingSystem_osArchitecture').text(data.operatingSystem_osArchitecture);
					$('#operatingSystem_csdVersion').text(data.operatingSystem_csdVersion);
					$('#operatingSystem_installDate').text(data.operatingSystem_installDate);
					$('#baseBoard_name').text(data.baseBoard_name);
					$('#baseBoard_serialNumber').text(data.baseBoard_serialNumber);
					$('#baseBoard_manufacturer').text(data.baseBoard_manufacturer);
					$('#bios_serialNumber').text(data.bios_serialNumber);
					$('#bios_name').text(data.bios_name);
					$('#bios_manufacturer').text(data.bios_manufacturer);
					$('#bios_version').text(data.bios_version);
					$('#bios_releaseDate').text(data.bios_releaseDate);
					$('#processor_name').text(data.processor_name);
					$('#processor_maxClockSpeed').text(data.processor_maxClockSpeed);
					$('#processor_l2CacheSize').text(data.processor_l2CacheSize);
					$('#processor_l3CacheSize').text(data.processor_l3CacheSize);
					$('#processor_level').text(data.processor_level);
					$('#netWork_mac').text(data.netWork_mac);
					$('#netWork_name').text(data.netWork_name);
					$('#netWork_dhcp').text(data.netWork_dhcp);
					$('#netWork_dhcpServer').text(data.netWork_dhcpServer);
					$('#desktopMonitor_name').text(data.desktopMonitor_name);
					$('#desktopMonitor_screenHeight').text(data.desktopMonitor_screenHeight);
					$('#desktopMonitor_screenWidth').text(data.desktopMonitor_screenWidth);
					$('#desktopMonitor_monitorManufacturer').text(data.desktopMonitor_monitorManufacturer);
					$('#desktopMonitor_serialNumber').text(data.desktopMonitor_serialNumber);
					$('#pointingDevice_Name').text(data.pointingDevice_Name);
					$('#pointingDevice_description').text(data.pointingDevice_description);
					$('#pointingDevice_serialNumber').text(data.pointingDevice_serialNumber);
					$('#pointingDevice_manufacturer').text(data.pointingDevice_manufacturer);
					$('#keyboard_Name').text(data.keyboard_Name);
					$('#keyboard_description').text(data.keyboard_description);
					$('#keyboard_numberOfFunctionKeys').text(data.keyboard_numberOfFunctionKeys);
					$('#keyboard_serialNumber').text(data.keyboard_serialNumber);
					$('#keyboard_manufacturer').text(data.keyboard_manufacturer);
					
					//逻辑盘
					if(data.logicalDisk!=null && data.logicalDisk!='' && data.logicalDisk!='null'){
						$('#logicalDisk table').html('');
						var logicalDisk=data.logicalDisk;
						var logicalDiskJson =$.parseJSON(logicalDisk);
						$('#logicalDisk table').append("<tr><td>名称</td><td>描述</td><td>系统文件</td><td>可用空间</td><td>总大小</td></tr>");
						$.each(logicalDiskJson,function(i,item){
							$('#logicalDisk table').append("<tr><td>"+item.Caption+"</td><td>"+item.Description+"</td><td>"+item.FileSystem+"</td><td>"+item.FreeSpace+" (byte)</td><td>"+item.Size+" (byte)</td></tr>");
						});
					}
					//物理内存
					if(data.physicalMemory!=null && data.physicalMemory!='' && data.physicalMemory!='null'){
						$('#physicalMemory table').html('');
						var physicalMemory=data.physicalMemory;
						var physicalMemoryJson = $.parseJSON(physicalMemory);
						$('#physicalMemory table').append("<tr><td>名称</td><td>速度</td><td>设备位置</td><td>容量</td></tr>");
						$.each(physicalMemoryJson,function(i,item){
							$('#physicalMemory table').append("<tr><td>"+item.Name+"</td><td>"+item.Speed+" (byte)</td><td>"+item.DeviceLocator+"</td><td>"+item.Capacity+" (byte)</td></tr>");
						});
					}
					//驱动器
					if(data.diskDrive!=null && data.diskDrive!='' && data.diskDrive!='null'){
						$('#diskDrive table').html('');
						var diskDrive=data.diskDrive;
						var diskDriveJson = $.parseJSON(diskDrive);
						$('#diskDrive table').append("<tr><td>名称</td><td>生产厂家</td><td>接口类型</td><td>模型</td></tr>");
						$.each(diskDriveJson,function(i,item){
							$('#diskDrive table').append("<tr><td>"+item.Caption+"</td><td>"+item.Manufacturer+"</td><td>"+item.InterfaceType+"</td><td>"+item.Model+"</td></tr>");
						});
					}
					//光驱驱动器
					if(data.cdRomDrive!=null && data.cdRomDrive!='' && data.cdRomDrive!='null'){
						$('#cdRomDrive table').html('');
						var cdRomDrive=data.cdRomDrive;
						var cdRomDriveJson = $.parseJSON(cdRomDrive);
						$('#cdRomDrive table').append("<tr><td>名称</td><td>多媒体类型</td></tr>");
						$.each(cdRomDriveJson,function(i,item){
							$('#cdRomDrive table').append("<tr><td>"+item.Name+"</td><td>"+item.MediaType+"</td></tr>");
						});
					}
					//IDE控制器
					if(data.ideController!=null && data.ideController!='' && data.ideController!='null'){
						$('#ideController table').html('');
						var ideController=data.ideController;
						var ideControllerJson = $.parseJSON(ideController);
						$('#ideController table').append("<tr><td>名称</td><td>生产厂家</td></tr>");
						$.each(ideControllerJson,function(i,item){
							$('#ideController table').append("<tr><td>"+item.Name+"</td><td>"+item.Manufacturer+"</td></tr>");
						});
					}
					
					//USB控制器
					if(data.usbController!=null && data.usbController!='' && data.usbController!='null'){
						$('#usbController table').html('');
						var usbController=data.usbController;
						var usbControllereJson = $.parseJSON(usbController);
						$('#usbController table').append("<tr><td>名称</td></tr>");
						$.each(usbControllereJson,function(i,item){
							$('#usbController table').append("<tr><td>"+item.Name+"</td></tr>");
						});
					}
					//USBHub
					if(data.usbHub!=null && data.usbHub!='' && data.usbHub!='null'){
						$('#usbHub table').html('');
						var usbHub=data.usbHub;
						var usbHubJson = $.parseJSON(usbHub);
						$('#usbHub table').append("<tr><td>名称</td></tr>");
						$.each(usbHubJson,function(i,item){
							$('#usbHub table').append("<tr><td>"+item.Name+"</td></tr>");
						});
					}
					//端口(COM)
					if(data.serialPort!=null && data.serialPort!='' && data.serialPort!='null'){
						$('#serialPort table').html('');
						var serialPort=data.serialPort;
						var serialPortJson = $.parseJSON(serialPort);
						$('#serialPort table').append("<tr><td>名称</td><td>供应商</td></tr>");
						$.each(serialPortJson,function(i,item){
							$('#serialPort table').append("<tr><td>"+item.Name+"</td><td>"+item.ProviderType+"</td></tr>");
						});
					}	
					//端口(LPT)
					if(data.parallelPort!=null && data.parallelPort!='' && data.parallelPort!='null'){
						$('#parallelPort table').html('');
						var parallelPort=data.parallelPort;
						var parallelPortJson = $.parseJSON(parallelPort);
						$('#parallelPort table').append("<tr><td>名称</td><td>描述</td></tr>");
						$.each(parallelPortJson,function(i,item){
							$('#parallelPort table').append("<tr><td>"+item.Name+"</td><td>"+item.Description+"</td></tr>");
						});
					}
				}
			});
		},
		/**
		 * @description 配置项更新
		 */
		configureItemUpdate:function(ciRelevanceId){
			$("#configureItemInfoRelevanceCI").jqGrid('setGridParam',{page:1,url:'itsm/cim/ciRelevance!find.action?ciRelevanceId='+ciRelevanceId}).trigger('reloadGrid');
		},
		/**
		 * @description 链接到configureItemAdd.jsp页面
		 */
		configureItemEdit:function(){
			var url = 'ci!findItemByIdToEditOrAdd.action';
			var ciId = $("#configureCiId").val();
			$.post('ci!existCIById.action', {'ciEditId':ciId}, function(existCI){
				if(existCI){
			        url = url + '?ciDto.ciId='+ciId;
			        basics.tab.tabUtils.addTab(i18n['ci_editConfigureItem'], url, 'Edit');
				}else{
					msgAlert(i18n['msg_atLeastChooseOneData'],'warning');
				}
			}, "json");
		},
		
		/**
		 * @description 历史请求
		 */
		historyCall:function(){
			var params=$.extend({},jqGridParams,{
				url:'request!findRequests.action?requestQueryDTO.CiId='+$('#configureCiInfoId').val()+'&requestQueryDTO.currentUser='+userName+'&requestQueryDTO.lastUpdater='+userName,
				colNames:[i18n['number'],i18n['title'],i18n['requester'],i18n['status']],
				colModel:[
						  {name:'eno',width:120},
						  {name:'etitle',width:200},
						  {name:'createdByName',width:160,sortable:false},
						  {name:'statusName',width:155,sortable:false}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "eno"}),
				sortname:'eno',
				toolbar:[false,'top'],
				multiselect:false,
				ondblClickRow:function(rowId){basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+rowId,i18n["request_detail"])},
				pager:'#historyCallPager'		  
			});
			$("#historyCall").jqGrid(params);
			$("#historyCall").navGrid('#historyCallPager',navGridParams);
			configureItemInfoGrids.push('#historyCall');
		},
		
		/**
		 * @description 历史变更
		 **/
		historyChangeList:function(){
			var params = $.extend({},jqGridParams, {	
				url:'change!findPagerChange.action?queryDTO.ciId='+$('#configureCiInfoId').val()+'&queryDTO.lastUpdater='+userName,
				colNames:[i18n['title_snmp_id'],i18n['number'],i18n['title'],i18n['status'],i18n['priority'],i18n['common_createTime'],i18n['title_creator']],
				colModel:[
			   		{name:'eno',width:60,sortable:true},
			   		{name:'changeNo',width:150},
			   		{name:'etitle',width:150,formatter:itsm.cim.configureItemInfoFormCustom.historyChangeTitleUrlFrm},
			   		{name:'statusName',width:120,sortable:false},
			   		{name:'priorityName',width:120,sortable:false},
			   		{name:'createTime',width:120,sortable:false,formatter:timeFormatter},
			   		{name:'creator',width:150,sortable:false}
			   	],
			   	toolbar:false,
				jsonReader: $.extend(jqGridJsonReader, {id: "eno"}),
				sortname:'eno',
				multiselect:false,
				ondblClickRow:function(rowId){itsm.cim.configureItemInfoFormCustom.viewHistoryChangeInfo(rowId)},
				pager:'#historyChangePager'
			});
			$("#historyChangeGrid").jqGrid(params);
			$("#historyChangeGrid").navGrid('#historyChangePager',navGridParams);
			configureItemInfoGrids.push('#historyChangeGrid');
		},
		
		/**
		 * @description 变更标题连接
		 **/
		historyChangeTitleUrlFrm:function(cellvalue, options, rowObject){
			return '<a href="javascript:itsm.cim.configureItemInfoFormCustom.viewHistoryChangeInfo('+rowObject.eno+')">'+cellvalue+'</a>'
			
		},
		/**
		 * @description 变更详细
		 **/
		viewHistoryChangeInfo:function(id){
			basics.tab.tabUtils.reOpenTab('change!changeInfo.action?queryDTO.eno='+id+'&random=',i18n['change_detail']);
		},

			
		/**
		 * @description 关联配置项
		 */
		relevanceCI:function(configureCiInfoId){
			
			var params=$.extend({},jqGridParams,{
				url:'itsm/cim/ciRelevance!find.action?ciRelevanceId='+configureCiInfoId,
				colNames:['ID',i18n['related'],i18n['isAssociated'],i18n['label_dc_ciRelationType'],i18n['remark'],'',''],
				colModel:[
						  {name:'relevanceId',width:155},
						  {name:'ciRelevanceName',index:'ciRelevanceId',width:160,sortable:false},
						  {name:'unCiRelevanceName',index:'unCiRelevanceId',width:170,formatter:function(cellvalue, options, rowObject){
							 return rowObject.unCiRelevanceName; 
						  }},
						  {name:'ciRelationTypeName',index:'ciRelationType',width:160,formatter:function(cellvalue, options, rowObject){
							  return colorFormatter(rowObject.ciRelationTypeNameColor,cellvalue);
						   }},
						  {name:'relevanceDesc',width:160},
						  {name:'ciRelationTypeNo',hidden:true},
						  {name:'unCiRelevanceId',hidden:true},
						  ],
				toolbar: [true,"top"],
				jsonReader: $.extend(jqGridJsonReader, {id: "relevanceId"}),
				sortname:'relevanceId',
				pager:'#configureItemInfoRelevanceCIPager'
					
			});
			$("#configureItemInfoRelevanceCI").jqGrid(params);
			$("#configureItemInfoRelevanceCI").navGrid('#configureItemInfoRelevanceCIPager',navGridParams);
			//列表操作项
			$("#t_configureItemInfoRelevanceCI").css(jqGridTopStyles);
			$("#t_configureItemInfoRelevanceCI").append($('#ciRelevancaToolbar').html());
			configureItemInfoGrids.push('#configureItemInfoRelevanceCI');
		},
		/**
		 * @description 添加关联配置项窗口
		 */
		open_ci_relevance_win:function(){
			$('#unCiRelevanceName').attr('disabled',false);
			opt='save';
			$('#relevance_ciRelationType').val('');
			$('#unCiRelevanceId').val('');
			$('#unCiRelevanceName').val('');
			$('#relevanceDesc').val('');
			$('#ciRelevanceId').val($('#configureCiInfoId').val());
			windows('add_ci_relevance_win',{width:420});
		},
		/**
		 * @description 打开编辑配置项关联
		 */
		ciRelevanceEdit:function(){
			opt='update';
			checkBeforeEditGrid('#configureItemInfoRelevanceCI', itsm.cim.configureItemInfoFormCustom.ciRelevanceEditMothd);
		},
		/**
		 * @description 编辑配置项关联
		 */
		ciRelevanceEditMothd:function(rowData){
			if($('#configureCiInfoId').val()==rowData.unCiRelevanceId){
				msgAlert(i18n['isAssociatedNotEdit'],'info');
			}else{
				$('#relevanceId').val(rowData.relevanceId);
				$('#unCiRelevanceName').val(rowData.unCiRelevanceName);
				$('#unCiRelevanceId').val(rowData.unCiRelevanceId);
				$('#relevance_ciRelationType').val(rowData.ciRelationTypeNo);
				$('#relevanceDesc').val(rowData.relevanceDesc);
				$('#ciRelevanceId').val($('#configureCiInfoId').val());
				$('#unCiRelevanceName').attr('disabled',true);
				windows('add_ci_relevance_win',{width:420});
			}
			
		},
		
		/**
		 * @description 关联配置项添加到数据库
		 */
		ciRelevanceSave:function(){
			
			if($('#unCiRelevanceId').val()!=null & $('#unCiRelevanceId').val()!=''){
				if($('#unCiRelevanceId').val()==$('#ciRelevanceId').val()){
					msgAlert(i18n['err_ciRelevanceErr'],'info');
				}else{
					var ci_relevance_form = $('#add_ci_relevance_win form').serialize();
					startProcess();
					if(opt=='save'){
						$.post('ciRelevance!isCIRelevance.action',ci_relevance_form,function(data){
							if(!data){
								$.post('ciRelevance!'+opt+'.action', ci_relevance_form, function(){
									$('#configureItemInfoRelevanceCI').trigger('reloadGrid');
									msgShow(i18n['saveSuccess'],'show');
									$('#add_ci_relevance_win').dialog('close');
								})
								endProcess();
							}else{
								endProcess();
								msgAlert(i18n['err_ciRelevanceErr1'],'info');
								
							}
						})
					}else{
						$.post('ciRelevance!'+opt+'.action', ci_relevance_form, function(){
							$('#configureItemInfoRelevanceCI').trigger('reloadGrid');
							msgShow(i18n['saveSuccess'],'show');
							$('#add_ci_relevance_win').dialog('close');
						})
						endProcess();
					}
				}
			}else{
				msgAlert(i18n['err_ciRelevanceNotNull'],'info');
			}
			
		},
		
	    /**
	     * @description 删除关联配置项获取id
	     */
		ciRelevancetool_bar_delete_aff:function()
		{
			checkBeforeDeleteGrid('#configureItemInfoRelevanceCI', itsm.cim.configureItemInfoFormCustom.delCiRelevance);
		},
		  /**
	     * @description 删除关联配置项
	     * @param rowIds 配置项数组id
	     */
		delCiRelevance:function(rowIds){
			var pp = $.param({'ids':rowIds},true);
			$.post("ciRelevance!delete.action", pp, function(){
				msgShow(i18n['deleteSuccess'],'show');
				$('#configureItemInfoRelevanceCI').trigger('reloadGrid');
			}, "json");
		},	
		
		
		/**
		 * @description 编辑配置项
		 */
		configureItemEditLink:function(){
			basics.tab.tabUtils.reOpenTab('ci!configureItemEdit.action?ciEditId='+$('#configureCiInfoId').val()+'&categoryType='+_categoryType+'&random='+Math.random()*10+1,i18n['ci_editConfigureItem']);
		},
		
		/**
		 * 数据列表伸展
		 */
		configureItemInfoPanelCollapseAndExpand:function(){
			 $(configureItemInfoGrids).each(function(i, g) {
			      setGridWidth(g, 'configureItemInfoTab', 10);
			 });
		},
		/**
		 * 数据列表自动伸展
		 */
		fitConfigureItemInfoGrids:function(){
			
			$('#configureItemInfo_west,#configureItemInfo_center').panel({
				onCollapse:itsm.cim.configureItemInfoFormCustom.configureItemInfoPanelCollapseAndExpand,
				onExpand:itsm.cim.configureItemInfoFormCustom.configureItemInfoPanelCollapseAndExpand
			});
		},
		
		/**
		 * @description 显示软件许可统计
		 **/
		showSoftwareLicense:function(){
			var cino=$('#configureCiInfoId').val();
			if(cino!=""&&cino!=undefined){
			$.post('ci!softwareLicenseTotal.action','ciEditId='+cino,function(total){
				if(total!=-1){
					$.post('ci!softwareLicenseUseCount.action','ciDto.ciname='+$('#configureCiInfo_ciname').val(),function(useTotal){
						
						$('#configureItemInfo_div table').append('<tr><td>'+i18n['label_ci_softwareLicenses']+'</td><td>'+i18n['label_ci_licensesTotal']+
								':'+total+'<br>'+i18n['label_ci_usedLicenses']+':'+useTotal+'<br>'+i18n['label_ci_remainingLicenses']+':'+(total-useTotal)+'</td></tr>');
					})
				}
			});
			}
		},
		/**
		 * @description 判断生命周期与保存期是否已到期
		 **/
		showIsCiExpired:function(){
			var _msg1='LifeCycle';
			var _msg2='CalcWarranty';
			var cino=$('#configureCiInfoId').val();
			if(cino!=""&&cino!=undefined){
			$.post("ci!calcExpired.action",'ciQueryDTO.ciId='+cino,function(data){
				if(data!=null && data!='' && data!='""'){
					$('#configureItemInfo_div table').append('<tr><td>'+i18n['label_ci_lifeCycleAndWarrantyStatus']+
							'</td><td><span style=color:red>'+data.replace(_msg1,i18n['label_ci_outOfLifeCycle']).replace(_msg2,i18n['label_ci_OutOfWarranty']).replace(/"/g,'')+'</span></td></tr>');
				}
			})
			}
		},
		
		/**
		 * 软件列表
		 */
		softwareMainGrid:function(){
			var params = $.extend({},jqGridParams, {
				url:'ciSoftwareAction!findSoftwarePager.action',
				caption:'',
				colNames:['ID',i18n['title_softName'],i18n['label_software_version'],i18n['label_software_type'],i18n['label_software_category'],i18n['supplier'],i18n['title_installDate']],
			 	colModel:[
			 	          {name:'softwareId',align:'center',width:45},
			 	          {name:'softwareName',align:'left',width:280,formatter:function(cellvalue, options, rowObject){
								 return rowObject.softwareName; 
						  }},
			 	          {name:'softwareVersion',align:'center',width:100,formatter:function(cellvalue, options, rowObject){
								 return rowObject.softwareVersion; 
						  }},
			 	          {name:'softwareTypeName',index:'softwareType',align:'center',width:90},
			 	          {name:'softwareCategoryName',index:'softwareCategory',align:'center',width:90},
			 	          {name:'softwareProviderName',index:'softwareProvider',align:'center',width:80,hidden:true},
			 	          {name:'installDate',align:'center',width:80,formatter:itsm.cim.getConfig.dataForma,hidden:true}    
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id: "softwareId"}),
				sortname:'softwareId',
				pager:'#configureItemInfoSoftwareMainPager'
				});
				$("#configureItemInfoSoftwareMainGrid").jqGrid(params);
				$("#configureItemInfoSoftwareMainGrid").navGrid('#configureItemInfoSoftwareMainPager',navGridParams);
				//列表操作项
				$("#t_configureItemInfoSoftwareMainGrid").css(jqGridTopStyles);
				$("#t_configureItemInfoSoftwareMainGrid").append($('#configureItemInfo_installSoftwareToolbar').html());
				
				//自适应宽度
				setGridWidth("#softwareMainGrid","regCenter",20);
		},
		
		/**
		 * 打开添加关联软件窗口
		 */
		openAddConfigureItemSoftware:function(){
			if($('#configureItemInfoSoftwareMainGrid').html()==''){
				itsm.cim.configureItemInfoFormCustom.softwareMainGrid();
			}else{
				$('#configureItemInfoSoftwareMainGrid').trigger('reloadGrid');
			}
			windows('configureItemInfo_installSoftware_win',{width:650});
		},
		/**
		 * 搜索关联软件
		 */
		searchSoftware:function(){
			var _url = 'ciSoftwareAction!findSoftwarePager.action';
			var postData = $("#configureItemInfoSoftwareMainGrid").jqGrid("getGridParam","postData");     
			$.extend(postData,{'qdto.softwareName':$('#search_software_name').val()});
			$('#configureItemInfoSoftwareMainGrid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		},
		/**
		 * 添加关联软件操作
		 */
		addConfigureItemSoftware:function(){
			
			checkBeforeMethod('#configureItemInfoSoftwareMainGrid',function(rowIds){
				var _param = $.param({'ids':rowIds,'ciEditId':$('#configureCiInfoId').val()},true);
				var _url="ci!addConfigureItemSoftware.action";
				$.post(_url,_param,function(){
					msgShow(i18n['msg_add_successful'],'show');
					$('#configureItemInfo_installSoftware_win').dialog('close');
					$('#installSoftwareGrid').trigger('reloadGrid');
					
				});
			});
			
		},
		/**
		 * 删除关联软件操作
		 */
		deleteConfigureItemSoftware:function(){
			checkBeforeDeleteGrid('#installSoftwareGrid', function(rowIds){
				var _param = $.param({'ids':rowIds,'ciEditId':$('#configureCiInfoId').val()},true);
				var _url="ci!deleteConfigureItemSoftware.action";
				$.post(_url,_param,function(){
					msgShow(i18n['deleteSuccess'],'show');
					$('#installSoftwareGrid').trigger('reloadGrid');
				});
			});
		},
		/**
		 * 修改行为
		 */
		behaviorModificationlist:function(){
			$.post("ci!findBehaviorModification.action",{"ciDto.ciId":$('#configureCiInfoId').val()},function(data){
				if(data!=null && data.length>0){
					var cihtml='';
					$('#behaviormodification tbody').html('');
					for(var i=0;i<data.length;i++){
						cihtml="<tr>" +
								"<td>"+data[i].id+"</td>" +
								"<td>"+data[i].userName+"</td>" +
								"<td>"+data[i].updateDate+"</td>" +
								"<td style='text-align:left;'>"+data[i].actContent+"</td>" +
								"</tr>";
						$('#behaviormodification tbody').append(cihtml);
					}
				}else{
					$('#behaviormodification').html('');
					$('#behaviormodification').html('<tr><td colspan="2" align="center" style="color:#FF0000;font-size:16px">'+i18n['emptyrecords']+'...</td></tr>');
				}
			});
		},
		/**
		 *配置项历史更新显示
		 */
		historyUpdatelist:function(){
			//var s= timezoneJS.timezone
			//var dt = new timezoneJS.Date('10/31/2008', 'America/Chicago');
			
			//var _params = $('#configureItemEdit_panel form').serialize();
			$.post("ci!findCiHistoryUpdate.action",{"ciEditId":$('#configureCiInfoId').val()},function(data){
				if(data!=null && data.length>0){
					var cihtml='';
					$('#configureItemHistory tbody').html('');
					for(var i=0;i<data.length;i++){
						cihtml="<tr>" +
								"<td>"+data[i].id+"</td>" +
								"<td><font color=red>"+data[i].revision+"</font></td>" +
								"<td>"+data[i].userName+"</td>" +
								"<td>"+(data[i].updateDate).replace('T',' ')+"</td>" +
								"<td><a onclick='itsm.cim.configureItemInfoFormCustom.historyUpdateInfo(\""+data[i].id+"\")'>"+i18n.look+"</a></td>" +
								"<td><a onclick='itsm.cim.configureItemInfoFormCustom.behaviorModificationInfo(\""+data[i].beId+"\")'>"+i18n.look+"</a></td>" +
								"</tr>";
						$('#configureItemHistory tbody').append(cihtml);
					}
				}else{
					$('#configureItemHistory').html('');
					$('#configureItemHistory').html('<tr><td colspan="2" align="center" style="color:#FF0000;font-size:16px">'+i18n['emptyrecords']+'...</td></tr>');
				}
			});
		},
		/**
		 * 查看配置项修改行为
		 */
		behaviorModificationInfo:function(beId){
			$("#behaviorModificationInfo").html('');
			if(beId!=0){
				$.post("ci!findBehaviorModificationByid.action",{"beId":beId},function(data){
					var behaviorModificationInfo='';
					htmlInfo="<tr height=20px;>" +
					"<td width=100px;>"+i18n['label_updateContent']+"："+"</td>" +
					"<td >"+data.actContent+"</td>" +
					"</tr>";
					$('#behaviorModificationInfo').append(htmlInfo);
				});
				windows('behaviorModification',{width:580});
			}else{
				msgShow(i18n['emptyrecords']);
			}
		},
		/**
		 * 查看配置项历史更新信息
		 */
		historyUpdateInfo:function(id){
			if(id!=0){
				$.post("ci!findCiHistoryUpdateID.action",{"historyUpdateId":id},function(UpdateDTO){
					var data=UpdateDTO.ciDTO;
					$("#"+htmlDivId).hide();
					$.post("formCustom!findFormCustomByCiCategoryNo.action","formCustomDTO.ciCategoryNo="+data.categoryNo,function(res){
						var htmlDivId='CihistoryInfo';
						if(res!=null&&res.formCustomContents!=""){
							//itsm.cim.configureItemInfoFormCustom.initDetailTabs(res,data.attrVals);
							var formCustomContents = common.config.formCustom.formControlImpl.editHtml(res.formCustomContents,htmlDivId);
							var tabdata = res.formCustomTabsDtos;
							var tabContents='';
							for ( var i = 0; i < tabdata.length; i++) {
								tabContents += common.config.formCustom.formControlImpl.editHtml(tabdata[i].tabAttrsConent,htmlDivId);
							}
							$("#"+htmlDivId).html($("#ciCategory_div").html()+formCustomContents+tabContents);
							itsm.cim.configureItemInfoFormCustom.loadDetailHtml(data,data.eavNo,"#"+htmlDivId,data.attrVals);
							$("#"+htmlDivId+" .label .required_div").remove();
						}else{
							$("#"+htmlDivId).load('common/config/formCustom/ciDefaultField.jsp',function(){
								itsm.cim.configureItemInfoFormCustom.loadCimFormHtmlByFormId(data,htmlDivId);
							});
						}
					});
				});
			}
			windows('Cihistory',{width:640,height:380,model:false});
		},
		/**
		 * 查看配置项关联关系图
		 */
		showPrefuse:function(){
			var ciInfoId=$('#configureCiInfoId').val();
			var url='itsm/cim/ciRelevance!find.action?ciRelevanceId='+ciInfoId;
			$.post(url,function(data){
				if(data.total==0){
					msgShow(i18n['label_ci_Relevance'],'show');
				}else{
					window.open("../pages/itsm/cim/NewPrefuse.jsp?ciId="+ciInfoId+"&winWidth="+browserProp.browserProp.getWinWidth());
				}
			});
		},
		/**
		 * 关闭配置项关联关系窗口
		 */
		ciPrefuseColse:function(){
			$('#ciPrefuse').dialog('close');
		},
		/**
		 * 点击上传附件按钮
		 */
		configureitemInfo_uploadifyUpload:function(){
			if($('#configureItemInfo_effect_fileQueue').html()!="")
				$('#configureItemInfo_effect_file').uploadifyUpload();
			else
				msgShow(i18n['msg_add_attachments'],'show');
		},
		
		/**
		 * 二维码
		 */
		scanCreate:function(cino,ciId){		
			var _url="ciScanAction!SaveScan.action";
			$.post(_url,{'ciDetailDTO.cino':cino,'ciDetailDTO.ciId':ciId},function(data){	
				if(data){
					$('#input_QRcode_id').show();
					//$('#input_QRcode_id').attr({'src':'ciScanAction!findImage.action?ciScanDTO.ciId='+ciId+'&random='+new Date()+''});
					msgShow(i18n['createSuccess'],'show');
				}else{
					msgShow(i18n['label_ciScan_msg'],'show');
				}
			});
		},
		/**
		 * 扫描配置项
		 */
		scanShow:function(cid){
			var _url="ciScanAction!showScanCIInfo.action";
			$.post(_url,{'ciScanDTO.ciId':cid},function(data){
				
			});
		},
		
		imgClose:function(){
			$('#input_QRcode_id').hide();
		},
		
		/**
		 * 初始化数据
		 */
		init:function(){
			
			itsm.cim.includes.includes.loadSelectCIIncludesFile();//加载选择配置项
			common.security.includes.includes.loadSelectUserIncludesFile();//加载用户选择
			$("#configureItemInfo_loading").hide();
			$("#configureItemInfo_panel").show();
			$("#ciDetails_layout").show();
			
			//setTimeout(function(){
				//itsm.cim.configureItemInfoFormCustom.historyUpdatelist();
			//},0);
			itsm.cim.configureItemInfoFormCustom.configureInfoTab();
			//添加到数据库按钮
			$('#link_add_ci_relevance_ok').click(itsm.cim.configureItemInfoFormCustom.ciRelevanceSave);
			$('#configureItemInfo').attr('style','height:auto;padding:0px;background:#fafafa;');
			$('#configureItemEditLink').click(itsm.cim.configureItemInfoFormCustom.configureItemEditLink);
			setTimeout(itsm.cim.configureItemInfoFormCustom.fitConfigureItemInfoGrids,0)
			$('#unCiRelevanceName').click(function(){//选择关联配置项
				itsm.cim.configureItemUtil.selectCIS('#unCiRelevanceName','#unCiRelevanceId',$('#configureItemInfo_companyNo').val());
			})
			$('#configureItemInfo_returnToList').click(function(){
				basics.tab.tabUtils.addTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp');
			});
			
			$('#configureItem_edit_but').click(function(){
				basics.tab.tabUtils.reOpenTab('ci!configureItemEdit.action?ciEditId='+$('#configureCiInfoId').val(),i18n['ci_editConfigureItem']);
			});
			
			$('#configureItemInfoTab').tabs({
				scrollIncrement:230,
				scrollDuration:10
			});
			
			setTimeout(function(){
				itsm.cim.configureItemInfoFormCustom.showSoftwareLicense();//软件许可
				itsm.cim.configureItemInfoFormCustom.showIsCiExpired();//是否过期
				//getUploader('上传文件文本ID','上传后返回的信息字符串','显示上传成功的附件','');
				getUploader('#configureItemInfo_effect_file','#configureItemInfo_effect_attachmentStr','#conmfigureItemInfo_effect_success_attachment','configureItemInfo_effect_fileQueue', function(){
				    common.tools.event.eventAttachment.saveAttachment('show_configureItemInfo_effectAttachment',$('#configureCiInfoId').val(),'itsm.configureItem','configureItemInfo_effect_attachmentStr',true);
				});
				//initFileUpload("_CiInfo",$('#configureCiInfoId').val(),"itsm.configureItem",userName,"show_configureItemInfo_effectAttachment","configureItemInfo_effect_attachmentStr");
			},0);
			common.tools.event.eventAttachment.showAttachment('show_configureItemInfo_effectAttachment',$('#configureCiInfoId').val(),'itsm.configureItem',true);
			//setTimeout(function(){
				//itsm.cim.configureItemInfoFormCustom.showSoftwareLicense();//软件许可
				//itsm.cim.configureItemInfoFormCustom.showIsCiExpired();//是否过期
			//},150);
			
			itsm.cim.configureItemInfoFormCustom.initDetailForm();
			
		}
	};
 }();
 /**加载初始化数据**/
 $(document).ready(itsm.cim.configureItemInfoFormCustom.init);