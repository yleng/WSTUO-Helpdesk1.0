$package('itsm.request');  
 /**  
 * @author wstuo  
 * @since version 1.0 
 */
itsm.request.requestStats = function(){
	return {
		/**
		 * @description 请求统计
		 */
		countAllRquest: function(){
			itsm.request.requestStats.requestStat();
			//itsm.request.requestStats.companyStat();
		},
		/**
		 * @description 根据请求状态统计
		 */
		requestStat:function(){
			var url = 'request!countRequest.action';
			$.post(url,'requestQueryDTO.loginName='+userName+'&requestQueryDTO.currentUser='+userName,function(res){
				$("#allRequest").text("[ "+res.countAllRquest+" ]");
				
				$("#myProposedRequest").text("[ "+res.countMyRquest+" ]");
				$("#myGroupProposedRequest").text("[ "+res.countMyGroupProposedRequest+" ]");
				
				$("#assigneeToMyRequest").text("[ "+res.countMyPeRquest+" ]");
				$("#assigneeGroupRequest").text("[ "+res.countMyGrRquest+" ]");
				
				$("#myOwnerRequest").text("[ "+res.countMyOwRquest+" ]");

				$("#MYNOHANDLEREQUEST").text("[ "+res.countMyNoHandleRequest+" ]");
				$("#MYINGHANDLEREQUEST").text("[ "+res.countMyIngHandleRequest+" ]");
				$("#MYCOMPLETEREQUEST").text("[ "+res.countMyCompleteHandleRequest+" ]");
				$("#MYCLOSEREQUEST").text("[ "+res.countMyCloseHandleRequest+" ]");
				$("#myNotComprehensiveNotSubmitted").text("[ "+res.myNotComprehensiveNotSubmittedRequest+" ]");
				$("#actingToMyRequest").text("[ "+res.countactingToMyRequest+" ]");
			});
			
		},
		/**
		 * @description 根据请求所属客户统计
		 */
		companyStat:function(){
			//公司统计
			var requestCompanyId =  $("#requestCompanyDiv a[class='statsLeftDiv_a']").attr("id");
			if(allRequest_Res && versionType=='ITSOP'){
				$.post('request!requestDataCountByCompanyNo.action','loginName='+userName,function(data){
					$('#companyRequestDataCount').html('');
					if(data!=null && data.length>1){
						$('#requestCompanyDiv').show();
						var str='';
						for(var i=0;i<data.length;i++){
							str='<a id="requestCompany_{companyNo}" style="cursor:pointer" onClick="javascript:itsm.request.requestStats.requestCompanyCountSearch(\'{companyNo}\',this)">{companyName}</a><span style="color: #F00">&nbsp;&nbsp;[ {total} ]</span><br>';
							str=str.replace(/{companyName}/g,data[i].orgName)
							.replace(/{total}/g,data[i].total)
							.replace(/{orgNo}/g,data[i].orgNo)
							.replace(/{orgName}/g,data[i].orgName)
							.replace(/{companyNo}/g,data[i].orgNo);
							$('#companyRequestDataCount').append(str);
						}
					}else{
						$('#requestCompanyDiv').hide();
					}
					$("#requestCompanyDiv #"+requestCompanyId).attr("class","statsLeftDiv_a");
				});
			}else{
				$('#requestCompanyDiv').hide();
			}
		},
		
		/**
		 * @description 请求统计搜索
		 * @param type 统计类型
		 */
		requestCountSearch:function(type,event){
			var _url='request!findRequests.action';
			var _postData={};
			$.extend(_postData,{'requestQueryDTO.lastUpdater':userName});
			if(type!=='' && type!=null){
				$.extend(_postData,{'requestQueryDTO.countQueryType':type,'requestQueryDTO.currentUser':currentUser});
			}
			var url =basics.index.getCur_url();
			if(url!='request/requestMain.jsp'){
				basics.index.initContent('request/requestMain.jsp?countQueryType='+type);
			}else{
				$('#requestGrid').jqGrid('setGridParam',{postData:null});
				$('#requestGrid').jqGrid('setGridParam',{postData:_postData,page:1,url:_url}).trigger('reloadGrid');
				$("#requestMainBottom a").removeClass("statsLeftDiv_a");
				$(event).addClass("statsLeftDiv_a");
			}
			
		},
		/**
		 * @description 请求统计搜索
		 * @param type 统计类型
		 */
		requestCompanyCountSearch:function(companyNo,event){
			var _url='request!findRequests.action';
			var _postData={};
			$.extend(_postData,{'requestQueryDTO.countQueryType':'','requestQueryDTO.currentUser':'','requestQueryDTO.lastUpdater':''});
			if(companyNo!=null && companyNo!==''){//根据客户
				_url='request!findRequests.action?requestQueryDTO.companyNo='+companyNo;
				$('#request_search_companyNo').val(requestMain_companyNo);
			}
			$('#requestGrid').jqGrid('setGridParam',{postData:null});
			$('#requestGrid').jqGrid('setGridParam',{postData:_postData,page:1,url:_url}).trigger('reloadGrid');
			$("#requestMainBottom a").removeClass("statsLeftDiv_a");
			$(event).addClass("statsLeftDiv_a");
		},
		/**
		 * @description 加载请求统计
		 */
		loadRequestCount:function(){
			setTimeout(itsm.request.requestStats.countAllRquest,1000);
		},
		
		/**
		 * @description 请求统计搜索
		 * @param type 统计类型
		 * @param id 饼图中每块对应的Id
		 * @param groupField 根据什么进行统计
		 */
		requestCountSearchByPieChart:function(type,id,groupField,label){
			var _url='request!findRequests.action';
			var _postData={};
			$.extend(_postData,{'requestQueryDTO.lastUpdater':userName});
			if(type!=='' && type!=null){
				$.extend(_postData,{'requestQueryDTO.countQueryType':type,'requestQueryDTO.currentUser':currentUser});
				if(groupField=="status"){
					$.extend(_postData,{'requestQueryDTO.statusNo':id});
				}else if(groupField=="priority"){
					$.extend(_postData,{'requestQueryDTO.priorityNo':id});
				}else if(groupField=="slaState"){
					$.extend(_postData,{'requestQueryDTO.slaStateNo':id});
				}
				if(id===0 &&(label=='unknown' || label == 'other')){
					$.extend(_postData,{'requestQueryDTO.label':label});
				}
			}
			$('#requestGrid').jqGrid('setGridParam',{postData:null});
			$('#requestGrid').jqGrid('setGridParam',{postData:_postData,page:1,url:_url}).trigger('reloadGrid');
		}
	};
}();