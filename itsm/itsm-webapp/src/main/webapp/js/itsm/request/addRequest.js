$package("itsm.request");
$import("wstuo.dictionary.dataDictionaryUtil");
$import("wstuo.category.eventCategoryTree");
$import("wstuo.user.userUtil");
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
	$import('itsm.cim.ciCategoryTree');
}
$import('itsm.itsop.selectCompany');
$import('itsm.request.relatedRequestAndKnowledge');
$import('wstuo.tools.chooseAttachment');
$import('wstuo.knowledge.knowledgeTree');
$import('wstuo.category.serviceCatalog');
$import('wstuo.includes');
$import('wstuo.sysMge.base64Util');
$import('wstuo.customForm.formControlImpl');
$import('wstuo.tools.xssUtil');
$import('itsm.request.requestCommon');
$import('itsm.request.requestStats');
/**  
 * @author QXY  
 * @constructor WSTUO
 * @description 新增请求主函数.
 * @since version 1.0 
 * @returns
 * @param select
 */
itsm.request.addRequest = function(){
	var templateId='';
	this.addRequestGrids=[];
	var addRequest_hidden_formField = '';
	var add_request_attrJson = "";
	var is_show_border ="0" ;
	return {
		
		/**
		 * 根据语音卡用户Id赋值选择请求人
		 */
		selectRequestvoiceCard:function(){
			$.post('user!findUserDetail.action','userDto.userId='+voiceCarduserId,function(data){
				$('#addRequestForm #request_userName').val(data.fullName);
				$('#addRequestForm #request_userId').val(data.userId);
			});
		},
		
		/**
		 * @description 选择请求子分类.
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 */
		selectRequestCategoryZi:function(showAttrId,dtoName){
			var categoryNo= $('#addRequestCategoryNo').val();
			if(categoryNo==="")
				msgShow(i18n.request_categorysub_alert,'show');
			else{
			wstuo.category.eventCategoryTree.showSelectTreeZi('#request_category_select_window',
                                                                      '#request_category_select_tree',
                                                                      'Request',
                                                                      '#addRequestCategoryName_zi',
                                                                      '#addRequestCategoryNo_zi',
                                                                      '#request_etitle',
                                                                      '#request_edesc',
                                                                      showAttrId,
                                                                      dtoName,
                                                                      categoryNo
                                                                     );
			}
		},
		/**
		 * @description 提交保存请求.
		 */
		saveRequest:function(){
			var oEditor = CKEDITOR.instances.request_edesc;
			var edesc=trim(oEditor.getData());
			var uid=$('#request_userId').val();
			var username=$('#request_userName').val();
			var title= $('#addRequestForm #request_etitle').val();
			$('#addRequestForm #request_etitle').val(trim(title));
			$.each($("#addRequestForm :input[attrType=Lob]"),function(index,obj){
				var oEditorObj = CKEDITOR.instances[$(obj).attr("id")];
				$(obj).val(trim(oEditorObj.getData()));
			});
			var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
			if(!request_edesc){
				msgAlert(i18n.titleAndContentCannotBeNull,'info');
			}else if(username==='' || uid===''){
				$('#request_userId').val('');
				$('#request_userName').val('');
				msgAlert(i18n.ERROR_CREATE_BY_NULL,'info');
			}else{
				itsm.request.requestCommon.getFormAttributesValue("#addRequestForm");
				$('#addRequestForm #request_edesc').val(edesc);
				$.each($("#addRequestForm input[attrtype='String']"),function(ind,val){
					$(this).val(wstuo.tools.xssUtil.html_encode($(this).val()));
				});
				
				var frm = $('#addRequestForm').serialize();
				var url = 'request!saveRequest.action?requestDTO.isShowBorder='+is_show_border+'&requestDTO.isNewForm=true';
				//调用
				startProcess();
				$.post(url,frm, function(eno){
						//showRequestIndex();
						basics.index.initContent("request!requestDetails.action?eno="+eno);
						itsm.request.requestStats.countAllRquest();
						msgShow(i18n.msg_request_addSuccessful,'success');
						endProcess();
				});
			}
			
		},
		/**
		 * 查询相类似的请求和知识
		 * @param id 标识
		 */
		findLikeData:function(id){
			var _keyword=$(id).val();
			if(_keyword!=null && _keyword!=='' && _keyword!=' '){
				itsm.request.relatedRequestAndKnowledge.findKnowledge('#findLikeKnowledgeGrid','#findLikeKnowledgePager',_keyword);
				itsm.request.relatedRequestAndKnowledge.findLikeRequest('#findLikeRequestGrid','#findLikeRequestPager',_keyword);
				$('#addRequest_keyword').val(_keyword);
			}else{
				$("#findLikeKnowledgeGrid").jqGrid("clearGridData");
				$("#findLikeRequestGrid").jqGrid("clearGridData");
			}
		},
		/**
		 * 选择请求模板
		 */
		selectRequestTemplate:function(){
			//var template_formId = $('#addRequest_formId').val();
			var serviceDirId = $('#addRequest_serviceDirIds').val();
			if(!serviceDirId){
				serviceDirId=0;
			}
			$("#requestTemplate").html("");
			$('<option value="">-- '+i18n.common_pleaseChoose+' --</option>').appendTo("#requestTemplate");
			$.post("template!findAllTemplate.action",{"templateDTO.templateType":"request","templateDTO.serviceDirId":serviceDirId},function(data){
				if(data!=null && data.length>0){
					for(var i=0;i<data.length;i++){
						$('<option value="'+data[i].templateId+'">'+data[i].templateName+'</option>').appendTo("#requestTemplate");
					}
				}
			});
		},
		
		/**
		 * 获取模板值
		 * @param templateId 模板Id
		 */
		getTemplateValue:function(templateId){
		    $('#add_request_success_attachment').html('');
		    $('#add_request_attachmentStr').val('');
		    $('#requestRelatedCIShow tbody').html('');
			if(templateId!=null && templateId!==""){
				$('#saveRequestTemplateBtn').hide();
				$('#editRequestTemplateBtn').show();
				$('#add_request_serviceDirectory_tbody').html("");
				$.post("template!findByTemplateId.action",{"templateDTO.templateId":templateId},function(data){
					if(data!=null){
						var formCustomId="#addRequestFormCustom_setting";
						if(page=="main"){
							formCustomId="#addRequestFormCustom";
						}
						if(data.isShowBorder==="1"){
							$(formCustomId).attr("href","../styles/common/addRequestFormCustom.css");
						}else{
							$(formCustomId).attr("href","../styles/common/addRequestFormCustom-no.css");
						}
						$('#requestTemplate').attr('value',data.templateId);
						$('#requestTemplateNameInput').val(data.templateName);
						$('#requestTemplateId').val(data.templateId);
						var res = data.dto;
						if(data.formId>0){
							var _param = {"formCustomId" : data.formId};
							$.post('formCustom!findFormCustomById.action',_param,function(formData){
								if(formData.formCustomContents!==""){
									var formCustomContents = wstuo.customForm.formControlImpl.editHtml(formData.formCustomContents,'addRequest_formField');
									$('#addRequest_formField').html(formCustomContents);
									$.parser.parse($('#addRequest_formField'));
									wstuo.customForm.formControlImpl.formCustomInit('#addRequest_formField');
									itsm.request.requestCommon.setRequestFormValuesByScheduledTask(res,'#addRequest_formField',res.attrVals,formData.eavNo);
									itsm.request.addRequest.showBorderCss(data,'#addRequest_formField',formCustomId);
								
									itsm.request.requestCommon.removeRequiredField(page);
								
									if(pageType=="template"){
										$("#addRequest_formField").find("div[class='fieldName']").css("margin-right","0px");
									}
								}else
									itsm.request.addRequest.initDefault(res,data,formCustomId);
							});
						}else{
							itsm.request.addRequest.initDefault(res,data,formCustomId);
						}
						var ciNos = res.relatedCiNos;
						if(ciNos!=null && ciNos.length>0){
							var param = $.param({"ids":ciNos},true);
							$.post("ci!findByIds.action",param,function(res){
								for(var i=0;i<res.length;i++){
									var ci=res[i];
									$('<tr id="ref_ci_'+ci.ciId+'">'+
											'<td align="center" id="problemShowCIno">'+ci.cino+'</td>'+
											'<td align="center"><input id="probleShowCIId" type="hidden" name="cinos" value="'+ci.ciId+'" />'+
											'<a href=javascript:itsm.cim.configureItemUtil.lookConfigureItemInfo('+ci.ciId+') id="probleShowCIName">'+ci.ciname+'</a>'+
											'</td>'+
											'<td align="center" id="probleShowCIcategoryName">'+ci.categoryName+'</td>'+
											'<td align="center" id="probleShowCIStatus">'+ci.status+'</td>'+
											'<td align="center"><a onclick=itsm.cim.configureItemUtil.removeRow("#ref_ci_'+ci.ciId+'")>'+i18n.deletes+'</a></td>'+
											'</tr>').appendTo('#requestRelatedCIShow tbody');
								}
								
							});
						}
						$('#add_request_attachmentStr').val(data.dto.attachmentStr);
						var attrArr=data.dto.attachmentStr.split("-s-");
			    		for(var i=0;i<attrArr.length;i++){
			    			var url=attrArr[i].replace("\\", "/");
							if(url!=""){
			        			var attrArrs=url.split("==");
			        			var name=attrArrs[0];
			        			uploadSuccess(name.substr(name.lastIndexOf(".")),attrArrs[1],name,'#add_request_attachmentStr','#add_request_success_attachment',"");
							}
			    		}
					}
				});
			}else{
				$('#saveRequestTemplateBtn').show();
				$('#editRequestTemplateBtn').hide();
				var template_formId = $('#addRequest_formId').val();
				if(template_formId!=null && template_formId != 0){
					itsm.request.addRequest.loadRequestFormHtmlByFormId(template_formId,'addRequest_formField');
				}
			}
		},
		/**
		 * 保存请求内容模板
		 * @param type 模板类型，add为新增
		 */
		saveRequestTemplate:function(type){
			if(type=="add"){
				$('#requestTemplateId').val("");
			}
			
			if($("#request_companyName").val()===""){
				$("#request_companyNo").val("");
			}
			if($("#request_userName").val()===""){
				$("#request_userId").val("");
			}
			
			var oEditor = CKEDITOR.instances.request_edesc;
			var edesc=trim(oEditor.getData());
			edesc = edesc.replace(/\s*<\/p>\s*|\s*<\/p>\s*/, '').replace(/\s*<p>\s*|\s*<\/p>\s*/, '').replace(/&nbsp;/gi, '');
			$('#addRequestForm #request_edesc').val(edesc);
			$.each($("#addRequestForm :input[attrType=Lob]"),function(index,obj){
				var oEditorObj = CKEDITOR.instances[$(obj).attr("id")];
				$(obj).val(trim(oEditorObj.getData()));
			});
			var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
			if(!request_edesc){
				msgAlert(i18n.titleAndContentCannotBeNull,'info');
				return false;
			}
			if($('#requestTemplateForm').form('validate') && $('#addRequestForm').form('validate')){
				itsm.cim.ciCategoryTree.getFormAttributesValue("#addRequestForm");
				$.each($("#addRequest_formField input[attrtype='String']"),function(ind,val){
					$(this).val(wstuo.tools.xssUtil.html_encode($(this).val()));
				});
				$("#requestTemplateNameInput").val(wstuo.tools.xssUtil.html_encode($("#requestTemplateNameInput").val()));
				var frm = $('#addRequestForm,#addRequest_center form,#requestTemplateName form').serializeObject();
				var url = 'request!saveRequestTemplate.action';
				var serviceDirId = $('#addRequest_serviceDirIds').val();
				if(!serviceDirId){
					serviceDirId=0;
				}
				$.extend(frm,{"templateDTO.isShowBorder":is_show_border,"templateDTO.isNewForm":true,"templateDTO.serviceDirId":serviceDirId});
				//调用
				startProcess();
				$.post(url,frm, function(res){
						endProcess();
						$('#requestTemplateName').dialog('close');
						if(type=="add"){
							itsm.request.addRequest.selectRequestTemplate();
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n.saveSuccess,'show');
						}else{
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n.editSuccess,'show');
						}
					
				});
			}
			
		},
		/**
		 * 新增请求模板框
		 */
		requestTemplateNameWin:function(){
			$('#requestTemplateNameInput').val("");
			windows('requestTemplateName');
		},
		/**
		 * 请求新增上传
		 * @param locimage 文件名对象
		 */
		addrequestupload:function(locimage){
			var FileType = "jpg,png,gif"; 
	        var FileName = locimage.value;
		    FileName = FileName.substring(FileName.lastIndexOf('.')+1, FileName.length).toLowerCase(); 
		    if (FileType.indexOf(FileName) == -1){
		    	$('#request_upload_file').val("");
		    	msgAlert(i18n.lable_addRequest_upload_file,'info');
		     }else{
		    	 itsm.request.addRequest.uploadrequestimage(FileName);
		     }
		},
		/**
		 * 上传请求图片
		 * @param FileName 文件名
		 */
		uploadrequestimage:function(FileName){
			var imageFileNametoreuqest=new Date().getTime()+"."+FileName;
			$.ajaxFileUpload({
				url:'fileUpload!fileUpload.action?imageFileNametoreuqest='+imageFileNametoreuqest,
				secureuri:false,
	            fileElementId:'request_upload_file', 
	            dataType:'String',
				success: function (data, status){
					$('#request_upload_file').val("");
					$('#request_upload_opt_info').text("../upload/request/"+imageFileNametoreuqest);
					msgShow(i18n.lable_addRequest_upload_success,'show');
				},
				error: function (data, status){
					msgShow(i18n.lable_addRequest_upload_errors,'show');
	            }
			});
		},
		/**
		 * 呼叫中心集成打开的新增请求页面处理函数(韵达语音系统集成)
		 */
		callCenterAddRequest:function(){
			$("#phones").val(callNumber);
			var partten = /^0(([1,2]\d)|([3-9]\d{2}))\d{7,8}$/;
			var url="user!updateUserByPhone.action?userDto.moblie="+callNumber;
			if(partten.test(callNumber)){
				url="user!updateUserByPhone.action?userDto.phone="+callNumber;
		     }
			//在这里要判断一下电话是手机还是座机
			$.post(url,function(data){
				if(data != null){
					$("#selectCreator").attr("value",data.fullName);
				}
			});
			//输入用户修改电话号码
			$("#edit_User_By_LoginName").bind("click",function(){
				windows('updateUserWindow');
			});
			//新增一个用户
			$("#add_User_By_Phone").bind("click",function(){
				windows('addUserByPhone');
			});
			//输入用户名修改用户手机号码
			$("#update_user_by_loginName").bind("click",function(){
				if(callNumber == null || callNumber === ""){
					msgAlert(i18n.No_incoming_number,'info');
				}else{
					var loginNames=$("#bindByLoginName").val();
					if(loginNames != null || loginNames !== ""){
						var url="user!findUserUpdatePhone.action?userDto.loginName="+loginNames+"&userDto.moblie="+callNumber;
						var partten = /^0(([1,2]\d)|([3-9]\d{2}))\d{7,8}$/;
						if(partten.test(callNumber)){
							url="user!findUserUpdatePhone.action?userDto.loginName="+loginNames+"&userDto.phone="+callNumber;
					     }
						$.post(url,function(data){
							if(data){
								var urls="user!getUserDetailByLoginName.action?userName="+loginNames;
								$.post(urls,function(datas){
									$("#selectCreator").val(datas.fullName);
									$("#addRequestUserId").val(datas.userId);
									msgShow(i18n.editSuccess,'show');
									$('#updateUserWindow').dialog('close');
								});
							}else{
								msgAlert(i18n.user_phone_is_not_find,'info');
							}
						});
					}
				}
			});
			//添加操作
			$("#add_User_Phone").bind("click",function(){
				if(callNumber == null || callNumber === ""){
					msgAlert(i18n.No_incoming_number,'info');
				}else{
					var fullNames= $("#add_loginName").val();
					var orgNo=$("#addUserOroNo").val();
					var partten = /^0(([1,2]\d)|([3-9]\d{2}))\d{7,8}$/;
					var url="user!saveuserByPhone.action";
					var frm="userDto.fullName="+fullNames+"&userDto.orgNo="+orgNo+"&userDto.moblie="+callNumber;
					if(partten.test(callNumber)){
						frm="userDto.fullName="+fullNames+"&userDto.orgNo="+orgNo+"&userDto.phone="+callNumber;
				     }
					if(orgNo != null && orgNo !== ""){
						$.post(url,frm,function(data){
							if(data != null){
								msgShow(i18n.addSuccess,'show');
								$("#selectCreator").val($("#add_loginName").val());
								$("#add_loginName").val("");
								$("#addUserOroNo").val("");
								$("#addRequestUserId").val(data.userId);
							}
						});
					}else{
						msgAlert(i18n.title_user_org+i18n.err_nameNotNull,'info');
					}
				}
			});
		},
		//根据表单Id加载表单内容
		loadRequestFormHtmlByFormId:function(formId,htmlDivId){
			$("#addRequest_formId").val(formId);
			$.post("formCustom!findFormCustomById.action","formCustomId="+formId,function(data){
				if(data.formCustomContents!=""){
					var formCustomContents = wstuo.customForm.formControlImpl.editHtml(data.formCustomContents,htmlDivId);
					$('#'+htmlDivId).html(formCustomContents);
					$('#'+htmlDivId).find('.glyphicon-trash').remove();
					/*$('#'+htmlDivId).find(':input').each(function(i,obj){
						$(this).attr('name',"requestDTO."+$(this).attr('name'));
					});*/
					wstuo.customForm.formControlImpl.formCustomInit('#'+htmlDivId);
					wstuo.customForm.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
					if(pageType=="template"){
						$("#addRequest_formField").find("div[class='fieldName']").css("margin-right","0px");
					}
					
				}
			});
		},
		
		/**
		 * 初始化
		 * @private
		 */
		init:function() {
			//判断是否有传来电号码(韵达语音系统集成)
			/*if(callNumber!=="" && callNumber != null || callNumber.length !== 0){
				itsm.request.addRequest.callCenterAddRequest();
			}*/
			//如果是内容模板，则隐藏请求保存按钮
			if(pageType=="template"){
				$('#saveRequestBtn').hide();
				$("#addRequest_serviceDirIds").val(serviceDirId);
			}
			//加载请求模板includes文件
			wstuo.includes.loadRequestActionIncludesFile();
			wstuo.includes.loadSelectCustomerIncludesFile();
			wstuo.includes.loadCategoryIncludesFile();
			wstuo.includes.loadCustomFilterIncludesFile();

			//绑定请求保存按钮
			$('#saveRequestBtn').click(itsm.request.addRequest.saveRequest);
			
			//绑定选择指派信息按钮
			$('#selectAssigneeInfo').click(itsm.request.addRequest.selectRequestAssignee);
			
			//初始化附件上传控件
			setTimeout(function(){
				getUploader('#add_request_file','#add_request_attachmentStr','#add_request_success_attachment','addRequestQueId');
			},0);
			
			//绑定退回请求列表页面按钮
			$('#add_request_backList').click(function(){
				if(pageType=='template'){
					basics.index.initContent('../pages/common/config/template/templateMain.jsp');
				}else{
					showLeftMenu('../pages/itsm/request/leftMenu.jsp','leftMenu');		
					basics.index.initContent('../pages/itsm/request/requestMain.jsp');
				}
			});
			
			//服务目录
		/*	$('#add_request_service_add').click(function(){
				wstuo.category.serviceCatalog.selectServiceDir('#add_request_serviceDirectory_tbody');
			});
			*/
			
			/*if(fixRequestAndKnowledge=="1"){
				setTimeout(function(){
					$("#phoneNums").val(phoneNum);
					itsm.request.relatedRequestAndKnowledge.findKnowledge('#findLikeKnowledgeGrid','#findLikeKnowledgePager','');
					itsm.request.relatedRequestAndKnowledge.findLikeRequest('#findLikeRequestGrid','#findLikeRequestPager','');
				},500);
			}*/
			//加载请求内容模板
			itsm.request.addRequest.selectRequestTemplate();
			
			$('#saveRequestTemplateBtn').click(function(){
				itsm.request.addRequest.requestTemplateNameWin();
			});
			$('#addRequestTemplateOk').click(function(){
				itsm.request.addRequest.saveRequestTemplate("add");
			});
			$('#editRequestTemplateBtn').click(function(){
				itsm.request.addRequest.saveRequestTemplate("edit");
			});
			if(editTemplateId!==""){
				$('#requestTemplate').val(editTemplateId);
				setTimeout(function(){
					itsm.request.addRequest.getTemplateValue(editTemplateId);
				},500);
			}else{
				$.post("formCustom!findSimilarFormCustom.action?type=request",function(data){
					var classw="glyphicon glyphicon-ok";
					$.each(data,function(i,obj){
						if(obj.isDefault){
							classw="glyphicon glyphicon-ok";
							$("#addRequest_formField_row").show();defaultFormId=obj.formCustomId;
							itsm.request.addRequest.loadRequestFormHtmlByFormId(obj.formCustomId,'addRequest_formField');
						}else
							classw="whitespace";
						$("#customform").append('<li><a data-value="'+obj.formCustomId+'" href="#" style="width:98%;height: 26px;"><i class="'
								+classw+'"></i> '+obj.formCustomName+' </a></li>');
					});
					$('#customform a').click(function (e) {
				        e.preventDefault();
				        var formCustomId = $(this).attr('data-value');
				        itsm.request.addRequest.loadRequestFormHtmlByFormId(formCustomId,'addRequest_formField');
				        $('#customform i').attr('class','whitespace');
				        $(this).find('i').attr('class','glyphicon glyphicon-ok');
				    });
					/*if(voiceCarduserId!==""){
						if(callNumber== null || callNumber ===""){
							itsm.request.addRequest.selectRequestvoiceCard();
						}
					}*/
				});
			}
			$('#addrequestRelatedCIBtn').click(function(){
				if(userRoleCode =="ROLE_ENDUSER,"){//如果是终端用户，根据请求人去搜索，不需要分类权限控制； 
					itsm.cim.configureItemUtil.enduserSelectCISM('#requestRelatedCIShow',$("#addRequest_formField #request_companyNo").val());
				}else{
					itsm.cim.configureItemUtil.requestSelectCI('#requestRelatedCIShow',$("#addRequest_formField #request_companyNo").val(),'');
				}
			});
			//服务目录导航
			$('#addRequest_ServicesCatalogNavigation').click(function(){
				itsm.request.addRequest.showFormCustomDesign();
			});
			initCkeditor('request_edesc','Simple',function(){});
			itsm.request.requestCommon.bindAutoCompleteToRequest('basicInfo_field');
			
			/*if(fixRequestAndKnowledge=="1"){
				$('#basicInfo_field #request_etitle').keyup(function(){
					itsm.request.addRequest.findLikeData('#addRequest_formField #request_etitle');
					});
				$('#addRequest_keyword_search_ok').click(function(){itsm.request.addRequest.findLikeData('#addRequest_keyword');});
			}*/
			if(belongsClient=="0"){
				//设置所属客户不可编辑,并且移除图标
				$("#basicInfo_field #request_companyName").attr("disabled",true);
			}
			if(requestUser=="0"){
				//设置请求用户不可编辑,并且移除图标
				$("#basicInfo_field #request_userName").attr("disabled",true);
				$("#basicInfo_field #request_userName").val(fullName);
				$("#basicInfo_field #request_userId").val(userId);
			}
		}
	}
}();
//载入
$(document).ready(itsm.request.addRequest.init);