$package('itsm.app.activation')
/**  
* @author QXY  
* @constructor activation
* @description activation
* @since version 1.0  
*/
itsm.app.activation.activation=function(){
	
	var url;
	var param;
	return {
//		validateKeyFile:function(){
//			
//			
//			if($('#licenseKeyFile').val()!=''){
////				$.ajaxFileUpload({
////		            url:'licenseKey!uploadLicenseKey.action',
////		            secureuri:true,
////		            fileElementId:'licenseKeyFile',    //文件输入框的ID名
////		            dataType:'json',
////		            success: function(data){
////		             	if(data=="true"){
////		             		$('#licenseKeyFile').val('');
////							msgAlert(i18n['label_license_rightCode_restartService'],'info');
////		             	}else{
////		             		msgAlert(i18n['label_license_errorFile'],'error');
////		             	}
////		            }
////		        });
//				importCSV();
//			}else{
//				msgAlert(i18n['label_license_nullFile'],'warning');
//			}
//			
//		},
		/**
		 * 验证激活码
		 */
		validateKeyCode:function(){
			if($('#codeForm').form('validate')){
				url = "licenseKey!validateKey.action";
				param = $('#codeForm').getForm();
				$.post(url,param,function(result){
					if(result=="true"){
						$('#licenseKeyCode').val('');
						msgAlert(i18n['label_license_rightCode_restartService'],'info');
					}else{
						msgAlert(i18n['label_license_errorCode'],'error');
					}
				});
			}
		},
		init:function(){
			$('#sub_licenseKey').button().bind('click',itsm.app.activation.activation.validateKeyCode);
//			$('#sub_licenseKeyFile').button().bind('click',itsm.app.activation.activation.validateKeyFile);
		}
	}
}();

$(function(){itsm.app.activation.activation.init()});