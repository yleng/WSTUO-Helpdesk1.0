$package('wstuo.jbpmMge') 
/**  
 * @fileOverview "processUse"
 * @author tan
 * @version 1.0  
 */  
 /**  
 * @author tan  
 * @constructor processUse
 * @description 流程任务函数
 * @date 2010-11-17
 * @since version 1.0 
 */
wstuo.jbpmMge.processTask=function(){
	//载入
	return {
		/**
		 * @description 类型格式化
		 * @param cellvalue 当前列值
		 * @param options 事件
		 * @param rowObject 行数据
		 * @private
		 */
		taskTypeForma:function(cellvalue, options, rowObject){
			var executionId=rowObject.executionId;
			if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
				return i18n['change']
			}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){  
				return i18n['problem']
			}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
				return i18n['release']
			}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
				return i18n['request']
			}
		},
		/**
		 * 操作项格式化
		 */
		viewDetailedForma:function(value){
			if(value==''){
			    msgAlert('未知任务');
			}else{
				var kv = value.split(',');
				var type = kv[0];
				var eno = kv[1];
				if(type=='Change'){
					basics.index.initContent('change!changeInfo.action?queryDTO.eno='+eno);
				}else if(type=='Problem'){
					basics.index.initContent('problem!findProblemById.action?eno='+eno);
				}else if(type=='Release'){
					basics.index.initContent('release!releaseDetails.action?releaseDTO.eno='+eno);
				}else if(type=='Request'){
					basics.index.initContent('request!requestDetails.action?eno='+eno);
				}
			}
		},
		/**
		 * @description 变量格式化
		 * @param cellvalue 当前列值
		 * @param options 事件
		 * @param rowObject 行数据
		 * @private
		 */
		variablesForma:function(cellvalue, options, rowObject){
			var executionId=rowObject.executionId;
			var variables = cellvalue;
			var dto = variables.dto;
			if(dto!=undefined && dto!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					return "Change,"+dto.eno
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					return "Problem,"+dto.eno
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					return "Release,"+dto.eno
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					return "Request,"+dto.eno
				}
			}else if(variables.eno!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					return "Change,"+variables.eno
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					return "Problem,"+variables.eno
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					return "Release,"+variables.eno
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					return "Request,"+variables.eno
				}
			}else{
				return '';
			}
		},
		/**
		 * 我待处理的任务(面板)
		 * @param tableID 面板tableID
		 */
		showMyProcessingTasksDashboard:function(tableID){
			var _url='jbpm!findPagePersonalTasks.action';
			var params=$.extend({},jqGridParams,{
				caption:'',
				url:_url,
				postData:{'assignee':userName},
				colNames:
						[
						 i18n['label_bpm_task_id']
						,i18n['title']
						,i18n['title_request_assignTo']
						,i18n['label_bpm_activity_name']
						,i18n['label_bpm_task_name']
						,i18n['priority']
						,i18n['common_createTime']
						,i18n['label_bpm_task_duedate']
						,i18n['label_bpm_task_type']
						,''],
				colModel:[
				          {name:'id',sortable:false,align:'center',hidden:true},
				          {name:'variables',width:120,align:'center',hidden:false,formatter:function(cellvalue, options, rowObject){
								 return rowObject.variables.etitle;
						  }},
						  {name:'assignee',sortable:false,align:'center',hidden:true},
						  {name:'activityName',hidden:true,sortable:false,align:'center'},
						  {name:'name',width:100,sortable:false,align:'center'},
						  {name:'priority',sortable:false,align:'center',hidden:true},
						  {name:'createTime',formatter:timeFormatter,sortable:false,align:'center'},
						  {name:'duedate',formatter:timeFormatter,sortable:false,align:'center',hidden:true},
						  {name:'executionId',sortable:false,formatter:wstuo.jbpmMge.processTask.taskTypeForma,align:'center'},
						  {name:'variables',formatter:wstuo.jbpmMge.processTask.variablesForma,align:'center',hidden:true}
						  
					  ],
				toolbar:false,
				multiselect:false,
				jsonReader: $.extend(jqGridJsonReader,{id:"executionId"}),
				sortname:'id',
				ondblClickRow:function(rowId){
					var rowData= $('#TAB_ID_'+tableID).jqGrid('getRowData',rowId);  // 行数据
					wstuo.jbpmMge.processTask.viewDetailedForma(rowData.variables);
				},
				pager:'#'+tableID+'_0'
			});
			$('#TAB_ID_'+tableID).jqGrid(params);
			$('#TAB_ID_'+tableID).navGrid('#'+tableID+'_0',navGridParams);
			setGridWidth("#TAB_ID_"+tableID,2);
		},
		/**
		 * 指派给我组的任务(面板)
		 * @param tableID 面板tableID
		 */
		showTasksAssignedToOurTeamDashboard:function(tableID){
			var params=$.extend({},jqGridParams,{
				caption:'',
				url:'jbpm!findPageGroupTasks.action',
				postData:{'assignee':userName},
				colNames:
						[
						 i18n['label_bpm_task_id']
						,i18n['title']
						,i18n['title_request_assignTo']
						,i18n['label_bpm_activity_name']
						,i18n['label_bpm_task_name']
						,i18n['priority']
						,i18n['common_createTime']
						,i18n['label_bpm_task_duedate']
						,i18n['label_bpm_task_type']
						,''],
				colModel:[
				          {name:'id',sortable:false,align:'center',hidden:true},
				          {name:'variables',width:120,align:'center',hidden:false,formatter:function(cellvalue, options, rowObject){
								 return rowObject.variables.etitle;
						  }},
						  {name:'assignee',width:150,sortable:false,hidden:true},
						  {name:'activityName',hidden:true,sortable:false},
						  {name:'name',width:100,sortable:false},
						  {name:'priority',width:100,sortable:false,align:'center',hidden:true},
						  {name:'createTime',formatter:timeFormatter,sortable:false,align:'center'},
						  {name:'duedate',formatter:timeFormatter,sortable:false,align:'center',hidden:true},
						  {name:'executionId',sortable:false,formatter:wstuo.jbpmMge.processTask.taskTypeForma,align:'center'},
						  {name:'variables',formatter:wstuo.jbpmMge.processTask.variablesForma,align:'center',hidden:true}
					  ],
				toolbar:false,
				ondblClickRow:function(rowId){
					var rowData= $('#TAB_ID_'+tableID).jqGrid('getRowData',rowId);  // 行数据
					wstuo.jbpmMge.processTask.viewDetailedForma(rowData.variables);
				},
				multiselect:false,
				jsonReader: $.extend(jqGridJsonReader,{id:"executionId"}),
				sortname:'id',
				pager:'#'+tableID+'_0'
			});
			$('#TAB_ID_'+tableID).jqGrid(params);
			$('#TAB_ID_'+tableID).navGrid('#'+tableID+'_0',navGridParams);
			setGridWidth("#TAB_ID_"+tableID,2);
		},
		taskStat:function(tableID){
		    $('#TAB_ID_'+tableID).html($('#portal_taskStat').html());
			$.post('request!countRequestFlowTask.action','requestQueryDTO.loginName='+userName,function(res){
				$("#myOutstandingTasks").text("[ "+res.countMyRequestTask+" ]");
				$("#myProcessedTasks").text("[ "+res.countMyProcessedTask+" ]");
				$("#tasksAssignedToOurTeam").text("[ "+res.countMyGroupRequestTask+" ]");
				$("#actingmyOutstandingTasks").text("[ "+res.actingmyOutstandingTasks+" ]");
			});
		},
		
		init:function(){
			
		}
	}
}();
$(document).ready(wstuo.jbpmMge.processTask.init);