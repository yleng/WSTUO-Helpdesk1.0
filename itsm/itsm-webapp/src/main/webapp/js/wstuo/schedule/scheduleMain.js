$package('wstuo.schedule');
$import('basics.dateUtility');
$import('wstuo.tools.xssUtil');
/**  
 * @author QXY  
 * @constructor WSTO
 * @description 人员行程管理
 * @date 2010-11-17
 * @since version 1.0 
 */ 
wstuo.schedule.scheduleMain = function(){
	/**
	 * 时间对象的格式化;
	 */
	Date.prototype.format = function(format) {
	    /*
	     * eg:format="YYYY-MM-dd hh:mm:ss";
	     */
	    var o = {
	        "M+" :this.getMonth() + 1, // month
	        "d+" :this.getDate(), // day
	        "h+" :this.getHours(), // hour
	        "m+" :this.getMinutes(), // minute
	        "s+" :this.getSeconds(), // second
	        "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
	        "S" :this.getMilliseconds()
	    // millisecond
	    }

	    if (/(y+)/.test(format)) {
	        format = format.replace(RegExp.$1, (this.getFullYear() + "")
	                .substr(4 - RegExp.$1.length));
	    }

	    for ( var k in o) {
	        if (new RegExp("(" + k + ")").test(format)) {
	            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
	                    : ("00" + o[k]).substr(("" + o[k]).length));
	        }
	    }
	    return format;
	}
	//开始时间
	this._startDate = new Date().format("yyyy-MM-dd");
	//结束时间
	this._endDate = '';
	//总列数
	this._totalCol = 8;
	//开始小时
	this._startHour = 8;
	//结束小时
	this._endHour = 19;
	this._showTask = '';
	
	this._importUrl='';
	return {
		/***
		 * 加载数据
		 * @param startTime 开始时间
		 * @param endTime 结束时间
		 * @param param 查询参数
		 */
		
		
		initMonthSchedule:function(startDate,endDate,param){
			_showTask = '';
			startProcess();
			$.post('scheduled!findScheduleRequest.action',param,function(data){
				
				if(data!=null){
//					if(data.maximumTime!=null){
//						endDate = data.maximumTime;
//					}
//					if(data.minimumTime!=null){
//						startDate = data.minimumTime;
//					}
				}
				
				$('#schedule_category_technician,#schedule_day,#schedule_day_technician,#schedule_day_ticket').html('');
				
				$('#schedule_category_technician').append('<div align="center" style="border-right:1px none;overflow: hidden;">'+i18n['title_technician']+'/'+i18n['time']+'</div>');
				//人员行程管理时间轴
				//本月的所有日期
				var monthDay = wstuo.schedule.scheduleMain.dateBetweenDayByStartAndEndTime(startDate,endDate).split('<>');
				//设置宽度
				var schedule_main_width = (36*(monthDay.length-1))+($('#schedule_category_technician').width()+5);
				//显示日期(头部)
				$('#schedule_day').append('<div id="schedule_day_center" style="display:inline-block;"></div>');
				$('#schedule_day_center').css('width',(schedule_main_width-145)+'px')
//				if(schedule_main_width<1305){
//					$('#schedule_show_main,#schedule_main').css('width','100%');
//				}else{
//					$('#schedule_show_main,#schedule_main').css('width',(schedule_main_width+monthDay.length)+'px');
//				}
				$('#schedule_day').css('width',(schedule_main_width-155)+'px');
				$('#schedule_show_main').css('width',(schedule_main_width)+75+'px');
				
				for(var j=0;j<monthDay.length-1;j++){
					if(wstuo.schedule.scheduleMain.ifSatSun(monthDay[j])){
						$('#schedule_day_center').append('<div style="width:35px;display:inline-block;border-right:1px solid;overflow: auto;background-color: #D8D8D8;" align="center" title="'+monthDay[j]+'">'+
								wstuo.schedule.scheduleMain.dateFormat(monthDay[j])+
								'<input type="hidden" id="schedule_day_'+j+'" value="'+monthDay[j]+'" />'+
								'</div>');	
					}else{
						$('#schedule_day_center').append('<div style="width:35px;display:inline-block;border-right:1px solid;overflow: auto;" align="center" title="'+monthDay[j]+'" >'+
								wstuo.schedule.scheduleMain.dateFormat(monthDay[j])+
								'<input type="hidden" id="schedule_day_'+j+'" value="'+monthDay[j]+'" />'+
								'</div>');	
					}
				}
				//根据请求数控制行
				if(data!=null && data.scheduleShowDataDTO!=null && data.scheduleShowDataDTO.length>0){
					//得到行程数据
					var _showData = data.scheduleShowDataDTO;
					for(var k=0;k<_showData.length;k++){//对行程进行循环遍历
							//判断是否要换行显示
							//输出行程行
							//技术员格标题显示
							$('#schedule_day_technician').append('<div  id="schedule_technician_tr_'+k+'" style="border:1px solid;height: 15px;"></div>');
							//时间行标题显示
							$('#schedule_day_ticket').append('<div id="schedule_event_task_tr_'+k+'" style="border:1px solid;height: 15px;"></div>');
							//设置时间行宽度
							$('#schedule_event_task_tr_'+k).css('width',(schedule_main_width-147)+'px')
							//技术员+模块标题显示
							$('#schedule_technician_tr_'+k).append('<div style="width:157px;height: 22px;display:inline-block;overflow: auto;border-right:1px solid;">'+
									/*'<span style="width: 50%;display:inline-block;border-right: 1px solid;padding-left:3px;" title="'+_showData[k].category+'">'+
									wstuo.schedule.scheduleMain.showLengthFormat(_showData[k].category,5)+
									'</span>'+*/
									'<span style="padding-left:3px;" title="'+_showData[k].technician+'">'+wstuo.schedule.scheduleMain.showLengthFormat(_showData[k].technician,6)+'</span>'+
//									'<div style="width:147px;display:inline-block;overflow: auto;border-right:1px solid;">&nbsp;</div>'+
									'</div>');
							
							//得到当前行程行下的行程数据
							var _data=_showData[k].data;
							for(var j=0;j<monthDay.length-1;j++){
								//判断是否是周六周日，如果是则背景色为灰色
								if(wstuo.schedule.scheduleMain.ifSatSun(monthDay[j]))
									$('#schedule_event_task_tr_'+k).append('<div id="schedule_day_'+k+'_'+j+'" style="width:35px;display:inline-block;border-right:1px solid;overflow: auto;background-color: #D8D8D8;" align="center" >&nbsp;</div>');
								else
									$('#schedule_event_task_tr_'+k).append('<div id="schedule_day_'+k+'_'+j+'" style="width:35px;display:inline-block;border-right:1px solid;overflow: auto;" align="center" >&nbsp;</div>');
							
								//显示相应Ticket进度条
								for(var n=0;n<_data.length;n++){
									
									if(wstuo.schedule.scheduleMain.sameTime($('#schedule_day_'+j).val(),_data[n].startTime,_data[n].endTime)){
										
										//在不同的格式上加上相应的颜色
										var background ='../images/schedule/{img}.png';
										
										if(_data[n].hang=='HANG'){
											if(wstuo.schedule.scheduleMain.isLastEndDate($('#schedule_day_'+j).val(),_data[n].startTime,_data[n].endTime)){
												background = background.replace('{img}','hang_02');
											}else
												background = background.replace('{img}','hang_01');
										}else{
											
											//判断是Ticket还是Project
											if(_data[n].type=='Project'){
												if(wstuo.schedule.scheduleMain.isLastEndDate($('#schedule_day_'+j).val(),_data[n].startTime,_data[n].endTime))
													background = background.replace('{img}','project_02');
												else
													background = background.replace('{img}','project_01');
											}if(_data[n].type=='It'){//判断是Ticket还是IT
												if(wstuo.schedule.scheduleMain.isLastEndDate($('#schedule_day_'+j).val(),_data[n].startTime,_data[n].endTime))
													background = background.replace('{img}','it_02');
												else
													background = background.replace('{img}','it_01');
											}else{
												
												if(wstuo.schedule.scheduleMain.isLastEndDate($('#schedule_day_'+j).val(),_data[n].startTime,_data[n].endTime))
													background = background.replace('{img}','ticket_02');
												else
													background = background.replace('{img}','ticket_01');
											}
											
											
										}
										
										if(_data[n].type=='Task'){
											var background_task ='../images/schedule/{img}.png';
											if(wstuo.schedule.scheduleMain.isLastEndDate($('#schedule_day_'+j).val(),_data[n].startTime,_data[n].endTime)){
												background_task = background_task.replace('{img}','task_02');
												_showTask=_showTask+'&task_'+_data[n].taskId;
											}else{
												background_task = background_task.replace('{img}','task_01');
											}
											$('#schedule_day_'+k+'_'+j).html('<div '+
													'onMouseMove="wstuo.schedule.scheduleMain.showTaskTip(event,\''+_data[n].title+'\',\''+_data[n].location+'\',\''+_data[n].introduction+'\',\''+_data[n].status+'\',\''+_data[n].startTime+'\',\''+_data[n].endTime+'\',\''+_data[n].allDay+'\')" '+
													'onMouseOut="wstuo.schedule.scheduleMain.hideTaskTip()" '+
													'ondblclick="wstuo.schedule.scheduleMain.openTaskUpdateWin('+_data[n].taskId+')"'+
													'style="width: 35px;display:inline-block;background-image: url('+background_task+');">&nbsp;</div>');
											
										}else{
											var eventType=_data[n].eventType.replace(".","_")+"_";
											var key =eventType+_data[n].eno;
											if(data.events!=null && data.events[key]!=undefined){
												$('#schedule_day_'+k+'_'+j).html('<div id="schedule_ticket_show_'+k+'_'+j+'"'+
														'onMouseMove="wstuo.schedule.scheduleMain.showTip(event,\''+_data[n].eno+'\',\''+data.events[key].title+'\',\''+data.events[key].status+'\',\''+data.events[key].eventCode+'\',\''+_data[n].startTime+'\',\''+_data[n].endTime+'\')" '+
														'onMouseOut="wstuo.schedule.scheduleMain.hideTip()" '+
														'ondblclick="wstuo.schedule.scheduleMain.openReuqestDetail(\''+_data[n].eno+'\',\''+_data[n].eventType+'\')"'+
														'style="width:35px;display:inline-block;background-image: url('+background+');">&nbsp;</div>');
											}
										}
										
									}
								}	
							}
							if($('#schedule_event_task_tr_'+k).html().indexOf('onmouseout')==-1){
								
								$('#schedule_event_task_tr_'+k+',#schedule_technician_tr_'+k).hide();
							}	
					}	
				}else{
					$('#schedule_day_ticket').append('<div class="schedule_event_task_tr"><span style="color:red;">'+i18n['scheduleNotHaveInfo']+'!</span></div>');
				}
				endProcess();
			});
			
		},

		getTs:function(date){
			var d = date.split('-');
			return Date.parse((d[0]+'-'+d[1]+'-'+d[2]).replace(/-/g,"/"));
		},
		getDate:function(ts){
			ts = new Date(ts);
			var year='',month='',day='';
			with(ts){
				year = getFullYear();
				month = getMonth()+1;
				day = getDate();
			}
			if(month<10){
				month = '0'+month
			}
			if(day<10){
				day = '0'+day
			}
			return year+'-'+month+'-'+day;
		},
		/**
		 * 根据开始时间和结束时间返回两时间之间的日期JSON数据
		 * @param startTime 开始时间
		 * @param endTime 结束时间
		 */
		dateBetweenDayByStartAndEndTime:function(startTime,endTime){
			var t1 = wstuo.schedule.scheduleMain.getTs(startTime);
			var t2 = wstuo.schedule.scheduleMain.getTs(endTime);
			var str = '';
			while(t1<=t2){
				str +=wstuo.schedule.scheduleMain.getDate(t1)+'<>';
				t1 +=3600*24*1000;
			}
			return str;
		},
		/**
		 * 根据日期获取星期
		 */
		getWeek:function(date){
			
			var t1 = new Date(Date.parse(date.replace(/-/g,"/")));
			var y = t1.getDay() ; //返回0～6，表示星期日～星期六
			
		    var week = "";
		    if (y == 0){
		    	week = ""+i18n['label_calendar_Sunday']+"" ;
		    }
		    else if (y == 1)
		    {
		    	week = ""+i18n['label_calendar_Monday']+"" ;
		    }
		    else if (y == 2)
		    {
		    	week = ""+i18n['label_calendar_Tuesday']+"" ;
		    }
		    else if (y == 3)
		    {
		    	week = ""+i18n['label_calendar_Wednesday']+"" ;
		    }
		    else if (y == 4)
		    {
		    	week = ""+i18n['label_calendar_Thursday']+"" ;
		    }
		    else if (y == 5)
		    {
		    	week = ""+i18n['label_calendar_Friday']+"" ;
		    }
		    else if (y == 6)
		    {
		    	week = ""+i18n['label_calendar_Saturday']+"" ;
		    }
			
			return week;
		},
		
		/**
		 * 判断日期是否在同一日期内
		 */
		sameTime:function(p1,startTime,endTime){
			//要比较的日期
			var t1 = new Date(Date.parse(p1.replace(/-/g,"/")));
			
			var t2 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			
			var t2_1 = t2.format("yyyy-MM-dd")
			
			var t2_2 = Date.parse(t2_1.replace(/-/g,"/"))
			
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));

			if(t2_2<=t1.getTime() && t1.getTime()<=t3.getTime()){
				return true;
			}else
				return false;
		},
		/**
		 * 判断是否是最后的结束时间，用于显示最后的进度箭头
		 */
		isLastEndDate:function(p1,startTime,endTime){
			var t1 = new Date(Date.parse(p1.replace(/-/g,"/")));
			var t2 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));
			var end = t3.format("yyyy-MM-dd")
			if(p1==end)
				return true;
			else
				return false;
		},
		
		/**
		 * 显示请求详情
		 */
		showTip:function(ev,eno,etitle,estatus,eventCode,startTime,endTime){
			var left = wstuo.schedule.scheduleMain.getMousePoint(ev).x;
			var top = wstuo.schedule.scheduleMain.getMousePoint(ev).y;
			$('#showRequestInfo_div').css({
				'top':top-40,
				'left':left-230,
				'display':'block',
				'position':'absolute',
				'border':'1px solid #85BDE2',
				'background-color':'#eee' 
			});
			$('#showRequestInfo_ecode').text(eventCode);
			$('#showRequestInfo_etitle').text(etitle);
			$('#showRequestInfo_estatus').text(estatus);
			$('#showRequestInfo_startTime').text(startTime);
			$('#showRequestInfo_endTime').text(endTime);
		},
		/**
		 * 隐藏请求详情
		 */
		hideTip:function(){
			$('#showRequestInfo_div').css({
				'display':'none'
			});
		},
		/**
		 * 显示任务详情
		 */
		showTaskTip:function(ev,title,location,introduction,taskStatus,startTime,endTime,allDay){
			var left = wstuo.schedule.scheduleMain.getMousePoint(ev).x;
			var top = wstuo.schedule.scheduleMain.getMousePoint(ev).y;
			$('#showTaskInfo_div').css({
				'top':top-40,
				'left':left-230,
				'display':'block',
				'position':'absolute',
				'border':'1px solid #F0F0F0',
				'background-color':'#eee' 
			});
			$('#showTaskInfo_title').text(title);
			$('#showTaskInfo_location').text(location);
			$('#showTaskInfo_introduction').text(introduction);
			
			if(taskStatus=='0')
				taskStatus=i18n['title_newCreate']
			if(taskStatus=='1')	
				taskStatus=i18n['task_label_pending']
			if(taskStatus=='2')
				taskStatus=i18n['lable_complete']
			$('#showTaskInfo_taskStatus').text(taskStatus);
			$('#showTaskInfo_startTime').text(startTime);
			$('#showTaskInfo_endTime').text(endTime);
			if(allDay=='true')
				allDay = i18n['label_basicConfig_deafultCurrencyYes']
			else{
				allDay = i18n['label_basicConfig_deafultCurrencyNo']
			}
			$('#showTaskInfo_allDay').text(allDay);
		},
		/**
		 * 隐藏任务详情
		 */
		hideTaskTip:function(){
			$('#showTaskInfo_div').css({
				'display':'none' 
			});
		},
		/**
		 * 定义鼠标在视窗中的位置
		 */
		getMousePoint:function(ev){
			var point = {
				x:0,
				y:0
			};
		 
			// 如果浏览器支持 pageYOffset, 通过 pageXOffset 和 pageYOffset 获取页面和视窗之间的距离
			if(typeof window.pageYOffset != 'undefined') {
				point.x = window.pageXOffset;
				point.y = window.pageYOffset;
			}
			// 如果浏览器支持 compatMode, 并且指定了 DOCTYPE, 通过 documentElement 获取滚动距离作为页面和视窗间的距离
			// IE 中, 当页面指定 DOCTYPE, compatMode 的值是 CSS1Compat, 否则 compatMode 的值是 BackCompat
			else if(typeof document.compatMode != 'undefined' && document.compatMode != 'BackCompat') {
				point.x = document.documentElement.scrollLeft;
				point.y = document.documentElement.scrollTop;
			}
			// 如果浏览器支持 document.body, 可以通过 document.body 来获取滚动高度
			else if(typeof document.body != 'undefined') {
				point.x = document.body.scrollLeft;
				point.y = document.body.scrollTop;
			}
		 
			// 加上鼠标在视窗中的位置
			point.x += ev.clientX;
			point.y += ev.clientY;
		 
			// 返回鼠标在视窗中的位置
			return point;
		},
		/**
		 * 打开请求详细页面
		 */
		openReuqestDetail:function(eno,eventType){
			if(eventType=='itsm.problem'){
				basics.tab.tabUtils.reOpenTab('problem!findProblemById.action?eno='+eno,i18n['probelm_detail']);
			}else if(eventType=='itsm.change'){
				basics.tab.tabUtils.reOpenTab('change!changeInfo.action?queryDTO.eno='+eno,i18n['change_detail']);
			}else{
				basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+eno,i18n["request_detail"]);
			}
		},
		/**
		 * 上周
		 */
		prevWeek:function(){
			var datas =  basics.dateUtility.nextWeekMonth('WEEKS','w',_startDate);
			var _data = datas.split('&');
			
			_startDate = _data[0];
			_endDate = _data[1];
			wstuo.schedule.scheduleMain.initSchedule(_startDate,_endDate,'week','');
		},
		/**
		 * 下周
		 */
		nextWeek:function(){
			var datas =  basics.dateUtility.nextWeekMonth('WEEKS','next',_startDate);
			var _data = datas.split('&');
			_startDate = _data[0];
			_endDate = _data[1];
			wstuo.schedule.scheduleMain.initSchedule(_startDate,_endDate,'week','');
		},
		/**
		 * 本周
		 */
		thisWeek:function(){
			var nowTime = new Date().format("yyyy-MM-dd")
			_startDate =basics.dateUtility.showWeekFirstDay(nowTime).format("yyyy-MM-dd");
			_endDate =basics.dateUtility.showWeekLastDay(nowTime).format("yyyy-MM-dd");
			wstuo.schedule.scheduleMain.initSchedule(_startDate,_endDate,'week','');
		},
		
		/**
		 * 上月
		 */
		prevMonth:function(){
			var datas =  basics.dateUtility.nextWeekMonth('MONTHS','m',_startDate);
			var _data = datas.split('&');
			
			_startDate = _data[0];
			_endDate = _data[1];
			wstuo.schedule.scheduleMain.initMonthSchedule(_startDate,_endDate,'');
		},
		/**
		 * 下月
		 */
		nextMonth:function(){
			var datas =  basics.dateUtility.nextWeekMonth('MONTHS','next',_startDate);
			var _data = datas.split('&');
			_startDate = _data[0];
			_endDate = _data[1];
			wstuo.schedule.scheduleMain.initMonthSchedule(_startDate,_endDate,'');
		},
		/**
		 * 本月
		 */
		thisMonth:function(){
			var nowTime = new Date().format("yyyy-MM-dd")
			_startDate =basics.dateUtility.showMonthFirstDay(nowTime).format("yyyy-MM-dd");
			_endDate =basics.dateUtility.showMonthLastDay(nowTime).format("yyyy-MM-dd");
			wstuo.schedule.scheduleMain.initMonthSchedule(_startDate,_endDate,'');
		},
		
		
		
		/**
		 * 搜索
		 */
		search:function(){
			if($('#scheduleMain_search_form2').form('validate')){
				var _param = $('#scheduleMain_search_form1,#scheduleMain_search_form2').serialize();
				wstuo.schedule.scheduleMain.initMonthSchedule($('#scheduleMain_search_startTime').val(),$('#scheduleMain_search_endTime').val(),_param);
			}
		},
		/**
		 * 打开任务的编辑窗口
		 */
		openTaskUpdateWin:function(taskId){
			var postUrl="tasks!findByTaskId.action";
			var param="taskId="+taskId;
			$.post(postUrl,param,function(taskDto){
				$("#addcalendarTreatmentResultsTr").attr("style","display:none;");
				itsm.portal.schedule.historyShow(taskId);
				$('#schedule_task_process_but').show();
				$('#schedule_task_closed_but').show();
				$('#schedule_task_remark_but').show();
				$('#calendarResults_tr').hide();
				$('#calendarTask_taskId').val(taskId);
				$('#calendarTaskTitle').text(taskDto.title);
				$('#calendarTaskLocation').text(taskDto.location);
				$('#calendarTaskIntroduction').text(taskDto.introduction);
				$('#calendarTaskCreator').text(taskDto.creator);
				$('#calendarTaskOwner').text(taskDto.owner);
				$('#calendarTaskStartTime').text(timeFormatter(taskDto.startTime));
				$('#calendarTaskEndTime').text(timeFormatter(taskDto.endTime));
				$('#calendarRealStartTime').text(taskDto.realStartTime);
				$('#calendarRealEndTime').text(taskDto.realEndTime);
				$('#calendarRealFree').text(Math.floor(taskDto.realFree*1/1440)+i18n.label_slaRule_days+" "+Math.floor(taskDto.realFree*1%1440/60)+i18n.label_slaRule_hours+" "+Math.floor(taskDto.realFree*1%1440%60)+i18n.label_slaRule_minutes);
				if (taskDto.taskStatus == '0') {
					$('#calendarTaskStatus').text(i18n['title_newCreate']);
					$('#schedule_task_closed_but').hide();
				}
				if (taskDto.taskStatus == '1') {
					$('#calendarTaskStatus').text(i18n['task_label_pending']);
					$('#schedule_task_process_but').hide();
				}
				if (taskDto.taskStatus == '2') {
					$('#calendarTaskStatus').text(i18n['lable_complete']);
					$('#schedule_task_process_but').hide();
					$('#schedule_task_closed_but').hide();
					$('#calendarResults_tr').show();
					$('#calendarResults').text(taskDto.treatmentResults);
				}
				if (taskDto.allDay == 'true' || taskDto.allDay) {
					$('#calendarTaskAllDay').text(i18n['label_basicConfig_deafultCurrencyYes']);
				} else {
					$('#calendarTaskAllDay').text(i18n['label_basicConfig_deafultCurrencyNo']);
				}
				
				$('#schedule_task_process_but').hide();
				$('#schedule_task_closed_but').hide();
				$('#schedule_task_delete_but').hide();
				$('#schedule_task_edit_but').hide();
				$('#schedule_task_remark_but').hide();
				//$('#schedule_task_process_but').unbind().click(function(){processCalendarTask(id)});
				//$('#schedule_task_closed_but').unbind().click(function(){closedCalendarTask_opt(id)});
				//$('#schedule_task_remark_but').unbind().click(function(){remarkCalendarTask_opt(id)});
				windows('calendarTaskDiv',{width:600,modal: true});
			});
			
		},
		/**
		 * 日程表日期显示格式化，只显示月和日
		 */
		dateFormat:function(d){
			var _d = d.split('-');
			return _d[1]+'/'+_d[2];
		},
		/**
		 * 日程表日期显示格式化，只显示月和日
		 */
		yearFormat:function(d){
			var _d = d.split('-');
			return _d[0];
		},
		
		/**
		 * 根据是否是周六周日
		 */
		ifSatSun:function(date){
			var t1 = new Date(Date.parse(date.replace(/-/g,"/")));
			var y = t1.getDay() ; //返回0～6，表示星期日～星期六
		    if (y == 0){
		    	return true;
		    }
		    if (y == 6){
		    	return true;
		    }
		    
		    return false;
		},
		/**
		 * 判断精确到天是否是小于当前天
		 */
		dateLeToday:function(data){
			var nowTime = new Date().format("yyyy-MM-dd")
			var t1 = new Date(Date.parse(data.replace(/-/g,"/")));
			var d1 = t1.format("yyyy-MM-dd");
			if(nowTime<=d1)
				return true;
			else
				return false;
		},
		
		/**
		 * 显示长度
		 */
		showLengthFormat:function(value,length){
			if(value==null || value=='null' || value==''){
				return '-';
			}else{
				if(value!=null && value!=''){
					var _length = value.length;
					if(_length>length)
						return value.substring(0,length)+'...';
					else
						return value;
				}else{
					return value;
				}
			}
			
		},
		/**
		 * 根据模块或顾问进行排序
		 */
		scheduleSort:function(sidx){
			$('#scheduleMain_sidx').val(sidx);
			var _param = $('#scheduleMain_search_form1,#scheduleMain_search_form2').serialize();
			wstuo.schedule.scheduleMain.initMonthSchedule($('#scheduleMain_search_startTime').val(),$('#scheduleMain_search_endTime').val(),_param);
			if($('#scheduleMain_sord').val()=='desc'){
				$('#scheduleMain_sord').val('asc');
			}else{
				$('#scheduleMain_sord').val('desc');
			}
		},
		/**
		 * 导出到excel
		 */
		exportScheduleExcel:function(){
			$('#scheduleMain_search_form2').attr('action','scheduled!exportScheduleView.action');
			$("#scheduleMain_search_form2").submit();
		},
		/**
		 * 初始化
		 */
		init:function(){
			$('#scheduleMain_loading').hide();
			$('#scheduleMain_content').show();
			DatePicker97(['#scheduleMain_search_startTime','#scheduleMain_search_endTime']);
			
			//上月
			$('#scheduleMain_prevMonth').click(wstuo.schedule.scheduleMain.prevMonth);
			//下月
			$('#scheduleMain_nextMonth').click(wstuo.schedule.scheduleMain.nextMonth);
			//本月
			$('#scheduleMain_thisMonth').click(wstuo.schedule.scheduleMain.thisMonth);
			
			//选择顾问
			$('#scheduleMain_technician_name').click(function(){
				wstuo.tools.xssUtil.selectUser('#scheduleMain_technician_name','#scheduleMain_technician_id','','fullName',companyNo,function(){
				});
			});
			wstuo.schedule.scheduleMain.hideTaskTip();
			wstuo.schedule.scheduleMain.hideTip();
			//搜索
			$('#scheduleMain_search,#scheduleMain_refresh').click(wstuo.schedule.scheduleMain.search);
			
			_startDate = basics.dateUtility.showMonthFirstDay(_startDate);
			
			_endDate = basics.dateUtility.showMonthLastDay(_startDate);
			
			$('#scheduleMain_search_startTime').val(_startDate);
			$('#scheduleMain_search_endTime').val(_endDate);
			//初始化
			wstuo.schedule.scheduleMain.initMonthSchedule(_startDate,_endDate,'scheduleQueryDTO.startTime='+_startDate+'&scheduleQueryDTO.endTime='+_endDate+'&sidx=technician&scheduleQueryDTO.scheduleType=All');
			
			//优先级
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('priority','#scheduleMain_priority_select');
			//请求状态
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('requestStatus','#scheduleMain_status_select');
			
			//所属公司
			$('#scheduleMain_company_name').click(function(){//选择公司
				itsm.itsop.selectCompany.openSelectCompanyWin('#scheduleMain_company_id','#scheduleMain_company_name','');
			});
			//分类
			$('#scheduleMain_category_name').click(function(){
				common.config.category.eventCategoryTree.showSelectTree('#request_category_select_window'
						,'#request_category_select_tree'
						,'Request'
						,'#scheduleMain_category_name'
						,'#scheduleMain_category_id');
			});
			//根据滚动条显示
			$("#schedule_day_ticket").scroll(function() {
				$("#schedule_day").scrollLeft($("#schedule_day_ticket").scrollLeft());
				$("#schedule_day_technician").scrollTop($("#schedule_day_ticket").scrollTop());
				$("#schedule_day_ticket").css('height','415px');
			});
			/*$('#schedule_technician_sort').click(function(){
				wstuo.schedule.scheduleMain.scheduleSort('technician');
			});
			$('#schedule_category_sort').click(function(){
				wstuo.schedule.scheduleMain.scheduleSort('category');
			});*/

			$('#exportExcelBut').click(wstuo.schedule.scheduleMain.exportScheduleExcel);
			
			
			//选择请求分类
			$('#scheduleQuery_module').click(function(){
				common.config.category.eventCategoryTree.requestCategory('#scheduleQuery_module','');
			});
		}
	}
}();
$(function(){wstuo.schedule.scheduleMain.init()});