$package('wstuo.slaMge');
$import('wstuo.rules.ruleCM');
/**  
 * @author WSTUO  
 * @constructor slaDetail_SLARule
 * @description SLA规则主函数.

 */
wstuo.slaMge.slaDetail_ruleGrid_Operation = function(){
	this.actionNo;
	this.slaRuleOperationTag="";
	this._slaRuleSalienceFlag="0";
	this.i=0;
	
	
	return {
		

		/**
		 * @description 动态加载条件.
		 */
		addSLARule_ruleTermSet:function(){
			
			$('#addSLARule_team').html('');
			$(wstuo.rules.ruleCM.loadConditionHTMLs()).appendTo("#addSLARule_team");
			$('#addSLARule_matical').html('');
			$(wstuo.rules.ruleCM.loadConditionMathematicalOperationHTML()).appendTo("#addSLARule_matical");
		},
		
		/**
		 * @description 获取规则条件.
		 */
		addAndEdit_matchPname:function(){
			return $("#addSLARule_team").find("option:selected").text()+" "+$("#addSLARule_matical").find("option:selected").text();
		},
		
		/**
		 * @description 获取列表显示内容.
		 */
		addAndEdit_matchPvalue:function(){		
			return wstuo.rules.ruleCM.getRuleTitle('#addSLARule_team','#addSLARule_propertyValue','#slaRule_propertyName');
		},
	
		/**
		 * @description 设置值显示方式.
		 */
		setSLAPVHTML:function(){
			var flag=$('#addSLARule_team').val();
			wstuo.rules.ruleCM.createRuleEvent('',flag,'#addSLARule_propertyValueDIV','slaRule_propertyName','addSLARule_propertyValue','');
		},
		addSLA_switchOrAnd:function(){
			var andOr="<option value='or'>or</option><option value='and'>and</option>";
			if($("#addSLARule_and").attr("checked")==true){
				andOr="<option value='and'>and</option><option value='or'>or</option>";
			}
			return andOr;
			
		},
		/**
		 * @description 符号.
		 */
		addSLARule_setTerm:function(){
			wstuo.rules.ruleCM.setMathematicalOperation('#addSLARule_team','#addSLARule_matical');
		},
	
		
		/**
		 * @description 表单验证.
		 * @param method 回调函数
		 */
		checkBeforeSaveSLARule:function(method){
			var rday=$('#rule_rday').val();
			var rhour=$('#rule_rhour').val();
			var rminute=$('#rule_rminute').val();
			if(rday==null||rday==''){
				rday='0';
				$('#rule_rday').val('0');
			}
			if(rhour==null||rhour==''){
				rhour='0';
				$('#rule_rhour').val('0');
			}
			if(rminute==null||rminute==''){
				rminute='0';
				$('#rule_rminute').val('0');
			}
			
			var fday=$('#rule_fday').val();
			var fhour=$('#rule_fhour').val();
			var fminute=$('#rule_fminute').val();
			
			if(fday==null||fday==''){
				fday='0';
				$('#rule_fday').val('0');
			}
			if(fhour==null||fhour==''){
				fhour='0';
				$('#rule_fhour').val('0');
			}
			if(fminute==null||fminute==''){
				fminute='0';
				$('#rule_fminute').val('0');
			}
			var rule_ruleName=$('#rule_ruleName').val();
			
				var response_rate=0;response_rate=eval($('#rule_responseRate').val());
				var complete_rate=0;complete_rate=eval($('#rule_completeRate').val());
				var rtime = parseInt(rday)*24*60 + parseInt(rhour)*60 + parseInt(rminute);
				var ftime = parseInt(fday)*24*60 + parseInt(fhour)*60 + parseInt(fminute);
				
				if(Number(rtime)>Number(ftime)){
					msgAlert(i18n['rule_time_alert'],'info');
				}else if((rday=='0'&&rhour=='0'&&rminute=='0') || (fday=='0'&&fhour=='0'&&fminute=='0')){
					msgAlert(i18n['msg_msg_requestTimeAndCompleteTime'],'info');
					$('#slaTab_main').tabs('select',slaDetail);
				}else if(complete_rate>response_rate){
					msgAlert(i18n['rule_rate_alert'],'info');
				}else{
					if(slaRuleOperationTag=='save'){
					    startProcess();
						$.post('slaRule!slaNameExist.action','slaRuleQueryDTO.ruleName='+rule_ruleName,function(data){
							if(data){
								msgAlert(i18n['msg_ruleName_exist'],'info');
							}else{
								method();
							}
							endProcess();
						});
					}else{
						method();
					};
					
//					var constraintsNames = $('#addSLARuleDiv form input[name="rule_condition_constraints_propertyName"]');
//					
//					if(constraintsNames.length>0){
//						
//						
//					}else{
//						
//						msgAlert(i18n['msg_msg_vaildateSLARules'],'error');
//						$('#slaTab_main').tabs('select',fitRule);
//					}
				}
				$('#addSLARule_propertyValue').val('');
		
		},
		/**
		 * @description 清空表单数据
		 */
		clearData:function(){
			//清空数据
			$('#sla_rule_condition_rulePatternNo').val("");
			$('#sla_rule_ruleNo').val("");
			$('#rule_ruleName').val("");
			$('#slaRuleSalience').val("50");
			$('#rule_salience').val("50");
			$('#rule_rday').val("0");
			$('#rule_rhour').val("0");
			$('#rule_rminute').val("0");
			$('#rule_fday').val("0");
			$('#rule_fhour').val("0");
			$('#rule_fminute').val("0");
			$('#rule_responseRate').val("0");
			$('#rule_completeRate').val("0");
			$('#sla_rule_dataFlag').val("0");
			//清空规则列表
			$('#addRule_constraintsTable tbody').html('');
		},
		
		
		/**
		 * @description 新增SLA规则.
		 */
		addSLARuleOpenWindow:function(){
			var titilNames=new Array();
			titilNames[0]=i18n.label_rule_addRequestRule;
			titilNames[1]=i18n.label_rule_addEmailToRequestRule;
			titilNames[2]=i18n.label_rule_addChangeRule;
			titilNames[3]=i18n.label_rule_editRequestRule;
			titilNames[4]=i18n.label_rule_editEmailToRequestRule;
			titilNames[5]=i18n.label_rule_editChangeRule;
			titilNames[6]=i18n.lable_created_adddrl_processing;
			titilNames[7]=i18n.lable_created_editdrl_processing;
			
			wstuo.slaMge.slaDetail_ruleGrid_Operation.clearData();
			
			slaRuleOperationTag="save";
			$('#slaRuleSalience').html("");
			for(var i=0;i<=99;i++){
				if(i==50)
					$('#slaRuleSalience').append("<option value='"+i+"' selected='selected'>"+i+"</option>");
				else
					$('#slaRuleSalience').append("<option value='"+i+"'>"+i+"</option>");
			}
			windows('addSLARuleDivWindow',{title:i18n["title_sla_addSLARule"],width:700,height:530});
			$('#rule_salience_default').hide();
			$('#slaRuleSalience').show();
			//$('#rule_ruleName').attr({"disabled":""});
			$('#slaDetail_ruleGrid_addRuleToList_tr').show();
			$('#rule_salience').attr("name","");
			$('#slaRuleSalience').attr("name","rule.salience");
			
		},
		
		
		/**
		 * @description 编辑SLA规则.
		 */
		editSLARuleOpenWindow:function(){
			checkBeforeEditGrid('#slaDetail_ruleGrid',function(data){
				var titilNames=new Array();
				titilNames[0]=i18n.label_rule_addRequestRule;
				titilNames[1]=i18n.label_rule_addEmailToRequestRule;
				titilNames[2]=i18n.label_rule_addChangeRule;
				titilNames[3]=i18n.label_rule_editRequestRule;
				titilNames[4]=i18n.label_rule_editEmailToRequestRule;
				titilNames[5]=i18n.label_rule_editChangeRule;
				titilNames[6]=i18n.lable_created_adddrl_processing;
				titilNames[7]=i18n.lable_created_editdrl_processing;
				
				
				//删除表格里所有行
				$('#addRule_constraintsTable tbody').html('');
				
				var url = "slaRule!findSLARule.action?contractNo="+_contractNo+"&ruleNo="+data.ruleNo;
				
				slaRuleOperationTag="merge";
				wstuo.slaMge.slaDetail_ruleGrid_Operation.fillSLARuleData(url);
				$('#sla_rule_ruleNo').val(data.ruleNo);
				$('#sla_rule_dataFlag').val(data.dataFlag);
			
				windows('addSLARuleDivWindow',{title:i18n["title_sla_editSLARule"],width:700,height:530});
			});
		},
	
		/**
		 * @description 保存方法
		 */
		saveSLARuleMethod:function(){
			var ckVal=$("input[name='rule_includeHoliday']:checked").val(); 
			$('#rule_includeHolidayVl').val(ckVal);
			
			var rule=wstuo.rules.ruleCM.getRuleInfo('#addSLARuleDiv','#addSLARule_and','sla');//取得规则对象
			var url= "slaRule!"+slaRuleOperationTag+".action";
			
			var operationMsg=i18n["msg_add_successful"];
			if(slaRuleOperationTag=="merge"){
				operationMsg=i18n["msg_edit_successful"];
			}
			var salience=$('#slaRuleSalience').val();
			var dataFlag=$('#sla_rule_dataFlag').val();
			
			if(salience<100 || dataFlag==1){
				startProcess();
				$.post(url,rule,function(){
					$('#addSLARuleDivWindow').dialog('close');
					//提示信息
					msgShow(operationMsg,'show');
					//刷新列表
					$("#slaDetail_ruleGrid").trigger('reloadGrid');
					endProcess();//结束进程
				});
				
			}else{
				msgAlert(i18n['msg_salience_validate'],'info');
				$('#rule_salience').val('0');
				endProcess();//结束进程
			}
		},
		
		
		/**
		 * @description 提交新增.
		 */
		saveSLARule:function(){
			wstuo.slaMge.slaDetail_ruleGrid_Operation.checkBeforeSaveSLARule(wstuo.slaMge.slaDetail_ruleGrid_Operation.saveSLARuleMethod);
		},
		/**
		 * @description 获取全部的id
		 * */
		sla_forValues:function(){
			var arr=new Array();
			var propertyValueid=$('#addSlaRule_propertyValueid').val();
			var propertyValue = $('#addSLARule_propertyValue').val();
			var addPropertyID=$('#addSLARule_team').val();
			
			var arrayValue="";
			 
			if(propertyValueid!=undefined)
				arr=propertyValueid.split(",");
			else
				arr=propertyValue.split(",");
			 var lan=arr.length;
			 for(var i=0;i<lan;i++){
					arrayValue+=addPropertyID+arr[i]+",";
			 }
			return arrayValue;
		},

		/**
		 * @description 添加规则集.
		 */
		addToSLARuleList:function(){
			var propertyName = $('#addSLARule_team').val()+' '+$('#addSLARule_matical').val();
			var propertyValue = $('#addSLARule_propertyValue').val();
			var addPropertyID = $('#addSLARule_team').val();
			
			var arr=$("."+addPropertyID);
		    var arrValue="";
			for ( var it = 0; it < arr.size(); it++) {
				arrValue+=$("."+addPropertyID+":eq("+it+")").val();
			}
			
			var array1=new Array();
			var array2=new Array();
			array1=arrValue.split(",");
			var forValues=wstuo.slaMge.slaDetail_ruleGrid_Operation.sla_forValues();
			if(forValues.length>1)
				forValues=forValues.substring(0,forValues.length-1);
			array2=forValues.split(",");
			
			for(var a=0;a<array1.length;a++){
				
				for(var b=0;b<array2.length;b++){
					
					if(array1[a]===array2[b]){
						
						msgAlert(i18n['label_requireHave'],'info');
						return false;
					}
				}
			}

			if(propertyValue!=""){
				
			var key=$('#addSLARule_matical').val();
			if(key=="matches"){
				propertyValue=".*"+propertyValue+".*";
			}
			
			if(key=="matches start"){
				propertyValue=propertyValue+".*";
			}
			
			if(key=="matches end"){
				propertyValue=".*"+propertyValue;
			}
			if(key=="in"){
				propertyValue=$('#addSlaRule_propertyValueid').val();
					
				if(propertyValue==undefined)
					propertyValue = $('#addSLARule_propertyValue').val();
			}
			if(key=="notIn"){
				propertyValue=$('#addSlaRule_propertyValueid').val();
					
				if(propertyValue==undefined)
					propertyValue = $('#addSLARule_propertyValue').val();
			}
			if (addPropertyID == "weekNo") {
				propertyValue = wstuo.rules.ruleCM
						.convert_string(propertyValue);
			}
			i++;

			var trHTML=wstuo.rules.ruleCM.replaceRuleStr(i,
					wstuo.slaMge.slaDetail_ruleGrid_Operation.addAndEdit_matchPname(),
					'',
					wstuo.rules.ruleCM.switchDataType('#addSLARule_team'),
					propertyName,
					wstuo.slaMge.slaDetail_ruleGrid_Operation.addAndEdit_matchPvalue(),
					wstuo.slaMge.slaDetail_ruleGrid_Operation.addAndEdit_matchPvalue(),
					propertyValue,
					wstuo.slaMge.slaDetail_ruleGrid_Operation.addSLA_switchOrAnd(),
					
					addPropertyID,
					wstuo.slaMge.slaDetail_ruleGrid_Operation.sla_forValues(),
					'addSLARule'
					
			);
			$("#addRule_constraintsTable tbody").append(trHTML);
			$('#addSLARule_propertyValue').val('');
			$('#slaRule_propertyName').val('');
			$('#addSLARule_propertyValue').val('');
			}else{
				msgAlert(i18n['label_rule_actionValueCanNotBeNull'],'info');
			}
		},

	
		/**
		 * @description 查找动集并显示.
		 * @param url post url
		 */
		fillSLARuleData:function(url){
			$.post(url,function(res){	
				 var data=eval('('+res+')');
				 actionNo=data.actionNo;
				 $('#sla_rule_condition_rulePatternNo').val(data.rulePatternNo);
				 $('#sla_rule_ruleNo').val(data.ruleNo);
				 $('#rule_ruleName').val(data.ruleName);
				 $('#rule_salience').val($vl(data.salience));
				 $('#rule_rday').val(data.rday);
				 $('#rule_rhour').val(data.rhour);
				 $('#rule_rminute').val(data.rminute);
				 $('#rule_fday').val(data.fday);
				 $('#rule_fhour').val(data.fhour);
				 $('#rule_fminute').val(data.fminute);
				 $('#rule_responseRate').val('0');
				 $('#rule_completeRate').val('0');
				 $('#slaRuleSalience').html("");
				 $('#slaRuleSalience').val($vl(data.salience));
				 for(var i=0;i<=99;i++){
					if(i==$vl(data.salience))
						$('#slaRuleSalience').append("<option value='"+i+"' selected='selected' >"+i+"</option>");
					else
						$('#slaRuleSalience').append("<option value='"+i+"'>"+i+"</option>");
				 }
				 $('#slaRuleSalience').append("<option hidden='true' value='100'>100</option>");
				if(data.dataFlag!=null && data.dataFlag==1){
					//$('#rule_ruleName').attr({"disabled":true});
					$('#slaDetail_ruleGrid_addRuleToList_tr').hide();
					$('#rule_salience_default').show();
					$('#rule_salience_default').val($vl(data.salience));
					$('#rule_salience_default').attr({"readonly":"readonly"});
					$('#rule_salience').attr("name","rule.salience");
					$('#slaRuleSalience').hide();
					$('#rule_salience,#slaRuleSalience').val($vl(data.salience));
				}else{
					//$('#rule_ruleName').attr({"readonly":""});
					$('#slaDetail_ruleGrid_addRuleToList_tr').show();
					$('#rule_salience_default').hide();
					$('#slaRuleSalience').show();
					//$('#rule_salience').numberbox({"max":"99","disabled":false});
				}
				 if(data.responseRate!=null && data.responseRate!="null"){
					 $('#rule_responseRate').val(data.responseRate);
				 }
				 
				 if(data.completeRate!=null && data.completeRate!="null"){
					 $('#rule_completeRate').val(data.completeRate);
				 }
				 
				 $("input[name='rule_includeHoliday']").val([data.includeHoliday]);  
				 for(var i=0;i<data.constraints.length;i++){
					 
					 if(data.constraints[i]!=null){
	
						 $('input[name="orand"]').val([data.constraints[i].andOr]);        //选中radio
						  
						 	var pnameid=data.constraints[i].propertyName;
						 	var pnameValue=data.constraints[i].propertyValue;
						   
						    var array1=new Array();
						    var dataid=(pnameValue).replace("(","").replace(")","");
						    var dataValue=pnameid.replace(" in","").replace(" notIn","").replace("..","");
						    var dataZhi="";
						    array1=dataid.split(",");
						    for(var ii=0;ii<array1.length;ii++){
						    	dataZhi+=dataValue+array1[ii]+",";
						    }
						   
						    
							var trHTML=wstuo.rules.ruleCM.replaceRuleStr(data.constraints[i].conNo,
									wstuo.rules.ruleCM.switchRuleName(data.constraints[i].propertyName),
									data.constraints[i].conNo,
									data.constraints[i].dataType,
									data.constraints[i].propertyName,
									data.constraints[i].propertyValueName,
									data.constraints[i].propertyValueName,
									data.constraints[i].propertyValue,
									wstuo.slaMge.slaDetail_ruleGrid_Operation.editSLA_switchOrAnd(data.constraints[i].andOr),
									dataValue,
									dataZhi,
									'addSLARule'
									
							);
						$("#addRule_constraintsTable tbody").append(trHTML);
						 
					 }
					 
				 }
			});
	
		},
		/**
		 * @description SLA编辑join条件构造
		 * @param joinType 连接类型
		 */
		editSLA_switchOrAnd:function(joinType){
			var andOr="<option value='or'>or</option><option value='and'>and</option>";
			if(joinType=='and'){
				andOr="<option value='and'>and</option><option value='or'>or</option>";
			}
			return andOr;
			
		},
	
	};
	
}();
