/**
 * 控件类
 * @author will
 */
function formControl(obj){
	
	this.commonHtml='<label class="control-label">{label}{required}：</label><i onclick="wstuo.customForm.customFormDesign.deleteField(this)" class="glyphicon glyphicon-trash float_right"></i>{field}';
	this.field='';
	
	this.fieldType=obj.fieldType;
	this.attrType=obj.attrType;
	this.label=obj.label;
	this.value=obj.value!=undefined?obj.value:'';
	this.attrNo=obj.attrNo;
	this.attrName=obj.attrName;
	this.name=this.attrName;
	this.required=obj.required;//是否必填  false必填
	
	this.detail=false;
	
	this.getAttrs=function(){
		return {
			fieldType:this.fieldType,
			attrType:this.attrType,
			label:this.label,
			value:this.value,
			attrNo:this.attrNo,
			attrName:this.attrName,
			required:this.required//是否必填  false必填
		};
	};
	/**
	 * 生成详情HTMl时替换
	 */
	this.replaceCommonAttrByDetail=function(field,bool){
		this.field=field;
		this.detail=bool;
		return this.replaceCommonAttr();
	};
	/**
	 * 替换公共的属性
	 * @param bool 是否是详情
	 */
	this.replaceCommonAttr=function(){
		var html=this.commonHtml.replace(/{field}/g,this.field).replace(/{label}/g,this.label)
				.replace(/{id}/g,this.attrNo).replace(/{name}/g,'requestDTO.attrVals[\''+this.attrName+'\']').replace(/{attrType}/g,this.attrType)
				.replace(/{value}/g,this.value);
		if(this.detail || this.required==="false")
			html=html.replace(/{required}/g,'');
		else{
			html=html.replace(/{required}/g,'<span class="required">*</span>')
					.replace(/class="form-control/g,'validType="nullValueValid" required="true" class="form-control');
			
		}
		return html;
	};
	
}
function radio(obj){
	formControl.call(this,obj);//继承
	this.attrItemName=obj.attrItemName;//多选选项
	this.attrItemNo=obj.attrItemNo;//多选选项
	this.attrColumn=obj.attrColumn;//显示列数
	this.attrdataDictionary=obj.attrdataDictionary;
	this.getAllAttrs=function(){
		var attrs=this.getAttrs();
		attrs.attrdataDictionary=this.attrdataDictionary;
		attrs.attrItemName=this.attrItemName;
		attrs.attrColumn=this.attrColumn;
		return attrs;
	};
	this.replaceAttr=function(){
		return this.replaceCommonAttr().replace(/{attrdataDictionary}/g,this.attrdataDictionary);
	};
	this.getControl=function(){
/*		var data = this.attrItemName.split(',');
		var dataNo = this.attrItemNo.split(',');
		for ( var _int = 0; _int < data.length; _int++) {
			var validType='';
			if(_int==data.length-1 && this.required==="false"){
				validType='validType="radioAndchekboxValid[\''+this.attrNo.substr(0,this.attrNo.lastIndexOf('_'))+'\',\''
				+this.name+'\']" required="true"';
			}
			this.field=this.field+'&nbsp;&nbsp;<label class="radio-inline"><input name="'+this.name+'" type="radio" {checked} '+
			validType+' value="'+dataNo[_int]+'" >'+data[_int]+'</label>&nbsp;&nbsp;';
			if(this.value===dataNo[_int])
				this.field=this.field.replace(/{checked}/g,'checked="checked"');
			else
				this.field=this.field.replace(/{checked}/g,'');
		}*/
		this.field='<label class="radio-inline"><input type="radio" value="1">radio1</label>&nbsp;&nbsp<label class="radio-inline"><input type="radio" value="2">radio2</label>';
		this.field=this.field + '<input name="requestDTO.attrVals[\''+this.attrName+'\']"  attrdataDictionary={attrdataDictionary} type="hidden" attrType="'+this.attrType+'" id="'+this.name+'"  class="form-control">';
		return this.replaceAttr();
	};
}
function checkbox(obj){
	formControl.call(this,obj);//继承
	this.attrItemName=obj.attrItemName;//多选选项
	this.attrColumn=obj.attrColumn;//显示列数
	this.attrItemNo=obj.attrItemNo;//多选选项
	this.attrdataDictionary=obj.attrdataDictionary;
	this.getAllAttrs=function(){
		var attrs=this.getAttrs();
		attrs.attrdataDictionary=this.attrdataDictionary;
		attrs.attrItemName=this.attrItemName;
		attrs.attrColumn=this.attrColumn;
		return attrs;
	};
	this.replaceAttr=function(){
		return this.replaceCommonAttr().replace(/{attrdataDictionary}/g,this.attrdataDictionary);
	};
	this.getControl=function(){
		/*var data = this.attrItemName.split(',');
		var dataNo = this.attrItemNo.split(',');
		for ( var _int = 0; _int < data.length; _int++) {
			var validType='';
			if(_int==data.length-1 && this.required==="false"){
				validType='validType="radioAndchekboxValid[\''+this.attrNo.substr(0,this.attrNo.lastIndexOf('_'))+'\',\''
					+this.name+'\']" required="true"';
			}
			this.field=this.field+'&nbsp;&nbsp;<label class="checkbox-inline"><input name="'+this.name+'" type="checkbox" {checked} '+validType+' value="'+dataNo[_int]+'">'+data[_int]+'</label>&nbsp;&nbsp;';
			if(this.value.indexOf(data[_int])>-1)
				this.field=this.field.replace(/{checked}/g,'checked="checked"');
			else
				this.field=this.field.replace(/{checked}/g,'');
		}*/
		this.field='<label class="checkbox-inline"><input type="checkbox" value="1">checkbox1</label>&nbsp;&nbsp;<label class="checkbox-inline"><input type="checkbox" value="2">checkbox2</label>';
		this.field=this.field + '<input name="requestDTO.attrVals[\''+this.attrName+'\']"  type="hidden" attrdataDictionary={attrdataDictionary} attrType="'+this.attrType+'" id="'+this.name+'"  class="form-control">';
		return this.replaceAttr();
	};
}
function date(obj){
	formControl.call(this,obj);//继承
	this.getAllAttrs=function(){
		var attrs=this.getAttrs();
		return attrs;
	};
	this.getControl=function(){
		this.field='<input label="{label}" type="text" id="{id}" name="{name}" attrType="{attrType}" value="{value}" class="form-control" />';
		return this.replaceCommonAttr();
	};
}
function select(obj){
	formControl.call(this,obj);//继承
	this.attrdataDictionary=obj.attrdataDictionary;
	this.getAllAttrs=function(){
		var attrs=this.getAttrs();
		attrs.attrdataDictionary=this.attrdataDictionary;
		return attrs;
	};
	this.replaceAttr=function(){
		return this.replaceCommonAttr().replace(/{attrdataDictionary}/g,this.attrdataDictionary);
	};
	this.getControl=function(){
		this.field='<select label="{label}" id="{id}" name="{name}" attrdataDictionary={attrdataDictionary} attrType="{attrType}" val="{value}" class="form-control"></select>';
		return this.replaceAttr();
	};
}
function textarea(obj){
	formControl.call(this,obj);//继承
	this.getAllAttrs=function(){
		var attrs=this.getAttrs();
		return attrs;
	};
	this.getControl=function(){
		this.field='<textarea label="{label}" id="{id}" name="{name}" attrType="{attrType}" class="form-control" style="height: 53px;">{value}</textarea>';
		return this.replaceCommonAttr().replace(/class="field"/g,'class="field_lob"');
	};
}
function number(obj){
	formControl.call(this,obj);//继承
	this.getAllAttrs=function(){
		var attrs=this.getAttrs();
		return attrs;
	};
	this.getControl=function(){
		this.field='<input label="{label}" type="text" id="{id}" name="{name}"  attrType="{attrType}" value="{value}" class="form-control" />';
		return this.replaceCommonAttr();
	};
}
/**
 * 小数
 */
function double(obj){
	formControl.call(this,obj);//继承
	this.getAllAttrs=function(){
		var attrs=this.getAttrs();
		return attrs;
	};
	this.getControl=function(){
		this.field='<input label="{label}" type="text" id="{id}" name="{name}" attrType="{attrType}" value="{value}" class="form-control" />';
		return this.replaceCommonAttr();
	};
}
/**
 * 树
 */
function tree(obj){
	formControl.call(this,obj);//继承
	this.onclick='wstuo.category.eventCategoryTree.showSelectTree(\'#request_category_select_window\',\'#request_category_select_tree\','+
                        '\'{attrdataDictionary}\','+
                        '\'#show_{id}\','+
                        '\'#{id}\',\'\',\'\',\'\',\'\')';
    this.attrdataDictionary=obj.attrdataDictionary;
	this.getAllAttrs=function(){
		var attrs=this.getAttrs();
		attrs.attrdataDictionary=this.attrdataDictionary;
		return attrs;
	};
	this.replaceAttr=function(){
		return this.replaceCommonAttr().replace(/{attrdataDictionary}/g,this.attrdataDictionary);
	};
	this.getControl=function(){
		this.field='<input label="{label}" type="text" id="show_{id}" attrType="{attrType}" value="{value}" class="form-control" onclick="'+this.onclick+'"/>'
			+ '<input type="hidden" name="{name}" id="{id}" attrType="{attrType}" value="">';
		return this.replaceAttr();
	};
}
/**
 * 文本框
 */
function text(obj){
	formControl.call(this,obj);//继承
	this.attrHiddenId=obj.attrHiddenId;//隐藏域id
	this.attrHiddenName=obj.attrHiddenName;//隐藏域Name
	this.attrHiddenVal=obj.attrHiddenVal;//隐藏域value
	this.attrEventType=obj.attrEventType;//控件绑定事件的类型
	this.attrEventVal=obj.attrEventVal;//绑定事件的方法
	this.attrImgSrc=obj.attrImgSrc;
	this.attrI18n=obj.attrI18n;
	
	this.getAllAttrs=function(){
		var attrs=this.getAttrs();
		return attrs;
	};
	this.replaceAttr=function(){
		return this.replaceCommonAttr().replace(/{attrHiddenVal}/g,this.attrHiddenVal).replace(/{attrHiddenId}/g,this.attrHiddenId)
			.replace(/{attrHiddenName}/g,this.attrHiddenName).replace(/{attrEventType}/g,this.attrEventType)
			.replace(/{attrEventVal}/g,this.attrEventVal).replace(/{attrImgSrc}/g,this.attrImgSrc)
			.replace(/{attrI18n}/g,this.attrI18n);
			//.replace(/{event}/g,this.attrEventType+'="'+this.attrEventVal+'"');
	};
	this.getControl=function(){
		this.field='<input label="{label}" type="text" id="{id}" name="{name}" attrType="{attrType}" value="{value}" class="form-control"/>';
		this.field = this.judgeAttrs(this.field);
		return this.replaceAttr();
	};
	/**
	 * 生成隐藏域和事件
	 */
	this.judgeAttrs=function(field){
		if(this.attrHiddenId)
			field = field + '<input type="hidden" name="{attrHiddenName}" id="{attrHiddenId}" value="{attrHiddenVal}">';
		/*if(this.attrImgSrc){
			field = field.replace(/{event}/g,'') + '<a label="'+i18n[this.attrI18n]+'" {event}><img style="vertical-align:middle;" src="{attrImgSrc}"></a>';
			field = field.replace(/class="control"/g,"class=\"control-img\"");
		}*/
		return field;
	};
	
}
