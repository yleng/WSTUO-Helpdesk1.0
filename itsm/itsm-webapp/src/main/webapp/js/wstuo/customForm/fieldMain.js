$package('wstuo.customForm');
/**  
 * @author Wstuo  
 * @description 自定义字段.
 * @since version 1.0 
 */
wstuo.customForm.fieldMain=function(){
	this.operator=''; 
	return {
		typeFormat:function(cell,event,data){
			var type='文本';
			switch(data.type)
			{
				case "text":type='文本';break;
				case "textarea":type='多行文本';break;
				case "number":type='数字';break;
				case "double":type='小数';break;
				case "date":type='日期';break;
				case "select":type='下拉框';break;
				case "radio":type='单选框';break;
				case "checkbox":type='多选框';break;
				case "tree":type='树结构';break;
				default:type='文本';
			}
			return type;
		},
		 /**
		  * 查看用户操作日志列表
		  */
		 showfieldGrid:function(){
				var params = $.extend({},jqGridParams, {	
					url:'field!findPagefield.action',
					colNames:['ID',i18n['name'],i18n['moduleName'],'',i18n['type'],i18n.description,i18n['common_createTime'],'','','','','',i18n.sort,i18n.common_action],
					colModel:[
					          {name:'id',width:30,align:'center'},
					          {name:'fieldName',width:80,align:'center'},
							  {name:'module',width:80,align:'center',sortable:false},
							  {name:'type',width:80,hidden:true},
							  {name:'typeName',width:80,align:'center',sortable:false,formatter:wstuo.customForm.fieldMain.typeFormat},
							  {name:'description',width:100,align:'center',sortable:false},
							  {name:'createTime',width:80,align:'center'},
							  {name:'name',hidden:true},
							  {name:'required',hidden:true},
							  {name:'showList',hidden:true},
							  {name:'export',hidden:true},
							  {name:'search',hidden:true},
							  {name:'sort',width:80,align:'center'},
							  {name:'act', width:80,align:'center',sortable:false,formatter:function(cell,event,data){
								  return $('#fieldGridAction').html().replace(/{id}/g,event.rowId);
							  }}
							  ],
					jsonReader: $.extend(jqGridJsonReader, {id: "id"}),
					sortname:'sort',
					pager:'#fieldGridPager'
				});
				$("#fieldGrid").jqGrid(params);
				$("#fieldGrid").navGrid('#fieldGridPager',navGridParams);
				//列表操作项
				$("#t_fieldGrid").css(jqGridTopStyles);
				$("#t_fieldGrid").append($('#fieldGridToolbar').html());
				//自适应宽度
				setGridWidth("#fieldGrid",15);
		},
		 /**
		  * @description 搜索操作日志
		  */
		searchfield_do:function(){				
			//获得表单对象
			var sdata = $('#searchFieldDiv form').getForm();		
			var postData = $('#fieldGrid').jqGrid('getGridParam', 'postData');       
			$.extend(postData, sdata);
			var _url ="field!findPagefield.action";	
			$('#fieldGrid').jqGrid('setGridParam',{url:_url,page:1}).trigger('reloadGrid');
	
		},
		searchOpenWindow:function(){
			windows('searchFieldDiv',{width:400,modal: false});
		},
		addOpenWindow:function(){
			resetForm("#addFieldDiv form");$("#add_dictionary_div").hide();$("#add_dictionary").empty();
			$('#add_fieldType,#add_fieldModule,#add_dictionary').removeAttr("disabled");
			windows('addFieldDiv',{title:i18n['newAdd'],width:500});
			operator = 'save';
		},
		editOpenWindow:function(){
			checkBeforeEditGrid('#fieldGrid',wstuo.customForm.fieldMain.editOrgMethod);
			
		},
		editfield:function (rowId){
			var rowData= $("#fieldGrid").jqGrid('getRowData',rowId);  // 行数据  
			wstuo.customForm.fieldMain.editOrgMethod(rowData);
		},
		editOrgMethod:function(data){
			resetForm("#addFieldDiv form");$("#add_dictionary_div").hide();$("#add_dictionary").empty();
			$('#add_name').val(data.name);
			$('#add_fieldId').val(data.id);
			$('#add_fielddescription').val(data.description);
			$('#add_fieldType').val(data.type);
			$('#add_fieldModule').val(data.module);
			$('#add_fieldName').val(data.fieldName);
			if(data.type=="select" || data.type=="radio" || data.type=="checkbox" || data.type=="tree"){
				$("#add_dictionary_div").show();
				wstuo.customForm.fieldMain.loadDataDictionarysToSelect("#add_dictionary",data.type,data.dataDictionary);
			}
			if(data.required=='true')$("#field_required")[0].checked=true;
			if(data.showList=='true')$("#field_showList")[0].checked=true;
			if(data.export=='true')$("#field_export")[0].checked=true;
			//if(data.search=='true')$("#field_search")[0].checked=true;
			$('#add_fieldsort').val(data.sort);
			$('#add_fieldType,#add_fieldModule,#add_dictionary').attr("disabled","disabled");
			windows('addFieldDiv',{title:i18n.edit,width:500,close:function(){
				resetForm("#addFieldDiv form");
			}});
			operator = 'update';
		},
		 /**
		  * @description 新增数据字典组
		  * */
		saveField:function(){
			if($("#field_required").is(":checked"))$("#add_required").val(true);
			else $("#add_required").val(false);
			if($("#field_showList").is(":checked"))$("#add_showList").val(true);
			else $("#add_showList").val(false);
			if($("#field_export").is(":checked"))$("#add_export").val(true);
			else $("#add_export").val(false);
			/*if($("#field_search").is(":checked"))$("#add_search").val(true);
			else $("#add_search").val(false);*/
			removeFormDisable('#addFieldDiv form');
			var param = $('#addFieldDiv form').serialize();
			var url = "field!"+operator+".action";
			$.post(url, param, function(){
				$('#addFieldDiv').dialog('close');
				$('#fieldGrid').jqGrid('setGridParam',{page:1}).trigger('reloadGrid');
				//清空框内内容
				resetForm("#addFieldDiv form");
				msgShow(i18n['common_operation_success'],'info');
				$("#add_dictionary_div").hide();
			});	
			
		},
		deleteField:function(){
			checkBeforeDeleteGrid("#fieldGrid",wstuo.customForm.fieldMain.deleteFieldMethod);
		},
		delete_aff:function(id){
			msgConfirm(i18n.msg_msg,i18n['msg_confirmDelete'],function(){
				wstuo.customForm.fieldMain.deleteFieldMethod(id);
			});
		},
		deleteFieldMethod:function(rowIds){
			var param = $.param({'ids':rowIds},true);
			$.post("field!remove.action", param, function(){
				$('#fieldGrid').jqGrid('setGridParam',{url:'field!findPagefield.action',page:1,rows:20}).trigger('reloadGrid');
				//$('#fieldGrid').trigger('reloadGrid');
				msgShow(i18n['msg_deleteSuccessful'],"show");
			});
		},
		selectDictionary:function(type){
			if(type=="select" || type=="radio" || type=="checkbox" || type=="tree"){
				$("#add_dictionary_div").show();
				wstuo.customForm.fieldMain.loadDataDictionarysToSelect("#add_dictionary",type);
			}else
				$("#add_dictionary_div").hide();
		},
		loadDataDictionarysToSelect:function(select,type,dcode){
			$(select).html('');
			var url="dataDictionaryGroup!findDataDictionaryByType.action";
			$.post(url,'reDto.groupType='+type,function(dataDictionary){     
				$.each(dataDictionary,function(key,value){
					$("<option value='"+value.groupCode+"'>"+value.groupName+"</option>").appendTo(select);
				});
				if(dcode!=undefined)
					$(select).val(dcode);
				
			});            
		},
		loadModule:function(){
			$("#add_fieldModule").html('');
			var url="module!findAll.action";
			$.post(url,function(dataDictionary){     
				$.each(dataDictionary,function(key,value){
					$("<option value='"+value.moduleName+"'>"+value.title+"</option>").appendTo("#add_fieldModule");
				});
			});
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			 wstuo.customForm.fieldMain.showfieldGrid();//加载列表
			 $("#fieldGrid_search").click(function(){
				 windows('searchfield',{width:450,close:function(){
					 $('#searchfield').click();
				 }});
			 });
			 wstuo.customForm.fieldMain.loadModule();
			 $("#fieldGrid_doSearch").click(wstuo.customForm.fieldMain.searchfield_do);
		}
	 }
 }();
//载入
 $(document).ready(wstuo.customForm.fieldMain.init);