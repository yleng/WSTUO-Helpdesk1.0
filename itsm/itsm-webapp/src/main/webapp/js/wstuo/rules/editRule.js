$package('wstuo.rules');
$import('wstuo.rules.ruleCM');
/**  
 * @author WSTUO
 * @constructor WSTO
 * @description 编辑SLA规则. WSTUO/rules/editRule.jsp

 */
wstuo.rules.editRule = function(){
	
	this.j=0;
	this.i=0;
	
	
	return {
		/**
		 * @description 加载规则集属性.
		 */
		editRule_ruleTermSet:function(){
			
			$("#editRule_team").html(''); 
			$("#editRule_matical").html(''); 
			$(wstuo.rules.ruleCM.loadConditionHTMLs()).appendTo("#editRule_team"); 
			$(wstuo.rules.ruleCM.loadConditionMathematicalOperationHTML()).appendTo("#editRule_matical"); 
			
		},

		
		/**
		 * @description 取得属性中文显示名称.
		 * @param str 匹配规则动作名称
		 * @param putId 输入框ID
		 */
		switchRuleNameActionName:function(str,putId){
			var newStr=wstuo.rules.ruleCM.switchRuleName(str);
			$(putId).html(newStr);
		},
		
		/**
		 * @description 规则编辑条件匹配
		 */
		edit_switchOrAnd:function(){
			var andOr="<option value='or'>or</option><option value='and'>and</option>";
			
			if($("#edit_rule_and").attr("checked")==true){
				andOr="<option value='and'>and</option><option value='or'>or</option>";
			}
			return andOr;
		},
		/**
		 * @description 规则编辑标题匹配
		 */
		edit_switchTitle:function(){
			return $("#editRule_team").find("option:selected").text()+" "+$("#editRule_matical").find("option:selected").text();

		},
		
		/**
		 * 取值.
		 */
		edit_switchValue:function(){

			return wstuo.rules.ruleCM.getRuleTitle('#editRule_team','#editRule_propertyValue','#editRule_namePut');
			
		},
		
		
		/**
		 * @description 加载规则集运算符号.
		 */
		editRule_setTerm:function(){
		
			
			
			//加载条件名称HTML
			wstuo.rules.ruleCM.setMathematicalOperation('#editRule_team','#editRule_matical');
			
			var flag=$("#editRule_team").val();
			
			//调用方法显示规则条件HTML
			wstuo.rules.ruleCM.createRuleEvent('#editRule_content',flag,'#editRule_propertyValueDIV','editRule_namePut','editRule_propertyValue','');
			
		},

		/**
		 * @description 编辑.
		 */
		editRule_merge:function(){
			
			 var actionsNames = $('#editRule_content form:eq(2) input[name="rule_actions_propertyName"]');
				
				if(actionsNames.length>0){
					
					wstuo.rules.editRule.editRule_do();
					
				}else{
					
					msgAlert(i18n['msg_msg_vaildateRulesAndAction'],'info');
				}
			
		},
		
		/**
		 * @description 提交新增.
		 */
		editRule_do:function(){
		
			wstuo.rules.ruleCM.saveRuleCommon('#editRule_content','merge','#edit_rule_and',_flag);

		},
		/**
		 * @description 获取全部的id
		 * */
		edit_forValues:function(){
			var arr=new Array();
			var propertyValueid=$('#addRule_propertyValueid').val();
			var propertyValue = $('#editRule_propertyValue').val();
			var addPropertyID=$('#editRule_team').val();
			
			var arrayValue="";
			 
			if(propertyValueid!=undefined)
				arr=propertyValueid.split(",");
			else
				arr=propertyValue.split(",");
			 var lan=arr.length;
			 for(var i=0;i<lan;i++){
					arrayValue+=addPropertyID+arr[i]+",";
			 }
			return arrayValue;
		},
		
		/**
		 * @description 添加规则集.
		 */
		editRule_addRulesToList:function(){
			
			var propertyName = $('#editRule_team').val()+' '+$('#editRule_matical').val();
			var propertyValue = $('#editRule_propertyValue').val();
			var addPropertyID=$('#editRule_team').val();
			
			var arr=$("."+addPropertyID);
		    var arrValue="";
			for ( var it = 0; it < arr.size(); it++) {
				arrValue+=$("."+addPropertyID+":eq("+it+")").val();
			}
			
			var array1=new Array();
			var array2=new Array();
			array1=arrValue.split(",");
			var forValues=wstuo.rules.editRule.edit_forValues();
			if(forValues.length>1)
				forValues=forValues.substring(0,forValues.length-1);
			array2=forValues.split(",");
			
			for(var a=0;a<array1.length;a++){
				
				for(var b=0;b<array2.length;b++){
					
					if(array1[a]===array2[b]){
						
						msgAlert(i18n['label_requireHave'],'info');
						return false;
					}
				}
			}
			
			if(propertyValue!=''){
				
				var key=$('#editRule_matical').val();
				
				
				if(key=="matches"){
					propertyValue=".*"+propertyValue+".*";
				}
				
				if(key=="matches start"){
					propertyValue=propertyValue+".*";
				}
				
				if(key=="matches end"){
					propertyValue=".*"+propertyValue;
				}
				
				if(key=="in"){
					propertyValue=$('#addRule_propertyValueid').val();
					if(propertyValue==undefined)
						propertyValue = $('#editRule_propertyValue').val();
				}
				if(key=="notIn"){
					propertyValue=$('#addRule_propertyValueid').val();
					if(propertyValue==undefined)
						propertyValue = $('#editRule_propertyValue').val();
				}
				if(addPropertyID=="weekNo"){
					propertyValue=wstuo.rules.ruleCM.convert_string(propertyValue);
				}
				i++;
				
				var trHTML=wstuo.rules.ruleCM.replaceRuleStr(i,
						wstuo.rules.editRule.edit_switchTitle(),
						'',
						wstuo.rules.ruleCM.switchDataType('#editRule_team'),
						propertyName,
						wstuo.rules.editRule.edit_switchValue(),
						wstuo.rules.editRule.edit_switchValue(),
						propertyValue,
						wstuo.rules.editRule.edit_switchOrAnd(),
						addPropertyID,
						wstuo.rules.editRule.edit_forValues(),
						"editRule"
				);
				$("#editRule_constraintsTable tbody").append(trHTML);
				$('#editRule_propertyValue').val('');
				$('#editRule_namePut').val('');
				$('#editRule_propertyValue').val('');
				
			}else{
				
				msgAlert(i18n['msg_rule_ruleActionCanNotBeNull'],'info');
			}
		},


		
		/**
		 * @description 执行动作.
		 */
		editRule_executeAction:function(){
			
			$(wstuo.rules.ruleCM.loadActionHTML()).appendTo("#editRule_executeAction"); 
		},
		
		/**
		 * @description 动作集显示效果.
		 */
		edit_switchAction:function(){
			
			return $("#editRule_executeAction").find("option:selected").text(); 
			
		},
		
		/**
		 * @description 获取相关动作的标题以显示到列表.
		 */
		edit_switchGavenValue:function(){		
			
			return wstuo.rules.ruleCM.getRuleTitle('#editRule_executeAction','#editRule_givenValue','#editRule_givenValue_Panel');
		},
		
		
		/**
		 * @description 创建填写动作HTML控件.
		 */
		setEXHTML_edit:function(){
			
			var flag=$('#editRule_executeAction').val();

			wstuo.rules.ruleCM.createRuleEvent('#editRule_content',flag,'#editRule_givenValueDIV','editRule_givenValue_Panel','editRule_givenValue','wstuo.rules.editRule.editRule_selectServiceOrg()');
		},

		/**
		 * @description 选择服务机构.
		 */
		editRule_selectServiceOrg:function(){			

			wstuo.rules.ruleCM.selectServiceOrg('#index_selectServiceORG_window_tree','#index_selectServiceORG_window','#editRule_givenValue','#editRule_givenValue_Panel');
		},

		/**
		 * @description 添加动作集
		 */
		addToActionList:function(){
			
			var propertyName = $('#editRule_executeAction').val();
			var givenValue = $('#editRule_givenValue').val();
			
			var exitTag="0";

			$("#editRule_actionTable input:hidden[name='rule_actions_propertyName']").each(function(i,e){
				
				if(propertyName==$(this).val()){
					
					exitTag="1";
				}
				
			});
			
			var valueTag=$('#editRule_givenValue').val();

			if(exitTag=="0" && valueTag!=''){
				
				j++;
				
				 var trHTML=wstuo.rules.ruleCM.replaceActionStr(j,
						 wstuo.rules.editRule.edit_switchAction(),	
						 wstuo.rules.ruleCM.switchDataType('#editRule_executeAction'),
						 	propertyName,
						 	wstuo.rules.editRule.edit_switchGavenValue(),
						 	wstuo.rules.editRule.edit_switchGavenValue(),
						 	givenValue,
							"#action"+j
					);
				
				$("#editRule_actionTable").append(trHTML);				
				$('#editRule_givenValue').val('');
				$('#editRule_givenValue_Panel').val('');

				
			}else{
				
				if(valueTag==''){					
					msgAlert(i18n['label_rule_actionCanNotBeNull'],'info');					
				}else{
					msgAlert(i18n['label_rule_actionExist'],'info');
				}
			}

		},
		/**
		 * @description 编辑多个规则
		 */
		editMultitermRule:function(){
			$('#rightop option').attr("selected", true);
			var strId = $('#rightop').val();
			var strText="";
			$('#rightop').each(function(){
				$(this).children("option").each(function(){
					strText+=$(this).text()+",";
					
				})
			});
			
			var lan=strText.length;
				strText=strText.substring(0,lan-1);
			
			$('#addRule_propertyValueid').val(strId);
			$('#addRule_propertyValue').val(strText);
			$('#editRule_propertyValue').val(strText);
			$('#multitermSLA').dialog('close');
		},
		/**
		 * @description 初始化
		 */
		init:function(){
			var flagName=$("#edit_Proce_ruleFlagName").val();
			if(flagName=="saveRequest"){
				wstuo.rules.ruleCM.loadType("requestFit");
			}else if(flagName=="requestProce"){
				wstuo.rules.ruleCM.loadType("requestProce");
			}else if(flagName=="saveChange"){
				wstuo.rules.ruleCM.loadType("changeApproval");
			}else if(flagName=="changeProce"){
				wstuo.rules.ruleCM.loadType("changeProce");
			}else{
				wstuo.rules.ruleCM.loadType("mailToRequest");
			}
			
			$("#editRule_loading").hide();
			$("#editRule_content").show();
			wstuo.rules.editRule.editRule_ruleTermSet();
			wstuo.rules.editRule.editRule_executeAction();
			
			$('#editRule_team').change(function(){
				wstuo.rules.editRule.editRule_setTerm();
			});
			
			$('#editRule_addRuleToList').click(wstuo.rules.editRule.editRule_addRulesToList);

			$('#editRule_executeAction').change(wstuo.rules.editRule.setEXHTML_edit);
			
			if(_flag=="requestFit"||_flag=="requestProce"){			
				$('#editRule_givenValue_Panel').click(wstuo.rules.editRule.editRule_selectServiceOrg);
			}
			
			$('#editRule_addActionsToList').click(wstuo.rules.editRule.addToActionList);			
			$('#editRule_saveRule').click(wstuo.rules.editRule.editRule_merge);
			
			$('#editRule_backToRuleList').click(function(){
				
				basics.index.initContent('../pages/wstuo/rules/ruleMain.jsp?rulePackageNo='+_rulePackageNo+'&flag='+_flag);
				
			});


			$('#multiterm_saveRuleBtn').click(wstuo.rules.editRule.editMultitermRule);
			var valid=$('#edit_rule_salience').val();
			for(var i=0;i<=99;i++){
				
				if(i==valid)
					$('#editRuleSalience').append("<option value='"+i+"' selected='selected'>"+i+"</option>");
				else
					$('#editRuleSalience').append("<option value='"+i+"'>"+i+"</option>");
			}
		}
	};
	
}();
$(document).ready(wstuo.rules.editRule.init);
