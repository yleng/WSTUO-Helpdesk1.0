$package('wstuo.user')

/**  
* @author QXY  
* @constructor 修改用户密码主函数.
* @description 修改用户密码主函数.
* @since version 1.0 
*/
wstuo.user.userPwdUpdate=function(){
	
	return {
		/**
		 * 修改密码
		 */
		userPwdUpdate:function(){
			var pass=$('#newPassword').val();
			var parent=/^[A-Za-z]+$/;
	
			var _param = $('#edit_user_password_form').serialize();
			$.post('user!resetPassword.action',_param,function(data){
				if(data){
					$('#oldPassword,#newPassword,#repeatPassword').val('');
			
					msgShow(i18n['editSuccess'],'show');
		
					window.location.href="../pages/logout.jsp"; 
					
				}else{
					msgAlert(i18n['oldPasswordError'],'info');
				}
			});
			
		},
		/**
		 * 修改密码(用户管理)
		 */
		userPwdUpdate_manage:function(){
			var pass=$('#editUserPwd_newPassword').val();
			var parent=/^[A-Za-z]+$/;
			var _param = $('#editUserPwd_win form').serialize();
			$.post('user!resetPassword.action',_param,function(data){
				if(data){
					$('#editUserPwd_win').dialog('close');
					msgShow(i18n['editSuccess'],'show');
				}else{
					msgAlert(i18n['oldPasswordError'],'info');
				}
			});

		},
		/**
		 * 密码重置
		 * @param loginName  登录名
		 */
		userPasswordReset:function(loginName){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['confirmUserPasswordReset'],function(){
				$.post('user!resetPassword.action','userPwdUpdateDTO.loginName='+loginName+'&userPwdUpdateDTO.resetPassword=resetPassword',function(data){
					$('#password,#rpassword').val('Password123');
					msgShow(i18n['editSuccess'],'show');
						$.post('email!sendMailByLoginName.action','loginName='+loginName+'&title='+i18n['passwordReset']+'&content='+loginName+i18n['passwordResetInfo'],function(result){
					});
					$('#password,#rpassword,#itsopUserPassword,#itsopUserPasswordConfirm').val('Password123');
				})
			});
			
		},
		
		Open_PwdUpdate:function(){
			windows('pwdUpdate',{width:350});
		},
		
		/**
		 * 初始化加载
		 */
		init:function(){
			$('#link_eidt_user_password').click(wstuo.user.userPwdUpdate.userPwdUpdate);
		}
	}
}();
$(document).ready(wstuo.user.userPwdUpdate.init);


