﻿$package('wstuo.knowledge');
$import('wstuo.knowledge.knowledgeDetail');
$import('wstuo.knowledge.knowledgeTree');
$import('wstuo.category.serviceDirectoryUtils');
$import('wstuo.includes');
$import('wstuo.knowledge.leftMenu');
$import('wstuo.user.userUtil');
$import('wstuo.customFilter.filterGrid_Operation');
/**  
 * @author wstuo  
 * @constructor knowledge
 * @description 知识库表格操作主函数
 * @since version 1.0 
 */
wstuo.knowledge.knowledgeGrid = function () {
	
	
	//this.flag=false;
	this.passKwflag='';
	//变量名_类型:字段名
	var options = {
			'serviceDirectoryItem.subServiceId_subServiceId':'label_knowledge_relatedService',
			//'serviceLists.eventId':'label_knowledge_relatedService',
			'title_String':'title',
			'creator_loginName':'title_creator',
			'category.eventId_KeventId' : 'category',
			'addTime_Data' :'time',
			'knowledgeStatus_String' :'status'
		};

    return {

        /**
         * @description 动作格式化
         */
        knowledgeGridFormatter:function () {

            return $('#knowledgeGridACT').html();
        },
        /**
         * @description 标题格式化
         */
        knowledgeGridTitleFormatter: function (cell, opt, data) {

            return "<a href=javascript:wstuo.knowledge.knowledgeDetail.showKnowledgeDetail('" + data.kid + "')>" + cell + "</a>";
        },
        
        /**
         * 状态格式化
         */
        statusFormatter:function(cell,event,data){
        	
        	var _status={'1':i18n['knowledge_satus_normal'],'0':i18n['knowledge_satus_approving'],'-1':i18n['title_refused']};
        	
        	return _status[cell];
        },
        
        /**
         * @description 加载列表
         */
        showKnowledgeGrid: function () {

        	var _url="knowledgeInfo!findAllKnowledges.action";
        	
        	if(filterId!=null && filterId!='' && filterId!=-1){
				_url='knowledgeInfo!findKnowledgesPagerByCustomFilter.action?knowledgeQueryDto.filterId='+filterId;
			}
        	
        	var _postData={'knowledgeQueryDto.opt':kw_opt};
        	
        	if(knowledge_fullSearchFlag=="yes"){//全文检索
				var fullSearchQueryString=$('#fullsearch').val();
				_url="knowledgeInfo!fullSearch.action";						
				$.extend(_postData,{'knowledgeQueryDto.alias':fullSearchQueryString});
				
			}
        	if(knowledge_queryType=='dynamicSearch'){//报表可链接数据传递
				_url='knowledgeInfo!findKnowledgesByCrosstabCell.action';
				$.extend(_postData,{'ktd.rowValue':rowValue,'ktd.colValue':colValue,'ktd.rowKey':rowKey,'ktd.colKey':colKey,'ktd.customFilterNo':customFilterNo});
			}
        	if(sec_updateKnowledgeItems=="0"&&kw_opt=="myapf"){
        		$('#knowledgeGridEdit').show();
        	}
            var params = $.extend({},jqGridParamsTen, {
                url:_url,
                postData:_postData,
               /* caption: i18n['title_request_knowledgeGrid'],*/
                colNames: [i18n['number'],i18n['label_knowledge_relatedService'],i18n['title'],i18n['title_creator'], i18n['knowledge_knowledgeCategory'], i18n['title_createTime'],i18n['common_updateTime'],i18n['status'],'', i18n['operateItems'],''],
                colModel: [
                           {name: 'kid',width: 50,hidden: true}, 
                           {name:'knowledgeServiceName',width:120,align:'center',sortable: false},
                
                {
                    name: 'title',
                    width: 170,
                    formatter: wstuo.knowledge.knowledgeGrid.knowledgeGridTitleFormatter
                }, 
                
                {
                    name: 'creatorFullName',
                    width: 80,
                    align: 'center',
                    sortable: false
                },
                
                {
                    name: 'categoryName',
                    width: 70,
                    align: 'center',
                    index:'category'
                }, {
                    name: 'addTime',
                    width: 120,
                    align: 'center',
                    formatter: timeFormatter
                },{
                    name: 'lastUpdateTime',
                    width: 120,
                    align: 'center',
                    formatter: timeFormatter
                },{
                    name: 'knowledgeStatus',
                    width: 60,
                    align: 'center',
                    sortable: false,
                    formatter:wstuo.knowledge.knowledgeGrid.statusFormatter
                },
                {
                    name: 'keyWords',
                    hidden: true
                }, {
                    name: 'act',
                    width: 200,
                    align: 'center',
                    sortable: false,
                    formatter:function(cell,event,data){
                  	   return $('#knowledgeGridACT').html().replace(/{kid}/g,data.kid);
                     }
                },
                {
                    name: 'statusDesc',
                    hidden: true
                   
                }
                
                ],
                jsonReader: $.extend(jqGridJsonReader, {
                    id: "kid"
                }),
                sortname: 'kid',
                height:'100%',
                shrinkToFit:true,
                ondblClickRow:function(rowId){wstuo.knowledge.knowledgeDetail.showKnowledgeDetail(rowId)},
                pager: '#knowledgeGridPager'
            });
            $("#knowledgeGrid").jqGrid(params);
            $("#knowledgeGrid").navGrid('#knowledgeGridPager', navGridParams);
            //列表操作项
            $("#t_knowledgeGrid").css(jqGridTopStyles);
            $("#t_knowledgeGrid").append($('#knowledgeGridToolbar').html());
            //自适应宽度
            $("#t_knowledgeGrid").css("overflow","auto");
            setGridWidth("#knowledgeGrid",9);
        },

        /**
         * @description 转到新增知识页面
         */
        showAddKnowledge: function () {
        	//basics.tab.tabUtils.closeTab(i18n["issue_add_templet_title"]);
			//basics.tab.tabUtils.closeTab(i18n["issue_edit_templet_title"]);
        	//basics.tab.tabUtils.refreshTab(i18n['title_request_newKnowledge'],'../pages/wstuo/knowledge/addKnowledge.jsp');
        	basics.index.initContent('wstuo/knowledge/addKnowledge.jsp');
        },

        /**
         * @description 转到知识详细页面
         */
        showKnowledge:function(){
        	 checkBeforeEditGrid("#knowledgeGrid", function (rowData) {
        		 wstuo.knowledge.knowledgeDetail.showKnowledgeDetail(rowData.kid);
             });
        },
        
        /**
         * @description 转到知识详细页面
         * @param kid 知识id
         */
        showKnowledge_aff:function(kid){
        	wstuo.knowledge.knowledgeDetail.showKnowledgeDetail(kid);
        },
        
        /**
         * @description 转到编辑知识页面
         */
        showEditKnowledge: function () {

            checkBeforeEditGrid("#knowledgeGrid", function (rowData) {
            	basics.index.initContent('wstuo/knowledge/editKnowledge.jsp?id=' + rowData.kid);
            	//basics.tab.tabUtils.refreshTab(i18n['knowledge_editKnowledge'], "../pages/wstuo/knowledge/editKnowledge.jsp?id=" + rowData.kid);

            });
        },
        /**
         * @description 根据知识ID转到编辑知识页面
         */
        showEndUserEditKnowledge: function () {
            checkBeforeEditGrid("#knowledgeGrid", function (rowData) {
            	basics.index.initContent('wstuo/knowledge/editKnowledge.jsp?id='+rowData.kid+'&opt=myapf');
            	//basics.tab.tabUtils.refreshTab(i18n['knowledge_editKnowledge'], "../pages/wstuo/knowledge/editKnowledge.jsp?id=" + rowData.kid+"&opt=myapf");

            });
        },
        
        /**
         * @description 根据知识ID转到编辑知识页面
         * @param kid 知识id
         */
        showEditKnowledge_aff: function (kid) {
        	basics.index.initContent('wstuo/knowledge/editKnowledge.jsp?id=' + kid);
        	//basics.tab.tabUtils.refreshTab(i18n['knowledge_editKnowledge'], "../pages/wstuo/knowledge/editKnowledge.jsp?id=" + kid);
        },
        
        /**
         * @description 删除知识
         */
        deleteKnowledge: function () {
            checkBeforeDeleteGrid("#knowledgeGrid", function (rowIds) {
                var _param = $.param({
                    'nos': rowIds
                }, true);
                
                $.post("knowledgeInfo!removeKnowledgeItems.action", _param, function () {

                    msgShow(i18n['msg_deleteSuccessful'], 'show');
                    wstuo.knowledge.knowledgeGrid.refresh();
                });
            });
        },
        
        /**
         * @description 删除知识
         * @param kid 知识id
         */
        deleteKnowledge_aff: function (kid) {

        	msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
                var _param = $.param({
                    'nos': kid
                }, true);
                $.post("knowledgeInfo!removeKnowledgeItems.action", _param, function () {
                    wstuo.knowledge.knowledgeGrid.refresh();
                    msgShow(i18n['msg_deleteSuccessful'], 'show');
                });
            });
        },
        

        /**
         * @description 行内删除
         */
        deleteKnowledge_inLine: function () {

            checkBeforeDeleteGridInLine("#knowledgeGrid", function (rowId) {

                var _param = $.param({
                    'nos': rowId
                }, true);
                $.post("knowledgeInfo!removeKnowledgeItems.action", _param, function () {
                    msgShow(i18n['msg_deleteSuccessful'], 'show');
                    wstuo.knowledge.knowledgeGrid.refresh();
                    
                }, "json");

            });
        },
        /**
         * @description 打开搜索框
         */
        search_konwledge_openwindow: function () {
        	//itsm.app.autocomplete.autocomplete.bindAutoComplete('#search_knTitle','com.wstuo.itsm.knowledge.entity.KnowledgeInfo','title','title','kid','Long','','','false');//标题
        	windows('knowledgeSearchDiv',{title:i18n['search'],width:400,close:function(){
            	var postData = $("#knowledgeGrid").jqGrid("getGridParam", "postData");
	            $.each(postData,function(k,v){
	            	if(k=='knowledgeQueryDto.knowledgeServiceNo'){
	            		delete postData[k];
	            	}
	            });
	            var sdata = $('#knowledgeSearchDiv form').getForm();
                $.extend(postData, sdata); //将postData中的查询参数覆盖为空值
            	//$('#search_knTitle').autocomplete("destroy");
            }});
        },
        /**
         * @description 关闭搜索框
         */
        search_konwledge_closewindow: function () {

            $('#knowledgeSearchDiv [name="knowledgeQueryDto.category"]').val("");
            $('#knowledgeSearchDiv [name="knowledgeQueryDto.addTime"]').val("");
            $('#knowledgeSearchDiv [name="knowledgeQueryDto.knTitle"]').val("");
            $('#knowledgeSearchDiv').dialog('close');
        },
        /**
         * @description 搜索
         */
        doSearchKnowledge: function () {
        	//if($('#knowledgeSearchDiv form').form('validate')){
    			var knowledgeServiceParam = '';
        		//获取Name为knowledgeQueryDto.knowledgeServiceNo的值
        		/*var $knowledgeServiceNos = $("#search_service_name input[name=knowledgeDto.knowledServiceNo]");//假设name为test
        		for(var i=0;i<$knowledgeServiceNos.length;i++){
        			knowledgeServiceParam =knowledgeServiceParam+'&knowledgeQueryDto.knowledgeServiceNo='+$knowledgeServiceNos.eq(i).val();;
        		}
	            var postData = [];*/
	            var _url = 'knowledgeInfo!findAllKnowledges.action?'+knowledgeServiceParam;
	            var sdata = $('#knowledgeSearchDiv form').getForm();
	            var postData = $("#knowledgeGrid").jqGrid("getGridParam", "postData");
	            postData['fullTextQueryDTO.queryString'] = '';
	            $("#knowledgeCategoryTree a").removeClass("jstree-clicked");
                $.extend(postData, sdata); //将postData中的查询参数覆盖为空值
	            $('#knowledgeGrid').jqGrid('setGridParam', {
	                url: _url,
	                page: 1
	            }).trigger('reloadGrid');
		//}

        },
        /**
         * 搜索关联知识.
         */
        init_usefullKnowledge: function () {
        	
            if (keyWord != "" & kw_opt=="") {
                $('#knowledgeQueryDto_keyWord').val(keyWord);
                var sdata = $('#knowledgeSearchDiv form').getForm();
                var postData = $("#knowledgeGrid").jqGrid("getGridParam", "postData");
                $.extend(postData, sdata); //将postData中的查询参数覆盖为空值		
                var _url = 'knowledgeInfo!findAllKnowledges.action';
                $("#knowledgeGrid").jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
               
            }
        },
        showKw:function(ktype){
        	if(ktype!=null && ktype=='myap' || ktype=='allap'){
        		$('#kw_app_link,#kw_app_btn').show();
        		$('#kw_reapp_btn').hide();
        	}else if(ktype=='myapf' || ktype=='allapf'){
        		$('#kw_app_link,#kw_reapp_btn').show();
        		$('#kw_app_btn').hide();
        	}else{
        		$('#kw_app_link,#kw_app_btn,#kw_reapp_btn').hide();
        	}
        	var url =basics.index.getCur_url();
        	if(url!='wstuo/knowledge/knowledgeMain.jsp'){
        	   basics.index.initContent('wstuo/knowledge/knowledgeMain.jsp?opt='+ktype);
        	}else{
        		var _url='knowledgeInfo!findAllKnowledges.action';
            	var _postData={'knowledgeQueryDto.opt':ktype};
            	$.extend(_postData,{'knowledgeQueryDto.eventId':'','knowledgeQueryDto.category':''});
            	$('#knowledgeGrid').jqGrid('setGridParam',{postData:null});
            	$('#knowledgeGrid').jqGrid('setGridParam',{postData:_postData,page:1,url:_url}).trigger('reloadGrid');
        	}
        		
        	
        },
        /**
         * @description 查找，打开下拉树.
         * 
         */
        showKnowledgeCategory: function(){
        	wstuo.knowledge.knowledgeTree.selectKnowledgeCategory('#knowledge_category_select_window','#knowledge_category_select_tree','#search_knCategory','#search_eventId');
        },
        openSelectFormat:function(){
        	windows('index_export_common_window',{width:400});
        	$('#index_export_commonClick').click(wstuo.knowledge.knowledgeGrid.exportKnowledgeData);
        },
        
        /**
         * 导出知识库.
         */
        exportKnowledgeData: function () {
        	var _postData = $("#knowledgeGrid").jqGrid("getGridParam", "postData"); //列表参数
			$('#export_knowledge_values').html('');//清空参数
			
			$.each(_postData,function(k,v){
				//加入参数
				$("<input type='hidden' name='"+k+"' value='"+v+"'/>").appendTo("#export_knowledge_values");
			});
			var _postUrl = $("#knowledgeGrid").jqGrid("getGridParam", "url"); //列表参数
			
			var _params=$('#export_knowledge_form').serialize();
			var params=(_postUrl.split('?')[1]);
			params=params+"&"+_params;
			$.post('knowledgeInfo!exportKnowledgeByFind.action',params,function(data){
				$('#exportInfoGrid').trigger('reloadGrid');
				basics.index.initContent('wstuo/includes/includes_exportManage.jsp');
			});
			
        },


        /**
         * 打开导入数据窗口.
         */
        importKnowledgeData_openWindow: function () {
        	 if(language=="en_US"){
					$('#index_import_href').attr('href',"../importFile/en/knowledge.zip");
				}else{
					$('#index_import_href').attr('href',"../importFile/knowledge.zip");
				}
			    // wstuo.role.role.importRoleData
				windows('index_import_excel_window',{width:400});
				$('#index_import_confirm').unbind().click(wstuo.knowledge.knowledgeGrid.importKnowledgeData);
           // windows('importKnowledgeDataWindow',{width:400});
        },

        /**
         * 导入数据.
         */
        importKnowledgeData: function () {
        	if($("#importKnowledgeCsvFile").val()==""){
        		msgAlert(i18n['msg_dc_fileNull'],'info');
        	}else{
        		startProcess();
    			$.ajaxFileUpload({
    	            url:'knowledgeInfo!importKnowledgeFromExcel.action',
    	            secureuri:false,
    	            fileElementId:'importKnowledgeCsvFile', 
    	            dataType:'json',
    	            success: function(data){
    	            	if(data=="ERROR_CSV_FILE_NOT_EXISTS"){
    						msgAlert(i18n['msg_dc_fileNotExists'],'info');
    					}else if(data=="ERROR_CSV_FILE_IO"){
    						msgAlert(i18n['msg_dc_importFailure'],'info');
    					}else{
    						$('#importKnowledgeCsvWindow').dialog('colse');
    						msgAlert(data.replace('Total',i18n['opertionTotal']).replace('Insert',i18n['newAdd']).replace('Update',i18n['update']).replace('Failure',i18n['failure']),'show');
    						$('#knowledgeGrid').trigger('reloadGrid');
    					}
    	            	endProcess();
    	            }
    	        });
        	}
        	
        },

    	/**
    	 * 获取事件
    	 */
    	fitKnowledgeGrids:function(){
    		
    		/*$('#knowledgeCategoryDiv').panel({
    			onCollapse:function(){
    				setGridWidth('#knowledgeGrid', 'regCenter', 50);
    				basics.bottomMain.resizeWithKnowledgeChart();
    			},
    			onExpand:function(){
    				setGridWidth('#knowledgeGrid', 'regCenter', 223);
    				basics.bottomMain.resizeWithKnowledgeChart();
    			}
    		});*/
    	},
    	
    	/**
    	 * 加载知识分类树结构.
    	 */
    	showKnowledgeTreeView:function(){
    	
	        //加载树结构
	       	 wstuo.knowledge.knowledgeTree.showKnowledgeTree_view('#knowledgeCategoryTree',function(e,data){
	       		wstuo.knowledge.knowledgeTree.showKnowledgeServiceTree_selectNode(data,'#knowledgeCategoryTree',function(obj){
		    		var _name=obj.attr('cname');
		    		var _kid=obj.attr('id');
		   			if(_name == i18n['knowledge_knowledgeCategorys']){
		   				_name="";
		   			}
		   			else{
		   				
		   				var _postData={
		   						'knowledgeQueryDto.eventId':_kid,
		   						'knowledgeQueryDto.category':_name,
		   						'knowledgeQueryDto.opt':'',
		   						'knowledgeQueryDto.title':'',
		   						'knowledgeQueryDto.endTime':'',
		   						'knowledgeQueryDto.keyWord':'',
		   						'knowledgeQueryDto.startTime':'',
		   						'knowledgeQueryDto.attachmentContent':''
		   				};
		   				
		   				var _url = 'knowledgeInfo!findAllKnowledges.action';
		   				$('#knowledgeGrid').jqGrid('setGridParam',{url:_url,page:1,postData:_postData}).trigger('reloadGrid');
		   			}
	       		});
	   			
	           });
    		
    	},
    	
    	 /**
    	 * 通过过滤器查询知识库数据
    	 * @param  filterId 过滤器编号
    	 */
		getDataByFilterSearch:function(filterId){
    		
    		var _url = 'knowledgeInfo!findKnowledgesPagerByCustomFilter.action?knowledgeQueryDto.filterId='+filterId;
			
			if(filterId==0||filterId==-1){//空数据
				var _url='knowledgeInfo!findAllKnowledges.action';	
			}
			
			$('#knowledgeGrid').jqGrid('setGridParam',{page:1,url:_url,postData:{}}).trigger('reloadGrid');

		},
		
		/**
    	 * 打开过滤器页面
    	 */
		openCustomFilterWin:function(){
			wstuo.customFilter.filterGrid_Operation.openCustomFilterWin(options,"knowledge","com.wstuo.itsm.knowledge.entity.KnowledgeInfo","knowledge_userToSearch");
		},
		/**
		 * 重新提交审核
		 * @param  typeflag  typeflag类型标识
		 */
		appKw_reopenwindow:function(typeflag){
		    $('#knowledge_reopenApp_window form')[0].reset();
			passKwflag=typeflag;
			checkBeforeMethod("#knowledgeGrid", function (rowIds) {
				windows('knowledge_reopenApp_window',{width: 350});
			});
		},
		/**
		 * 审核知识.
		 * @param  typeflag  typeflag类型标识
		 */
		appKw_openwindow:function(typeflag){
		    $("#showAppInfo").show();
		    $('#knowledge_app_window form')[0].reset();
			passKwflag=typeflag;
			
			if(passKwflag=='showKnowledge'){
				windows('knowledge_app_window',{width: 350});
			}else{
				checkBeforeMethod("#knowledgeGrid", function (rowIds) {
					windows('knowledge_app_window',{width: 350});
		        });
			}
		},
		/**
		 * 审核知识.
		 */
		appKw_save:function(){
			var knowledgeIds='';
			
			if(passKwflag=='showKnowledge'){
				knowledgeIds=$('#showKnowledge_kid').val();
			}else{
				checkBeforeMethod("#knowledgeGrid", function (rowIds) {
					knowledgeIds=rowIds;
		         });
			}
			var _param = $.param({'knowledgeDto.kids': knowledgeIds}, true);

            var _form = $('#knowledge_app_window form').serialize();
            if(knowledgeIds=='' || knowledgeIds==null){
            	return false;
            }
            startProcess();
            $.post("knowledgeInfo!passKW.action", _param+'&'+_form, function () {

            	$('#knowledge_app_window').dialog('close');
            	 msgShow(i18n['common_operation_success'], 'show');
            	 wstuo.knowledge.knowledgeGrid.refresh();
            	 if(passKwflag=='showKnowledge'){
					wstuo.knowledge.knowledgeDetail.showKnowledgeDetail(knowledgeIds);
				}
            	endProcess();

            });
		},
		/**
		 * 重新提交审核知识.
		 */
		reopenappKw_save:function(){
			var knowledgeIds='';
			checkBeforeMethod("#knowledgeGrid", function (rowIds) {
				knowledgeIds=rowIds;
	        });
			var _param = $.param({'knowledgeDto.kids': knowledgeIds}, true);

            var _form = $('#knowledge_reopenApp_window form').serialize();
            
            $.post("knowledgeInfo!passKW.action", _param+'&'+_form, function () {
            	$('#knowledge_reopenApp_window').dialog('close');
            	msgShow(i18n['common_operation_success'], 'show');
            	wstuo.knowledge.knowledgeGrid.refresh();
            });
		},
		/**
		 * 显示结果
		 */
		showAppResult:function(){
			var rowData=$('#knowledgeGrid').getRowData($('#knowledgeGrid').getGridParam('selrow'));
			var _code=rowData.statusDesc;
			if(_code==null && _code==''){
				_code=i18n['noData'];
			}
			_code = _code.replace(/\r\n/ig,"<br />"); 
			$('#kw_app_detail').html(_code);
			windows('knowledge_show_app_window',{width: 350});
		},
		/**
		 * 显示结果
		 * @param kid 知识编号
		 */
		showAppResult_aff:function(kid){
			 var _params = $.param({'kid':kid}, true);
			$.post('knowledgeInfo!appResult.action',_params,function(data){
				var _code=data.statusDesc;
				if(_code==''){
					_code=i18n['noData'];
				}
				_code = _code.replace(/\r\n/ig,"<br />"); 
				_code=_code.replace("null",""); 
				$('#kw_app_detail').html(_code);
				windows('knowledge_show_app_window',{width: 350});
			});
		},
		/**
		 * 刷新知识库
		 */
		refresh : function () {
		    $('#knowledgeGrid').trigger('reloadGrid');
            wstuo.knowledge.leftMenu.showHotKnowledges();
            wstuo.knowledge.leftMenu.showNewKnowledges();
            wstuo.knowledge.leftMenu.countKnowledge();
		},
		/**
		 * 初始化加载
		 * @private
		 */
        init: function () {
        	wstuo.includes.loadCategoryIncludesFile();
        	wstuo.includes.loadExportInfoIncludesFile();
        	wstuo.includes.loadCustomFilterIncludesFile();
			if(type=="sustainable"){
				$("#sustainableMainDiv").show();
				scategory=i18n['label_sustainable'];
			}else{
				$("#sustainableMainDiv").hide();
				scategory="";
			}
        	
        	wstuo.knowledge.knowledgeGrid.showKnowledgeGrid();
        	
        	//加载右边树菜单
        	wstuo.knowledge.knowledgeGrid.showKnowledgeTreeView();
            
            $('#search_knCategory').click(wstuo.knowledge.knowledgeGrid.showKnowledgeCategory);
            $('#knowledge_search_ok').click(wstuo.knowledge.knowledgeGrid.doSearchKnowledge);
            $('#knowledge_customFilter').click(wstuo.knowledge.knowledgeGrid.openCustomFilterWin);
        	wstuo.customFilter.filterGrid_Operation.loadFilterByModule("#knowledge_userToSearch","knowledge");
          
            setTimeout(function () {
            	
                wstuo.knowledge.knowledgeGrid.init_usefullKnowledge();
                wstuo.knowledge.knowledgeGrid.fitKnowledgeGrids();
            }, 0);
            $('#knowledge_ap_submit').click(wstuo.knowledge.knowledgeGrid.appKw_save);
            $('#knowledge_reopenApp_submit').click(wstuo.knowledge.knowledgeGrid.reopenappKw_save);
            $('#search_creator_select').click(function(){
            	 wstuo.user.userUtil.selectUser('#search_creator','','','fullName',companyNo);
            });
            //知识库列表工具栏中消除滚动条
            $("#t_knowledgeGrid").css("overflow","hidden");
            //列表下方显示
           // basics.bottomMain.resizeWithKnowledgeChart();
            
            setInterval(function () {
            	if($("#ui-datepicker-div").css("display")=="none"){
	            	$("#know_search_startTime").blur();
	            	$("#know_search_endTime").blur();
            	}
            }, 500);
        }
    };
}();

//载入
$(document).ready(wstuo.knowledge.knowledgeGrid.init);