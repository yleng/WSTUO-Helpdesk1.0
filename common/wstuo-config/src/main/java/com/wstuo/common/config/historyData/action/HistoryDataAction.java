package com.wstuo.common.config.historyData.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.historyData.dto.HistoryDataDTO;
import com.wstuo.common.config.historyData.dto.HistoryDataQueryDTO;
import com.wstuo.common.config.historyData.service.IHistoryDataService;
import com.wstuo.common.dto.PageDTO;

/**
 * 历史数据Action
 * @author will
 *
 */
@SuppressWarnings("serial")
public class HistoryDataAction extends ActionSupport{
	@Autowired
	private IHistoryDataService historyDataService;
	private PageDTO pageDTO;
	private HistoryDataDTO dto;
	private HistoryDataQueryDTO qdto=new HistoryDataQueryDTO();
	private int page = 1;
	private int rows = 10;
	private Long [] historyDataNos;
	private String sord;
	private String sidx;
	
	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public Long[] getHistoryDataNos() {
		return historyDataNos;
	}

	public void setHistoryDataNos(Long[] historyDataNos) {
		this.historyDataNos = historyDataNos;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public HistoryDataDTO getDto() {
		return dto;
	}

	public void setDto(HistoryDataDTO dto) {
		this.dto = dto;
	}

	public HistoryDataQueryDTO getQdto() {
		return qdto;
	}

	public void setQdto(HistoryDataQueryDTO qdto) {
		this.qdto = qdto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	
	
	/**
	 * 分页查询历史数据.
	 * @return String
	 */
	public String findHistoryDataPager(){
		
		qdto.setSidx(sidx);
		qdto.setSord(sord);
		
		int start = (page - 1) * rows;
		qdto.setStart(start);
		qdto.setLimit(rows);
        pageDTO = historyDataService.findHistoryDataPager(qdto);
        pageDTO.setPage(page);
        pageDTO.setRows(rows);
        
        return SUCCESS;
	}
	
	/**
	 * 彻底删除数据.
	 */
	public String deleteHistoryData(){
		
		historyDataService.deleteHistoryData(historyDataNos);
		
		return SUCCESS;
	}
	
}
