package com.wstuo.common.config.onlinelog.service;

import com.wstuo.common.config.onlinelog.dto.UserErrLogDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 用户操作异常Service
 * @author WSTUO
 *
 */
public interface IUserErrLogService {
	
	/**
	 * 分页查看用户操作异常记录
	 * @param userErrLogDTO
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findUserErrLogPager(final UserErrLogDTO userErrLogDTO,int start, int limit,String sidx,String sord); 
	/**
	 * 保存用户操作异常记录
	 * @param userErrLogDTO
	 */
	void saveUserErrLog(UserErrLogDTO userErrLogDTO);
	
	/**
	 * 
     * 导出数据方法
	 * @param userErrLogDTO
	 * @param start
	 * @param limit
	 * @param fileName
	 */
    void exportUserErrLog(UserErrLogDTO userErrLogDTO,int start, int limit,String fileName);
    
    /**
	 * 查询错误日志列表
	 * @param page
	 * @param rows
	 * @param path
	 * @param csvFileName
	 * @param startTime
	 * @param endTime
	 * @return PageDTO
	 */
	PageDTO showAllErrLogFiles(int page, int rows, String path, String csvFileName, String startTime, String endTime);
	
	/**
	 * 删除错误日志
	 * @param fileNames
	 * @param path
	 */
	void deleteErrorLog(String[] fileNames,String path);
}
