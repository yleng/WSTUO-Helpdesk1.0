package com.wstuo.common.config.userCustom.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wstuo.common.config.userCustom.entity.RowsWidth;
import com.wstuo.common.dto.BaseDTO;

/**
 * 用户自定义DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class UserCustomDTO extends BaseDTO{
	private Long userCustomId;//ID
	private String loginName;//登录名
	private Long customType;//自定义类型.1:门户模块,2:自定义列
	private String eventType;//事件类型
	private String customResult;//自定义的名称
	private List<DashboardDTO> dashboards;//要显示的面板 
	private List<ViewDTO> viewdtos; //要显示的视图
	private Long[] dashboardIds;
	private String layoutType;//布局类型
	private String viewIdStr;  //面板显示ID排序组合字符串
	private RowsWidth rowsWidth;
	private String viewRowsStr;
	private Map<String,Long> map = new HashMap<String, Long>();
	public String getViewIdStr() {
		return viewIdStr;
	}
	public void setViewIdStr(String viewIdStr) {
		this.viewIdStr = viewIdStr;
	}
	public List<ViewDTO> getViewdtos() {
		return viewdtos;
	}
	public void setViewdtos(List<ViewDTO> viewdtos) {
		this.viewdtos = viewdtos;
	}
	public Long getUserCustomId() {
		return userCustomId;
	}
	public void setUserCustomId(Long userCustomId) {
		this.userCustomId = userCustomId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Long getCustomType() {
		return customType;
	}
	public void setCustomType(Long customType) {
		this.customType = customType;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getCustomResult() {
		return customResult;
	}
	public void setCustomResult(String customResult) {
		this.customResult = customResult;
	}
	public List<DashboardDTO> getDashboards() {
		return dashboards;
	}
	public void setDashboards(List<DashboardDTO> dashboards) {
		this.dashboards = dashboards;
	}
	public Long[] getDashboardIds() {
		return dashboardIds;
	}
	public void setDashboardIds(Long[] dashboardIds) {
		this.dashboardIds = dashboardIds;
	}
	public String getLayoutType() {
		return layoutType;
	}
	public void setLayoutType(String layoutType) {
		this.layoutType = layoutType;
	}
	public RowsWidth getRowsWidth() {
		return rowsWidth;
	}
	public void setRowsWidth(RowsWidth rowsWidth) {
		this.rowsWidth = rowsWidth;
	}
	public Map<String, Long> getMap() {
		map.put("oneRow", 1L);
		map.put("twoRow", 2L);
		map.put("threeRow", 3L);
		map.put("fourRow", 4L);
		return map;
	}
	public void setMap(Map<String, Long> map) {
		this.map = map;
	}
	public String getViewRowsStr() {
		return viewRowsStr;
	}
	public void setViewRowsStr(String viewRowsStr) {
		this.viewRowsStr = viewRowsStr;
	}
	

}
