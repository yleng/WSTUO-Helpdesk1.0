package com.wstuo.common.config.moduleManage.service;

import java.util.List;

import com.wstuo.common.config.moduleManage.dto.ModuleManageDTO;
import com.wstuo.common.config.moduleManage.entity.ModuleManage;
import com.wstuo.common.dto.PageDTO;
/**
 * 模块管理Service层
 * @author Wstuo
 *
 */
public interface IModuleManageService {
	/**
	 * 分页查询模块
	 * 
	 * @param queryDto
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findModuleManagePager(ModuleManageDTO queryDto, String sord,
			String sidx);


	/**
	 * 查询所有模块
	 * 
	 * @return  List<ModuleManage>
	 */
	List<ModuleManage> findAll();

	/**
	 * 保存模块信息
	 * 
	 * @param moduleManage
	 */
	void saveModule(ModuleManageDTO dto);


	/**
	 * 卸载模块
	 * @param dto
	 */
	void moduleDisable(Long ids);
	/**
	 * 修改模块显示顺序
	 * 
	 * @param dto
	 */
	void editModule(ModuleManageDTO dto);
	
}
