package com.wstuo.common.config.onlinelog.dao;

import com.wstuo.common.config.onlinelog.dto.UserErrLogDTO;
import com.wstuo.common.config.onlinelog.entity.UserErrLog;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 用户操作异常DAO接口
 * @author WSTUO
 *
 */
public interface IUserErrLogDAO extends IEntityDAO<UserErrLog> {
	/**
	 * 分页查询用户操作异常
	 * @param userErrLogDTO
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findUserErrLogPager(final UserErrLogDTO userErrLogDTO,int start, int limit,String sidx,String sord);
}
