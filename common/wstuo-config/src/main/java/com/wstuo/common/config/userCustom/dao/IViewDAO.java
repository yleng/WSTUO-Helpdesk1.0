package com.wstuo.common.config.userCustom.dao;

import java.util.List;

import com.wstuo.common.config.userCustom.dto.ViewQueryDTO;
import com.wstuo.common.config.userCustom.entity.View;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;


/**
 *  interFace view
 * @author Mark
 *
 */
public interface IViewDAO extends IEntityDAO<View>{
	
	/**
	 * 分类查询视图
	 * @param queryDTO
	 * @return PageDTO
	 */
	PageDTO findPageView(ViewQueryDTO queryDTO);
	
	/**
	 * 查询List
	 * @param queryDTO
	 * @return PageDTO
	 */
	List<View> findListView(ViewQueryDTO queryDTO);
}
