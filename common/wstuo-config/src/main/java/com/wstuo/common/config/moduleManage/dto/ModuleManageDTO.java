package com.wstuo.common.config.moduleManage.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;
/**
 * 模块管理DTO类
 * @author Wstuo
 *
 */
public class ModuleManageDTO extends BaseDTO{
	private Long moduleId;
	private String moduleName;//模块名称
	private Date createTime;//日期
	private String version;//版本
	private Long showSort;//显示顺序
	private String createName;//作者
	private String title; //标题
	private String description;//描述
	protected Byte dataFlag = 0;
	private int start;
	private int limit;
	private String pid;
	
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public Long getModuleId() {
		return moduleId;
	}
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Long getShowSort() {
		return showSort;
	}
	public void setShowSort(Long showSort) {
		this.showSort = showSort;
	}
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Byte getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	
}
