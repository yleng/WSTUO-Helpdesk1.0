package com.wstuo.common.config.authorization.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;


import com.wstuo.common.entity.BaseEntity;
/**
 * 授权管理实体类
 * @author Will
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity()
@Table(name="T_Authorization")
public class Authorization extends BaseEntity{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long authId;
	@Column(nullable=false)
	private String authName;
	@Column(nullable=false)
	private String authType;
	@Lob
	@Basic(fetch = FetchType.LAZY) 
	@Column(nullable=false)
	private String authPawd;
	
	public Long getAuthId() {
		return authId;
	}
	public void setAuthId(Long authId) {
		this.authId = authId;
	}
	public String getAuthName() {
		return authName;
	}
	public void setAuthName(String authName) {
		this.authName = authName;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	public String getAuthPawd() {
		return authPawd;
	}
	public void setAuthPawd(String authPawd) {
		this.authPawd = authPawd;
	}
	
	
	
	
}
