package com.wstuo.common.config.updatelevel.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.User;

/**
 * UpdateLevel Entity class 
 * @author QXY
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
public class UpdateLevel  extends BaseEntity{

	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long ulId;
	@Column(nullable=false)
	private String ulName;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn( name = "userId" )
	private User approval;
	
	public Long getUlId() {
		return ulId;
	}
	public void setUlId(Long ulId) {
		this.ulId = ulId;
	}
	
	
	public String getUlName() {
		return ulName;
	}

	public void setUlName(String ulName) {
		this.ulName = ulName;
	}

	public User getApproval() {
		return approval;
	}
	
	public void setApproval(User approval) {
		this.approval = approval;
	}

	
	
}
