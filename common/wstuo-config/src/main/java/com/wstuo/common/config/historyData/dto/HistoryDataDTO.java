package com.wstuo.common.config.historyData.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 历史数据DTO
 * @author Will
 *
 */

@SuppressWarnings("serial")
public class HistoryDataDTO extends BaseDTO{
	
	private Date createTime;    //删除时间
	private Long historyNo;     //历史数据编号
	private String entityName;  //实体名称
	private Long entityId;      //实体ID引用 
	private String entityTitle; //实体标题
	private String jsonData;    //实体数据的JSON格式保存
	
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getHistoryNo() {
		return historyNo;
	}
	public void setHistoryNo(Long historyNo) {
		this.historyNo = historyNo;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public String getEntityTitle() {
		return entityTitle;
	}
	public void setEntityTitle(String entityTitle) {
		this.entityTitle = entityTitle;
	}
	public String getJsonData() {
		return jsonData;
	}
	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	
	/**
	 * 构造.
	 * @param entityId
	 * @param entityName
	 * @param jsonData
	 */
	public HistoryDataDTO(Long entityId,String entityName,String entityTitle,String jsonData){
		
		this.createTime=new Date();
		this.entityId = entityId;
		this.entityName = entityName;
		this.entityTitle = entityTitle;
		this.jsonData = jsonData;
		
	}
	
	/**
	 * 默认构造.
	 */
	public HistoryDataDTO(){
		
		
		
	}

}
