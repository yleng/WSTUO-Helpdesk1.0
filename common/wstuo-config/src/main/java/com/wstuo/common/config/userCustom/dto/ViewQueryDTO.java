package com.wstuo.common.config.userCustom.dto;
/**
 * View Query DTO
 * @author Will
 *
 */
public class ViewQueryDTO {
	private Long viewId; // 视图ID
	private String viewName; //视图名称
	private String viewType; //视图内容类型
	private String viewContent;  //视图内容
	private Integer proportion;  //视图比重
	private String userName;  //视图所属用户
	private String sord;
	private String sidx;
	private Integer start=0;
	private Integer limit=10;
	private Long customType;//自定义类型.1:门户模块,2:自定义列
	private String viewCode;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getViewId() {
		return viewId;
	}
	public void setViewId(Long viewId) {
		this.viewId = viewId;
	}
	public String getViewName() {
		return viewName;
	}
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public String getViewContent() {
		return viewContent;
	}
	public void setViewContent(String viewContent) {
		this.viewContent = viewContent;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public Long getCustomType() {
		return customType;
	}
	public void setCustomType(Long customType) {
		this.customType = customType;
	}
	public Integer getProportion() {
		return proportion;
	}
	public void setProportion(Integer proportion) {
		this.proportion = proportion;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public String getViewCode() {
		return viewCode;
	}
	public void setViewCode(String viewCode) {
		this.viewCode = viewCode;
	}
	
}
