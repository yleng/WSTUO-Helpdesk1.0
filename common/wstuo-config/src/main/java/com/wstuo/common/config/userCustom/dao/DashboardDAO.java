package com.wstuo.common.config.userCustom.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.userCustom.dto.DashboardQueryDTO;
import com.wstuo.common.config.userCustom.entity.Dashboard;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

/**
 * 面板DAO类
 * @author WSTUO
 *
 */
public class DashboardDAO extends BaseDAOImplHibernate<Dashboard> implements IDashboardDAO {
	/**
	 * 查询全部面板
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDashboardAll(){
		final DetachedCriteria dc = DetachedCriteria.forClass( Dashboard.class );
		dc.addOrder(Order.asc("sortNo"));
		return getHibernateTemplate().findByCriteria(dc);
	};
	
	/**
	 * 分页查询
	 * @param queryDTO
	 *@return PageDTO
	 */
	public PageDTO findPageDashboard(DashboardQueryDTO queryDTO){
		final DetachedCriteria dc = DetachedCriteria.forClass(Dashboard.class);
		Integer start = 0;
		Integer limit = 10;
		if(queryDTO!=null){
			start = queryDTO.getStart();
			limit = queryDTO.getLimit();
			if(StringUtils.hasText(queryDTO.getDashboardName())){
				dc.add(Restrictions.like("dashboardName", queryDTO.getDashboardName(),MatchMode.ANYWHERE));
			}
			if(StringUtils.hasText(queryDTO.getDashboardDataLoadUrl())){
				dc.add(Restrictions.like("dashboardDataLoadUrl", queryDTO.getDashboardDataLoadUrl(),MatchMode.ANYWHERE));
			}
			if(StringUtils.hasText(queryDTO.getDashboardDivId())){
				dc.add(Restrictions.like("dashboardDivId", queryDTO.getDashboardDivId(),MatchMode.ANYWHERE));
			}
			
			//排序
	        if(StringUtils.hasText(queryDTO.getSord())&&StringUtils.hasText(queryDTO.getSidx())){
	        	if("desc".equals(queryDTO.getSord())){
	        		dc.addOrder(Order.desc(queryDTO.getSidx()));
	        	}else{
	        		dc.addOrder(Order.asc(queryDTO.getSidx()));
	        	}
	        }
		}
		return super.findPageByCriteria(dc,start,limit);
	}
}
