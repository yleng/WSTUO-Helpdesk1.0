package com.wstuo.common.config.basicConfig.dao;

import com.wstuo.common.config.basicConfig.entity.Currency;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 货币设置DAO�?
 * @author QXY
 *
 */
public class CurrencyDAO extends BaseDAOImplHibernate<Currency> implements ICurrencyDAO{
}
