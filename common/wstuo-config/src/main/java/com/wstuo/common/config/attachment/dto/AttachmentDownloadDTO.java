package com.wstuo.common.config.attachment.dto;

import java.io.FileInputStream;

/**
 * 附件下载DTO
 * @author QXY
 *
 */
public class AttachmentDownloadDTO {
	
	private FileInputStream downloadStream;
	private String downloadFileName;
	private String downloadContentType;


	public FileInputStream getDownloadStream() {
		return downloadStream;
	}

	public void setDownloadStream(FileInputStream downloadStream) {
		this.downloadStream = downloadStream;
	}

	public String getDownloadFileName() {
		return downloadFileName;
	}

	public void setDownloadFileName(String downloadFileName) {
		this.downloadFileName = downloadFileName;
	}

	public String getDownloadContentType() {
		return downloadContentType;
	}

	public void setDownloadContentType(String downloadContentType) {
		this.downloadContentType = downloadContentType;
	}
	
	
}
