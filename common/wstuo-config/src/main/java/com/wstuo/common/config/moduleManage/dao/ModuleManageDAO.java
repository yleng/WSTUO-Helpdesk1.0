package com.wstuo.common.config.moduleManage.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.moduleManage.dto.ModuleManageDTO;
import com.wstuo.common.config.moduleManage.entity.ModuleManage;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;
/**
 * 模块管理DAO类
 * @author Wstuo
 *
 */
public class ModuleManageDAO extends BaseDAOImplHibernate<ModuleManage>
		implements IModuleManageDAO {
	/**
	 * 分页安装模块
	 */
	public PageDTO findPager(ModuleManageDTO queryDto, String sord,
			String sidx) {
		DetachedCriteria dc = DetachedCriteria
				.forClass(ModuleManage.class);
		int start = 0;
		int limit = 0;

		if (queryDto != null) {
			start = queryDto.getStart();
			limit = queryDto.getLimit();
			if (StringUtils.hasText(queryDto.getModuleName())) {
				dc.add(Restrictions.like("moduleName",
						queryDto.getModuleName(), MatchMode.ANYWHERE));
			}
			if (StringUtils.hasText(queryDto.getTitle())) {
				dc.add(Restrictions.like("title",
						queryDto.getTitle(), MatchMode.ANYWHERE));
			}
			if (StringUtils.hasText(queryDto.getDescription())) {
				dc.add(Restrictions.like("description",
						queryDto.getDescription(), MatchMode.ANYWHERE));
			}
		}
		// 排序
		dc = DaoUtils.orderBy(sidx, sord, dc);
		return super.findPageByCriteria(dc, start, limit);
	}
	public List<ModuleManage> findAllC() {
		DetachedCriteria dc = DetachedCriteria.forClass(ModuleManage.class);
		dc.add(Restrictions.ne("dataFlag",
				BaseEntity.DELETED.byteValue()));
		return super.getHibernateTemplate().findByCriteria(dc);
	}
	public void saveC(ModuleManage entity) {
		if(!entity.getModuleName().equals("request")){
			String hql="create table cu_"+entity.getModuleName().toLowerCase() +"(" +
					"  eno BIGINT(20) NOT NULL AUTO_INCREMENT ," +
					" companyNo BIGINT(20) NULL ," +
					" createTime DATETIME NULL ," +
					" creator VARCHAR(60) NULL ," +
					" dataFlag TINYINT(4) NULL ," +
					" lastUpdateTime DATETIME NULL ," +
					" description LONGTEXT NULL ," +
					" lastUpdater VARCHAR(60) NULL ," +
					" processKey VARCHAR(255) NULL ," +
					" closeTime DATETIME NULL ," +
					" formId BIGINT(20) NULL ," + //表单id
					" pid VARCHAR(255) NULL ," + 
					" technician_userId BIGINT(20) NULL ," +
					" PRIMARY KEY (eno))";
			getHibernateTemplate().excuteSQL(hql);
		}
		super.save(entity);
	}
}
