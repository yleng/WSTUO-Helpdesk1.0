package com.wstuo.common.config.historyData.service;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.struts2.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.historyData.dao.IHistoryDataDAO;
import com.wstuo.common.config.historyData.dto.HistoryDataDTO;
import com.wstuo.common.config.historyData.dto.HistoryDataQueryDTO;
import com.wstuo.common.config.historyData.entity.HistoryData;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
/**
 * 历史数据Serivce
 * @author Will 
 *
 */
public class HistoryDataService implements IHistoryDataService{
	
	
	@Autowired
	private IHistoryDataDAO historyDataDAO;
	
	/**
	 * 分页查询历史数据.
	 * @param qdto
	 * @return PageDTO
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public PageDTO findHistoryDataPager(HistoryDataQueryDTO qdto){
		
		PageDTO pageDTO=historyDataDAO.findHistoryDataPager(qdto);
		List<HistoryData> entites=(List<HistoryData>)pageDTO.getData();
		List<HistoryDataDTO> dtos=new ArrayList<HistoryDataDTO>(entites.size());

		for(HistoryData entity:entites){
			HistoryDataDTO dto=new HistoryDataDTO();
			dto.setHistoryNo(entity.getHistoryNo());
			dto.setEntityId(entity.getEntityId());
			dto.setEntityName(entity.getEntityName());
			dto.setEntityTitle(entity.getEntityTitle());
			dto.setCreateTime(entity.getCreateTime());
			dtos.add(dto);
		}
		
		pageDTO.setData(dtos);
		return pageDTO;
	}
	
	
	/**
	 * 保存历史数据.
	 * @param dto 历史数据DTO
	 */
	@Transactional
	public void saveHistoryData(HistoryDataDTO dto){
		
		HistoryData entity=new HistoryData();
		HistoryDataDTO.dto2entity(dto, entity);
		historyDataDAO.save(entity);
		dto.setHistoryNo(entity.getHistoryNo());
	}

	/**
	 * 删除历史数据.
	 * @param ids
	 */
	@Transactional
	public void deleteHistoryData(Long [] ids){
		historyDataDAO.deleteByIds(ids);
	}
	

	/**
	 * 根据编号取得JSON数据.
	 * @param entityId 编号
	 * @param entityClass 实体名称
	 * @return Object
	 */
	@Transactional
	public Object restore(Long entityId,String entityClass){
		
		HistoryData entity=historyDataDAO.findById(entityId);
		
		try {
			Object object=JSONUtil.deserialize(entity.getJsonData());
			return JSONObject.toBean(JSONObject.fromObject(object),Class.forName(entityClass));
		} catch (Exception e) {
			throw new ApplicationException("ERROR_CONVERT_JSON_TO_BEAN/n"+e.getMessage(),e);
		}
	}


}
