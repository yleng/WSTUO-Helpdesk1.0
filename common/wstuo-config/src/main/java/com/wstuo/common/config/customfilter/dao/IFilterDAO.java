package com.wstuo.common.config.customfilter.dao;



import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;

import com.wstuo.common.config.customfilter.dto.KeyTransferDTO;
import com.wstuo.common.config.customfilter.entity.CustomFilter;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

public interface IFilterDAO extends IEntityDAO<CustomFilter>{
	/**
     * 获取用户的过滤器
     * @param userName
     * @param filterCategory
     * @return List<CustomFilter>
     * 
     */
    List<CustomFilter> findFiltersByUser(String userName,String filterCategory);

    
	/**
     * 通过过滤器获取数据
     * @param filter
     * @return PageDTO
     * 
     */
    PageDTO  findPageByCustomFilter(CustomFilter filter, int start , int limit,String sidx,String sord);
    
    
    /**
     * 通过类名获取表的字段属性
     * @param className
     * @return Map
     */ 
     Map getClassMetaDataByClassName(String className);
     
     /**
      * 根据过滤器查询数据，并取得关联关系列表.
      * @param filter
      * @param dc
      * @return List<String>
      */
     List<String> setFilterDetachedCriteria(CustomFilter filter,Map<Long,String> selfExpressions,DetachedCriteria dc);
     
     
     
     /**
      * 通过过滤器查询实体数据
      * @param filter
      * @return PageDTO
      * 
      */
    PageDTO findPageByCustomFilter(CustomFilter filter,Long [] companyNos,Long[] categorys, int start , int limit,String sidx,String sord);
    
    /**
     * 通过过滤器查询实体数据
     * @param filter
     * @return List
     * 
     */
    List findByCustomFilter(CustomFilter filter,Long [] companyNos,Long[] categorys, int start , int limit,String sidx,String sord);
    
    /**
     * 查询我的私有过滤器
     * @param userName
     * @param filterCategory
     * @return List<CustomFilter>
     */
    List<CustomFilter> findMyFilters(String userName,String filterCategory);
	
	PageDTO findPageByCustomFilter(CustomFilter filter,Long [] companyNos,int start,int limit,String sidx,String sord,KeyTransferDTO ktd);
}
