package com.wstuo.common.config.updatelevel.action;


import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.updatelevel.dto.UpdateLevelDTO;
import com.wstuo.common.config.updatelevel.dto.UpdateLevelQueryDTO;
import com.wstuo.common.config.updatelevel.service.IUpdateLevelService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * 升级更新Action类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class UpdateLevelAction extends ActionSupport{
	
	
	private IUpdateLevelService updateLevelService;
	private int page = 1;
	private int rows = 10;
	private PageDTO updateLevels;
	/***
	 * 升级级别IDS
	 */
	private Long [] ulIds;
	private boolean result;
	private String sord;
	private String sidx;
	private UpdateLevelQueryDTO updateLevelQueryDTO=new UpdateLevelQueryDTO();
	private UpdateLevelDTO updateLevelDTO=new UpdateLevelDTO();
	  
	  

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public IUpdateLevelService getUpdateLevelService() {
		return updateLevelService;
	}

	public void setUpdateLevelService(IUpdateLevelService updateLevelService) {
		this.updateLevelService = updateLevelService;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public PageDTO getUpdateLevels() {
		return updateLevels;
	}

	public void setUpdateLevels(PageDTO updateLevels) {
		this.updateLevels = updateLevels;
	}

	public UpdateLevelQueryDTO getUpdateLevelQueryDTO() {
		return updateLevelQueryDTO;
	}

	public void setUpdateLevelQueryDTO(UpdateLevelQueryDTO updateLevelQueryDTO) {
		this.updateLevelQueryDTO = updateLevelQueryDTO;
	}

	public UpdateLevelDTO getUpdateLevelDTO() {
		return updateLevelDTO;
	}

	public void setUpdateLevelDTO(UpdateLevelDTO updateLevelDTO) {
		this.updateLevelDTO = updateLevelDTO;
	}

	public Long[] getUlIds() {
		return ulIds;
	}

	public void setUlIds(Long[] ulIds) {
		this.ulIds = ulIds;
	}


	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	/**
	 * 分页
	 * @return updateLevels
	 */
    public String findAllLevels() {

    	updateLevelQueryDTO.setSidx(sidx);
    	updateLevelQueryDTO.setSord(sord);
        int start = (page - 1) * rows;
        updateLevelQueryDTO.setStart(start);
        updateLevelQueryDTO.setLimit(rows);
        updateLevels = updateLevelService.findPagerUpdateLevel(updateLevelQueryDTO);
        updateLevels.setPage(page);
        updateLevels.setRows(rows);

        return SUCCESS;
    }
    
    /**
     * 保存升级级别
     * @return SUCCESS
     */
    public String save(){
    	
    	updateLevelService.saveUpdateLevel(updateLevelDTO);
    	return SUCCESS;
    }
    
    /**
     * 修改升级级别
     * @return String
     */
    public String merge(){

    	updateLevelService.mergeUpdateLevel(updateLevelDTO);
    	return SUCCESS;
    	
    }
    
    /**
     * 删除升级级别
     * delete
     * @return String
     */
    public String delete(){
    	
    	try{
    		result=updateLevelService.deleteUpdateLevel(ulIds);
    	}
    	catch(Exception ex){
    		throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",ex);
    	}
    	return "result";
    }
    
    

}
