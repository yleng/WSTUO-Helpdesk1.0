package com.wstuo.common.config.userCustom.comparator;

import java.util.Comparator;

import com.wstuo.common.config.userCustom.dto.DashboardDTO;

@SuppressWarnings("rawtypes")
public class ComparatorDashboardDTO implements Comparator {
	public int compare(Object arg0, Object arg1) {
		DashboardDTO d1=(DashboardDTO)arg0;
		DashboardDTO d2=(DashboardDTO)arg1;

		  int flag=d1.getSortNo().compareTo(d2.getSortNo());
		  if(flag==0){
			  flag = d1.getDashboardName().compareTo(d2.getDashboardName());
		  }
		  return flag;  
	}
}
