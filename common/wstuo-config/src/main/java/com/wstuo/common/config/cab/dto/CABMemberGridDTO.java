package com.wstuo.common.config.cab.dto;



/**
 * 变更委员会成员列表GRID DTO
 * @author WSTUO
 *
 */
public class CABMemberGridDTO{
	private Long cabMemberId;
	private Long approvalMemberId;
	private String approvalMember; 
	private String approvalMemberFullName;
	private Long delegateMemberId;
	private String delegateMember;
	private String delegateMemberFullName;
	private String Desc;
	public Long getCabMemberId() {
		return cabMemberId;
	}
	public void setCabMemberId(Long cabMemberId) {
		this.cabMemberId = cabMemberId;
	}
	public Long getApprovalMemberId() {
		return approvalMemberId;
	}
	public void setApprovalMemberId(Long approvalMemberId) {
		this.approvalMemberId = approvalMemberId;
	}
	public String getApprovalMember() {
		return approvalMember;
	}
	public void setApprovalMember(String approvalMember) {
		this.approvalMember = approvalMember;
	}
	public Long getDelegateMemberId() {
		return delegateMemberId;
	}
	public void setDelegateMemberId(Long delegateMemberId) {
		this.delegateMemberId = delegateMemberId;
	}
	public String getDelegateMember() {
		return delegateMember;
	}
	public void setDelegateMember(String delegateMember) {
		this.delegateMember = delegateMember;
	}
	public String getDesc() {
		return Desc;
	}
	public void setDesc(String desc) {
		Desc = desc;
	}
	public String getApprovalMemberFullName() {
		return approvalMemberFullName;
	}
	public void setApprovalMemberFullName(String approvalMemberFullName) {
		this.approvalMemberFullName = approvalMemberFullName;
	}
	public String getDelegateMemberFullName() {
		return delegateMemberFullName;
	}
	public void setDelegateMemberFullName(String delegateMemberFullName) {
		this.delegateMemberFullName = delegateMemberFullName;
	}
	
	
}
