package com.wstuo.common.config.authorization.service;

import com.wstuo.common.config.authorization.dto.AuthorizationDTO;
import com.wstuo.common.config.authorization.entity.Authorization;
import com.wstuo.common.dto.PageDTO;


/**
 * 授权管理Service接口类
 * @author liufei
 *
 */

public interface IAuthorizationService {
	/**
	 * 任务分页查询
	 * @param authorizationDTO
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPagerAuthorization(AuthorizationDTO authorizationDTO,
			int start, int limit,String sidx,String sord);
	
	/**
	 * 添加过滤器
	 * @param dto
	 */
	void Save(AuthorizationDTO dto);
	
	/**
	 * 更新过滤器
	 * @param dto
	 */
	void update(AuthorizationDTO dto);
	
	/**
	 * 删除过滤器
	 * @param ids
	 */
	void delete(Long[] ids);
	/**
	 * 更具类型查找
	 * */
	Authorization findByAuthType(String type);
}
