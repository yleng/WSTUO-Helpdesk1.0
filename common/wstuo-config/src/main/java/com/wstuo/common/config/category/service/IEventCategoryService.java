package com.wstuo.common.config.category.service;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.wstuo.common.config.category.dto.CategoryTreeViewDTO;
import com.wstuo.common.config.category.dto.EventCategoryDTO;
import com.wstuo.common.config.category.dto.PullDownTreeViewDTO;
import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.dto.PageDTO;



/**
 * Event category business logic interfaces
 * @version 0.1
 * @author Jet
 *  date 2010-9-10
 */
public interface IEventCategoryService
{
    /**
     * Save node
     * @param ec
     */
	EventCategory saveEventCategory( EventCategoryDTO ec );

    /**
     * 判断分类是否存在
     * @param ec
     * @return
     */
    boolean isCategoryExisted( EventCategoryDTO ec );
    
        /**
      * 判断编辑时分类是否存在
      * @param dto
      * @return
      */
    boolean isCategoryExistdOnEdit(EventCategoryDTO dto);
    /**
     * Check all nodes
     * @param CategoryName
     */
    List<CategoryTreeViewDTO> findEventCategorys( String CategoryName ,String flag ,Long parentEventId ,Long maxViewLevel ,int num,int start);

    
    /**
     * 查询常用分类树
     * @param categoryName
     * @param flag
     * @param parentNo
     * @param maxViewLevel
     * @return CategoryTreeViewDTO
     */
    CategoryTreeViewDTO findEventCategoryTree(String categoryName,String flag,Long parentNo,Long maxViewLevel,String pageFlag ,int num,int start);
    /**
     * 查询分类集合
     * @param categoryName
     * @param flag
     * @param parentEventId
     * @param maxViewLevel
     */
    List<CategoryTreeViewDTO> findEventCategoryTreeSub(String categoryName,String flag,Long parentEventId,Long maxViewLevel,String pageFlag,int num,int start);
    /**
     * find Category
     *
     * @param eventId
     */
    EventCategoryDTO findEventCategoryById(long eventId);
    
    /**
     * Modify node
     * @param ec
     */
    void mergeEventCategory( EventCategoryDTO ec );

    /**
     * According to modify the node state Sesssion
     * @param ec
     */
    void updateEventCategory( EventCategoryDTO ec );

    /**
     * Remove the node, including its child nodes
     * @param treeId
     */
    void removeEventCategory( Long treeId );

    /**
     * Mobile node
     *
     * @param ec
     */
    void changeParent( EventCategoryDTO ec );
    /**
     * copy CateGory
     *
     * @param ec
     * @return EventCategoryDTO
     */
    EventCategoryDTO copyCategory(EventCategoryDTO ec);
    
    
    /**
     * 根据分类名称查询分类信息
     * @param categoryName
     */
    List<EventCategory> findEventCategory(String categoryName);
    /**
     * 导出分类信息
     * @param categoryName
     * @return InputStream
     */
    InputStream exportEventCategory(String categoryName);
    /**
     * 导入分类信息
     * @param importFile
     * @return String
     */
    String importEventCategory(File importFile);
    
    /**
     * 同步知识库分类和资源.
     */
    void syncKnowledgeCategorys();
    
    /**
     * 同步请求分类和资源.
     */
    void syncRequestCategorys();
    
    /**
     * 递归查找子分类.
     * @param eventId
     * @return List<EventCategoryDTO>
     */
     List<EventCategoryDTO> findSubCategorys(Long eventId);
     
     /**
      * 根据权限递归查找子分类.
      * @param eventId 父分类Id
      * @param resType 权限分类
      * @return List<EventCategoryDTO>
      */
     List<EventCategoryDTO> findSubCategorysByResType(Long eventId,String resType);
    /**
     * 分页查询分类信息
     * @param ec
     * @return PageDTO
     */
     PageDTO findByServiceNos(EventCategoryDTO ec);
     /**
      * 根据树节点，查询
      * */
     public List<EventCategoryDTO> findSubCategoryArray(Long[] eventId);
     /**
      * 根据树节点查询
      * */
     public List<EventCategoryDTO> findScheduledCategoryArray(Long[] eventId);
     
     /**
      * 下面所有分类的id组
      * @param eventId
      * @return
      */
     Long[] findSubCategoryIdsByEventId(Long eventId);
     
     /**
      * 根据Path路径查询子分类
      * @param path 分类路径
      * @return 返回当前路径下的所有分类
      */
     List<EventCategory> findSubCategoryByPath(String path);
     
     /**
 	 * 获取下拉树所需数据
 	 * @param categoryRoot
 	 * @return PullDownTreeViewDTO
 	 */
 	PullDownTreeViewDTO getCategoryCombotreeData(String categoryRoot);
 	/**
 	 * 根据编号获取分级路径
 	 * @param eventId
 	 * @return
 	 */
 	EventCategoryDTO findLocationNameById(Long eventId);
}


