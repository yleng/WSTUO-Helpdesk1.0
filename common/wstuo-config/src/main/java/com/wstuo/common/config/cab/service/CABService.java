package com.wstuo.common.config.cab.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.cab.dao.ICABDAO;
import com.wstuo.common.config.cab.dto.CABDTO;
import com.wstuo.common.config.cab.dto.CABGridDTO;
import com.wstuo.common.config.cab.dto.CABMemberGridDTO;
import com.wstuo.common.config.cab.dto.CABQueryDTO;
import com.wstuo.common.config.cab.entity.CAB;
import com.wstuo.common.config.cab.entity.CABMember;
import com.wstuo.common.dto.PageDTO;


/**
 * CAB Service 类
 * @author WSTUO
 *
 */
public class CABService implements ICABService {

	@Autowired
	private ICABDAO cabDAO;
	/**
	 * CAB 分页查询
	 * @param cabQueryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	@Transactional
	public PageDTO findPagerCAB(CABQueryDTO cabQueryDto, int start, int limit) {
		
		PageDTO page = cabDAO.findPagerCAB(cabQueryDto, start, limit);

        List<CAB> entities = (List<CAB>) page.getData(  );
        List<CABGridDTO> dtos = new ArrayList<CABGridDTO>( entities.size(  ) );
        for ( CAB entity : entities )
        {
        	CABGridDTO dto = new CABGridDTO(  );
        	dto.setCabId(entity.getCabId());
        	dto.setCabName(entity.getCabName());
        	dto.setCabDesc(entity.getCabDesc());
            dtos.add( dto );
        }
        page.setData( dtos );
        return page;
	}
	/**
	 * 保存CAB
	 * @param cabDto
	 */
	@Transactional
	public void saveCAB(CABDTO cabDto) {
		CAB cab=new CAB();
		cab.setCabName(cabDto.getCabName());
		cab.setCabDesc(cabDto.getCabDesc());
		cabDAO.save(cab);
		cabDto.setCabId(cab.getCabId());
	}
	
	/**
	 * 编辑CAB
	 * @param cabDto
	 */
	@Transactional
	public void editCAB(CABDTO cabDto) {
		CAB cab=cabDAO.findById(cabDto.getCabId());
		cab.setCabName(cabDto.getCabName());
		cab.setCabDesc(cabDto.getCabDesc());
		cabDAO.merge(cab);
	}

	/**
	 * 删除CAB
	 * @param cabId
	 * @param cabIds
	 */
	@Transactional
	public void deleteCAB(Long cabId,Long[] cabIds) {
		cabDAO.deleteByIds(cabIds);
		
	}
	
	/**
	 * 根据CABID获取成员列表
	 * @param cabId
	 * @param page
	 * @param limit
	 * @return PageDTO
	 */
	@Transactional
	public PageDTO findPagerCABMember(Long cabId,int page,int limit){
		PageDTO pages=new PageDTO();
		if(cabId!=null){
			CAB cab=cabDAO.findById(cabId);
			List<CABMember> cabMembers=(List<CABMember>) cab.getCabMember();
			List<CABMemberGridDTO> cabMemGrid = null;
			Integer cabMembersSize=0;
			if(cabMembers!=null && cabMembers.size()>0){
				cabMembersSize = cabMembers.size();
				cabMemGrid=new ArrayList<CABMemberGridDTO>(cabMembersSize);
				int totalSize = cabMembers.size();
	            int start = ( page - 1 ) * limit;
	            int end = ( limit * page ) - 1;
	            if ( end >= totalSize ){
	                end = totalSize - 1;
	            }

				for(int i=start;i<=end;i++){
					CABMemberGridDTO cabMemberGridDto=new CABMemberGridDTO();
					cabMemberGridDto.setCabMemberId(cabMembers.get(i).getCabMemberId());
					cabMemberGridDto.setDesc(cabMembers.get(i).getDescription());
					if(cabMembers.get(i).getApprovalMember()!=null){
						cabMemberGridDto.setApprovalMember(cabMembers.get(i).getApprovalMember().getLoginName());
						cabMemberGridDto.setApprovalMemberId(cabMembers.get(i).getApprovalMember().getUserId());
						cabMemberGridDto.setApprovalMemberFullName(cabMembers.get(i).getApprovalMember().getFullName());
					}
					if(cabMembers.get(i).getDelegateMember()!=null){
						cabMemberGridDto.setDelegateMember(cabMembers.get(i).getDelegateMember().getLoginName());
						cabMemberGridDto.setDelegateMemberId(cabMembers.get(i).getDelegateMember().getUserId());
						cabMemberGridDto.setDelegateMemberFullName(cabMembers.get(i).getDelegateMember().getFullName());
					}
					
					cabMemGrid.add(cabMemberGridDto);
				}
				
			}
			pages.setData(cabMemGrid);
			pages.setTotalSize(cabMembersSize);
		}
		
        return pages;
	}
	

}
