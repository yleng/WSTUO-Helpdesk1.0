package com.wstuo.common.config.userCustom.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.userCustom.entity.RowsWidth;
import com.wstuo.common.config.userCustom.service.IRowsWidthService;

/**
 * Rows Width Action
 * @author Candy
 *
 */
@SuppressWarnings("serial")
public class RowsWidthAction extends ActionSupport {

	@Autowired
	private IRowsWidthService rowsWidthService;
	private Boolean result;
	private RowsWidth rowsWidth;
	private Long[] ids;
	
	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public RowsWidth getRowsWidth() {
		return rowsWidth;
	}

	public void setRowsWidth(RowsWidth rowsWidth) {
		this.rowsWidth = rowsWidth;
	}

	
	/**
	 * 修改视图宽度
	 *  @return String
	 */
	public String updateRowsWidth(){
		result=rowsWidthService.updateRowsWidth(rowsWidth);
		return "result";
	}
	
	/**
	 * 根据用户名查询
	 *  @return String
	 */
	
	public String findRowsWidthByLoginName(){
		return "rowsWidth";
	}
}
