package com.wstuo.common.config.dictionary.dao;

import java.util.List;

import com.wstuo.common.config.dictionary.dto.DataDictionaryGroupQueryDTO;
import com.wstuo.common.config.dictionary.entity.DataDictionaryGroup;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 数据字典分组DAO类接口
 * @author QXY
 */
public interface IDataDictionaryGroupDAO
    extends IEntityDAO<DataDictionaryGroup>{
    
	/**
	 * 分页查询字典分组.
	 * @param rqdto 查询DTO
	 * @return PageDTO 分页数据
	 */
    PageDTO findpager( final DataDictionaryGroupQueryDTO rqdto );

    /**
     * 查询数据字典分组.
     * @return List<DataDictionaryGroup> 数据字典集合.
     */
    List<DataDictionaryGroup> findDataDictionaryGroups(  );
}
