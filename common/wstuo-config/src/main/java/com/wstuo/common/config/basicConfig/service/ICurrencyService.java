package com.wstuo.common.config.basicConfig.service;

import com.wstuo.common.config.basicConfig.dto.CurrencyDTO;

/**
 * 货币Service接口类
 * @author will
 *
 */
public interface ICurrencyService {

	/**
	 * 获取默认货币单位
	 * @return CurrencyDTO
	 */
	CurrencyDTO findDefaultCurrency();
	/**
	 * 添加修改货币信息
	 * @param dto
	 */
	String saveOrUpdateCurrency(CurrencyDTO dto);
	
	
}