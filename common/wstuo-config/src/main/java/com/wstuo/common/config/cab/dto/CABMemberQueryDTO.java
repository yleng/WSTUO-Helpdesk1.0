package com.wstuo.common.config.cab.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * 变更委员会成员查询DTO
 * @author WSTUO
 *
 */
public class CABMemberQueryDTO extends BaseDTO {
	private Long approvalMemberId;
	private String approvalMember; 
	private Long delegateMemberId;
	private String delegateMember;
	private String Desc;
	
    private String sidx;
    private String sord;
    
    
	
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public Long getApprovalMemberId() {
		return approvalMemberId;
	}
	public void setApprovalMemberId(Long approvalMemberId) {
		this.approvalMemberId = approvalMemberId;
	}
	public String getApprovalMember() {
		return approvalMember;
	}
	public void setApprovalMember(String approvalMember) {
		this.approvalMember = approvalMember;
	}
	public Long getDelegateMemberId() {
		return delegateMemberId;
	}
	public void setDelegateMemberId(Long delegateMemberId) {
		this.delegateMemberId = delegateMemberId;
	}
	public String getDelegateMember() {
		return delegateMember;
	}
	public void setDelegateMember(String delegateMember) {
		this.delegateMember = delegateMember;
	}
	public String getDesc() {
		return Desc;
	}
	public void setDesc(String desc) {
		Desc = desc;
	}
	
}
