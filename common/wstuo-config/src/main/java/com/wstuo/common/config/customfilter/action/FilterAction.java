package com.wstuo.common.config.customfilter.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.customfilter.dto.CustomFilterDTO;
import com.wstuo.common.config.customfilter.dto.CustomFilterQueryDTO;
import com.wstuo.common.config.customfilter.service.IExpressionService;
import com.wstuo.common.config.customfilter.service.IFilterService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * 自定义过滤器类
 * 
 * @author Mars
 *  date 2011-08-03
 */
@SuppressWarnings("serial")
public class FilterAction extends ActionSupport {

	@Autowired
	private IFilterService filterService;

	@Autowired
	private IExpressionService expressionService;
	private List<CustomFilterDTO> filters = new ArrayList<CustomFilterDTO>();
	private CustomFilterDTO customFilterDTO = new CustomFilterDTO();
	private CustomFilterQueryDTO queryDTO = new CustomFilterQueryDTO();
	private Long[] ids;
	private String orgs;
	private PageDTO pageDTO;
	private Long filterId;

	public String getOrgs() {
		return orgs;
	}

	public void setOrgs(String orgs) {
		this.orgs = orgs;
	}

	public List<CustomFilterDTO> getFilters() {
		return filters;
	}

	public void setFilters(List<CustomFilterDTO> filters) {
		this.filters = filters;
	}

	public CustomFilterDTO getCustomFilterDTO() {
		return customFilterDTO;
	}

	public void setCustomFilterDTO(CustomFilterDTO customFilterDTO) {
		this.customFilterDTO = customFilterDTO;
	}

	public CustomFilterQueryDTO getQueryDTO() {
		return queryDTO;
	}

	public void setQueryDTO(CustomFilterQueryDTO queryDTO) {
		this.queryDTO = queryDTO;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public Long getFilterId() {
		return filterId;
	}

	public void setFilterId(Long filterId) {
		this.filterId = filterId;
	}

	/**
	 *根据用户登录名查询过滤器
	 * 
	 */
	public String findCustomFilter() {
		filters = filterService.findFiltersByUserName(queryDTO);
		return "CustomFilters";
	}

	/**
	 * 查询我的私有过滤器
	 */
	public String findMyFilters() {
		filters = filterService.findMyFilters(queryDTO);
		return "CustomFilters";
	}

	/**
	 * 根据过滤器编号查询
	 */
	public String findFilterById() {
		customFilterDTO = filterService.findById(queryDTO.getFilterId());
		return "Filter";
	}

	/**
	 * 添加过滤器
	 * 
	 */
	public String save() {
		// 将表达式保存
		expressionService.batchSave(customFilterDTO.getExpressions());
		// 保存过滤器
		filterService.save(customFilterDTO, orgs);
		return SUCCESS;
	}

	/**
	 * 更新过滤器
	 * 
	 */
	public String update() {
		filterService.update(customFilterDTO, orgs);
		return SUCCESS;
	}

	/**
	 * 删除过滤器
	 * 
	 */
	public String delete() {
		try {
			filterService.delete(ids);
		}catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE","ERROR_DATA_CAN_NOT_DELETE");
		}
		return SUCCESS;
	}

	/**
	 * 根据过滤器编号查找分享的组.
	 * 
	 * @return String
	 */
	public String findShareGroups() {
		pageDTO = filterService.findShareGroups(filterId);
		return "pageDTO";
	}
}
