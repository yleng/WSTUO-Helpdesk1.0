package com.wstuo.common.config.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.server.dao.IServerUrlDAO;
import com.wstuo.common.config.server.dto.ServerUrlDTO;
import com.wstuo.common.config.server.entity.ServerUrl;

/**
 * ServerUrl Service class
 * @author Administrator
 *
 */
public class ServerUrlService implements IServerUrlService {
	
	@Autowired
	private IServerUrlDAO serverUrlDAO;

	/**
	 * 保存或编辑服务器路径
	 */
	@Transactional
	public void saveOrUpdateServiceUrl(ServerUrlDTO dto) {
		ServerUrl su = null;
		if(dto.getId()!=null&&dto.getId()!=0){
			su = serverUrlDAO.findById(dto.getId());
		}else{
			su = new ServerUrl();
		}
		ServerUrlDTO.dto2entity(dto, su);
		serverUrlDAO.merge(su);
	}
	
	/**
	 * 查询服务器路径
	 */
	@Transactional
	public ServerUrlDTO findServiceUrl(){
		
		ServerUrlDTO dto = new ServerUrlDTO();
		List<ServerUrl> list = serverUrlDAO.findAll();
		if(list!=null&&list.size()>0){
			ServerUrlDTO.entity2dto(list.get(0), dto);
		}
		return dto;
	}

}
