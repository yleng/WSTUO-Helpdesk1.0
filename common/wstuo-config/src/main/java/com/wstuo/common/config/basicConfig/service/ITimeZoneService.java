package com.wstuo.common.config.basicConfig.service;

import java.util.TimeZone;

import com.wstuo.common.config.basicConfig.dto.TimeZoneDTO;

/**
 * 时区接口类
 * @author Will
 *
 */
public interface ITimeZoneService {
	
	/** 时区GMT */
	public final static String TIME_ZONE_GMT = "GMT";

	/**
	 * 查找时区信息.
	 * @return TimeZoneDTO
	 */
	TimeZoneDTO findTimeZone();

	/**
	 * 添加或保存时区信息
	 * @param dto
	 */
	void saveOrUpdateTomeZone(TimeZoneDTO dto);
	
	/**
	 * 获取中央时区或用户默认时区
	 * @param type
	 * @return TimeZone
	 */
	TimeZone getDefaultTimeZone(String type);
}