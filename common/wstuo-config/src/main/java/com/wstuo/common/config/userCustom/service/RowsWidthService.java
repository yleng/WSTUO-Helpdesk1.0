package com.wstuo.common.config.userCustom.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.userCustom.dao.IRowsWidthDAO;
import com.wstuo.common.config.userCustom.entity.RowsWidth;
/**
 * RowsWidth interface
 * @author Cnady
 *
 */
public class RowsWidthService implements IRowsWidthService {

	@Autowired
	private IRowsWidthDAO rowsWidthDAO;
	
	/**
	 * update and save RowsWidth
	 * @param rowsWidth
	 * @return boolean
	 */
	@Transactional
	public boolean updateRowsWidth(RowsWidth rowsWidth) {
		List<RowsWidth> list=rowsWidthDAO.findBy("loginName", rowsWidth.getLoginName());
		if(list.size()>0){
			RowsWidth rowsW=list.get(0);
			rowsW.setOneRows(rowsWidth.getOneRows());
			rowsW.setTwoRows(rowsWidth.getTwoRows());
			rowsW.setThreeRows(rowsWidth.getThreeRows());
			rowsW.setFourRows(rowsWidth.getFourRows());
			rowsWidthDAO.merge(rowsW);
		}else{
			rowsWidthDAO.save(rowsWidth);
		}
		return true;
	}
	
	
	/**
	 * find RowsWidth By LoginName
	 * @param loginName
	 * @return RowsWidth
	 */
	@Transactional
	public RowsWidth findRowsWidthByLoginName(String loginName) {
		List<RowsWidth> lists= rowsWidthDAO.findBy("loginName",loginName);
		RowsWidth width=new RowsWidth();
		if(lists.size()!=0){
			width.setOneRows(lists.get(0).getOneRows());
			width.setTwoRows(lists.get(0).getTwoRows());
			width.setThreeRows(lists.get(0).getThreeRows());
			width.setFourRows(lists.get(0).getFourRows());
		}else{
			width.setOneRows("49");
			width.setTwoRows("49");
		}
		return width;
	}
	
}
