package com.wstuo.common.sla.service;

import java.io.InputStream;
import java.util.List;

import com.wstuo.common.sla.dto.SLARuleDTO;
import com.wstuo.common.sla.dto.SLARuleQueryDTO;
import com.wstuo.common.sla.entity.SLARule;
import com.wstuo.common.dto.PageDTO;


/**
 * SLA规则物业类接口.
 * @author QXY
 */
public interface ISLARuleService {


	/**
	 * 添加SLA规则
	 * @param slaRule sla规则
	 */
    void saveRuleEntity(SLARule slaRule);
    
    /**
     * 修改SLA规则
     * @param slaRule sla规则
     */
    SLARule mergeRuleEntity(SLARule slaRule);
    
    /**
     * 删除SLA规则.
     * @param no sla编号 
     */
    void removeRule(Long no);

    /**
     * 批量删除SLA规则.
     * @param nos SLA规则编号集合
     */
    void removeRules(Long[] nos);

    /**
     * 根据编号查找SLA规则.
     * @param no SLA规则编号
     * @return SLARule
     */
    SLARule findSLARuleById(Long no);

	/**
	 * 分页查找数据.
	 * @param qdto 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
    PageDTO findPagerByContractNo(SLARuleQueryDTO qdto,String sidx,String sord, int start,int limit);
    /**
     * 查询所有sla规则
     * @param qdto
     */
    List<SLARuleDTO> findAll(SLARuleQueryDTO qdto);
    /**
     * 导出CSV.
     * @param qdto SLAContractQueryDTO
     */
    InputStream exportSLARule(SLARuleQueryDTO qdto);
    
    /**
     * 导入CSV.
     * @param importFilePath 导入文件路径
     * @return String
     */
    String importSLARule(String importFilePath);
    
    /**
     * 导入规则文件.
     * @param drlFilePath 文件路径
     * @param slaNo 
     * @return String
     */
    String importSLARule_drl(String drlFilePath,Long slaNo);
    
    /**
     * 判断SLA名称是否已存在
     * @param slaName
     * @return boolean
     */
    boolean slaNameExist(String slaName);
    /**
     * 根据SLA名称查询SLA
     * @param slaRuleNo 
     * @return SLARuleDTO
     */
    SLARuleDTO findBySlaNo(Long slaRuleNo);
}