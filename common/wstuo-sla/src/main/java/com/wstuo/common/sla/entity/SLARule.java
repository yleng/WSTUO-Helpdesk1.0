package com.wstuo.common.sla.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.rules.entity.RuleAction;

/**
 * SLA Rule Entity
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
@Entity
public class SLARule extends Rule {
	@Column(nullable = true)
	private Integer respondTime;
	@Column(nullable = true)
	private Integer finishTime;
	private Boolean includeHoliday = false;
	@Transient
	private Integer rday;
	@Transient
	private Integer rhour;
	@Transient
	private Integer rminute;
	@Transient
	private Integer fday;
	@Transient
	private Integer fhour;
	@Transient
	private Integer fminute;
	@ManyToOne
	@JoinColumn(name = "contractNo")
	private SLAContract slaContract;
	private Double responseRate;
	private Double completeRate;

	public SLARule(){
		super();
	}
	public Boolean getIncludeHoliday() {
		return includeHoliday;
	}

	public void setIncludeHoliday(Boolean includeHoliday) {
		this.includeHoliday = includeHoliday;
	}

	public Double getResponseRate() {
		return responseRate;
	}

	public void setResponseRate(Double responseRate) {
		this.responseRate = responseRate;
	}

	public Double getCompleteRate() {
		return completeRate;
	}

	public void setCompleteRate(Double completeRate) {
		this.completeRate = completeRate;
	}

	public Integer getRday() {
		rday = respondTime / 86400;
		return rday;
	}

	public void setRday(Integer rday) {
		this.rday = rday;
		rday = rday * 86400;
		respondTime = rday;
	}

	public Integer getRhour() {
		rhour = respondTime / 3600;
		while (rhour >= 24) {
			rhour = rhour - 24;
		}
		return rhour;
	}

	public void setRhour(Integer rhour) {
		this.rhour = rhour;
		rhour = rhour * 3600;
		respondTime += rhour;
	}

	public Integer getRminute() {
		rminute = respondTime / 60;
		while (rminute >= 60) {
			rminute = rminute - 60;
		}
		return rminute;
	}

	public void setRminute(Integer rminute) {
		this.rminute = rminute;
		rminute = rminute * 60;
		respondTime += rminute;
	}

	public Integer getFday() {
		fday = finishTime / 86400;
		return fday;
	}

	public void setFday(Integer fday) {
		this.fday = fday;
		fday = fday * 86400;
		finishTime = fday;
	}

	public Integer getFhour() {
		fhour = finishTime / 3600;
		while (fhour >= 24) {
			fhour = fhour - 24;
		}
		return fhour;
	}

	public void setFhour(Integer fhour) {
		this.fhour = fhour;
		fhour = fhour * 3600;
		finishTime += fhour;
	}

	public Integer getFminute() {
		fminute = finishTime / 60;
		while (fminute >= 60) {
			fminute = fminute - 60;
		}
		return fminute;
	}

	public void setFminute(Integer fminute) {
		this.fminute = fminute;
		fminute = fminute * 60;
		finishTime += fminute;
	}

	public SLAContract getSlaContract() {
		return slaContract;
	}

	public void setSlaContract(SLAContract slaContract) {
		this.slaContract = slaContract;
	}

	public Integer getRespondTime() {

		return respondTime;
	}

	public void setRespondTime(Integer respondTime) {

		this.respondTime = respondTime;

		RuleAction act1 = new RuleAction();

		act1.setPropertyName("respondTime");
		act1.setGivenValue(respondTime + "");
		super.getActions().add(act1);
	}

	public Integer getFinishTime() {

		return finishTime;
	}

	public void setFinishTime(Integer finishTime) {

		this.finishTime = finishTime;

		RuleAction act1 = new RuleAction();

		act1.setPropertyName("finishTime");
		act1.setGivenValue(finishTime + "");
		super.getActions().add(act1);
	}
	
	/**
	 * get sla contract no
	 * @return sla contract no
	 */
	public Long getSlaContractNo(){
		return slaContract.getContractNo();
	}
	
	/**
	 * get sla contract name
	 * @return sla contract name
	 */
	public String getSlaContractName(){
		return slaContract.getContractName();
	}
	
	/**
	 * get sla contract service org name
	 * @return sla contract service org name
	 */
	public String getSlaContractServiceOrgName(){
		return slaContract.getServiceOrg().getOrgName();
	}
	
	/**
	 * get sla contract beginTime
	 * @return sla contract beginTime
	 */
	public Date getSlaContractBeginTime(){
		return slaContract.getBeginTime();
	}
	
	/**
	 * get sla contract endTime
	 * @return sla contract endTime
	 */
	public Date getSlaContractEndTime(){
		return slaContract.getEndTime();
	}
}