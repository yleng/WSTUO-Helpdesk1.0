package com.wstuo.common.sla.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.util.StringUtils;

/**
 * SLA Contract DTO
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class SLAContractDTO extends BaseDTO {
	private Long contractNo;
	private String contractName;
	private String versionNumber;
	private Date beginTime;
	private Date endTime;
	private String agreement;
	private Long dcode;
	private String dname;
	private Long serviceOrgNo;
	private String serviceOrgName;
	private Long rulePackageNo;
	private Long[] byServicesNos;
	private Byte dataFlag;
	private Long[] serviceDirNos;
	private Boolean isDefault = false;// 是否默认

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Long[] getServiceDirNos() {
		return serviceDirNos;
	}

	public void setServiceDirNos(Long[] serviceDirNos) {
		this.serviceDirNos = serviceDirNos;
	}

	public Long[] getByServicesNos() {
		return byServicesNos;
	}

	public void setByServicesNos(Long[] byServicesNos) {
		this.byServicesNos = byServicesNos;
	}

	public Long getRulePackageNo() {
		return rulePackageNo;
	}

	public void setRulePackageNo(Long rulePackageNo) {
		this.rulePackageNo = rulePackageNo;
	}

	public Long getContractNo() {

		return contractNo;
	}

	public String getAgreement() {

		return agreement;
	}

	public void setAgreement(String agreement) {

		this.agreement = agreement;
	}

	public void setContractNo(Long contractNo) {

		this.contractNo = contractNo;
	}

	public String getContractName() {

		return contractName;
	}

	public void setContractName(String contractName) {

		this.contractName = contractName;
	}

	public String getVersionNumber() {

		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {

		this.versionNumber = versionNumber;
	}

	public Date getBeginTime() {

		return beginTime;
	}

	public void setBeginTime(Date beginTime) {

		this.beginTime = beginTime;
	}

	public Date getEndTime() {

		return endTime;
	}

	public void setEndTime(Date endTime) {

		this.endTime = endTime;
	}

	public Long getDcode() {

		return dcode;
	}

	public void setDcode(Long dcode) {

		this.dcode = dcode;
	}

	public String getDname() {

		return dname;
	}

	public void setDname(String dname) {

		this.dname = dname;
	}

	public Long getServiceOrgNo() {
		return serviceOrgNo;
	}

	public void setServiceOrgNo(Long serviceOrgNo) {
		this.serviceOrgNo = serviceOrgNo;
	}

	public String getServiceOrgName() {
		return serviceOrgName;
	}

	public void setServiceOrgName(String serviceOrgName) {
		this.serviceOrgName = serviceOrgName;
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public SLAContractDTO() {

	}

	public SLAContractDTO(Long contractNo, String contractName,
			String versionNumber, Date beginTime, Date endTime,
			String agreement, Long dcode, String dname, Long serviceOrgNo,
			String serviceOrgName, Long rulePackageNo, Long[] byServicesNos) {
		super();
		this.contractNo = contractNo;
		this.contractName = contractName;
		this.versionNumber = versionNumber;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.agreement = agreement;
		this.dcode = dcode;
		this.dname = dname;
		this.serviceOrgNo = serviceOrgNo;
		this.serviceOrgName = serviceOrgName;
		this.rulePackageNo = rulePackageNo;
		this.byServicesNos = byServicesNos;
	}
	/**
	 * 根据字符串设置被服务机构编号.
	 */
	public Long[] getByServicesNosFormByServicesNosStr(String byServicesNosStr){
		if(StringUtils.hasText(byServicesNosStr)){
			return splitString(byServicesNosStr);
		}else{
			throw new ApplicationException(
					"ERROR_BYSERVICE_ORG_CAN_NOT_BE_NULL");
		}
	}
	/**
	 * 根据字符串设置服务目录编号.
	 */
	public Long[] getServicesDirNosFormservicesNosStr(String servicesNosStr){
		return splitString(servicesNosStr);
	}

	/**
	 * 转换字符串
	 * @return
	 */
	private Long[] splitString(String str){
		Long[] nos = null;
		if (StringUtils.hasText(str)) {
			if (str.indexOf(",") != -1) {
				String[] arr = str.split(",");
				nos = new Long[arr.length];
				for (int i = 0; i < arr.length; i++) {
					nos[i] = Long.parseLong(arr[i].trim());
				}
			} else {
				// 只有一个
				nos= new Long[1];
				try {
					nos[0] = Long.parseLong(str.trim());
				} catch (NumberFormatException ex) {
					throw new ApplicationException("ERROR_NUMBER_FORMAT/n"
							+ ex.getMessage(), ex);
				}
			}
		}
		return nos;
	}
}