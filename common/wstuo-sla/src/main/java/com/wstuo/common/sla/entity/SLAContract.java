
package com.wstuo.common.sla.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.rules.entity.RulePackage;
import com.wstuo.common.security.entity.Organization;
import com.wstuo.common.security.entity.OrganizationServices;
import com.wstuo.common.entity.BaseEntity;

/**
 * SLA Contract Entity
 * @author QXY
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Inheritance( strategy = InheritanceType.JOINED )
public class SLAContract extends BaseEntity {

	@Id
    private Long contractNo;
	@Column(nullable=false)
    private String contractName;
	@Column(nullable=true)
    private String versionNumber;
    @Temporal(TemporalType.TIMESTAMP)
    private Date beginTime;
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    @Column(nullable=true)
    private String agreement;
    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "contractNo")
    private List<SLARule> rules;
    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "contractNo")
    private List<PromoteRule> promoteRule;
    @OneToOne
    private RulePackage rulePackage;

    @ManyToOne
	private OrganizationServices serviceOrg;//服务机构
    @JoinTable(  
    		name="SLAContract_Organization",
            joinColumns ={@JoinColumn(name = "slaContractNo", referencedColumnName = "contractNo") },   
            inverseJoinColumns = { @JoinColumn(name = "serviceRecipientNo", referencedColumnName = "orgNo")   
    })
    @ManyToMany
	private List<Organization> byServiceOrg;
    @ManyToMany
    private List<EventCategory> serviceDirs;
    private Boolean isDefault = false;//是否默认
    
	public Boolean getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}
	public List<EventCategory> getServiceDirs() {
		return serviceDirs;
	}
	public void setServiceDirs(List<EventCategory> serviceDirs) {
		this.serviceDirs = serviceDirs;
	}
	public Long getContractNo() {
		return contractNo;
	}
	public void setContractNo(Long contractNo) {
		this.contractNo = contractNo;
	}
	public String getContractName() {
		return contractName;
	}
	public void setContractName(String contractName) {
		this.contractName = contractName;
	}
	public String getVersionNumber() {
		return versionNumber;
	}
	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getAgreement() {
		return agreement;
	}
	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}
	public List<SLARule> getRules() {
		return rules;
	}
	public void setRules(List<SLARule> rules) {
		this.rules = rules;
	}
	public RulePackage getRulePackage() {
		return rulePackage;
	}
	public void setRulePackage(RulePackage rulePackage) {
		this.rulePackage = rulePackage;
	}
	public OrganizationServices getServiceOrg() {
		return serviceOrg;
	}
	public void setServiceOrg(OrganizationServices serviceOrg) {
		this.serviceOrg = serviceOrg;
	}
	public List<Organization> getByServiceOrg() {
		return byServiceOrg;
	}
	public void setByServiceOrg(List<Organization> byServiceOrg) {
		this.byServiceOrg = byServiceOrg;
	} 
	public List<PromoteRule> getPromoteRule() {
		return promoteRule;
	}
	public void setPromoteRule(List<PromoteRule> promoteRule) {
		this.promoteRule = promoteRule;
	}
	@Override
	public String toString() {
	    StringBuffer result = new StringBuffer();
	    result.append("SLAContract ")
        .append("[")
        .append("contractNo=").append(contractNo).append(", ")
        .append("contractName=").append(contractName).append(", ")
        .append("versionNumber=").append(versionNumber).append(", ")
        .append("beginTime=").append(beginTime).append(", ")
        .append("endTime=").append(endTime).append(", ")
        .append("agreement=").append(agreement).append(", ")
        .append("rules=").append(rules).append(", ")
        .append("promoteRule=").append(promoteRule).append(", ")
        .append("rulePackage=").append(rulePackage).append(", ")
        .append("serviceOrg=").append(serviceOrg).append(", ")
        .append("byServiceOrg=").append(byServiceOrg)
        .append("]");
		return result.toString();
	}
	
	public Long getRulePackageNo(){
		return rulePackage.getRulePackageNo();
	}
	
}