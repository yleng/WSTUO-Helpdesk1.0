package com.wstuo.common.rules.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * dto of ruleaction
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010.9.28
 * */
@SuppressWarnings("serial")
public class RuleActionDTO extends BaseDTO {
	/**
	 * action no
	 */
	private Long actionNo;
	/**
	 * action name
	 */
	private String actionName;
	/**
	 * property name
	 */
	private String propertyName;
	/**
	 * govem value
	 */
	private String givenValue;
	/**
	 * sequence
	 */
	private Integer sequence = 0;
	/**
	 * and or
	 */
	private String andOr;
	/**
	 * rule no
	 */
	private Long ruleNo;
	/**
	 * rule name
	 */
	private String ruleName;

	public Long getActionNo() {
		return actionNo;
	}

	public void setActionNo(Long actionNo) {
		this.actionNo = actionNo;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getGivenValue() {
		return givenValue;
	}

	public void setGivenValue(String givenValue) {
		this.givenValue = givenValue;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getAndOr() {
		return andOr;
	}

	public void setAndOr(String andOr) {
		this.andOr = andOr;
	}

	public Long getRuleNo() {
		return ruleNo;
	}

	public void setRuleNo(Long ruleNo) {
		this.ruleNo = ruleNo;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public RuleActionDTO() {
		super();
	}

	public RuleActionDTO(Long actionNo, String actionName, String propertyName, String givenValue, Integer sequence, String andOr, Long ruleNo, String ruleName) {
		super();
		this.actionNo = actionNo;
		this.actionName = actionName;
		this.propertyName = propertyName;
		this.givenValue = givenValue;
		this.sequence = sequence;
		this.andOr = andOr;
		this.ruleNo = ruleNo;
		this.ruleName = ruleName;
	}
}
