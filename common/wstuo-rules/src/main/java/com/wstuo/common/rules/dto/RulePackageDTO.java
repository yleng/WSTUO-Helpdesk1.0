package com.wstuo.common.rules.dto;

import org.apache.commons.beanutils.BeanUtils;

import com.wstuo.common.rules.entity.RulePackage;
import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * dto of rulepackage
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010.9.28
 * */
@SuppressWarnings("serial")
public class RulePackageDTO extends BaseDTO {
	/**
	 * package no
	 */
	private Long rulePackageNo;

	/**
	 * package name
	 */
	private String packageName;
	/**
	 * package module
	 */
	private String module;
	/**
	 * imports
	 */
	private String imports;
	private String rulePackageRemarks;// 规则包备注
	/**
	 * flagName
	 */
	private String flagName;
	
	
	public String getFlagName() {
		return flagName;
	}

	public void setFlagName(String flagName) {
		this.flagName = flagName;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getRulePackageRemarks() {
		return rulePackageRemarks;
	}

	public void setRulePackageRemarks(String rulePackageRemarks) {
		this.rulePackageRemarks = rulePackageRemarks;
	}

	public Long getRulePackageNo() {
		return rulePackageNo;
	}

	public void setRulePackageNo(Long rulePackageNo) {
		this.rulePackageNo = rulePackageNo;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getImports() {
		return imports;
	}

	public void setImports(String imports) {
		this.imports = imports;
	}

	public RulePackageDTO() {
		super();
	}

	public RulePackageDTO(Long rulePackageNo, String packageName, String imports) {
		super();
		this.rulePackageNo = rulePackageNo;
		this.packageName = packageName;
		this.imports = imports;
	}
	
	/**
	 * dto2entity Method
	 * 
	 * @param dto
	 * @param entity
	 */
	public static void dto2entity(RulePackageDTO dto, RulePackage entity) {
		try {
			BeanUtils.copyProperties(entity, dto);
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}

	/**
	 * entity2dto Method
	 * 
	 * @param entity
	 * @param dto
	 */
	public static void entity2dto(RulePackage entity, RulePackageDTO dto) {
		try {
			BeanUtils.copyProperties(dto, entity);
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}

}
