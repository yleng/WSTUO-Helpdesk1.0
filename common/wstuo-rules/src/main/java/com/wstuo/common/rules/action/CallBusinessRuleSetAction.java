package com.wstuo.common.rules.action;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.rules.dto.RuleDTO;
import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.rules.entity.RuleAction;
import com.wstuo.common.rules.service.IRuleService;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 请求业务规则设置Action类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CallBusinessRuleSetAction
    extends ActionSupport
{
    public static final Logger LOGGER = Logger.getLogger( CallBusinessRuleSetAction.class );
    private Rule rule = new Rule(  );
    @Autowired
    private IRuleService ruleService;
    private Long ruleNo;
    private RuleAction action;
    private List<RuleDTO> lidto =new ArrayList<RuleDTO>();

    
    public List<RuleDTO> getLidto() {
		return lidto;
	}

	public void setLidto(List<RuleDTO> lidto) {
		this.lidto = lidto;
	}
	//Van
    private RuleDTO ruleDTO = new RuleDTO(  );

    public RuleDTO getRuleDTO(  )
    {
        return ruleDTO;
    }

    public void setRuleDTO( RuleDTO ruleDTO )
    {
        this.ruleDTO = ruleDTO;
    }

    public RuleAction getAction(  )
    {
        return action;
    }

    public void setAction( RuleAction action )
    {
        this.action = action;
    }

    public Long getRuleNo(  )
    {
        return ruleNo;
    }

    public void setRuleNo( Long ruleNo )
    {
        this.ruleNo = ruleNo;
    }

    public Rule getRule(  )
    {
        return rule;
    }

    public void setRule( Rule rule )
    {
        this.rule = rule;
    }

    /**
     * method
     * @return String
     */
    public String findRule(  )
    {
        rule = ruleService.findRuleById( ruleNo );

        return "showrule";
    }

    /**
     * merge
     * @return String
     */
    public String merge(  )
    {
    	ruleService.printDrlFiles(ruleService.mergeRuleEntity(rule));

        return "showrule";
    }

    /**
     * save
     * @return String
     */
    public String save(  ){
        ruleService.printDrlFiles(ruleService.saveRuleEntity( rule ));//打印文件
        
        return "showrule";
    }

    /**
     * save rule.
     * @return String
     */
    public String saveRule(  )
    {
        ruleService.saveRule( ruleDTO );

        return "showrule";
    }

    /**
     * update rule.
     * @return String
     */
    public String updateRule(  )
    {
        ruleService.mergeRule( ruleDTO );

        return "showrule";
    }
    public String findlidto(){
    	lidto=ruleService.findByRuleSelect();
    	return "lidto";
    }
}
