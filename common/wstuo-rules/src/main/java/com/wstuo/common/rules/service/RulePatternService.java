package com.wstuo.common.rules.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.rules.dao.IRuleDAO;
import com.wstuo.common.rules.dao.IRulePatternDAO;
import com.wstuo.common.rules.dto.RulePatternDTO;
import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.rules.entity.RulePattern;

/**
 * the service class of RulePatternService
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010-9-29
 * */
public class RulePatternService implements IRulePatternService {
	@Autowired
	private IRulePatternDAO rulePatternDAO;
	@Autowired
	private IRuleDAO ruleDAO;
	/**
	 * entity2dto Method
	 * 
	 * @param entity
	 *            ,dto
	 */
	private void entity2dto(RulePattern entity, RulePatternDTO dto) {
		if (entity.getRule() != null) {
			Long ruleNo = entity.getRule().getRuleNo();
			String ruleName = entity.getRule().getRuleName();
			dto.setRuleNo(ruleNo);
			dto.setRuleName(ruleName);
		}
	}

	/**
	 * --------------save,remove,update Method--------------
	 * */

	/**
	 * save rulePattern
	 * 
	 * @param dto
	 */
	@Transactional
	public void saveRulePattern(RulePatternDTO dto) {
		RulePattern entity = new RulePattern();
		RulePatternDTO.dto2entity(dto, entity);
		if (null != dto.getRuleNo()) {
			Rule rule = ruleDAO.findById(dto.getRuleNo());
			rule.setCondition(entity);
		}
		rulePatternDAO.save(entity);
		dto.setRulePatternNo(entity.getRulePatternNo());
	}

	/**
	 * remove rulePattern
	 * 
	 * @param no
	 */
	@Transactional
	public void removeRulePattern(Long no) {
		rulePatternDAO.delete(rulePatternDAO.findById(no));
	}

	/**
	 * remove rulePatterns
	 * 
	 * @param nos
	 */
	@Transactional
	public void removeRulePatterns(Long[] nos) {
		rulePatternDAO.deleteByIds(nos);
	}

	/**
	 * merge rulePattern
	 * 
	 * @param dto
	 * @return RulePatternDTO
	 */
	@Transactional
	public RulePatternDTO mergeRulePattern(RulePatternDTO dto) {
		RulePattern entity = new RulePattern();
		RulePatternDTO.dto2entity(dto, entity);
		if (null != dto.getRuleNo()) {
			Rule rule = ruleDAO.findById(dto.getRuleNo());
			rule.setCondition(entity);
		}
		entity = rulePatternDAO.merge(entity);
		entity2dto(entity, dto);
		RulePatternDTO.entity2dto(entity, dto);
		return dto;
	}

	/**
	 * merge rulePatterns
	 * 
	 * @param dtos
	 */
	@Transactional
	public void mergeAllRulePattern(List<RulePatternDTO> dtos) {
		List<RulePattern> entities = new ArrayList<RulePattern>();
		for (RulePatternDTO dto : dtos) {
			RulePattern entity = new RulePattern();
			RulePatternDTO.dto2entity(dto, entity);
			if (null != dto.getRuleNo()) {
				Rule rule = ruleDAO.findById(dto.getRuleNo());
				rule.setCondition(entity);
			}
			entities.add(entity);
		}
		rulePatternDAO.mergeAll(entities);
	}

	/**
	 * find all rulePattern
	 * 
	 * @return List<RulePatternDTO>
	 */
	public List<RulePatternDTO> findRulePatterns() {
		List<RulePattern> entities = rulePatternDAO.findAll();
		List<RulePatternDTO> dtos = new ArrayList<RulePatternDTO>(entities.size());

		for (RulePattern entity : entities) {
			RulePatternDTO dto = new RulePatternDTO();
			entity2dto(entity, dto);
			RulePatternDTO.entity2dto(entity, dto);
			dtos.add(dto);
		}

		return dtos;
	}

	/**
	 * find rulePattern by id
	 * 
	 * @param no
	 * @return RulePatternDTO
	 */
	public RulePatternDTO findRulePatternById(Long no) {
		RulePattern entity = rulePatternDAO.findById(no);
		RulePatternDTO dto = new RulePatternDTO();
		entity2dto(entity, dto);
		RulePatternDTO.entity2dto(entity, dto);
		return dto;
	}

	/**
	 * find rulePattern by ruleNo
	 * 
	 * @param ruleNo
	 * @return RulePatternDTO
	 */
	public RulePatternDTO findRulePatternByRuleNo(Long ruleNo) {
		Rule rule = ruleDAO.findById(ruleNo);
		RulePattern entity = rule.getCondition();
		RulePatternDTO dto = new RulePatternDTO();
		entity2dto(entity, dto);
		RulePatternDTO.entity2dto(entity, dto);
		return dto;
	}
}
