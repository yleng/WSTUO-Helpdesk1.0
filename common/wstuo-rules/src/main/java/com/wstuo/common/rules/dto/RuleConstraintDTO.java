package com.wstuo.common.rules.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * dto of ruleconstraint
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010.9.28
 * */
public class RuleConstraintDTO extends BaseDTO {
	/**
	 * constraint no
	 */
	private Long conNo;

	/**
	 * and,or
	 */
	private String andOr;

	/**
	 * property name
	 */
	private String propertyName;

	/**
	 * property type
	 */
	private Boolean propertyType = false;

	/**
	 * property value
	 */
	private String propertyValue;

	/**
	 * sequence
	 */
	private Integer sequence = 0;

	/**
	 * rule pattern no
	 */
	private Long rulePatternNo;

	public Long getConNo() {
		return conNo;
	}

	public void setConNo(Long conNo) {
		this.conNo = conNo;
	}

	public String getAndOr() {
		return andOr;
	}

	public void setAndOr(String andOr) {
		this.andOr = andOr;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public Boolean getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(Boolean propertyType) {
		this.propertyType = propertyType;
	}

	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Long getRulePatternNo() {
		return rulePatternNo;
	}

	public void setRulePatternNo(Long rulePatternNo) {
		this.rulePatternNo = rulePatternNo;
	}

	public RuleConstraintDTO() {
		super();
	}

	public RuleConstraintDTO(Long conNo, String andOr, String propertyName,
			Boolean propertyType, String propertyValue, Integer sequence,
			Long rulePatternNo) {
		super();
		this.conNo = conNo;
		this.andOr = andOr;
		this.propertyName = propertyName;
		this.propertyType = propertyType;
		this.propertyValue = propertyValue;
		this.sequence = sequence;
		this.rulePatternNo = rulePatternNo;
	}

}
