package com.wstuo.common.priorityMatrix.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 优先级矩阵状态
 * @author WSTUO
 *
 */
@Entity
public class PriorityMatrixStatus {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String priorityMatrixStatus;//状态
	@Column(nullable=false,unique=true)
	private String type;//类型
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPriorityMatrixStatus() {
		return priorityMatrixStatus;
	}
	public void setPriorityMatrixStatus(String priorityMatrixStatus) {
		this.priorityMatrixStatus = priorityMatrixStatus;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
