package com.wstuo.common.rules.dao;

import com.wstuo.common.rules.entity.RuleConstraint;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * dao of rule constraint
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1         2010.9.28
 * */
public class RuleConstraintDAO
    extends BaseDAOImplHibernate<RuleConstraint>
    implements IRuleConstraintDAO
{
}
