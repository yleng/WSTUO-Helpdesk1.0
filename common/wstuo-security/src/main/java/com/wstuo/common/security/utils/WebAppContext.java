package com.wstuo.common.security.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

import com.wstuo.common.security.enums.SessionName;
import com.wstuo.common.security.utils.AppContext;
import com.wstuo.multitenancy.TenantIdResolver;



public class WebAppContext implements AppContext {
	final static Logger LOGGER = Logger.getLogger(WebAppContext.class);
	/**
	 * get attribute
	 * @param attrName
	 * @return
	 */
	public Object getAttribute(String attrName) {
		try {
			HttpServletRequest httpServletRequestl = ServletActionContext.getRequest();
			if (httpServletRequestl != null) {
				HttpSession session = httpServletRequestl.getSession();
				return session.getAttribute(attrName);
			}else{
				LOGGER.error("get attribute error");
				return null;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}
	/**
	 * get scheme
	 * @return String
	 */
	public String getScheme(){
		HttpServletRequest httpServletRequestl = ServletActionContext.getRequest();
		if (httpServletRequestl != null) {
			String scheme = httpServletRequestl.getScheme();
			return scheme;
		}else{
			LOGGER.error("get scheme error");
			return "";
		}
	}
	
	public String getRequestURI(){
		HttpServletRequest httpServletRequestl = ServletActionContext.getRequest();
		if (httpServletRequestl != null) {
			String requestURI = httpServletRequestl.getRequestURI();
			return requestURI;
		}else{
			LOGGER.error("get requestURI error");
			return "";
		}
	}
	/**
	 * get Header
	 * @return String
	 */
	public String getHeader(){
		HttpServletRequest httpServletRequestl = ServletActionContext.getRequest();
		if (httpServletRequestl != null) {
			String header = httpServletRequestl.getHeader("host");
			return header;
		}else{
			LOGGER.error("get header error");
			return "";
		}
	}
	/**
	 * 获取当前登录的全名
	 * @return 当前登录的登录全名
	 */
	public String getCurrentFullName() {
		String fullName = "";
		Object obj = getAttribute("fullName");
		if(obj!=null){
			fullName = (String)obj;
		}
		return fullName;
	}
	
	/**
	 * 获取当前登录的登录账号
	 * @return 当前登录的登录账号
	 */
	public String getCurrentLoginName() {
		String loginName = "";
		SecurityContext sc = SecurityContextHolder.getContext();
		if(sc!=null){
			Authentication auth = sc.getAuthentication();
			if(auth!=null){
				Object obj = auth.getPrincipal();
				if(obj!=null){
					loginName = (String)obj;
				}
			}
			
		}
		if(!StringUtils.hasText(loginName)){
			Object obj = getAttribute("loginUserName");
			if(obj!=null){
				loginName = (String)obj;
			}
		}
		return loginName;
	}

	/**
	 * 获取当前语言版本
	 * @return 当前语言版本
	 */
	public String getCurrentLanguage() {
		String lang = getAttributeByName("lang");
		if(StringUtils.hasText(lang)){
			return lang;
		}else{
			return "zh_CN";
		}
	}


	/**
	 * get attribute value by name
	 * @param attrName
	 * @return attribute value
	 */	
	public String getAttributeByName(String attrName) {
		try {
			Object obj = getAttribute(attrName);
			if(obj!=null){
				return  (String)obj;
			}else{
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}


	/**
	 * set attribute
	 * @param attrName
	 * @param value
	 */
	public void setAttribute(String attrName, Object value) {
		HttpSession session=ServletActionContext.getRequest().getSession();
		session.setAttribute(attrName, value);
	}

	/**
	 * session invalidate
	 */
	public void invalidate() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		session.invalidate();
	}
	
	
	public String getCurrentTenantId() {
		// TODO Auto-generated method stub
		String tenantId = getAttributeByName(SessionName.TENANTID.getName());
		if(StringUtils.hasText(tenantId)){
			return tenantId;
		}else{
			return TenantIdResolver.getInstance().getDefaultTenantId();
		}
	}
	
	public String getCurrentTenantIdNotDefaultValue() {
		// TODO Auto-generated method stub
		return getAttributeByName(SessionName.TENANTID.getName());
	}
	
	
	

}
