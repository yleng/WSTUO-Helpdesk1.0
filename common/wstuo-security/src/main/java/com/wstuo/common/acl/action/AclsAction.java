package com.wstuo.common.acl.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.acl.dto.AclDTO;
import com.wstuo.common.acl.service.IAclsService;

/**
 * ACL Action 类
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class AclsAction extends ActionSupport{
	@Autowired
	private IAclsService aclsService;
	private Long[] identitys;
	private String aclclass;
	private List<AclDTO> aclList;
	private String[] recipients;
	private int[] allowMasks;
	private int[] rejectMasks;

	public Long[] getIdentitys() {
		return identitys;
	}

	public void setIdentitys(Long[] identitys) {
		this.identitys = identitys;
	}

	public String getAclclass() {
		return aclclass;
	}

	public void setAclclass(String aclclass) {
		this.aclclass = aclclass;
	}

	public List<AclDTO> getAclList() {
		return aclList;
	}

	public void setAclList(List<AclDTO> aclList) {
		this.aclList = aclList;
	}

	public String[] getRecipients() {
		return recipients;
	}

	public void setRecipients(String[] recipients) {
		this.recipients = recipients;
	}

	public int[] getAllowMasks() {
		return allowMasks;
	}

	public void setAllowMasks(int[] allowMasks) {
		this.allowMasks = allowMasks;
	}

	public int[] getRejectMasks() {
		return rejectMasks;
	}

	public void setRejectMasks(int[] rejectMasks) {
		this.rejectMasks = rejectMasks;
	}

	/**
	 * 查询资源权限
	 * @return PageDTO
	 */
	public String findAcl(){
		
		return SUCCESS;
	}
	
	/**
	 * 更新授权
	 * @return not return
	 */
	public String updatePermission(){
		
		aclsService.updatePermission(identitys, aclclass, recipients, allowMasks, rejectMasks);
		
		return SUCCESS;
	}
	
	/**
	 * 添加授权
	 * @return not return
	 */
	public String addPermission(){
		aclsService.addPermission(identitys, aclclass, recipients, allowMasks);
		return SUCCESS;
	}
	
	/**
	 * 收回授权
	 * @return not return
	 */
	public String deletePermission(){
		aclsService.deletePermission(identitys, aclclass, recipients, rejectMasks);
		return SUCCESS;
	}
	
	/**
	 * 查询相关标识体的权限
	 * @return return
	 */
	public String findAclByClzIds(){
		aclList = aclsService.findAclByClzIds(identitys, aclclass);
		return "aclList";
	}
	
}
