package com.wstuo.common.bpm.dto;

import java.util.Date;
import java.util.List;

import com.wstuo.common.bpm.dto.FlowActivityDTO;
import com.wstuo.common.bpm.dto.ParticipationDTO;
import com.wstuo.common.dto.BaseDTO;

/**
 * Flow Task DTO Class
 * @author wstuo
 *
 */
@SuppressWarnings("serial")
public class FlowTaskDTO extends BaseDTO {
	private String taskId;//任务ID
	private String taskName;//任务名
	private String groups;//指派组
	private String assignee;//指派人员
	private String description;//描述
	private Date duedate;//过期日期
	private Integer priority;//优先级
	private Integer progress;//进度
	private List<String> outcomes;//出口
	private String formResourceName;//
	private FlowActivityDTO flowActivityDTO;//任务节点下的属性值
	private List<ParticipationDTO> participationDTO;
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getGroups() {
		return groups;
	}
	public void setGroups(String groups) {
		this.groups = groups;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDuedate() {
		return duedate;
	}
	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Integer getProgress() {
		return progress;
	}
	public void setProgress(Integer progress) {
		this.progress = progress;
	}
	public List<String> getOutcomes() {
		return outcomes;
	}
	public void setOutcomes(List<String> outcomes) {
		this.outcomes = outcomes;
	}
	public String getFormResourceName() {
		return formResourceName;
	}
	public void setFormResourceName(String formResourceName) {
		this.formResourceName = formResourceName;
	}
	public FlowActivityDTO getFlowActivityDTO() {
		return flowActivityDTO;
	}
	public void setFlowActivityDTO(FlowActivityDTO flowActivityDTO) {
		this.flowActivityDTO = flowActivityDTO;
	}
	public List<ParticipationDTO> getParticipationDTO() {
		return participationDTO;
	}
	public void setParticipationDTO(List<ParticipationDTO> participationDTO) {
		this.participationDTO = participationDTO;
	}
	
}
