package com.wstuo.common.security.dao;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.RoleQueryDTO;
import com.wstuo.common.security.entity.Role;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import java.util.List;

/**
 * 角色DAO.
 * 
 * @author will
 */
@SuppressWarnings("unchecked")
public class RoleDAO extends BaseDAOImplHibernate<Role> implements IRoleDAO {

	/**
	 * 根据用户编号，查询所有角色。
	 * 
	 * @param userId
	 * @return List<Role>
	 */
	public List<Role> findByUserId(Long userId) {
		final DetachedCriteria dc = DetachedCriteria.forClass(Role.class);

		dc.createAlias("users", "uss");
		dc.add(Restrictions.eq("uss.userId", userId));

		return super.getHibernateTemplate().findByCriteria(dc);
	}

	/**
	 * 分页查找角色.
	 * 
	 * @param rqdto
	 *            查询DTO:RoleQueryDTO
	 * @param sidx
	 * @param sord
	 * @return 分页数据:PageDTO
	 */
	public PageDTO findpager(RoleQueryDTO rqdto, String sidx, String sord) {
		DetachedCriteria dc = DetachedCriteria.forClass(Role.class);
		int start = 0;
		int limit = 0;

		if (rqdto != null) {
			start = rqdto.getStart();
			limit = rqdto.getLimit();

			if (StringUtils.hasText(rqdto.getRoleName())) {
				dc.add(Restrictions.like("roleName", rqdto.getRoleName(), MatchMode.ANYWHERE));
			}

			if (StringUtils.hasText(rqdto.getRoleCode())) {
				dc.add(Restrictions.like("roleCode", rqdto.getRoleCode(), MatchMode.ANYWHERE));
			}

			if (StringUtils.hasText(rqdto.getDescription())) {
				dc.add(Restrictions.like("description", rqdto.getDescription(), MatchMode.ANYWHERE));
			}

			if (StringUtils.hasText(rqdto.getRemark())) {
				dc.add(Restrictions.like("remark", rqdto.getRemark(), MatchMode.ANYWHERE));
			}
		}
		// 排序
		dc = DaoUtils.orderBy(sidx, sord, dc);
		return super.findPageByCriteria(dc, start, limit);
	}

	/**
	 * 根据相关属性查找角色列表.
	 * 
	 * @param propertyName
	 *            属性名称：String propertyName
	 * @param value
	 *            属性值： Object value
	 * @return 角色列表：List<Role>
	 */
	public List<Role> findBy(String propertyName, Object value) {
		Byte dataFlag = 99;
		final DetachedCriteria dc = DetachedCriteria.forClass(Role.class);

		if (StringUtils.hasText(propertyName)) {
			dc.add(Restrictions.eq(propertyName, value));
		}

		dc.add(Restrictions.ne("dataFlag", dataFlag));

		dc.add(Restrictions.ne("roleState", false));

		List<Role> list = dc.getExecutableCriteria(getSession()).list();

		return list;
	}

	/**
	 * 根据资源查找角色.
	 * 
	 * @param resourceCode
	 * @return List<Role>
	 */
	public List<Role> findRolesByResource(String resourceCode) {

		final DetachedCriteria dc = DetachedCriteria.forClass(Role.class);
		dc.createAlias("resources", "rs");
		dc.add(Restrictions.eq("rs.resCode", resourceCode));

		return super.getHibernateTemplate().findByCriteria(dc);

	}
	/**
	 * 根据资料URL，查询所有角色。
	 * 
	 * @param resourceUrl
	 * @return List<Role>
	 */
	public List<Role> findRolesByResourceByResUrl(String resourceUrl) {

		final DetachedCriteria dc = DetachedCriteria.forClass(Role.class);
		dc.createAlias("resources", "rs");
		dc.add(Restrictions.eq("rs.resUrl", resourceUrl));

		return super.getHibernateTemplate().findByCriteria(dc);

	}
	/**
	 * 根据资源查找角色.
	 * @param roleCodes 角色Code
	 */
	public List<Role> findRolesByRoleCodes(String[] roleCodes){
		final DetachedCriteria dc = DetachedCriteria.forClass(Role.class);
		dc.add(Restrictions.in("roleCode", roleCodes));
		return super.getHibernateTemplate().findByCriteria(dc);

	}
}
