package com.wstuo.common.security.service;


import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.HolidayDTO;
import com.wstuo.common.security.dto.HolidayQueryDTO;

/**
 * 节假日业务类接口.
 */
public interface IHolidayService
{
    /**
     * 分页查找节假日
     * @param qdto 分页查询DTO：HolidayQueryDTO
     * @param sidx
     * @param sord
     * @return 分页数据：PageDTO
     */
	PageDTO findHolidayPager( HolidayQueryDTO qdto,String sidx, String sord);

    /**
     * 新增节假日
     * @param holiday 节假日DTO：HolidayDTO
     * @return Long[]
     */
    Long[] addOrEditHoliday( HolidayDTO holiday );


    /**
     * 批量删除节假日
     * @param hids 节假日编号数据：Long[] hids
     */
    void deteleHoliday( Long[] hids );
    
    
    /**
     * 根据ID获取节假日DTO
     * @param hid 节假日编号数据：Long hid
     * @return HolidayDTO
     */
    HolidayDTO findById(Long hid );
}
