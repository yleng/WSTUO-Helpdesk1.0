package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.security.entity.LDAPAuthenticationSetting;

/**
 * LDAP验证DAO接口类
 * @author will
 *
 */
public interface ILDAPAuthenticationSettingDAO extends IEntityDAO<LDAPAuthenticationSetting> {
	
}
