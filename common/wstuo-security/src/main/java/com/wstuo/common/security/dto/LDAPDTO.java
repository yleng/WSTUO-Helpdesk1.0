package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * LDAPParameterDTO
 * @author will
 *
 */
@SuppressWarnings("serial")
public class LDAPDTO extends BaseDTO{
    private long ldapId;
    private String ldapName;
    private String ldapURL;
    private String prot;
    private String adminName;
    private String adminPassword;
    private String searchFilter;
    private String searchBase;
    private String ldapType;
    private String ifImport;//是否已导入
    private String connStatus;//连接状态
    private String ifAllowAutoImport;//是否允许自动导入
    private String loginName;
    private String password;
	private Integer updateSum;//更新记录
    private String optType;//操作类型
    public LDAPDTO(  )
    {
        super(  );
    }

    public long getLdapId(  )
    {
        return ldapId;
    }

    public void setLdapId( long ldapId )
    {
        this.ldapId = ldapId;
    }

    public String getLdapName(  )
    {
        return ldapName;
    }

    public void setLdapName( String ldapName )
    {
        this.ldapName = ldapName;
    }

    public String getLdapURL(  )
    {
        return ldapURL;
    }

    public void setLdapURL( String ldapURL )
    {
        this.ldapURL = ldapURL;
    }

    public String getProt(  )
    {
        return prot;
    }

    public void setProt( String prot )
    {
        this.prot = prot;
    }

    public String getAdminName(  )
    {
        return adminName;
    }

    public void setAdminName( String adminName )
    {
        this.adminName = adminName;
    }

    public String getAdminPassword(  )
    {
        return adminPassword;
    }

    public void setAdminPassword( String adminPassword )
    {
        this.adminPassword = adminPassword;
    }

    public String getSearchFilter(  )
    {
        return searchFilter;
    }

    public void setSearchFilter( String searchFilter )
    {
        this.searchFilter = searchFilter;
    }

    public String getSearchBase(  )
    {
        return searchBase;
    }

    public void setSearchBase( String searchBase )
    {
        this.searchBase = searchBase;
    }

    public String getLdapType(  )
    {
        return ldapType;
    }

    public void setLdapType( String ldapType )
    {
        this.ldapType = ldapType;
    }

	public String getIfImport() {
		return ifImport;
	}

	public void setIfImport(String ifImport) {
		this.ifImport = ifImport;
	}

	public String getConnStatus() {
		return connStatus;
	}

	public void setConnStatus(String connStatus) {
		this.connStatus = connStatus;
	}

	public String getIfAllowAutoImport() {
		return ifAllowAutoImport;
	}

	public void setIfAllowAutoImport(String ifAllowAutoImport) {
		this.ifAllowAutoImport = ifAllowAutoImport;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    public Integer getUpdateSum() {
		return updateSum;
	}

	public void setUpdateSum(Integer updateSum) {
		this.updateSum = updateSum;
	}

	public String getOptType() {
		return optType;
	}

	public void setOptType(String optType) {
		this.optType = optType;
	}
}
