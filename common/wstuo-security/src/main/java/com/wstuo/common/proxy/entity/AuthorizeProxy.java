package com.wstuo.common.proxy.entity;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.wstuo.common.security.entity.User;

/**
 * 代理实体类
 * @author Administrator
 *
 */
@Entity
@Cacheable
public class AuthorizeProxy {

	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long proxyId;//代理Id
	private Date startTime;//开始时间
	private Date endTime;//结束时间
	@ManyToOne
    @JoinColumn( name = "userAgentId" )
    private User userAgent; //代理人
	@ManyToOne
    @JoinColumn( name = "proxieduserId" )
    private User proxieduser; //被代理人
	
	
	public Long getProxyId() {
		return proxyId;
	}
	public void setProxyId(Long proxyId) {
		this.proxyId = proxyId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public User getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(User userAgent) {
		this.userAgent = userAgent;
	}
	public User getProxieduser() {
		return proxieduser;
	}
	public void setProxieduser(User proxieduser) {
		this.proxieduser = proxieduser;
	}
	
}
