package com.wstuo.common.acl.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * ACL OBJECT IDENTITY ENTITY CLASS
 * @author Administrator
 *
 */
//@Entity(name="acl_object_identity")
public class ACL_Object_Identity {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;
	@ManyToOne
	@JoinColumn(name="object_id_class")
	private ACL_Class object_id_class;
	private Long object_id_identity;
	private Long parent_object;
	@ManyToOne
	@JoinColumn(name="owner_sid")
	private ACL_SID owner_sid;
	private Byte entries_inheriting;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ACL_Class getObject_id_class() {
		return object_id_class;
	}
	public void setObject_id_class(ACL_Class object_id_class) {
		this.object_id_class = object_id_class;
	}
	public Long getObject_id_identity() {
		return object_id_identity;
	}
	public void setObject_id_identity(Long object_id_identity) {
		this.object_id_identity = object_id_identity;
	}
	public Long getParent_object() {
		return parent_object;
	}
	public void setParent_object(Long parent_object) {
		this.parent_object = parent_object;
	}
	public ACL_SID getOwner_sid() {
		return owner_sid;
	}
	public void setOwner_sid(ACL_SID owner_sid) {
		this.owner_sid = owner_sid;
	}
	public Byte getEntries_inheriting() {
		return entries_inheriting;
	}
	public void setEntries_inheriting(Byte entries_inheriting) {
		this.entries_inheriting = entries_inheriting;
	}
	
	
}
