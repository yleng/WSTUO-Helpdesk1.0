package com.wstuo.common.bpm.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.bpm.dto.ProcessUseDTO;
import com.wstuo.common.bpm.service.IProcessUseService;

/**
 * 默认流程设置 action class
 * @author wst
 *
 */
@SuppressWarnings("serial")
public class ProcessUseAction extends ActionSupport{
	@Autowired
    private IProcessUseService processUseService;
	private ProcessUseDTO processUseDTO;
	
	public ProcessUseDTO getProcessUseDTO() {
		return processUseDTO;
	}
	public void setProcessUseDTO(ProcessUseDTO processUseDTO) {
		this.processUseDTO = processUseDTO;
	}
	/**
     * 修改流程选用设置
     * @return String
     */
    public String processUseSet(){
    	
    	processUseService.editProcessUse(processUseDTO);
    	
    	return SUCCESS;
    }
    /**
     * 根据ID查询默认流程设置
     * @return String
     */
    public String processUseById(){
    	
    	if(processUseDTO.getProcessUseNo()!=null){
    		processUseDTO=processUseService.findById(processUseDTO.getProcessUseNo());
    	}
    	
    	return SUCCESS;
    }
    /**
     * 根据名称查询默认流程设置
     * @return String
     */
    public String processUseByUseName(){
    	
    	if(processUseDTO.getUseName()!=null){
    		processUseDTO=processUseService.findProcessUseByUseName(processUseDTO.getUseName());
    	}
    	return SUCCESS;
    }
}
