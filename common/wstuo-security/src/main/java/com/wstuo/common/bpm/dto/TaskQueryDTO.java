package com.wstuo.common.bpm.dto;

import java.util.Date;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.wstuo.common.exception.ApplicationException;

/**
 * 任务查询DTO
 * 
 * @author wstuo
 * 
 */
public class TaskQueryDTO {
	private Long id;
	private String remark;
	private String name;
	private String description;
	private String assignee;
	private String formResourceName;
	private Date createTime;
	private Date duedate;
	private Integer progress;
	private int priority;
	private String executionId;
	private String activityName;
	private String[] assignees;
	private String[] groups;
	private Map<String, Object> variables;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<String, Object> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, Object> variables) {
		this.variables = variables;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getFormResourceName() {
		return formResourceName;
	}

	public void setFormResourceName(String formResourceName) {
		this.formResourceName = formResourceName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getDuedate() {
		return duedate;
	}

	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String[] getAssignees() {
		return assignees;
	}

	public void setAssignees(String[] assignees) {
		this.assignees = assignees;
	}

	public String[] getGroups() {
		return groups;
	}

	public void setGroups(String[] groups) {
		this.groups = groups;
	}

	/**
	 * Transform dto to entity.
	 * 
	 * @param dto
	 *            DTO object
	 * @param entity
	 *            domain entity object
	 */
	public static void dto2entity(Object dto, Object entity) {
		try {
			if (dto != null) {
				BeanUtils.copyProperties(entity, dto);
			}
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}

	/**
	 * Transform entity to dto.
	 * 
	 * @param entity
	 *            doamin entity object
	 * @param dto
	 *            dto object
	 */
	public static void entity2dto(Object entity, Object dto) {
		try {
			if (entity != null) {
				BeanUtils.copyProperties(dto, entity);
			}
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}

}
