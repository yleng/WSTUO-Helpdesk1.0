package com.wstuo.common.proxy.dao;



import java.util.Date;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.proxy.dto.ProxyDTO;
import com.wstuo.common.proxy.entity.AuthorizeProxy;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

/**
 * 用户代理DAO类
 * @author Administrator
 *
 */
public class ProxyDAO extends BaseDAOImplHibernate<AuthorizeProxy> implements IProxyDAO{
	
	/**
	 *分页查询方法
	 * @param proxyDTO
	 * @param sidx
	 * @param sord
	 * @param start
	 * @param rows
	 * @return PageDTO
	 */
	public PageDTO findPager(ProxyDTO proxyDTO,String sidx,String sord,int start,int rows){
		DetachedCriteria dc = DetachedCriteria.forClass( AuthorizeProxy.class );
        dc.createAlias( "userAgent", "ua" );
        if(proxyDTO.getUserId()!=null){//代理人
        	dc.add( Restrictions.eq("ua.userId",proxyDTO.getUserId() ) );
		}
        if(proxyDTO.getProxyState()!=null){//代理人状态
        	dc.add( Restrictions.eq("ua.holidayStatus",proxyDTO.getProxyState()));
		}
        if(proxyDTO.getProxieduserId()!=null){//被代理的用户
        	dc.createAlias( "proxieduser", "pu" );
        	dc.add( Restrictions.eq("pu.userId",proxyDTO.getProxieduserId()));
		}
        //排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
        return super.findPageByCriteria( dc, start, rows );
	}
	/**
	 * 查询代理了那些用户
	 * @param loginName
	 * @param technicalloginName
	 * @return List<AuthorizeProxy>
	 */
	@SuppressWarnings("unchecked")
	public List<AuthorizeProxy> findProxyOrpersonal(String loginName,String technicalloginName){
		final DetachedCriteria dc = DetachedCriteria.forClass( AuthorizeProxy.class );
		 dc.createAlias("userAgent","ua");
		 dc.add(Restrictions.eq("ua.loginName",loginName));//代理人是当前登录用户
		 dc.createAlias("proxieduser","pu");
		 dc.add(Restrictions.eq("pu.holidayStatus",true));//被代理的用户在休假中
		 if(StringUtils.hasText(technicalloginName)){
			 dc.add(Restrictions.eq("pu.loginName",technicalloginName));//被代理的用户是这个技术
		 }
		 dc.add(Restrictions.and(Restrictions.le("startTime",new Date()),Restrictions.ge("endTime", new Date())));//当前时间在代理时间段内
		 return super.getHibernateTemplate().findByCriteria(dc);
    }
}
