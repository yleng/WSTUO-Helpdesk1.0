package com.wstuo.common.sso.service;

import com.wstuo.common.sso.dto.SSOUserDTO;


public interface ILoginService {
	 Boolean loginServcie(String account);
	 
	 /**
	 * 获取接口的用户密码
	 * 
	 * @return
	 */
	public String getUserPassword(String userName);
	
	String base64Decode(String in);
	
	SSOUserDTO getBtUserInfoByLogin(String login);
}
