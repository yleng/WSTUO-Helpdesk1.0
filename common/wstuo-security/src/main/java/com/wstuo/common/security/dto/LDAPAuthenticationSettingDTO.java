package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * LDAP验证DTO类
 * @author will
 *
 */
@SuppressWarnings("serial")
public class LDAPAuthenticationSettingDTO extends BaseDTO {
	private Long ldapId;
	private String ldapType;//LDAP服务器类型
    private String ldapServer;//LDAP服务器地址  
    private String prot;//LDAP端口号
    private String adminName;//用户名
    private String adminPassword;//密码
    private String searchFilter;//搜索条件
    private String searchBase;//搜索DN
    private String authenticationMode;//验证方式(userName:用户名验证，userNamePassword:用户名密码验证)
    private Boolean enableAuthentication = false;//LDAP验证是否启动
    private String commonUserName;//公共帐号
    private String commonPassword;//公共帐号的密码
    private String kerberosType;//Kerberos服务器类型
    private String kerberosServer;//kerberos服务器地址  
    private String kerberosName;//kerberos用户名
    private String kerberosPassword;//kerberos密码
    private String kerberosBase;//kerberos域名称
    private Boolean kerberosEnableAuthentication = false;//Kerberos验证是否启动
    private String loginName;
    private String password;
	public Long getLdapId() {
		return ldapId;
	}
	public void setLdapId(Long ldapId) {
		this.ldapId = ldapId;
	}
	public String getLdapType() {
		return ldapType;
	}
	public void setLdapType(String ldapType) {
		this.ldapType = ldapType;
	}
	public String getLdapServer() {
		return ldapServer;
	}
	public void setLdapServer(String ldapServer) {
		this.ldapServer = ldapServer;
	}
	public String getProt() {
		return prot;
	}
	public void setProt(String prot) {
		this.prot = prot;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public String getAdminPassword() {
		return adminPassword;
	}
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
	public String getSearchFilter() {
		return searchFilter;
	}
	public void setSearchFilter(String searchFilter) {
		this.searchFilter = searchFilter;
	}
	public String getSearchBase() {
		return searchBase;
	}
	public void setSearchBase(String searchBase) {
		this.searchBase = searchBase;
	}
	public String getAuthenticationMode() {
		return authenticationMode;
	}
	public void setAuthenticationMode(String authenticationMode) {
		this.authenticationMode = authenticationMode;
	}
	public Boolean getEnableAuthentication() {
		return enableAuthentication;
	}
	public void setEnableAuthentication(Boolean enableAuthentication) {
		this.enableAuthentication = enableAuthentication;
	}
	public String getCommonUserName() {
		return commonUserName;
	}
	public void setCommonUserName(String commonUserName) {
		this.commonUserName = commonUserName;
	}
	public String getCommonPassword() {
		return commonPassword;
	}
	public void setCommonPassword(String commonPassword) {
		this.commonPassword = commonPassword;
	}
	public String getKerberosType() {
		return kerberosType;
	}
	public void setKerberosType(String kerberosType) {
		this.kerberosType = kerberosType;
	}
	public String getKerberosServer() {
		return kerberosServer;
	}
	public void setKerberosServer(String kerberosServer) {
		this.kerberosServer = kerberosServer;
	}
	public String getKerberosName() {
		return kerberosName;
	}
	public void setKerberosName(String kerberosName) {
		this.kerberosName = kerberosName;
	}
	public String getKerberosPassword() {
		return kerberosPassword;
	}
	public void setKerberosPassword(String kerberosPassword) {
		this.kerberosPassword = kerberosPassword;
	}
	public String getKerberosBase() {
		return kerberosBase;
	}
	public void setKerberosBase(String kerberosBase) {
		this.kerberosBase = kerberosBase;
	}
	public Boolean getKerberosEnableAuthentication() {
		return kerberosEnableAuthentication;
	}
	public void setKerberosEnableAuthentication(Boolean kerberosEnableAuthentication) {
		this.kerberosEnableAuthentication = kerberosEnableAuthentication;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * 组装krd5配置文件
	 * @return
	 */
	public String assemblyKrbPapers(){
		return "[libdefaults]"
				+ "\r\ndefault_realm ="
				+ kerberosBase.toUpperCase()
				+ ".COM"
				+ "\r\ndefault_tkt_enctypes = rc4-hmac,des-cbc-md5,des-cbc-crc"
				+ "\r\ndefault_tgs_enctypes = rc4-hmac,des-cbc-md5,des-cbc-crc"
				+ "\r\n[realms]\r\n"
				+ kerberosBase.toUpperCase() + ".COM={"
				+ "\r\nkdc =" + kerberosServer + "\r\n}"
				+ "\r\n[domain_realm]\r\n"
				+ kerberosBase + ".com="
				+ kerberosBase.toUpperCase() + ".COM";
	}
	/**
	 * 组装jaas配置文件
	 * @return
	 */
	public String assemblytestPapers(String Format){
		String PaperString="";
		if("OpenLDAP".equals(ldapType)){
			PaperString="openldap{\r\ncom.wstuo.common.jaas.service.OpenLDAPLoginModule sufficient debug=false;\r\n};";
		}else{
			PaperString="ldap{\r\ncom.sun.security.auth.module.LdapLoginModule required"+
					"\r\nuserProvider=\""+ldapServer+":"+prot+"/"+searchBase+"\""+
					"\r\nauthIdentity=\"{USERNAME}@"+Format+"\""+
					"\r\nuserFilter=\"(&(sAMAccountName={USERNAME})(objectclass=user))\""+
					"\r\nuseSSL=false"+
					"\r\ndebug=false;"+
					"\r\n};"+
					"\r\nkrb5{com.sun.security.auth.module.Krb5LoginModule sufficient useTicketCache=false debug=false refreshKrb5Config=true;\r\n};";
		}
		
		return PaperString;
	}
	/**
	 * 组装LoginConfStr配置文件
	 * @return
	 */
	public String assemblyLDAPVerifyPapers(String Format){
		return "\r\ncom.sun.security.auth.module.LdapLoginModule sufficient"+
		"\r\nuserProvider=\""+ldapServer+":"+prot+"/"+searchBase+"\""+
		"\r\nauthIdentity=\"{USERNAME}@"+Format+"\""+
		"\r\nuserFilter=\"(&(sAMAccountName={USERNAME})(objectclass=user))\""+
		"\r\nuseSSL=false"+
		"\r\ndebug=false;";
	}
	/**
	 * 组装DBLogin配置文件
	 * @return
	 */
	public String assemblyDBLoginPapers(){
		return "WSTUO{\r\ncom.wstuo.common.jaas.service.DBLoginModule sufficient debug=false;";
	}
	/**
	 * 组装LDAPLogin配置文件
	 * @return
	 */
	public String assemblyLDAPLoginPapers(){
		return "\r\ncom.wstuo.common.jaas.service.OpenLDAPLoginModule sufficient debug=false;";
	}
	/**
	 * 组装LDAPLogin配置文件
	 * @return
	 */
	public String assemblySunOneLoginPapers(){
		return "\r\ncom.wstuo.common.jaas.service.SunOneLDAPLoginModule sufficient debug=false;";
	}
	/**
	 * 组装Krb5Login配置文件
	 * @return
	 */
	public String assemblyKrb5LoginPapers(){
		return "\r\ncom.sun.security.auth.module.Krb5LoginModule sufficient useTicketCache=false debug=false refreshKrb5Config=true;";
	}
	/**
	 * 组装LoginConfStr配置文件
	 * @return
	 */
	public String assemblySunOnePapers(){
		return "LDAP_URL="+ldapServer+":"+prot+
		"\r\nMANAGER_DN="+adminName+
		"\r\nMANAGER_PASSWORD="+adminPassword+
		"\r\nBASE_DN="+searchBase;
	}
}
