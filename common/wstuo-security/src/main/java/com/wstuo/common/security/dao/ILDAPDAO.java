package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.LDAPDTO;
import com.wstuo.common.security.dto.LDAPQueryDTO;
import com.wstuo.common.security.dto.UserDTO;
import com.wstuo.common.security.entity.LDAP;

import java.util.List;

/**
 * ILDAPDAO
 * @author will
 *
 */
public interface ILDAPDAO
    extends IEntityDAO<LDAP>
{
    /**
     * MSLDAP
     * @return List<UserDTO>
     */
    List<UserDTO> microsoftLDAP( LDAPDTO parDto );

    /**
     * LinuxLDAP
     * @return List<UserDTO>
     */
    List<UserDTO> linuxLDAP( LDAPDTO parDto );

    /**
     * CSLDAP
     * @return List<UserDTO>
     */
    List<UserDTO> ciscoLDAP( LDAPDTO parDto );

    /**
     * OtherLDAP
     * @return List<UserDTO>
     */
    List<UserDTO> otherLDAP( LDAPDTO parDto );

    /**
     * findPager
     * @param queryDto
     * @return PageDTO
     */
    PageDTO findPager( final LDAPQueryDTO queryDto,String sidx,String sord);

    /**
     * LDAPAuthentication
     * @param usrName
     * @param userPwd
     * @param ldapURl
     * @param port
     * @return boolean
     */
    boolean ldapAuthentication( String usrName, String userPwd, String ldapURl, String port );

    /**
     * LDAPAuthentication
     * @param ldapDtos
     * @param userName
     * @param password
     * @return boolean
     */
    boolean ldapAuthentication( List<LDAPDTO> ldapDtos, String userName, String password );

}
