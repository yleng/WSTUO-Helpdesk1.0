package com.wstuo.common.security.service;

import java.util.Arrays;
import java.util.List;

/**
 *  终端用户比较排序
 */
public class EnduserCompetence {

	public List<String> competences;
	
	public List<String> getCompetences() {
		putCompetence();
		return competences;
	}

	public void setCompetences(List<String> competences) {
		this.competences = competences;
	}
	
	/**
	 *  add Competence
	 */
	private void putCompetence(){
		     competences = Arrays.asList(new String[] {
				"PORTAL_MAIN",                       // 门户面板
				"BASICSET_MAIN",                     //系统设置 
				"SEC_ORG",                           // 机构管理
				"SEC_SECURITY",                      // 访问控制
				//"SYSTEM_TOOL_RES",                           // 系统工具
				"PORTAL_DASHBOARD_RES",                      // 门户面板配置
				"modify_password",                   //修改密码
				"MODIFY_PASSWORD_MODIFY",            //修改密码
				"REQUEST_MAIN",                      //请求
				"REQACTION",                         //请求动作
				"MYREQUEST_RES",                     //"我提出的请求
				"REQUEST_MENU_MYUNTREADREQUEST",     //我描述不全退回的请求
				"/pages/request!updateRequest.action",//编辑请求
				"/pages/request!findRequestPager.action",//查看请求
				"/pages/request!saveRequest.action",  //新增请求
				"SELFHELPRESOLVE_RES",                //自助解决
				"ADD_REQUEST_SOLUTIONS_RES",            //添加解决方案
				"VIEW_REQUEST_HISTORYRECORD_RES",       //"历史记录"
				"itsm_request_requestMain_getDataByFilterSearch",//请求过滤器
				"MYPAGE_MENU_FOUNDREQUEST",                   //"快速创建请求"
				"MYREQUEST_RES",                             //我提出的请求
				"TOOLS_MAIN",								//辅助工具
				"KNOWLEDGEBASE_MAIN" ,                      //知识库
				"knowledge_menu",                           //二级菜单（知识库）
				"KNOWLEGEINFO_VIEWKNOWLEGE"  ,              //"查看知识"
				"KNOWLEDGEINFO_ADDKNOWLEDGE",                //新增知识
				"KNOWLEDGEINFO_MYKNOWLEDGE",                //"我发布的知识"
				"KNOWLEDGEINFO_MYSTAYCHECKKNOWLEDGE",      //我的待审核的知识
				"KNOWLEDGEINFO_MYNONCHECKKNOWLEDGE",       //我未审核通过的知识
				"IM",                                       //消息管理
				"IMSL" ,                                    //"查看消息"
				"IMMANAGE_DELETE" ,                         //删除消息
				"/pages/immanage!save.action"//,              //发送消息"
				//"TOOLS_VIEWAFFICHE"                          //查看公告
		});
		
		
		/*competences = new ArrayList<String>();
		//门户面板
		//competences.add("PORTAL_MAIN");// 门户面板
		//系统设置
		//competences.add("BASICSET_MAIN");//系统设置 
		
		competences.add("SEC_ORG");// 机构管理
		competences.add("SEC_SECURITY");// 访问控制
		competences.add("modify_password"); //修改密码
		competences.add("MODIFY_PASSWORD_MODIFY");
		//请求模块
		competences.add("REQUEST_MAIN");//1
		competences.add("REQACTION");// 请求动作
		competences.add("MYREQUEST_RES");
		competences.add("REQUEST_MENU_MYUNTREADREQUEST");  
		
		competences.add("/pages/request!updateRequest.action");
		competences.add("/pages/request!findRequestPager.action");
		competences.add("/pages/request!saveRequest.action");
		competences.add("SELFHELPRESOLVE_RES");
		competences.add("ADD_REQUEST_SOLUTIONS_RES");//添加解决方案
		competences.add("VIEW_REQUEST_HISTORYRECORD_RES");
		competences.add("itsm_request_requestMain_getDataByFilterSearch");
		
		competences.add("MYPAGE_MENU_FOUNDREQUEST");
		competences.add("MYREQUEST_RES,MYREQUEST_RES");  
		
		competences.add("REQUEST_MENU_MYUNTREADREQUEST");
		
		//辅助工具
		competences.add("TOOLS_MAIN");// 辅助工具
		//知识库模块
		competences.add("KNOWLEDGEBASE_MAIN");// 知识库
		
		competences.add("knowledge_menu");
		competences.add("KNOWLEGEINFO_VIEWKNOWLEGE");
		competences.add("KNOWLEDGEINFO_ADDKNOWLEDGE");
		competences.add("KNOWLEDGEINFO_MYKNOWLEDGE");
		competences.add("KNOWLEDGEINFO_MYSTAYCHECKKNOWLEDGE");
		competences.add("KNOWLEDGEINFO_MYNONCHECKKNOWLEDGE");
		
		//即时信息
		competences.add("IM");//消息管理
		competences.add("IMSL");
		competences.add("IMMANAGE_DELETE");
		competences.add("/pages/immanage!save.action");
		//公告
		competences.add("TOOLS_VIEWAFFICHE");
		
		competences.add("");*/
	}
}
