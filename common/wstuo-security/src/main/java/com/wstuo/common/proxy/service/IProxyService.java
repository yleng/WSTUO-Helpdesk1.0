package com.wstuo.common.proxy.service;


import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.proxy.dto.ProxyDTO;

/**
 * 代理Service接口类
 * @author Administrator
 *
 */
public interface IProxyService {
	
	/**
	 * 代理分页查询
	 * @param proxyDTO
	 * @param sidx
	 * @param sord
	 * @param start
	 * @param rows
	 * @return PageDTO
	 */
	PageDTO findProxyByPage(ProxyDTO proxyDTO,String sidx,String sord,int start,int rows);
	/**
	 * 新增代理人
	 * @param dto
	 */
	void saveProxy(final ProxyDTO dto);
	/**
	 * 编辑代理人
	 * @param dto
	 */
	void editProxy(final ProxyDTO dto);
	/**
	 * 删除代理人
	 * @param ids
	 */
	void deleteProxy(final Long[] ids);
	/**
	 * 查询数据
	 * @param loginName
	 * @return String[]
	 */
	String[] findProxyOrpersonal(String loginName);
	/**
	 * 查询显示状态
	 * @param loginName
	 * @param technicalloginName
	 * @return true or false
	 */
	boolean findProxyOrlogin(String loginName,String technicalloginName);
}