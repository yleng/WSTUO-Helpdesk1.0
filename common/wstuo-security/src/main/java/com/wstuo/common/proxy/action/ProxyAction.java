package com.wstuo.common.proxy.action;


import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.proxy.dto.ProxyDTO;
import com.wstuo.common.proxy.service.IProxyService;
import com.wstuo.common.security.action.SupplierAction;

/**
 * 用户代理Action类
 * @author will
 *
 */
@SuppressWarnings("serial")
public class ProxyAction extends SupplierAction{
	@Autowired
	private IProxyService proxyService;
	private int page = 1;
	private int rows = 10;
	private Long[] ids;
	private String sidx;
	private String sord;
	private PageDTO pageDto=new PageDTO(); 
	private ProxyDTO proxyDTO = new ProxyDTO();
	
	
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public IProxyService getProxyService() {
		return proxyService;
	}
	public void setProxyService(IProxyService proxyService) {
		this.proxyService = proxyService;
	}
	public ProxyDTO getProxyDTO() {
		return proxyDTO;
	}
	public void setProxyDTO(ProxyDTO proxyDTO) {
		this.proxyDTO = proxyDTO;
	}
	public PageDTO getPageDto() {
		return pageDto;
	}
	public void setPageDto(PageDTO pageDto) {
		this.pageDto = pageDto;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}

	/**
	 * 软件分页查询
	 * @return PageDTO
	 */
	public String findProxyByPage(){
		int start = (page - 1) * rows;
		pageDto=proxyService.findProxyByPage(proxyDTO,sidx,sord,start,rows);
		pageDto.setPage(page);
		pageDto.setRows(rows);
		return SUCCESS;
	}
	/**
	 * 软件保存
	 * @return Null PageDTO
	 */
	public String saveProxy(){
		proxyService.saveProxy(proxyDTO);
		return SUCCESS;
	}
	/**
	 * 软件更新
	 * @return Null PageDTO
	 */
	public String editProxy(){
		proxyService.editProxy(proxyDTO);
		return SUCCESS;
	}
	/**
	 * 软件删除
	 * @return Null PageDTO
	 */
	public String deleteProxy(){
		try {
			proxyService.deleteProxy(ids);
		} catch (Exception e) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",e);
		}
		return SUCCESS;
	}
}
