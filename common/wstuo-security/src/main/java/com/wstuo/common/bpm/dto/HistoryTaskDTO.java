package com.wstuo.common.bpm.dto;

import com.wstuo.common.dto.BaseDTO;

import java.util.Date;

/**
 * 历史任务信息DTO
 * @author wstuo
 */
@SuppressWarnings("serial")
public class HistoryTaskDTO extends BaseDTO{
    protected String assignee;
    protected Date createTime;
    protected Long duration; // 持续时间
    protected Date endTime;
    protected String executionId;
    protected String id;
    protected String outcome;
    protected String state;
    protected String activityName;
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getExecutionId() {
		return executionId;
	}
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOutcome() {
		return outcome;
	}
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
  
}
