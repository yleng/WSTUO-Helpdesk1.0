package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * Class SupplierDTO
 */
@SuppressWarnings( "serial" )
public class SupplierDTO
    extends BaseDTO
{
    private Long orgNo;
    private String orgName;
    private String address;
    private String email;
    private String officePhone;
    private String officeFax;
    private Long companyNo;
    private Byte dataFlag;

    public SupplierDTO(  )
    {
        super(  );
    }

    public Long getOrgNo(  )
    {
        return orgNo;
    }

    public void setOrgNo( Long orgNo )
    {
        this.orgNo = orgNo;
    }

    public String getOrgName(  )
    {
        return orgName;
    }

    public void setOrgName( String orgName )
    {
        this.orgName = orgName;
    }

    public String getAddress(  )
    {
        return address;
    }

    public void setAddress( String address )
    {
        this.address = address;
    }

    public String getEmail(  )
    {
        return email;
    }

    public void setEmail( String email )
    {
        this.email = email;
    }

    public String getOfficePhone(  )
    {
        return officePhone;
    }

    public void setOfficePhone( String officePhone )
    {
        this.officePhone = officePhone;
    }

    public String getOfficeFax(  )
    {
        return officeFax;
    }

    public void setOfficeFax( String officeFax )
    {
        this.officeFax = officeFax;
    }

    public Long getCompanyNo(  )
    {
        return companyNo;
    }

    public void setCompanyNo( Long companyNo )
    {
        this.companyNo = companyNo;
    }

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public SupplierDTO(Long orgNo, String orgName, String address,
			String email, String officePhone, String officeFax, Long companyNo) {
		super();
		this.orgNo = orgNo;
		this.orgName = orgName;
		this.address = address;
		this.email = email;
		this.officePhone = officePhone;
		this.officeFax = officeFax;
		this.companyNo = companyNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((companyNo == null) ? 0 : companyNo.hashCode());
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((officeFax == null) ? 0 : officeFax.hashCode());
		result = prime * result
				+ ((officePhone == null) ? 0 : officePhone.hashCode());
		result = prime * result + ((orgName == null) ? 0 : orgName.hashCode());
		result = prime * result + ((orgNo == null) ? 0 : orgNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SupplierDTO other = (SupplierDTO) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (companyNo == null) {
			if (other.companyNo != null)
				return false;
		} else if (!companyNo.equals(other.companyNo))
			return false;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (officeFax == null) {
			if (other.officeFax != null)
				return false;
		} else if (!officeFax.equals(other.officeFax))
			return false;
		if (officePhone == null) {
			if (other.officePhone != null)
				return false;
		} else if (!officePhone.equals(other.officePhone))
			return false;
		if (orgName == null) {
			if (other.orgName != null)
				return false;
		} else if (!orgName.equals(other.orgName))
			return false;
		if (orgNo == null) {
			if (other.orgNo != null)
				return false;
		} else if (!orgNo.equals(other.orgNo))
			return false;
		return true;
	}
	
	
    
    
}
