package com.wstuo.common.security.entity;

import javax.persistence.Entity;
import javax.persistence.Transient;

/**
*
* Entity class Supplier
*/
@Entity
public class Supplier
    extends OrganizationOuter{
	
	@Transient
    public String getOrgType() {
		return "suppiler";
	}
}
