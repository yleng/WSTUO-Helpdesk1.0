package com.wstuo.common.sso.dao;

import java.sql.ResultSet;
import java.sql.SQLException;


public class LoginDAO
extends BaseDAO implements ILoginDAO
{
    
    public int login( String account )
    {
    	runlink(  );
    	int num = 0;
        ResultSet result = null;
        String sql =this.hql;
        try
        {
            this.ps = this.conn.prepareStatement( sql );
            ps.setString(1, account);
            result = this.ps.executeQuery();
            if(result.next()){
            	num = result.getInt(1);
            }
        } catch ( SQLException e )
        {
            System.out.println( "login failure" );
            e.printStackTrace(  );
        } finally
        {
            stoplink(  );
        }

        return num;
    }
}
