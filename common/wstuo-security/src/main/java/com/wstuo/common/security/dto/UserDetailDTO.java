package com.wstuo.common.security.dto;

import java.util.Date;
import java.util.List;

import com.wstuo.common.dto.BaseDTO;

/**
 * 请求详细DTO
 */
@SuppressWarnings("serial")
public class UserDetailDTO extends BaseDTO {
	private Long userId;
    private String loginName; //登录帐号
    private String password; //密码
    private String firstName; //姓名
    private String lastName; //名字
    private String trueName;//真实名称
    private String email; //邮件
    private String moblie; //手机
    private String phone; //电话
    private String officePhone; //办公电话
    private String orgName; //组织(机构)
    private Long orgNo;
    private Boolean userState = false;//状态
    private Float userCost;//成本
    private String job; //职称
    private String fax; //传真
    private String msn;//msn
    private String description;
    private String officeAddress; //办公地址
    private String pinyin;//姓名拼音
    private Date birthday;//生日
    private String position;//职务
    private String icCard;//证件号
    private Boolean sex = false;//性别
    private String lcallCodeOper;//小灵呼语音卡工号
    private String extension;//分机号
    private String fullName;//真实姓名
    private String remoteHost;//小灵呼语音卡远程主机
    private String remotePort;//小灵呼语音卡远程端口
    private Boolean holidayStatus = false;//休假状态
    private List<OrganizationDTO> belongsGroups;//所属组
    private List<String> roleCode;
    
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMoblie() {
		return moblie;
	}
	public void setMoblie(String moblie) {
		this.moblie = moblie;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Long getOrgNo() {
		return orgNo;
	}
	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}
	public Boolean getUserState() {
		return userState;
	}
	public void setUserState(Boolean userState) {
		this.userState = userState;
	}
	public Float getUserCost() {
		return userCost;
	}
	public void setUserCost(Float userCost) {
		this.userCost = userCost;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMsn() {
		return msn;
	}
	public void setMsn(String msn) {
		this.msn = msn;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}
	public String getPinyin() {
		return pinyin;
	}
	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getIcCard() {
		return icCard;
	}
	public void setIcCard(String icCard) {
		this.icCard = icCard;
	}
	public Boolean getSex() {
		return sex;
	}
	public void setSex(Boolean sex) {
		this.sex = sex;
	}
	public String getLcallCodeOper() {
		return lcallCodeOper;
	}
	public void setLcallCodeOper(String lcallCodeOper) {
		this.lcallCodeOper = lcallCodeOper;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getRemoteHost() {
		return remoteHost;
	}
	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}
	public String getRemotePort() {
		return remotePort;
	}
	public void setRemotePort(String remotePort) {
		this.remotePort = remotePort;
	}
	public Boolean getHolidayStatus() {
		return holidayStatus;
	}
	public void setHolidayStatus(Boolean holidayStatus) {
		this.holidayStatus = holidayStatus;
	}
	public List<OrganizationDTO> getBelongsGroups() {
		return belongsGroups;
	}
	public void setBelongsGroups(List<OrganizationDTO> belongsGroups) {
		this.belongsGroups = belongsGroups;
	}
	public List<String> getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(List<String> roleCode) {
		this.roleCode = roleCode;
	}
     
}
