package com.wstuo.common.bpm.entity;

import javax.persistence.MappedSuperclass;

/**
 * 流程动作属性实体类
 * @author wstuo
 *
 */
@MappedSuperclass
public class FlowActionProperty {
	private Long groups;//指派组
	private Long assignee;//指派技术员
	private String formName;//表单
	private String taskAction;//任务动作
	private String noticeRule;//通知规则
	private Boolean dynamicAssignee = false;//是否动态指派
	private Boolean eventAssign = false;//是否使用业务数据中的指派组及指派技术员
	private String validMethod;//验证方法(JS验证函数)
	private Boolean remarkRequired = false;//备注信息是否必填
	private Long statusNo;//对应状态
	private String roleCode;//角色CODE
	private String assignType;//指派方式
	private Boolean approverTask = false;//是否是审批任务
	@Deprecated
	private Boolean allowUseDynamicAssignee = false;//允许动态指派覆盖已有指派;
	private Boolean allowUdateDefaultAssignee = false;//允许修改默认指派
	private Boolean allowUdateDefaultAssigneeGroup = false;//允许修改默认指派组
	private Long noticeRuleId;//任务完成后的 通知规则
	private String noticeRuleIds;//任务完成后的 通知规则
	private Boolean taskAssigneeUserNotice = false;//任务指派通知
	private Boolean taskAssigneeGroupNotice = false;//任务指派组组通知
	private String noticSpecifiedUser;//通知指定的用户
	private String noticSpecifiedEmail;//通知指定的邮箱
	private String variablesAssigneeType;//变量指派类型
	private Long leaderNum;//级层
	@Deprecated
	private Boolean allowUseVariablesAssignee = false; //是否允许变量指派覆盖已有指派
	private Long variablesAssigneeGroupNo; //变量指派指派组
	private String variablesAssigneeGroupName; //变量指派指派组
	private Boolean mailHandlingProcess = false; //是否邮件处理流程
	private Long matchRuleId;//规则编号
	private String matchRuleIds;//多个规则编号
	private String matchRuleName;//规则名称
	private Boolean isUpdateEventAssign=false;//是否更新事件指派
	private Boolean isFallbackToTechnician = false ;//是否回退给上次任务指派的技术员
	private String candidateGroupsNo;//候选组
	private String candidateGroupsName;//候选组名
	private String candidateUsersNo;//候选人
	private String candidateUsersName;//候选人名
	
	public String getCandidateGroupsNo() {
		return candidateGroupsNo;
	}
	public void setCandidateGroupsNo(String candidateGroupsNo) {
		this.candidateGroupsNo = candidateGroupsNo;
	}
	public String getCandidateGroupsName() {
		return candidateGroupsName;
	}
	public void setCandidateGroupsName(String candidateGroupsName) {
		this.candidateGroupsName = candidateGroupsName;
	}
	public String getCandidateUsersNo() {
		return candidateUsersNo;
	}
	public void setCandidateUsersNo(String candidateUsersNo) {
		this.candidateUsersNo = candidateUsersNo;
	}
	public String getCandidateUsersName() {
		return candidateUsersName;
	}
	public void setCandidateUsersName(String candidateUsersName) {
		this.candidateUsersName = candidateUsersName;
	}
	public Boolean getIsFallbackToTechnician() {
		return isFallbackToTechnician;
	}
	public void setIsFallbackToTechnician(Boolean isFallbackToTechnician) {
		this.isFallbackToTechnician = isFallbackToTechnician;
	}
	public Long getGroups() {
		return groups;
	}
	public void setGroups(Long groups) {
		this.groups = groups;
	}
	public Long getAssignee() {
		return assignee;
	}
	public void setAssignee(Long assignee) {
		this.assignee = assignee;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getTaskAction() {
		return taskAction;
	}
	public void setTaskAction(String taskAction) {
		this.taskAction = taskAction;
	}
	public String getNoticeRule() {
		return noticeRule;
	}
	public void setNoticeRule(String noticeRule) {
		this.noticeRule = noticeRule;
	}
	public Boolean getDynamicAssignee() {
		return dynamicAssignee;
	}
	public void setDynamicAssignee(Boolean dynamicAssignee) {
		this.dynamicAssignee = dynamicAssignee;
	}
	public Boolean getEventAssign() {
		return eventAssign;
	}
	public void setEventAssign(Boolean eventAssign) {
		this.eventAssign = eventAssign;
	}
	public String getValidMethod() {
		return validMethod;
	}
	public void setValidMethod(String validMethod) {
		this.validMethod = validMethod;
	}
	public Boolean getRemarkRequired() {
		return remarkRequired;
	}
	public void setRemarkRequired(Boolean remarkRequired) {
		this.remarkRequired = remarkRequired;
	}
	public Long getStatusNo() {
		return statusNo;
	}
	public void setStatusNo(Long statusNo) {
		this.statusNo = statusNo;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getAssignType() {
		return assignType;
	}
	public void setAssignType(String assignType) {
		this.assignType = assignType;
	}
	public Boolean getApproverTask() {
		return approverTask;
	}
	public void setApproverTask(Boolean approverTask) {
		this.approverTask = approverTask;
	}
	public Boolean getAllowUseDynamicAssignee() {
		return allowUseDynamicAssignee;
	}
	public void setAllowUseDynamicAssignee(Boolean allowUseDynamicAssignee) {
		this.allowUseDynamicAssignee = allowUseDynamicAssignee;
	}
	public Boolean getAllowUdateDefaultAssignee() {
		return allowUdateDefaultAssignee;
	}
	public void setAllowUdateDefaultAssignee(Boolean allowUdateDefaultAssignee) {
		this.allowUdateDefaultAssignee = allowUdateDefaultAssignee;
	}
	public Boolean getAllowUdateDefaultAssigneeGroup() {
		return allowUdateDefaultAssigneeGroup;
	}
	public void setAllowUdateDefaultAssigneeGroup(
			Boolean allowUdateDefaultAssigneeGroup) {
		this.allowUdateDefaultAssigneeGroup = allowUdateDefaultAssigneeGroup;
	}
	public Long getNoticeRuleId() {
		return noticeRuleId;
	}
	public void setNoticeRuleId(Long noticeRuleId) {
		this.noticeRuleId = noticeRuleId;
	}
	public String getNoticeRuleIds() {
		return noticeRuleIds;
	}
	public void setNoticeRuleIds(String noticeRuleIds) {
		this.noticeRuleIds = noticeRuleIds;
	}
	public Boolean getTaskAssigneeUserNotice() {
		return taskAssigneeUserNotice;
	}
	public void setTaskAssigneeUserNotice(Boolean taskAssigneeUserNotice) {
		this.taskAssigneeUserNotice = taskAssigneeUserNotice;
	}
	public Boolean getTaskAssigneeGroupNotice() {
		return taskAssigneeGroupNotice;
	}
	public void setTaskAssigneeGroupNotice(Boolean taskAssigneeGroupNotice) {
		this.taskAssigneeGroupNotice = taskAssigneeGroupNotice;
	}
	public String getNoticSpecifiedUser() {
		return noticSpecifiedUser;
	}
	public void setNoticSpecifiedUser(String noticSpecifiedUser) {
		this.noticSpecifiedUser = noticSpecifiedUser;
	}
	public String getNoticSpecifiedEmail() {
		return noticSpecifiedEmail;
	}
	public void setNoticSpecifiedEmail(String noticSpecifiedEmail) {
		this.noticSpecifiedEmail = noticSpecifiedEmail;
	}
	public String getVariablesAssigneeType() {
		return variablesAssigneeType;
	}
	public void setVariablesAssigneeType(String variablesAssigneeType) {
		this.variablesAssigneeType = variablesAssigneeType;
	}
	public Long getLeaderNum() {
		return leaderNum;
	}
	public void setLeaderNum(Long leaderNum) {
		this.leaderNum = leaderNum;
	}
	public Boolean getAllowUseVariablesAssignee() {
		return allowUseVariablesAssignee;
	}
	public void setAllowUseVariablesAssignee(Boolean allowUseVariablesAssignee) {
		this.allowUseVariablesAssignee = allowUseVariablesAssignee;
	}
	public Long getVariablesAssigneeGroupNo() {
		return variablesAssigneeGroupNo;
	}
	public void setVariablesAssigneeGroupNo(Long variablesAssigneeGroupNo) {
		this.variablesAssigneeGroupNo = variablesAssigneeGroupNo;
	}
	public String getVariablesAssigneeGroupName() {
		return variablesAssigneeGroupName;
	}
	public void setVariablesAssigneeGroupName(String variablesAssigneeGroupName) {
		this.variablesAssigneeGroupName = variablesAssigneeGroupName;
	}
	public Boolean getMailHandlingProcess() {
		return mailHandlingProcess;
	}
	public void setMailHandlingProcess(Boolean mailHandlingProcess) {
		this.mailHandlingProcess = mailHandlingProcess;
	}
	public Long getMatchRuleId() {
		return matchRuleId;
	}
	public void setMatchRuleId(Long matchRuleId) {
		this.matchRuleId = matchRuleId;
	}
	public String getMatchRuleIds() {
		return matchRuleIds;
	}
	public void setMatchRuleIds(String matchRuleIds) {
		this.matchRuleIds = matchRuleIds;
	}
	public String getMatchRuleName() {
		return matchRuleName;
	}
	public void setMatchRuleName(String matchRuleName) {
		this.matchRuleName = matchRuleName;
	}
	public Boolean getIsUpdateEventAssign() {
		return isUpdateEventAssign;
	}
	public void setIsUpdateEventAssign(Boolean isUpdateEventAssign) {
		this.isUpdateEventAssign = isUpdateEventAssign;
	}
	
}
