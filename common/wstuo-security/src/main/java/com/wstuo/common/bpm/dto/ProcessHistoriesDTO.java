package com.wstuo.common.bpm.dto;

import java.util.Date;

/**
 * 历史流程实例DTO
 * @author wstuo
 *
 */
public class ProcessHistoriesDTO
{
    private String key;
    private String processDefinitionId;
    private String processInstanceId;
    private String state;
    private Date startTime;
    private Date endTime;
    private Long duration;
    private String endActivityName;

    public String getKey(  )
    {
        return key;
    }

    public void setKey( String key )
    {
        this.key = key;
    }

    public String getProcessDefinitionId(  )
    {
        return processDefinitionId;
    }

    public void setProcessDefinitionId( String processDefinitionId )
    {
        this.processDefinitionId = processDefinitionId;
    }

    public String getProcessInstanceId(  )
    {
        return processInstanceId;
    }

    public void setProcessInstanceId( String processInstanceId )
    {
        this.processInstanceId = processInstanceId;
    }

    public String getState(  )
    {
        return state;
    }

    public void setState( String state )
    {
        this.state = state;
    }

    public Date getStartTime(  )
    {
        return startTime;
    }

    public void setStartTime( Date startTime )
    {
        this.startTime = startTime;
    }

    public Date getEndTime(  )
    {
        return endTime;
    }

    public void setEndTime( Date endTime )
    {
        this.endTime = endTime;
    }

    public Long getDuration(  )
    {
        return duration;
    }

    public void setDuration( Long duration )
    {
        this.duration = duration;
    }

    public String getEndActivityName(  )
    {
        return endActivityName;
    }

    public void setEndActivityName( String endActivityName )
    {
        this.endActivityName = endActivityName;
    }
}
