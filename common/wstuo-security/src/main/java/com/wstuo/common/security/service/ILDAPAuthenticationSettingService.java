package com.wstuo.common.security.service;

import com.wstuo.common.security.dto.LDAPAuthenticationSettingDTO;

/**
 * LDAP验证Service接口类
 */
public interface ILDAPAuthenticationSettingService {
	/**
	 * 保存LDAP验证配置信息
	 * @param dto
	 */
	void saveLDAPAuthenticationSetting(LDAPAuthenticationSettingDTO dto);
	
	
	/**
	 * 获取LDAP验证配置信息
	 * @return LDAPAuthenticationSettingDTO
	 */
	LDAPAuthenticationSettingDTO findLDAPAuthenticationSetting();
}
