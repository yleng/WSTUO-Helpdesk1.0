package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.HolidayQueryDTO;
import com.wstuo.common.security.entity.Holiday;

import java.util.Date;
import java.util.List;

/**
 * 节假日DAO接口.
 * @author will
 */
public interface IHolidayDAO
    extends IEntityDAO<Holiday>
{
    /**
     * 分页查找节假日功能
     * @param  qdto 查询DTO HolidayQueryDTO 
     * @return 分页数据 PageDTO
     */
	PageDTO findHolidayPage( HolidayQueryDTO qdto,String sidx, String sord );

    /**
     * 根据机构查询节假日列表
     * @param  servicesNo 机构编号 Long 
     * @return 节假日列表 List<Holiday>
     */
    List<Holiday> findHolidayByServicesNo( Long servicesNo );
    
    /**
     * 根据机构和日期查询节假日列表
     * @param servicesNo 机构编号 Long
     * @param hdate
     * @return 节假日列表List<Holiday>
     */
    Holiday findHolidayByServicesNoAndHdate( Long servicesNo,Date hdate );
}
