package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.security.entity.ServiceTime;

/**
 * 服务时间DAO接口.
 * @author will
 */
public interface IServiceTimeDAO
    extends IEntityDAO<ServiceTime>
{
    /**
     * 根据机构编号查找服务时间.
     * @param orgNo 机构编号：Long orgNo
     * @return 服务时间对象：ServiceTime
     */
    ServiceTime findByOrgNo( Long orgNo );
}
