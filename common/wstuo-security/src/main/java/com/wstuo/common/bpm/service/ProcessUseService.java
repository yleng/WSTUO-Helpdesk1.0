package com.wstuo.common.bpm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.bpm.service.IProcessUseService;
import com.wstuo.common.bpm.dao.IProcessUseDAO;
import com.wstuo.common.bpm.dto.ProcessUseDTO;
import com.wstuo.common.bpm.entity.ProcessUse;


/**
 * 默认流程设置Service Class
 * @author wstuo
 *
 */
public class ProcessUseService implements IProcessUseService {
	
	@Autowired
	private IProcessUseDAO processUseDAO;

	/**
	 * processUse Save
	 * @param processUseDTO
	 */
	@Transactional
	public void processUseSave(ProcessUseDTO processUseDTO){
		ProcessUse processUse=new ProcessUse();
		ProcessUseDTO.dto2entity(processUseDTO, processUse);
		processUseDAO.merge(processUse);
	}
	/**
	 * find By Id ProcessUse
	 * @param id
	 * @return ProcessUseDTO
	 */
	public ProcessUseDTO findById(Long id){
		ProcessUseDTO processUseDTO=new ProcessUseDTO();
		ProcessUse processUse=processUseDAO.findById(id);
		ProcessUseDTO.entity2dto(processUse, processUseDTO);
		
		return processUseDTO;
	}
	/**
	 * 获取流程选用 
	 * @param useName
	 * @return ProcessUseDTO
	 */
	public ProcessUseDTO findProcessUseByUseName(String useName){
		ProcessUseDTO processUseDTO=new ProcessUseDTO();
		ProcessUse processUse=processUseDAO.findUniqueBy("useName",useName);
		ProcessUseDTO.entity2dto(processUse, processUseDTO);
		return processUseDTO;
	}
	/**
	 * 流程选用 修改
	 * @param processUseDTO
	 */
	@Transactional
	public void editProcessUse(ProcessUseDTO processUseDTO){
		ProcessUse processUse=processUseDAO.findUniqueBy("useName",processUseDTO.getUseName());
		
		processUseDTO.setUseName(processUse.getUseName());
		processUseDTO.setProcessUseNo(processUse.getProcessUseNo());
		
		ProcessUseDTO.dto2entity(processUseDTO, processUse);
		processUseDAO.merge(processUse);
	}
	
}
