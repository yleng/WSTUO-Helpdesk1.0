package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 
 * this is ServiceTimeDTO class
 * 
 * @author Administrator
 */
@SuppressWarnings("serial")
public class ServiceTimeDTO extends BaseDTO {
	private Long sid;
	private Boolean allday = false;
	private Integer startHour;
	private Integer startMinute;
	private Integer endHour;
	private Integer endMinute;
	private Integer startNoonHour;
	private Integer startNoonMinute;
	private Integer endNoonHour;
	private Integer endNoonMinute;
	private Boolean monday = false;
	private Boolean tuesday = false;
	private Boolean wednesday = false;
	private Boolean thursday = false;
	private Boolean friday = false;
	private Boolean saturday = false;
	private Boolean sunday = false;
	private Long orgNo;
	private Long orgName;

	public Integer getStartNoonHour() {
		return startNoonHour;
	}

	public void setStartNoonHour(Integer startNoonHour) {
		this.startNoonHour = startNoonHour;
	}

	public Integer getStartNoonMinute() {
		return startNoonMinute;
	}

	public void setStartNoonMinute(Integer startNoonMinute) {
		this.startNoonMinute = startNoonMinute;
	}

	public Integer getEndNoonHour() {
		return endNoonHour;
	}

	public void setEndNoonHour(Integer endNoonHour) {
		this.endNoonHour = endNoonHour;
	}

	public Integer getEndNoonMinute() {
		return endNoonMinute;
	}

	public void setEndNoonMinute(Integer endNoonMinute) {
		this.endNoonMinute = endNoonMinute;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public Boolean getAllday() {
		return allday;
	}

	public void setAllday(Boolean allday) {
		this.allday = allday;
	}

	public Boolean getMonday() {
		return monday;
	}

	public void setMonday(Boolean monday) {
		this.monday = monday;
	}

	public Boolean getTuesday() {
		return tuesday;
	}

	public void setTuesday(Boolean tuesday) {
		this.tuesday = tuesday;
	}

	public Boolean getWednesday() {
		return wednesday;
	}

	public void setWednesday(Boolean wednesday) {
		this.wednesday = wednesday;
	}

	public Boolean getThursday() {
		return thursday;
	}

	public void setThursday(Boolean thursday) {
		this.thursday = thursday;
	}

	public Boolean getFriday() {
		return friday;
	}

	public void setFriday(Boolean friday) {
		this.friday = friday;
	}

	public Boolean getSaturday() {
		return saturday;
	}

	public void setSaturday(Boolean saturday) {
		this.saturday = saturday;
	}

	public Boolean getSunday() {
		return sunday;
	}

	public void setSunday(Boolean sunday) {
		this.sunday = sunday;
	}

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	public Long getOrgName() {
		return orgName;
	}

	public void setOrgName(Long orgName) {
		this.orgName = orgName;
	}

	public Integer getStartHour() {
		return startHour;
	}

	public void setStartHour(Integer startHour) {
		this.startHour = startHour;
	}

	public Integer getStartMinute() {
		return startMinute;
	}

	public void setStartMinute(Integer startMinute) {
		this.startMinute = startMinute;
	}

	public Integer getEndHour() {
		return endHour;
	}

	public void setEndHour(Integer endHour) {
		this.endHour = endHour;
	}

	public Integer getEndMinute() {
		return endMinute;
	}

	public void setEndMinute(Integer endMinute) {
		this.endMinute = endMinute;
	}

	public ServiceTimeDTO() {

	}

	public ServiceTimeDTO(Long sid, Boolean allday, int startHour, int startMinute, int endHour, int endMinute, Boolean monday, Boolean tuesday, Boolean wednesday, Boolean thursday, Boolean friday, Boolean saturday, Boolean sunday, Long orgNo) {
		super();
		this.sid = sid;
		this.allday = allday;
		this.startHour = startHour;
		this.startMinute = startMinute;
		this.endHour = endHour;
		this.endMinute = endMinute;
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.saturday = saturday;
		this.sunday = sunday;
		this.orgNo = orgNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allday == null) ? 0 : allday.hashCode());
		result = prime * result + ((endHour == null) ? 0 : endHour.hashCode());
		result = prime * result
				+ ((endMinute == null) ? 0 : endMinute.hashCode());
		result = prime * result + ((friday == null) ? 0 : friday.hashCode());
		result = prime * result + ((monday == null) ? 0 : monday.hashCode());
		result = prime * result + ((orgName == null) ? 0 : orgName.hashCode());
		result = prime * result + ((orgNo == null) ? 0 : orgNo.hashCode());
		result = prime * result
				+ ((saturday == null) ? 0 : saturday.hashCode());
		result = prime * result + ((sid == null) ? 0 : sid.hashCode());
		result = prime * result
				+ ((startHour == null) ? 0 : startHour.hashCode());
		result = prime * result
				+ ((startMinute == null) ? 0 : startMinute.hashCode());
		result = prime * result + ((sunday == null) ? 0 : sunday.hashCode());
		result = prime * result
				+ ((thursday == null) ? 0 : thursday.hashCode());
		result = prime * result + ((tuesday == null) ? 0 : tuesday.hashCode());
		result = prime * result
				+ ((wednesday == null) ? 0 : wednesday.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceTimeDTO other = (ServiceTimeDTO) obj;
		if (allday == null) {
			if (other.allday != null)
				return false;
		} else if (!allday.equals(other.allday))
			return false;
		if (endHour == null) {
			if (other.endHour != null)
				return false;
		} else if (!endHour.equals(other.endHour))
			return false;
		if (endMinute == null) {
			if (other.endMinute != null)
				return false;
		} else if (!endMinute.equals(other.endMinute))
			return false;
		if (friday == null) {
			if (other.friday != null)
				return false;
		} else if (!friday.equals(other.friday))
			return false;
		if (monday == null) {
			if (other.monday != null)
				return false;
		} else if (!monday.equals(other.monday))
			return false;
		if (orgName == null) {
			if (other.orgName != null)
				return false;
		} else if (!orgName.equals(other.orgName))
			return false;
		if (orgNo == null) {
			if (other.orgNo != null)
				return false;
		} else if (!orgNo.equals(other.orgNo))
			return false;
		if (saturday == null) {
			if (other.saturday != null)
				return false;
		} else if (!saturday.equals(other.saturday))
			return false;
		if (sid == null) {
			if (other.sid != null)
				return false;
		} else if (!sid.equals(other.sid))
			return false;
		if (startHour == null) {
			if (other.startHour != null)
				return false;
		} else if (!startHour.equals(other.startHour))
			return false;
		if (startMinute == null) {
			if (other.startMinute != null)
				return false;
		} else if (!startMinute.equals(other.startMinute))
			return false;
		if (sunday == null) {
			if (other.sunday != null)
				return false;
		} else if (!sunday.equals(other.sunday))
			return false;
		if (thursday == null) {
			if (other.thursday != null)
				return false;
		} else if (!thursday.equals(other.thursday))
			return false;
		if (tuesday == null) {
			if (other.tuesday != null)
				return false;
		} else if (!tuesday.equals(other.tuesday))
			return false;
		if (wednesday == null) {
			if (other.wednesday != null)
				return false;
		} else if (!wednesday.equals(other.wednesday))
			return false;
		return true;
	}

}
