package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * UserRoleDTO
 */
@SuppressWarnings( "serial" )
public class UserRoleDTO
    extends BaseDTO
{
    private long roleId;
    private String roleName;
    private String roleCode;
    

    public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public UserRoleDTO(  )
    {
        super(  );
    }

    public long getRoleId(  )
    {
        return roleId;
    }

    public void setRoleId( long roleId )
    {
        this.roleId = roleId;
    }

    public String getRoleName(  )
    {
        return roleName;
    }

    public void setRoleName( String roleName )
    {
        this.roleName = roleName;
    }
}
