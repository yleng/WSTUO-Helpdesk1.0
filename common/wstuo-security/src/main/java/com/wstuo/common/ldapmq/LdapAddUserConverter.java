package com.wstuo.common.ldapmq;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.log4j.Logger;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import com.wstuo.common.security.dto.UserDTO;

/**
 * ldap add user Converter
 * @author will
 *
 */
public class LdapAddUserConverter implements MessageConverter {
	final static Logger LOGGER = Logger.getLogger(LdapAddUserConverter.class);
	public Object fromMessage(Message msg) throws JMSException,
			MessageConversionException {
		Object obj =null;
		if (msg instanceof ObjectMessage) {
			HashMap map = (HashMap) ((ObjectMessage) msg).getObjectProperty("Map");
			try {
				// Order,Order,Product must implements Seralizable
				ByteArrayInputStream bis = new ByteArrayInputStream(
						(byte[]) map.get("User"));
				ObjectInputStream ois = new ObjectInputStream(bis);
				obj = ois.readObject();
			} catch (IOException e) {
				LOGGER.error(e);
			} catch (ClassNotFoundException e) {
				LOGGER.error(e);
			}
			return obj;
		} else {
			throw new JMSException("Msg:[" + msg + "] is not Map");
		}
	}

	public Message toMessage(Object obj, Session session) throws JMSException,
			MessageConversionException {
		// check Type
		if (obj instanceof UserDTO) {
			ActiveMQObjectMessage objMsg = (ActiveMQObjectMessage) session
					.createObjectMessage();
			Map map = new HashMap();
			ByteArrayOutputStream bos = null;
			ObjectOutputStream oos =null;
			try {
				// Order,Order,Product must implements Seralizable
				bos = new ByteArrayOutputStream();
				oos = new ObjectOutputStream(bos);
				oos.writeObject(obj);
				map.put("User", bos.toByteArray());
				objMsg.setObjectProperty("Map", map);
			} catch (IOException e) {
				LOGGER.error(e);
			} finally{
				try {
					if(bos!=null){
						bos.close();
					}
					if(oos!=null){
						oos.close();
					}
				} catch (IOException e) {
					LOGGER.error(e);
				}
			}
			return objMsg;
		} else {
			throw new JMSException("Object:[" + obj + "] is not User");
		}
	}

}
