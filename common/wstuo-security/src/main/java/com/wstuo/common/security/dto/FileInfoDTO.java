package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * FileInfoDTO
 * @author will
 */
@SuppressWarnings("serial")
public class FileInfoDTO extends BaseDTO {

	private String err = "";
    private String msg = "";
    private Long fileSize;
    
    
    public FileInfoDTO() {
        super();
    }

    public String getErr() {

        return err;
    }

    public void setErr(String err) {

        this.err = err;
    }

    public String getMsg() {

        return msg;
    }

    public void setMsg(String msg) {

        this.msg = msg;
    }

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
    
}