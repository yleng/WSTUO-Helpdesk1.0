package com.wstuo.common.security.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wstuo.common.security.dto.OnlineUserDTO;

/**
 * 用户在线状况统计
 */
public class OnlineUserService implements IOnlineUserService {
    
    /**
     * 租户和对应的在线用户的集合
     */
    public static final Map<String, Object> onlineUsers = new HashMap<String, Object>();
    
    /**
     * 设置租户和对应的在线用户的集合
     * @param tenantId 租户ID
     * @param userName 用户名
     * @param isTerminal 是否技术员
     */
    public void setOnlineUsersMap(String tenantId, String userName, boolean isTerminal) {
        
        // 先下线
        setUserOffline(tenantId, userName);
        
        // 用于存放某个租户下的所有用户
        List<OnlineUserDTO> users = getUsersByTenantId(tenantId);
        
        // 设置一个用户信息
        //Map<String, Object> userInfo = new HashMap<String, Object>();
        //userInfo.put("userName", userName);
        //userInfo.put("isTerminal", isTerminal);
        
        OnlineUserDTO dto = new OnlineUserDTO(userName, isTerminal);
        
        // 将当前的用户信息存入该租户的用户集合列表
        users.add(dto);
        onlineUsers.put(tenantId, users);
        
        users = null;
        
    }
    
    /**
     * 根据租户id获取在线的用户
     * @param tenantId
     * @return
     */
    public List<OnlineUserDTO> getUsersByTenantId (String tenantId) {
        List<OnlineUserDTO> users = (List<OnlineUserDTO>) onlineUsers.get(tenantId);
        List<OnlineUserDTO> list = new ArrayList<OnlineUserDTO>();
        
        if (users != null && users.size() > 0){
            list = new ArrayList<OnlineUserDTO>(users);
        }
        return  list;
    }
    
    /**
     * 根据租户id统计在线用户数
     * @param tenantId
     * @return {总数, 技术员数, 终端数}
     */
    public int[] countUserByTenantId (String tenantId) {
        int count = getUsersByTenantId(tenantId).size();
        int terminal = 0;
        for (OnlineUserDTO dto : getUsersByTenantId(tenantId)) {
            boolean isTerminal = dto.getIsTerminal();
            if (isTerminal)
                terminal ++;
        }
        return new int[]{count, terminal, (count - terminal)};
    }
    
    /**
     * 从租户和对应的在线用户的集合中移除
     * @param tenantId
     * @param userName
     */
    public void setUserOffline(String tenantId, String userName) {
        List<OnlineUserDTO> list = (List<OnlineUserDTO>) getUsersByTenantId(tenantId);
        for (OnlineUserDTO dto : list) {
            if (userName!=null && dto.getUserName().equals(userName)) {
                list.remove(dto);
                break;
            }
        }
        onlineUsers.put(tenantId, list);
        if (list.size() < 1)
            onlineUsers.remove(tenantId);
        
        list = null;
    }
}
