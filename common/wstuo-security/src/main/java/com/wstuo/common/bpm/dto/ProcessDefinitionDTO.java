package com.wstuo.common.bpm.dto;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * Process Definition DTO
 * @author wstuo
 *
 */
@SuppressWarnings("serial")
public class ProcessDefinitionDTO extends AbstractValueObject{
    private String Id;
    private String key;
    private String name;
    private int version;
    private String description;
    private String imageResourceName;
    private String deploymentId;

    public String getId(  )
    {
        return Id;
    }

    public void setId( String id )
    {
        Id = id;
    }

    public String getKey(  )
    {
        return key;
    }

    public void setKey( String key )
    {
        this.key = key;
    }

    public String getName(  )
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public int getVersion(  )
    {
        return version;
    }

    public void setVersion( int version )
    {
        this.version = version;
    }

    public String getDescription(  )
    {
        return description;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public String getImageResourceName(  )
    {
        return imageResourceName;
    }

    public void setImageResourceName( String imageResourceName )
    {
        this.imageResourceName = imageResourceName;
    }

    public String getDeploymentId(  )
    {
        return deploymentId;
    }

    public void setDeploymentId( String deploymentId )
    {
        this.deploymentId = deploymentId;
    }
}
