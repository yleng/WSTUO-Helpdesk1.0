package com.wstuo.common.security.dto;

import java.io.File;

import com.wstuo.common.dto.BaseDTO;

/**
 * 
 * Role Query DTO
 * 
 * @author Administrator
 */
public class RoleQueryDTO extends BaseDTO {
	private String roleName;
	private String roleCode;
	private String description;
	private String remark;
	private Integer start;
	private Integer limit;
	private File importFile;

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public RoleQueryDTO() {
		super();
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

}
