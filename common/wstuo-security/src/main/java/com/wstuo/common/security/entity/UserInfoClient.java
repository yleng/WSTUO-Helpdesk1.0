package com.wstuo.common.security.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wstuo.common.entity.BaseEntity;

/**客户端信息的实体
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Table(name = "T_UserInfoClient")
public class UserInfoClient extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String moblieClientId; // 客户端唯一标识
	private Integer messageCount = 0;//未读消息数量
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getMoblieClientId() {
		return moblieClientId;
	}
	public void setMoblieClientId(String moblieClientId) {
		this.moblieClientId = moblieClientId;
	}
	public Integer getMessageCount() {
		return messageCount;
	}
	public void setMessageCount(Integer messageCount) {
		this.messageCount = messageCount;
	}
	public UserInfoClient(Long id, String moblieClientId,
			Integer messageCount) {
		super();
		this.id = id;
		this.moblieClientId = moblieClientId;
		this.messageCount = messageCount;
	}
	public UserInfoClient() {
		super();
	}
	@Override
	public String toString() {
		return "UserInfoClient [id=" + id 
				+ ", moblieClientId=" + moblieClientId + ", messageCount="
				+ messageCount + "]";
	}
}
