package com.wstuo.common.bpm.dao;

import com.wstuo.common.bpm.entity.FlowTransition;
import com.wstuo.common.dao.IEntityDAO;
/**
 * Flow Transition DAO Interface Class
 * @author wstuo
 *
 */
public interface IFlowTransitionDAO extends IEntityDAO<FlowTransition> {

}
