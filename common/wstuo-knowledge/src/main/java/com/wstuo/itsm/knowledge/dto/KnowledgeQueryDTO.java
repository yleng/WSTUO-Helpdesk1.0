package com.wstuo.itsm.knowledge.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.wstuo.common.tools.entity.ExportInfo;
import com.wstuo.common.dto.AbstractValueObject;


/**
 * 知识库信息查询DTO类
 * @author Eileen
 *
 */
public class KnowledgeQueryDTO extends AbstractValueObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9058550228352226406L;
	private Long[] kids;
	private Long eventId;
	private Long[] eventIds;
	private String category;
    private Date addTime;
    private String title;
    private String keyWord;
    private Date startTime;
    private Date endTime;
    
    private String importPath;
	
    private String [] categoryNames;
    private Long [] categoryIds;
    private Long [] companyNos;
    private String loginUserName="";
    private Long[] enableCategorys; 
    private Long filterId;//过滤器ID 
    
    
    private String queryType;
    private String attachmentContent;
    
    private Long serviceDirectoryItemNo;
    
    private String opt;
    private String creatorName;
    private String status;
    private String creator;
    private String creatorFullName;
    private Long[] knowledgeServiceNo;
	private String fileFormat;
	private String content;
	private String groupField;//分组统计字段
    private ExportInfo entity;

    private String lang;  //i18n国际化参数
    private String tanentId;
	private String sord;
	private String sidx;
	private int page;
	private	int rows;
	
	private String alias;
	private ExportInfo exportInfo;
	
	
	public Long[] getKids() {
		return kids;
	}

	public void setKids(Long[] kids) {
		this.kids = kids;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getTanentId() {
		return tanentId;
	}

	public void setTanentId(String tanentId) {
		this.tanentId = tanentId;
	}
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public ExportInfo getExportInfo() {
		return exportInfo;
	}

	public void setExportInfo(ExportInfo exportInfo) {
		this.exportInfo = exportInfo;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public ExportInfo getEntity() {
		return entity;
	}

	public void setEntity(ExportInfo entity) {
		this.entity = entity;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getGroupField() {
		return groupField;
	}

	public void setGroupField(String groupField) {
		this.groupField = groupField;
	}
	public Long getEventId() {
		return eventId;
	}
	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}
	public Long[] getEventIds() {
		return eventIds;
	}
	public void setEventIds(Long[] eventIds) {
		this.eventIds = eventIds;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getFileFormat() {
		return fileFormat;
	}
	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}
	public Long[] getKnowledgeServiceNo() {
		return knowledgeServiceNo;
	}
	public void setKnowledgeServiceNo(Long[] knowledgeServiceNo) {
		this.knowledgeServiceNo = knowledgeServiceNo;
	}
	public Long getServiceDirectoryItemNo() {
		return serviceDirectoryItemNo;
	}
	public void setServiceDirectoryItemNo(Long serviceDirectoryItemNo) {
		this.serviceDirectoryItemNo = serviceDirectoryItemNo;
	}
	public String getAttachmentContent() {
		return attachmentContent;
	}
	public void setAttachmentContent(String attachmentContent) {
		this.attachmentContent = attachmentContent;
	}
	public String getQueryType() {
		return queryType;
	}
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	public Long[] getEnableCategorys() {
		return enableCategorys;
	}
	public void setEnableCategorys(Long[] enableCategorys) {
		this.enableCategorys = enableCategorys;
	}
	public String getLoginUserName() {
		return loginUserName;
	}
	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}
	public String[] getCategoryNames() {
		return categoryNames;
	}
	public void setCategoryNames(String[] categoryNames) {
		this.categoryNames = categoryNames;
	}
	public String getImportPath() {
		return importPath;
	}
	public void setImportPath(String importPath) {
		this.importPath = importPath;
	}
    
    
    public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord.replace("%", "\\%");
	}
	private Integer start;
    private Integer limit;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title.replace("%", "\\%");
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Long getFilterId() {
		return filterId;
	}
	public void setFilterId(Long filterId) {
		this.filterId = filterId;
	}
	public String getOpt() {
		return opt;
	}
	public void setOpt(String opt) {
		this.opt = opt;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreatorFullName() {
		return creatorFullName;
	}
	public void setCreatorFullName(String creatorFullName) {
		this.creatorFullName = creatorFullName;
	}

    public static boolean isContainOpt(String opt){
    	List<String> lists = new ArrayList<String>();
    	lists.add("my");
    	lists.add("myap");
    	lists.add("myapf");
    	lists.add("allapf");
    	lists.add("allap");
    	return lists.contains(opt);
    }

	public Long[] getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(Long[] categoryIds) {
		this.categoryIds = categoryIds;
	}

	public Long[] getCompanyNos() {
		return companyNos;
	}

	public void setCompanyNos(Long[] companyNos) {
		this.companyNos = companyNos;
	}
	
}