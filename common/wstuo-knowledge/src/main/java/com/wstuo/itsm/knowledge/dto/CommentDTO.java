package com.wstuo.itsm.knowledge.dto;


import java.util.Date;

import com.wstuo.common.dto.BaseDTO;


public class CommentDTO extends BaseDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String remark;//内容
	public String creator;//创建人
	private String stars;//星级
	private String type;//类型
	private long knowledgeNo;//知识库
	private Date createTime;
	
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getStars() {
		return stars;
	}
	public void setStars(String stars) {
		this.stars = stars;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getKnowledgeNo() {
		return knowledgeNo;
	}
	public void setKnowledgeNo(long knowledgeNo) {
		this.knowledgeNo = knowledgeNo;
	}
	
	
}
