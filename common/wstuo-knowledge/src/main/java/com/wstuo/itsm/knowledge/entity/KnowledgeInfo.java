package com.wstuo.itsm.knowledge.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.config.comment.entity.Comment;
import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.User;

/**
 * 附件详细信息实体类
 * @author Eileen
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class KnowledgeInfo extends BaseEntity<KnowledgeInfo> {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long kid;
    private String title;
    @ManyToOne
    @JoinColumn(name = "categoryNo")
    private EventCategory category;
    @ManyToMany
    private List<Attachment> attachements;
    private String permission;
    @Lob
    private String content;
    private String keyWords;
    //@FieldBridge(impl=DateFieldBridge.class)
    private Date addTime;
    private Long clickRate = 0L;

    //@Field
    //@FieldBridge(impl=KnowledgeStatusFieldBridge.class)
    private String knowledgeStatus;//1正常，0审核中，-1审核失败
    @Lob
    private String statusDesc;//状态描述，即审核失败消息.

    private String creatorFullName;
    @ManyToOne
    @JoinColumn( name = "createFullNo" )
    private User create;//创建人
    
	//服务目录项树
    @ManyToMany
    private List<EventCategory> serviceLists;

    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    private List<Comment> evaluates;//星级]
    
    @Column
	private Long count;//引用个数  sun 2015-12-18
    
    
	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public List<Comment> getEvaluates() {
		return evaluates;
	}

	public void setEvaluates(List<Comment> evaluates) {
		this.evaluates = evaluates;
	}

	public List<EventCategory> getServiceLists() {
		return serviceLists;
	}
    
	public void setServiceLists(List<EventCategory> serviceLists) {
		this.serviceLists = serviceLists;
	}

    public Long getKid() {
		return kid;
	}

	public void setKid(Long kid) {
		this.kid = kid;
	}

    public EventCategory getCategory() {
		return category;
	}

	public void setCategory(EventCategory category) {
		this.category = category;
	}
	
	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


    public String getPermission() {

        return permission;
    }

    public void setPermission(String permission) {

        this.permission = permission;
    }

    public String getContent() {

        return content;
    }

    public void setContent(String content) {

        this.content = content;
    }


    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    public Date getAddTime() {

        return addTime;
    }

    public void setAddTime(Date addTime) {

        this.addTime = addTime;
    }

    public Long getClickRate() {

        return clickRate;
    }

    public void setClickRate(Long clickRate) {

        this.clickRate = clickRate;
    }

	public List<Attachment> getAttachements() {
		return attachements;
	}

	public void setAttachements(List<Attachment> attachements) {
		this.attachements = attachements;
	}

	public String getKnowledgeStatus() {
		return knowledgeStatus;
	}

	public void setKnowledgeStatus(String knowledgeStatus) {
		this.knowledgeStatus = knowledgeStatus;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getCreatorFullName() {
		return creatorFullName;
	}

	public void setCreatorFullName(String creatorFullName) {
		this.creatorFullName = creatorFullName;
	}

	public User getCreate() {
		return create;
	}

	public void setCreate(User create) {
		this.create = create;
	}
	
}