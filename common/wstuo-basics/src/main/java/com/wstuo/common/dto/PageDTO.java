package com.wstuo.common.dto;

import java.util.List;


/**
 * This is a pagination DTO
 * @author wstuo.com
 */
public class PageDTO extends AbstractValueObject {

    private static final int DEFAULT_ROW_NUM = 20;
    private int totalSize;//总数
    private List data;
    private int page;//当前页
    private int total;
    private int rows = DEFAULT_ROW_NUM;
    private int start;
    private int end;
    private int pageCount;//页数
    
    public int getStart() {
    	start = (page - 1) * rows;
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		if (totalSize < rows) {
			end = totalSize - 1;
		} else {
			end = rows * page - 1;
			// 如果是最后一页，并且最大可查大于总数 才减去1
			if (page == pageCount && rows * page > totalSize) {
				end = end - (rows * page - totalSize);
			}
			if (end > totalSize) {
				end = totalSize - 1;
			}
		}
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getPageCount() {
		if (totalSize % rows == 0) {
			pageCount = totalSize / rows;
		} else {
			pageCount = (totalSize / rows) + 1;
		}
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getRows() {

        return rows;
    }

    public void setRows(int rows) {

        this.rows = rows;
    }

    public int getPage() {

        return page;
    }

    public void setPage(int page) {

        this.page = page;
    }

    public int getTotal() {

        total = totalSize / rows;

        if ((totalSize % rows) != 0) {

            total = total + 1;
        }

        return total;
    }

    public void setTotal(int total) {

        this.total = total;
    }

    public int getTotalSize() {

        return totalSize;
    }

    public void setTotalSize(int totalSize) {

        this.totalSize = totalSize;
    }

    public List getData() {

        return data;
    }

    public void setData(List data) {

        this.data = data;
    }
}