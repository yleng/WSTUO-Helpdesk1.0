package com.wstuo.common.dto;

import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 全文检索DTO
 */
public class FullTextQueryDTO<T> extends BaseDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3391850050274910764L;
	/**
	 * 全文检索默认字段
	 */
	public static final String DEFAULT_FIELD = "ALL";
	
	private String alias;
	private String queryString = "";
	/** 大于0*/
	private Long companyNo;
	private Long[] companyNos;
	private Long[] categoryIds;
	private String[] queryFields;
	private Class<T> queryClass;
	private String indexClassId;
	/**
	 * 查询的条件，将实例化好的 Criterion 对象放进集合
	 */
	private Map<String, String> expr = new HashMap<String, String>();
	/**
	 * 排序的条件， key：sidx, value: sord
	 */
	private String sortField;
	/**  */
	private String sortType;
	/**
	 * 0和1之间：0代表任何字串都算匹配，而1则代表只有完全符合才算匹配
	 */
	private float accuracy = 0.7f;
	/**
	 * 总数
	 */
	private int count;
	
	private String loginName;//15-04-29 scott

	public FullTextQueryDTO() {
		super();
	}
	/**
	 * 
	 * @param queryClass
	 */
	public FullTextQueryDTO(String[] queryFields, Class<T> queryClass) {
		this.queryFields = queryFields;
		this.queryClass = queryClass;
	}
	/**
	 * 使
	 * @param queryClass
	 */
	public FullTextQueryDTO( Class<T> queryClass) {
		this.queryClass = queryClass;
	}
	/**
	 * 如果该 LastUpdater 为NULL， 使用默认值
	 * @param defaultName
	 * @return
	 */
	public String getLastUpdater(String defaultName) {
		if (lastUpdater == null) {
			setLastUpdater(defaultName);
		}
		return lastUpdater;
	}
	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @param alias
	 *            the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * @return the queryString
	 */
	public String getQueryString() {
		if(queryString==null)
			queryString="";
		return queryString;
	}

	/**
	 * @param queryString
	 *            the queryString to set
	 */
	public void setQueryString(String queryString) {
		this.queryString = queryString.replace("%", "\\%");
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		if (companyNo > 0) {
			this.companyNo = companyNo;
		}
	}

	public Long[] getCompanyNos() {
		return companyNos;
	}

	public void setCompanyNos(Long[] companyNos) {
		this.companyNos = companyNos;
	}


	@Override
	public String toString() {
		return "FullTextQueryDTO [alias=" + alias + ", queryString="
				+ queryString + ", companyNo=" + companyNo + ", companyNos="
				+ Arrays.toString(companyNos) + ", queryFields="
				+ Arrays.toString(queryFields) + ", queryClass=" + queryClass
				+ ", expr=" + expr + ", sort=" + sortField
				+ ", accuracy=" + accuracy + ", count=" + count + "]";
	}
	public String[] getQueryFields() {
//		if (queryFields == null || queryFields.length < 1) {
//			//queryFields = new String[]{DEFAULT_FIELD};
//		}
		return queryFields;
	}

	public void setQueryFields(String[] queryFields) {
		this.queryFields = queryFields;
	}

	public Class<T> getQueryClass() {
		return queryClass;
	}

	public void setQueryClass(Class<T> queryClass) {
		this.queryClass = queryClass;
	}

	public static boolean isValid(FullTextQueryDTO<?> queryDTO) {
		boolean result = false;
		result = (queryDTO != null && queryDTO.getQueryClass( ) != null);
		return result;
	}
	public float getAccuracy() {
		return accuracy;
	}
	
	public void setAccuracy(float accuracy) {
		if (accuracy > 1) {
			this.accuracy = 1;
		}else if (accuracy < 0) {
			this.accuracy = 0;
		}else{
			this.accuracy = accuracy;
		}
	}
	
	public Map<String, String> getExpr() {
		return expr;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	
	public String getSortType() {
		return sortType;
	}
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	public void setExpr(Map<String, String> expr) {
		this.expr = expr;
	}
	public int getCount() {
		if (this.count < 0) {
			this.count = 0;
		}
		return count;
	}
	public void setCount(int count) {
		if (count < 1) {
			count = 0;
		}
		this.count = count;
	}
	public void setCount(Long count) {
		if (count > Integer.MAX_VALUE) {
			this.count = Integer.MAX_VALUE;
		}else{
			setCount( count.intValue() ) ;
		}
	}
	public Long[] getCategoryIds() {
		return categoryIds;
	}
	public void setCategoryIds(Long[] categoryIds) {
		this.categoryIds = categoryIds;
	}
	
	/**
	 * 校验全文检索参数是否合法
	 * //<br>调用参数验证方法，可以减少后续查询条件的判断
	 * @return 参数合法返回true，否则false
	 */
	public boolean validate() {
		boolean result = true;
		if (this.getQueryClass() == null
				|| this.getQueryFields() == null
				|| !StringUtils.hasText(this.getQueryString()) //如果不是合法字符串
				) {
			result = false;
		}
		return result;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getIndexClassId() {
		return indexClassId;
	}
	public void setIndexClassId(String indexClassId) {
		this.indexClassId = indexClassId;
	}
}
