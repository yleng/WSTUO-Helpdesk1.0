package com.wstuo.common.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * Miscellaneous collection utility methods.
 * Mainly for internal use within the framework.
 *
 * @version 1.00 
 * @author wstuo.com
 */
@SuppressWarnings("unchecked")
public abstract class CollectionUtils {

    /**
     * protected constructor.
     */
    protected CollectionUtils() {

    }

    public static boolean isEmpty(Collection collection) {

        return ((collection == null) || collection.isEmpty());
    }
    public static boolean isEmpty(Map map) {

        return ((map == null) || map.isEmpty());
    }
    public static List arrayToList(Object source) {

        return Arrays.asList(ObjectUtils.toObjectArray(source));
    }
    public static void mergeArrayIntoCollection(Object array,
        Collection collection) {

        if (collection == null) {

            throw new IllegalArgumentException(
                "Collection must not be null");
        }

        Object[] arr = ObjectUtils.toObjectArray(array);

        for (int i = 0; i < arr.length; i++) {

            collection.add(arr[i]);
        }
    }
    public static void mergePropertiesIntoMap(Properties props, Map map) {

        if (map == null) {

            throw new IllegalArgumentException("Map must not be null");
        }

        if (props != null) {

            for (Enumeration en = props.propertyNames();
                    en.hasMoreElements();) {

                String key = (String) en.nextElement();

                map.put(key, props.getProperty(key));
            }
        }
    }
    public static boolean contains(Iterator iterator, Object element) {
    	boolean result = false;
        if (iterator != null) {

            while (iterator.hasNext()) {

                Object candidate = iterator.next();

                if (ObjectUtils.nullSafeEquals(candidate, element)) {

                	result = true;
                	break;
                }
            }
        }

        return result;
    }
    public static boolean contains(Enumeration enumeration, Object element) {
    	boolean result = false;

        if (enumeration != null) {

            while (enumeration.hasMoreElements()) {

                Object candidate = enumeration.nextElement();

                if (ObjectUtils.nullSafeEquals(candidate, element)) {
                	result = true;
                	break;
                }
            }
        }

        return result;
    }
    public static boolean containsInstance(Collection collection,
        Object element) {
    	boolean result = false;
        if (collection != null) {

            for (Iterator it = collection.iterator(); it.hasNext();) {

                Object candidate = it.next();

                if (candidate == element) {

                	result = true;
                	break;
                }
            }
        }

        return result;
    }
    public static boolean containsAny(Collection source,
        Collection candidates) {
    	boolean result = false;
        if ( !( isEmpty(source) || isEmpty(candidates) ) ) {

        	for (Iterator it = candidates.iterator(); it.hasNext();) {

                if (source.contains(it.next())) {
                	result = true;
                	break;
                }
            }
        }

        return result;
    }
    public static Object findFirstMatch(Collection source,
        Collection candidates) {
    	Object result = null;
        if ( !( isEmpty(source) || isEmpty(candidates) ) ) {

        	for (Iterator it = candidates.iterator(); it.hasNext();) {

                Object candidate = it.next();

                if (source.contains(candidate)) {

                	result = candidate;
                }
            }
        }

        return result;
    }
    public static Object findValueOfType(Collection collection, Class type) {


        Class typeToUse = ((type != null) ? type : Object.class);
        Object value = null;

        if (!isEmpty(collection)) {

        	for (Iterator it = collection.iterator(); it.hasNext();) {

                Object obj = it.next();

                if (typeToUse.isInstance(obj)) {

                    if (value != null) {

                        throw new IllegalArgumentException(
                            "More than one value of type ["
                            + typeToUse.getName() + "] found");
                    }
                    value = obj;
                }
            }
        }

        return value;
    }

    /**
     * Find a value of one of the given types in the given Collection:
     * searching the Collection for a value of the first type, then
     * searching for a value of the second type, etc.
     * @param collection the collection to search
     * @param types the types to look for, in prioritized order
     * @return a of one of the given types found, or <code>null</code> if none
     */
    public static Object findValueOfType(Collection collection,
        Class[] types) {
    	Object result = null;
        if ( !( isEmpty(collection) || ObjectUtils.isEmpty(types) ) ) {

        	for (int i = 0; i < types.length; i++) {

                Object value = findValueOfType(collection, types[i]);

                if (value != null) {
                	result = value;
                	break;
                }
            }
        }

        return result;
    }

    /**
     * Determine whether the given Collection only contains a single unique object.
     * @param collection the Collection to check
     * @return <code>true</code> if the collection contains a single reference or
     * multiple references to the same instance, <code>false</code> else
     */
    public static boolean hasUniqueObject(Collection collection) {
    	boolean result = true;
        if (!isEmpty(collection)) {

        	boolean hasCandidate = false;
            Object candidate = null;

            for (Iterator it = collection.iterator(); it.hasNext();) {

                Object elem = it.next();

                if (!hasCandidate) {

                    hasCandidate = true;
                    candidate = elem;
                } else if (candidate != elem) {

                	result = false;
                }
            }
        }else{
        	result = false;
        }

        return result;
    }
}