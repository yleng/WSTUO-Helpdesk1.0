package com.wstuo.common.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 重新实现HibernateDaoSupport
 * </p>
 * @author wstuo.com
 */
public class HibernateDaoSupport {
    
	@Autowired()
    private HibernateTemplate hibernateTemplate;

    public HibernateTemplate getHibernateTemplate() {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }
    
    public SessionFactory getSessionFactory() {
        return hibernateTemplate.getSessionFactory();
    }
    
    public Session getSession() {
        return hibernateTemplate.getCurrentSession();
    }
}
