package com.wstuo.common.util;

import java.util.regex.Pattern;

public class MathUtils {
	
	/**
	 * 判断是否为正数
	 * @param num
	 * @return Boolean
	 */
	public static Boolean isPositive(Long num){
		if(num !=null && num > 0l){
			return true;
		}
		return false;
	}
	/**
	 * 判断是否为正数
	 * @param num
	 * @return Boolean
	 */
	public static Boolean isPositive(Integer num){
		if(num !=null && num > 0){
			return true;
		}
		return false;
	}
	/**
	 * 判断是否为正数
	 * @param num
	 * @return Boolean
	 */
	public static Boolean isPositive(Double num){
		if(num !=null && num > 0.0){
			return true;
		}
		return false;
	}
	/**
	 * 判断是否为正整数
	 * @param num
	 * @return Boolean
	 */
	public static Boolean isInteger(String num){
		Pattern pattern = Pattern.compile("[1-9][0-9]*");
		return pattern.matcher(num).matches();
	}
	/**
	 * 判断值是否正确
	 * 
	 * @param ddNo
	 * @return boolean
	 */
	public static boolean isLongEmpty(Long ddNo) {

		if (ddNo != null && ddNo != 0)
			return true;
		else
			return false;
	}
	
	/**
	 * 是否是数字
	 * @param num
	 * @return boolean
	 */
	public static boolean isNum(String num){
		Pattern pattern = Pattern.compile("[0-9]+");
		return pattern.matcher(num).matches();
	}
}
