package com.wstuo.common.tools.entity;

import javax.persistence.Entity;
/**
 * event task entity class
 * @author QXY
 *
 */
@SuppressWarnings({ "serial"})
@Entity
public class EventTask extends Task{
	private Long eno;
	private String eventType;
	
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
}
