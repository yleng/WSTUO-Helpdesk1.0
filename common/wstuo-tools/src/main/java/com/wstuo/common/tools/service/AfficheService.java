package com.wstuo.common.tools.service;

import com.wstuo.common.tools.dao.IAfficheDAO;
import com.wstuo.common.tools.dto.AfficheDTO;
import com.wstuo.common.tools.dto.AfficheQueryDTO;
import com.wstuo.common.tools.entity.AfficheInfo;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.file.csv.CSVReader;
import com.wstuo.common.security.utils.FileEncodeUtils;
import com.wstuo.common.util.StringUtils;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 公告管理Service
 * 
 * @author brian date 2010/9/10
 * **/
public class AfficheService implements IAfficheService {
	final static Logger LOGGER = Logger.getLogger(AfficheService.class);
	@Autowired
	private IAfficheDAO afficheDAO;

	/**
	 * 查询全部公告信息
	 * 
	 * @return List<AfficheDTO>
	 */
	public List<AfficheDTO> findAllAffiche() {
		List<AfficheDTO> dtos = new ArrayList<AfficheDTO>();
		List<AfficheInfo> afficheInfos =  afficheDAO.findAll();
		for(AfficheInfo info : afficheInfos){
			AfficheDTO dto = new AfficheDTO();
			BeanUtils.copyProperties(info, dto);
			dtos.add(dto);
		}
		return dtos;
	}

	/**
	 * 公告分页查询
	 * 
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	@SuppressWarnings("unchecked")
	public PageDTO findPagerAffiche(AfficheQueryDTO dto, int start, int limit,
			String sidx, String sord) {
		PageDTO p = afficheDAO.findPager(dto, start, limit, sidx, sord);
		List<AfficheInfo> entities = (List<AfficheInfo>) p.getData();
		List<AfficheDTO> dtos = new ArrayList<AfficheDTO>(entities.size());
		for (AfficheInfo entity : entities) {
			AfficheDTO adto = new AfficheDTO();
			AfficheDTO.entity2dto(entity, adto);
			dtos.add(adto);
		}
		p.setData(dtos);
		return p;
	}

	/**
	 * 保存公告信息
	 * 
	 * @param dto
	 * 公告信息DTO
	 */
	@Transactional
	public void saveAffiche(AfficheDTO dto) {
		Calendar cd = Calendar.getInstance(); // 取得Calendar实例
		Date d = cd.getTime();
		dto.setAffCreaTime(d);
		AfficheInfo aff = new AfficheInfo();
		AfficheDTO.dto2entity(dto, aff);
		afficheDAO.save(aff);
		dto.setAffId(aff.getAffId());
	}

	/**
	 * 删除公告信息
	 * 
	 * @param affId
	 *            要删除的公告信息ID
	 */
	@Transactional
	public void removeAffiche(Long affId) {
		afficheDAO.delete(afficheDAO.findById(affId));
	}

	/**
	 * 批量删除公告信息
	 * 
	 * @param affids
	 *            要批量删除的公告信息数组ID
	 */
	@Transactional
	public void removeAffiches(Long[] affids) {
		afficheDAO.deleteByIds(affids);
	}

	/**
	 * 修改公告信息
	 * 
	 * @param dto
	 *            公告信息DTO
	 */
	@Transactional
	public void mergeAffiche(AfficheDTO dto) {
		AfficheInfo aff = afficheDAO.findById(dto.getAffId());
		Date d = aff.getAffCreaTime();
		AfficheDTO.dto2entity(dto, aff);
		aff.setAffCreaTime(d);
		afficheDAO.merge(aff);
	}

	/**
	 * 根据标题查询公告信息
	 * 
	 * @param affTitle
	 *            公告信息标题
	 * @return List<AfficheDTO>：
	 */
	@Transactional
	public List<AfficheDTO> findAfficheByName(String affTitle) {
		List<AfficheInfo> entitise = afficheDAO.findByName(affTitle);
		List<AfficheDTO> dtos = new ArrayList<AfficheDTO>();
		if(entitise!=null){
			for (AfficheInfo aff : entitise) {
				AfficheDTO adto = new AfficheDTO();
				AfficheDTO.entity2dto(aff, adto);
				dtos.add(adto);
			}	
		}
		
		return dtos;
	}

	/**
	 * 更新公告信息
	 * 
	 * @param adto
	 *            AfficheDTO
	 * */
	@Transactional
	public void updateAfficheInfo(AfficheDTO adto) {
		AfficheInfo entity = afficheDAO.findById(adto.getAffId());
		BeanUtils.copyProperties(adto, entity);
	}

	/**
	 * 根据公告ID获取公告信息
	 * 
	 * @param affId
	 * @return AfficheDTO
	 */
	@Transactional
	public AfficheDTO findById(Long affId) {
		AfficheDTO dto = new AfficheDTO();
		AfficheInfo aff = afficheDAO.findById(affId);
		AfficheDTO.entity2dto(aff, dto);
		return dto;
	}
	
	/**
	 * 导入公告业务数据
	 * 
	 * @param importFile
	 */
	@Transactional
	public void importAffiche (File importFile) {
		try {
			String fileEncode = FileEncodeUtils.getFileEncode(importFile);
			Reader rd = new InputStreamReader(new FileInputStream(importFile),fileEncode);// 以字节流方式读取数据
			CSVReader reader = new CSVReader(rd);
			String[] line = null;
			Calendar c = Calendar.getInstance();
			try {
				while ((line = reader.readNext()) != null) {
					c.setTime(new Date()); // 设置当前日期
					AfficheInfo afficheInfo = new AfficheInfo();
					afficheInfo.setCreateTime(c.getTime());
					afficheInfo.setAffCreaTime(afficheInfo.getCreateTime());
					afficheInfo.setCompanyNo(1L);
					if (StringUtils.hasText(line[0])) {
						afficheInfo.setAffTitle(line[0]);
					}
					if (StringUtils.hasText(line[1])) {
						afficheInfo.setAffContents(line[1]);
					}
					if (StringUtils.hasText(line[2])) {
						afficheInfo.setAffCreator(line[2]);
					}
					afficheInfo.setAffStart(afficheInfo.getAffCreaTime());
					c.add(Calendar.DATE, 3);//结束时间为3天后
					afficheInfo.setAffEnd(c.getTime());
					afficheDAO.save(afficheInfo);
				}
			} catch (IOException e) {
				LOGGER.error(e);
			} catch (ApplicationException ex) {
				LOGGER.error(ex);
			}

		} catch (Exception e1) {
			e1.printStackTrace();
			LOGGER.error(e1);
		}
	}
}
