package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.dto.UserReturnVisitQueryDTO;
import com.wstuo.common.tools.entity.UserReturnVisit;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 用户回访DAO 接口
 * @author WSTUO
 *
 */
public interface IUserReturnVisitDAO extends IEntityDAO<UserReturnVisit> {
	/**
	 * 分页查询回访结果
	 * @param queryDTO
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	PageDTO findPagerUserReturnVisit(UserReturnVisitQueryDTO queryDTO,String sidx, String sord);
}
