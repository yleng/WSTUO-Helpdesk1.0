package com.wstuo.common.tools.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.tools.dto.HistoryRecordDTO;
import com.wstuo.common.tools.entity.HistoryRecord;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 历史记录DAO类
 * @author WSTUO
 *
 */
public class HistoryRecordDAO extends BaseDAOImplHibernate<HistoryRecord> implements IHistoryRecordDAO {
	/**
	 * 查询全部
	 * @param dto HistoryRecordDTO
	 * @return List<HistoryRecordDTO>
	 */
	public List<HistoryRecord> findAllHistoryRecord(HistoryRecordDTO dto){
		final DetachedCriteria dc = DetachedCriteria.forClass(HistoryRecord.class);
		if(dto!=null && dto.getEventType()!=null && dto.getEno()!=null){
			dc.add(Restrictions.eq("eno", dto.getEno()));
			dc.add(Restrictions.eq("eventType", dto.getEventType()));
			if(dto.getLogTitle()!=null){
				dc.add(Restrictions.eq("logTitle", dto.getLogTitle()));
			}
		}
		dc.addOrder(Order.desc("logNo"));
        return super.getHibernateTemplate().findByCriteria(dc);
	}

	/**
	 * 删除根据Eno历史记录
	 * @param eno
	 * @param eventType
	 */
	@Override
	public void deleteHistoryRecordByEno(Long[] enos, String eventType) {
		final DetachedCriteria dc = DetachedCriteria.forClass(HistoryRecord.class);
		dc.add( Restrictions.in("eno", enos ) );
		dc.add( Restrictions.eq("eventType", eventType ) );
		deleteAll(super.getHibernateTemplate().findByCriteria(dc));
		/*
		String hql = "delete from HistoryRecord hr where hr.eno = :eno and hr.eventType = :et ";
		Query query = getSession().createQuery(hql);
		query.setParameter("et", eventType);
		query.setParameter("eno", enos);
		query.executeUpdate();
		*/
	} 
}
