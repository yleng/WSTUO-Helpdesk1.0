package com.wstuo.common.customForm.action;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.customForm.dto.FieldDTO;
import com.wstuo.common.customForm.entity.Field;
import com.wstuo.common.customForm.service.IFieldService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * 自定义字段Action类
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class FieldAction extends ActionSupport {
	@Autowired
	private IFieldService fieldService;
	private PageDTO pageDTO;
	private Long[] ids;
    private String sord;
    private String sidx;
    private int page = 1;
    private int rows = 20;
    private Field field;
    private boolean result;
    private List<FieldDTO> list;
    
	
	public List<FieldDTO> getList() {
		return list;
	}
	public void setList(List<FieldDTO> list) {
		this.list = list;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public IFieldService getFieldService() {
		return fieldService;
	}
	public void setFieldService(IFieldService fieldService) {
		this.fieldService = fieldService;
	}
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}
	public Field getField() {
		return field;
	}
	public void setField(Field field) {
		this.field = field;
	}
	
	/**
	 * 分页查询
	 * @return PageDTO
	 */
	public String findPagefield(){
		int start = (page - 1) * rows;
		pageDTO=fieldService.findPageField(field,start,rows,sidx, sord);
		pageDTO.setPage(page);
		pageDTO.setPage(rows);
		return SUCCESS;
	}
	
	/**
	 * 根据ID获取
	 * @return 
	 */
	public String findFieldById(){
		field=fieldService.findfieldById(field.getId());
		return "fieldDTO";
	}
	
	
	/**
	 * 保存
	 * @return NULL
	 */
	public String save(){
		result=fieldService.add(field);
		return SUCCESS;
	}
	
	/**
	 * 编辑
	 * @return null
	 */
	public String update(){
		result=fieldService.update(field);
		return SUCCESS;
	}
	/**
	 * 删除
	 * @return null
	 */
	public String remove(){
		try {
			result=fieldService.del(ids);
		}catch (Exception e) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", e);
		}
		return SUCCESS;
	}
	public String findByCustom(){
		int start = (page - 1) * rows;
		pageDTO=fieldService.findByCustom(field.getModule(),start,rows,sidx, sord);
		pageDTO.setPage(page);
		pageDTO.setPage(rows);
		return SUCCESS;
	}
	public String findAllField(){
		list=fieldService.findAll(field.getModule());
		return "list";
	}
}
