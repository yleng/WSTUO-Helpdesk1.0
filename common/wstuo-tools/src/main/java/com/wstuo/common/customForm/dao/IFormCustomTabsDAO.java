package com.wstuo.common.customForm.dao;

import com.wstuo.common.customForm.entity.FormCustomTabs;
import com.wstuo.common.dao.IEntityDAO;

public interface IFormCustomTabsDAO extends IEntityDAO<FormCustomTabs>{

}
