package com.wstuo.common.tools.entity;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 页面设置实体
 * @author Candy
 *
 */
@Entity
@Table(name="pagesSet")
@Cacheable
public class PagesSet {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String paName;
	private String imgname;
	private Date createDate;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPaName() {
		return paName;
	}
	public void setPaName(String paName) {
		this.paName = paName;
	}
	public String getImgname() {
		
		return imgname;
	}
	public void setImgname(String imgname) {
	
		this.imgname = imgname;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		if(createDate==null){
			createDate=new Date();
		}
		this.createDate = createDate;
	}
	
}
