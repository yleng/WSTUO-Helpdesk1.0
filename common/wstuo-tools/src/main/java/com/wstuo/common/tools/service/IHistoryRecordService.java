package com.wstuo.common.tools.service;

import java.util.List;

import com.wstuo.common.tools.dto.HistoryRecordDTO;

/**
 * History Record Service interface class
 * @author WSTUO
 *
 */
public interface IHistoryRecordService {
	/**
	 * 查询全部历史
	 * @return List<HistoryRecordDTO>
	 */
	public List<HistoryRecordDTO> findAllHistoryRecord(HistoryRecordDTO dto);
	/**
	 * 保存历史
	 * @param dto
	 */
	public void saveHistoryRecord(HistoryRecordDTO dto);
	/**
	 * 根据登录名，返回显示的名字
	 * @param loginName
	 * @return
	 */
	public String findUserNameByLoginName(String loginName);
	
	/**
	 * 删除历史记录
	 * @param eno
	 * @param eventType
	 */
	void deleteHistoryRecord(Long eno,String eventType);
	
	/**
	 * 删除根据Eno历史记录
	 * @param eno
	 * @param eventType
	 */
	void deleteHistoryRecordByEno(Long[] enos, String eventType);
	void deleteHistoryRecordByEno(List<Long> taskIds, String eventType);
}
