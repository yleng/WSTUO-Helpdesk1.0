package com.wstuo.common.tools.dto;

import com.wstuo.common.config.attachment.entity.Attachment;

import java.util.Date;
import java.util.List;

/**
 * 邮件消息DTO
 * 
 * @author Mars
 * 
 */
public class EmailMessageDTO {
	private Long emailMessageId;
	private String subject; // 邮件主题
	private String description; // 邮件描述
	private String content; // 邮件内容
	private String mailPath; // 邮件存储路径（用来保存邮件在本地)
	private String affachmentPath; // 附件的路径
	private String picPath; // 图片路径
	private List<Attachment> affachment; // 附件
	private String folderName; // 邮件类型--发送or接收

	// 接收
	private String fromUser; // 发件人
	private Date receiveDate; // 接收日期

	// 发送
	private String toUser; // 接收人
	private String bcc; // 密件抄送
	private String cc; // 抄送
	private Date sendDate; // 发送日期
	private Boolean isToRequest = false;
	private String keyword;

	public Long getEmailMessageId() {
		return emailMessageId;
	}

	public void setEmailMessageId(Long emailMessageId) {
		this.emailMessageId = emailMessageId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMailPath() {
		return mailPath;
	}

	public void setMailPath(String mailPath) {
		this.mailPath = mailPath;
	}

	public String getAffachmentPath() {
		return affachmentPath;
	}

	public void setAffachmentPath(String affachmentPath) {
		this.affachmentPath = affachmentPath;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public List<Attachment> getAffachment() {
		return affachment;
	}

	public void setAffachment(List<Attachment> affachment) {
		this.affachment = affachment;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public Boolean getIsToRequest() {
		return isToRequest;
	}

	public void setIsToRequest(Boolean isToRequest) {
		this.isToRequest = isToRequest;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	@Override
	public String toString() {
		return "EmailMessageDTO [emailMessageId=" + emailMessageId
				+ ", subject=" + subject + ", description=" + description
				+ ", content=" + content + ", mailPath=" + mailPath
				+ ", affachmentPath=" + affachmentPath + ", picPath=" + picPath
				+ ", affachment=" + affachment + ", folderName=" + folderName
				+ ", fromUser=" + fromUser + ", receiveDate=" + receiveDate
				+ ", toUser=" + toUser + ", bcc=" + bcc + ", cc=" + cc
				+ ", sendDate=" + sendDate + ", isToRequest=" + isToRequest
				+ ", keyword=" + keyword + "]";
	}
}
