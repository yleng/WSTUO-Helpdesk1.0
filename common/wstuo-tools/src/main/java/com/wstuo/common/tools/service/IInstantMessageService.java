package com.wstuo.common.tools.service;


import com.wstuo.common.tools.dto.InstantMessageDTO;
import com.wstuo.common.tools.dto.InstantMessageQueryDTO;
import com.wstuo.common.tools.dto.MessageConsumerDTO;
import com.wstuo.common.tools.entity.InstantMessage;
import com.wstuo.common.dto.PageDTO;

import java.util.List;

/**
 * 即时信息Service接口类
 * @author QXY
 */
public interface IInstantMessageService
{
    /**
     * 查询全部信息
     * @return List<InstantMessageDTO>
     */
    public List<InstantMessageDTO> findAllInstantMessage(  );

    /**
     * 根据标题查询全部信息
     * @param dto
     * @return List<InstantMessageDTO>
     */
    public List<InstantMessageDTO> findAllIMLikeTitle( String dto );
    
    /**
     * 保存信息
     * @param receiveUsers
     * @param imDTO
     * @param dto
     * @return
     */
    public String saveInstantMessage(String[] receiveUsers, InstantMessageDTO imDTO );
    
    /**
     * 保存信息
     * @param dto
     */
    public void saveInstantMessage( InstantMessageDTO dto );

    /**
     * 删除即时信息
     * @param id 要删除的数组
     */
    public void removeInstantMessage( Long[] id );
    
    /**
     * 批量修改即时消息
     * @param ids
     * @param receiveUsers
     * @param imDTO
     */
    public void updateInstantMessage(Long[] ids,String[] receiveUsers,InstantMessageDTO imDTO);
    
    /**
     * 修改即时信息
     * @param dto
     */
    public void updateInstantMessage( InstantMessageDTO dto );

   /**
    * 即时信息分页查询
    * @param instantmessageQueryDTO
    * @param start
    * @param limit
    * @param sidx
    * @param sord
    * @return PageDTO
    */
    public PageDTO findPagerIM( InstantMessageQueryDTO instantmessageQueryDTO,
    		int start, int limit,String sidx,String sord );

    void sendIM(MessageConsumerDTO messageConsumerDTO);
    /**
     * 未读信息
     * @return int
     */
    int findUnreadIM(  );
    /**
     * 根据即时信息ID获取
     * @param id
     * @return InstantMessage
     */
    public InstantMessage findInstantMessageById(Long id);
    
    /**
     * 根据即时信息ID获取InstantMessageDTO
     * @param id
     * @return InstantMessage
     */
    public InstantMessageDTO findInstantMessageDTOById(Long id);
    /**
     * 根据接受用户Id查询消息
     * @param userId
     * @return List<InstantMessage>
     */
   List<InstantMessage> findByUserId(Long userId);
   /**
    * 更新弹屏发送状态
    */
   void updateDwrSendStatus(List<InstantMessage> lis);
}
