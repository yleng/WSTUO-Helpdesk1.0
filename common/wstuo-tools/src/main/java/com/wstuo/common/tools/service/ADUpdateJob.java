package com.wstuo.common.tools.service;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import com.wstuo.common.security.service.ILDAPService;
import com.wstuo.common.util.AppliactionBaseListener;

/**
 * AD自动更新JOB
 * @author WSTUO
 *
 */
public class ADUpdateJob implements Job {
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		ApplicationContext ctx =AppliactionBaseListener.ctx;
		ILDAPService ldapService=(ILDAPService)ctx.getBean("ldapService");
		ldapService.adAutoUpdate();
	}
}
