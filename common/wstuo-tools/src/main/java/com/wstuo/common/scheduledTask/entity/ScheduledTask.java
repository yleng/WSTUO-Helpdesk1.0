package com.wstuo.common.scheduledTask.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.wstuo.common.entity.BaseEntity;

/**
 * 定期任务实体类
 * @author WSTUO
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
@Table(name="scheduledtask")
public class ScheduledTask extends BaseEntity{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long scheduledTaskId;//ID
	@Lob
	@Column(nullable=false)
	private String requestDtoJson;// 请求详细信息JSON
	
	@Column(nullable=false)
	private String timeType;//时间类型：day、日计划;weekly、周计划;month、月计划;cycle：周期性计划；on_off、一次性计划
	
	@Column(nullable=true)
	private Long taskHour;//小时
	@Column(nullable=true)
	private Long taskMinute;//分
	@Column(nullable=true)
	private Date taskDate;//具体时间(开始时间)
	@Column(nullable=true)
	private Date taskEndDate;//结束时间
	
	@Column(nullable=true)
	private String weekWeeks;//周计划：每周星期几

	@Column(nullable=true)
	private String monthMonths;//月计划：每月
	@Column(nullable=true)
	private String monthDay;//月计划：每月的日期
	
	private String cronExpression;//任务时间

	@Column(nullable=true)
	private Long cyclicalDay;//周期性计划：每过多少天执行一次
	@Column(nullable=false)
	private String beanId;//job beanId
	@Column(nullable=true)
	private String scheduledTaskType;//任务类型:请求、SLA到期、邮件扫描、配置项过期扫描、定期获取AD
	@Column(nullable=true)
	private Long cyclicalMinute;//周期性计划：每过多少分钟执行一次
	//对应的报表编号
	private Long reportId=0L;
	private Long formId;
	
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Long getScheduledTaskId() {
		return scheduledTaskId;
	}
	public void setScheduledTaskId(Long scheduledTaskId) {
		this.scheduledTaskId = scheduledTaskId;
	}


	public String getRequestDtoJson() {
		return requestDtoJson;
	}


	public void setRequestDtoJson(String requestDtoJson) {
		this.requestDtoJson = requestDtoJson;
	}


	public String getTimeType() {
		return timeType;
	}


	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	public Long getTaskHour() {
		return taskHour;
	}


	public void setTaskHour(Long taskHour) {
		this.taskHour = taskHour;
	}


	public Long getTaskMinute() {
		return taskMinute;
	}


	public void setTaskMinute(Long taskMinute) {
		this.taskMinute = taskMinute;
	}


	public Date getTaskDate() {
		return taskDate;
	}


	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
	}


	public String getWeekWeeks() {
		return weekWeeks;
	}


	public void setWeekWeeks(String weekWeeks) {
		this.weekWeeks = weekWeeks;
	}


	public String getMonthMonths() {
		return monthMonths;
	}


	public void setMonthMonths(String monthMonths) {
		this.monthMonths = monthMonths;
	}


	public String getMonthDay() {
		return monthDay;
	}


	public void setMonthDay(String monthDay) {
		this.monthDay = monthDay;
	}


	public Long getCyclicalDay() {
		return cyclicalDay;
	}


	public void setCyclicalDay(Long cyclicalDay) {
		this.cyclicalDay = cyclicalDay;
	}


	public String getCronExpression() {
		return cronExpression;
	}


	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}



	public String getBeanId() {
		return beanId;
	}


	public void setBeanId(String beanId) {
		this.beanId = beanId;
	}


	public Date getTaskEndDate() {
		return taskEndDate;
	}


	public void setTaskEndDate(Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}


	public String getScheduledTaskType() {
		return scheduledTaskType;
	}


	public void setScheduledTaskType(String scheduledTaskType) {
		this.scheduledTaskType = scheduledTaskType;
	}


	public Long getCyclicalMinute() {
		return cyclicalMinute;
	}


	public void setCyclicalMinute(Long cyclicalMinute) {
		this.cyclicalMinute = cyclicalMinute;
	}


	public Long getReportId() {
		return reportId;
	}


	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	
}
