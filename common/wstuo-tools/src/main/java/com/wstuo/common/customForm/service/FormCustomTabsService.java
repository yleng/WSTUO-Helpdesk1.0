package com.wstuo.common.customForm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.customForm.dao.IFormCustomTabsDAO;
import com.wstuo.common.customForm.dto.FormCustomTabsDTO;
import com.wstuo.common.customForm.entity.FormCustomTabs;

public class FormCustomTabsService implements IFormCustomTabsService{
	@Autowired
	private IFormCustomTabsDAO formCustomTabsDAO;
	
	@Transactional
	public Long saveFormCustomTabs(FormCustomTabsDTO formCustomTabsDTO){
		FormCustomTabs tabs = new FormCustomTabs();
		FormCustomTabsDTO.dto2entity(formCustomTabsDTO, tabs);
		formCustomTabsDAO.save(tabs);
		Long id = tabs.getTabId();
		return id;
	}
	
	@Transactional
	public FormCustomTabsDTO findFormCustomTabsByTabsId(Long tabsId){
		FormCustomTabs tabs = formCustomTabsDAO.findById(tabsId);
		FormCustomTabsDTO dto = new FormCustomTabsDTO();
		FormCustomTabsDTO.entity2dto(tabs, dto);
		return dto;
	}

	@Transactional
	public Long updateFormCustomTabs(FormCustomTabsDTO formCustomTabsDTO) {
		FormCustomTabs tabs = new FormCustomTabs();
		FormCustomTabsDTO.dto2entity(formCustomTabsDTO, tabs);
		formCustomTabsDAO.merge(tabs);
		Long id = formCustomTabsDTO.getTabId();
		return id;
	}
	
	

}
