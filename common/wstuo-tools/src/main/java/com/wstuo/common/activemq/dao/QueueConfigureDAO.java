package com.wstuo.common.activemq.dao;

import com.wstuo.common.activemq.entity.QueueConfigure;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 消息队列配置DAO类
 * @author WSTUO
 *
 */
public class QueueConfigureDAO extends BaseDAOImplHibernate<QueueConfigure> implements IQueueConfigureDAO{
		
}
