package com.wstuo.common.tools.action;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.tools.dto.AfficheDTO;
import com.wstuo.common.tools.dto.AfficheQueryDTO;
import com.wstuo.common.tools.service.IAfficheService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.action.SupplierAction;

/**
 * 公告Action类
 * 
 * @author QXY
 */
@SuppressWarnings("serial")
public class AfficheAction extends SupplierAction {
	public static final Logger LOGGER = Logger.getLogger(AfficheAction.class);
	@Autowired
	private IAfficheService afficheService;
	private AfficheDTO afficheDto = new AfficheDTO();
	private AfficheDTO afficheDTO = new AfficheDTO();
	private AfficheQueryDTO afficheQueryDto;
	private PageDTO pageDTO;
	private Long[] ids;
	private int page = 1;
	private int rows = 10;
	private String sidx;
	private String sord;

	public AfficheDTO getAfficheDto() {
		return afficheDto;
	}

	public void setAfficheDto(AfficheDTO afficheDto) {
		this.afficheDto = afficheDto;
	}

	public AfficheDTO getAfficheDTO() {
		return afficheDTO;
	}

	public void setAfficheDTO(AfficheDTO afficheDTO) {
		this.afficheDTO = afficheDTO;
	}

	public AfficheQueryDTO getAfficheQueryDto() {
		return afficheQueryDto;
	}

	public void setAfficheQueryDto(AfficheQueryDTO afficheQueryDto) {
		this.afficheQueryDto = afficheQueryDto;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	/**
	 * 公告查询
	 * 
	 * @return String
	 */
	public String find() {
		int start = (page - 1) * rows;
		pageDTO = afficheService.findPagerAffiche(afficheQueryDto, start, rows,
				sidx, sord);
		pageDTO.setPage(page);
		pageDTO.setRows(rows);
		return SUCCESS;
	}

	/**
	 * 公告添加
	 * 
	 * @return String
	 */
	public String saveAffice() {
		afficheService.saveAffiche(afficheDTO);

		return SUCCESS;
	}

	/**
	 * 公告删除
	 * 
	 * @return String
	 */
	public String delete() {
		LOGGER.info("begin...delete.....");
		afficheService.removeAffiches(ids);
		LOGGER.info("over...delete.....");

		return SUCCESS;
	}

	/**
	 * 公告更新
	 * 
	 * @return String
	 */
	public String update() {
		afficheService.mergeAffiche(afficheDto);

		return SUCCESS;
	}

	/**
	 * 根据公告ID获取公告信息
	 * 
	 * @return AfficheDTO
	 */
	public String findAfficheDTOById() {
		afficheDto = afficheService.findById(afficheDto.getAffId());
		return "afficheDto";
	}

	/**
	 * 登录页面查询所有公告
	 * 
	 * @return pageDTO
	 */
	public String loginFindAllAffiche() {
		int start = (page - 1) * rows;
		pageDTO = afficheService.findPagerAffiche(afficheQueryDto, start, rows,
				sidx, sord);
		pageDTO.setPage(page);
		pageDTO.setRows(rows);
		return "pageDTO";
	}
}
