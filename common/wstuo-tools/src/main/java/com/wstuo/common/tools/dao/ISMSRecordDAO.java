package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.dto.SMSRecordDTO;
import com.wstuo.common.tools.entity.SMSRecord;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * SMS Record DAO Interface
 * @author Candy
 *
 */
public interface ISMSRecordDAO  extends IEntityDAO<SMSRecord>{
	
	/**
	 * 分页查询短信记录.
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
    PageDTO findPager(SMSRecordDTO dto, int start, int limit, String sidx, String sord);

}
