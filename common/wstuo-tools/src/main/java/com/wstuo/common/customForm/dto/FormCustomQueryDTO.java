package com.wstuo.common.customForm.dto;

import java.util.List;

import com.wstuo.common.dto.BaseDTO;

/**
 * 自定义表单查询DTO类
 * 
 * @author Haley
 * 
 */
@SuppressWarnings("serial")
public class FormCustomQueryDTO extends BaseDTO {

	private Long formCustomId;
	private String formCustomName;
	private String formCustomContents;
	private Integer start = 0;
	private Integer limit = 10;
	private Long eventId;
	private String eventName;
	private int isTemplate;
	private String isShowBorder;
	private Boolean isDefault = false;//是否默认
	private Long ciCategoryNo;
	private String type;//模块类型 request ci
	private String ciCategoryName;
	private List<FormCustomTabsDTO> formCustomTabsDtos;
	
	public List<FormCustomTabsDTO> getFormCustomTabsDtos() {
		return formCustomTabsDtos;
	}

	public void setFormCustomTabsDtos(List<FormCustomTabsDTO> formCustomTabsDtos) {
		this.formCustomTabsDtos = formCustomTabsDtos;
	}
	public String getCiCategoryName() {
		return ciCategoryName;
	}
	public void setCiCategoryName(String ciCategoryName) {
		this.ciCategoryName = ciCategoryName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public Long getCiCategoryNo() {
		return ciCategoryNo;
	}

	public void setCiCategoryNo(Long ciCategoryNo) {
		this.ciCategoryNo = ciCategoryNo;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Long getFormCustomId() {
		return formCustomId;
	}

	public void setFormCustomId(Long formCustomId) {
		this.formCustomId = formCustomId;
	}

	public String getFormCustomName() {
		return formCustomName;
	}

	public void setFormCustomName(String formCustomName) {
		this.formCustomName = formCustomName;
	}

	public String getFormCustomContents() {
		return formCustomContents;
	}

	public void setFormCustomContents(String formCustomContents) {
		this.formCustomContents = formCustomContents;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public int getIsTemplate() {
		return isTemplate;
	}

	public void setIsTemplate(int isTemplate) {
		this.isTemplate = isTemplate;
	}

	public String getIsShowBorder() {
		return isShowBorder;
	}

	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}

}
