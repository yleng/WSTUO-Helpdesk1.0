package com.wstuo.common.tools.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author wing
 *
 */
public class TaskShowDataDTO {
	
	   //类型
		private String type;
		//工号
		private String loginName;
		private Long userId;
		//姓名
		private String technician;
		private String module;
		
		private List<ScheduleJsonDTO> data =new ArrayList<ScheduleJsonDTO>();

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getLoginName() {
			return loginName;
		}

		public void setLoginName(String loginName) {
			this.loginName = loginName;
		}

		public Long getUserId() {
			return userId;
		}

		public void setUserId(Long userId) {
			this.userId = userId;
		}

		public String getTechnician() {
			return technician;
		}

		public void setTechnician(String technician) {
			this.technician = technician;
		}

		public String getModule() {
			return module;
		}

		public void setModule(String module) {
			this.module = module;
		}

		public List<ScheduleJsonDTO> getData() {
			return data;
		}

		public void setData(List<ScheduleJsonDTO> data) {
			this.data = data;
		}
		
		

}
