package com.wstuo.common.tools.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 导出信息DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ExportInfoDTO extends BaseDTO{
	private Long exportInfo_id;//ID
	private String exportFileName;//下载名称
	private String exportStatus;//导出状态
	private Long exportFileSize;//文件大小
	private String exportType;//导出类型
	private Date createTime;
	private String exportFileFormat;//文件格式
	
	public String getExportFileFormat() {
		return exportFileFormat;
	}
	public void setExportFileFormat(String exportFileFormat) {
		this.exportFileFormat = exportFileFormat;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public void setExportStatus(String exportStatus) {
		this.exportStatus = exportStatus;
	}
	public Long getExportInfo_id() {
		return exportInfo_id;
	}
	public void setExportInfo_id(Long exportInfo_id) {
		this.exportInfo_id = exportInfo_id;
	}
	public String getExportFileName() {
		return exportFileName;
	}
	public void setExportFileName(String exportFileName) {
		this.exportFileName = exportFileName;
	}
	public String getExportStatus() {
		return exportStatus;
	}
	public void setExportsSatus(String exportStatus) {
		this.exportStatus = exportStatus;
	}
	public Long getExportFileSize() {
		return exportFileSize;
	}
	public void setExportFileSize(Long exportFileSize) {
		this.exportFileSize = exportFileSize;
	}
	public String getExportType() {
		return exportType;
	}
	public void setExportType(String exportType) {
		this.exportType = exportType;
	}
	
	
}
