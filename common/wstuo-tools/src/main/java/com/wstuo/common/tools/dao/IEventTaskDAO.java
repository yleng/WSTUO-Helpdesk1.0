package com.wstuo.common.tools.dao;

import java.util.List;

import com.wstuo.common.tools.dto.EventTaskDTO;
import com.wstuo.common.tools.dto.EventTaskQueryDTO;
import com.wstuo.common.tools.entity.EventTask;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * EventTaskDAO Interface Class
 * @author WSTUO
 *
 */
public interface IEventTaskDAO extends IEntityDAO<EventTask> {
	/**
	 * 分页查询任务
	 * @param queryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPagerEventTask(EventTaskQueryDTO queryDto,int start,int limit);
	
	/**
	 * 查询指定时间段任务
	 * @param eventTaskDTO
	 * @return boolean
	 */
	boolean findSameTimeEventTask(EventTaskDTO eventTaskDTO);
	/**
	 * 查询人员行程管理任务
	 * @param queryDto
	 */
	List<EventTask> findScheduleEventTask(EventTaskQueryDTO queryDto);
}
