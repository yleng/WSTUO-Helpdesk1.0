package com.wstuo.common.tools.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.tools.dao.IHistoryRecordDAO;
import com.wstuo.common.tools.dto.HistoryRecordDTO;
import com.wstuo.common.tools.entity.HistoryRecord;
import com.wstuo.common.security.dao.UserDAO;
import com.wstuo.common.security.entity.User;
import com.wstuo.common.util.StringUtils;

/**
 * History Record Service class
 * @author WSTUO
 *
 */
public class HistoryRecordService implements IHistoryRecordService {
	@Autowired
	private IHistoryRecordDAO historyRecordDAO;
	@Autowired
	private UserDAO userDAO;
	/**
	 * 查询全部历史
	 * @return List<HistoryRecordDTO>
	 */
	@Transactional
	public List<HistoryRecordDTO> findAllHistoryRecord(HistoryRecordDTO dto){
		List<HistoryRecordDTO> hrDTOs=new ArrayList<HistoryRecordDTO>();
			List<HistoryRecord> hrs=historyRecordDAO.findAllHistoryRecord(dto);
			if(hrs!=null ){//去掉&& hrs.size()>0
				for(HistoryRecord hr:hrs){
					HistoryRecordDTO hrDTO=new HistoryRecordDTO();
					HistoryRecordDTO.entity2dto(hr,hrDTO);
					hrDTO.setCreatedTime(hr.getCreateTime());
					hrDTO.setOperator(findUserNameByLoginName(hrDTO.getOperator()));
					hrDTOs.add(hrDTO);
				}
			}
		return hrDTOs;
	}
	/**
	 * 保存历史
	 * @param dto
	 */
	@Transactional
	public void saveHistoryRecord(HistoryRecordDTO dto){
		HistoryRecord hr=new HistoryRecord();
		HistoryRecordDTO.dto2entity(dto, hr);
		hr.setCreateTime(new Date());
		historyRecordDAO.save(hr);
		dto.setLogNo(hr.getLogNo());
	}
	/**
	 * 根据登录名，返回显示的名字
	 * @param loginName
	 * @return
	 */
	public String findUserNameByLoginName(String loginName){
		String userName="";
		User user=userDAO.findUniqueBy("loginName", loginName);
		if(user!=null){
			if(StringUtils.hasText(user.getFullName())){
				userName=user.getFullName();
			}else{
				userName=user.getLoginName();
			}
		}else{
			userName=loginName+"(unknown)";
		}
		return userName;
	}

	/**
	 * 删除历史记录
	 * @param eno
	 * @param eventType
	 */
	@Transactional
	public void deleteHistoryRecord(Long eno,String eventType) {
		if (eno != null && eno.longValue() > 0L && eventType != null) {
			HistoryRecordDTO dto = new HistoryRecordDTO();
			dto.setEno(eno);
			dto.setEventType(eventType);
			List<HistoryRecord> hrs = historyRecordDAO.findAllHistoryRecord(dto);
			if (hrs != null && hrs.size() > 0) {
				historyRecordDAO.deleteAll(hrs);
			}
		}
	}
	
	/**
	 * 删除根据Eno历史记录
	 * @param eno
	 * @param eventType
	 */
	@Transactional
	public void deleteHistoryRecordByEno(Long[] enos, String eventType){
		if (enos != null && enos.length > 0L && StringUtils.hasText(eventType) ) {
			historyRecordDAO.deleteHistoryRecordByEno(enos, eventType);
		}
	}
	/**
	 * 删除根据Eno历史记录
	 * @param eno
	 * @param eventType
	 */
	@Transactional
	public void deleteHistoryRecordByEno(List<Long> taskIds, String eventType) {
		int size = 0;
		if (taskIds != null && (size = taskIds.size()) > 0) {
			Long[] enos = taskIds.toArray(new Long[size]);
			historyRecordDAO.deleteHistoryRecordByEno(enos, eventType);
		}
	}
}
