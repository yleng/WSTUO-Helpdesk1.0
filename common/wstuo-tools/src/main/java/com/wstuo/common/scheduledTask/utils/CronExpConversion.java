package com.wstuo.common.scheduledTask.utils;

import java.util.Calendar;

import com.wstuo.common.scheduledTask.dto.ScheduledTaskDTO;
import com.wstuo.common.util.StringUtils;

/**
 * 页面设置转为UNIX cron expressions 转换类
 * @author WSTUO
 *
 */
public class CronExpConversion {
	
	/**
	 * 页面设置转为UNIX cron expressions 转换算法
	 * 时间类型：day、日计划;weekly、周计划;month、月计划;cycle：周期性计划；on_off、一次性计划
	 * 秒 0-59 , - * /
	 * 分 0-59 , - * /
	 * 小时 0-23 , - * /
	 * 日期 1-31 , - * ? / L W C
	 * 月份 1-12 或者 JAN-DEC , - * /
	 * 星期 1-7 或者 SUN-SAT , - * ? / L C #
	 * 年（可选） 留空, 1970-2099 , - * / 
	 * @param scheduledTaskDTO
	 * @return convertDateToCronExp
	 */
	public String convertDateToCronExp(ScheduledTaskDTO scheduledTaskDTO){
		String cronEx="second minute hour day month week year";
		String cronExs="second minute hour day month week";//不带年
		//cronEx="0/10 * * * * ?";
		if(scheduledTaskDTO!=null){
			Integer year=0;
			Integer month=0;
			Integer day=0;
			if(scheduledTaskDTO.getTaskDate()!=null){
				Calendar cd = Calendar.getInstance();
				cd.setTime(scheduledTaskDTO.getTaskDate());
				year=cd.get(Calendar.YEAR);
				month=cd.get(Calendar.MONTH)+1;
				day=cd.get(Calendar.DATE);
			}
			cronEx = dayPlan(scheduledTaskDTO, cronEx, cronExs);//日计划
			cronEx = weekPlan(scheduledTaskDTO, cronEx, cronExs);//周计划
			cronEx = monthPlan(scheduledTaskDTO, cronEx, cronExs);//月计划
			cronEx = cyclePlan(scheduledTaskDTO, cronEx, cronExs);//周期性计划
			cronEx = oncePlan(scheduledTaskDTO, cronEx, year, month, day);//一次性计划
			cronEx = minutePlan(scheduledTaskDTO, cronEx, year);//周期性计划（分钟）
			
		}
		return cronEx;
	}
	
	private String minutePlan(ScheduledTaskDTO scheduledTaskDTO, String cronEx,
			Integer year) {
		if(scheduledTaskDTO.getTimeType().equals("cycleMinute")){//周期计划(分钟)
			cronEx=cronEx
			.replace("second","0")
			.replace("minute","1/"+scheduledTaskDTO.getCyclicalMinute())
			.replace("hour","*")
			.replace("day","*")
			.replace("month","*")
			.replace("week", "?")
			.replace("year", year.toString());
		}
		return cronEx;
	}

	private String oncePlan(ScheduledTaskDTO scheduledTaskDTO, String cronEx,
			Integer year, Integer month, Integer day) {
		if(scheduledTaskDTO.getTimeType().equals("on_off")){//一次性计划
			cronEx=cronEx
			.replace("second", "0")
			.replace("minute", scheduledTaskDTO.getTaskMinute().toString())
			.replace("hour", scheduledTaskDTO.getTaskHour().toString())
			.replace("day", day.toString())
			.replace("month", month.toString())
			.replace("week", "?")
			.replace("year", year.toString());
		}
		return cronEx;
	}

	private String cyclePlan(ScheduledTaskDTO scheduledTaskDTO, String cronEx,
			String cronExs) {
		if(scheduledTaskDTO.getTimeType().equals("cycle")){//周期计划
			//"0 0 0 ? * *" 每天00:00触发
			cronEx=cronExs
			.replace("second", "0")
			.replace("minute", scheduledTaskDTO.getTaskMinute().toString())
			.replace("hour", scheduledTaskDTO.getTaskHour().toString())
			.replace("day", "1/"+scheduledTaskDTO.getCyclicalDay())
			.replace("month","*")
			.replace("week", "?");
			//.replace("year", "*");
		}
		return cronEx;
	}

	private String monthPlan(ScheduledTaskDTO scheduledTaskDTO, String cronEx,
			String cronExs) {
		if(scheduledTaskDTO.getTimeType().equals("month")){//月计划
			//cronEx="0 11 14 14 * ?" ;//每月14日14:10触发
			String months="-";
			if(StringUtils.hasText(scheduledTaskDTO.getMonthMonths())){
				months=scheduledTaskDTO.getMonthMonths().replaceAll(" ", "");
			}
			cronEx=cronExs
			.replace("second", "0")
			.replace("minute", scheduledTaskDTO.getTaskMinute().toString())
			.replace("hour", scheduledTaskDTO.getTaskHour().toString())
			.replace("day", scheduledTaskDTO.getMonthDay())
			.replace("month",months)
			.replace("week", "?");
			//.replace("year", "*");
		}
		return cronEx;
	}

	private String weekPlan(ScheduledTaskDTO scheduledTaskDTO, String cronEx,
			String cronExs) {
		if(scheduledTaskDTO.getTimeType().equals("weekly")){//周计划
			//"0 15 10 ? * MON-FRI" 周一至周五的上午10:15触发
			String week="-";
			if(StringUtils.hasText(scheduledTaskDTO.getWeekWeeks())){
				week=scheduledTaskDTO.getWeekWeeks().replaceAll(" ", "");
			}
			cronEx=cronExs
			.replace("second", "0")
			.replace("minute", scheduledTaskDTO.getTaskMinute().toString())
			.replace("hour", scheduledTaskDTO.getTaskHour().toString())
			.replace("day", "?")
			.replace("month", "*")
			.replace("week",week);
			//.replace("year", "*");
		}
		return cronEx;
	}

	private String dayPlan(ScheduledTaskDTO scheduledTaskDTO, String cronEx,
			String cronExs) {
		if(scheduledTaskDTO.getTimeType().equals("day")){//日计划
			//cronEx="0 0 12 * * ?"每天中午十二点触发
			cronEx=cronExs
			.replace("second", "0")
			.replace("minute", scheduledTaskDTO.getTaskMinute().toString())
			.replace("hour", scheduledTaskDTO.getTaskHour().toString())
			.replace("day", "*")
			.replace("month", "*")
			.replace("week", "?");
			
		}
		return cronEx;
	}
}
