package com.wstuo.common.tools.dto;

import java.util.Date;

import org.apache.struts2.json.annotations.JSON;

import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.util.TimeUtils;

/**
 * 邮件信息查询DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class EmailMessageQueryDTO extends BaseDTO{

	private String subject; 
    private String description; 
    private String content; 
    private String folderName; 
    private String fromUser; 
    private String toUser;
    private Boolean isToRequest = false;
    private String keyword;
    private Date receiveStartDate; 
    private Date receiveEndDate; 
    private Date sendStartDate;
    private Date sendEndDate;
    private String moduleCode;//关联的模块编号
    private String moduleEno;//关联的模块Eno
    
	public String getModuleEno() {
		return moduleEno;
	}
	public void setModuleEno(String moduleEno) {
		this.moduleEno = moduleEno;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public String getFromUser() {
		return fromUser;
	}
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	public String getToUser() {
		return toUser;
	}
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}
	public Boolean getIsToRequest() {
		return isToRequest;
	}
	public void setIsToRequest(Boolean isToRequest) {
		this.isToRequest = isToRequest;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	@JSON(format=TimeUtils.DATETIME_PATTERN)
	public Date getReceiveStartDate() {
		return receiveStartDate;
	}
	public void setReceiveStartDate(Date receiveStartDate) {
		this.receiveStartDate = receiveStartDate;
	}
	@JSON(format=TimeUtils.DATETIME_PATTERN)
	public Date getSendStartDate() {
		return sendStartDate;
	}
	public void setSendStartDate(Date sendStartDate) {
		this.sendStartDate = sendStartDate;
	}
	@JSON(format=TimeUtils.DATETIME_PATTERN)
	public Date getReceiveEndDate() {
		return receiveEndDate;
	}
	public void setReceiveEndDate(Date receiveEndDate) {
		this.receiveEndDate = receiveEndDate;
	}
	@JSON(format=TimeUtils.DATETIME_PATTERN)
	public Date getSendEndDate() {
		return sendEndDate;
	}
	public void setSendEndDate(Date sendEndDate) {
		this.sendEndDate = sendEndDate;
	}
	
    
}
