package com.wstuo.common.scheduledTask.dto;

import java.util.HashMap;
import java.util.Map;

import com.wstuo.common.dto.BaseDTO;

/**
 * 请求详细DTO类（用于DTO转JSON）
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ScheduledTaskRequestDTO extends BaseDTO {
	private Long companyNo;
	private String companyName;
	private String etitle;
    private String edesc;
    private Long ecategoryNo;
    private String ecategoryName;
    private Long imodeNo;
    private Long priorityNo;
    private Long levelNo;
    private Long effectRangeNo;
    private Long seriousnessNo;
    private Long createdByNo;
    private String createdByName;
    private String creator;
    private Long[] relatedConfigureItemNos;//关联配置项编号
    private String ciids;//关联配置项编号
    private String attachmentStr;
	private Map<String,String> attrVals =new HashMap<String, String>();
	private Long[] requestServiceDirNo;
    private String requestServiceDirNos;
    private String isShowBorder;
    private String serviceDirNames;
    private Long formId;
    private String requestCategoryName;
    private Long requestCategoryNo;
    private Boolean isNewForm;
    
    private Long locationNos;
    private String locationName;//地点

	public Long getLocationNos() {
		return locationNos;
	}
	public void setLocationNos(Long locationNos) {
		this.locationNos = locationNos;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Boolean getIsNewForm() {
		return isNewForm;
	}
	public void setIsNewForm(Boolean isNewForm) {
		this.isNewForm = isNewForm;
	}
    
	public String getRequestCategoryName() {
		return requestCategoryName;
	}
	public void setRequestCategoryName(String requestCategoryName) {
		this.requestCategoryName = requestCategoryName;
	}
	public Long getRequestCategoryNo() {
		return requestCategoryNo;
	}
	public void setRequestCategoryNo(Long requestCategoryNo) {
		this.requestCategoryNo = requestCategoryNo;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
    
	public String getServiceDirNames() {
		return serviceDirNames;
	}
	public void setServiceDirNames(String serviceDirNames) {
		this.serviceDirNames = serviceDirNames;
	}
	public String getIsShowBorder() {
		return isShowBorder;
	}
	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}
	public String getCiids() {
		return ciids;
	}
	public void setCiids(String ciids) {
		this.ciids = ciids;
	}
	public Long[] getRelatedConfigureItemNos() {
		return relatedConfigureItemNos;
	}
	public void setRelatedConfigureItemNos(Long[] relatedConfigureItemNos) {
		this.relatedConfigureItemNos = relatedConfigureItemNos;
	}
	public String getEtitle() {
		return etitle;
	}
	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}
	public String getEdesc() {
		return edesc;
	}
	public void setEdesc(String edesc) {
		this.edesc = edesc;
	}
	public Long getEcategoryNo() {
		return ecategoryNo;
	}
	public void setEcategoryNo(Long ecategoryNo) {
		this.ecategoryNo = ecategoryNo;
	}
	public String getEcategoryName() {
		return ecategoryName;
	}
	public void setEcategoryName(String ecategoryName) {
		this.ecategoryName = ecategoryName;
	}
	public Long getImodeNo() {
		return imodeNo;
	}
	public void setImodeNo(Long imodeNo) {
		this.imodeNo = imodeNo;
	}
	public Long getPriorityNo() {
		return priorityNo;
	}
	public void setPriorityNo(Long priorityNo) {
		this.priorityNo = priorityNo;
	}
	public Long getLevelNo() {
		return levelNo;
	}
	public void setLevelNo(Long levelNo) {
		this.levelNo = levelNo;
	}
	public Long getEffectRangeNo() {
		return effectRangeNo;
	}
	public void setEffectRangeNo(Long effectRangeNo) {
		this.effectRangeNo = effectRangeNo;
	}
	public Long getSeriousnessNo() {
		return seriousnessNo;
	}
	public void setSeriousnessNo(Long seriousnessNo) {
		this.seriousnessNo = seriousnessNo;
	}
	public Long getCreatedByNo() {
		return createdByNo;
	}
	public void setCreatedByNo(Long createdByNo) {
		this.createdByNo = createdByNo;
	}

	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}


	public String getAttachmentStr() {
		return attachmentStr;
	}
	public void setAttachmentStr(String attachmentStr) {
		this.attachmentStr = attachmentStr;
	}
	public Map<String, String> getAttrVals() {
		return attrVals;
	}
	public void setAttrVals(Map<String, String> attrVals) {
		this.attrVals = attrVals;
	}
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Long[] getRequestServiceDirNo() {
		return requestServiceDirNo;
	}
	public void setRequestServiceDirNo(Long[] requestServiceDirNo) {
		this.requestServiceDirNo = requestServiceDirNo;
	}
	public String getRequestServiceDirNos() {
		return requestServiceDirNos;
	}
	public void setRequestServiceDirNos(String requestServiceDirNos) {
		this.requestServiceDirNos = requestServiceDirNos;
	}

    
   
    
}
