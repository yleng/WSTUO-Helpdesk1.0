package com.wstuo.common.tools.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.dto.CostDTO;
import com.wstuo.common.tools.service.ICostService;
import com.wstuo.common.dto.PageDTO;

/**
 * 事件任务Action
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class CostAction extends ActionSupport {
	@Autowired
	private ICostService costService;
	private CostDTO costDTO;
	private Long progressId;
    private PageDTO pageDTO;
    private Long[] ids;
    private int page = 1;
    private int rows = 10;
    private String sidx;
    private String sord;
	public CostDTO getCostDTO() {
		return costDTO;
	}
	public void setCostDTO(CostDTO costDTO) {
		this.costDTO = costDTO;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public Long getProgressId() {
		return progressId;
	}
	public void setProgressId(Long progressId) {
		this.progressId = progressId;
	}
	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	/**
	 * 成本分页查询
	 * @return SUCCESS
	 */
	public String findPagerCost(){
		
		
		int start = ( page - 1 ) * rows;
		pageDTO =costService.findPagerCost(costDTO, start, rows, sidx, sord);
		pageDTO.setPage( page );
		pageDTO.setRows( rows );
		return SUCCESS;
	}
	
	/**
	 * 保存成本
	 * @return String
	 */
	public String saveCost(){
		costService.saveCost(costDTO);
		return SUCCESS;
	}
	
	/**
	 * 编辑成本
	 * @return String
	 */
	public String editCost(){
		costService.editCost(costDTO);
		return SUCCESS;
	}
	
	
	/**
	 * 删除成本
	 * @return String
	 */
	public String deleteCost(){
		costService.deleteCost(ids);
		return SUCCESS;
	}
	
	public String findCostById(){
		costDTO = costService.findCostById(progressId);
		return "costDTO";
	}
	
}
