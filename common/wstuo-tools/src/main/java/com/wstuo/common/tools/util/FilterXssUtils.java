package com.wstuo.common.tools.util;

import com.wstuo.common.util.StringUtils;

public class FilterXssUtils {
	
	public static String string_decode(String s){
		s = s.replaceAll("&amp;", "&")
				.replaceAll("&lt;", "<")
				.replaceAll("&gt;", ">")
				.replaceAll("&#39;", "'")
				.replaceAll("&quot;", "\"")
				.replaceAll("<br>", "\n");
		return s;
	}
	
	
	public static String string_ecode(String s){
		String str="";
		if(StringUtils.hasText(s)){
			str=s.replaceAll("&","&amp;")
			.replaceAll("<","&lt;")
			.replaceAll(">","&gt;")
			.replaceAll("'","&#39;")
			.replaceAll("\"","&quot;")
			.replaceAll("\n","<br>");
		}
		return str;
	}

}
