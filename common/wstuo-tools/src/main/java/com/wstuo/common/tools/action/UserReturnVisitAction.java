package com.wstuo.common.tools.action;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.tools.dto.UserReturnVisitDTO;
import com.wstuo.common.tools.dto.UserReturnVisitDetailDTO;
import com.wstuo.common.tools.dto.UserReturnVisitQueryDTO;
import com.wstuo.common.tools.service.IUserReturnVisitService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.action.SupplierAction;

/**
 * 用户回访事项Action class
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class UserReturnVisitAction extends SupplierAction {
	private PageDTO pageDTO;
	private UserReturnVisitDetailDTO userReturnVisitDetailDTO;
	private UserReturnVisitDTO userReturnVisitDTO;
	private UserReturnVisitQueryDTO queryDTO;
	private String sidx;
	private String sord;
	private int page = 1;
    private int rows = 10;
    private Long[] ids;
    private InputStream exportStream;
    private String fileName="";
    private boolean returnVisitResult;
    @Autowired
    private IUserReturnVisitService userReturnVisitService;
    
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}
	public UserReturnVisitDetailDTO getUserReturnVisitDetailDTO() {
		return userReturnVisitDetailDTO;
	}
	public void setUserReturnVisitDetailDTO(
			UserReturnVisitDetailDTO userReturnVisitDetailDTO) {
		this.userReturnVisitDetailDTO = userReturnVisitDetailDTO;
	}
	public UserReturnVisitDTO getUserReturnVisitDTO() {
		return userReturnVisitDTO;
	}
	public void setUserReturnVisitDTO(UserReturnVisitDTO userReturnVisitDTO) {
		this.userReturnVisitDTO = userReturnVisitDTO;
	}
	public UserReturnVisitQueryDTO getQueryDTO() {
		return queryDTO;
	}
	public void setQueryDTO(UserReturnVisitQueryDTO queryDTO) {
		this.queryDTO = queryDTO;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
    
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	
	public InputStream getExportStream() {
		return exportStream;
	}
	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public boolean isReturnVisitResult() {
		return returnVisitResult;
	}
	public void setReturnVisitResult(boolean returnVisitResult) {
		this.returnVisitResult = returnVisitResult;
	}
	/**
	 * 保存用户回访记录
	 * @return null
	 */
	public String saveUserReturnVisit(){
		
		userReturnVisitService.saveUserReturnVisit(userReturnVisitDTO);
		
		return SUCCESS;
	}
	/**
	 * 编辑用户回访记录
	 * @return null
	 */
	public String editUserReturnVisit(){
		userReturnVisitService.editUserReturnVisit(userReturnVisitDTO);
		return SUCCESS;
	}
	/**
	 * 用户回复回访结果
	 * @return null
	 */
	public String replyUserReturnVisit(){
		returnVisitResult=userReturnVisitService.replyUserReturnVisit(userReturnVisitDTO);
		return "returnVisitResult";
	}
	/**
	 * 删除回访记录
	 * @return null
	 */
	public String deleteUserReturnVisit(){
		userReturnVisitService.deleteUserReturnVisit(ids);
		return SUCCESS;
	}
	
	/**
	 * 分页查询用户回访记录
	 * @return pageDTO
	 */
	public String findPagerReturnVisit(){
		int start = ( page - 1 ) * rows;
		queryDTO.setStart(start);
		queryDTO.setLimit(rows);
		pageDTO=userReturnVisitService.findPagerReturnVisit(queryDTO, sidx, sord);
		pageDTO.setPage( page );
		pageDTO.setRows( rows );
		return SUCCESS;
	}
	
	/**
	 * 根据ID查询回访详细记录
	 * @return userReturnVisitDetailDTO
	 */
	public String findUserReturnVisitById(){
		userReturnVisitDetailDTO=userReturnVisitService.findUserReturnVisitById(queryDTO.getVisitId());
		return "returnVisitDetail";
	}
	/**
	 * 导出回访记录
	 * @return exportStream
	 */
	public String exportReturnVisit(){
		fileName="UserReturnVisit.csv";
	
		queryDTO.setStart(0);
		exportStream=userReturnVisitService.exportReturnVisit(queryDTO, sidx, sord);
		return "exportFileSuccessful";
	}
	
}
