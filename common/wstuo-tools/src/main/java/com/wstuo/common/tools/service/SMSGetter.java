package com.wstuo.common.tools.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 短信发送连接构造
 * 
 * @author QXY
 * 
 */
public class SMSGetter {
	private final static Logger LOGGER = Logger.getLogger(SMSGetter.class);

	public String getSoapSmsGetForSMS(String userid, String pass, String idType) {
	    StringBuffer soap = null;
		try {
		    soap = new StringBuffer();
		    
			soap.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>")
            .append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">")
                .append("<soap:Body>")
                    .append("<GET_SMS_MO xmlns=\"http://tempuri.org/\">")
                        .append("<uid>").append(userid).append("</uid>")
                        .append("<pwd>").append(pass).append("</pwd>")
                        .append("<IDtype>").append(idType).append("</IDtype>")
                    .append("</GET_SMS_MO>")
                .append("</soap:Body>")
            .append("</soap:Envelope>");
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return soap.toString();
	}

	private InputStream getSoapInputForGetSMS(String userid, String pass,
			String idType) throws Exception {

		URLConnection conn = null;
		InputStream is = null;
		try {
			String soap = getSoapSmsGetForSMS(userid, pass, idType);
			if (soap != null) {
				try {
					URL url = new URL("http://service2.winic.org:8003/Service.asmx");

					conn = url.openConnection();
					conn.setUseCaches(false);
					conn.setDoInput(true);
					conn.setDoOutput(true);
					conn.setRequestProperty("Content-Length",
							Integer.toString(soap.length()));
					conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
					conn.setRequestProperty("HOST", "service2.winic.org");
					conn.setRequestProperty("SOAPAction", "\"http://tempuri.org/GET_SMS_MO\"");

					OutputStream os = conn.getOutputStream();
					OutputStreamWriter osw = new OutputStreamWriter(os, "utf-8");
					osw.write(soap);
					osw.flush();
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}
				try {
					is = conn.getInputStream();
				} catch (Exception ex1) {
					LOGGER.error(ex1.getMessage());
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return is;
	}

	// 接收短信回复
	public String GetSMS(String userid, String pass, String idType) {
		String result = "-12";
		InputStream is = null;
		try {
			Document doc;
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			is = getSoapInputForGetSMS(userid, pass, idType); // vfbtye1,
			if (is != null) {
				doc = db.parse(is);
				NodeList nl = doc.getElementsByTagName("GET_SMS_MOResult");
				Node n = nl.item(0);
				result = n.getFirstChild().getNodeValue();
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			try {
				if(is!=null){
					is.close();
				}
			} catch (IOException e) {
				LOGGER.error(e);
			}
		}
		return result;

	}

}
