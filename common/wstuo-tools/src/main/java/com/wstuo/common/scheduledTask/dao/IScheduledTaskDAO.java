package com.wstuo.common.scheduledTask.dao;

import com.wstuo.common.scheduledTask.dto.ScheduledTaskQueryDTO;
import com.wstuo.common.scheduledTask.entity.ScheduledTask;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 定期任务DAO接口类
 * @author WSTUO
 *
 */
public interface IScheduledTaskDAO extends IEntityDAO<ScheduledTask> {
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	PageDTO findPageScheduledTask(ScheduledTaskQueryDTO queryDTO); 
	
	/**
	 * 查询关联表单的数量
	 * @param formId
	 * @return
	 */
	Integer findScheduledTaskNumByFormId(Long[] formIds);

}
