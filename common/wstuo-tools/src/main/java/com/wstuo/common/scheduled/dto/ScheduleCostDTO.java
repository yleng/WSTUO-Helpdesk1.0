package com.wstuo.common.scheduled.dto;

import java.util.Date;

/**
 * 人员行程 进展及成本DTO
 * @author WSTUO
 *
 */
public class ScheduleCostDTO {
	private String loginName;
	//事件ENO
	private Long eno;
	//计划开始时间
	private Date startTime;
	//计划结束时间
	private Date endTime;
	//计划处理时间
	private Long startToEedTime;//开始到结束时间(分钟)
	//实际处理时间
	private Long actualTime;//实际处理时间(分钟)
	
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Long getStartToEedTime() {
		return startToEedTime;
	}
	public void setStartToEedTime(Long startToEedTime) {
		this.startToEedTime = startToEedTime;
	}
	public Long getActualTime() {
		return actualTime;
	}
	public void setActualTime(Long actualTime) {
		this.actualTime = actualTime;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	
	
}
