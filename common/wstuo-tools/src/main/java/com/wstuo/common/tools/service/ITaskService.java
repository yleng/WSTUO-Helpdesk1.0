package com.wstuo.common.tools.service;

import java.io.InputStream;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.tools.dao.ITaskDAO;
import com.wstuo.common.tools.dto.ScheduleJsonDTO;
import com.wstuo.common.tools.dto.TaskDTO;
import com.wstuo.common.tools.dto.TaskQueryDTO;
import com.wstuo.common.tools.dto.TaskViewDTO;
import com.wstuo.common.tools.entity.Task;
import com.wstuo.common.dto.PageDTO;

/**
 * 任务Service接口类
 * @author QXY
 *
 */
public interface ITaskService {
	
	/**
	 * 任务分页查询
	 * @param taskQueryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPagerTask(TaskQueryDTO taskQueryDto,
			int start, int limit,String sidx,String sord);

	/**
	 * 查找最近任务，默认显示本月任务
	 * @return
	 */
	List<TaskDTO> findMyTaskForRecently(TaskQueryDTO taskQueryDto);
	/**
	 * 编辑任务
	 * @param taskDto
	 */
	Long[] editTask(TaskDTO taskDto);
	/**
	 * 保存任务
	 * @param taskDto
	 */
	Long[] saveTask(TaskDTO taskDto);
	/**
	 * 删除任务
	 * @param taskIds
	 */
	void deleteTask(Long[] taskIds);
	void deleteScheduleTask(TaskDTO taskDto);
	/**
	 * 查询全部任务
	 * @param taskQueryDto
	 * @return List<Task>
	 */
	List<Task> findAllTask(TaskQueryDTO taskQueryDto);
	
	/**
	 * 判断同一时间段任务是否冲突
	 * @param taskDTO
	 * @return boolean
	 */
	boolean timeConflict(TaskDTO taskDTO);

	List<ScheduleJsonDTO> findAllTaskShowSchedue(TaskQueryDTO taskQueryDto);
	
	
	/**
	 * 根据任务ID获取任务DTO
	 * @param taskId
	 * @return TaskDTO
	 */
	@Transactional
	TaskDTO findById(Long taskId);
	/**
	 * 导出任务
	 * @param taskQueryDto
	 * @return InputStream
	 */
	InputStream exportTask(TaskQueryDTO taskQueryDto);
	/**
	 * 编辑任务详情
	 * @param dto
	 */
	void editTaskDetail(TaskDTO dto);
	/**
	 * 任务完成
	 * @param dto
	 */
	void closedTaskDetail(TaskDTO dto);
	/**
	 * 处理备注
	 * @param dto
	 */
	void remarkTaskDetail(TaskDTO dto);
	
	boolean findTaskByIds(Long[] ids);
	TaskDTO findPortalById(Long taskId);
	
	TaskDTO taskRealTimeDefaultValue(Long taskId);
	TaskDTO taskRealTimeDefaultValue(TaskDTO taskDTO);
	/**
	 * 查询指定时间段的所有排班任务的负责的用户名
	 * @param taskDto.startTime 不能为null，且必须
	 * @param taskDto.endTime 不能为null，且必须
	 * @return
	 */
	List<String> attendanceTaskByTimeRange(TaskDTO taskDto);
	
	/**
	 * 获取排班管理数据
	 * @param dto
	 * @param sidx
	 * @param sord
	 * @return
	 */
	TaskViewDTO findWorkForceShowData(TaskQueryDTO dto,String sidx,String sord);
	
	/**
	 * 查询指定任务的进展信息
	 * @param taskDto
	 * @return
	 */
	TaskDTO findTaskCostWith(TaskDTO taskDto);
	TaskDTO findTaskCost(TaskDTO taskDto);
	void setTaskDao(ITaskDAO taskDAO);
}
