package com.wstuo.common.tools.service;

import com.wstuo.common.tools.dto.SMSAccountDTO;


/**
 * 短信连接信息帐号Service接口类
 * @author QXY
 */
public interface ISMSAccountService {
	
	/**
	 * 获取短信帐号信息
	 * @return SMSAccountDTO
	 */
	SMSAccountDTO findSMSAccount();
	/**
	 * 保存或更新短信信息
	 * @param dto
	 */
	public void saveOrUpdateSMSAccount(SMSAccountDTO dto);
	/**
	 * 根据系统状态测试连接
	 * @return true连接成功，false连接失败
	 */
	Boolean testConnection();
}
