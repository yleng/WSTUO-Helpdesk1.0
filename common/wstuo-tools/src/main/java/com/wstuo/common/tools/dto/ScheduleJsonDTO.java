package com.wstuo.common.tools.dto;

import java.util.Date;

/**
 * 会员行程JSON DTO
 * @author Administrator
 *
 */
public class ScheduleJsonDTO {
	private Long taskId;
	private String title;
	private Date start;
	private Date end;
	private String url;
	private Boolean allDay = false;
	private String introduction;
	private String location;
	private Long taskStatus = 0L;
	private String className;
	private String owner;
	private String creator;
	private Date realStartTime;
	private Date realEndTime;
	private Double realFree;
	private String treatmentResults;//处理结果
	private String taskCreator;
	private String ownerLoginName;
	private String taskType;//类型
	private String taskCycle;//循环类型
	private String scheduleType;
	
	
	
	public String getScheduleType() {
		return scheduleType;
	}
	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public String getOwnerLoginName() {
		return ownerLoginName;
	}
	public void setOwnerLoginName(String ownerLoginName) {
		this.ownerLoginName = ownerLoginName;
	}
	public String getTaskCreator() {
		return taskCreator;
	}
	public void setTaskCreator(String taskCreator) {
		this.taskCreator = taskCreator;
	}
	public String getTreatmentResults() {
		return treatmentResults;
	}
	public void setTreatmentResults(String treatmentResults) {
		this.treatmentResults = treatmentResults;
	}
	public Date getRealStartTime() {
		return realStartTime;
	}
	public void setRealStartTime(Date realStartTime) {
		this.realStartTime = realStartTime;
	}
	public Date getRealEndTime() {
		return realEndTime;
	}
	public void setRealEndTime(Date realEndTime) {
		this.realEndTime = realEndTime;
	}
	public Double getRealFree() {
		return realFree;
	}
	public void setRealFree(Double realFree) {
		this.realFree = realFree;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public Boolean getAllDay() {
		return allDay;
	}
	public void setAllDay(Boolean allDay) {
		this.allDay = allDay;
	}
	public Long getTaskStatus() {
		if (this.taskStatus == null) {
			return 0L;
		}
		return taskStatus;
	}
	public void setTaskStatus(Long taskStatus) {
		if (this.taskStatus == null) {
			this.taskStatus = 0L;
		}
		this.taskStatus = taskStatus;
	}
	public String getTaskCycle() {
		return taskCycle;
	}
	public void setTaskCycle(String taskCycle) {
		this.taskCycle = taskCycle;
	}
	
}
