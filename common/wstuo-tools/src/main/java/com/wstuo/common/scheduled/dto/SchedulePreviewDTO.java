package com.wstuo.common.scheduled.dto;

/**
 * 人员行程预览DTO
 * @author WSTUO
 *
 */
public class SchedulePreviewDTO {
	private String id;//ID
	private String userName;//顾问姓名
	private String module;//负责模块
	private String queryStartTime;//查询起始日期
	private String queryEndTime;//"查询结束日期",
	private String queryUseDay;//"查询期间内可用工作日",
	private String queryPlanDay;//"查询期间内已计划工作日",
	private String ticketNo;//"编号",
	private String ticketTitle;//"标题",
	private String ticketStatus;//"Tickets状态",
	private String scheduleType;//"行程类型",
	private String startTime;//"开始时间",
	private String endTime;//"结束时间",
	private String scheduleStatus;//"行程状态",
	private String ticketPlanWorkingDay;//"Tickets计划工作日",
	private String completeWorkingDay;//"完成工作日",
	private String completeRates; //"完成率"
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getQueryStartTime() {
		return queryStartTime;
	}
	public void setQueryStartTime(String queryStartTime) {
		this.queryStartTime = queryStartTime;
	}
	public String getQueryEndTime() {
		return queryEndTime;
	}
	public void setQueryEndTime(String queryEndTime) {
		this.queryEndTime = queryEndTime;
	}
	public String getQueryUseDay() {
		return queryUseDay;
	}
	public void setQueryUseDay(String queryUseDay) {
		this.queryUseDay = queryUseDay;
	}
	public String getQueryPlanDay() {
		return queryPlanDay;
	}
	public void setQueryPlanDay(String queryPlanDay) {
		this.queryPlanDay = queryPlanDay;
	}
	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public String getTicketTitle() {
		return ticketTitle;
	}
	public void setTicketTitle(String ticketTitle) {
		this.ticketTitle = ticketTitle;
	}
	public String getTicketStatus() {
		return ticketStatus;
	}
	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
	public String getScheduleType() {
		return scheduleType;
	}
	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getScheduleStatus() {
		return scheduleStatus;
	}
	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}
	public String getTicketPlanWorkingDay() {
		return ticketPlanWorkingDay;
	}
	public void setTicketPlanWorkingDay(String ticketPlanWorkingDay) {
		this.ticketPlanWorkingDay = ticketPlanWorkingDay;
	}
	public String getCompleteWorkingDay() {
		return completeWorkingDay;
	}
	public void setCompleteWorkingDay(String completeWorkingDay) {
		this.completeWorkingDay = completeWorkingDay;
	}
	public String getCompleteRates() {
		return completeRates;
	}
	public void setCompleteRates(String completeRates) {
		this.completeRates = completeRates;
	}
	
}
