package com.wstuo.common.tools.entity;


import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.tools.dto.CostDTO;
import com.wstuo.common.tools.util.ToolsConstant;
import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.User;

/**
 * 任务实体类
 * @author QXY
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Inheritance( strategy = InheritanceType.JOINED )
public class Task  extends BaseEntity implements Comparable<Task>{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long taskId;
	@Column(nullable=false)
	private String title;
	private String introduction;
	@Column(nullable=false)
	private Date startTime;
	@Column(nullable=false)
	private Date endTime;
	private Long taskStatus = 0L;
	private Long taskType = 0L;
	@ManyToOne
	private User owner;
	private Boolean allDay = false;
	private String location;
	private Date realStartTime;//实际开始时间
	private Date realEndTime;//实际完成时间
	private Double realFree;//实际处理时长
	@Lob
	private String treatmentResults;//处理结果
	@ManyToOne
	private User taskCreator;
	@ManyToOne
	private DataDictionaryItems type;
	@OneToOne(cascade=CascadeType.ALL)
	private TaskCycle taskCycle;
	
	public DataDictionaryItems getType() {
		return type;
	}
	public void setType(DataDictionaryItems type) {
		this.type = type;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public User getTaskCreator() {
		return taskCreator;
	}
	public void setTaskCreator(User taskCreator) {
		this.taskCreator = taskCreator;
	}
	public String getTreatmentResults() {
		return treatmentResults;
	}
	public void setTreatmentResults(String treatmentResults) {
		this.treatmentResults = treatmentResults;
	}
	public Date getRealStartTime() {
		return realStartTime;
	}
	public void setRealStartTime(Date realStartTime) {
		this.realStartTime = realStartTime;
	}
	public Date getRealEndTime() {
		return realEndTime;
	}
	public void setRealEndTime(Date realEndTime) {
		this.realEndTime = realEndTime;
	}
	public Double getRealFree() {
		return realFree;
	}
	public void setRealFree(Double realFree) {
		this.realFree = realFree;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public Long getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(Long taskStatus) {
		if (taskStatus == null) {
			taskStatus = 0L;
		}
		this.taskStatus = taskStatus;
	}
	public Long getTaskType() {
		return taskType;
	}
	public void setTaskType(Long taskType) {
		this.taskType = taskType;
	}
	public Boolean getAllDay() {
		return allDay;
	}
	public void setAllDay(Boolean allDay) {
		this.allDay = allDay;
	}
	
	public String getOwnerLoginName(){
		return owner!=null?owner.getLoginName():"";
	}
	public String getTaskCreatorLoginName(){
		return taskCreator!=null?taskCreator.getLoginName():"";
	}
	public TaskCycle getTaskCycle() {
		if (taskCycle == null) {
			taskCycle = new TaskCycle();
		}
		return taskCycle;
	}
	public String taskCycleValue() {
		if (this.taskCycle != null) {
			return this.taskCycle.getType();
		}
		return null;
	}
	/**
	 * 是否是不循环任务
	 * @return
	 */
	public boolean taskNotCycle() {
		String cycleValue = taskCycleValue();
		return ( cycleValue == null || ToolsConstant.TASK_TYPE_NORMAL.equals(cycleValue) );
	}
	public void setTaskCycle(TaskCycle taskCycle) {
		this.taskCycle = taskCycle;
	}
	public Task taskCycleInfo(CostDTO costDTO) {
		if (costDTO != null) {
			this.setTaskStatus( costDTO.getStatus() );
			this.setRealStartTime( costDTO.getStartTime() );
			this.setRealEndTime( costDTO.getEndTime() );
			if ( costDTO.getActualTime()!= null ) {
				this.setRealFree( costDTO.getActualTime() * 1.00 );
			}
		}
		return this;
	}
	
	public Task copy() {
		Task task = new Task();
		task.setTaskId( this.taskId );
		task.setTitle( this.title );
		task.setIntroduction( this.introduction );
		task.setStartTime( this.startTime );
		task.setEndTime( this.endTime );
		task.setTaskStatus( this.taskStatus );
		task.setTaskType( this.taskType );
		task.setOwner( this.owner );
		task.setAllDay( this.allDay );
		task.setLocation( this.location );
		task.setRealStartTime( this.realStartTime );
		task.setRealEndTime( this.realEndTime );
		task.setRealFree( this.realFree );
		task.setTreatmentResults( this.treatmentResults );
		task.setTaskCreator( this.taskCreator );
		task.setType( this.type );
		task.setTaskCycle( this.taskCycle);
		return task;
	}
	@Override
	public String toString() {
		return "Task [taskId=" + taskId + ", title=" + title
				+ ", introduction=" + introduction + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", taskStatus=" + taskStatus
				+ ", taskType=" + taskType + ", owner=" + owner + ", allDay="
				+ allDay + ", location=" + location + ", realStartTime="
				+ realStartTime + ", realEndTime=" + realEndTime
				+ ", realFree=" + realFree + ", treatmentResults="
				+ treatmentResults + ", taskCreator=" + taskCreator + ", type="
				+ type + ", taskCycle=" + taskCycle + "]";
	}
	@Override
	public int compareTo(Task o) {
		return this.getStartTime().compareTo(o.getStartTime());
	}
	
}
