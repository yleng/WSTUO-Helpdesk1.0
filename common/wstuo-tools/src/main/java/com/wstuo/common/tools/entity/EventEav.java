package com.wstuo.common.tools.entity;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.entity.BaseEntity;


/**
 * 事件扩展属性
 * @author WSTUO
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EventEav extends BaseEntity{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long EventEavId;
	private Long eno;
	private String eventType;
	//@OneToMany(cascade=CascadeType.ALL)
    //@MapKey(name="attribute")
    //private Map<Attribute,AttributeValue> attrVals=new HashMap<Attribute,AttributeValue>();
	
	
	public Long getEventEavId() {
		return EventEavId;
	}
	public void setEventEavId(Long eventEavId) {
		EventEavId = eventEavId;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	/*public Map<Attribute, AttributeValue> getAttrVals() {
		return attrVals;
	}
	public void setAttrVals(Map<Attribute, AttributeValue> attrVals) {
		this.attrVals = attrVals;
	}*/
	
	
}
